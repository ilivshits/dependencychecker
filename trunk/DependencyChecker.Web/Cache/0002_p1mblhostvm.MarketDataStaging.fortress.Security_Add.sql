
CREATE PROCEDURE [fortress].[Security_Add]
	@sid bigint,
	@ticker	varchar(50),
	@description varchar(150),
	@currencySid bigint,
	@exchangeId int,
	@securityPrimaryTypeId int,
	@securitySubTypeId int,	
	@startDate datetime,
	@endDate datetime,
	@priceMultiplier float,
	@tradableFlag bit,
	@priceableFlag bit,
	@primaryFlag bit,
	@underlying_sid	bigint,
	@issuerId int,
	@lotSizeInitial float,
	@lotSizeIncrement float,
	
	--- Identifiers.
	@idBloombergCodeShort varchar(100),
	@idBloombergCodeLong varchar(100),
	@idCusip varchar(100),
	@idSedol varchar(100),
	@idIsin varchar(100),
	@idRed6 varchar(100),
	@idRed9 varchar(100),
	@idTradeId varchar(100),
	@idBloombergCdsTicker5Y varchar(100),
	@idBloombergGlobal varchar(100),
	@idBloombergUnique varchar(100),
	@idBelgium varchar(100),
	@idCedelEuroclear varchar(100),
	@idDutch varchar(100),
	@idFrench varchar(100),
	@idValoren varchar(100),
	@idWertpapier varchar(100),
	@idOcc varchar(100),
	@idOpra varchar(100),
	@idBloombergTickerAndExchange varchar(100),
	@idAustrian varchar(100),
	@idCedel varchar(100),
	@idDanish varchar(100),
	@idEuroclear varchar(100),
	@idItaly varchar(100),
	@idJapan varchar(100),
	@idLuxembourg varchar(100),
	@idNorway varchar(100),
	@idSpain varchar(100),
	@idSwedish varchar(100),
	@idXtrakter varchar(100),
	
	--- Restrictions.
	@restriction144A bit,
	@restrictionRegS bit,
			
	--- Auditing.
	@application_source varchar(100),

	--- TODO: Move when appropriate.	
	@alternateDescription varchar(150) = null,
	
	--- TODO: Move when appropriate.
	@idBloombergCodeLocalExchange varchar(100) = null,
	
	--- Industry classifications
	@bloombergIndustrySectorCode varchar(100) = null,
	@bloombergIndustryGroupCode varchar(100) = null,
	@bloombergIndustrySubgroupCode varchar(100) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--- Insert the record.
	insert into FTBTrade.dbo_Security (
		sid, ticker, description, currencySid, exchangeId, securityPrimaryTypeId, securitySubTypeId, 
		startDate, endDate, priceMultiplier, tradableFlag, priceableFlag, primaryFlag, underlying_sid, 
		issuerId, lotSizeInitial, lotSizeIncrement, alternateDescription,
		application_source
	) values (
		@sid,
		@ticker,
		@description,
		@currencySid,
		@exchangeId,
		@securityPrimaryTypeId,
		@securitySubTypeId,
		@startDate,
		@endDate,
		@priceMultiplier,
		@tradableFlag,
		@priceableFlag,
		@primaryFlag,
		@underlying_sid,
		@issuerId,
		@lotSizeInitial,
		@lotSizeIncrement,
		@alternateDescription,
		@application_source
	)
		
	--- Update security identifiers.
	insert into FTBTrade.dbo_SecurityIdentifier (
		sid, identifierTypeId, code, application_source
	)
	select @sid as sid, i.[Bloomberg Code - short] as identifierTypeId, @idBloombergCodeShort as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergCodeShort is not null union
	select @sid as sid, i.[Bloomberg Code - long] as identifierTypeId, @idBloombergCodeLong as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergCodeLong is not null union
	select @sid as sid, i.CUSIP as identifierTypeId, @idCusip as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idCusip is not null union
	select @sid as sid, i.SEDOL as identifierTypeId, @idSedol as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idSedol is not null union
	select @sid as sid, i.ISIN as identifierTypeId, @idIsin as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idIsin is not null union
	select @sid as sid, i.RED6 as identifierTypeId, @idRed6 as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idRed6 is not null union
	select @sid as sid, i.RED9 as identifierTypeId, @idRed9 as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idRed9 is not null union
	select @sid as sid, i.TID as identifierTypeId, @idTradeId as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idTradeId is not null union
	select @sid as sid, i.[Bloomberg CDS Ticker 5Y] as identifierTypeId, @idBloombergCdsTicker5Y as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergCdsTicker5Y is not null union
	select @sid as sid, i.[Bloomberg Global] as identifierTypeId, @idBloombergGlobal as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergGlobal is not null union
	select @sid as sid, i.[Bloomberg Unique] as identifierTypeId, @idBloombergUnique as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergUnique is not null union
	select @sid as sid, i.Belgium as identifierTypeId, @idBelgium as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idBelgium is not null union
	select @sid as sid, i.[Cedel / Euroclear] as identifierTypeId, @idCedelEuroclear as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idCedelEuroclear is not null union
	select @sid as sid, i.Dutch as identifierTypeId, @idDutch as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idDutch is not null union
	select @sid as sid, i.French as identifierTypeId, @idFrench as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idFrench is not null union
	select @sid as sid, i.Valoren as identifierTypeId, @idValoren as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idValoren is not null union
	select @sid as sid, i.Wertpapier as identifierTypeId, @idWertpapier as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idWertpapier is not null union
	select @sid as sid, i.OCC as identifierTypeId, @idOcc as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idOcc is not null union
	select @sid as sid, i.OPRA as identifierTypeId, @idOpra as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idOpra is not null union
	select @sid as sid, i.[Bloomberg Ticker and Exchange] as identifierTypeId, @idBloombergTickerAndExchange as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergTickerAndExchange is not null union
	select @sid as sid, i.Austrian as identifierTypeId, @idAustrian as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idAustrian is not null union
	select @sid as sid, i.Cedel as identifierTypeId, @idCedel as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idCedel is not null union
	select @sid as sid, i.Danish as identifierTypeId, @idDanish as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idDanish is not null union
	select @sid as sid, i.Euroclear as identifierTypeId, @idEuroclear as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idEuroclear is not null union
	select @sid as sid, i.Italy as identifierTypeId, @idItaly as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idItaly is not null union
	select @sid as sid, i.Japan as identifierTypeId, @idJapan as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idJapan is not null union
	select @sid as sid, i.Luxembourg as identifierTypeId, @idLuxembourg as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idLuxembourg is not null union
	select @sid as sid, i.Norway as identifierTypeId, @idNorway as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idNorway is not null union
	select @sid as sid, i.Spain as identifierTypeId, @idSpain as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idSpain is not null union
	select @sid as sid, i.Swedish as identifierTypeId, @idSwedish as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idSwedish is not null union
	select @sid as sid, i.Xtrakter as identifierTypeId, @idXtrakter as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idXtrakter is not null union
	select @sid as sid, i.[Bloomberg Code - Local Exchange] as identifierTypeId, @idBloombergCodeLocalExchange as code, @application_source as application_source from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergCodeLocalExchange is not null
	
	--- Get the current date.
	declare @now date = getdate()
	
	--- Insert the restrictions if they exist, starting from this date.
	insert into FTBTrade.dbo_SecurityRestriction (
		sid, restrictionTypeId, startDate, endDate, application_source
	)
	select @sid as sid, rt.[144a] as restrictionTypeId, @now as startDate, '9999-12-31' as endDate, @application_source as application_source from FTBTrade.dbo_RestrictionType_Transposed as rt where ISNULL(@restriction144A, 0) <> 0 union
	select @sid as sid, rt.REGS as restrictionTypeId, @now as startDate, '9999-12-31' as endDate, @application_source as application_source from FTBTrade.dbo_RestrictionType_Transposed as rt where ISNULL(@restrictionRegS, 0) <> 0
	
	--- Update Bloomberg industry classification.
	exec [FTBTrade].[dbo_SecurityIndustryClassification_Upsert] @sid, @application_source,
		@bloombergIndustrySectorCode, @bloombergIndustryGroupCode, @bloombergIndustrySubgroupCode
END



