﻿using System;
using System.Collections.Generic;
using DependencyChecker.Entities;

namespace DependencyChecker.Collector.Interfaces
{
    public interface IServerProvider
    {
        List<string> GetServers(EntityProviderType type);
        List<String> GetDomains();
    }
}
