
CREATE FUNCTION [tagging].[BloombergIssuerValueSourceConfiguration_GetCalls]
(	
	@sids dbo.Int64List readonly
)
RETURNS TABLE 
AS
RETURN 
	select
		--- Taggable object ID.
		tt.Issuer as targetId,

		--- Grouping.
		t.contextId, t.tagId, 
		
		--- Object ID and lookup object ID.
		t.issuerId as objectId,
				
		--- Bloomberg IDs
		bgid.code as BloombergGlobalId, buid.code as BloombergUniqueId, blc.code as BloombergCodeLong,

		--- The name of the fields.
		flds.field as Field
	from
		(
			select
				--- Ordinal.
				row_number() over (partition by ic.contextId, ic.tagId, i.issuerId order by
					case when ic.issuerId != 0 then 1 else 0 end) as _Ordinal,

				--- Id.
				ic.issuerValueSourceConfigurationId,
				
				--- Grouping.
				ic.contextId, ic.tagId, i.issuerId,
				
				--- Bloomberg configuration.
				bic.bloombergFieldId							
			from
				[dbo].[Security_GetSecurityHierarchy](@sids) as soi
					inner join [dbo].[Security] as s with (nolock) on
						s.sid = soi.sid
					cross apply (
						select
							s.issuerId
						where
							s.issuerId != 0
						union
						select
							i.ultimateParentId
						from
							[dbo].[Issuer] as i with (nolock)
						where
							s.issuerId != 0 and
							i.issuerId = s.issuerId and
							i.ultimateParentId != i.issuerId
					) as i
					inner join [tagging].[IssuerValueSourceConfiguration] as ic on
						(ic.issuerId != 0 and ic.issuerId = i.issuerId) or
						(ic.issuerId = 0)
					inner join tagging.Tag as t on
						t.tagId = ic.tagId
					inner join tagging.BloombergIssuerValueSourceConfiguration as bic on
						bic.issuerValueSourceConfigurationId = ic.issuerValueSourceConfigurationId
					cross join [tagging].[ValueSource_Transposed] as vs
			where
				t.valueSourceId = vs.[Bloomberg SAPI]
		) as t
			--- Cross join target.
			cross join Target_Transposed as tt

			--- Field.
			inner join bbg.BloombergField as flds on
				flds.bloombergFieldId = t.bloombergFieldId

			--- Join on the issuer fundamental securities.
			inner join [dbo].[Issuer] as ifs with (nolock) on
				ifs.issuerId = t.issuerId

			--- Identifier types.
			cross join [dbo].[IdentifierType_Transposed] as it

			--- Get the actual ids.
			left outer join [dbo].[SecurityIdentifier] as bgid with (nolock) on
				bgid.sid = ifs.fundamentalSid and
				bgid.identifierTypeId = it.[Bloomberg Global]
			left outer join [dbo].[SecurityIdentifier] as buid with (nolock) on
				buid.sid = ifs.fundamentalSid and
				buid.identifierTypeId = it.[Bloomberg Unique]
			left outer join [dbo].[SecurityIdentifier] as blc with (nolock) on
				blc.sid = ifs.fundamentalSid and
				blc.identifierTypeId = it.[Bloomberg Code - long]
	where
		t._Ordinal = 1 and
		coalesce(bgid.code, buid.code, blc.code) is not null

