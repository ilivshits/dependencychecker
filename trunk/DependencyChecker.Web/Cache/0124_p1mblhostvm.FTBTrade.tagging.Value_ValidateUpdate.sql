

CREATE PROCEDURE [tagging].[Value_ValidateUpdate]
	@rowcount int,
	@valueId int,
	@version int,
	@overrideFlag bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--- Select the errors.
	select
		~cast ((
			select
				count(v.valueId)
			from
				tagging.Value as v with (nolock)
			where
				v.valueId = @valueId
		) as bit) as InvalidValueId,
		~cast ((
			select
				count(v.valueId)
			from
				tagging.Value as v with (nolock)
			where
				v.valueId = @valueId and
				v.version = @version
		) as bit) as VersionMismatch,
		~cast((
			select
				count(v.valueId)
			from
				tagging.Value as v with (nolock)
			where
				v.valueId = @valueId and
				v.version = @version and
				(
					v.overrideFlag = 0 or
					(v.overrideFlag != 0 and @overrideFlag != 0)
				)
		) as bit) as CannotOverride
	where
		@rowcount = 0
END




