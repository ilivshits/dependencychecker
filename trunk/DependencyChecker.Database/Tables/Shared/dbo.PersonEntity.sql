﻿CREATE TABLE [dbo].[PersonEntity]
(
	[Id]		UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[Name]       NVARCHAR (500) NOT NULL,
	[FullName]       NVARCHAR (500) NULL,
    [Created]     DATETIME2       NULL,
    [LastModified] DATETIME2       NULL, 
	[InternalId] NVARCHAR(100) NULL,
)
