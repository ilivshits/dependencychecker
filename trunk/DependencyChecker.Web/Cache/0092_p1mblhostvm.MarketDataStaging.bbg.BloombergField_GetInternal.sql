

CREATE FUNCTION [bbg].[BloombergField_GetInternal]
(	
	@bloombergFields bbg.BloombergFieldTableType readonly
)
RETURNS TABLE 
AS
RETURN 
(
	--- Just join on the incoming table.
	select distinct
		f.*
	from
		bbg.BloombergField as f with (nolock)
			inner join @bloombergFields as p on 
				(p.BloombergFieldId is not null and p.Field is not null and p.BloombergFieldId = f.BloombergFieldId and p.Field = f.Field) or
				(p.BloombergFieldId is not null and p.Field is null and p.BloombergFieldId = f.BloombergFieldId) or
				(p.Field is not null and p.BloombergFieldId is null and p.Field = f.Field)
)



