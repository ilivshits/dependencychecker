﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DependencyChecker.DataProvider.Helpers;
using DependencyChecker.Web.Authorization;
using System.Data.Entity;
using DependencyChecker.Common;

namespace DependencyChecker.Web.Controllers
{
    //[AuthorizeRoles("Administrator")]
    public class ControlPanelController : Controller
    {
        //
        // GET: /ControlPanel/

        public ActionResult SPA()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Users()
        {
            var users = UserManager.GetAllUsers();

            return View(users);
        }

        
        public ActionResult ExceptionLog()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetAllUsers()
        {
            return Json(UserManager.GetAllUsers(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult CheckUserExistence(string username)
        {
            return Json(UserManager.CheckUserExistence(username), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void AddNewAdministrator(string username)
        {
            if (!UserManager.CheckUserExistence(username))
            {
                return;
            }
            else
            {
                UserManager.AddAdministrator(username);
            }
        }

        [HttpPost]
        public void RemoveUser(string username)
        {

            UserManager.Remove(username);
        }

        [HttpGet]
        public JsonResult GetBatches(string from = null, string to = null)
        {
            var client = new DependencyCheckerServiceProxy();
            List<Common.Batch> ents = null;
            if (from == null || to == null)
            {
                ents = client.GetBatches();
            }
            else
            {
                try
                {
                    var fromDate = DateTime.Parse(from);
                    var toDate = DateTime.Parse(to);

                    ents = client.GetBatches(fromDate, toDate);
                }
                catch (Exception e)
                {
                    LoggerHelper.Log.Error("Couldn't parse date. Provide ISO format, please.", e);
                    throw e;
                }
                
            }
            

            return Json(new { result = ents.OrderBy(e => e.Date).ToList() }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        
        public JsonResult GetBatchExceptions(int batchId)
        {
            var client = new DependencyCheckerServiceProxy();
            var ents = client.GetBatchExceptions(batchId);

            return Json(new { result = ents.OrderBy(e => e.Level).ThenBy(e => e.Date).ToList() }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult Restart()
        {
            var client = new DependencyCheckerServiceProxy();
            client.RestartService();
            return RedirectToAction("Index", "Main");
        }
    }
}
