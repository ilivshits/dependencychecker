CREATE procedure [dbo].[GetNextSequence]
(
	@sequenceName varchar(100),
	@count int = 1
)
as
begin
	
	set nocount on
	
	declare @nextValue bigint
	update SequenceGenerator
	set @nextValue = nextValue,
	nextValue = nextValue + increment * @count
	where sequenceName = @sequenceName
	
	return @nextValue
	
end


