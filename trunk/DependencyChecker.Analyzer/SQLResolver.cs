﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using DependencyChecker.Analyzer.Interfaces;
using DependencyChecker.Common;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Analyzer
{
    public class SQLResolver : IResolver
    {
        #region Implementation of IResolver

        public string ProviderName
        {
            get
            {
                return DatabaseEntity.ProviderName;
            }
        }

        public bool TryResolve(EntityMatch textMatch, List<IEntity> matches, IEntity entity, SearchContent content, out bool added)
        {
            const string dynamicQueryServer = @"(\WAT[ \t\r\n]*|OPENQUERY[ \t\r\n]*\([ \t\r\n]*)(?<server>(\[[a-z0-9]+\]|[a-z0-9]+))\W";
            var servers = matches.Select(m => m.Root).ToList();

            var dynamicQueries = Regex.Matches(
                Regex.Replace(content.Text.Replace("''", ""), @"('[ ]?\+[^+]+\+[ ]?'|\-\-.*\r\n)",
                    System.String.Empty), @"([""'])(?:\\\1|.)*?\1", RegexOptions.IgnoreCase | RegexOptions.Singleline);

            var resolved = false;
            added = false;
            var dynamics = dynamicQueries.Cast<Match>().Where(q => q.Value.IndexOf(textMatch.MatchText, StringComparison.InvariantCultureIgnoreCase) >= 0).ToList();
            if (dynamics.Count == 0)
            {
                var local = GetLocal(entity, matches, textMatch.MatchText, content);
                if (local == null)
                {
                    return false;
                }
                added = entity.AddDependency(local, textMatch);
                resolved = true;
            }
            else
            {
                foreach (var query in dynamics)
                {
                    if (query.Value.IndexOf(textMatch.MatchText, StringComparison.InvariantCultureIgnoreCase) >= 0)
                    {
                        var serverMatch = Regex.Match(query.Value, dynamicQueryServer,
                            RegexOptions.IgnoreCase | RegexOptions.Singleline);
                        if (!serverMatch.Success)
                        {
                            var start = content.Text.Substring(0, query.Index).LastIndexOf('\n');
                            var end = (dynamics.Last() == query
                                ? content.Text.Length
                                : dynamics[dynamics.IndexOf(query) + 1].Index);

                            var str = content.Text.Substring(start, end - start);
                            serverMatch = Regex.Match(str, dynamicQueryServer,
                                RegexOptions.IgnoreCase | RegexOptions.Singleline);
                        }

                        if (serverMatch.Success)
                        {
                            var server = serverMatch.Groups["server"].Value.Replace("[", String.Empty)
                                .Replace("]", String.Empty);
                            var execServer = entity.Root as DatabaseEntity;
                            if (execServer != null)
                            {
                                var linked =
                                    execServer.ScopeChildren.FirstOrDefault(c => c.Type == EntityType.LinkedServer &&
                                                                                  c.Name.Equals(server,
                                                                                      StringComparison
                                                                                          .InvariantCultureIgnoreCase));
                                if (linked != null)
                                {
                                    if (linked.DependsOn.Count != 1)
                                    {
                                        linked.AddErrorFormat("Parsing {2} in {3}: Linked server {0} has {1} dependencies: {2}",
                                            linked.Name, linked.DependsOn.Count,
                                            String.Join(", ", linked.DependsOn.Select(d => d.Name)),
                                            textMatch, entity.FullName);
                                        resolved = false;
                                    }
                                    else
                                    {
                                        var match = matches.Where(m => m.Root.Id == linked.DependsOn.Single().Id).ToList();
                                        if (match.Any())
                                        {
                                            added = entity.AddDependency(match.First(), textMatch);
                                            resolved = true;
                                        }
                                        else
                                        {
                                            resolved = false;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(server))
                                    {
                                        execServer.AddErrorFormat(
                                            "Parsing {2} in {3}: Cannot locate linked server {0} on server {1}",
                                            server, execServer.Name,
                                            textMatch, entity.FullName);
                                    }
                                    else
                                    {
                                        entity.AddErrorFormat(
                                            "Cannot parse dynamic content {0} in {1}. Looking for dependency {2}",
                                             query.Value, entity.FullName, textMatch);
                                    }
                                    resolved = false;
                                }
                            }
                            else
                            {
                                var matchedRoots = matches.Where(m => m.Root.SearchNames.Contains(server)).ToList();
                                if (matchedRoots.Count > 1)
                                {
                                    var local = GetLocal(entity, matchedRoots, textMatch.MatchText, content);
                                    if (local == null)
                                    {
                                        return false;
                                    }
                                    added = entity.AddDependency(local, textMatch);
                                    resolved = true;
                                }
                                else if (matchedRoots.Count == 1)
                                {
                                    added = entity.AddDependency(matchedRoots.Single(), textMatch);
                                    resolved = true;
                                }
                            }
                        }
                        else
                        {
                            var local = GetLocal(entity, matches, textMatch.MatchText, content);
                            if (local == null)
                            {
                                return false;
                            }
                            added = entity.AddDependency(local, textMatch);
                            resolved = true;
                        }
                    }
                }
                if (!resolved)
                {
                    var local = GetLocal(entity, matches, textMatch.MatchText, content);
                    if (local == null)
                    {
                        return false;
                    }
                    added = entity.AddDependency(local, textMatch);
                    resolved = true;
                }
            }
            return resolved;
        }

        private IEntity GetLocal(IEntity entity, IEnumerable<IEntity> dependencies, string match, SearchContent content)
        {
            var scope = content.Scope;

            var entities = dependencies as List<IEntity> ?? dependencies.ToList();
            if (entity.DataProvider == ProviderName)
            {
                scope = entity;
            }

            if (scope != null && scope.Type == EntityType.DataSource)
            {
                if (scope.DependsOn.Count != 1)
                {
                    scope.AddErrorFormat("Datasource '{0}' is not detected properly. Dependencies found: {1}", scope.FullName,
                        String.Join("; ", scope.DependsOn.Select(d => d.FullName)));
                }
                else
                {
                    scope = scope.DependsOn.Single();
                }
            }

            IEntity local = null;
            if (scope != null && (scope.DataProvider == ProviderName))
            {
                var localMatches = (((DatabaseEntity)scope).Database == null) ? new List<DatabaseEntity>() :
                    entities
                    .OfType<DatabaseEntity>()
                    .Where(m => m.Database != null && m.Database.Id == ((DatabaseEntity)scope).Database.Id).ToList();

                if (((DatabaseEntity)scope).Database == null)
                {
                    LoggerHelper.Log.DebugFormat("Cannot detect databse for search scope {0}. Parsing element: {1} Search text: {2}",
                        scope.FullName,
                        entity.FullName,
                        match);
                }

                if (localMatches.Count == 1)
                {
                    local = localMatches.Single();
                }
                else if (localMatches.Count == 0)
                {
                    localMatches = entities
                        .OfType<DatabaseEntity>()
                        .Where(m => m.Root.Id == ((DatabaseEntity)scope).Root.Id).ToList();

                    if (localMatches.Count == 1)
                    {
                        local = localMatches.Single();
                    }
                    else
                    {
                        //check case for matched items
                        local = ExtraMatch(entity, localMatches, match, content);
                        if (local == null)
                        {
                            entity.AddErrorFormat(EntityErrorCode.DependencyWarning,
                                "Cannot detect local dependency for {2} in {0}. Possible matches: {1}",
                                entity.FullName,
                                String.Join("; ", entities.Select(m => m.FullName + " (" + m.TypeName + ")")), match);
                        }
                    }
                }
                else
                {
                    //find posible closest matched item
                    local = ExtraMatch(entity, localMatches, match, content);
                    if (local == null)
                    {
                        entity.AddErrorFormat(EntityErrorCode.DependencyError,
                            "Cannot detect local dependency for {2} in {0}. Matches: {1}",
                            entity.FullName,
                            String.Join("; ", localMatches.Select(m => m.FullName + " (" + m.TypeName + ")")), match);
                    }
                }
            }
            if (local == null && match.ToCharArray().Count(c => c == '.') >= 2)
            {
                local = entities.SingleOrDefault(m => m.Root.Id == entity.Root.Id);
            }
            return local;
        }

        private static IEntity ExtraMatch(IEntity entity, List<DatabaseEntity> localMatches, string match, SearchContent content)
        {
            if (localMatches.Count == 1)
            {
                return localMatches[0];
            }
            if (match.Split(new[] {'.'}, StringSplitOptions.RemoveEmptyEntries).Length != 1)
            {
                return null;
            }
            var dboMatches = localMatches.Where(m => m.Scope != null && m.Scope.Type == EntityType.Schema && m.Scope.Name == "dbo").ToList();
            return dboMatches.Count == 1 ? dboMatches.Single() : null;
        }
        #endregion
    }
}