﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Principal;
using System.ServiceModel;
using AutoMapper;
using DependencyChecker.Common;
using DependencyChecker.DataProvider;
using DependencyChecker.DataProvider.DAL;
using DependencyChecker.DataProvider.DAL.Repositories;
using DependencyChecker.DataProvider.Extensions;
using DependencyChecker.DataProvider.Helpers;
using DependencyChecker.Entities.Interfaces;
using DependencyChecker.Tests.Proxy;
using DependencyChecker.WCF;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EntityType = DependencyChecker.DataProvider.EntityType;

namespace DependencyChecker.Tests
{
    [TestClass]
    public class DataProviderTests
    {
        private BulkInsertManager<DatabaseEntity> _dbManager;
        private BulkInsertManager<Dependency> _depManager;
        private DependencyCheckerEntities _context;

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public Dictionary<Guid, Entities.DatabaseEntity> Roots { get; set; }

        public Entities.DatabaseEntity GetRoot(Guid? id)
        {
            if (id == null) return null;
            if (Roots.Any(e => e.Key == id.Value)) return Roots[id.Value];

            var dbSet = _context.Set<DatabaseEntity>();

            var root = dbSet.Single(e => e.Id == id);
            var rootEnt = new Entities.DatabaseEntity(root.Name, root.Definition,
                (Entities.EntityType) root.Type)
            {
                Created = root.Created.HasValue ? root.Created.Value : DateTime.MinValue,
                LastModified = root.LastModified.HasValue ? root.LastModified.Value : DateTime.MinValue,
                InternalId = root.InternalId
            };
            Roots.Add(id.Value, rootEnt);
            return rootEnt;
        }

        [TestInitialize]
        public void InitTest()
        {
            _context = DatabaseHelpers.GetContext();
            _dbManager = new BulkInsertManager<DatabaseEntity>(_context);
            _depManager = new BulkInsertManager<Dependency>(_context);
            //DatabaseHelpers.InitializeMapper();
        }

        [TestCleanup]
        public void FinalizeTest()
        {
            _context.Dispose();
        }

        [TestMethod]
        public void FakeFillDatabaseTest()
        {
            List<DatabaseEntity> dbEntities = new List<DatabaseEntity>();
            Random rnd = new Random();

            DatabaseEntity root = new DatabaseEntity
            {
                Id = Guid.NewGuid(),
                Name = "Root",
                Type = 2,
                InternalId = Guid.NewGuid().ToString(),
                Provider = "SQL"
                
            };
            dbEntities.Add(root);

            for (int i = 0; i < 99999; i++)
            {
                dbEntities.Add(new DatabaseEntity()
                {
                    Id = Guid.NewGuid(),
                    Name = RandomString(8),
                    FullName = RandomString(8),
                    Root = root.Id,
                    Definition = "DEFINITION",
                    Type = rnd.Next(5,8),
                    Provider = "SQL"
                });
            }
            Stopwatch sw = new Stopwatch();
            sw.Start();
            _dbManager.InsertAll(dbEntities);
            sw.Stop();
            Trace.WriteLine(sw.Elapsed);

            //Dependencies
            List<Dependency> depEntities = new List<Dependency>();
            foreach (var db in dbEntities)
            {
                for (int i = 0; i < 10; i++)
                {
                    depEntities.Add(new Dependency()
                    {
                        DependsOnId = dbEntities[rnd.Next(1, 100000)].Id,
                        SourceId = db.Id
                    });
                    
                }
            }

            sw = new Stopwatch();
            sw.Start();
            _depManager.InsertAll(depEntities);
            sw.Stop();
            Trace.WriteLine(sw.Elapsed);
        }

        [TestMethod]
        public void PerfomanceTrackingTest()
        {
            var dbSet = _context.Set<DatabaseEntity>();
            var depSet = _context.Set<Dependency>();

            Guid entityId = Guid.Parse("BAC8CB05-5BD8-456A-BCFB-087F29440200");

            var entity = dbSet.Single(e => e.Id == entityId);

            IEntity entityTreeRoot = new Entities.DatabaseEntity(entity.Name, (Entities.EntityType)entity.Type);

            var dependsOnIds = depSet.Where(e => e.SourceId == entityId).Select(e => e.DependsOnId).Cast<Guid>();
            var usedByIds = depSet.Where(e => e.DependsOnId == entityId).Select(e => e.SourceId).Cast<Guid>();
            

            var dependsOnEnts = dbSet.Where(e=>dependsOnIds.Contains(e.Id)).ToList().Select(e =>
            {
                return new Entities.DatabaseEntity(e.Name, e.Definition, (Entities.EntityType) e.Type)
                {
                    Created = e.Created.HasValue ? e.Created.Value : DateTime.MinValue,
                    LastModified = e.LastModified.HasValue ? e.LastModified.Value : DateTime.MinValue,
                    InternalId = e.InternalId,
                    RootId = e.Root.HasValue ? e.Root.Value : Guid.Empty,
                    ParentId = e.Parent.HasValue ? e.Parent.Value : Guid.Empty,
                };
            }).ToList();

            var usedByEnts = dbSet.Where(e => usedByIds.Contains(e.Id)).ToList().Select(e =>
            {
                return new Entities.DatabaseEntity(e.Name, e.Definition, (Entities.EntityType) e.Type)
                {
                    Created = e.Created.HasValue ? e.Created.Value : DateTime.MinValue,
                    LastModified = e.LastModified.HasValue ? e.LastModified.Value : DateTime.MinValue,
                    InternalId = e.InternalId,
                    RootId = e.Root.HasValue ? e.Root.Value : Guid.Empty,
                    ParentId = e.Parent.HasValue ? e.Parent.Value : Guid.Empty,
                };
            }).ToList();

            entityTreeRoot.DependsOn = dependsOnEnts.Cast<IEntity>().ToList();
            entityTreeRoot.UsedBy = usedByEnts.Cast<IEntity>().ToList();
        }

        

        [TestMethod]
        public void GetConfigurationTest()
        {
            ProviderItemRepository pItemRepository = new ProviderItemRepository(DatabaseHelpers.GetContext());
            var _configuration = pItemRepository.GetConfiguration();
        }

        [TestMethod]
        public void GetEfDefinitionsTest()
        {
            var mappings = _dbManager.Mappings;
            var metadata = DatabaseHelpers.GetMetadata(_context);
            var sc = DatabaseHelpers.GetSContainer(_context);
            var cc = DatabaseHelpers.GetCContainer(_context);
            var oic = DatabaseHelpers.GetObjectItemCollection(_context);

            var set = DatabaseHelpers.GetSContainer(_context).EntitySets.GetValue(typeof(DatabaseEntity).Name, true);
            var mapping = metadata.GetItems<System.Data.Entity.Core.Metadata.Edm.EntityType>(DataSpace.SSpace).Single(e=> (e).Name == typeof(DatabaseEntity).Name); //.GetItems<EntityContainerMapping>(DataSpace.OSpace);
            var h = sc.EntitySets.Single(e => e.Name == "DatabaseEntity");

        }
       
        [TestMethod]
        public void InsertAllTest()
        {
            List<DatabaseEntity> deps = new List<DatabaseEntity>()
            {
                new DatabaseEntity()
                {
                    Name = "123",
                    FullName = "123",
                    Id = Guid.NewGuid(),
                    Definition = "",
                    JobLastRunTime = DateTime.MinValue,
                    Provider = "SQL",
                    Type = 0,
                }
            };
            _dbManager.InsertAll(deps);
        }

        [TestMethod]
        public void TruncateTest()
        {
            _context.spDropConstraints();

            _context.Set<DataProvider.EntityError>().Truncate();
            _context.Set<DataProvider.ReportEntity>().Truncate();
            _context.Set<DataProvider.Dependency>().Truncate();
            _context.Set<DataProvider.DatabaseEntity>().Truncate();
            _context.Set<DataProvider.ScheduledTaskEntity>().Truncate();
            _context.Set<DataProvider.EntityBase>().Truncate();
            _context.Set<DataProvider.PersonEntity>().Truncate();
            _context.Set<DataProvider.PerlScriptEntity>().Truncate();

            _context.spRecreateConstraints();
        }

        [TestMethod]
        public void GetDetailsTest()
        {

            var e = DatabaseHelpers.GetEntityDetailsById(Guid.Parse("98733d7e-7111-4e4f-8d56-8386a24e88fb"));
        }

        [TestMethod]
        public void AutomapperTest()
        {
            DatabaseEntity entity = new DataProvider.DatabaseEntity()
            {
                Created = null,
                LastModified = null,
                CreatedBy = null,
                LastModifiedBy = null,
                Database = "dasf",
                Type = 1,
                JobLastRunTime = DateTime.MaxValue,
                FullName = "name",
                Id = Guid.NewGuid(),

            };

            var e = MapperManager<Entity>.Map(entity);

            /*IEntity entity = new Entities.DatabaseEntity("Name", "DEF", Entities.EntityType.Database) {JobLastRunTime = DateTime.Now};
            entity.Created = DateTime.Now;
            var e = Mapper.Map<DatabaseEntity>(entity); */
        }

        [TestMethod]
        public void RegexQueryTest()
        {
            var s = _context.EntityBase.RegexSearch("FullName", "dbo", _context);

            //unit.PerlScriptEntityRepository.DeleteAll();
            //_context.EntityBase.Where(s => _context.Like());

        }

        [TestMethod]
        public void GetDependsOnRec()
        {
           //var s =  DatabaseHelpers.GetDependsOnRecursively(Guid.Parse("59b54d39-4e11-44ec-9a2a-5d74c8966b4b"));
            //var ss = DatabaseHelpers.GetDependsOn(Guid.Parse("d024bc2f-dbf1-4428-9e79-1dbba1b16935"));
            var t = DatabaseHelpers.GetUsedBy(Guid.Parse("59b54d39-4e11-44ec-9a2a-5d74c8966b4b"));
            string s = DatabaseHelpers.DependenciesToString(t.Keys.ToList());
        }

        [TestMethod]
        public void GetDatavasesTest()
        {
            var s = DatabaseHelpers.GetDatabases();
        }

        [TestMethod]
        public void BatchInsertTest()
        {
            using (var ctx = DatabaseHelpers.GetContext())
            {
                ctx.Batch.Add(new DataProvider.Batch() { date = DateTime.Now, username = WindowsIdentity.GetCurrent()?.Name });
                try
                {
                    ctx.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    int x = 5;
                }
                //_batchId = DatabaseHelpers.GetCurrentBatchId();
            }
        }

        [TestMethod]
        public void UpdateBatchStatusTest()
        {
            
        }
    }
}
