

create view [tagging].[Tag_TransposedColumns] as
	--- The transposed columns.
	select
		t.targetId, t.tagId, t.columnName, t.sqlDataType, t.displayValue
	from
		(
			--- Get all of the columns.
			select
				t.targetId,
				t.tagId,
				'_' + t.description as columnName,
				case tt.baseTypeId
					when bt.Bool then 'bit'
					when bt.Text then 'varchar(max)'
					when bt.Date then 'date'
					when bt.DateTime then 'datetime'
					when bt.Decimal then 'decimal (38, 14)'
					when bt.Integer then 'int'
					when bt.UserList then 'int'
					when bt.SystemList then pkt.name
					when bt.Time then 'time'
					when bt.XML then 'xml'
				end	as sqlDataType,
				0 as displayValue
			from
				tagging.Tag as t with (nolock)
					inner join tagging.Type as tt with (nolock) on
						tt.typeId = t.typeId
					cross join tagging.v_BaseType_Pivot as bt
					left outer join tagging.SystemList as sl with (nolock) on
						sl.typeId = t.typeId
					left outer join sys.tables as st with (nolock) on
						st.name = sl.tableName
					left outer join sys.schemas as ss with (nolock) on
						ss.schema_id = st.schema_id and
						ss.name = 'dbo'
					left outer join sys.columns as pk with (nolock) on
						pk.object_id = object_id(sl.tableName) and
						pk.name = sl.primaryIdColumn
					left outer join sys.types as pkt with (nolock) on
						pkt.user_type_id = pk.user_type_id
			union
			--- User defined and system lists.
			select
				t.targetId,
				t.tagId,
				'_' + t.description + '_displayValue' as columnName,
				'varchar(max)' as sqlDataType,
				1 as displayValue
			from
				tagging.Tag as t with (nolock)
					inner join tagging.Type as tt with (nolock) on
						tt.typeId = t.typeId
					cross join tagging.v_BaseType_Pivot as bt
			where
				tt.baseTypeId in (bt.UserList, bt.SystemList)
		) as t







