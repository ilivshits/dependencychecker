CREATE VIEW [grid].[ViewType_Transposed] AS
SELECT
[Portfolio] = 1
, [Financing Tool] = 2
, [Securities] = 3
, [Orders] = 4
, [Configuration] = 5
, [STP] = 6
, [Permissions] = 7
, [P1Live, Trader Mode, Strategy Level] = 101
, [P1Live, Trader Mode, Security Level] = 102
, [P1Live, TA Mode, Strategy Level] = 103
, [P1Live, TA Mode, Security Level] = 104


