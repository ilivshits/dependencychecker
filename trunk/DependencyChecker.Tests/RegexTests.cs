﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DependencyChecker.Tests
{
    [TestClass]
    public class RegexTests
    {
        private static Regex comments = new Regex(@"\/\*(.*?)\*\/", RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex onelineComments = new Regex(@"\-\-.*\r\n", RegexOptions.Compiled);
        private static Regex allComments = new Regex("\\-\\-[^\\n]*\\n", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private static Regex textPicker = new Regex(@"('[ ]?\+[^+]+\+[ ]?'|\-\-.*\r\n)", RegexOptions.Compiled);
        private static Regex quotesPicker = new Regex(@"([""'])(?:\\\1|.)*?\1", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex expressionQuotesPicker = new Regex(@"(as|=)+\s*([""'])(\\?.)*?\2|(?<=([\s\t\n\r]|^)values\s*\(.*)\'[\w\s]+\'(?=.*\))", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex xmlValuesParamsPicker = new Regex(@"([\s\t\n\r]|^)[\w\.]+\.value\(.*?\)[,\r\n\t]", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex casesPicker = new Regex(@"(?<=^|\s)CASE\s.*?\sEND(?=$|\s)", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex sqlNativePicker = new Regex(@"(?<=declare\s+).*?(?=\s+.*?(\[dbo\]\.\[fnGetSqlNativeDllDatabaseName\]))", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex linkedSelfPicker = new Regex(@"\@\S+(?=.*[\s\t\r]+('SELF'))", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);

        [TestMethod]
        public void DeclareFunctionsTest()
        {
            var commentsRemoved = comments.Replace(onelineComments.Replace(RegexTestsStrings.DeclaredFuncsString, String.Empty).Replace("''", "")
                                                                                                    .Replace("**", "*")
                                                                                                    .Replace("/**", "/*")
                                                                                                    .Replace("**/", "*/"), String.Empty);

            commentsRemoved = allComments.Replace(commentsRemoved, String.Empty);

            var declares = sqlNativePicker.Matches(commentsRemoved);
            foreach (Match declare in declares)
            {
                commentsRemoved = Regex.Replace(commentsRemoved, @"\'\s*\+\s*\" + declare.Value + @"\s*\+\s*\'", String.Empty);
            }
        }

        [TestMethod]
        public void LinkedSrvTest()
        {
            var commentsRemoved = comments.Replace(onelineComments.Replace(RegexTestsStrings.LinkedSrvString, String.Empty).Replace("''", "")
                                                                                                    .Replace("**", "*")
                                                                                                    .Replace("/**", "/*")
                                                                                                    .Replace("**/", "*/"), String.Empty);

            commentsRemoved = allComments.Replace(commentsRemoved, String.Empty);

            var linkeds = linkedSelfPicker.Matches(commentsRemoved);
            foreach (Match linkedSrvVar in linkeds)
            {
                commentsRemoved = Regex.Replace(commentsRemoved, @"\'\s*\+\s*\" + linkedSrvVar.Value + @"\s*\+\s*\'",
                    "SELF");
            }
        }
    }
}
