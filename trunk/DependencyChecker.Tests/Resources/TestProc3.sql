﻿
-- =============================================
-- Author:		fortress\aboridko,Alexander Boridko
-- Edited by:	
-- Create date: 7/19/2013
-- Description:	 sp that return RiskEntities list for 'Imagine/FrontOffice/PnlTrendReport report and other.
-- Status:     
-- =============================================
CREATE PROCEDURE [risk].[spGetPnlTrendReportData]
	@asOfDate date, 
	@riskEntityName varchar(max)
AS
BEGIN
	declare @debug bit = 0
	---Remove comment this section to test the procedure---
	--declare @asOfDate  date, @riskEntityName  varchar(max)
	--set @asOfDate = '8/28/2013'
	--set @riskEntityName = 'Adam'
	--set @debug = 1
	---END TEST PARAMETER SECTION-----

	declare @dateYear				date = Dateadd(year, Datediff(year, 0, @asOfDate), 0)
	declare @dateMonth				date = Dateadd(month, Datediff(month, 0, @asOfDate), 0)
	declare @dateWeek				date = Dateadd(week, Datediff(week, 0, @asOfDate), 0)
	declare @dateWinnerDaysStart	date = Dateadd(day,-31, @asOfDate)
	declare @dateWinnerDaysEnd		date = Dateadd(day,-1, @asOfDate)
	declare @date5Days				date = Dateadd(day,-5, @asOfDate)
	declare @datePrev				date = @asOfDate
	declare @dateToday				date = @asOfDate
	declare @dateYearPlus5			date = Dateadd(day,-5,@dateYear)

	print '#0: ' + convert(varchar,getdate(),21)

		
	IF OBJECT_ID('tempdb..#re') > 0					DROP TABLE #re
	IF OBJECT_ID('tempdb..#rawPnL') > 0				DROP TABLE #rawPnL	
	IF OBJECT_ID('tempdb..#finallResult') > 0		DROP TABLE #finallResult	
	IF OBJECT_ID('tempdb..#finallResultWinner') > 0	DROP TABLE #finallResultWinner	
	IF OBJECT_ID('tempdb..#top5Dates') > 0			DROP TABLE #top5Dates
	

	
	--if(@debug = 1 )  select  dateYear = @dateYear, dateMonth = @dateMonth, dateWeek = @dateWeek,date5Days = @date5Days,dateToday = @dateToday

	CREATE TABLE #finallResult(
		[category]		int, --One of: 1- Year, 2-Month, 3-Last 5 Days, 4-Today, 33 - last 5 Days extended. 6 - distinct trader/fund pairs.
		[date]			date,
		[strategy]		varchar(max),	
		[pnl]			float,
		[totalPnL]		float,
		[streak]		int,
		[fund]			varchar(max) null,
		[trader]		varchar(max) null,
		[sign]			int,
		[isFirstDay]	bit,
		
	)	

	select top(0) * into #finallResultWinner from #finallResult

	-----------------------------------
	-- Raw PnL Loading without filtering by Risk Entity (because filter take long time)
	-----------------------------------
	create table #rawPnL(		
		[FundId]				int,
		[fund]					varchar(100) COLLATE   Latin1_General_BIN2  NULL   , 
		[aexeo_trader_id]		int NULL,
		[trader]				varchar(max),
		[strategy]				char(50) NULL,
		[pnlDate]				char(30) NULL,
		[date]					date NULL,
		[custom_pnl]			float NULL,		
		isSelectedRiskEntity	bit,
		[sign]					int,

	)

	select 
		AexeoTraderId	= Convert(varchar(max),ft.[AexeoTraderId]) ,
		FundId			= Convert(varchar(max),ft.[FundId]) 
	into #re
	from [RiskDataHist].[dbo].[v_RiskEntity] ft 
	where ft.[Active] = 1 and ft.Name = @riskEntityName

	declare @qry varchar(max);

	------------------------------
	-- Year
	------------------------------	
	set @qry = '			
	;WITH rawPnL_CTE1( fund,FundId, aexeo_trader_id, trader, strategy,  custom_pnl) 
	AS
	(
		select * from openquery(CUBE1_OLAP, 
		''    
		select 
			non empty(	
	  			 [Legal Entity].[Legal Entity].[Legal Entity]
				*[Legal Entity].[Fund Id].[Fund Id]
				*[Traders].[Aexeo Trader Id].[Aexeo Trader Id]  
				*[Traders].[Aexeo Trader Name].[Aexeo Trader Name] 
				*[Strategy].[Strategy - Strategy].[Strategy - Strategy]				
			) on 1	,
			{[Measures].[Aexeo Pnl]} on 0 			
		from [Pnl Data]
		where [Time].[Hierarchy].[Date].&[' +Convert(varchar(10), @dateYear, 126)+ 'T00:00:00]:[Time].[Hierarchy].[Date].&[' +Convert(varchar(10), @asOfDate, 126)+ 'T00:00:00]
		
		'')
	)	
		
	insert into #rawPnL ( fund, trader, strategy, custom_pnl)
		select  p.fund, p.trader, p.strategy,  p.custom_pnl
		from rawPnL_CTE1 p
		join #re ft on 
								( Convert(varchar(max),p.aexeo_trader_id)	collate  Latin1_General_BIN2 = isnull(ft.[AexeoTraderId], p.aexeo_trader_id) ) 
							and ( Convert(varchar(max),p.FundId)			collate  Latin1_General_BIN2 = isnull(ft.FundId,p.FundId) ) 	
	';
	if(@debug=1) print(@qry);			
	exec (@qry);			
	--------------------------------
	------------------------------
	-- Year
	------------------------------	
	insert into #finallResult(category, [strategy], pnl, date)
		SELECT [category]		= 1     
			  ,[strategy]		= pnl.[strategy]
			  ,[pnl]			= SUM(pnl.[custom_pnl])	
			  ,[date]			= @dateYear
		FROM #rawPnL pnl			
		GROUP BY  pnl.[strategy]	
	------------------------------

	------------------------------
	--  Distinct Trader/Fund pairs 
	------------------------------	
	insert into #finallResult(category, trader,fund)
		SELECT distinct 
			   category			= 6,
			   trader			= pnl.trader,
			   fund				= pnl.fund
		FROM #rawPnL pnl		
	-------------------	

	-- CleanUp data for the Year
	delete #rawPnL

	--------------------------------------
	-- last 31 Days
	---------------------------------------
	set @qry = '			
	;WITH rawPnL_CTE2( FundId, aexeo_trader_id, strategy, dateYear,dateWeek,pnlDate,  custom_pnl) 
	AS
	(
		select * from openquery(CUBE1_OLAP, 
		''    
		select 
			non empty(		  			
				 [Legal Entity].[Fund Id].[Fund Id]
				*[Traders].[Aexeo Trader Id].[Aexeo Trader Id]  			
				*[Strategy].[Strategy - Strategy].[Strategy - Strategy]		
				*[Time].[Hierarchy].[Date].&[' +Convert(varchar(10), @dateWinnerDaysStart, 126)+ 'T00:00:00]:[Time].[Hierarchy].[Date].&[' +Convert(varchar(10), @asOfDate, 126)+ 'T00:00:00]		
			) on 1	,
			{[Measures].[Aexeo Pnl]} on 0 			
		from [Pnl Data]		
		
		'')
	)	
		
	insert into #rawPnL ( strategy, custom_pnl,date)
		select p.strategy,  p.custom_pnl, [date] = convert(date, substring(p.pnlDate, charindex('' '', p.pnlDate,0), 100) , 101)
		from rawPnL_CTE2 p
		join #re ft on 
								( Convert(varchar(max),p.aexeo_trader_id)	collate  Latin1_General_BIN2 = isnull(ft.[AexeoTraderId], p.aexeo_trader_id) ) 
							and ( Convert(varchar(max),p.FundId)			collate  Latin1_General_BIN2 = isnull(ft.FundId,p.FundId) ) 	
	';
	if(@debug=1) print(@qry);			
	exec (@qry);			
	--------------------------------
			
	------------------------------
	-- Month
	------------------------------		
	insert into #finallResult(category, [strategy], pnl, date)
		SELECT [category]		= 2     
			  ,[strategy]		= pnl.[strategy]
			  ,[pnl]			= SUM(pnl.[custom_pnl])
			  ,[date]			= @dateMonth
		FROM #rawPnL pnl		
		WHERE pnl.[date] between @dateMonth and @asOfDate 
		GROUP BY  pnl.[strategy]	
	--------------------------------

	------------------------------
	-- Last 5 Days daily based
	------------------------------	
	select distinct top 5 pnl.[date] into #top5Dates from #rawPnL pnl order by pnl.[date] desc

	insert into #finallResult(category, [strategy], pnl, date)
		SELECT [category]		= 3     
			  ,[strategy]		= null
			  ,[pnl]			= SUM(pnl.[custom_pnl])	
			  ,[date]			= pnl.[date]
		FROM #rawPnL pnl	
		JOIN #top5Dates d on pnl.date = d.date 	
		GROUP BY pnl.[date]
	
	------------------------------
	-- Last 5 Days Top and Bottom N
	------------------------------	
	insert into #finallResult(category, [strategy], pnl, date)
		SELECT [category]		= 33     
			  ,[strategy]		= pnl.[strategy]
			  ,[pnl]			= SUM(pnl.[custom_pnl])	
			  ,[date]			= @asOfDate	
		FROM #rawPnL pnl	
		JOIN #top5Dates d on pnl.date = d.date 	
		GROUP BY pnl.[strategy]		
		
	------------------------------
	--  Previous Day 
	------------------------------	
	insert into #finallResult(category, [strategy], pnl, date)
		SELECT category			= 4 
			  ,[strategy]		= pnl.[strategy]
			  ,[pnl]			= SUM(pnl.[custom_pnl])	
			  ,[date]			= @asOfDate

		FROM #rawPnL pnl	
		WHERE pnl.[date] = @asOfDate
		GROUP BY pnl.[strategy]
		
	-- Let's add dummy row if there are no rows at all. 
	if(not exists(select * from #finallResult where category = 4))	
	begin
		insert into #finallResult(category, [strategy], pnl, date)
			SELECT category			= 4 
				  ,[strategy]		= ''
				  ,[pnl]			= null
				  ,[date]			= @asOfDate
	end
	-----------------------------------------------
	
			
	------------------------------
	-- Winners/Losers
	------------------------------	
	select @dateWinnerDaysEnd = Max(pnl.[date])
	from #top5Dates pnl
	where pnl.[date] <= @asOfDate

	
	insert into #finallResultWinner(category, [strategy], pnl, [date],[sign],[isFirstDay],[streak])
		SELECT [category]		= 5  
			  ,[strategy]		= pnl.[strategy]
			  ,[pnl]			= SUM(pnl.[custom_pnl])	
			  ,[date]			= pnl.[date]			 
			  ,[sign]			= case when SUM(pnl.[custom_pnl]) > 0 then 1 else -1 end
			  ,[isFirstDay]		= case when pnl.[date] = @dateWinnerDaysEnd then 1 else 0 end	
			  ,[streak]			= case when pnl.[date] = @dateWinnerDaysEnd then 0 else null end		  	 	 	  	 	 

		FROM #rawPnL pnl
		--cross apply ( select max(p.date) as firstDay from #rawPnL p where pnl.[strategy] = p.[strategy] and (p.[date] between @dateWinnerDaysStart and @dateWinnerDaysEnd)) p	
		WHERE pnl.[date] between @dateWinnerDaysStart and @dateWinnerDaysEnd	
		GROUP BY pnl.[strategy],pnl.[date]--,p.firstDay
		having SUM(pnl.[custom_pnl])<>0
	-------------------------------
	--------------------
	-- Stale Days calculation (where pnl was change during the lasn N Days )...
	--------------------
	update p 
	set streak = abs(datediff(d,coalesce(c.date,l.date),p.date))
	from #finallResultWinner p
	outer apply 
		( 	-- the day when sign was changed
			select top 1 x.date
			from #finallResultWinner x 
			where
			      x.[strategy]=p.[strategy]
			  and x.date < p.date
			  and x.[sign] <> p.[sign] 
			order by x.date desc  
		)x 
	outer apply 
		( 	
			select top 1 c.date
			from #finallResultWinner c 
			where 
			      c.[strategy]=p.[strategy]			
			  and c.date > x.date
			  and c.[sign] = p.[sign] 
			  and x.date is not null
			order by c.date asc  
		)c 
	outer apply 
		( 	-- the last day when sing was not changed
			select top 1 l.date
			from #finallResultWinner l 
			where 
			      l.[strategy]=p.[strategy]
			  and l.date < p.date
			  and l.[sign] = p.[sign] 
			  and c.date is null
			order by l.date asc  
		)l 
	
	where p.isFirstDay = 1	
	
	--Calculate total of Streak (replace existing streak of date diff by the count of the streaks)
	update p 
	set p.streak = g.totalStreaks, 
		p.totalPnL = g.totalStreakPnl
	from #finallResultWinner p
	cross apply( -- select total PnL and count of Streaks  incide of the Streak group
					select sum(g.pnl) as totalStreakPnl,Count(*) as totalStreaks from #finallResultWinner g  
					where g.strategy = p.strategy and (g.date between dateadd(d,p.streak*(-1),p.date) and p.date) 					
				)g
	where  p.isFirstDay = 1 and p.streak > 0
	
	--CleanUp result. We do not need strategies that hasn't skeaks 
	--Comment this delete section to see all positions of streak.
	delete #finallResultWinner
	where ((isFirstDay = 0 ) or (isFirstDay = 1 and (streak = 0 or streak is null) ))
	--- End Data Loading--------------------------------------------------

	----------------------
	-- Printing Result...
	----------------------	
	SELECT 	[category], [date], [strategy], [pnl], [trader], [fund],[streak],[totalPnL],[sign],[isFirstDay]
	FROM #finallResult 
	UNION ALL
	SELECT 	[category]=5, [date], [strategy], [pnl], [trader], [fund],[streak],[totalPnL],[sign],[isFirstDay]
	FROM #finallResultWinner	  

END
