﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DependencyChecker.Entities
{
    public class OlapEntity : DatabaseEntity
    {
        public new static readonly string ProviderName = EntityProviderType.OLAP.ToString();

        public OlapEntity(string name, EntityType type)
            : this(name, String.Empty, type)
        {
        }

        public OlapEntity(string name, string definition, EntityType type)
            : base(name, definition, type, ProviderName)
        {
        }

        public override List<string> SearchNames
        {
            get
            {
                return Type == EntityType.Cube ? 
                    new List<String> { "[" + Name + "]" } : 
                    base.SearchNames.Where(n => n.EndsWith("]")).ToList();
            }
        }

    }
}