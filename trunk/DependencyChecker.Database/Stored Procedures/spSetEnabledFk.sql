﻿CREATE PROCEDURE [dbo].[spSetEnabledFk]
	@tableName nvarchar(max),
	@isEnabled bit
AS 
	IF (@isEnabled = 'FALSE')
		EXEC ('ALTER TABLE' + @tableName + 'NOCHECK CONSTRAINT ALL')
	ELSE
		EXEC ('ALTER TABLE' + @tableName + 'WITH CHECK CHECK CONSTRAINT ALL')	
RETURN 0
