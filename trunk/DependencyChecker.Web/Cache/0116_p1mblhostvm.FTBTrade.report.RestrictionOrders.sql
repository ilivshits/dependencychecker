
CREATE view [report].[RestrictionOrders] as
select [Order Id] = o.orderId, Manager = m.[description], [Security] = s.[description], [Security Type] = sp.[description],
       [Restriction Type] = 
			(stuff(
            isnull((select cast(', ' + t.[description] as varchar(max))
            from [RestrictionType] t
            join [SecurityRestriction] sr on t.restrictionTypeId = sr.restrictionTypeId
            where sr.[sid] = s.[sid]
            for xml path ('')), '')
            +
            isnull((select cast(', ' + t.[description] as varchar(max))
			from [RestrictionType] t
			join [IssuerRestriction] ir on t.restrictionTypeId = ir.restrictionTypeId
			cross apply GetIssuerWithAncestors(s.issuerId) res
			where res.childId = ir.issuerId
            for xml path ('')), '')
			, 1, 2, '')),
       [Override Officer] = oa.overrideOfficer, [Note] = oa.note,
       [Last Update] = oa.alertTime, [Last Update User] = oa.application_source
from [OrderAlert] oa
join [Order] o with(nolock) on o.orderId = oa.orderId
join [Manager] m with(nolock) on o.managerId = m.managerId
join [Security] s with(nolock) on o.[sid] = s.[sid]
join [SecurityPrimaryType] sp with(nolock) on s.securityPrimaryTypeId = sp.securityPrimaryTypeId
where o.orderStatusId = 2


