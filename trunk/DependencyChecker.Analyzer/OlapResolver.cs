﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DependencyChecker.Analyzer.Interfaces;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Analyzer
{
    public class OlapResolver : IResolver
    {
        #region Implementation of IResolver

        public string ProviderName
        {
            get
            {
                return OlapEntity.ProviderName;
            }
        }

        public bool TryResolve(EntityMatch textMatch, List<IEntity> matches, IEntity entity, SearchContent content, out bool added)
        {
            foreach (var match in matches)
            {
                entity.AddDependency(match, textMatch);
            }
            added = true;
            return true;
        }

        #endregion
    }
}
