CREATE view [dbo].[CR_TransferReport] as
with Links as
(
	select r.created, fromOrderId, toOrderId
	from OpsTransferRun r
	join OpsTransferNode n on r.opsTransferRunId = n.opsTransferRunId
	join OpsTransferLink l on n.opsTransferNodeId = l.opsTransferNodeId
),
Transfers as
(
	select created, orderId = fromOrderId from Links
	union all
	select created, toOrderId from Links
)
select distinct sid, transferTime = t.created
from [Order] o with(nolock)
join Transfers t on o.orderId = t.orderId


