﻿CREATE TABLE [dbo].[EntityError]
(
	[EntityId] UNIQUEIDENTIFIER CONSTRAINT [FK_Error_EntityId] FOREIGN KEY REFERENCES [dbo].[EntityBase] ([Id]) NOT NULL, 
    [Message] NVARCHAR(MAX) NULL 
)
