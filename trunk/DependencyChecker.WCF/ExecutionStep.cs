﻿using System;
using DependencyChecker.Common;

namespace DependencyChecker.WCF
{
    internal class ExecutionStep
    {
        internal String Name { get; set; }
        internal int Weight { get; set; }
        internal DependencyCheckerServiceState StateType { get; set; }
        internal Action Action;
    }
}