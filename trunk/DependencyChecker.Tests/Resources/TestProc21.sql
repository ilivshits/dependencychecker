CREATE PROCEDURE [dbo].[proc_AddItem]
            @id         varchar(512),
            @item       varbinary(max),
            @timeout    int
        AS
            DECLARE @utcnow AS datetime
            SET @utcnow = GETUTCDATE()

            INSERT [dbo].Sessions
                (ItemId,
                 ItemData,
                 TimeoutMinutes,
                 ExpireTimeUtc,
                 Locked,
                 LockTimeUtc,
                 LockCookie)
            VALUES
                (@id,
                 @item,
                 @timeout,
                 DATEADD(minute, @timeout, @utcnow),
                 0,
                 @utcnow,
                 1)

            RETURN 0