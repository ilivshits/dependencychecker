﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyChecker.Common
{
    public class ParseError
    {
        public int BatchId { get; set; }
        public DateTime? Date { get; set; }
        public String Username { get; set; }
    }
}
