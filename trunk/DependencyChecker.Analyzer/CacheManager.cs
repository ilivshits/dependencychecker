﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DependencyChecker.Common;
using DependencyChecker.DataProvider;
using DependencyChecker.DataProvider.DAL;
using DependencyChecker.DataProvider.Extensions;
using DependencyChecker.DataProvider.Helpers;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Events;
using DependencyChecker.Entities.Interfaces;
using Batch = DependencyChecker.DataProvider.Batch;
using DatabaseEntity = DependencyChecker.Entities.DatabaseEntity;

namespace DependencyChecker.Analyzer
{
    public class CacheManager : IProgressReporter
    {
        private const int BULK_TOP_ROWS = 10000;

        private List<Guid> _excludedGuids = new List<Guid>();
        private List<KeyValuePair<Guid, Guid>> _addedDependencies = new List<KeyValuePair<Guid, Guid>>();
        private List<Guid> _excludedPersonGuids = new List<Guid>();
        private int _batchId;
        
        public event EventHandler<DependencyCheckerProgressEventArgs> Progress;

        protected void OnProgress(DependencyCheckerProgressEventArgs e)
        {
            Progress?.Invoke(this, e);
        }

        public CacheManager(int batchId)
        {
            _batchId = batchId;

            
        }

        public void CacheToDatabase(List<IEntity> servers)
        {
            try
            {
                OnProgress(new DependencyCheckerProgressEventArgs
                {
                    Message = "Prepare database",
                    Progress = 0
                });

                using (var ctx = DatabaseHelpers.GetContext())
                {
                    LoggerHelper.Log.Debug("Drop Constraints");
                    ctx.spDropConstraints();

                    LoggerHelper.Log.Debug("Truncate tables");
                    ctx.Set<DataProvider.EntityError>().Truncate();
                    ctx.Set<DataProvider.ReportEntity>().Truncate();
                    ctx.Set<DataProvider.Dependency>().Truncate();
                    ctx.Set<DataProvider.DatabaseEntity>().Truncate();
                    ctx.Set<DataProvider.ScheduledTaskEntity>().Truncate();
                    ctx.Set<DataProvider.EntityBase>().Truncate();
                    ctx.Set<DataProvider.PersonEntity>().Truncate();
                    ctx.Set<DataProvider.PerlScriptEntity>().Truncate();

                    try
                    {
                        OnProgress(new DependencyCheckerProgressEventArgs
                        {
                            Message = "Filling Entities data",
                            Progress = 0.3f
                        });

                        Stopwatch sw = new Stopwatch();
                        sw.Start();

                        LoggerHelper.Log.Info("Started filling Database with SQL Ents");
                        FillDb(ctx, servers);
                        LoggerHelper.Log.Info("Finished filling Database with SQL Ents. Elapsed: " + sw.Elapsed);

                        _excludedGuids.Clear();


                        OnProgress(new DependencyCheckerProgressEventArgs
                        {
                            Message = "Filling Dependencies data",
                            Progress = 0.7f
                        });

                        sw.Restart();

                        LoggerHelper.Log.Info("Started filling Database dependencies");
                        FillDbDependencies(ctx, servers);
                        LoggerHelper.Log.Info("Finished filling Database dependencies. Elapsed: " + sw.Elapsed);

                        sw.Stop();

                        DatabaseHelpers.SetBatchSuccessful(_batchId);
                    }
                    catch (Exception e)
                    {
                        ctx.Set<DataProvider.EntityError>().Truncate();
                        ctx.Set<DataProvider.ReportEntity>().Truncate();
                        ctx.Set<DataProvider.Dependency>().Truncate();
                        ctx.Set<DataProvider.DatabaseEntity>().Truncate();
                        ctx.Set<DataProvider.ScheduledTaskEntity>().Truncate();
                        ctx.Set<DataProvider.EntityBase>().Truncate();
                        ctx.Set<DataProvider.PersonEntity>().Truncate();
                        ctx.Set<DataProvider.PerlScriptEntity>().Truncate();

                        throw;
                    }
                    finally
                    {
                        LoggerHelper.Log.Debug("Recreate Constraints");
                        ctx.spRecreateConstraints();
                    }

                    
                }
            }
            catch (Exception e)
            {
                OnProgress(new DependencyCheckerProgressEventArgs
                {
                    Message = "Error occured while creating database cache",
                    Progress = 1
                });
                throw e;
            }
        }

        private bool TryAddEntityToDictionary<T>(Dictionary<EntityProviderType, List<EntityBase>> dbEntities, IEntity entity) where T : EntityBase
        {
            if (_excludedGuids.Contains(entity.Id))
                return false;

            EntityProviderType provider =
                (EntityProviderType)Enum.Parse(typeof(EntityProviderType), entity.DataProvider);
            if (!dbEntities.ContainsKey(provider))
            {
                dbEntities.Add(provider, new List<EntityBase>());
            }

            _excludedGuids.Add(entity.Id);
            dbEntities[provider].Add(MapperManager<T>.Map(entity));

            return true;
        }

        private void BulkInsertEntities(Dictionary<EntityProviderType, List<EntityBase>> dbEntities, DependencyCheckerEntities ctx, bool forceInsert)
        {
            foreach (var ents in dbEntities)
            {
                if (forceInsert || ents.Value.Count >= BULK_TOP_ROWS)
                {
                    switch (ents.Key)
                    {
                        case EntityProviderType.SQL:
                            ctx.Set<DataProvider.DatabaseEntity>().BulkInsert(ents.Value.Cast<DataProvider.DatabaseEntity>());
                            ents.Value.Clear();
                            break;
                        case EntityProviderType.ScheduledTask:
                            ctx.Set<DataProvider.ScheduledTaskEntity>().BulkInsert(ents.Value.Cast<DataProvider.ScheduledTaskEntity>());
                            ents.Value.Clear();
                            break;
                        case EntityProviderType.SSRS:
                            ctx.Set<DataProvider.ReportEntity>().BulkInsert(ents.Value.Cast<DataProvider.ReportEntity>());
                            ents.Value.Clear();
                            break;
                        case EntityProviderType.Perl:
                            ctx.Set<DataProvider.PerlScriptEntity>().BulkInsert(ents.Value.Cast<DataProvider.PerlScriptEntity>());
                            ents.Value.Clear();
                            break;
                    }
                }
            }
        }

        private void HandlePersons(IEntity entity, DependencyCheckerEntities ctx)
        {
            Entities.PersonEntity createdBy = null;
            Entities.PersonEntity modifiedBy = null;
            if (entity.CreatedBy != null)
            {
                createdBy = entity.CreatedBy as Entities.PersonEntity;
                if (!_excludedPersonGuids.Contains(createdBy.Id))
                {
                    ctx.PersonEntity.Add(new DataProvider.PersonEntity()
                    {
                        Id = createdBy.Id,
                        FullName = createdBy.FullName,
                        Name = createdBy.Name,
                        InternalId = createdBy.InternalId
                    });
                    _excludedPersonGuids.Add(createdBy.Id);
                    ctx.SaveChanges();
                }
            }
            if (entity.LastModifiedBy != null)
            {
                modifiedBy = entity.LastModifiedBy as Entities.PersonEntity;
                if (!_excludedPersonGuids.Contains(modifiedBy.Id))
                {
                    ctx.PersonEntity.Add(new DataProvider.PersonEntity()
                    {
                        Id = modifiedBy.Id,
                        FullName = modifiedBy.FullName,
                        Name = modifiedBy.Name,
                        InternalId = modifiedBy.InternalId
                    });
                    _excludedPersonGuids.Add(modifiedBy.Id);
                    ctx.SaveChanges();
                }
            }
        }

        private void FillDb(
            DependencyCheckerEntities ctx,
            List<IEntity> entities,
            Dictionary<EntityProviderType, List<EntityBase>> dbEntities = null,
            List<DataProvider.EntityError> entityErrors = null,
            List<DataProvider.ErrorsArchive> errorsArchive = null,
            bool isStart = true)
        {
            if (dbEntities == null)
                dbEntities = new Dictionary<EntityProviderType, List<EntityBase>>();
            if (entityErrors == null)
                entityErrors = new List<DataProvider.EntityError>();
            if (errorsArchive == null)
                errorsArchive = new List<DataProvider.ErrorsArchive>();
            if (entities != null)
                foreach (var entity in entities)
                {
                    /**
                     * Data filling
                     */

                    if (entity.DataProvider == EntityProviderType.SQL.ToString())
                    {
                        if (!TryAddEntityToDictionary<DataProvider.DatabaseEntity>(dbEntities, entity)) continue;
                    }
                    else if (entity.DataProvider == EntityProviderType.ScheduledTask.ToString())
                    {
                        if (!TryAddEntityToDictionary<DataProvider.ScheduledTaskEntity>(dbEntities, entity)) continue;
                    }
                    else if (entity.DataProvider == EntityProviderType.SSRS.ToString())
                    {
                        if (!TryAddEntityToDictionary<DataProvider.ReportEntity>(dbEntities, entity)) continue;
                    }
                    else if (entity.DataProvider == EntityProviderType.Perl.ToString())
                    {
                        if (!TryAddEntityToDictionary<DataProvider.PerlScriptEntity>(dbEntities, entity)) continue;
                    }
                    else continue;

                    /**
                     * Errors filling
                    */

                    foreach (var error in entity.GetErrors())
                    {
                        DataProvider.EntityError entityError = new EntityError()
                        {
                            EntityId = entity.Id,
                            Message = error,
                        };
                        entityErrors.Add(entityError);

                        errorsArchive.Add(new ErrorsArchive()
                        {
                            batchId = _batchId,
                            entityName = entity.FullName,
                            errorText = error,
                            entityType = entity.Type.ToString()
                        });

                        if (entityErrors.Count >= BULK_TOP_ROWS)
                        {
                            ctx.EntityError.BulkInsert(entityErrors);
                            entityErrors.Clear();
                        }
                        if (errorsArchive.Count >= BULK_TOP_ROWS)
                        {
                            ctx.ErrorsArchive.BulkInsert(errorsArchive);
                            errorsArchive.Clear();
                        }
                    }

                    /**
                     * Persons filling
                    */
                    
                    HandlePersons(entity, ctx);

                    BulkInsertEntities(dbEntities, ctx, false);

                    FillDb(ctx, entity.ScopeChildren, dbEntities, entityErrors, errorsArchive, false);
                }
            if (isStart)
            {
                BulkInsertEntities(dbEntities, ctx, true);
                ctx.EntityError.BulkInsert(entityErrors);
                ctx.ErrorsArchive.BulkInsert(errorsArchive);
            }
        }

        private void FillDbDependencies(DependencyCheckerEntities ctx, List<IEntity> entities, List<Dependency> dbEntities = null, bool isStart = true)
        {
            if (dbEntities == null)
                dbEntities = new List<Dependency>();
            if (entities != null)
                foreach (var entity in entities)
                {
                    Guid sourceId = entity.Id;

                    if (_excludedGuids.Contains(entity.Id))
                        continue;

                    _excludedGuids.Add(sourceId);


                    foreach (var depOnEntity in entity.DependsOnRecursive)
                    {
                        Guid dependsOnId = depOnEntity.Id;

                        if (dependsOnId != Guid.Empty)
                        {
                            var dep = new KeyValuePair<Guid, Guid>(sourceId, dependsOnId);
                            if (_addedDependencies.Contains(dep)) continue;
                            _addedDependencies.Add(dep);
                            dbEntities.Add(new Dependency()
                            {
                                SourceId = sourceId,
                                DependsOnId = dependsOnId
                            });

                            if (dbEntities.Count == BULK_TOP_ROWS)
                            {
                                ctx.Dependency.BulkInsert(dbEntities);
                                dbEntities.Clear();
                            }
                        }
                    }
                    foreach (var usedByEntity in entity.UsedByRecursive)
                    {
                        Guid usedById = usedByEntity.Id;

                        var dep = new KeyValuePair<Guid, Guid>(usedById, sourceId);
                        if (_addedDependencies.Contains(dep)) continue;
                        _addedDependencies.Add(dep);

                        if (usedById != Guid.Empty)
                        {
                            dbEntities.Add(new Dependency()
                            {
                                SourceId = usedById,
                                DependsOnId = sourceId
                            });

                            if (dbEntities.Count == BULK_TOP_ROWS)
                            {
                                ctx.Dependency.BulkInsert(dbEntities);
                                dbEntities.Clear();
                            }
                        }
                    }

                    FillDbDependencies(ctx, entity.ScopeChildren, dbEntities, false);
                }

            if (isStart)
            {
                ctx.Dependency.BulkInsert(dbEntities);
                ctx.Database.ExecuteSqlCommand(@"
DELETE FROM[DependencyChecker].[dbo].[Dependency]
WHERE DependsOnId NOT IN (SELECT Id FROM [DependencyChecker].[dbo].[EntityBase])
OR SourceId NOT IN (SELECT Id FROM [DependencyChecker].[dbo].[EntityBase])");
            }
        }
    }
}
