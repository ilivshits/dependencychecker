﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceProcess;
using DependencyChecker.Collector;
using DependencyChecker.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32;

namespace DependencyChecker.Tests
{
    [DeploymentItem("Resources\\POne.Kernel.dll")]
    [TestClass]
    public class DataCollectionTests
    {

        [TestMethod]
        public void DataSourceTest()
        {
            var dsList = GetReportDataSources("/ZZ_UAT/Compliance/OptionPositionLimit");
            foreach (var i in dsList)
            {
                Console.WriteLine(i);
            }
            Assert.IsTrue(dsList.Any(i =>
                i.Trim().IndexOf("Data Source=ny-gmitftbdb1;Initial Catalog=P1ReportingZZUAT",
                    StringComparison.InvariantCultureIgnoreCase) >= 0));
            Assert.AreEqual(1, dsList.Count);
        }

        [TestMethod]
        public void DataSourceTest2()
        {
            var dsList = GetReportDataSources("/ZZ_UAT/GMIT/Reporting Group Risk Entity Rec");
            foreach (var i in dsList)
            {
                Console.WriteLine(i);
            }
            Assert.IsTrue(dsList.Any(i =>
                i.Trim().IndexOf("Data Source=ny-gmitftbdb1;Initial Catalog=P1ReportingZZUAT",
                    StringComparison.InvariantCultureIgnoreCase) >= 0));
            Assert.AreEqual(1, dsList.Count);
        }

        [TestMethod]
        public void TestDomainName()
        {
            var provider = new ActiveDirectoryDataProvider(null);
            var resuilt = provider.DomainName;

            Assert.IsNotNull(resuilt);
            Debug.WriteLine(resuilt);
        }

        [TestMethod]
        public void TestDomainShortName()
        {
            var provider = new ActiveDirectoryDataProvider(null);
            var resuilt = ActiveDirectoryDataProvider.UserDomainName;

            Assert.IsNotNull(resuilt);
            Debug.WriteLine(resuilt);
        }

        [TestMethod]
        [Ignore]
        public void TestADRequest()
        {
            var provider = new ActiveDirectoryDataProvider(null);
            var resuilt = provider.GetAllDomainUsers();

            Assert.IsNotNull(resuilt);
            Assert.IsTrue(resuilt.Count > 1000);

            Debug.WriteLine("\r\n\r\n\r\nLogins:\r\n");
            Debug.WriteLine(String.Join("\r\n", resuilt.Select(e => ActiveDirectoryDataProvider.UserDomainName + "\\" + e.Definition + " (" + e.Name + ")").OrderBy(s => s).ToArray()));
        }

        [TestMethod]
        [Ignore]
        public void TestADRequest2()
        {
            var provider = new ActiveDirectoryDataProvider(null);
            var resuilt = provider.GetAllServiceAccounts();

            Assert.IsNotNull(resuilt);
            Assert.IsTrue(resuilt.Count > 1000);

            Debug.WriteLine("\r\n\r\n\r\nLogins:\r\n");
            Debug.WriteLine(String.Join("\r\n", resuilt.Select(e => ActiveDirectoryDataProvider.UserDomainName + "\\" + e.Definition + " (" + e.Name + ")").OrderBy(s => s).ToArray()));
        }

        [TestMethod]
        public void TestP1TCEntitiesLoad()
        {
            var connection = new SqlConnectionStringBuilder
            {
                DataSource = "nj-gmitftbdb-de",
                InitialCatalog = "POneSqlClient",
                IntegratedSecurity = true
            };

            var result = P1TCDataProvider.LoadEntityChildren(connection.ConnectionString);

            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void TestTaskSchedulerCollection()
        {
            var provider = new ScheduledTasksDataProvider(null);
            var result = provider.LoadServer("NY-GMITFTB-DEV");

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ScopeChildren);
            Assert.IsTrue(result.ScopeChildren.Count > 0);

            foreach (var task in result.ScopeChildren)
            {
                Debug.WriteLine(String.Format("Task '{0}' actions:", task.Name));
                if (task.ScopeChildren != null)
                {
                    foreach (var action in task.ScopeChildren)
                    {
                        Debug.WriteLine(String.Format("\taction '{0}' [{2}] definition:\r\n\t{1}",
                            String.IsNullOrEmpty(action.Name) ? task.Name : action.Name,
                            action.Type == EntityType.Executable ? "BINARY NOT PRINTED" : action.Definition,
                            action.TypeName));
                    }
                }
                Debug.WriteLine(String.Empty);
            }

            foreach (var task in result.ScopeChildren)
            {
                Debug.WriteLine(String.Format("Task '{0}' definition:\r\n{1}", task.Name, task.Definition));
                Debug.WriteLine(String.Empty);
            }
        }


        private string GetImagePath(string service, string machine)
        {
            string registryPath = @"SYSTEM\CurrentControlSet\Services\" + service;
            var keyLocalMachine = Registry.LocalMachine;

            RegistryKey key = (machine != "")
                ? RegistryKey.OpenRemoteBaseKey
                    (RegistryHive.LocalMachine, machine).OpenSubKey(registryPath)
                : keyLocalMachine.OpenSubKey(registryPath);

            if (key != null)
            {
                string value = key.GetValue("ImagePath").ToString();
                key.Close();
                return ExpandEnvironmentVariables(value, machine);
            }
            return String.Empty;
        }

        private string ExpandEnvironmentVariables(string path, string machine)
        {
            if (machine == "")
            {
                return Environment.ExpandEnvironmentVariables(path);
            }
            const string systemRootKey = @"Software\Microsoft\Windows NT\CurrentVersion\";

            var key = RegistryKey.OpenRemoteBaseKey
                    (RegistryHive.LocalMachine, machine).OpenSubKey(systemRootKey);
            string expandedSystemRoot = key.GetValue("SystemRoot").ToString();
            key.Close();

            path = path.Replace("%SystemRoot%", expandedSystemRoot);
            return path;
        }

        [TestMethod]
        public void TestServicesList()
        {
            var serverName = "NY-GMITDEV5";
            var result = ServiceController.GetServices(serverName);

            foreach (var service in result)
            {
                if (service.ServiceName.Contains("POne.Portfolio"))
                {
                    var key =
                        RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, serverName)
                            .OpenSubKey(@"SYSTEM\CurrentControlSet\Services\" + service.ServiceName);
                    if (key != null)
                    {
                        var path = key.GetValue("ImagePath").ToString();
                        key.Close();

                        path = Path.Combine("\\\\" + serverName, path.Replace(":\\", "$\\").Trim('"') + ".config");

                        Debug.WriteLine("Opening configuration file: " + path);

                        var configurationMap = new ExeConfigurationFileMap
                        {
                            ExeConfigFilename = path
                        };
                        var config = ConfigurationManager.OpenMappedExeConfiguration(configurationMap, ConfigurationUserLevel.None);

                        Debug.WriteLine(config.AppSettings.Settings["Environment"].Value);

                        foreach (KeyValueConfigurationElement setting in config.AppSettings.Settings)
                        {
                            Debug.WriteLine("[{0}] = '{1}'", setting.Key, setting.Value);
                        }
                        
                    }
                }
            }

            Debug.WriteLine(result);
        }

        [TestMethod]
        public void TestFilePull()
        {
            var result = ScheduledTasksDataProvider.PullFileContents("NY-GMITFTB-DEV", @"Y:\Services\MipsQA\StopMipsQA.bat");

            Assert.IsFalse(String.IsNullOrEmpty(result));
            Debug.WriteLine(result);
        }

        [TestMethod]
        public void TestDTSFilePull()
        {
            var provider = new ScheduledTasksDataProvider(null);
            var result = provider.GetDTSPackageSource("NY-GMITFTB-DEV", @"Y:\DEV\Scripts\dts\db_fx.dtsx");

            Assert.IsFalse(String.IsNullOrEmpty(result));
            //Debug.WriteLine(result);
        }

        private static List<String> GetReportDataSources(string itemPath)
        {
            var service = new ReportingService("NY-GMITRPT1") { Credentials = CredentialCache.DefaultCredentials };
            //var items = service.ListDependentItems("/ZZ_UAT/Data Sources/P1Reporting_ZZUAT");
            var items = service.ListDependentItems(itemPath);
            var ds = service.GetItemDataSources(itemPath);
            foreach (var item in items)
            {
                Console.WriteLine(item.Path);
            }
            var result = new List<String>();
            foreach (var d in ds)
            {
                Console.WriteLine(d.Item);
                var dsRef = d.Item as DataSourceReference;
                var dsDef = d.Item as DataSourceDefinition;
                if (dsRef != null)
                {
                    Console.WriteLine(dsRef.Reference);
                    var item = service.GetDataSourceContents(dsRef.Reference);
                    if (item != null)
                    {
                        result.Add(item.ConnectString);
                    }
                    else
                    {
                        Assert.Fail("Reference is not found on a server");
                    }
                }

                if (dsDef != null)
                {
                    Console.WriteLine(dsDef.ConnectString);
                    result.Add(dsDef.ConnectString);
                }
            }
            return result;
        }

    }
}
