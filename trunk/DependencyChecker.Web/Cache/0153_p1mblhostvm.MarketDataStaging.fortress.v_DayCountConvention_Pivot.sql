
create view [fortress].[v_DayCountConvention_Pivot] as
	select
		 0 as NotSpecified,
		 1 as Actual365Fixed,
		 2 as Actual360,
		 3 as Actual365Actual,
		 4 as Thirty360Isda,
		 5 as Thirty360EuroIsma,
		 6 as Thirty360Euro,
		 7 as ActualActualIsma,
		 8 as ActualActualIsda,
		 9 as Thirty360Old,
		10 as Thirty360EuroOld,
		11 as Thirty360Sia,
		12 as Thirty360Bma,
		13 as Thirty360German,
		14 as Actual365L,
		15 as Nl365,
		16 as Bus252


