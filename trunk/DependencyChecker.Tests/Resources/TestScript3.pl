﻿use DBI;
use Net::SMTP;
use POSIX 'strftime';
use Text::CSV_XS;

require '..\Globals.pl';

#-----------------------------------------#
# Globals
#-----------------------------------------#

@EMAIL_TO = ();
@EMAIL_TO = ('Group_GMIT@fortress.com');
$EMAIL_FROM = 'GMIT@fortress.com';
$EMAIL_SUBJ = "FTB1 Markit Download jobs($GLOBAL_DATE) ";

#-------------------------------------#
# Configure SMTP connection
#-------------------------------------#
$smtp = Net::SMTP->new($EMAIL_SVR);

#-------------------------------------#
# Connect to DB 
#-------------------------------------#

$DB_SERVER = 'NY-GMITFTBDB1';
$dsn = "Provider=sqloledb;Integrated Security=SSPI;";
$dsn .= "Server=${DB_SERVER};Database=MarkIt";

my $dbh = DBI->connect("dbi:ADO:$dsn",  "", "") || die "Database connection not made: $DBI::errstr";

my $sql = qq{ SELECT * FROM  dbo.v_status order by Date desc};
my $sth = $dbh->prepare( $sql );
$sth->execute();

my $msg  = sprintf("Recentlly loaded data rows...\n\n%12s %12s %12s %12s %12s %12s %12s %12s\n", 
	"Date", "T0Index", "T1Index", "T1Constitutes", 
	"T1Tranche", "T1Spread", "T1FixedCoupon", "T0Spread" );

my $sc = " FAILED!!! "; 
$msg .= "-" x 110 . "\n";
my ($dt, $t0i, $t1i, $cc, $tc, $sp, $fc, $t0);
$sth->bind_columns( \$dt, \$t0i, \$t1i, \$cc, \$tc, \$sp, \$fc, \$t0);

my $i = 0;
while( $sth->fetch() ) {
  if($i == 0)
  {
     my @dd = split(/\//, $dt);
     my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
     my $b = 1;
     if( $wday == 1)
     {
        $b = 3;
     }

     ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time-24*$b*3600);

     if( $t1i && $cc && $tc && $sp && $fc && ($dd[0] == $mon + 1) 
	&& ($dd[1] == $mday)  && ($dd[2] == $year + 1900))
     {
        $sc = "Successful ";
     }
     elsif ($t1i && $sp && ($dd[0] == $mon + 1) 
	&& ($dd[1] == $mday)  && ($dd[2] == $year + 1900))
     {
	 $sc = "Warning on ";
     }
  }
  my $strLine = sprintf("%12s %12d %12d %12d %12d %12d %12d %12d\n",$dt, $t0i, $t1i, $cc, $tc, $sp, $fc, $t0);
  $msg .= $strLine;
  $i++;
	
}



$EMAIL_SUBJ = $sc . $EMAIL_SUBJ ;

$smtp->mail($EMAIL_FROM);
$smtp->to(@EMAIL_TO);

$smtp->data();
$smtp->datasend("To: " . join(',', @EMAIL_TO) . "\n");
$smtp->datasend("Subject: $EMAIL_SUBJ\n");
$smtp->datasend("\n\n" );
$smtp->datasend($msg );

#-------------------------------------#
# Close SMTP connection
#-------------------------------------#
$smtp->dataend();
$smtp->quit;
#-------------------------------------#


#-------------------------------
# Extra csv file generation for csv
#--------------------------------
#$sql = qq{ EXEC	 dbo.sp_getMarkitT1Curve };
#my $sth = $dbh->prepare( $sql );
#$sth->execute();


#my $csv = Text::CSV_XS->new();
#my @row;

#my $file = ">\\\\ny-files1\\Groups\\HedgeNonRes\\GMIT\\PNLGroup\\MarkitData\\T1.csv";
#open T1, $file || die "Can't open T1.csv file to write";
#my  $aref = $sth->{NAME}; 
                
#do
#{
#  $csv->combine( @$aref ); 
#  print T1 $csv->string() . "\n";             
#} while($aref = $sth->fetchrow_arrayref() ); 

#close T1;
