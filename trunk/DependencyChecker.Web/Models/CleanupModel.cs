﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DependencyChecker.Common;

namespace DependencyChecker.Web.Models
{
    public class CleanupModel
    {
        public Entity Scope { get; set; }
        public List<Entity> Unused { get; set; }
    }
}