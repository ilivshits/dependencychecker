CREATE procedure [dbo].[GenerateNextSequence]
(
	@sequenceName varchar(100),
	@thisManySequences int = 1
)
as
begin
	
	set nocount on
	
	declare @nextValue bigint
	update SequenceGenerator
	set @nextValue = nextValue = nextValue + increment * @thisManySequences
	where sequenceName = @sequenceName
	
	select @nextValue - 1

end


