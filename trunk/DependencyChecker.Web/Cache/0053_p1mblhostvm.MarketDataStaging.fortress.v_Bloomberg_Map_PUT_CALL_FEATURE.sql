
create view [fortress].[v_Bloomberg_Map_PUT_CALL_FEATURE] as
	select 'Annual' as PUT_CALL_FEATURE, 1 as putCallFeatureId union
	select 'Anytime', 2 as putCallFeatureId union
	select 'Bi-monthly', 3 as putCallFeatureId union
	select 'Monthly', 4 as putCallFeatureId union
	select 'Onetime', 5 as putCallFeatureId union
	select 'Quarterly', 6 as putCallFeatureId union
	select 'Semi-Annual', 7 as putCallFeatureId union
	select 'Semi-Monthly', 8 as putCallFeatureId union
	select 'Weekly', 9 as putCallFeatureId


