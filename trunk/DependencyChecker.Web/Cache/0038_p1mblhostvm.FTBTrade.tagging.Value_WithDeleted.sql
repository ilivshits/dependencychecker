create view [tagging].[Value_WithDeleted] as
select
	v.valueId, v.contextId, v.objectId, v.tagId, 1 as live
from
	tagging.Value as v with (nolock)
union
select
	v.valueId, v.contextId, v.objectId, v.tagId, 0 as live
from
	tagging.ValueLog as v with (nolock)
where
	not exists (
		select
			v2.*		
		from
			tagging.Value as v2 with (nolock)
		where
			v2.valueId = v.valueId
	) and
	v.action = 'D'


