create function [tagging].[GetSqlVariant](@baseTypeId int, @value_Int bigint, @value_Text nvarchar(max), @value_DateTime datetime, @value_Decimal decimal(19,8))
returns table as return
(
	select value = case @baseTypeId
		when [Text] then cast(cast(@value_Text as nvarchar(4000)) as sql_variant)
		when [Datetime] then cast( @value_DateTime as sql_variant)
		when [Bool] then cast(cast(@value_Int as bit) as sql_variant)
		when [Integer] then cast(cast(@value_Int as int) as sql_variant)
		when [Decimal] then cast(@value_Decimal as sql_variant)
		when [System List] then cast(@value_Int as sql_variant)
		when [User List] then cast(cast(@value_Int as int) as sql_variant)
		when [Date] then cast(cast(@value_DateTime as date) as sql_variant)
		when [Time] then cast(cast(@value_DateTime as time) as sql_variant)
		when [XML] then cast(cast(@value_Text as nvarchar(4000)) as sql_variant)
		else null end
	from tagging.BaseType_Transposed
)


