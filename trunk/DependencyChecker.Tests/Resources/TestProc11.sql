﻿CREATE PROCEDURE [FIG].[IntradayCRDRestrictedListUpdate]		
		@debug int = 0,
		@runningEnv VARCHAR(20) = 'PROD',
		@last_run_date datetime = NULL
AS
BEGIN
/*

		--exec fig.[IntradayCRDRestrictedListUpdate]	1, 'DEV', null
		Declare @debug int = 0
		Declare @runningEnv VARCHAR(20) = 'DEV'
		Declare @last_run_date datetime = NULL
		*/

declare @blockStartTime datetime, @startTime datetime, @err int, @baseMsg varchar(200), @msg varchar(max), @status varchar(20)
declare @errCnt int, @startRows int, @endRows int, @addDelMsg VARCHAR(200)
declare @CONST_ERROR varchar(20), @CONST_SUCCESS varchar(20), @CONST_WARN varchar(20)	

if ISNULL( @runningEnv , '') = ''  set @runningEnv = 'PROD'

select @CONST_ERROR = 'ERROR', @CONST_SUCCESS = 'SUCCESS', @CONST_WARN = 'WARNING'
select @status = @CONST_SUCCESS, @errCnt = 0

select @startTime = getdate(), @addDelMsg = ''
set @baseMsg = 'P1TC to Charles River Intraday Issuer & Security List Load (' + @runningEnv + ')'
if @debug = 1 print @baseMsg + ' starting at ' + CONVERT(varchar(100),@startTime,120) 
		
-- retrieve run time of last successful run.
if @last_run_date is null
	select @last_run_date = max(UpdateDate) from ProcessLog where ProcessTypeId = 3 and ProcessSubTypeId = 3

if @last_run_date is null
	set @last_run_date = '3/17/2014'

if @debug = 1 print 'IntradayCRDRestrictedListUpdate last successfully run: ' + convert(varchar(30),@last_run_date)
 
 
 -- see if there have been any changes
 declare @issuerRestrictionsChanged BIT, @securityRestrictionsChanged BIT 
 
 exec [P1TC].[FTBTrade].[dbo].[CR_CheckRestrictionChanges] @last_run_date, @issuerRestrictionsChanged output, @securityRestrictionsChanged output
 if @debug = 1 print '@issuerRestrictionsChanged: ' + ISNULL(CONVERT(VARCHAR(5), @issuerRestrictionsChanged ),'NULL') + ' ;  @securityRestrictionsChanged: ' + ISNULL(CONVERT(VARCHAR(50),  @securityRestrictionsChanged ),'NULL')
  
 -- issuer restrictions changed
 if @issuerRestrictionsChanged = 1
	 BEGIN
	 
		if @debug = 1 print 'Issuer Restrictions Changed'
		select @msg = 'P1TC to Charles River Intraday Restricted Issuer Load', @status = ''
		
		BEGIN TRY
		
			DELETE FROM FIG.OMV_TEMP_RESTRICTED_LIST
			SELECT @startRows = @@rowcount, @err = @@error					
			
			if @debug = 1 print convert(varchar(10),isnull(@startRows,0) ) + ' rows deleted from CRIMS.FIG.OMV_TEMP_RESTRICTED_LIST'	
			
            -- count before count			
            SELECT 
                @startRows = COUNT(*)
            FROM 
                CSM_ISSUER_RESTR isr
            INNER JOIN fig.security_list issL ON issL.security_list_cd = isr.ISSUER_LIST_CD collate Latin1_General_BIN2
	            AND issL.source_system = 'P1TC' AND issL.active_list = 1
	            AND issL.intraday_update = 1 AND issL.issuerApplicableFlag = 1
				
            
			INSERT INTO FIG.OMV_TEMP_RESTRICTED_LIST
			(
				ISSUER_LIST_CD,
				ISSUER_CD
			)
		    SELECT DISTINCT 			
			    issL.security_list_cd AS ISSUER_LIST_CD,
    			p1ir.bloombergCompanyId AS ISSUER_CD
	    	FROM 
		    	[P1TC].[FTBTrade].[dbo].[CR_IssuerRestriction] p1ir
		    	INNER JOIN fig.security_list issL ON issL.source_code collate Latin1_General_BIN2 = p1ir.restriction AND issL.source_system = 'P1TC' 
		    	        AND issL.active_list = 1 AND issL.intraday_update = 1 AND issL.issuerApplicableFlag = 1
		    	INNER JOIN CSM_ISSUER issr ON issr.ISSUER_CD = CONVERT(VARCHAR(20), p1ir.bloombergCompanyId collate Latin1_General_BIN2)
			WHERE 
				p1ir.endDate >= convert(DATETIME, left(getdate(), 11)) 
				AND p1ir.startDate <= convert(DATETIME, left(getdate(), 11))			
			
			SELECT @endRows = @@rowcount, @err = @@error	
			if @debug = 1 print convert(varchar(10),isnull(@endRows,0) ) + ' issuer codes inserted into CRIMS.FIG.OMV_TEMP_RESTRICTED_LIST from P1TC'
			
			-- merge these records into CRIMS
			exec fig.sp_merge_staging_into_restrissuer
			
			select @startRows = ISNULL( @startRows, 0), @endRows = ISNULL( @endRows , 0), @addDelMsg = ''
			
			if @startRows > @endRows
				set @addDelMsg = ' ' + CONVERT(VARCHAR(50), (@startRows - @endRows) ) + ' records deleted.'
			else if @startRows < @endRows
			    set @addDelMsg = ' ' + CONVERT(VARCHAR(50), (@endRows - @startRows) ) + ' records added.'			
			
			select @status = @CONST_SUCCESS
		END TRY
		BEGIN CATCH
			select 
				@msg = 'Inserting Issuer records into CRIMS.FIG.OMV_TEMP_RESTRICTED_LIST from P1TC' + '. Error is ' + ERROR_MESSAGE() + '.' , 
				@status = @CONST_ERROR,
				@errCnt = @errCnt + 1
		END CATCH			
			
		set @msg = @msg + ': ' + @status + '.' + @addDelMsg  -- + cast(getdate() as varchar)	
			
		insert into ProcessLog ([ProcessTypeId], [ProcessSubTypeId], [Status], [Message], [UpdateDate], [UpdatedBy])
			values(3, 1, @status, @msg, getdate(), SUSER_SNAME())	
	 END
	
	 if @securityRestrictionsChanged = 1
		BEGIN
		
			if @debug = 1 print 'Security Restrictions Changed'
			select @msg = 'P1TC to Charles River Intraday Restricted Security Load', @status = ''
			
			BEGIN TRY
			
				delete from fig.OMV_TEMP_SECURITY_LIST_MEMBER
				SELECT @startRows = @@rowcount	
				if @debug = 1 print convert(varchar(10),isnull(@startRows,0) ) + ' old rows deleted from CRIMS.FIG.OMV_TEMP_SECURITY_LIST_MEMBER'
				
                -- count before count			
                SELECT 
                    @startRows = COUNT(*)
                FROM 
                    CSM_SECURITY_RESTR isr
                INNER JOIN fig.security_list issL ON issL.security_list_cd = isr.SECURITY_LIST_CD collate Latin1_General_BIN2
	                AND issL.source_system = 'P1TC' AND issL.active_list = 1
	                AND issL.intraday_update = 1 AND issL.securityApplicableFlag = 1				
						
				INSERT INTO fig.OMV_TEMP_SECURITY_LIST_MEMBER
				(
				    SECURITY_LIST_CD, 
				    EXT_SEC_ID, 
				    SEC_ID
				)
				SELECT DISTINCT
                    secL.security_list_cd,	
    				cs.EXT_SEC_ID, --sid
					cs.SEC_ID
				FROM 
					[P1TC].[FTBTrade].[dbo].[SecurityRestriction] p1sir	
					INNER JOIN [P1TC].[FTBTrade].[dbo].[RestrictionType] rt ON rt.restrictionTypeId = p1sir.restrictionTypeId				
					INNER JOIN fig.[security_list] secL ON secL.source_code collate Latin1_General_BIN2 = rt.code AND secL.source_system = 'P1TC' AND secL.active_list = 1 
					    AND secL.intraday_update = 1 AND secL.securityApplicableFlag = 1
					INNER JOIN CSM_SECURITY cs ON cs.EXT_SEC_ID = CONVERT(VARCHAR(25), p1sir.sid) AND isnumeric(cs.EXT_SEC_ID) = 1 					
				WHERE 
					p1sir.endDate >= convert(DATETIME, left(getdate(), 11)) 
					AND p1sir.startDate <= convert(DATETIME, left(getdate(), 11))				
						
				
				SELECT @err = @@error, @endRows = @@rowcount
				if @debug = 1 print convert(varchar(10),isnull(@endRows,0) ) + ' rows inserted into CRIMS.FIG.OMV_TEMP_SECURITY_LIST_MEMBER'						
				
				exec fig.SP_MERGE_STAGING_INTO_RESTRSECURITY
				
			    select @startRows = ISNULL( @startRows, 0), @endRows = ISNULL( @endRows , 0), @addDelMsg = ''
    			
			    if @startRows > @endRows
				    set @addDelMsg = ' ' + CONVERT(VARCHAR(50), (@startRows - @endRows) ) + ' records deleted.'
			    else if @startRows < @endRows
			        set @addDelMsg = ' ' + CONVERT(VARCHAR(50), (@endRows - @startRows) ) + ' records added.'					
				
				select @status = @CONST_SUCCESS
				
			END TRY
			BEGIN CATCH
				select 
					@msg = @msg + ': ' + ERROR_MESSAGE(), 
					@status = @CONST_ERROR,
					@errCnt = @errCnt + 1
			END CATCH				
			
			    set @msg = @msg + ': ' + @status + '.' + @addDelMsg  -- + cast(getdate() as varchar)									
	
				insert into ProcessLog ([ProcessTypeId], [ProcessSubTypeId], [Status], [Message], [UpdateDate], [UpdatedBy])
					values(3, 2, @status, @msg, getdate(), SUSER_SNAME() )			
		END 
	
		
		if @errCnt > 0
			select 
				@status = @CONST_ERROR,
				@msg = @baseMsg + ' ran with ' + CONVERT(varchar(10),@errCnt) + ' errors.'
        else
            select                
				@msg = @baseMsg + ' COMPLETE. (' + convert(varchar(50), DATEDIFF(millisecond , @startTime, getdate())) + ' milliseconds.)'
			
	
		-- mark as completed
		insert into ProcessLog ([ProcessTypeId], [ProcessSubTypeId], [Status], [Message], [UpdateDate], [UpdatedBy])
					values(3, 3, @status, @msg, getdate(), SUSER_SNAME() )		
					
		if @debug = 1 
		begin
			print @baseMsg + ' finished at ' + CONVERT(varchar(100), GETDATE(),120) 		
			print 'Total Processing Time: ' + convert(varchar(50), DATEDIFF(millisecond , @startTime, getdate())) + ' milliseconds.'
		end		
 	
	
END
