create procedure [dispatcher].[Send]
(
	@targetService varchar(200),
	@message varbinary(max)
)
as
begin

	set nocount on;
	set xact_abort on;
	
	-- construct message
	if @message is null return;

	-- grab existing conversation
	declare @dialogId uniqueidentifier
	select @dialogId = dialogId
	from dispatcher.Conversation
	where targetService = @targetService;
	
	-- send on conversation
	send on conversation @dialogId message type DispatcherMessage (@message)

end


