﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using System.Text.RegularExpressions;
using System.Threading;
using DependencyChecker.Collector.Base;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Common;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Events;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Collector
{
    public class ReportsDataProvider : BaseDataProvider
    {
        private const string ReportDatabaseName = "ReportServer";
        private static readonly List<Regex> BasicPattern =
            new List<Regex>{ new Regex(@"^(?<name>[a-z0-9-\[\]\.]+)$", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Compiled)};

        public ReportsDataProvider(IServerProvider serverProvider)
            : base(serverProvider)
        {
        }

        #region Overrides of BaseDataProvider

        public override string Name
        {
            get { return ReportEntity.ProviderName; }
        }

        protected override List<Regex> ItemSearchPattern
        {
            get { return BasicPattern; }
        }

        public override List<IEntity> Search()
        {
            LoggerHelper.Log.InfoFormat("Starting search reports...");
            var result = ServerProvider.GetServers(EntityProviderType.SSRS).Select(LoadServer).ToList();
            GC.Collect();
            return result;
        }

        private static IEntity GetReportEntity(CatalogItem item, ReportingService service)
        {
            try
            {
                var type = (EntityType)Enum.Parse(typeof(EntityType), item.TypeName);
                var definitionStr = String.Empty;
                var user = String.Empty;
                if (type != EntityType.Folder)
                {
                    if (type == EntityType.DataSource)
                    {
                        var definition = service.GetDataSourceContents(item.Path);
                        definitionStr = definition.ConnectString + ";User=" + definition.UserName;
                        if (!String.IsNullOrEmpty(definition.UserName))
                        {
                            var userName = definition.UserName.Split('\\');
                            user = userName.Length > 1 ? userName[1] : userName[0];
                        }
                        LoggerHelper.Log.InfoFormat("DATASOURCE '{0}', User = '{1}', Impersonate = '{2}'", item.Path, definition.UserName, definition.ImpersonateUser);
                    }
                    else if (type != EntityType.LinkedReport)
                    {
                        var definition = service.GetItemDefinition(item.Path);
                        definitionStr = System.Text.Encoding.UTF8.GetString(definition);
                    }
                }
                return new ReportEntity(item.Name, definitionStr, type, user)
                {
                    InternalId = item.ID,
                    Created = item.CreationDate,
                    CreatedBy = new PersonEntity(item.CreatedBy),
                    LastModified = item.ModifiedDate,
                    LastModifiedBy = new PersonEntity(item.ModifiedBy)
                };
            }
            catch (Exception e)
            {
                LoggerHelper.Log.ErrorFormat("Error occured when loading reporting element '{0}' of type {1}: {2}", item.Name, item.TypeName, e);
            }
            return null;
        }

        private void LoadChildren(IEntity entity, CatalogItem item, ReportingService service, List<IEntity> reports)
        {
            var itemPath = item == null ? "/" : item.Path;
            LoggerHelper.Log.InfoFormat("Loading folder '{0}' contents...", itemPath);
            var children = service.ListChildren(itemPath, false);

            entity.ScopeChildren = children.Select(i => GetReportEntity(i, service)).Where(i => i != null).ToList();
            entity.ScopeChildren.ForEach(c => c.Scope = entity);

            reports.AddRange(entity.ScopeChildren.Where(e => e.Type == EntityType.Report));

            _totalProgress += _folderWeight;
            OnProgress(new DependencyCheckerProgressEventArgs
            {
                Message = "Loaded root SSRS folder " + entity.FullName,
                Progress = _totalProgress
            });

            foreach (var child in entity.ScopeChildren.Where(child => child.Type == EntityType.Folder))
            {
                LoadChildren(child, children.Single(c => c.Name == child.Name), service, reports);
            }
        }

        private float _folderWeight;
        private float _totalProgress;


        private IEntity LoadServer(string serverName)
        {
            var server = new ReportEntity(serverName, EntityType.Server);
            var service = new ReportingService(serverName) { Credentials = CredentialCache.DefaultCredentials };

            // Load tree of the objects
            var reports = new List<IEntity>();

            _totalProgress = 0;
            _folderWeight = 0.5f / service.ListChildren("/", true).Count(i => i.TypeName == EntityType.Folder.ToString());

            LoadChildren(server, null, service, reports);

            LoggerHelper.Log.InfoFormat("Loading reports references...");
            var p = 0;
            var step = reports.Count / 10.0;
            var lastP = 0;
            // Link reports to data sources
            foreach (var report in reports)
            {
                //var references = service.GetItemReferences(report.FullName, "DataSource");
                //foreach (var reference in references.Where(r => r.Reference != null))
                //{
                //    var entity = server.Find(reference.Reference);
                //    if (entity != null)
                //    {
                //        report.AddDependency(entity, reference.Name);
                //    }
                //}

                var dataSources = service.GetItemDataSources(report.FullName);
                foreach (var reference in dataSources)
                {
                    var dsRef = reference.Item as DataSourceReference;
                    var dsDef = reference.Item as DataSourceDefinition;
                    
                    if (dsRef != null)
                    {
                        var entity = server.Find(dsRef.Reference);
                        if (entity != null)
                        {
                            report.AddDependency(entity, reference.Name);
                        }
                        else
                        {
                            report.AddErrorFormat(EntityErrorCode.ParsingError, "Cannot get data source reference {0} for report {1}", reference.Name, report.FullName);
                        }
                    }
                    else if (dsDef != null)
                    {
                        if (report.ScopeChildren == null)
                        {
                            report.ScopeChildren = new List<IEntity>();
                        }
                        var userName = string.IsNullOrEmpty(dsDef.UserName) ? new string[0]  : dsDef.UserName.Split('\\');
                        var user = userName.Length > 1 ? userName[1] : (userName.Length > 0  ? userName[0] : string.Empty);
                        var newDataSource = new ReportEntity(reference.Name, dsDef.ConnectString, EntityType.DataSource, user)
                        {
                            Scope = report
                        };
                        report.ScopeChildren.Add(newDataSource);
                        report.AddDependency(newDataSource, reference.Name);
                    }
                    else
                    {
                        report.AddErrorFormat(EntityErrorCode.ParsingError, "Cannot get data source {0} for report {1}", reference.Name, report.FullName);
                    }
                }


                if (lastP >= (int)(++p / step))
                {
                    continue;
                }
                lastP = (int)(p / step);
                LoggerHelper.Log.InfoFormat("Processed {0}0% so far...", lastP);
                OnProgress(new DependencyCheckerProgressEventArgs
                {
                    Message = "Processing reports.. ",
                    Progress = 0.5f + (lastP / 20.0f)
                });
            }
            LoggerHelper.Log.InfoFormat("Reports references loaded");

            return server;
        }
        #endregion
    }
}