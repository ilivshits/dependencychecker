﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using DependencyChecker.Common;
using DependencyChecker.DataProvider.Helpers;

namespace DependencyChecker.Web.Authorization
{
    public static class PrincipalExtensions
    {
        public static bool IsUserInRole(this IPrincipal userPrincipal, string allowedRole)
        {
            return IsUserInRoles(userPrincipal, new []{allowedRole});
        } 

        public static bool IsUserInRoles(this IPrincipal userPrincipal, string[] allowedRoles)
        {
            
            var username = userPrincipal.Identity.IsAuthenticated
                ? userPrincipal.Identity.Name
                : string.Format("{0}\\{1}", String.IsNullOrEmpty(Environment.UserDomainName) ? Environment.MachineName : Environment.UserDomainName, Environment.UserName);

            var rolesArray = GetRolesForUser(username);

            if (allowedRoles.Length > 0)
            {
                foreach (string role in allowedRoles)
                {
                    if (rolesArray.Any(e => e == role))
                        return true;
                }
                return false;
            }
            return true;
        }

        private static string[] GetRolesForUser(string username)
        {
            List<string> roles = new List<string>();
            using (var ctx = DatabaseHelpers.GetContext())
            {
                try
                {
                    var userRoleList = ctx.UserRole.Include(e => e.User1).Include(e => e.Role1).Where(e => e.User1.Username.ToLower() == username.ToLower()).ToList();
                    roles.AddRange(userRoleList.Select(userRole => userRole.Role1.RoleName));
                }
                catch (Exception e)
                {
                    LoggerHelper.Log.Error("Couldn't get roles for user", e);
                    roles = new List<string>();
                }
            }
            return roles.ToArray();
        }
    }
}
