
CREATE view [dbo].[v_FspUnwindInstructions] as
select fsa.orderId, b.blockId, c.settlementDate, c.cashflowId
      , px = isnull(c.referenceAmount, c.amount) * os.quantityMultiplier * cs.quantityMultiplier / s.priceMultiplier / b.quantity
from dbo.ForwardSettlementAdjustment fsa
join dbo.[Order] o on fsa.orderId = o.orderId
join dbo.[OrderSide] os on o.orderSideId = os.orderSideId
join dbo.[Cashflow] c on c.cashflowId = fsa.cashflowId
join dbo.[OrderSide] cs on c.orderSideId = cs.orderSideId
join dbo.[Security] s on c.sid = s.sid
join
(
      select orderId, blockId, quantity = sum(quantity)
      from dbo.[OrderOTCEvent]
      group by orderId, blockId
      
) b on fsa.orderId = b.orderId and fsa.blockId = b.blockId
where c.orderStatusId = 2


