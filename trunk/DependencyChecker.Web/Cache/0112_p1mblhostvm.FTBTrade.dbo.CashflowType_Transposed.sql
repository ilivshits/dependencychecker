CREATE VIEW [dbo].[CashflowType_Transposed] AS
SELECT
[Financing] = 1
, [Dividend] = 2
, [Cost Adjustment] = 3
, [P&S] = 4
, [ABS Interest] = 5
, [ABS Paydown] = 6


