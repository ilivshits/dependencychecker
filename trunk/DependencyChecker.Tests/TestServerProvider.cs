﻿using System.Collections.Generic;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Entities;

namespace DependencyChecker.Tests
{
    public class TestServerProvider : IServerProvider
    {
        #region Implementation of IServerProvider

        public List<string> GetServers(EntityProviderType type)
        {
            return new List<string>();
        }

        public List<string> GetDomains()
        {
            return new List<string>();
        }

        #endregion
    }
}