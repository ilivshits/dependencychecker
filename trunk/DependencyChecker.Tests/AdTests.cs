﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using DependencyChecker.Web.Authorization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DependencyChecker.Tests
{
    [TestClass]
    public class AdTests
    {
        [TestMethod]
        public void GetUserEmail()
        {
            string domain = "universe";
            string name = "sbaburin";


            DirectoryEntry directoryEntry = new DirectoryEntry("LDAP://universe.dart.spb/DC=universe,DC=dart,DC=spb");
            //directoryEntry.Path = "LDAP://DC=universe";
            // get a DirectorySearcher object
            DirectorySearcher search = new DirectorySearcher(directoryEntry)
            {
                Filter = "(&(objectClass=user)(sAMAccountName=" + name + "))"
            };

            // specify the search filter

            // specify which property values to return in the search
            //search.PropertiesToLoad.Add("givenName");   // first name
            //search.PropertiesToLoad.Add("userPrincipalName");          // last name
            //search.PropertiesToLoad.Add("mail");        // smtp mail address
            //search.PropertiesToLoad.Add("name");       
            search.PropertiesToLoad.Add("sAMAccountName");       

            // perform the search
            var result = search.FindOne();
        }

        [TestMethod]
        public void GetUsers()
        {
            using (ActiveDirectoryContext ctx = new ActiveDirectoryContext())
            {
                var s = ctx.GetAllUsers();
            }
        }

        [TestMethod]
        public void GetUsersFilter()
        {
            using (ActiveDirectoryContext ctx = new ActiveDirectoryContext())
            {
                var s = ctx.GetUsersFilter(new List<string>() {"universe\\ilivshits", "universe\\sbaburin" });
            }
        }
    }
}
