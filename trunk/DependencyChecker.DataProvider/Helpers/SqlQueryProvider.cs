﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace DependencyChecker.DataProvider.Helpers
{
    public class SqlQueryProvider
    {
        private const int BATCH_SIZE = 15000;
        public void InsertItems<T>(IEnumerable<T> items, string schema, string tableName,
            IList<ColumnInfo> properties, DbConnection storeConnection) where T : class
        {
            using (var reader = new MyDataReader<T>(items, properties))
            {
                var con = storeConnection as SqlConnection;
                if (con.State != System.Data.ConnectionState.Open)
                {
                    con.Open();
                }
                using (SqlBulkCopy bcp = new SqlBulkCopy(con))
                {
                    bcp.BatchSize = Math.Min(reader.RecordsAffected, BATCH_SIZE);
                    bcp.DestinationTableName = !String.IsNullOrEmpty(schema) ? string.Format("[{0}].[{1}]", schema, tableName) : String.Format("[{0}]", tableName);
                    

                    foreach (ColumnInfo t in properties)
                    {
                        bcp.ColumnMappings.Add(t.ColumnName, t.ColumnName);
                    }


                    bcp.WriteToServer(reader);
                    bcp.Close();
                }
            }
        }
    }
}
