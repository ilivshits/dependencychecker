
create view [fortress].[v_Frequency_Pivot] as
	select
		 0 as NotSpecified,
		 1 as Annual,
		 2 as SemiAnnual,
		 3 as Quarterly,
		 4 as Monthly,
		 5 as Biweekly,
		 6 as Weekly,
		 7 as TwentyEightDays,
		 8 as ThirtyFiveDays,
		 9 as FourtyTwoDays,
		10 as Daily,
		11 as Continuous


