CREATE VIEW [dbo].[vSecurityRestriction] AS

-- direct restrictions
SELECT s.sid, r.startDate, r.endDate, t.restrictionTypeId
FROM [SecurityRestriction] r
INNER JOIN RestrictionType t ON r.restrictionTypeId = t.restrictionTypeId
INNER JOIN [Security] s ON r.sid = s.sid

UNION ALL

-- non propagated
SELECT s.sid, r.startDate, r.endDate, t.restrictionTypeId
FROM [IssuerRestriction] r
INNER JOIN RestrictionType t ON r.restrictionTypeId = t.restrictionTypeId
INNER JOIN [Security] s ON r.issuerId = s.issuerId
WHERE r.includeSubsidiariesFlag = 0

UNION ALL

-- propagated
SELECT s.sid, r.startDate, r.endDate, t.restrictionTypeId
FROM [IssuerRestriction] r
INNER JOIN RestrictionType t ON r.restrictionTypeId = t.restrictionTypeId
CROSS APPLY GetIssuerWithDescendants(r.issuerId) h
INNER JOIN [Security] s ON h.childId = s.issuerId
WHERE r.includeSubsidiariesFlag = 1



