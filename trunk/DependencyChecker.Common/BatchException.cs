﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyChecker.Common
{
    public class BatchException
    {
        public Int32 BatchId { get; set; }
        public DateTime Date { get; set; }
        public String Level { get; set; }
        public String Logger { get; set; }
        public String Message { get; set; }
        public String Exception { get; set; }
    }
}
