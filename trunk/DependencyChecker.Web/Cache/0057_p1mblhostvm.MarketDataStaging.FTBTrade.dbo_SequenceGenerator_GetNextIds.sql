

CREATE PROCEDURE [FTBTrade].[dbo_SequenceGenerator_GetNextIds]
	@sequenceName varchar(100),
	@id bigint output,
	@count int = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	--- The table.
	declare @idTable table (id bigint not null)
	
	--- Execute into the table.
	insert @idTable exec FTBTrade.dbo_GenerateNextSequence @sequenceName, @count
	
	--- Select into the variable.
	select @id = id from @idTable
END



