

CREATE PROCEDURE [fortress].[CorporateAction_Upsert]
	@corporateActionId	bigint,
	@sid bigint,
	@issuerId int,
	@actionId int,
	@corporateActionTypeId int,
	@announcementDate datetime,
	@effectiveDate datetime,
	@notes varchar(max),

	--- Auditing.
	@application_source varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--- Merge the record.
	-- if corporate action id and action id exist then update the corporate action
	-- or else insert the new record	
	MERGE INTO 	[dbo].[CorporateAction] as ca
	USING 
		(Select @corporateActionId, @actionId, @sid, @issuerId) as 
			nca (corporateActionId,actionId, sid, issuerId)
		ON nca.actionId = ca.actionId  and nca.corporateActionId = ca.corporateActionId 			
	WHEN MATCHED THEN
		update set ca.announcementDate = @announcementDate,
				effectiveDate = @effectiveDate,
				notes = @notes
	WHEN NOT MATCHED THEN
		insert (corporateActionId, sid, issuerId, actionId, corporateActionTypeId, announcementDate, effectiveDate, notes)
		values (@corporateActionId,	@sid, @issuerId, @actionId, @corporateActionTypeId, @announcementDate, @effectiveDate, @notes);	
		
END



