﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DependencyChecker.DataProvider.Helpers;
using log4net.Util;

namespace DependencyChecker.DataProvider.Extensions
{
    public static class DbSetExtensions
    {
        public static void Truncate<T>(this DbSet<T> set, DependencyCheckerEntities context = null) where T : class
        {
            context = context ?? DatabaseHelpers.GetContext();
            Type setType = typeof (T);
            string schemaName = DatabaseHelpers.GetSchemaName(context, setType);
            string tableName = DatabaseHelpers.GetTableName(context, setType);
            context.Database.ExecuteSqlCommand("TRUNCATE TABLE " + (!String.IsNullOrEmpty(schemaName) ? string.Format("[{0}].[{1}]", schemaName, tableName) : String.Format("[{0}]", tableName)));
        }

        public static void BulkInsert<TEntity>(this DbSet<TEntity> set, IEnumerable<TEntity> items, DependencyCheckerEntities context = null)
            where TEntity : class
        {
            context = context ?? DatabaseHelpers.GetContext();
            BulkInsertManager<TEntity> manager = new BulkInsertManager<TEntity>(context);

            manager.InsertAll(items);
        }

        public static DbSqlQuery<T> RegexSearch<T>(this DbSet<T> set, string fieldName, string pattern, DependencyCheckerEntities context = null, string includeFields = "*") where T : class
        {
            Type setType = typeof(T);
            string schemaName = DatabaseHelpers.GetSchemaName(context, setType);
            string tableName = DatabaseHelpers.GetTableName(context, setType);
            return set.SqlQuery(@"
exec('select " + includeFields + " from " +
(!String.IsNullOrEmpty(schemaName) ? string.Format("[{0}].[{1}]", schemaName, tableName) : string.Format("[{0}]", tableName)) +
@"where[dbo].[Like](" + fieldName + @", ''" + pattern +
"'') = 1')");
        }
    }
}
