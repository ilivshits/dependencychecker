﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DependencyChecker.DataProvider.Helpers
{
    public class MyDataReader<T> : IDataReader where T : class
    {
        private IEnumerable<T> values = null; 
        private IEnumerator<T> enumerator = null;

        //List of all public fields in <T>
        private List<ColumnInfo> _properties = new List<ColumnInfo>();

        public MyDataReader(IEnumerable<T> enumerator, IEnumerable<ColumnInfo> properties)
        {
            this.values = enumerator;
            this.enumerator = enumerator.GetEnumerator();


            _properties = properties.ToList();
        }

        #region IDataReader Interface Implementation
        //Advances the enumerator to the next record.
        public bool Read()
        {
            return enumerator.MoveNext();
        }

        //Closes the enumerator Object.
        public void Close()
        {
            enumerator.Dispose();
        }

        #endregion

        #region IDataReader Not Implemented
        public bool NextResult()
        {
            throw new NotImplementedException();
        }

        public int Depth
        {
            get { throw new NotImplementedException(); }
        }

        public DataTable GetSchemaTable()
        {
            throw new NotImplementedException();
        }

        public bool IsClosed
        {
            get { throw new NotImplementedException(); }
        }


        public int RecordsAffected
        {
            get { return this.values.Count(); }
        }

        #endregion

        #region IDisposable Interface Implementation

        public void Dispose()
        {
            Close();
        }

        #endregion

        #region IDataRecord Interface Implementation
        //Gets the number of fields (columns) in the current object.
        public int FieldCount
        {
            get { return _properties.Count; }
        }

        //Gets the name of the current field.
        public string GetName(int i)
        {
            return _properties[i].ColumnName;
        }

        //Return the value of the current field.
        public object GetValue(int i)
        {
            return enumerator.Current.GetType().GetProperty(GetName(i)).GetValue(enumerator.Current, null);
        }

        // Gets the System.Type information corresponding to the type of
        public Type GetFieldType(int i)
        {
            
            return _properties[i].Type;
        }

        #endregion

        #region IDataRecord Members             

        public bool GetBoolean(int i)
        {
            throw new NotImplementedException();
        }

        public byte GetByte(int i)
        {
            throw new NotImplementedException();
        }

        public long GetBytes(int i, long fieldOffset,
byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public char GetChar(int i)
        {
            throw new NotImplementedException();
        }

        public long GetChars(int i, long fieldoffset,
char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public IDataReader GetData(int i)
        {
            throw new NotImplementedException();
        }

        public string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }

        public DateTime GetDateTime(int i)
        {
            throw new NotImplementedException();
        }

        public decimal GetDecimal(int i)
        {
            throw new NotImplementedException();
        }

        public double GetDouble(int i)
        {
            throw new NotImplementedException();
        }

        public float GetFloat(int i)
        {
            throw new NotImplementedException();
        }

        public Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }

        public short GetInt16(int i)
        {
            throw new NotImplementedException();
        }

        public int GetInt32(int i)
        {
            throw new NotImplementedException();
        }

        public long GetInt64(int i)
        {
            throw new NotImplementedException();
        }

        public int GetOrdinal(string name)
        {
            return _properties.FindIndex(v=>v.ColumnName == name);
        }

        public string GetString(int i)
        {
            throw new NotImplementedException();
        }

        public int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        public bool IsDBNull(int i)
        {
            throw new NotImplementedException();
        }

        public object this[string name]
        {
            get { throw new NotImplementedException(); }
        }

        public object this[int i]
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }

}
