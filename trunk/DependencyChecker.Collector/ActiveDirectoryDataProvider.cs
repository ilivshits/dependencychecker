﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DependencyChecker.Collector.Base;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Common;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Collector
{
    public class ActiveDirectoryDataProvider : BaseDataProvider
    {
        private static readonly List<Regex> BasicPattern = new List<Regex>
        {
            new Regex(@"^(" + Regex.Escape(UserDomainName) + @"\\)?(?<name>[a-zA-Z0-9]+)$", RegexOptions.IgnoreCase | RegexOptions.Compiled),
            new Regex(@"'(" + Regex.Escape(UserDomainName) + @"\\)?(?<name>[a-zA-Z0-9]+)'", RegexOptions.IgnoreCase | RegexOptions.Compiled),
            new Regex(@"""(" + Regex.Escape(UserDomainName) + @"\\)?(?<name>[a-zA-Z0-9]+)""", RegexOptions.IgnoreCase | RegexOptions.Compiled)
        };
        public ActiveDirectoryDataProvider(IServerProvider serverProvider)
            : base(serverProvider)
        {
        }

        #region Overrides of BaseDataProvider

        public override string Name
        {
            get { return PersonEntity.ProviderName; }
        }

        public override List<IEntity> Search()
        {
            var users = GetAllDomainUsers();
            users.AddRange(GetAllServiceAccounts());
            var server = new PersonEntity(UserDomainName, EntityType.Server)
            {
                ScopeChildren = users
            };
            users.ForEach(u => { u.Scope = server; });
            return new List<IEntity>{ server }; 
        }

        protected override List<Regex> ItemSearchPattern
        {
            get { return BasicPattern; }
        }

        public List<IEntity> GetAllServiceAccounts()
        {
            return GetUsers("OU=SysAccounts");
        }

        public List<IEntity> GetAllDomainUsers()
        {
            return GetUsers("OU=People");
        }

        public List<IEntity> GetUsers(string query)
        {
            var allUsers = new List<IEntity>();

            var domainString = String.Join(String.Empty, DomainName.Split(new []{'.'}).Select(s => ",DC=" + s).ToArray());
            var searchRoot = new DirectoryEntry("LDAP://" + query + domainString);
            LoggerHelper.Log.DebugFormat("Scanning domain '{0}'...", UserDomainName);
            try
            {
                using (var search = new DirectorySearcher(searchRoot)
                                {
                                    Filter = "(&(objectClass=user)(objectCategory=person))"
                                })
                {
                    search.PropertiesToLoad.Add("*");
                    search.SizeLimit = 40000;
                    search.PageSize = 40000;

                    var resultCol = search.FindAll();
                    for (var counter = 0; counter < resultCol.Count; counter++)
                    {
                        var result = resultCol[counter];

                        if (result.Properties.Contains("samaccountname") && result.Properties.Contains("name"))
                        {
                            allUsers.Add(new PersonEntity((string)result.Properties["name"][0], (string)result.Properties["samaccountname"][0], UserDomainName));
                        }
                    }
                }
            }
            catch (DirectoryServicesCOMException e)
            {
                Debug.Print(e.Message);
                throw;
            }
            return allUsers;
        }

        private string _domainName;
        public string DomainName
        {
            get
            {
                if (String.IsNullOrEmpty(_domainName))
                {
                    _domainName = Domain.GetComputerDomain().Name;
                }
                return _domainName;
            }
        }

        private static string _userDomainName;
        public static string UserDomainName
        {
            get
            {
                if (String.IsNullOrEmpty(_userDomainName))
                {
                    _userDomainName = Environment.UserDomainName;
                }
                return _userDomainName;
            }
        }
        #endregion
    }
}
