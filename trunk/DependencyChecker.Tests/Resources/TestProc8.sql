﻿


CREATE PROCEDURE [cfg].[GetInvestorTemplateStressTests]
  @aexeo_trader_id VARCHAR(500) = '',
  @meta_fund_id    VARCHAR(500) = '',
  @strategy        VARCHAR(max) = NULL,
  @live            BIT = 1,
  @asofdate        DATETIME = NULL,
  @run             BIT = 1
AS
	BEGIN
		SET NOCOUNT ON;
      
		----FAMF
		--declare @aexeo_trader_id VARCHAR(500) = '1, 7, 96, 129, 137, 141, 145, 148, 168',
		--@meta_fund_id    VARCHAR(500) = '48',
		--@strategy        VARCHAR(max) = NULL,
		--@live            BIT = 1,
		--@asofdate        DATETIME = '2014-02-28',
		--@run             BIT = 1
		
		----FMF
		--declare @aexeo_trader_id VARCHAR(500) = '1, 4, 96, 120, 123, 131, 141, 146, 147, 148, 149, 150, 165, 166, 168',
		--@meta_fund_id    VARCHAR(500) = '38',
		--@strategy        VARCHAR(max) = NULL,
		--@live            BIT = 0,
		--@asofdate        DATETIME = '2014-03-31',
		--@run             BIT = 1

		IF (@run=0)
		RETURN;

		-- for dates before '3/30/2014' use only historical scenarios from RiskMetrics
		declare @cutoffDate datetime = '3/30/2014'
		declare @historical int = 0
		if @asofdate > @cutoffDate set @historical = 1
      
		print 'As of date: ' + convert(varchar, @asofdate, 112)
		print 'Cutoff date: ' + convert(varchar, @cutoffDate, 112)
      
		SELECT *
		INTO #tmpReporting1
		FROM 
		(
			SELECT RowID,
				Trader,
				Legal_Entity_Name,
				Value10,
				[FIG_Quantity],
				Acct,
				Type,
				FIG_Value_Date,
				Long_Security_Name,
				ch.notional
			FROM   [Mips].[dbo].[z_mips_reporting1] r1
				JOIN Mips.dbo.v_current_holdings ch
					ON r1.RowID=ch.holding_id
			WHERE  @live=1
			
			UNION ALL
			
			SELECT RowID,
				Trader,
				Legal_Entity_Name,
				Value10,
				[FIG_Quantity],
				Acct,
				Type,
				FIG_Value_Date,
				Long_Security_Name,
				ch.notional
			FROM   [MipsHist].[dbo].[z_mips_reporting1] r1
				JOIN MipsHist.dbo.v_current_holdings ch
					ON r1.RowID=ch.holding_id
					AND r1.as_of_date=ch.as_of_date
			WHERE  @live=0
				AND r1.as_of_date=@asofdate
		) A                     

		DECLARE @stressDate DATETIME
		print 'Legacy Stress Tests date: ' + convert(varchar, @stressDate, 112)

		SELECT @stressDate=MAX(BusDate)
		FROM   [RiskMetrics].[dbo].[v_StressTestShocks_Dates]
		WHERE  BusDate<=@asofdate
		OPTION(MAXRECURSION 0)

		SELECT st.Id,
			 st.Name,
			 isnull(alias.Alias, st.Name) [Alias],
			 shocks.StressTestId,
			 shocks.TID,
			 shocks.ValueDate,
			 shocks.Repo,
			 shocks.UnitQuantity,
			 shocks.MarketValueChange,
			 shocks.AsOfDate
		INTO   #tmpStressTest
		FROM   RiskMetrics.dbo.v_StressTestShock_Values shocks
			 INNER JOIN RiskMetrics.dbo.StressTest st
			   ON st.Id=shocks.StressTestId
			 LEFT JOIN RiskMetrics.dbo.StressTestAlias alias
			   ON st.Id=alias.Id
		WHERE  AsOfDate=@stressDate
			 --and Repo is null
			 AND st.Id IN (7, -- Russian Crisis '98 1D Predictive
						   12, -- Sept 11th 1D
						   15, -- Black Monday '87 1D
						   16, -- Asian Crisis '97 1D
						   19, -- Equity -10%
						   20, -- Equity +10%
						   44, -- Gulf War '90 1D
						   64, -- SnP 500 -10%
						   69, -- SnP 500 +10%
						   90, -- Crude -20%
						   92, -- Crude -10%
						   111, -- Crude +10%
						   113, -- Crude +20%
						   117, -- Gold -15%
						   118, -- Gold -10%
						   137, -- Gold +10%
						   138, -- Gold +15%
						   143, -- DXY -15%
						   149, -- DXY -5%
						   158, -- DXY +5%
						   164, -- DXY +15%
						   613, -- SPGSAG -15%
						   614, -- SPGSAG +15%
						   615, -- SPGSCI -10%
						   616, -- SPGSCI +10%
						   617, -- SPGSEN -20%
						   618, -- SPGSEN +20%
						   619, -- SPGSIN -10%
						   620, -- SPGSIN +10%
						   621, -- Volatility -10%
						   624, -- 25bp steepening (aka -25bp)
						   625, -- 25bp flattening (aka +25bp)
						   705, -- HY +8%
						   706, -- HY -8%
						   722, -- IR +50bp
						   723, -- IR -50bp
						   743, -- Lehman Default (2008)
						   785, -- Parallel +50bp Generalized
						   786, -- Parallel -50bp Generalized
						   850, -- Wheat -10%
						   863, -- Wheat +10%
						   977, -- Commodity +10%
						   978, -- Commodity -10%
						   979, -- Currency +15%
						   980, -- Currency -15%
						   1051, -- Nikkei +10%
						   1052, -- Nikkei -10%
						   1057, -- MSCI AC World +5%
						   1058-- MSCI AC World -5%
						  )

		SELECT A.Id [ShockId],
			 A.Name [ShockName],
			 A.Alias,
			 Scenario = '',
			 Shock = '',
			 sum(isnull(A.MarketValueChange, 0)/A.UnitQuantity*r1.notional) [ShockValue]
		into #tmp
		FROM   #tmpReporting1 r1
			 JOIN foffice.v_trader v_t
			   ON v_t.abbrev=r1.Trader
			 JOIN foffice.v_legalentity v_l
			   ON v_l.le_name=r1.Legal_Entity_Name
			 LEFT JOIN #tmpStressTest A
			   ON r1.Value10=A.TID
				  AND isnull(A.Repo, '')=''
				  AND CASE
						WHEN isnull(r1.FIG_Value_Date, '')=''
						THEN '1900/1/1'
						ELSE r1.FIG_Value_Date
					  END=CASE
							WHEN isnull(A.ValueDate, '')=''
							THEN '1900/1/1'
							ELSE A.ValueDate
						  END
		WHERE  r1.[FIG_Quantity]!=0
			 AND v_t.aexeo_trader_id IN (SELECT *
										 FROM   dbo.fnFunSplit(@aexeo_trader_id, ','))
			 AND v_l.fundId IN (SELECT *
								FROM   dbo.fnFunSplit(@meta_fund_id, ','))
			 AND (@strategy IS NULL
				   OR r1.Acct IN (SELECT Items COLLATE Latin1_General_BIN2
								  FROM   dbo.fnFunSplit(@strategy, ',')))
		GROUP  BY A.Id,
				A.Name,
				A.Alias;	    
	      
		with r as
		(
			select shockId
				, scenarioDescription
				, shockDescription
				, up = case when direction = 'up' then ShockValue else 0 end
				, down = case when direction = 'down' then ShockValue else 0 end
				, shockValue = case when historical =1 then ShockValue else 0 end
				, sortOrder
				, historical
			from #tmp r
			left join P1ReportingG2.cfg.StressTestScenarios s on r.ShockId = s.shockId
		) 
		select 
				label = scenarioDescription
				, shock = shockDescription
				, up = SUM(up)
				, down =sum(down)
				, shockValue = sum(shockValue)
				, sortOrder
				, historical
		into #result
		from r
		where scenarioDescription is not null
		and (historical = @historical or @historical = 0) 
		group by scenarioDescription
		, shockDescription
		, sortOrder
				, historical
		order by sortOrder
			
		if @asofdate > @cutoffDate
		begin
		
			set @stressDate=[dbo].[fnBusDayOrPrev] (@asofdate)
			print 'New stress Tests date: ' + convert(varchar, @stressDate, 112)
					
			select *
			into #stressTests
			from
			(           
				select 
					RowID
					, FIG_Asset_Class
					, FX_Shock_Curr
					,[sP&L_Comm_+10%]
					,[sP&L_Comm_-10%]
					,[sP&L_Equity_+10%]
					,[sP&L_Equity_-10%]
					,[sP&L_Credit_-100bps]
					,[sP&L_Credit_-50%]
					,[sP&L_Credit_+100bps]
					,[sP&L_Credit_+50%]
					,[sP&L_Rates_+100bps]
					,[sP&L_Rates_-100bps]
					,[sP&L_USD_+5%]
					,[sP&L_USD_-5%]
					,[sP&L_USD_+2%]
					,[sP&L_USD_-2%]
				from Mips.dbo.z_mips_StressTest
				where @live = 1
				union all
				select 
				RowID
				, FIG_Asset_Class
				, FX_Shock_Curr
				,[sP&L_Comm_+10%]
				,[sP&L_Comm_-10%]
				,[sP&L_Equity_+10%]
				,[sP&L_Equity_-10%]
				,[sP&L_Credit_-100bps]
				,[sP&L_Credit_-50%]
				,[sP&L_Credit_+100bps]
				,[sP&L_Credit_+50%]
				,[sP&L_Rates_+100bps]
				,[sP&L_Rates_-100bps]
				,[sP&L_USD_+5%]
				,[sP&L_USD_-5%]
				,[sP&L_USD_+2%]
				,[sP&L_USD_-2%]
				from MipsHist.dbo.z_mips_StressTest
				where @live = 0
				and as_of_date = @stressDate
			)st
			
			select
				st.*
			into #stressTestResults
			from #stressTests st
				join #tmpReporting1 r1 on r1.RowID = st.RowID
				JOIN foffice.v_trader v_t
					ON v_t.abbrev=r1.Trader
					JOIN foffice.v_legalentity v_l
					ON v_l.le_name=r1.Legal_Entity_Name
				WHERE  r1.[FIG_Quantity]!=0
					AND v_t.aexeo_trader_id IN (SELECT *
						FROM   dbo.fnFunSplit(@aexeo_trader_id, ','))
					AND v_l.fundId IN (SELECT *
						FROM   dbo.fnFunSplit(@meta_fund_id, ','))
					AND (@strategy IS NULL
					OR r1.Acct IN (SELECT Items COLLATE Latin1_General_BIN2
						FROM   dbo.fnFunSplit(@strategy, ','))) 
			
			insert into #result
			select 
				'Equity'
				, shock = '10%'
				, up = isnull(Sum([sP&L_Equity_+10%]), 0)
				, down = isnull(Sum([sP&L_Equity_-10%]), 0)
				, shockValue = 0
				, sortOrder = 0
				, historical = 0
			from #stressTestResults
			union
			select 
				'Commodity'
				, shock = '10%'
				, up = isnull(Sum([sP&L_Comm_+10%]), 0)
				, down = isnull(Sum([sP&L_Comm_-10%]), 0)
				, shockValue = 0
				, sortOrder = 0
				, historical = 0
			from #stressTestResults
			union
				select 
				'Credit'
				, shock = '100bps'
				, up = isnull(Sum([sP&L_Credit_+100bps]), 0)
				, down = isnull(Sum([sP&L_Credit_-100bps]), 0)
				, shockValue = 0
				, sortOrder = 0
				, historical = 0
			from #stressTestResults
			union
				select 
				'Rates'
				, shock = '100bps'
				, up = isnull(Sum([sP&L_Rates_+100bps]), 0)
				, down = isnull(Sum([sP&L_Rates_-100bps]), 0)
				, shockValue = 0
				, sortOrder = 0
				, historical = 0
			from #stressTestResults
			union
				select 
				'USD'
				, shock = '2%/5%**'
				, up = isnull(Sum(CASE
									WHEN RiskDataHist.[dbo].[fn_IsPeggedCurr] (FX_Shock_Curr)=1 THEN [sP&L_USD_+2%]
									ELSE [sP&L_USD_+5%]
								  END), 0)
				, down = isnull(Sum(CASE
									WHEN RiskDataHist.[dbo].[fn_IsPeggedCurr] (FX_Shock_Curr)=1 THEN [sP&L_USD_-2%]
									ELSE [sP&L_USD_-5%]
								  END), 0)
				, shockValue = 0
				, sortOrder = 0
				, historical = 0
			from #stressTestResults				                      
			
			drop table #stressTests
			drop table #stressTestResults			
	  
	  end
	  
	  select * from #result
	  order by sortOrder, label
	  
      drop table #tmp         
      drop table #result
      drop table #tmpReporting1
      drop table #tmpStressTest          
      
  END

