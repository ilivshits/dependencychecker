
CREATE view CreditDefaultSwapCoupon as
select s.sid,
	[date] = dateadd(day, 1, a.date),
	coupon = cds.coupon / 360 * (a.accruedDays + 1),
	periodDays = accruedDays + 1
from [Security] s
join [CreditDefaultSwap] cds on s.sid = cds.sid
join [CreditDefaultSwapAccrual] a
	on a.currencySid = case when s.currencySid in (104,130) then s.currencySid else 0 end
	and daysToNextCashFlow = 1
where dateadd(day, 1, a.date) between s.startDate and cds.terminationDate and
	a.couponFrequencyId = cds.pay_frequencyId

