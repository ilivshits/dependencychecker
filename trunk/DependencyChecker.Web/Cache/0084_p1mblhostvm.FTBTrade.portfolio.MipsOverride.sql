create view [portfolio].[MipsOverride] as
select sid = r.inputValue, quantityMultiplier = vQty.value_Decimal, priceMultiplier = vPx.value_Decimal
from tagging.LookupTable t
join tagging.LookupTableRow r on t.lookupTableId = r.lookupTableId
join tagging.LookupTableColumn cQty on t.lookupTableId = cQty.lookupTableId and cQty.perspective = 'Quantity Multiplier'
left join tagging.LookupTableCell vQty on r.lookupTableRowId = vQty.lookupTableRowId and cQty.lookupTableColumnId = vQty.lookupTableColumnId
join tagging.LookupTableColumn cPx on t.lookupTableId = cPx.lookupTableId and cPx.perspective = 'Price Multiplier'
left join tagging.LookupTableCell vPx on r.lookupTableRowId = vPx.lookupTableRowId and cPx.lookupTableColumnId = vPx.lookupTableColumnId
where name = 'Portfolio - Mips Override'


