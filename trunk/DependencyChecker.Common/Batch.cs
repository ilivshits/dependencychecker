﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyChecker.Common
{
    public class Batch
    {
        public int Id { get; set; }
        public String Date { get; set; }
        public string Username { get; set; }
        public BatchResult Status { get; set; }
    }

    public enum BatchResult
    {
        Failed,
        Success
    }
}
