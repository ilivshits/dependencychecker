
create view [fortress].[v_BusinessDayConvention_Pivot] as
	select
		 0 as NotSpecified,
		 1 as NoDateAdjustment,
		 2 as NextGoodBusinessDay,
		 3 as PreviousGoodBusinessDay,
		 4 as ModifiedFollowingBusinessDay,
		 5 as EndOfMonthNoAdjustment,
		 6 as EndOfMonthNextGoodBusinessDay,
		 7 as EndOfMonthPreviousGoodBusinessDay,
		 8 as TwoBusinessDaysPriorToThirdWednesday,
		 9 as EndOfMonthModifiedFollowingBusinessDay,
		10 as ThirdWednesday,
		11 as EndOfMonthIgnoreLeapYears


