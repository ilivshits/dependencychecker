
CREATE PROCEDURE [fortress].[Security_Edit]
	@sid bigint,
	@ticker	varchar(50),
	@description varchar(150),
	@currencySid bigint,
	@exchangeId int,
	@securityPrimaryTypeId int,
	@securitySubTypeId int,		
	@startDate datetime,
	@endDate datetime,
	@priceMultiplier float,
	@tradableFlag bit,
	@priceableFlag bit,
	@primaryFlag bit,
	@underlying_sid	bigint,
	@issuerId int,
	@lotSizeInitial float,
	@lotSizeIncrement float,
		
	--- Identifiers.
	@idBloombergCodeShort varchar(100),
	@idBloombergCodeLong varchar(100),
	@idCusip varchar(100),
	@idSedol varchar(100),
	@idIsin varchar(100),
	@idRed6 varchar(100),
	@idRed9 varchar(100),
	@idTradeId varchar(100),
	@idBloombergCdsTicker5Y varchar(100),
	@idBloombergGlobal varchar(100),
	@idBloombergUnique varchar(100),
	@idBelgium varchar(100),
	@idCedelEuroclear varchar(100),
	@idDutch varchar(100),
	@idFrench varchar(100),
	@idValoren varchar(100),
	@idWertpapier varchar(100),
	@idOcc varchar(100),
	@idOpra varchar(100),
	@idBloombergTickerAndExchange varchar(100),
	@idAustrian varchar(100),
	@idCedel varchar(100),
	@idDanish varchar(100),
	@idEuroclear varchar(100),
	@idItaly varchar(100),
	@idJapan varchar(100),
	@idLuxembourg varchar(100),
	@idNorway varchar(100),
	@idSpain varchar(100),
	@idSwedish varchar(100),
	@idXtrakter varchar(100),

	--- Restrictions.
	@restriction144A bit,
	@restrictionRegS bit,

	--- Auditing.
	@application_source varchar(100),
	
	--- TODO: Move when appropriate.
	@alternateDescription varchar(150) = null,
	
	--- TODO: Move when appropriate.
	@idBloombergCodeLocalExchange varchar(100) = null,

	--- Industry classifications
	@bloombergIndustrySectorCode varchar(100) = null,
	@bloombergIndustryGroupCode varchar(100) = null,
	@bloombergIndustrySubgroupCode varchar(100) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--Update the record.
	update 
		FTBTrade.dbo_Security
	set 
		ticker = @ticker,
		description = @description,
		currencySid = @currencySid,
		exchangeId = @exchangeId,
		securityPrimaryTypeId = @securityPrimaryTypeId,	
		securitySubTypeId = @securitySubTypeId,			
		startDate = @startDate,
		endDate = @endDate,
		--priceMultiplier = case @priceMultiplier when 0.0 then priceMultiplier else @priceMultiplier end,
		tradableFlag = @tradableFlag,
		primaryFlag = @primaryFlag,
		underlying_sid = @underlying_sid,
		issuerId  = @issuerId,
		lotSizeInitial = @lotSizeInitial,
		lotSizeIncrement = @lotSizeIncrement,
		alternateDescription = coalesce(@alternateDescription, alternateDescription),
		application_source = @application_source	
	where 
		sid = @sid;	
	
	--- Create mini security identifier table.
	select
		t.*
	into
		#ids
	from
		(
			select i.[Bloomberg Code - short] as identifierTypeId, @idBloombergCodeShort as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergCodeShort is not null union
			select i.[Bloomberg Code - long] as identifierTypeId, @idBloombergCodeLong as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergCodeLong is not null union
			select i.CUSIP as identifierTypeId, @idCusip as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idCusip is not null union
			select i.SEDOL as identifierTypeId, @idSedol as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idSedol is not null union
			select i.ISIN as identifierTypeId, @idIsin as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idIsin is not null union
			select i.RED6 as identifierTypeId, @idRed6 as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idRed6 is not null union
			select i.RED9 as identifierTypeId, @idRed9 as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idRed9 is not null union
			select i.TID as identifierTypeId, @idTradeId as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idTradeId is not null union
			select i.[Bloomberg CDS Ticker 5Y] as identifierTypeId, @idBloombergCdsTicker5Y as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergCdsTicker5Y is not null union
			select i.[Bloomberg Global] as identifierTypeId, @idBloombergGlobal as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergGlobal is not null union
			select i.[Bloomberg Unique] as identifierTypeId, @idBloombergUnique as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergUnique is not null union
			select i.Belgium as identifierTypeId, @idBelgium as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idBelgium is not null union
			select i.[Cedel / Euroclear] as identifierTypeId, @idCedelEuroclear as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idCedelEuroclear is not null union
			select i.Dutch as identifierTypeId, @idDutch as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idDutch is not null union
			select i.French as identifierTypeId, @idFrench as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idFrench is not null union
			select i.Valoren as identifierTypeId, @idValoren as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idValoren is not null union
			select i.Wertpapier as identifierTypeId, @idWertpapier as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idWertpapier is not null union
			select i.OCC as identifierTypeId, @idOcc as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idOcc is not null union
			select i.OPRA as identifierTypeId, @idOpra as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idOpra is not null union
			select i.[Bloomberg Ticker and Exchange] as identifierTypeId, @idBloombergTickerAndExchange as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idBloombergTickerAndExchange is not null union
			select i.Austrian as identifierTypeId, @idAustrian as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idAustrian is not null union
			select i.Cedel as identifierTypeId, @idCedel as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idCedel is not null union
			select i.Danish as identifierTypeId, @idDanish as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idDanish is not null union
			select i.Euroclear as identifierTypeId, @idEuroclear as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idEuroclear is not null union
			select i.Italy as identifierTypeId, @idItaly as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idItaly is not null union
			select i.Japan as identifierTypeId, @idJapan as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idJapan is not null union
			select i.Luxembourg as identifierTypeId, @idLuxembourg as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idLuxembourg is not null union
			select i.Norway as identifierTypeId, @idNorway as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idNorway is not null union
			select i.Spain as identifierTypeId, @idSpain as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idSpain is not null union
			select i.Swedish as identifierTypeId, @idSwedish as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idSwedish is not null union
			select i.Xtrakter as identifierTypeId, @idXtrakter as code from FTBTrade.dbo_IdentifierType_Transposed as i where @idXtrakter is not null	
		) as t
		
		
	---- Update the securityidentifier
	merge 
		FTBTrade.dbo_SecurityIdentifier as si
	using 
		#ids as id
			on si.identifierTypeId = id.identifierTypeId and si.sid = @sid
	when matched and si.code <> id.code then
		update set code = id.code 
	when not matched then 
		insert (sid, identifierTypeId, code, application_source) values(@sid, id.identifierTypeId, id.code, @application_source);
 		
	-- Delete the securityidentifier if it does not exist in new data 		
	merge 
		FTBTrade.dbo_SecurityIdentifier as si
	using 
		#ids as id
			on id.identifierTypeId not in (select identifierTypeId from #ids) and si.sid = @sid
	when matched then
		delete;

	--- Drop the table.
	drop table #ids
	
	--- Get the date.
	declare @now date = getdate()
	
	--- Update the restrictions.
	---
	--- If the flag is null then do nothing.
	---
	--- If the flag is true, then
	---		- If the time is in a window, do nothing
	---		- If the time is not in a window, create a new window.
	---
	--- If the flag is false, then
	---		- If the time is in a window, then close the window
	---		- If the flag is not in a window, then do nothing.

	--- True case.
	--- Insert the restrictions if they exist, starting from this date.
	insert into FTBTrade.dbo_SecurityRestriction (
		sid, restrictionTypeId, startDate, endDate, application_source
	)
	select
		@sid as sid, rt.restrictionTypeId, @now as startDate, '9999-12-31' as endDate,
		@application_source as application_source
	from
		(
			select rt._144A as restrictionTypeId from fortress.v_RestrictionType_Pivot as rt where ISNULL(@restriction144A, 0) <> 0 union
			select rt.RegS as restrictionTypeId from fortress.v_RestrictionType_Pivot as rt where ISNULL(@restrictionRegS, 0) <> 0
		) as rt
	where
		not exists (
			select 
				sr.* 
			from 
				FTBTrade.dbo_SecurityRestriction as sr with (nolock) 
			where 
				sr.sid = @sid and
				sr.restrictionTypeId = rt.restrictionTypeId and
				@now between sr.startDate and sr.endDate
		)
		
	--- False case.
	--- Update if the flag is not null, is false, and in a window.
	update
		FTBTrade.dbo_SecurityRestriction
	set
		endDate = @now,
		application_source = @application_source
	from
		FTBTrade.dbo_SecurityRestriction as sr with (nolock)
			inner join (
				select rt.[144a] as restrictionTypeId from FTBTrade.dbo_RestrictionType_Transposed as rt where @restriction144A is not null and @restriction144A = 0 union
				select rt.REGS as restrictionTypeId from FTBTrade.dbo_RestrictionType_Transposed as rt where @restrictionRegS is not null and @restrictionRegS = 0
			) as rt on rt.restrictionTypeId = sr.restrictionTypeId
	where
		sr.sid = @sid and
		@now between sr.startDate and sr.endDate		

	--- Update Bloomberg industry classification.
	exec [FTBTrade].[dbo_SecurityIndustryClassification_Upsert] @sid, @application_source,
		@bloombergIndustrySectorCode, @bloombergIndustryGroupCode, @bloombergIndustrySubgroupCode
END



