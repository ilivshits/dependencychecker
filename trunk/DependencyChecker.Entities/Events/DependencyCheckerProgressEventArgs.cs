﻿using System;

namespace DependencyChecker.Entities.Events
{
    public class DependencyCheckerProgressEventArgs : EventArgs
    {
        /// <summary>
        /// Current operation progress.
        /// Has values from 0.0f to 1.0f
        /// </summary>
        public float Progress { get; set; }
        public String Message { get; set; }
    }
}