﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using DependencyChecker.Entities.Interfaces;
using log4net.Config;

namespace DependencyChecker.Entities
{
    public class SearchContent
    {
        private static readonly Dictionary<EntityProviderType, Regex> _definitionExpression =
            new Dictionary<EntityProviderType, Regex>{ 
            {EntityProviderType.SQL, 
            new Regex(@"DECLARE[ \t\r\n]+(?<var>@[a-z0-9]+)[ \t\r\n]+N?VARCHAR[ \t\r\n]*\([0-9]+\)[ \t\r\n]*" +
                        @"=[ \t\r\n]*N?(?<value>'([^']*('[ \t\r\n]*\+[ \t\r\n]*((?<ref>@[a-z0-9]+)[ \t\r\n]*\+[ \t\r\n]*)?'[ \t\r\n]*)*)*')", RegexOptions.IgnoreCase | RegexOptions.Compiled)},
            {EntityProviderType.Perl, 
            new Regex(@"my([ \t]+)?(?<var>\$[a-z0-9_]+)([ \t]+)?=([ \t]+)?(qq([ \t]+)?{(?<value>[^}]+)}|(?<value>('[^']+'|""[^""]+"")))([ \t]+)?;([ \t]+)?", 
                RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled)}
        };

        private static readonly Dictionary<EntityProviderType, Regex> _assignmentExpression =
            new Dictionary<EntityProviderType, Regex>{ 
            {EntityProviderType.SQL, 
            new Regex(@"SET[ \t\r\n]+(?<var>@[a-z0-9]+)[ \t\r\n]*" +
                        @"=[ \t\r\n]*N?(?<value>'([^']*('[ \t\r\n]*\+[ \t\r\n]*((?<ref>@[a-z0-9]+)[ \t\r\n]*\+[ \t\r\n]*)?'[ \t\r\n]*)*)*')", RegexOptions.IgnoreCase | RegexOptions.Compiled)},
            {EntityProviderType.Perl, 
            new Regex(@"^([ \t]+)?(?<var>\$[a-z0-9_]+)([ \t]+)?(?<concat>\.)?=([ \t]+)?(qq([ \t]+)?{(?<value>[^}]+)}|(?<value>('[^']+'|""[^""]+?((?<ref>(\$[a-z0-9_]+|\${[a-z0-9_]+}))[^""]+)*"")))([ \t]+)?;([ \t]+)?", 
                RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled)}
        };

        private static readonly Dictionary<EntityProviderType, Regex> _executionExpression =
            new Dictionary<EntityProviderType, Regex>{ 
            {EntityProviderType.SQL, 
            new Regex(@"EXEC(UTE)?([ \t\r\n]+(sp_executesql|sp_sqlexec))?[ \t\r\n]+\(?(?<ref>@[a-z0-9]+)", RegexOptions.IgnoreCase | RegexOptions.Compiled)}
        };

        private static readonly Dictionary<EntityProviderType, Regex> _nonExecutionExpression =
            new Dictionary<EntityProviderType, Regex>{ 
            {EntityProviderType.SQL, 
            new Regex(@"^PRINT.*$", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Multiline)}
        };

        public String Text { get; set; }
        public IEntity Scope { get; set; }
        public String ScopeName { get; set; }
        public EntityProviderType ContentType { get; set; }

        public String CreatedBy { get; set; }
        public String CreatedDate { get; set; }
        public String ModifiedBy { get; set; }
        public String ModifiedDate { get; set; }

        private List<String> GetReplacementSearchStr(EntityProviderType type, string varName)
        {
            if (type == EntityProviderType.SQL)
            {
                return new List<string>{ varName };
            }
            if (type == EntityProviderType.Perl)
            {
                return new List<string> { varName, String.Format("${{{0}}}", varName.Replace("$", String.Empty)) };
            }
            return new List<string> { varName };
        }

        private String FormatVarName(string name)
        {
            if (ContentType == EntityProviderType.Perl)
            {
                name = name.Replace("{", String.Empty).Replace("}", String.Empty);
                if (!name.StartsWith("$"))
                {
                    return "$" + name;
                }
            }
            else if (ContentType == EntityProviderType.SQL)
            {
                if (!name.StartsWith("@"))
                {
                    return "@" + name;
                }
            }
            return name;
        }

        public void ReplaceMatch(EntityMatch match, string replacement, char? stopSymbol = ')')
        {
            var result = new StringBuilder();
            var matchLength = match.MatchText.Length;

            for (int i = 0, lastIndex = 0; i < match.Indexes.Count; i++)
            {
                result.Append(Text.Substring(lastIndex, match.Indexes[i] - lastIndex));
                result.Append(replacement);
                if (stopSymbol == null)
                {
                    lastIndex = match.Indexes[i] + matchLength;
                }
                else
                {
                    lastIndex = Text.IndexOf(stopSymbol.Value, match.Indexes[i] + matchLength) + 1;
                }
                if (i == match.Indexes.Count - 1)
                {
                    result.Append(Text.Substring(lastIndex, Text.Length - lastIndex));
                }
            }

            Text = result.ToString();
        }

        private string AddQuotes(string input, string quoteChar = "\"")
        {
            if (input.EndsWith("'") || input.EndsWith("\""))
            {
                return input;
            }
            return quoteChar + input + quoteChar;
        }

        public void ProcessVariables()
        {
            if (!_assignmentExpression.ContainsKey(ContentType))
            {
                return;
            }
            var executionText = _nonExecutionExpression.ContainsKey(ContentType)
                ? _nonExecutionExpression[ContentType].Replace(Text, String.Empty)
                : Text;

            var definitionMatches = _definitionExpression[ContentType].Matches(executionText).Cast<Match>().ToList();
            var assignmentMatches = _assignmentExpression[ContentType].Matches(executionText).Cast<Match>().ToList();
            if (definitionMatches.Count == 0 && assignmentMatches.Count == 0)
            {
                return;
            }

            var definitionEntityMatches = definitionMatches.Select(m => new EntityMatch
            {
                MatchText = m.Value,
                Indexes = new List<int> { m.Index }
            }).ToList();

            var assignmentExpressions = assignmentMatches.Select(a => new ContentExpression
            {
                Match = a,
                VarName = a.Groups["var"].Value,
                References = a.Groups["ref"].Captures.Cast<Capture>().Select(c => c.Value).GroupBy(FormatVarName).ToDictionary(c => c.Key, c => String.Empty),
                VarExpression = a.Groups["value"].Value,
                Value = a.Groups["value"].Value,
                IsConcat = a.Groups["concat"].Success
            }).ToList();
            assignmentExpressions.AddRange(definitionMatches.Select(a => new ContentExpression
            {
                Match = a,
                VarName = a.Groups["var"].Value,
                References = new Dictionary<string, string>(),
                VarExpression = a.Groups["value"].Value,
                Value = a.Groups["value"].Value,
                IsConcat = false
            }).ToList());

            if (_executionExpression.ContainsKey(ContentType))
            {
                var executionMatches = _executionExpression[ContentType].Matches(executionText).Cast<Match>().ToList();
                assignmentExpressions.AddRange(executionMatches.Select(a => new ContentExpression
                {
                    Match = a,
                    VarName = String.Empty,
                    References =
                        a.Groups["ref"].Captures.Cast<Capture>()
                            .Select(c => c.Value)
                            .GroupBy(FormatVarName)
                            .ToDictionary(c => c.Key, c => String.Empty),
                    VarExpression = a.Groups["value"].Value,
                    Value = a.Value,
                    IsConcat = false
                }).ToList());
            }

            assignmentExpressions = assignmentExpressions.OrderBy(a => a.Match.Index).ToList();

            var consts = definitionMatches
                .Where(m => !m.Groups["concat"].Success && !m.Groups["ref"].Success)
                .GroupBy(m => m.Groups["var"].Value)
                .ToDictionary(m => m.Key, m => AddQuotes(m.Last().Groups["value"].Value));

            for (var i = 0; i < assignmentExpressions.Count; i++)
            {
                var keys = new List<String>(assignmentExpressions[i].References.Keys);
                foreach (var reference in keys)
                {
                    var lastAssignment = assignmentExpressions.Take(i).LastOrDefault(a => a.VarName == reference);
                    if (lastAssignment != null)
                    {
                        assignmentExpressions[i].References[reference] = lastAssignment.Value;
                        continue;
                    }
                    var definitionMatch = definitionMatches.SingleOrDefault(m => m.Groups["var"].Value == reference);
                    if (definitionMatch != null)
                    {
                        assignmentExpressions[i].References[reference] = definitionMatch.Groups["value"].Value;
                    }
                }

                foreach (var key in assignmentExpressions[i].References.Keys)
                {
                    var keyVal = assignmentExpressions[i].References[key]; 
                    assignmentExpressions[i].Match.Groups["ref"].Captures.Cast<Capture>()
                        .Where(c => FormatVarName(c.Value) == key).ToList().ForEach(c =>
                        {
                            var name = c.Value.Replace("$", "\\$");
                            assignmentExpressions[i].Value = Regex.Replace(assignmentExpressions[i].Value,
                                String.Format(@"(('|"")[ \t\r\n]*\+[ \t\r\n]*)?{0}([ \t\r\n]*\+[ \t\r\n]*('|""))?", name),
                                String.IsNullOrEmpty(assignmentExpressions[i].VarName) ? keyVal : keyVal.Trim(new[] { '"', '\'' }));
                        });
                }

                if (assignmentExpressions[i].IsConcat)
                {
                    var lastAssignment = assignmentExpressions.Take(i).SingleOrDefault(a => a.VarName == assignmentExpressions[i].VarName);
                    if (lastAssignment != null)
                    {
                        assignmentExpressions[i].Value = lastAssignment.Value + assignmentExpressions[i].Value;
                    }
                }

                assignmentExpressions[i].Value = assignmentExpressions[i].Value.Replace("''", String.Empty).Replace("\"\"", String.Empty);
            }
            //Debug.WriteLine(String.Format("Found assignments:\r\n{0}", String.Join("\r\n", assignmentExpressions.Select(e => e.VarName + " = " + e.Value).ToArray())));

            assignmentExpressions.Reverse();

            var resultBuilder = new StringBuilder();
            var lastIndex = executionText.Length;
            var printerVars = new List<String>();
            foreach (var assignmentExpression in assignmentExpressions)
            {
                var expressionEndIndex = assignmentExpression.Match.Index + assignmentExpression.Match.Length;
                resultBuilder.Insert(0, executionText.Substring(expressionEndIndex, lastIndex - expressionEndIndex));
                if (!printerVars.Contains(assignmentExpression.VarName))
                {
                    resultBuilder.Insert(0, assignmentExpression.Value);
                    if (!String.IsNullOrEmpty(assignmentExpression.VarName))
                    {
                        printerVars.Add(assignmentExpression.VarName);
                    }
                }
                printerVars.AddRange(assignmentExpression.References.Select(r => r.Key));
                lastIndex = assignmentExpression.Match.Index;
            }
            if (lastIndex > 0)
            {
                resultBuilder.Insert(0, executionText.Substring(0, lastIndex - 1));
            }

            Debug.Print("BUILDED\r\n***************************\r\n***************************{0}\r\n***************************\r\n***************************",
                resultBuilder);

            Text = resultBuilder.ToString();

            //var concats = definitionMatches
            //    .Where(m => m.Groups["concat"].Success)
            //    .GroupBy(m => m.Groups["var"].Value)
            //    .ToDictionary(m => m.Key, m => AddQuotes(m.Last().Groups["value"].Value));
            //concats.Reverse();

            //foreach (var concatMatch in concats)
            //{
            //    if (consts.ContainsKey(concatMatch.Key))
            //    {
            //        consts[concatMatch.Key] = consts[concatMatch.Key].TrimEnd(new[] { '\'', '"' }) + concatMatch.Value.TrimStart(new[] { '\'', '"' });
            //        Log.DebugFormat("Concatination detected:\r\n{0}",
            //            concatMatch.Key + " += " + concatMatch.Value);
            //    }
            //    else
            //    {
            //        Log.WarnFormat("Concatination NOT matched any variable:\r\n{0}",
            //            concatMatch.Key + " += " + concatMatch.Value);
            //    }
            //}

            //Log.DebugFormat("Variables detected:\r\n{0}", String.Join("\r\n", consts.Select(c => c.Key + " = " + c.Value).ToArray()));

            //definitionEntityMatches.Reverse();
            //definitionEntityMatches.ForEach(d => ReplaceMatch(d, String.Empty, null));

            //for (var i = 0; i < consts.ToList().Count - 1; i++)
            //{
            //    var constant = consts.ToList()[i];

            //    for (var j = i + 1; j < consts.ToList().Count; j++)
            //    {
            //        foreach (var replacementStr in GetReplacementSearchStr(ContentType, constant.Key))
            //        {
            //            consts[consts.ToList()[j].Key] = consts[consts.ToList()[j].Key].Replace(replacementStr, constant.Value.Trim(new [] {'\'', '"'}));
            //        }
            //    }
            //}

            //foreach (var c in consts)
            //{
            //    foreach (var replacementStr in GetReplacementSearchStr(ContentType, c.Key))
            //    {
            //        Text = Text.Replace(replacementStr, c.Value);
            //    }
            //}
            //Text = Regex.Replace(Text, @"'[ \t\r\n]*\+[ \t\r\n]*'", String.Empty);
        }
    }

    class ContentExpression
    {
        public Match Match { get; set; }
        public String VarName { get; set; }
        public String VarExpression { get; set; }
        public Dictionary<String, String> References { get; set; }
        public String Value { get; set; }
        public bool IsConcat { get; set; }
    }
}