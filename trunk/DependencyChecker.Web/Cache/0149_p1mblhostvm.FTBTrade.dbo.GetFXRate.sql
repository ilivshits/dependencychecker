CREATE function [dbo].[GetFXRate](@fromCurrencySid bigint, @toCurrnecySid bigint, @date date)
returns float
AS
begin
	
	declare @fxSid bigint, @fromSid bigint
	select @fxSid = sid, @fromSid = fromCurrencySid
	from FX where (fromCurrencySid = @fromCurrencySid and toCurrencySid = @toCurrnecySid) or
		(fromCurrencySid = @toCurrnecySid and toCurrencySid = @fromCurrencySid)
	
	declare @mark float
	if @fromSid != @fromCurrencySid
		select @mark = 1/mark
		from SecurityMark
		cross join ValuationMode_Transposed
		where sid = @fxSid and date = @date and forwardDate = '1900-01-01' and valuationModeId = [Default]
	else
		select @mark = mark
		from SecurityMark
		cross join ValuationMode_Transposed
		where sid = @fxSid and date = @date and forwardDate = '1900-01-01' and valuationModeId = [Default]
	return @mark
end

