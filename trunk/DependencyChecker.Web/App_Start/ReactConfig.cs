using React;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(DependencyChecker.Web.ReactConfig), "Configure")]

namespace DependencyChecker.Web
{
	public static class ReactConfig
	{
		public static void Configure()
		{
		    ReactSiteConfiguration.Configuration
                .AddScript("~/Scripts/react-router.min.js")
                .AddScript("~/Scripts/app.jsx")
		        .AddScript("~/Scripts/cpanel-users.jsx");

		}
	}
}