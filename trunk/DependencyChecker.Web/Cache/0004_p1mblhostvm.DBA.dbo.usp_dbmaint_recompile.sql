CREATE PROCEDURE [dbo].[usp_dbmaint_recompile]
        @cur_dbname                sysname
       ,@report_only               char(1) = 'N'
       ,@debug_sw                  bit     = 0
       
AS
--PRINT 'Entering usp_dbmaint_reindex for ' + QUOTENAME(@cur_dbname)
DECLARE @dynsql                    varchar(max),
        @rowid                     int,
        @maxrows                   int,
        @dbmaint_log_rowid         int

SET NOCOUNT ON
       
IF object_id('tempdb.dbo.#recompile_objects') IS NOT NULL
  DROP TABLE #recompile_objects
  
CREATE TABLE #recompile_objects
  (
   [schema_name]     sysname,
   [objname]         sysname,
   [type]            varchar(2)
  )   

PRINT '{' + CONVERT(varchar(22), GETDATE(), 100) + 
      '}     ***** Start - Recompiling all stored procedures and triggers of database: ' + QUOTENAME(@cur_dbname) + ' *****'
PRINT '.'
    
SELECT @dynsql = 
    'USE ' + QUOTENAME(@cur_dbname) + '
        SELECT QUOTENAME(SCHEMA_NAME([schema_id])) as [schema_name], QUOTENAME([name]) as objname, type 
         FROM sys.objects 
         WHERE [type] IN  (''P'', ''TR'', ''V'')
         ORDER BY [type], [name]'  
    --PRINT @dynsql 
  
 INSERT INTO #recompile_objects ([schema_name], [objname],  [type])
   EXEC (@dynsql)

IF @debug_sw = 1
  SELECT * from #recompile_objects

-- deleting invalid characters like single/double quote in SP name
DELETE FROM #recompile_objects
   WHERE CHARINDEX('''', [objname]) > 0 OR CHARINDEX('"', [objname]) > 0

SELECT IDENTITY(int, 1, 1) AS [rowid], * 
  INTO #recompile_objects2 FROM  #recompile_objects WHERE 1 = 0
TRUNCATE TABLE #recompile_objects2 
INSERT  INTO #recompile_objects2 
  SELECT * FROM  #recompile_objects

IF @debug_sw = 1
  SELECT * from #recompile_objects

SELECT @maxrows = count(*) FROM #recompile_objects2

SET @rowid   = 1

IF @report_only = 'N'
BEGIN
  WHILE (@rowid <= @maxrows)
  BEGIN
    SELECT @dynsql= 'USE '  + QUOTENAME(@cur_dbname) + '
          SET QUOTED_IDENTIFIER ON
           EXEC sp_recompile  ''' + [schema_name] + '.' + [objname] + ''''
      FROM #recompile_objects2  WHERE rowid = @rowid
    EXEC(@dynsql)
    SET @rowid = @rowid + 1
  END
  
  UPDATE DBA.dbo.dbmaint_log
    SET maint_end_time = GETDATE(),
       [status]        = 'successful'
   WHERE rowid        = @dbmaint_log_rowid
END
ELSE
BEGIN
  WHILE (@rowid <= @maxrows)
  BEGIN
    SELECT @dynsql = CASE [type] 
                        WHEN 'P'  THEN 'Stored Procedure - ''' 
                        WHEN 'TR' THEN 'Trigger          - ''' 
                        WHEN 'V'  THEN 'View             - '''
                      END +
                      [schema_name] + '.' + [objname] + ''''
       FROM #recompile_objects2 WHERE rowid = @rowid
     PRINT @dynsql
     SET @rowid = @rowid + 1
  END
END  
PRINT '.'

PRINT '{' + CONVERT(varchar(22), GETDATE(), 100) + 
      '}     ***** End   - Recompiling all stored procedures and triggers of database: ' + QUOTENAME(@cur_dbname) + ' *****'
PRINT '.'
PRINT REPLICATE('==>', 50)
RETURN

