create view [tagging].[Tag_WithDeletes] as
	select
		t.*
	from
		(
			select
				ROW_NUMBER() over (partition by t.tagId order by t.live, t.version) as _ordinal,
				t.*
			from
				(
					select
						t.tagId, t.description, t.durationId, t.preferredFlag, t.targetId, t.typeId, t.valueSourceId,
						t.version, 1 as live
					from
						tagging.Tag as t with (nolock)
					union
					---- Log table.
					select
						t.tagId, t.description, t.durationId, t.preferredFlag, t.targetId, t.typeId, t.valueSourceId,
						t.version, 0 as live
					from
						tagging.TagLog as t with (nolock)
					where
						t.action = 'D'
				) as t
		) as t
	where
		t._ordinal = 1


