
CREATE PROCEDURE RenameCurves
	@namesXml xml
AS
BEGIN
	SET NOCOUNT ON;
    
	ALTER TABLE PublishImagine NOCHECK CONSTRAINT ALL

	create table #names (oldCode varchar(50), newCode varchar(50))

	insert into #names 
	select nameRows.n.value('oldCode[1]', 'varchar(50)'), nameRows.n.value('newCode[1]', 'varchar(50)')
	from @namesXml.nodes('//row') AS nameRows(n)
	
	UPDATE p
	SET code = r.newCode
	FROM PublishConfiguration p
	INNER JOIN #names r ON r.oldCode collate Latin1_General_BIN2 = p.code collate Latin1_General_BIN2
	
	UPDATE p
	SET code = r.newCode
	FROM dbo.PublishImagine p
	INNER JOIN #names r ON r.oldCode collate Latin1_General_BIN2 = p.code collate Latin1_General_BIN2
	
	ALTER TABLE dbo.PublishImagine CHECK CONSTRAINT ALL

	UPDATE p
	SET code = r.newCode
	FROM dbo.RollCurvePublishImagine p
	INNER JOIN #names r ON r.oldCode collate Latin1_General_BIN2 = p.code collate Latin1_General_BIN2

	drop table #names
END

