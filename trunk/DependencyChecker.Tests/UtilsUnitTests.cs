﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using DependencyChecker.Entities.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DependencyChecker.Tests
{
    [TestClass]
    [DeploymentItem("Resources\\testFilesList_1.txt")]
    [DeploymentItem("Resources\\test_5.rdl")]
    [DeploymentItem("Resources\\TestProc.sql")]
    [DeploymentItem("Resources\\TestProc2.sql")]
    [DeploymentItem("Resources\\TestProc3.sql")]
    public class UtilsUnitTests
    {
        [TestMethod]
        public void ZipPackerTestDuplicateFiles()
        {
            var package = ZipPacker.GeneratePackage("test package", File.ReadAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testFilesList_1.txt")).ToList());

            Assert.IsNotNull(package);
        }
    }
}
