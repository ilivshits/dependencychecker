

CREATE PROCEDURE [fortress].[Currency_TranslateFromBloomberg]
	@ticker varchar(50) output,
	@multiplier decimal output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--- If null, return null.
	if (@ticker is null) begin set @multiplier = null return end
	
	--- Set.
	if (@ticker in ('BWp', 'CAd', 'EUr', 'GBp', 'ILs', 'MWk', 'SZl', 'ZAr', 'USd')) 
	begin 
		--- Set the multiplier and ticker then get out.
		set @multiplier = @multiplier * 0.01 return 
		set @ticker = upper(@ticker)
		return
	end
	
	--- KWd.
	if (@ticker = 'KWd')
	begin
		--- Set multiplier and ticker then get out.
		set @multiplier = @multiplier * 0.001
		set @ticker = upper(@ticker)
		return
	end
	
	--- TRL, don't touch
	if (@ticker = 'TRL') set @ticker = 'TRY'
END



