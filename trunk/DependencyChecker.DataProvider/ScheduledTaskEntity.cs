//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DependencyChecker.DataProvider
{
    using System;
    using System.Collections.Generic;
    
    public partial class ScheduledTaskEntity : EntityBase
    {
        public Nullable<System.DateTime> TaskLastRunTime { get; set; }
    
        public virtual EntityBase EntityBase { get; set; }
    }
}
