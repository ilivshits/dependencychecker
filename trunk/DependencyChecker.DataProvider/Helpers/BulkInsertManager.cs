﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyChecker.DataProvider.Helpers
{
    public class BulkInsertManager<TEntity> where TEntity : class
    {
        private DbContext _context;

        public List<KeyValuePair<Type, List<ColumnInfo>>> Mappings { get; private set; }
        public List<TableInfo> TableInfos { get; private set; }

        public BulkInsertManager(DbContext ctx)
        {
            _context = ctx;
            SetMappings();
        }

        public static void BulkInsert(IEnumerable<TEntity> items)
        {

        }

        

        public void SetMappings()
        {
            Mappings = new List<KeyValuePair<Type, List<ColumnInfo>>>();
            TableInfos = new List<TableInfo>();
            var metadata = DatabaseHelpers.GetMetadata(_context);
            var oic = DatabaseHelpers.GetObjectItemCollection(_context);
            var sc = DatabaseHelpers.GetSContainer(_context);
            var objectType = typeof (TEntity);
            System.Data.Entity.Core.Metadata.Edm.EntityType objectItem;

            do
            {
                var set = sc.EntitySets.Single(e=>e.Name == objectType.Name);


                var storeEntity = metadata.GetItems<System.Data.Entity.Core.Metadata.Edm.EntityType>(DataSpace.SSpace).Single(e => (e).Name == objectType.Name);
                var declaredMembers = storeEntity.DeclaredMembers;

                objectItem = (System.Data.Entity.Core.Metadata.Edm.EntityType)oic[objectType.FullName];

                Mappings.Add(new KeyValuePair<Type, List<ColumnInfo>>(objectType, new List<ColumnInfo>()));
                var mapping = Mappings.Last();

                foreach (var prop in declaredMembers)
                {
                    if (mapping.Value.FirstOrDefault(ci=>ci.ColumnName == prop.Name) == null)
                        mapping.Value.Add(new ColumnInfo()
                        {
                            ColumnName = prop.Name,
                            Type = objectItem.Properties.Single(o=>o.Name == prop.Name).PrimitiveType.ClrEquivalentType
                        });
                }

                TableInfos.Add(new TableInfo()
                {
                    TableName = set.Name,
                    SchemaName = set.Schema,
                    Type = objectType
                });


                var edmType = objectItem.BaseType;
                objectType = edmType == null ? null : Type.GetType(edmType.FullName);

            } while (objectType != null);



            Mappings.Reverse();

        }

        public void InsertAll(IEnumerable<TEntity> items, DbConnection connection = null)
        {
            /* Get connection */
            var context = (_context as IObjectContextAdapter).ObjectContext;
            var con = context.Connection as EntityConnection;
            if (con == null && connection == null)
            {
                var set = context.CreateObjectSet<TEntity>();
                foreach (var item in items)
                {
                    set.AddObject(item);
                }
                context.SaveChanges();
            }

            var connectionToUse = connection ?? con.StoreConnection;

            /* Bulk Insert */

            var provider = new SqlQueryProvider();

            foreach (var mapping in this.Mappings)
            {
                string table = TableInfos.Single(e => e.Type == mapping.Key).TableName;
                string schema = TableInfos.Single(e => e.Type == mapping.Key).SchemaName;

                provider.InsertItems(items, schema, table, mapping.Value, connectionToUse);
            }

            
        }
    }

    public class TableInfo
    {
        public string TableName { get; set; }
        public string SchemaName { get; set; }
        public Type Type { get; set; }
    }

    public class ColumnInfo
    {
        public string ColumnName { get; set; }
        public Type Type { get; set; }
    }
}
