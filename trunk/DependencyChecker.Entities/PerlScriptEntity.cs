﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Utils;

namespace DependencyChecker.Entities
{
    public class PerlScriptEntity : EntityBase
    {
        public static readonly string ProviderName = EntityProviderType.Perl.ToString();

        public PerlScriptEntity(string name) : this(name, String.Empty)
        {
        }
        public PerlScriptEntity(string name, string definition) : this(name, definition, ProviderName)
        {
        }
        protected PerlScriptEntity(string name, string definition, string providerName)
            : base(name, definition, providerName, EntityType.PerlScript)
        {
            _searchNames = new List<string>
            {
                Name,
                Path.GetFileName(Name)
            }; 
        }

        public override string ScopeDelimiter
        {
            get
            {
                return "/";
            }
        }

        #region Overrides of EntityBase

        public override string InternalId { get; set; }

        public override string FullName
        {
            get { return FileUtils.BuildRemotePath(Root.Name, Name); }
        }

        #endregion

        private readonly List<string> _searchNames;
        public override List<string> SearchNames
        {
            get { return _searchNames; }
        }

        private Regex commentsRemoval = new Regex(@"(?<q>(""|'))(?:\\\<q>|.)*?\<q>", 
            RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Compiled);

        protected override List<SearchContent> GetSearchContent()
        {
            var result = new List<SearchContent>();
            if (String.IsNullOrEmpty(Definition))
            {
                return result;
            }

            var clearContent = new StringBuilder();

            var lastIndex = 0;
            commentsRemoval.Matches(Definition).Cast<Match>().ToList().ForEach(m =>
            {
                if (m.Groups["comment"].Success)
                {
                    clearContent.Append(Definition.Substring(lastIndex, m.Groups["comment"].Index - lastIndex));
                    lastIndex = m.Groups["comment"].Index + m.Groups["comment"].Length;
                }
            });

            if (lastIndex < Definition.Length)
            {
                clearContent.Append(Definition.Substring(lastIndex, Definition.Length - lastIndex));
            }

            Debug.WriteLine(clearContent.ToString());

            var content = new SearchContent
            {
                ContentType = EntityProviderType.Perl,
                Text = clearContent.ToString() //Regex.Replace(Definition, @"#.*$", String.Empty, RegexOptions.IgnoreCase | RegexOptions.Multiline)
            };
            content.ProcessVariables();

            var connectionMatch =
                Regex.Match(content.Text,
                    @"(Server=(?<serverName>[a-z0-9_\-]+);Database=(?<dbName>[a-z0-9_]+)|" + 
                    @"Database=(?<dbName>[a-z0-9_]+);Server=(?<serverName>[a-z0-9_\-]+))", RegexOptions.IgnoreCase);

            var dbName = String.Empty;
            if (connectionMatch.Success)
            {
                dbName = String.Format("[{0}].[{1}]",
                    connectionMatch.Groups["serverName"].Value,
                    connectionMatch.Groups["dbName"].Value);
                result.Add(new SearchContent()
                {
                    ContentType = EntityProviderType.SQL,
                    Text = dbName
                });
            }

            var queryMatch = Regex.Matches(content.Text, 
                    @"\-\>prepare\(([ \t]+)?""(?<query>[^""]+)""([ \t]+)?\)", RegexOptions.IgnoreCase);

            if (queryMatch.Count > 0)
            {
                result.AddRange(queryMatch.Cast<Match>().Select(m =>  new SearchContent()
                {
                    ContentType = EntityProviderType.SQL,
                    Text = m.Groups["query"].Value,
                    ScopeName = dbName
                }));
            }
            //result.Add(content);
            return result;
        }
    }
}