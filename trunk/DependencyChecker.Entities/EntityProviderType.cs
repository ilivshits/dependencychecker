﻿namespace DependencyChecker.Entities
{
    public enum EntityProviderType
    {
        SQL,
        SSRS,
        OLAP,
        ScheduledTask,
        P1TC,
        Perl,
        Batch,
        Person
    }
}
