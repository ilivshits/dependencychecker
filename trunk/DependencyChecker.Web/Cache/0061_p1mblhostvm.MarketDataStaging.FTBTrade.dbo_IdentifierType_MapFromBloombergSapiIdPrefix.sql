
create view [FTBTrade].[dbo_IdentifierType_MapFromBloombergSapiIdPrefix] as
	select  2 as IdentifierTypeId, 'ticker' as BloombergSapiIdPrefix union
	select  3 as IdentifierTypeId, 'cusip' as BloombergSapiIdPrefix union
	select  5 as IdentifierTypeId, 'isin' as BloombergSapiIdPrefix union
	select 10 as IdentifierTypeId, 'bbgid' as BloombergSapiIdPrefix union
	select 11 as IdentifierTypeId, 'buid' as BloombergSapiIdPrefix


