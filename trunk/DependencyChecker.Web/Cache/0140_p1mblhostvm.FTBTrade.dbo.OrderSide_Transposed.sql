CREATE VIEW [dbo].[OrderSide_Transposed] AS
SELECT
[Buy] = 1
, [Sell] = 2
, [Cover] = 3
, [Short] = 4
, [Initiate] = 5
, [Buy Protection] = 6
, [Sell Protection] = 7
, [Repo] = 8
, [Reverse Repo] = 9
, [Pay] = 10
, [Receive] = 11


