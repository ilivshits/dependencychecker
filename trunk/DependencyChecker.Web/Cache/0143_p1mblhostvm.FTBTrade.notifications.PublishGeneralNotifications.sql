CREATE procedure [notifications].[PublishGeneralNotifications]
(
	@notifications notifications.GeneralNotificationRow readonly
)
as
begin
    --- Don't count.
    set nocount on

    --- The batch size.
    declare @batchSize int = 1000

    --- The last ordinal.
    declare @lastOrdinal int = 0
    
    --- The message.
    declare @message varchar(max)

    --- While there are items.
    while (exists(select * from @notifications where __ordinal > @lastOrdinal))
    begin
        --- Construct message
        set @message = (
            select '<gn>' +
                            '<a>' + action + '</a>' +
                            '<s>' + schemaName + '</s>' +
                            '<tn>' + tableName + '</tn>' +
                            '<pk>' + primaryKey + '</pk>' +
                            '<pv>' + cast(primaryValue AS varchar) + '</pv>' +
                            '<spt>' + isnull(cast(securityPrimaryTypeId as varchar), '') + '</spt>' +
                            '<t>' + convert(varchar(24), lastUpdate, 121) + '</t>' +
            '</gn>'
            from (select top (@batchSize) * from @notifications where __ordinal > @lastOrdinal order by __ordinal) as n
            for xml path(''), type
        ).value('.', 'varchar(max)')

        --- Dispatch message
        if @message is not null
        begin
            declare @binary varbinary(max) = cast(('<msg>' + @message + '</msg>') as varbinary(max))
            exec dispatcher.Send 'GeneralNotificationService', @binary
        end

        --- Increase last ordinal.
        set @lastOrdinal = (
            select top 1
                            __ordinal
            from 
                            (select top (@batchSize) __ordinal from @notifications where __ordinal > @lastOrdinal order by __ordinal) as n
            order by
                            __ordinal desc
        )
    end
end


