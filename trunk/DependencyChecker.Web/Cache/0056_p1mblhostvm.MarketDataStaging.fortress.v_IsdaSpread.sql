
create view [fortress].[v_IsdaSpread] as
	--- Emerging markets.
	with EmergingMarket as (
		select 'AE' as CountryCode union all
		select 'AR' as CountryCode union all
		select 'BG' as CountryCode union all
		select 'BH' as CountryCode union all
		select 'BR' as CountryCode union all
		select 'CL' as CountryCode union all
		select 'CN' as CountryCode union all
		select 'CO' as CountryCode union all
		select 'CZ' as CountryCode union all
		select 'EE' as CountryCode union all
		select 'EG' as CountryCode union all
		select 'HU' as CountryCode union all
		select 'ID' as CountryCode union all
		select 'IN' as CountryCode union all
		select 'JO' as CountryCode union all
		select 'KW' as CountryCode union all
		select 'LK' as CountryCode union all
		select 'LT' as CountryCode union all
		select 'LV' as CountryCode union all
		select 'MA' as CountryCode union all
		select 'MU' as CountryCode union all
		select 'MX' as CountryCode union all
		select 'MY' as CountryCode union all
		select 'OM' as CountryCode union all
		select 'PE' as CountryCode union all
		select 'PH' as CountryCode union all
		select 'PK' as CountryCode union all
		select 'PL' as CountryCode union all
		select 'QA' as CountryCode union all
		select 'RO' as CountryCode union all
		select 'RU' as CountryCode union all
		select 'SK' as CountryCode union all
		select 'TH' as CountryCode union all
		select 'TR' as CountryCode union all
		select 'ZA' as CountryCode
	),
	Europe as (
		select 'AT' as CountryCode union all
		select 'BE' as CountryCode union all
		select 'CH' as CountryCode union all
		select 'DE' as CountryCode union all
		select 'DK' as CountryCode union all
		select 'ES' as CountryCode union all
		select 'FI' as CountryCode union all
		select 'FR' as CountryCode union all
		select 'GB' as CountryCode union all
		select 'GR' as CountryCode union all
		select 'IE' as CountryCode union all
		select 'IT' as CountryCode union all
		select 'NL' as CountryCode union all
		select 'NO' as CountryCode union all
		select 'PT' as CountryCode union all
		select 'SE' as CountryCode union all
		
		--- General Europe.
		select 'XS' as CountryCode
	),
	--- Special cases.
	--- Japan.
	SpecialCaseJapan as (
		--- Japan
		select 'JP' as CountryCode
	)
	--- Emerging markets
	select 
		em.CountryCode, s.Spread
	from 
		EmergingMarket as em
			cross join (
				select 1.00 as Spread union all
				select 5.00 as Spread
			) as s
	union all
	--- Europe.
	select
		e.CountryCode, s.Spread
	from
		Europe as e
			cross join (
				select  0.25 as Spread union all
				select  3.00 as Spread union all				
				select  1.00 as Spread union all
				select  5.00 as Spread union all
				select  7.50 as Spread union all
				select 10.00 as Spread
			) as s
	union all
	--- Special cases - Japan.
	select 
		sc.CountryCode, s.Spread 
	from 
		SpecialCaseJapan as sc 
			cross join (
				select 0.25 as Spread union all
				select 1.00 as Spread union all
				select 5.00 as Spread
			) as s
	union all
	--- Everything else.
	select
		c.code2 as CountryCode, s.Spread
	from
		FTBTrade.dbo_Country as c with (nolock)
			cross join (
				select 1.00 as Spread union all
				select 5.00 as Spread
			) as s
	where
		c.countryId <> 0 and
		c.code2 not in (select CountryCode from SpecialCaseJapan) and
		c.code2 not in (select CountryCode from Europe) and
		c.code2 not in (select CountryCode from EmergingMarket)


