



CREATE FUNCTION [dbo].[fun_Split]
(
	@String varchar(max),
	@Delimiter char(1)
)
RETURNS @Results TABLE (Items nvarchar(100))
AS

BEGIN
    DECLARE @Index int
    DECLARE @Slice varchar(max)
    SELECT @Index = 1
    IF @String IS NULL
        RETURN
    SET @String = ltrim(rtrim(@String));
    WHILE @Index != 0
    BEGIN
        SELECT @Index = CharIndex(@Delimiter, @String)
        IF (@Index != 0 )
            SELECT @Slice = LEFT(@String, @Index - 1)
        ELSE 
            SELECT @Slice = @String
        
        INSERT INTO @Results
        (Items )
        VALUES
        (@Slice )
        SELECT @String = RIGHT(@String, Len(@String) - @Index)
        IF Len(@String) = 0
        BREAK
    END
    RETURN
END





