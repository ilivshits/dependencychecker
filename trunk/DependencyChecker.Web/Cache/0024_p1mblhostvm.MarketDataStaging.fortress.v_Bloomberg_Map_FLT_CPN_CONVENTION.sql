
create view [fortress].[v_Bloomberg_Map_FLT_CPN_CONVENTION] as
	select 'Following bus day - adj' as FLT_CPN_CONVENTION, 2 as businessDayConventionId, 1 as businessDayConventionInterestAdjustmentId union
	select 'Following bus day - unadj' as FLT_CPN_CONVENTION, 2 as businessDayConventionId, 2 as businessDayConventionInterestAdjustmentId union
	select 'Mod Bus Day-Adj Nxt Month' as FLT_CPN_CONVENTION , 4 as businessDayConventionId, 3 as businessDayConventionInterestAdjustmentId union
	select 'Mod bus day - adj' as FLT_CPN_CONVENTION, 4 as businessDayConventionId, 1 as businessDayConventionInterestAdjustmentId union
	select 'Mod bus day - unadj' as FLT_CPN_CONVENTION, 4 as businessDayConventionId, 2 as businessDayConventionInterestAdjustmentId union
	select 'Preceding bus day - adj' as FLT_CPN_CONVENTION, 3 as businessDayConventionId, 1 as businessDayConventionInterestAdjustmentId union
	select 'Preceding bus day - unadj' as FLT_CPN_CONVENTION, 3 as businessDayConventionId, 1 as businessDayConventionInterestAdjustmentId


