create function [dbo].[GetUserRoles](@userId int)
returns table as return
with RoleSet as
(
	select roleId from dbo.UserRole where userId = @userId
	union all
	select childRoleId from dbo.RoleHierarchy m
	inner join RoleSet s on m.parentRoleId = s.roleId
)
select distinct roleId from RoleSet


