﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using DependencyChecker.Common;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Entities
{
    public static class EntityExporter
    {
       

        static EntityExporter()
        {
        }

        private static bool LoadResources()
        {
            EntityFormatsDictionary = new Dictionary<EntityType, string>();

            if (Directory.Exists("Resources"))
            {
                _pattern = File.ReadAllText(Path.Combine("Resources", "GraphPattern.xml")).Split(new[] { "{DATA}" }, StringSplitOptions.RemoveEmptyEntries);
                if (_pattern.Length != 2)
                {
                    LoggerHelper.Log.ErrorFormat("GraphML pattern file corrupted");
                    return false;
                }

                var files = Directory.GetFiles("Resources");
                foreach (var file in files)
                {
                    EntityType type;
                    if (Enum.TryParse(Path.GetFileNameWithoutExtension(file), true, out type))
                    {
                        LoggerHelper.Log.ErrorFormat("Pattern for {0} entity detected. Using file: {1}", type, file);
                        EntityFormatsDictionary.Add(type, File.ReadAllText(file));
                    }
                }

                Line = File.ReadAllText(Path.Combine("Resources", "_line.graphml"));
                return !String.IsNullOrEmpty(Line);
            }
            return false;
        }

        public static String ExportToText(this IEntity entity)
        {
            var sb = new StringBuilder();
            LoggerHelper.Log.InfoFormat("Printing object '{0}'...", entity.FullName);
            sb.AppendLine(String.Format("Object '{0}' of type '{1}'", entity.FullName, entity.TypeName));

            sb.AppendLine("\r\nUsed by:\r\n");
            sb.AppendLine("***********************************************");
            sb.AppendLine(PrintNode(entity.UsedByRecursive));
            sb.AppendLine("***********************************************");

            sb.AppendLine("\r\nDepends on:\r\n");
            sb.AppendLine("***********************************************");
            sb.AppendLine(PrintNode(entity.DependsOnRecursive));
            sb.AppendLine("***********************************************");

            sb.AppendLine("\r\nIssues detected:\r\n");
            sb.AppendLine("***********************************************");
            foreach (var error in entity.GetErrors())
            {
                sb.AppendLine(error);
            }
            sb.AppendLine("***********************************************");

            sb.AppendLine();
            sb.AppendLine("\r\nDefinition:\r\n");
            sb.AppendLine("***********************************************");
            sb.AppendLine(entity.Definition);
            sb.AppendLine("***********************************************");
            return sb.ToString();
        }

        // ReSharper disable once InconsistentNaming
        public static void ExportToGraphML(this List<IEntity> entities, string filename, bool includeDown = true, bool includeUp = false)
        {
            try
            {
                if (!LoadResources())
                {
                    LoggerHelper.Log.ErrorFormat("Resources load failed. please check Resources folder.");
                    return;
                }
                LoggerHelper.Log.InfoFormat("Resources load completed.");
                LoggerHelper.Log.InfoFormat("Exporting entities to GraphML with options: up={1}, down={2}, filename='{3}'. Entities list:\r\n{0}",
                    String.Join("\r\n", entities.Select(e => e.FullName)), includeUp, includeDown, filename);
                using (var writer = File.Open(filename, FileMode.Create))
                {
                    writer.WriteString(_pattern[0]);

                    var printed = new List<Guid>();

                    foreach (var entity in entities)
                    {
                        printed.AddRange(entity.WriteGraphNodeTextRecursive(writer, includeDown, includeUp));
                    }

                    var i = 1;
                    foreach (var entity in entities)
                    {
                        entity.WriteGraphLinksTextRecursive(writer, ref i, printed, includeDown, includeUp);
                    }

                    writer.WriteString(_pattern[1]);
                    writer.Close();
                }
            }
            catch (Exception e)
            {
                LoggerHelper.Log.ErrorFormat("Exception occured: {0}", e);
            }
        }

        // ReSharper disable once InconsistentNaming
        public static void ExportToGraphML(this List<IEntity> entities, Stream writer, bool includeDown = true, bool includeUp = false)
        {
            try
            {
                if (!LoadResources())
                {
                    LoggerHelper.Log.ErrorFormat("Resources load failed. please check Resources folder.");
                    return;
                }
                LoggerHelper.Log.InfoFormat("Resources load completed.");
                LoggerHelper.Log.InfoFormat("Exporting entities to GraphML with options: up={1}, down={2}, ouput->stream. Entities list:\r\n{0}",
                    String.Join("\r\n", entities.Select(e => e.FullName)), includeUp, includeDown);

                writer.WriteString(_pattern[0]);

                var printed = new List<Guid>();

                foreach (var entity in entities)
                {
                    printed.AddRange(entity.WriteGraphNodeTextRecursive(writer, includeDown, includeUp));
                }

                var i = 1;
                foreach (var entity in entities)
                {
                    entity.WriteGraphLinksTextRecursive(writer, ref i, printed, includeDown, includeUp);
                }

                writer.WriteString(_pattern[1]);
            }
            catch (Exception e)
            {
                LoggerHelper.Log.ErrorFormat("Exception occured: {0}", e);
            }
        }

        private static void WriteString(this Stream stream, string text)
        {
            var info = new UTF8Encoding(true).GetBytes(text);
            stream.Write(info, 0, info.Length);
        }

        private static bool IsGraphSupported(this IEntity entity)
        {
            return EntityFormatsDictionary.ContainsKey(entity.Type) &&
                   !String.IsNullOrEmpty(EntityFormatsDictionary[entity.Type]);
        }

        private static void WriteGraphLinksTextRecursive(this IEntity entity, Stream writer, ref int counter, List<Guid> printed, bool includeDown = true, bool includeUp = false, int gen = 0)
        {
            if (gen > 100)
            {
                LoggerHelper.Log.WarnFormat("Deep recursion detected during printing links. Exiting...");
                return;
            }
            if (!entity.IsGraphSupported())
            {
                return;
            }
            if (includeDown)
            {
                if (entity.ScopeChildren != null)
                {
                    foreach (var child in entity.ScopeChildren.Where(i => i.IsGraphSupported()))
                    {
                        if (printed.Contains(child.Id) && printed.Contains(entity.Id))
                        {
                            writer.WriteString(String.Format(Line
                                .Replace("{ID}", "edge{0}")
                                .Replace("{FROM}", "{2}")
                                .Replace("{TO}", "{1}")
                                .Replace("{COLOR}", "000000")
                                .Replace("{TYPE}", "line")
                                .Replace("{SMOOTH}", "false"),
                                counter++, entity.Id, child.Id));
                        }
                        if (gen < 100)
                        {
                            child.WriteGraphLinksTextRecursive(writer, ref counter, printed, true, false, gen + 1);
                        }
                    }
                }
                if (entity.DependsOn != null)
                {
                    foreach (var dependency in entity.DependsOn.Where(i => i.IsGraphSupported()))
                    {
                        if (printed.Contains(dependency.Id) && printed.Contains(entity.Id))
                        {
                            writer.WriteString(String.Format(Line
                                .Replace("{ID}", "dep{0}")
                                .Replace("{FROM}", "{2}")
                                .Replace("{TO}", "{1}")
                                .Replace("{COLOR}", "808080")
                                .Replace("{TYPE}", "dashed")
                                .Replace("{SMOOTH}", "true"),
                                counter++, entity.Id, dependency.Id));
                        }
                    }
                }
            }
            if (includeUp)
            {
                if (entity.Scope != null)
                {
                    if (printed.Contains(entity.Scope.Id) && printed.Contains(entity.Id))
                    {
                        writer.WriteString(String.Format(Line
                            .Replace("{ID}", "edge{0}")
                            .Replace("{FROM}", "{1}")
                            .Replace("{TO}", "{2}")
                            .Replace("{COLOR}", "000000")
                            .Replace("{TYPE}", "line")
                            .Replace("{SMOOTH}", "false"),
                            counter++, entity.Id, entity.Scope.Id));
                        if (gen < 100)
                        {
                            entity.Scope.WriteGraphLinksTextRecursive(writer, ref counter, printed, false, true, gen + 1);
                        }
                    }
                }
                if (entity.UsedBy != null)
                {
                    foreach (var dependency in entity.UsedBy.Where(i => i.IsGraphSupported()))
                    {
                        if (printed.Contains(dependency.Id) && printed.Contains(entity.Id))
                        {
                            writer.WriteString(String.Format(Line
                                .Replace("{ID}", "dep{0}")
                                .Replace("{FROM}", "{1}")
                                .Replace("{TO}", "{2}")
                                .Replace("{COLOR}", "808080")
                                .Replace("{TYPE}", "dashed")
                                .Replace("{SMOOTH}", "true"),
                                counter++, entity.Id, dependency.Id));
                            if (gen < 100)
                            {
                                dependency.WriteGraphLinksTextRecursive(writer, ref counter, printed, false, true,
                                    gen + 1);
                            }
                        }
                    }
                }
            }
        }

        private static IEnumerable<Guid> WriteGraphNodeTextRecursive(this IEntity entity, Stream writer, bool includeDown = true, bool includeUp = false, int gen = 0, List<Guid> printed = null)
        {
            var result = printed ?? new List<Guid>();

            if (gen > 100)
            {
                LoggerHelper.Log.WarnFormat("Deep recursion detected during printing nodes. Exiting...");
                return result;
            }

            if (!result.Contains(entity.Id) && entity.IsGraphSupported())
            {
                writer.WriteString(entity.GetGraphNodeText());
                result.Add(entity.Id);
            }
            else
            {
                return result;
            }
            if (includeDown)
            {
                if (entity.ScopeChildren != null)
                {
                    foreach (var child in entity.ScopeChildren)
                    {
                        child.WriteGraphNodeTextRecursive(writer, true, false, gen + 1, result);
                    }
                }
            }
            if (includeUp)
            {
                if (entity.Scope != null)
                {
                    entity.Scope.WriteGraphNodeTextRecursive(writer, false, true, gen + 1, result);
                }
                if (entity.UsedBy != null
                       && entity.Type != EntityType.DataSource
                       && entity.Type != EntityType.Server
                       && gen < 100) //DataSource exception added to prevent pulling all the reports
                {
                    foreach (var u in entity.UsedBy)
                    {
                        u.WriteGraphNodeTextRecursive(writer, false, true, gen + 1, result);
                    }
                }
            }
            return result;
        }

        private static string GetGraphNodeText(this IEntity entity)
        {
            if (!entity.IsGraphSupported())
            {
                return String.Empty;
            }
            return
                String.Format(
                    EntityFormatsDictionary[entity.Type].Replace("{ID}", "{0}")
                        .Replace("{NAME}", "{1}")
                        .Replace("{DEFINITION}", "{2}")
                        .Replace("{ICON}", String.Empty),
                        entity.Id, entity.Name, entity.Type == EntityType.Table ? System.Security.SecurityElement.Escape(entity.Definition) : entity.Definition);
        }

        public static string ExportToFile(this IEntity entity, string path = "output", FileFormat format = FileFormat.TXT)
        {
            try
            {
                var fileName = entity.FullName;
                Path.GetInvalidFileNameChars().ToList().ForEach(c => fileName = fileName.Replace(c, ' '));
                fileName = Path.Combine(path, fileName.Trim() + ".txt");
                var file = File.Open(fileName, FileMode.Create);
                using (var writer = new StreamWriter(file))
                {
                    writer.Write(entity.ExportToText());

                    writer.Close();
                    writer.Dispose();
                }

                file.Close();
                file.Dispose();
                LoggerHelper.Log.InfoFormat("Entity '{0}' printed to file", entity.FullName);
                return fileName;
            }
            catch (Exception ex)
            {
                LoggerHelper.Log.ErrorFormat("Entity '{0}' cannot be printed to file. Error: {1}", entity.FullName, ex);
            }
            return String.Empty;
        }

        private static String PrintNode(IEnumerable<IEntity> allItems, int level = 0)
        {
            var writer = new StringBuilder();
            if (allItems == null)
            {
                return String.Empty;
            }
            var nodeFiltered = allItems.Where(i => i.Level >= level).ToList();
            if (nodeFiltered.Count == 0)
            {
                return String.Empty;
            }
            foreach (var node in nodeFiltered.GroupBy(i => i.Parents[level]))
            {
                writer.AppendFormat("{0}{1} ({2})\r\n", new String('-', node.Key.Level), node.Key.Name, node.Key.TypeName);
                writer.Append(PrintNode(node, level + 1));
            }
            return writer.ToString();
        }

        private static Dictionary<EntityType, String> EntityFormatsDictionary;

        private static string Line;
        private static string[] _pattern;

        private const string Icon = @"horizontalTextPosition=""right"" iconData=""icon"" iconTextGap=""4"" verticalTextPosition=""center"" height=""18.0""";
    }

    public enum FileFormat
    {
        TXT,
        XML,
        GraphML
    }
}