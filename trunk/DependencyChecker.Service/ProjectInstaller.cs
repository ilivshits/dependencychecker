﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Linq;

namespace DependencyChecker.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();

            var arguments = Environment.GetCommandLineArgs();

            foreach (var argument in arguments)
            {
                if (argument.ToLower().StartsWith("/servicename="))
                {
                    DCServiceInstaller.ServiceName = argument.Split('=')[1].Trim('"');
                }
                if (argument.ToLower().StartsWith("/displayname="))
                {
                    DCServiceInstaller.DisplayName = argument.Split('=')[1].Trim('"');
                }
            }
        }
    }
}
