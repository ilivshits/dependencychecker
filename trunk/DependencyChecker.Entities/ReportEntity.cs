﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Entities
{
    public class ReportEntity : EntityBase
    {
        public static readonly string ProviderName = EntityProviderType.SSRS.ToString();
        private static readonly XNamespace SSRSNamespace = "http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition";

        public override String InternalId { get; set; }

        private readonly Dictionary<String, IEntity> _dataSources = new Dictionary<string, IEntity>();

        public Dictionary<String, IEntity> DataSources
        {
            get
            {
                return _dataSources;
            }
        }

        public override string ScopeDelimiter
        {
            get
            {
                return "/";
            }
        }

        public override string FullName
        {
            get
            {
                return (Type == EntityType.Server) ?
                            String.Empty :
                            ((Type == EntityType.Folder && String.IsNullOrEmpty(Name)) ?
                                                                    "reports" :
                                                                    base.FullName);
            }
        }

        public override List<string> SearchNames
        {
            get
            {
                var selfNames = new List<String> { Name, Name + ".rdl" };
                var result = new List<String>();

                if (Scope == null)
                {
                    return selfNames;
                }

                result.AddRange(selfNames);
                foreach (var parentName in Scope.SearchNames)
                {
                    var name = parentName;
                    result.AddRange(selfNames.Select(n => name + ScopeDelimiter + n));
                }
                return result;
            }
        }

        #region Overrides of EntityBase

        public override bool AddDependency(IEntity dependency, EntityMatch match = null)
        {
            var result = base.AddDependency(dependency, match);
            if (dependency is ReportEntity &&
                dependency.Type == EntityType.DataSource &&
                match != null &&
                !_dataSources.ContainsKey(match.MatchText))
            {
                _dataSources.Add(match.MatchText, dependency);
            }
            return result;
        }

        #endregion

        protected override List<SearchContent> GetSearchContent()
        {
            var result = new List<SearchContent>();
            if (!String.IsNullOrEmpty(User))
            {
                result.Add(new SearchContent
                {
                    ContentType = EntityProviderType.Person,
                    Text = User
                });
            } 
            if (String.IsNullOrEmpty(Definition))
            {
                return result;
            }
            if (Type == EntityType.DataSource)
            {
                var match = Regex.Match(Definition.Trim(), "Data Source=(?<server>[^;]+);Initial Catalog=(?<database>[^;]+)($|;)", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    result.AddRange(new List<SearchContent>
                    {
                        new SearchContent
                        {
                            ContentType = EntityProviderType.SQL,
                            Text = (match.Groups["server"].Value.ToUpper() + "." + match.Groups["database"].Value).Trim()
                        }
                    });
                }
                return result;
            }
            if (Type != EntityType.Report)
            {
                return result;
            }

            try
            {
                var encodedString = Encoding.UTF8.GetBytes(Definition);

                XDocument definitionDoc;
                using (var ms = new MemoryStream(encodedString))
                {
                    definitionDoc = XDocument.Load(ms);
                }
                var ns = SSRSNamespace;
                if (definitionDoc.Root != null)
                {
                    ns = definitionDoc.Root.Name.Namespace.NamespaceName;
                }

                var subReports =
                    definitionDoc.Descendants(ns + "Subreport")
                        .SelectMany(s => s.Descendants(ns + "ReportName"))
                        .ToList();
                if (subReports.Count > 0)
                {
                    result.AddRange(subReports.Select(subReport => new SearchContent
                    {
                        ContentType = EntityProviderType.SSRS, Text = subReport.Value
                    }));
                }

                var reportsLinks =
                    definitionDoc.Descendants(ns + "Drillthrough")
                        .SelectMany(s => s.Descendants(ns + "ReportName"))
                        .ToList();
                if (reportsLinks.Count > 0)
                {
                    result.AddRange(reportsLinks.Select(reportLink => new SearchContent
                    {
                        ContentType = EntityProviderType.SSRS, Text = reportLink.Value
                    }));
                }

                definitionDoc.Descendants(ns + "DataSet").ToList().ForEach(s =>
                {
                    var element = s.Descendants(ns + "CommandText").SingleOrDefault();
                    if (element != null)
                    {
                        IEntity scope = null;
                        var ds = s.Descendants(ns + "DataSourceName").SingleOrDefault();

                        if (ds != null)
                        {
                            if (DataSources.ContainsKey(ds.Value))
                            {
                                var dataSource = DataSources[ds.Value];
                                scope = dataSource;
                            }
                            else
                            {
                                AddErrorFormat("Report {1} contains undefined datasource: {0}", ds.Value, FullName);
                            }
                        }

                        var commandTypeElement = s.Descendants(ns + "CommandType").SingleOrDefault();
                        var isCommand = commandTypeElement != null && commandTypeElement.Value == "StoredProcedure";

                        if (isCommand)
                        {
                            // Remove SSRS code
                            var str = String.Join(" ", Regex.Matches(element.Value.Trim(), @"([""'])(?:\\\1|.)*?\1",
                                RegexOptions.IgnoreCase | RegexOptions.Singleline).Cast<Match>().Select(m => m.Value));

                            // Extract stored procedures names
                            var matches = Regex.Matches(String.IsNullOrEmpty(str) ? element.Value.Trim() : str,
                                @"((?<name>(^[a-z0-9_\-]+$|([a-z0-9_\-\[\]]+\.)+([a-z0-9_\- ]+$|\[[a-z0-9_\- ]+\]|[a-z0-9_]+)))|" +
                                    @"(?<name>^([a-z0-9_\-\[\]\.~]+\.)?([a-z0-9_\- ~!%\$\#\]\[]+)$))",
                                RegexOptions.IgnoreCase);

                            if (matches.Count == 0)
                            {
                                var attr = s.Attribute(ns + "Name");
                                var sName = attr == null ? s.ToString() : attr.Value;

                                AddErrorFormat(
                                    "Stored procedure ws not detected in dataset {0} of the report {1}. dataSet definition: {2}",
                                    sName, FullName, element.Value.Trim());
                            }

                            foreach (Match match in matches)
                            {

                                var text = String.Join(".", match.Value.Split(new[] {'.'})
                                    .Select(p => p.Contains("[") ? p : ("[" + p + "]"))
                                    .ToArray());

                                result.Add(new SearchContent
                                {
                                    ContentType = EntityProviderType.SQL,
                                    Text = text,
                                    Scope = scope
                                });
                            }
                        }
                        else
                        {
                            result.Add(new SearchContent
                            {
                                ContentType = EntityProviderType.SQL,
                                Text = element.Value,
                                Scope = scope
                            });
                        }
                    }
                    else
                    {
                        AddErrorFormat(EntityErrorCode.ParsingError,
                            "Cannot extract command text of dataset '{3}' for {0} with name '{1}'. Definition: {2}",
                            TypeName, FullName, Definition, s.Attribute(ns + "Name").Value);
                    }
                });
            }
            catch (Exception e)
            {
                AddErrorFormat(EntityErrorCode.Unexpected,
                    "Cannot extract content for {0} with name '{1}'. Definition: {2}. Exception: {3}",
                    TypeName, FullName, Definition, e);
            }
            return result;
        }


        public ReportEntity(string name, EntityType type)
            : this(name, String.Empty, type)
        {

        }

        public ReportEntity(string name, string definition, EntityType type)
            : base(name, definition, ProviderName, type)
        {
            if (type == EntityType.DataSource)
            {
                Priority = 1;
            }
        }
        public ReportEntity(string name, string definition, EntityType type, string user)
            : this(name, definition, type)
        {
            User = user;
        }

        protected string User { get; set; }
    }
}