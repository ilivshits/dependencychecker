﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Entities
{
    public class P1TCEntity : EntityBase
    {
        private static Regex comments = new Regex(@"\/\*(.*?)\*\/", RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex onelineComments = new Regex(@"\/\/.*\r\n", RegexOptions.Compiled);
        private static Regex allComments = new Regex(@"\/\/[^\n]*\n", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public static readonly string ProviderName = EntityProviderType.P1TC.ToString();
        public override string InternalId { get; set; }

        public override IEntity Scope
        {
            get { return _scope; }
            set { _scope = value; }
        }

        public List<String> Prefixes { get; private set; } 

        public override List<string> SearchNames
        {
            get
            {
                if (Type == EntityType.P1TCView)
                {
                    return new List<String> { "P1TCView:" + Name };
                }
                if (Scope != null && Scope.Type == EntityType.P1TCEntity)
                {
                    var result = new List<String>();
                    foreach (var parentName in Scope.SearchNames)
                    {
                        var name = parentName;
                        result.AddRange(result.Select(n => name + ScopeDelimiter + n));
                    }
                    result.AddRange(Prefixes.Select(p => p + ScopeDelimiter + Name));
                    return result;
                }
                return new List<String> { Name };
            }
        }

        protected override List<SearchContent> GetSearchContent()
        {
            if (Type == EntityType.P1TCInstance || Type == EntityType.P1TCMipsCol)
            {
                return new List<SearchContent>
                {
                    new SearchContent
                    {
                        ContentType = EntityProviderType.SQL,
                        Text = Definition
                    }
                };
            }
            if (Type == EntityType.P1TCCalcCol || Type == EntityType.P1TCView)
            {
                var commentsRemoved = comments.Replace(onelineComments.Replace(Definition.Trim(), String.Empty).Replace("**", "*")
                                                                                                        .Replace("/**", "/*")
                                                                                                        .Replace("**/", "*/"), String.Empty);
                commentsRemoved = allComments.Replace(commentsRemoved, String.Empty);

                return new List<SearchContent>
                {
                    new SearchContent
                    {
                        ContentType = EntityProviderType.P1TC,
                        Text = commentsRemoved
                    }
                };
            }
            return new List<SearchContent>();
        }

        public P1TCEntity(string name, EntityType type)
            : base(name, ProviderName, type)
        {
            Prefixes = new List<string>();
        }

        public P1TCEntity(string name, string definition, EntityType type)
            : base(name, definition, ProviderName, type)
        {
            Prefixes = new List<string>();
        }

        protected P1TCEntity(string name, string definition, EntityType type, string providerName)
            : base(name, definition, providerName, type)
        {
            Prefixes = new List<string>();
        }
    }
}