﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Common;
using DependencyChecker.DataProvider;
using DependencyChecker.DataProvider.DAL.Repositories;
using DependencyChecker.DataProvider.Helpers;
using DependencyChecker.Entities;

namespace DependencyChecker.Collector
{
    public class ServerProvider : IServerProvider
    {
        private static Dictionary<string, List<string>> _configuration;
        #region Implementation of IServerProvider

        private Dictionary<EntityProviderType, List<string>> _servers = new Dictionary<EntityProviderType, List<string>>();
        public List<string> GetServers(EntityProviderType type)
        {
            if (_servers.ContainsKey(type))
            {
                return _servers[type];
            }

            if (_configuration == null)
            {
                ProviderItemRepository pItemRepository = new ProviderItemRepository(DatabaseHelpers.GetContext());
                _configuration = pItemRepository.GetConfiguration();
            }
            _servers.Add(type, _configuration[type.ToString()]);

            LoggerHelper.Log.DebugFormat("Servers for search:\r\n{0}", String.Join("\r\n", _servers[type].Select(s => "- " + s).ToArray()));
            return _servers[type];
        }

        public List<string> GetDomains()
        {
            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["domains"]))
            {
                return
                    ConfigurationManager.AppSettings["domains"].Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                        .ToList();
            }
            return new List<string>();
        }

        #endregion

        public ServerProvider()
        {
            
        }
    }
}