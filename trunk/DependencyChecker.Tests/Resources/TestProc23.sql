-- ========================================================================================================================
-- Author:             fortress\yzaryadov, Yury Zaryadov
-- Modified: 11/01/2013 by fortress\aboridko, Alexander Boridko  --PREP-288 Extendd restirciont by restriction Type 7 (like Options and Like Future)
-- Create date: 04/04/2013
-- Description: Migrated from MPS to P1TC  by fortress\aboridko.  Sp that return GSMA Country Block Positions
--                             Reports that use this sp: /Operations/GSMARestrictedSurveillance
-- ========================================================================================================================
CREATE PROCEDURE [operations].[spGetGSMACountryPositions]
(
        @asOfDate      datetime  = null,      
        @includeRepos INT = 0,
        @debug BIT = 0
)
AS
BEGIN
        SET NOCOUNT ON; 
        DECLARE @databaseName varchar(200)     = [dbo].[fnGetP1ReportingG2DatabaseName]();
        DECLARE @live varchar(1) = '1', @asOfDateParam varchar(100) = ''

        ---Remove comment this section to test the procedure---      
        --declare @asOfDate date, @debug bit,@includeRepos INT = 0
        --set @asOfDate                       = '12/10/2013'
        --set @debug                          = 1     
        --END TEST PARAMETER SECTION-----     
               
        IF OBJECT_ID('tempdb..#fund') > 0                                    DROP TABLE #fund
        IF OBJECT_ID('tempdb..#manager') > 0                         DROP TABLE #manager
        IF OBJECT_ID('tempdb..#strategy') > 0                        DROP TABLE #strategy
        IF OBJECT_ID('tempdb..#Positions') > 0                       DROP TABLE #Positions
        IF OBJECT_ID('tempdb..#Securities') > 0                              DROP TABLE #Securities
        IF OBJECT_ID('tempdb..#optionsAndFutures') > 0               DROP TABLE #optionsAndFutures
        IF OBJECT_ID('tempdb..#optionsAndFuturesSptTypes') > 0               DROP TABLE #optionsAndFuturesSptTypes
        IF OBJECT_ID('tempdb..#optionsAndFuturesOTC') > 0                    DROP TABLE #optionsAndFuturesOTC
        
        
               
        -----------------------------------------------------

        if( @asOfDate <> Convert(date,GetDate()) )
        begin   
               set @live = '0'
               set @asOfDateParam = ''''''+Convert(varchar,@asOfDate,21)+ ''''''         
        end
        else 
        begin
               set @live = '1'
               set @asOfDateParam = 'NULL'
        end 
        
        IF @debug = 1
        BEGIN
               print  '@live= ' +Convert(varchar(10),@live)
               print  '@asOfDateParam= ' +@asOfDateParam
        END
        --------------------------------------------------------------------
        CREATE TABLE #Positions
        (
               [sid]                          INT                            NOT NULL,
               [tid]                          INT                            NULL,
               [sptId]                        INT                            NOT NULL,
               [fundId]                       INT                            NOT NULL,
               [managerId]                    INT                            NOT NULL,
               [strategyId]           INT                            NOT NULL,
               [quantity]                     FLOAT                  NOT NULL,
               [clearBroker]          VARCHAR(max)   NOT NULL,
               [lastTradeDate]        DATE                   NOT NULL
        )

        CREATE TABLE #Securities
        (
               [sid]                                 INT                            NOT NULL,
               [country]                             VARCHAR(150)   NULL,
               [countryId]                           INT                            NULL,
               [secPrimaryTypeId]             INT                            NOT NULL,
               [sstId]                               INT                            NULL,
               [secTicker]                           VARCHAR(200)   NOT NULL,
               [secDescription]               VARCHAR(150)   NOT NULL,
               [sptDescription]               VARCHAR(150)   NOT NULL,
               [primaryCurrencyCode]  VARCHAR(25)            NULL
        )

        IF @debug = 1
        BEGIN
               DECLARE @countries VARCHAR(8000)  
               SELECT @countries = COALESCE(@countries + ',', '') + [countryId] FROM 
                       (SELECT DISTINCT CONVERT(VARCHAR,[CountryName]) as countryId FROM operations.GSMATradeRistrictionCountry) as tmp
               PRINT 'Countries to be filtered: ' + @countries
        END

        DECLARE @sql NVARCHAR(4000)    
        SET @sql = 'EXEC ['+@databaseName+'].[operations].[spGSMAPositions]
                                 @live = '+@live+'    
                               , @asOfDate =  '+@asOfDateParam+'
        ' ; 
        SET @sql  = N' 
        SELECT         
                sid
               , tid                          
               , sptId        
               , fundId
               , managerId
               , strategyId
               , quantity
               , clearBroker
               , lastTradeDate
        FROM OPENQUERY([SELF], ''' +  @sql + ''') AS p 
        WHERE p.quantity <> 0
        '

        IF @includeRepos = 0
               SET @sql = @sql + ' AND p.tid > 0'

        IF @debug = 1
               print @sql

        INSERT INTO #Positions
        EXEC sp_executesql @statement = @sql

        SET @sql = 'EXEC ['+@databaseName+'].[P1TC].[spPositionSecurityInfo]
                                 @live = '+@live+'    
                               , @asOfDate =  '+@asOfDateParam+'

        ' ; 

        SET @sql  = N' 
        SELECT         
                 sid
               , COALESCE(primaryExchangeCountry, primaryIncIssuerCountry, primaryCurrencyCountry) as country
               , COALESCE(primaryExchangeCountryId, primaryIncIssuerCountryId, primaryCurrencyCountryId) as countryId
               , secPrimaryTypeId
               , sstId
               , secTicker
               , secDescription
               , sptDescription
               , primaryCurrencyCode
        FROM OPENQUERY([SELF], ''' +  @sql + ''') AS p
        '
        IF @debug = 1
               print @sql

        INSERT INTO #Securities
        EXEC sp_executesql @statement = @sql
        --------------------------------------------------------------

        ---------------------------------
        --Dictionary data initialization...
        ---------------------------------
        -- Fund
        ---------------------------------
        -- initialization of #fund table with fields...
        SELECT * INTO #fund FROM [P1TC].[fnTableDefinitionFund]()                   
        INSERT INTO #fund EXEC P1TC.spFundTable

        ---------------------------------
        -- Manager
        ---------------------------------
        SELECT * INTO #manager FROM [P1TC].[fnTableDefinitionManager]() 
        INSERT INTO #manager EXEC P1TC.spManagerTable 
        --------------------------------------------------------------

        ---------------------------------
        -- strategy
        ---------------------------------
        SELECT strategyId, code INTO #strategy        FROM [P1G2].[FTBTrade].[dbo].[Strategy]

        
        ---------------------------------
        -- All Securities like Options of like Futures BUT OTC
        ---------------------------------
        
        create table #optionsAndFuturesSptTypes([sptId] int)         
        SET @sql = '
        select spt.securityPrimaryTypeId      
        from  FTBTrade.dbo.SecurityPrimaryType  spt 
        where    UPPER(spt.description) like ''''%OPTION%'''' or UPPER(spt.description) like ''''%FUTURE%''''          

               ' ; 
        SET @sql  = N' SELECT  securityPrimaryTypeId  FROM OPENQUERY([P1G2], ''' +  @sql + ''') AS p    '
        IF @debug = 1
               print @sql

        INSERT INTO #optionsAndFuturesSptTypes([sptId])
               EXEC sp_executesql @statement = @sql
               
        
        create table #optionsAndFuturesOTC ([sid] bigint)
        SET @sql = '
        create table #optionsAndFuturesSptTypes([sptId] int)
        insert into #optionsAndFuturesSptTypes(sptId)
        select spt.securityPrimaryTypeId      
        from  FTBTrade.dbo.SecurityPrimaryType  spt 
        where    UPPER(spt.description) like ''%OPTION%'' or UPPER(spt.description) like ''%FUTURE%''           

        SELECT  s.sid          
        FROM   FTBTrade.dbo.Security s
        JOIN #optionsAndFuturesSptTypes  spt ON s.securityPrimaryTypeId= spt.sptId  
        where  s.exchangeId <1000             
        drop table #optionsAndFuturesSptTypes 
        
               ' ; 
        --SET @sql  = N' SELECT  sid   FROM OPENQUERY([P1G2], ''' +  @sql + ''') AS p       '
        IF @debug = 1
               print @sql

        INSERT INTO #optionsAndFuturesOTC([sid])
               EXECUTE (@sql) AT [P1G2]
               --EXEC sp_executesql @statement = @sql

        create table #optionsAndFutures ([sid] bigint)
        insert into #optionsAndFutures([sid])
        select s.sid 
        from    #Securities s 
        join #optionsAndFuturesSptTypes spt on spt.sptId = s.secPrimaryTypeId
        where   s.sid not in (select o.sid from #optionsAndFuturesOTC o)
        

        ----------------------------------
        
        --------------------------------
        -- Printing Result 
        --------------------------------
        SELECT 
               f.[fund]                       as [Fund]
               , p.[sid]                                     as [SID]
               , p.[tid]                                     as [TID]
               , st.[code]                                   as [Strategy]
               , s.[country]                         as [Country]
               , s.[secTicker]                       as [Ticker]
               , s.[secDescription]           as [Instrument]
               , s.[sptDescription]           as [InstrumentType]
               , s.[primaryCurrencyCode]      as [Currency]
               , m.[managerDescription]       as [Manager]
               , SUM(p.quantity)                     as [Quantity]
               , p.[lastTradeDate]                   as [LastTradeDate]
        FROM #Positions AS p
        JOIN #Securities AS s ON (s.sid = p.sid)
        JOIN operations.GSMATradeRistrictionCountry cr 
               ON (cr.CountryId = s.countryId)
        left join #optionsAndFutures oafs ON oafs.[sid] = p.[sid]            -- Restriction by Sid where Security Type or security Sub  Type like  %Option% or %Future%
        JOIN operations.GSMATradeRistrictionIntrumentTypeGroup crg           -- Regular Restriction by Security Type  + By Sid where Type like  %Option% or %Future% 
               ON (cr.RestInstrumentGroupId = crg.RestInstrumentGroupId 

               AND (   (crg.SecurityPrimaryTypeId = s.secPrimaryTypeId AND (crg.SecuritySubTypeId IS NULL OR crg.SecuritySubTypeId = s.sstId))
                       OR      (cr.RestInstrumentGroupId = 7 and oafs.[sid] is not null)
                       )
               )       
        JOIN #fund AS f 
               ON (f.fundId = p.fundId)
        JOIN #manager AS m 
               ON (m.managerId = p.managerId)
        JOIN #strategy AS st 
               ON (st.strategyId = p.strategyId)
        WHERE
               s.primaryCurrencyCode collate Latin1_General_BIN2 NOT IN (SELECT UPPER(LTRIM(RTRIM(Items))) FROM dbo.fnStringSplit(cr.CurrNotIn,',')) 
               AND p.clearBroker collate Latin1_General_BIN2  NOT IN (SELECT UPPER(LTRIM(RTRIM(Items))) FROM dbo.fnStringSplit(crg.BrokerNotIn,',')) 
               AND (1 =  (
                       
                                              case when cr.LongShort = 'S' and p.quantity < 0 then 1 
                                                      when cr.LongShort = '*' then 1
                                                     else 0
                                              end
                                         )
                               )       
        GROUP BY 
               f.[fund]
               , p.[sid]
               , p.[tid]
               , s.[secTicker]
               , s.[secDescription]
               , p.[clearBroker]
               , st.[code]
               , s.[sptDescription]
               , s.[country]
               , m.[managerDescription]
               , s.[primaryCurrencyCode]
               , p.[lastTradeDate]    
END
