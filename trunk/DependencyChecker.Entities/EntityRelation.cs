﻿using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Entities
{
    public class EntityRelation
    {
        public IEntity Parent { get; set; }
        public IEntity Child { get; set; }
    }
}
