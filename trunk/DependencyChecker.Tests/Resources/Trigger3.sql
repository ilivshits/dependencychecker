﻿CREATE TRIGGER CS_COLUMN_CASCADE ON CS_COLUMN FOR DELETE AS
BEGIN
    DELETE CS_VIEW_TYPE_COLUMN FROM CS_VIEW_TYPE_COLUMN, DELETED
    WHERE CS_VIEW_TYPE_COLUMN.COL_ID = DELETED.COL_ID
    DELETE CS_VIEW_TYPE_COL_PROP FROM CS_VIEW_TYPE_COL_PROP, DELETED
    WHERE CS_VIEW_TYPE_COL_PROP.COL_ID = DELETED.COL_ID
    DELETE CS_VIEW_TYPE_DETERM_PROP FROM CS_VIEW_TYPE_DETERM_PROP, DELETED
    WHERE CS_VIEW_TYPE_DETERM_PROP.COL_ID = DELETED.COL_ID
    DELETE CS_VIEW_TYPE_COL_REQ FROM CS_VIEW_TYPE_COL_REQ, DELETED
    WHERE CS_VIEW_TYPE_COL_REQ.COL_ID = DELETED.COL_ID
END