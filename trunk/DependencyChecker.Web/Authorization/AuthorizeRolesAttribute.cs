﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using DependencyChecker.Common;
using DependencyChecker.DataProvider;
using DependencyChecker.DataProvider.Helpers;
using AuthorizeAttribute = System.Web.Mvc.AuthorizeAttribute;

namespace DependencyChecker.Web.Authorization
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        private string[] _allowedRoles;

        public AuthorizeRolesAttribute(params string[] roles) : base()
        {
            _allowedRoles = roles;
            
            Roles = string.Join(",", roles);
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!httpContext.User.IsUserInRoles(_allowedRoles))
            {
                throw new HttpException(401, "Unauthorized");
            }
            else
            {
                return true;
            }
        }
    }
}
