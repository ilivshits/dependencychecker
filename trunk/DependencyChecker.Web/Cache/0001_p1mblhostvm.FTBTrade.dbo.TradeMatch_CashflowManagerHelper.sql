CREATE view TradeMatch_CashflowManagerHelper as
select t.sid
	, t.managerId
	, t.legalEntityId
	, t.clearBrokerId
	, t.strategyId
	, t.quantity
	
	, le.fundId
	, t.orderId
	, o.businessUnitId
	, o.execBrokerId
	, liquidationId = l.orderId
	, t.tradeDate
	, t.settlementDate
	, t.liquidationTradeDate
	, t.liquidationSettlementDate

from [TradeMatch] t
join [LegalEntity] le on t.legalEntityId = le.legalEntityId
join [Order] o on t.orderId = o.orderId
join [TradeMatch] l on t.tradeMatchId = l.tradeMatchId and l.liquidationFlag = 1

