
CREATE FUNCTION [tagging].[BloombergSecurityValueSourceConfiguration_GetCalls]
(
	@sids dbo.Int64List readonly
)
RETURNS TABLE
AS
RETURN
	--- Select.
	select
		--- Taggable object ID.
		tt.Security as targetId,

		--- Grouping.
		t.contextId, t.tagId,

		--- Object ID and lookup object ID.
		t.sid as objectId, t.lookupSid as lookupObjectId,

		--- Bloomberg IDs.
		t.BloombergGlobalId, t.BloombergUniqueId, t.BloombergCodeLong,

		--- The name of the fields.
		flds.field as Field
	from
		(		
			--- Root, securities of interest
			select
				--- The sid that the values are being obtained for.
				t.sid,
				
				--- The lookup sid that is being used.
				coalesce(sr.relatedSid, t.sid) as lookupSid,
			
				--- Omit ordinal, get sub query values.
				t.securityValueSourceConfigurationId,
				t.contextId, t.tagId, 		
				t.bloombergFieldId,
				
				--- Bloomberg lookups.
				case when sr.sid is null then bgid.code else bgid_p.code end as BloombergGlobalId,
				case when sr.sid is null then buid.code else buid_p.code end as BloombergUniqueId,
				case when sr.sid is null then blc.code else blc_p.code end as BloombergCodeLong
			from
				(
					select							
						--- Ordinal.
						row_number() over (partition by sc.contextId, sc.tagId, soi.sid order by
							case when sc.sid != 0 then 0 else 1 end) as _Ordinal,
						
						--- Id.
						sc.securityValueSourceConfigurationId,
						
						--- Grouping.
						sc.contextId, sc.tagId, soi.sid,
						
						--- Lookup sid and Bloomberg configuration.
						bsc.bloombergFieldId
					from
						dbo.Security_GetSecurityHierarchy(@sids) as soi
							cross join ValueSource_Transposed as vs
							inner join [tagging].[SecurityValueSourceConfiguration] as sc on
								(sc.sid != 0 and sc.sid = soi.sid) or
								(sc.sid = 0 and sc.securityPrimaryTypeId = soi.securityPrimaryTypeId)
							inner join tagging.Tag as t on
								t.tagId = sc.tagId
							inner join tagging.BloombergSecurityValueSourceConfiguration as bsc on
								bsc.securityValueSourceConfigurationId = sc.securityValueSourceConfigurationId
					where
						t.valueSourceId = vs.[Bloomberg SAPI]
				) as t
					cross join [dbo].[IdentifierType_Transposed] as it
					left outer join [dbo].[SecurityIdentifier] as buid with (nolock) on
						buid.sid = t.sid and
						buid.identifierTypeId = it.[Bloomberg Unique]
					left outer join [dbo].[SecurityIdentifier] as bgid with (nolock) on
						bgid.sid = t.sid and
						bgid.identifierTypeId = it.[Bloomberg Global]
					left outer join [dbo].[SecurityIdentifier] as blc with (nolock) on
						blc.sid = t.sid and
						blc.identifierTypeId = it.[Bloomberg Code - long]
					cross join [dbo].[RelationshipType_Transposed] as rt
					left outer join [dbo].[SecurityRelationship] as sr with (nolock) on
						sr.sid = t.sid and
						sr.relationshipTypeId = rt.[Tagging Auto-Enrichment Identifier Proxy]
					left outer join [dbo].[SecurityIdentifier] as buid_p with (nolock) on
						buid_p.sid = sr.relatedSid and
						buid_p.identifierTypeId = it.[Bloomberg Unique]
					left outer join [dbo].[SecurityIdentifier] as bgid_p with (nolock) on
						bgid_p.sid = sr.relatedSid and
						bgid_p.identifierTypeId = it.[Bloomberg Global]
					left outer join [dbo].[SecurityIdentifier] as blc_p with (nolock) on
						blc_p.sid = sr.relatedSid and
						blc_p.identifierTypeId = it.[Bloomberg Code - long]
			where		
				t._Ordinal = 1
		) as t
			cross join Target_Transposed as tt
			inner join bbg.BloombergField as flds on
				flds.bloombergFieldId = t.bloombergFieldId
	where
		--- Filter out items that have no lookup.
		coalesce(t.BloombergGlobalId, t.BloombergUniqueId, t.BloombergCodeLong) is not null

