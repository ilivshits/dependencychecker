CREATE VIEW [dbo].[Target_Value] AS
SELECT [targetId] = 1, [objectId] = [sid], [display] = [description]
FROM [dbo].[Security] UNION ALL 
SELECT [targetId] = 3, [objectId] = [issuerId], [display] = [description]
FROM [dbo].[Issuer] UNION ALL 
SELECT [targetId] = 5, [objectId] = [managerId], [display] = [description]
FROM [dbo].[Manager] UNION ALL 
SELECT [targetId] = 6, [objectId] = [legalEntityId], [display] = [description]
FROM [dbo].[LegalEntity] UNION ALL 
SELECT [targetId] = 7, [objectId] = [brokerId], [display] = [code]
FROM [dbo].[Broker] UNION ALL 
SELECT [targetId] = 8, [objectId] = [exchangeId], [display] = [description]
FROM [dbo].[Exchange] UNION ALL 
SELECT [targetId] = 9, [objectId] = [tagId], [display] = [description]
FROM [tagging].[Tag] UNION ALL 
SELECT [targetId] = 10, [objectId] = [brokerBranchId], [display] = [description]
FROM [dbo].[BrokerBranch] UNION ALL 
SELECT [targetId] = 11, [objectId] = [countryId], [display] = [description]
FROM [dbo].[Country] UNION ALL 
SELECT [targetId] = 12, [objectId] = [businessUnitId], [display] = [description]
FROM [dbo].[BusinessUnit] UNION ALL 
SELECT [targetId] = 13, [objectId] = [moduleId], [display] = [description]
FROM [dbo].[Module] UNION ALL 
SELECT [targetId] = 14, [objectId] = [actionId], [display] = [description]
FROM [dbo].[Action] UNION ALL 
SELECT [targetId] = 15, [objectId] = [securityPrimaryTypeId], [display] = [description]
FROM [dbo].[SecurityPrimaryType] UNION ALL 
SELECT [targetId] = 16, [objectId] = [executionPlatformId], [display] = [description]
FROM [dbo].[ExecutionPlatform] UNION ALL 
SELECT [targetId] = 17, [objectId] = [fundId], [display] = [description]
FROM [dbo].[Fund2] UNION ALL 
SELECT [targetId] = 18, [objectId] = [reportingGroupId], [display] = [description]
FROM [dbo].[ReportingGroup] UNION ALL 
SELECT [targetId] = 19, [objectId] = [strategyId], [display] = [code]
FROM [dbo].[Strategy] UNION ALL 
SELECT [targetId] = 21, [objectId] = [securitySubTypeId], [display] = [description]
FROM [dbo].[SecuritySubType] UNION ALL 
SELECT [targetId] = 22, [objectId] = [identifierTypeId], [display] = [description]
FROM [dbo].[IdentifierType] UNION ALL 
SELECT [targetId] = 23, [objectId] = [userId], [display] = [fullName]
FROM [dbo].[User] UNION ALL 
SELECT [targetId] = 24, [objectId] = [viewDefinitionId], [display] = [description]
FROM [grid].[ViewDefinition] UNION ALL 
SELECT [targetId] = 25, [objectId] = [calculationDefinitionId], [display] = [description]
FROM [grid].[CalculationDefinition] UNION ALL 
SELECT [targetId] = 26, [objectId] = [lookupTableId], [display] = [name]
FROM [tagging].[LookupTable]

