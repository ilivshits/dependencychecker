



CREATE FUNCTION [dbo].[GetPrevIMMDate](@idate as datetime, @ipayFrequency as varchar(20))
RETURNS datetime
AS
BEGIN
	-- get next IMM date 
	DECLARE @date datetime;
	DECLARE @Year int;
	DECLARE @Month int;
	DECLARE @Day int;
	declare @dates table(tempdate varchar(10));
	
	select @Year = YEAR(@idate)
	select @Month = Month(@idate)
	select @Day = Day(@idate)

if(@ipayFrequency = 'Quarterly')
begin
	insert into @dates (tempdate) values( CAST(@Year as varchar(4)) + '/' + '03' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year as varchar(4)) + '/' + '06' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year as varchar(4)) + '/' + '09' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year as varchar(4)) + '/' + '12' + '/' + '20')
	
	insert into @dates (tempdate) values(  CAST(@Year-1 as varchar(4)) + '/' + '03' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year-1 as varchar(4)) + '/' + '06' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year-1 as varchar(4)) + '/' + '09' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year-1 as varchar(4)) + '/' + '12' + '/' + '20')
	
	insert into @dates (tempdate) values(  CAST(@Year+1 as varchar(4)) + '/' + '03' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year+1 as varchar(4)) + '/' + '06' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year+1 as varchar(4)) + '/' + '09' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year+1 as varchar(4)) + '/' + '12' + '/' + '20')
end
else if(@ipayFrequency = 'Semi-Annual')
begin
	insert into @dates (tempdate) values(  CAST(@Year as varchar(4)) + '/' + '06' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year as varchar(4)) + '/' + '12' + '/' + '20')
	
	insert into @dates (tempdate) values(  CAST(@Year-1 as varchar(4)) + '/' + '06' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year-1 as varchar(4)) + '/' + '12' + '/' + '20')
	
	insert into @dates (tempdate) values(  CAST(@Year+1 as varchar(4)) + '/' + '06' + '/' + '20')
	insert into @dates (tempdate) values(  CAST(@Year+1 as varchar(4)) + '/' + '12' + '/' + '20')
end	

	set @date = (select top 1 * from @dates where tempdate < @idate order by tempdate desc) 
		
	RETURN Convert(varchar(8), @date, 112);
END




