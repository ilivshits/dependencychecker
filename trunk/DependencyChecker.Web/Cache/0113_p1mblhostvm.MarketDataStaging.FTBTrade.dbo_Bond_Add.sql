

CREATE PROCEDURE [FTBTrade].[dbo_Bond_Add]
	--- Bond-specific
	@announceDate date,
	@series varchar(10),
	@issueDate date,
	@amountIssued float,
	@issuePrice float,
	@accrualStartDate date,
	@firstCouponDate date,
	@penultimateCouponDate date,
	@finalMaturityDate date,
	@maturityDate date,
	@dayCountConventionId int,
	@businessDayConventionId int,
	@businessDayConventionInterestAdjustmentId int,
	@frequencyId int,
	@daysBetweenPayments int,
	@faceValue float,
	@couponTypeId int,
	@coupon float,
	@couponCurrencySid bigint,
	@floatingRateBenchmarkMultiplier float,
	@daysBeforeCouponRefix int,
	@unadjustedBusinessDayConventionPayDateId int,
	@lastCouponResetDate date,
	@nextCouponDate date,
	@nextCouponResetDate date,
	@nextFactorDate date,
	@nextRefundDate date,
	@guarantorId int,
	@collateralTypeId int,
	@amountOutstanding float,
	@baseCpi float,
	@putableFlag bit,
	@discretelyPutableFlag bit,
	@putFeatureId int,
	@nextPutDate date,
	@nextPutPrice float,
	@nextParPutDate date,
	@callableFlag bit,
	@discretelyCallableFlag bit,
	@callFeatureId int,
	@calledFlag bit,
	@firstCallDate date,
	@calledDate date,
	@calledPrice float,
	@nextCallDate date,
	@nextCallPrice float,
	@nextParCallDate date,
	@bearerFlag bit,
	@defaultedFlag bit,
	@dtcEligibleFlag bit,
	@dtcRegisteredFlag bit,
	@dualCurrencyFlag bit,
	@exchangeableFlag bit,
	@extendibleFlag bit,
	@floaterFlag bit,
	@insuranceBackedFlag bit,
	@perpetualFlag bit,
	@reverseConvertibleFlag bit,
	@softCallFlag bit,
	@unitTradedFlag bit,
	@makeWholeCallFlag bit,
	@originalIssueDiscountFlag bit,
	@registeredWithTrusteeFlag bit,
	@sinkableFlag bit,
	@structuredNoteFlag bit,
	@daysToSettle int,
	@maturityForCalculationsDate date,
	@referenceIndexSid bigint,
	@inflationLinkedFlag bit,
	@privatePlacementFlag bit, 
	@quotedAsPercentageOfParFlag bit, 
	@previousCouponDate date, 
	@stepupDate date, 
	@stepupCoupon float,
	@quoteTypeId int,
	@spread float,
	@resetIndexSid bigint,
	@calculatedFloaterFlag bit,
	
	--- Security.
	@ticker	varchar(50),
	@description varchar(150),
	@currencySid bigint,
	@exchangeId int,
	@securityPrimaryTypeId int,
	@securitySubTypeId int,	
	@startDate datetime,
	@endDate datetime,
	@priceMultiplier float,
	@tradableFlag bit,
	@priceableFlag bit,
	@primaryFlag bit,
	@underlying_sid	bigint,
	@issuerId int,
	@lotSizeInitial float,
	@lotSizeIncrement float,
	@alternateDescription varchar(150),
		
	--- Identifiers.
	@idBloombergCodeShort varchar(100),
	@idBloombergCodeLong varchar(100),
	@idCusip varchar(100),
	@idSedol varchar(100),
	@idIsin varchar(100),
	@idRed6 varchar(100),
	@idRed9 varchar(100),
	@idTradeId varchar(100),
	@idBloombergCdsTicker5Y varchar(100),
	@idBloombergGlobal varchar(100),
	@idBloombergUnique varchar(100),
	@idBelgium varchar(100),
	@idCedelEuroclear varchar(100),
	@idDutch varchar(100),
	@idFrench varchar(100),
	@idValoren varchar(100),
	@idWertpapier varchar(100),
	@idOcc varchar(100),
	@idOpra varchar(100),
	@idBloombergTickerAndExchange varchar(100),
	@idAustrian varchar(100),
	@idCedel varchar(100),
	@idDanish varchar(100),
	@idEuroclear varchar(100),
	@idItaly varchar(100),
	@idJapan varchar(100),
	@idLuxembourg varchar(100),
	@idNorway varchar(100),
	@idSpain varchar(100),
	@idSwedish varchar(100),
	@idXtrakter varchar(100),
	@idBloombergCodeLocalExchange varchar(100),
	
	--- Restrictions.
	@restriction144A bit,
	@restrictionRegS bit,

	--- Industry classifications
	@bloombergIndustrySectorCode varchar(100),
	@bloombergIndustryGroupCode varchar(100),
	@bloombergIndustrySubgroupCode varchar(100),

	--- Auditing.
	@application_source varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- XACT_ABORT is used to abort the transaction in the face of
	-- anything.
	SET XACT_ABORT, NOCOUNT ON;
	
	--- Get the sid.
	declare @sid bigint
	exec [FTBTrade].[dbo_SequenceGenerator_GetNextSidBySecurityPrimaryType] @securityPrimaryTypeId, @sid output
	
	--- Wrap.
	begin try
		--- Begin the transaction.
		begin transaction

		--- Begin work.

		--- Add the records for the base security first.
		exec fortress.Security_Add @sid, @ticker, @description, @currencySid, @exchangeId, 
			@securityPrimaryTypeId, @securitySubTypeId, @startDate, @endDate, @priceMultiplier, 
			@tradableFlag, @priceableFlag, @primaryFlag, @underlying_sid, @issuerId, @lotSizeInitial, @lotSizeIncrement,
			@idBloombergCodeShort, @idBloombergCodeLong, @idCusip, @idSedol, @idIsin, @idRed6, @idRed9, @idTradeId, 
			@idBloombergCdsTicker5Y, @idBloombergGlobal, @idBloombergUnique, @idBelgium, @idCedelEuroclear, @idDutch, 
			@idFrench, @idValoren, @idWertpapier, @idOcc, @idOpra, @idBloombergTickerAndExchange, 
			@idAustrian, @idCedel, @idDanish, @idEuroclear, @idItaly, @idJapan, @idLuxembourg, @idNorway, @idSpain, 
			@idSwedish, @idXtrakter, @restriction144A, @restrictionRegS,
			@application_source, @alternateDescription, @idBloombergCodeLocalExchange,
			@bloombergIndustrySectorCode, @bloombergIndustryGroupCode, @bloombergIndustrySubgroupCode
			
		--- Insert the record.
		insert into FTBTrade.dbo_Bond (
			sid, announceDate, series, issueDate, amountIssued, issuePrice, accrualStartDate, 
			firstCouponDate, penultimateCouponDate, maturityDate, 
			dayCountConventionId, businessDayConventionId, businessDayConventionInterestAdjustmentId, frequencyId, 
			daysBetweenPayments, faceValue, couponTypeId, coupon, couponCurrencySid, floatingRateBenchmarkMultiplier,
			lastCouponResetDate, nextCouponDate, nextCouponResetDate, nextFactorDate, nextRefundDate,
			daysBeforeCouponRefix, unadjustedBusinessDayConventionPayDateId, collateralTypeId, guarantorId, 
			amountOutstanding, baseCpi, 
			putableFlag, discretelyPutableFlag, putFeatureId, nextPutDate, nextPutPrice, nextParPutDate,
			callableFlag, discretelyCallableFlag, callFeatureId, calledFlag, firstCallDate, calledDate, calledPrice, 
			nextCallDate, nextCallPrice, nextParCallDate,
			bearerFlag, defaultedFlag, dtcEligibleFlag, dtcRegisteredFlag, dualCurrencyFlag, exchangeableFlag, extendibleFlag, floaterFlag, insuranceBackedFlag, 
			perpetualFlag, reverseConvertibleFlag, softCallFlag, unitTradedFlag, makeWholeCallFlag, 
			originalIssueDiscountFlag, registeredWithTrusteeFlag, 
			sinkableFlag, structuredNoteFlag, daysToSettle, maturityForCalculationsDate, referenceIndexSid, inflationLinkedFlag,
			privatePlacementFlag, quotedAsPercentageOfParFlag, previousCouponDate, stepupDate, stepupCoupon,
			quoteTypeId, spread, resetIndexSid, calculatedFloaterFlag, finalMaturityDate,
			application_source
		) values (
			@sid, @announceDate, @series, @issueDate, @amountIssued, @issuePrice, @accrualStartDate, 
			@firstCouponDate, @penultimateCouponDate, @maturityDate, 
			@dayCountConventionId, 	@businessDayConventionId, @businessDayConventionInterestAdjustmentId, @frequencyId, 
			@daysBetweenPayments, @faceValue, @couponTypeId, @coupon, @couponCurrencySid, @floatingRateBenchmarkMultiplier,
			@lastCouponResetDate, @nextCouponDate, @nextCouponResetDate, @nextFactorDate, @nextRefundDate,
			@daysBeforeCouponRefix, @unadjustedBusinessDayConventionPayDateId, @collateralTypeId, @guarantorId, 
			@amountOutstanding, @baseCpi, 
			@putableFlag, @discretelyPutableFlag, @putFeatureId, @nextPutDate, @nextPutPrice, @nextParPutDate,
			@callableFlag, @discretelyCallableFlag, @callFeatureId, @calledFlag, @firstCallDate, @calledDate, @calledPrice, 
			@nextCallDate, @nextCallPrice, @nextParCallDate,
			@bearerFlag, @defaultedFlag, @dtcEligibleFlag, @dtcRegisteredFlag, @dualCurrencyFlag, @exchangeableFlag, @extendibleFlag, @floaterFlag, @insuranceBackedFlag, 
			@perpetualFlag, @reverseConvertibleFlag, @softCallFlag, @unitTradedFlag, @makeWholeCallFlag, 
			@originalIssueDiscountFlag, @registeredWithTrusteeFlag, 
			@sinkableFlag, @structuredNoteFlag, @daysToSettle, @maturityForCalculationsDate, @referenceIndexSid, @inflationLinkedFlag,
			@privatePlacementFlag, @quotedAsPercentageOfParFlag, @previousCouponDate, @stepupDate, @stepupCoupon,
			@quoteTypeId, @spread, @resetIndexSid, @calculatedFloaterFlag, @finalMaturityDate,
			@application_source
		)
		
		--- Return the sid.
		select @sid
		
		--- End work.

		--- Commit the transaction.
		commit transaction
	end try
	begin catch
		--- Store the error message, etc.
		declare @errMessage nvarchar(max) = error_message()
		declare @errSeverity int = error_severity()
		declare @errState int = error_state()

		--- Rollback if in a transaction state.
		--- Possible if something happened between begin try and 
		--- begin transaction.
		if (xact_state() <> 0) rollback transaction

		--- Raise the error again.
		raiserror(@errMessage, @errSeverity, @errState)
	end catch
END



