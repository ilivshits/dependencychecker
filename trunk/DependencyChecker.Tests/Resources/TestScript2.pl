﻿use DBI;
use POSIX 'strftime';
use Net::SMTP;
use MIME::Lite;

require '..\Globals.pl';


my $PnLDir = '\\\\ny-files1\\Groups\\HedgeNonRes\\GMIT\\PNLGroup\\MarkitData';
my $DB_SERVER = "NY-GMITMPS1";
my $dsn = "Provider=sqloledb;Integrated Security=SSPI;";
$dsn .= "Server=${DB_SERVER};Database=FortressShared;CommandTimeout=0";
my $dbh = DBI->connect("dbi:ADO:$dsn",  "", "") || die "Database connection not made: $DBI::errstr";

my $date = strftime '%Y%m%d', localtime;

my $sql = qq{ exec FTB.P1Reporting.operations.AutoMarkCDS 
              SELECT tid, spread, upfront, recovery from cds_marks where date = '$date' order by tid} ;
my $sth = $dbh->prepare( $sql );
$sth->execute();

my $ret = open(OUTFILE, ">" . $PnLDir ."\\attribute.marks_new.csv");
if (!$ret)
 { 
	$EMAIL_SUBJ = "CDS attributes file loaded fail(Can't open the attribute.marks_new.csv File)";
	email_notify($EMAIL_SUBJ); 
	die "Can't open the attribute.marks_new.csv File"; 
 }
#print OUTFILE join(", ", @{$sth->{NAME}}), "\n";
my @row;
while (@row = $sth->fetchrow_array) 
{ 
    if($row[2])
    {
       print OUTFILE "f, ", $row[0],  ", 2005, 500\n";
       printf OUTFILE "f, %d, 2006, %.4f\n", $row[0], $row[2];
    }
    else
    {
       printf OUTFILE "f, %d, 2005, %.2f\n", $row[0], $row[1];
    } 
    if($row[3])
    {
       printf OUTFILE "f, %d, 2015, %.2f\n", $row[0], $row[3];
    }
}
close(OUTFILE);

$EMAIL_SUBJ = "CDS attributes file has been loaded for PnL group";

email_notify($EMAIL_SUBJ);

sub email_notify
{
  my($email_sub) = @_;

$EMAIL_TO = 'Group_PnL@fortress.com';
$EMAIL_FROM = 'GMIT@fortress.com';
$smtp = Net::SMTP->new($EMAIL_SVR);

$type = "TEXT";
$mime = MIME::Lite->new(From => $EMAIL_FROM, To => $EMAIL_TO, Subject => $email_sub, Type => $type, Data => "");
MIME::Lite->send('smtp', $EMAIL_SVR);
$mime->send();
}

