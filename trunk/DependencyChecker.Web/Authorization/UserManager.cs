﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DependencyChecker.DataProvider;
using DependencyChecker.DataProvider.Helpers;
using DependencyChecker.Web.Models;

namespace DependencyChecker.Web.Authorization
{
    public static class UserManager
    {
        public static void Remove(string username)
        {
            using (var ctx = DatabaseHelpers.GetContext())
            {
                var user = ctx.User.First(e => e.Username == username);
                ctx.UserRole.RemoveRange(ctx.UserRole.Where(e => e.User == user.Id));
                ctx.SaveChanges();

                ctx.User.Remove(user);
                ctx.SaveChanges();


                // ctx.UserRole.RemoveRange(ctx.UserRole.Where)
            }
        }

        public static List<AdUser> GetAllUsers()
        {
            using (ActiveDirectoryContext adCtx = new ActiveDirectoryContext())
            {
                Dictionary<string, string> users = new Dictionary<string, string>();
                using (var ctx = DatabaseHelpers.GetContext())
                {
                    var userList = ctx.User.ToList()
                        .Select(e => new KeyValuePair<string, string>(e.Username, String.Join(", ", GetRolesForUser(ctx, e.Id)))).ToList();

                    foreach (var user in userList)
                    {
                        users.Add(user.Key, user.Value);
                    }
                }

                var list = adCtx.GetUsersFilter(users.Keys.ToList());
                foreach (var user in list)
                {
                    user.Role = users[user.Username];
                }
                return list.OrderBy(e => e.Username).ToList();
            }
        }

        public static List<string> GetRolesForUser(DependencyCheckerEntities ctx, short userId)
        {
            return ctx.UserRole.Include(e => e.Role1).Where(e => e.User == userId).Select(e=>e.Role1.RoleName).ToList();
        }

        public static bool CheckUserExistence(string username)
        {
            using (ActiveDirectoryContext adCtx = new ActiveDirectoryContext())
            {
                return adCtx.IsUserExisting(username);
            }
        }

        public static void AddAdministrator(string username)
        {
            using (var ctx = DatabaseHelpers.GetContext())
            {
                using (ActiveDirectoryContext adCtx = new ActiveDirectoryContext())
                {
                    var domain = adCtx.GetDomain();
                    username = domain + '\\' + username;

                    if (ctx.User.FirstOrDefault(e => e.Username == username) != null)
                    {
                        throw new Exception("User already in base");
                    }
                }

                var role = ctx.Role.First(e => e.RoleName == "Administrator");
                short roleId = role.Id;

                ctx.User.Add(new User()
                {
                    Username = username
                });
                ctx.SaveChanges();

                var user = ctx.User.First(e => e.Username == username);

                ctx.UserRole.Add(new UserRole()
                {
                    Role = roleId,
                    User = user.Id
                });
                ctx.SaveChanges();
            }
        }
    }
}
