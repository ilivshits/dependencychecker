
create view [fortress].[v_SecuritySubType_Pivot] as
	select
		 0 as None,
		 1 as SingleName,
		 2 as [Index],
		 3 as Sovereign,
		 4 as IndexTranche,
		 5 as BondNote,
		 6 as CommonStock,
		 7 as DepositaryReceipt,
		 8 as MutualFund,
		 9 as PartnershipShares,
		10 as Preferred,
		11 as RealEstateInvestmentTrust,
		12 as [Right],
		13 as Unit
		
		


