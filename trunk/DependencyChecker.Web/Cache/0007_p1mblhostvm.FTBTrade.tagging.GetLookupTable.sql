
CREATE procedure [tagging].[GetLookupTable](@tableName varchar(200), @perspectives varchar(max) = null)
as
begin
	set nocount on;
	set xact_abort on;
	
	-- declare dynamic sql var
	declare @sql varchar(max)
	
	-- extract basic items for the table
	declare @tableId int, @inputCount int, @perspectiveTargetId int
	select @tableId = lookupTableId,
		@inputCount = case
		when inputTypeId4 != 0 then 4
		when inputTypeId3 != 0 then 3
		when inputTypeId2 != 0 then 2
		else 1 end,
		@perspectiveTargetId = perspectiveTargetId
	from tagging.LookupTable
	where name = @tableName

	-- extract perspective names
	create table #perspectivesTable (
		columnId int not null,
		baseTypeId int not null,
		objectId int not null,		
		perspective varchar(max) collate database_default not null
	)
	
	-- resolve perspectives
	if @perspectives is null
	begin
		
		-- add all columns with perspectives
		insert into #perspectivesTable(columnId, baseTypeId, objectId, perspective)
		select lookupTableColumnId, baseTypeId, objectId, isnull(perspective, '')
		from tagging.LookupTableColumn c
		join tagging.Type t on c.outputValueTypeId = t.typeId
		where lookupTableId = @tableId
		
		-- enrich perspectives if coming from target
		if @perspectiveTargetId != 0
		begin
			-- extract target information
			select @sql =
				'update p
				set perspective = t.[' + displayColumn + ']
				from #perspectivesTable p
				join [' + schemaName + '].[' + tableName + '] t on t.[' + primaryColumn + '] = p.objectId'				
			from [Target] t
			where targetId = @perspectiveTargetId
			exec(@sql)
		end
	
	end
	else
	begin
	
		-- extract perspective names from parameter
		insert into #perspectivesTable(columnId, baseTypeId, objectId, perspective)
		select 0, 0, 0, ltrim(rtrim(Items)) from fun_Split(@perspectives, ',')

		-- map perspectves to columns
		if @perspectiveTargetId != 0
		begin
			-- extract target information
			select @sql =
				'update p
				set columnId = lookupTableColumnId, baseTypeId = ot.baseTypeId, objectId = c.objectId
				from #perspectivesTable p
				join [' + schemaName + '].[' + tableName + '] t on t.[' + displayColumn + '] = p.perspective
				join tagging.LookupTableColumn c on c.objectId = t.[' + primaryColumn + ']
				join tagging.Type ot on c.outputValueTypeId = ot.typeId'
			from [Target] t
			where targetId = @perspectiveTargetId			
			exec(@sql)
		end
		else
		begin
			-- perspective names are free form
			update p
			set columnId = lookupTableColumnId, baseTypeId = t.baseTypeId
			from #perspectivesTable p
			join tagging.LookupTableColumn c on p.perspective = c.perspective
			join tagging.Type t on c.outputValueTypeId = t.typeId
		end
	end
	
	select 
		rowId = r.lookupTableRowId
		,columnId = c.lookupTableColumnId
		
		,inputTypeName = it.description
		,inputTypeId = it.typeId
		,inputBaseTypeId = it.baseTypeId
		,[input] = isnull(cast(iop.value as sql_variant), r.inputValue)
		,[input_id] = iop.userListOptionId
	    
		,inputTypeName2 = it2.description
		,inputTypeId2 = it2.typeId
		,inputBaseTypeId2 = it2.baseTypeId
		,[input2] = isnull(cast(iop2.value as sql_variant), r.inputValue2)
		,[input2_id] = iop2.userListOptionId
	    
		,inputTypeName3 = it3.description
		,inputTypeId3 = it3.typeId
		,inputBaseTypeId3 = it3.baseTypeId
		,[input3] = isnull(cast(iop3.value as sql_variant), r.inputValue3)
		,[input3_id] = iop3.userListOptionId
	    
		,inputTypeName4 = it4.description
		,inputTypeId4 = it4.typeId
		,inputBaseTypeId4 = it4.baseTypeId
		,[input4] = isnull(cast(iop4.value as sql_variant), r.inputValue4)	    
		,[input4_id] = iop4.userListOptionId
	    
		, outputTypeName = ot.description
		, outputTypeId = ot.typeId
		, outputBaseTypeId = ot.baseTypeId
		, output_Int = v.value_Int
		, output_Decimal = v.value_Decimal
		, output_DateTime = v.value_DateTime
		, output_Text = isnull(cast(oop.value as varchar(max)), v.value_Text)
		, output_userListOptionId = oop.userListOptionId
		, output_systemListPrimaryId = null
	into 
		#lookups
	from 
		tagging.LookupTable t	
			join tagging.LookupTableRow r on t.lookupTableId = r.lookupTableId	
			join tagging.LookupTableColumn c on t.lookupTableId = c.lookupTableId
			join #perspectivesTable p on c.lookupTableColumnId = p.columnId	
			left join tagging.LookupTableCell v on r.lookupTableRowId = v.lookupTableRowId and c.lookupTableColumnId = v.lookupTableColumnId
			cross join tagging.BaseType_Transposed as bt
			join tagging.Type ot on c.outputValueTypeId = ot.typeId
			left join tagging.UserListOption oop on v.value_Int = oop.userListOptionId and ot.baseTypeId = bt.[User List]
			join tagging.Type it on t.inputTypeId = it.typeId
			left join tagging.UserListOption iop on r.inputValue = iop.userListOptionId and it.baseTypeId = bt.[User List]
			join tagging.Type it2 on t.inputTypeId2 = it2.typeId
			left join tagging.UserListOption iop2 on r.inputValue2 = iop2.userListOptionId and it2.baseTypeId = bt.[User List]
			join tagging.Type it3 on t.inputTypeId3 = it3.typeId
			left join tagging.UserListOption iop3 on r.inputValue3 = iop3.userListOptionId and it3.baseTypeId = bt.[User List]
			join tagging.Type it4 on t.inputTypeId4 = it4.typeId
			left join tagging.UserListOption iop4 on r.inputValue4 = iop4.userListOptionId and it4.baseTypeId = bt.[User List]
	where 
		t.lookupTableId = @tableId

	-- update system list items
	set @sql = null
	select @sql = isnull(@sql, '') + '		
		update l
		set 
			output_Text = t.[' + displayColumn + '],
			output_systemListPrimaryId = t.[' + primaryIdColumn + ']
		from #lookups l
		cross join tagging.BaseType_Transposed
		join [' + tableName + '] t on l.output_Int = t.[' + primaryIdColumn + ']
		where l.outputBaseTypeId = [System List] and l.outputTypeId = ' + cast(typeId as varchar) + '
		
		update l
		set 
			l.[input] = t.[' + displayColumn + ']
		from #lookups l
		cross join tagging.BaseType_Transposed
		join [' + tableName + '] t on t.[' + primaryIdColumn + '] = l.input
		where l.inputBaseTypeId = [System List] and l.inputTypeId = ' + cast(typeId as varchar)
		
		+ case when @inputCount >= 2 then
		'update l
		set l.[input2] = t.[' + displayColumn + ']
		from #lookups l
		cross join tagging.BaseType_Transposed
		join [' + tableName + '] t on t.[' + primaryIdColumn + '] = l.input2
		where l.inputBaseTypeId2 = [System List] and l.inputTypeId2 = ' + cast(typeId as varchar)
		else '' end
		
		+ case when @inputCount >= 3 then
		'update l
		set l.[input3] = t.[' + displayColumn + ']
		from #lookups l
		cross join tagging.BaseType_Transposed
		join [' + tableName + '] t on t.[' + primaryIdColumn + '] = l.input3
		where l.inputBaseTypeId3 = [System List] and l.inputTypeId3 = ' + cast(typeId as varchar)
		else '' end
		
		+ case when @inputCount >= 4 then
		'update l
		set l.[input4] = t.[' + displayColumn + ']
		from #lookups l
		cross join tagging.BaseType_Transposed
		join [' + tableName + '] t on t.[' + primaryIdColumn + '] = l.input4
		where l.inputBaseTypeId4 = [System List] and l.inputTypeId4 = ' + cast(typeId as varchar)
		else '' end
		
	from tagging.SystemList s
	where typeId in
	(
		select outputTypeId from #lookups union all
		select inputTypeId from #lookups union all
		select inputTypeId2 from #lookups union all
		select inputTypeId3 from #lookups union all
		select inputTypeId4 from #lookups
	)
	exec (@sql)

	-- construct the final sql
	set @sql = '
		from tagging.LookupTableRow r ' + 
		' inner join (select distinct rowId, input, input2, input3, input4 from #lookups) as i on i.rowId = r.lookupTableRowId '
	declare @select varchar(max) = 'select ' + (select distinct
		'[input] = cast(r.[inputValue] as ' +
			case inputBaseTypeId
				when [Bool] then 'bit'
				when [Integer] then 'bigint'
				when [Decimal] then 'decimal(19, 8)'
				when [Datetime] then 'datetime'
				when [Date] then 'date'
				when [Time] then 'time'
				when [System List] then 'bigint'
				when [User List] then 'bigint'
				else 'varchar(max)' 
			end + ')' +
		case when l.inputBaseTypeId in (bt.[User List], bt.[System List]) then ', [input_displayValue] = cast(i.input as varchar(max))' else '' end +
		case when @inputCount >= 2 then ', [input2] = cast(r.[inputValue2] as ' +
			case inputBaseTypeId2
				when [Bool] then 'bit'
				when [Integer] then 'bigint'
				when [Decimal] then 'decimal(19, 8)'
				when [Datetime] then 'datetime'
				when [Date] then 'date'
				when [Time] then 'time'
				when [System List] then 'bigint'
				when [User List] then 'bigint'
				else 'varchar(max)' 
			end + ')'
		else '' end +
		case when @inputCount >= 2 and l.inputBaseTypeId2 in (bt.[User List], bt.[System List]) then ', [input2_displayValue] = cast(i.input2 as varchar(max))' else '' end +
		case when @inputCount >= 3 then ', [input3] = cast(r.[inputValue3] as ' +
			case inputBaseTypeId3
				when [Bool] then 'bit'
				when [Integer] then 'bigint'
				when [Decimal] then 'decimal(19, 8)'
				when [Datetime] then 'datetime'
				when [Date] then 'date'
				when [Time] then 'time'
				when [System List] then 'bigint'
				when [User List] then 'bigint'
				else 'varchar(max)' 
			end + ')'		
		else '' end +
		case when @inputCount >= 3 and l.inputBaseTypeId3 in (bt.[User List], bt.[System List]) then ', [input3_displayValue] = cast(i.input3 as varchar(max))' else '' end +
		case when @inputCount >= 4 then ', [input4] = cast(r.[inputValue4] as ' +
			case inputBaseTypeId4
				when [Bool] then 'bit'
				when [Integer] then 'bigint'
				when [Decimal] then 'decimal(19, 8)'
				when [Datetime] then 'datetime'
				when [Date] then 'date'
				when [Time] then 'time'
				when [System List] then 'bigint'
				when [User List] then 'bigint'
				else 'varchar(max)' 
			end + ')'				
		else '' end +
		case when @inputCount >= 4 and l.inputBaseTypeId4 in (bt.[User List], bt.[System List]) then ', [input4_displayValue] = cast(i.input4 as varchar(max))' else '' end
	from
		#lookups as l
			cross join tagging.BaseType_Transposed bt)

	select
		@select = @select + ', [' + perspective + '] = ' +
			case baseTypeId
				when [Bool] then 'cast(p' + cast(columnId as varchar) + '.output_Int as bit)'
				when [Integer] then 'p' + cast(columnId as varchar) + '.output_Int'
				when [Decimal] then 'p' + cast(columnId as varchar) + '.output_Decimal'
				when [Datetime] then 'cast(p' + cast(columnId as varchar) + '.output_DateTime as datetime)'
				when [Date] then 'cast(p' + cast(columnId as varchar) + '.output_DateTime as date)'
				when [Time] then 'convert(time, p' + cast(columnId as varchar) + '.output_DateTime)'
				else 'p' + cast(columnId as varchar) + '.output_Text' 
			end +
			case baseTypeId
				when [User List] then ', [' + perspective + '_id] = p' + cast(columnId as varchar) + '.output_userListOptionId'
				when [System List] then ', [' + perspective + '_id] = p' + cast(columnId as varchar) + '.output_systemListPrimaryId'
				else ''
			end,
		@sql = @sql + '
		left join #lookups p' + cast(columnId as varchar) +
		' on r.lookupTableRowId = p' + cast(columnId as varchar) +
		'.rowId and p'+cast(columnId as varchar)+'.columnId = ' + cast(columnId as varchar)
	from #perspectivesTable
	cross join tagging.BaseType_Transposed bt
    set @sql = @select + @sql + ' where r.lookupTableId = ' + cast(@tableId as varchar)
	
	exec(@sql)

	drop table #perspectivesTable	
	drop table #lookups
end



