﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using DependencyChecker.Analyzer;
using DependencyChecker.Analyzer.Interfaces;
using DependencyChecker.Collector;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Interfaces;
using log4net.Config;

namespace DependencyChecker.Console
{
    class Program
    {
        protected static readonly log4net.ILog Log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            System.Console.WriteLine("Loading application. Please wait...");
            var inputNames = new List<string>();
            XmlConfigurator.Configure(new FileInfo("log4net.config"));

            // Initial dictionary load
            var serversProvider = new ServerProvider();
            var sqlDataLoader = new SQLDataProvider(serversProvider);
            var reportsDataLoader = new ReportsDataProvider(serversProvider);
            var olapDataLoader = new OlapDataProvider(serversProvider);
            var tasksLoader = new ScheduledTasksDataProvider(serversProvider);
            var servers = new List<IEntity>();
            //servers.AddRange(tasksLoader.Search());
            //servers.AddRange(reportsDataLoader.Search());
            //servers.AddRange(olapDataLoader.Search());
            servers.AddRange(sqlDataLoader.Search());

            // Dependency building
            var analyzer = new Analyzer.Analyzer(servers, new List<IDataProvider> { sqlDataLoader, olapDataLoader, reportsDataLoader }, new ResolverFactory());
            analyzer.Analyze();

            if (!Directory.Exists("output"))
            {
                Directory.CreateDirectory("output");
            }

            ReadAndProcessInput(analyzer);
        }

        private static void ReadAndProcessInput(IAnalyzer analyzer)
        {
            if (!Directory.Exists("output"))
            {
                Directory.CreateDirectory("output");
            }
            System.Console.WriteLine("Please enter object name and press [Enter]. Or type 'exit' to exit:");

            var input = new StringBuilder();
            var matches = new Dictionary<string, List<IEntity>>();
            var index = -1;
            var lastPrinted = String.Empty;
            IEntity selectedItem = null;
            while (true)
            {
                var newKey = System.Console.ReadKey(true);

                if (newKey.Key == ConsoleKey.Enter)
                {
                    if (String.Equals(input.ToString(), "exit", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return;
                    }
                    try
                    {
                        HandleInput(analyzer, input.ToString(), selectedItem);
                    }
                    catch (Exception e)
                    {
                        Log.ErrorFormat("Execption occured: {0}", e);
                    }
                    selectedItem = null;
                    index = -1;
                    input = new StringBuilder();
                    System.Console.WriteLine();
                    System.Console.WriteLine("Please enter object name and press [Enter]. Or type 'exit' to exit:");
                }
                else if (newKey.Key == ConsoleKey.Tab)
                {
                    if (!matches.ContainsKey(input.ToString()))
                    {
                        matches.Add(input.ToString(), analyzer.Search(input.ToString()));
                    }
                    if (matches[input.ToString()].Count > 0)
                    {
                        ClearLine();
                        index++;
                        if (index >= matches[input.ToString()].Count)
                        {
                            index = -1;
                            selectedItem = null;
                            System.Console.Write(input);
                        }
                        else
                        {
                            selectedItem = matches[input.ToString()][index];
                            lastPrinted = selectedItem.FullName;
                            System.Console.Write(lastPrinted);
                        }
                    }
                }
                else if (newKey.Key != ConsoleKey.End &&
                         newKey.Key != ConsoleKey.Delete &&
                         newKey.Key != ConsoleKey.LeftArrow &&
                         newKey.Key != ConsoleKey.RightArrow)
                {
                    if (selectedItem != null)
                    {
                        input = new StringBuilder(lastPrinted);
                        selectedItem = null;
                        index = -1;
                    }
                    if (newKey.Key == ConsoleKey.Backspace)
                    {
                        if (input.Length >= 1)
                        {
                            input = new StringBuilder(input.ToString(0, input.Length - 1));
                        }
                        ClearLine();
                        System.Console.Write(input);
                    }
                    else
                    {
                        input.Append(newKey.KeyChar);
                        System.Console.Write(newKey.KeyChar);
                    }
                }
            }
        }

        private static void ClearLine()
        {
            var lineNumber = System.Console.CursorTop;
            System.Console.SetCursorPosition(0, System.Console.CursorTop);
            System.Console.Write(new string(' ', System.Console.WindowWidth));
            System.Console.SetCursorPosition(0, lineNumber);
        }

        private static void HandleInput(IAnalyzer analyzer, string input, IEntity selectedEntity = null)
        {
            if (selectedEntity != null)
            {
                selectedEntity.ExportToFile();
                return;
            }
            var result = File.Exists(input) ? File.ReadAllLines(input).ToList() : input.Split(new[] { ';' }).ToList();

            if (input.Trim() == "/?")
            {
                System.Console.WriteLine(@"Help:
    ([command]|[entity list]|[entities file]) (options)
    [command] - List of commands:
                export:     exports to GraphML
                            Usage:
                                export: [filename] [(objects)] [(-u|-d)]
                            Options:
                                [filename]  - destination file
                                [objects]   - list of objects names separated with semicolon. Default: all objects
                                -u          - prints all objects which use selected object. Default: false
                                -d          - prints dependencies. Default: true
                cleanup:    builds cleanup scripts basing on selected rule
                            Usage:
                                cleanup: (mode) -f:(filename)
                            Options:
                                mode        - Possible values: 
                                                names   - generates cleanup scripts to remove all objects which depend on NON-EXISTING enumerated names
                                                pattern - generates cleanup scripts to remove all objects which match any pattern provided
                                                unused  - generates cleanup scripts to remove all unused SQL objects
                                filename    - file with names of dependencies or patterns separated with new line");
            }

            if (input.StartsWith("export:"))
            {
                System.Console.WriteLine();
                System.Console.WriteLine("Export to GraphML command detected...");

                var fileName = input.Replace("export:", String.Empty).Trim().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);


                var entities = analyzer.Servers;
                if (fileName.Length > 1)
                {
                    entities = analyzer.Match(fileName[1].Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList());
                }

                var includeDown = true;
                var includeUp = false;

                if (fileName.Length > 2)
                {
                    includeDown = fileName.Skip(2).Contains("-d");
                    includeUp = fileName.Skip(2).Contains("-u");
                }


                Path.GetInvalidFileNameChars().ToList().ForEach(c => fileName[0] = fileName[0].Replace(c, ' '));
                fileName[0] += ".graphml";

                System.Console.WriteLine("Writing to file {0}...", fileName);
                entities.ExportToGraphML(fileName[0], includeDown, includeUp);
                System.Console.WriteLine("Export completed.");
            }
            else if (input.StartsWith("analyze:"))
            {
                System.Console.WriteLine();
                System.Console.WriteLine("Analyze command detected...");
                var items = analyzer.Match(input.Replace("analyze:", String.Empty).Split(new[] { ';' }).ToList());
                foreach (var entity in items)
                {
                    System.Console.WriteLine("Analysis of {0} started...", entity.FullName);
                    analyzer.AnalyzeEntity(entity);
                }
                System.Console.WriteLine("Analysis completed.");
            }
            if (input.StartsWith("cleanup:"))
            {
                System.Console.WriteLine();
                System.Console.WriteLine("Cleanup command detected...");

                var command = input.Replace("cleanup:", String.Empty).ToLowerInvariant().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (command.Contains("names"))
                {
                    var filter = new List<String>();
                    if (command.Any(n => n.StartsWith("-f:")))
                    {
                        var file = new FileInfo(command.Single(n => n.StartsWith("-f:")).Replace("-f:", String.Empty));
                        if (file.Exists)
                        {
                            filter = File.ReadAllLines(file.FullName).ToList();
                            System.Console.WriteLine("Filters file '{0}' accepted", file.FullName);
                        }
                        else
                        {
                            System.Console.WriteLine("Filters file '{0}' provided does not exist", file.FullName);
                        }
                    }
                    System.Console.WriteLine("Cleanup by name detected...");
                    analyzer.GenerateCleanupByNames(filter);
                }
                if (command.Contains("unused"))
                {
                    var filter = new List<String>();
                    System.Console.WriteLine("Cleanup unused detected...");
                    if (command.Any(n => n.StartsWith("-db:")))
                    {
                        var dbName = command.Single(n => n.StartsWith("-db:")).Replace("-db:", String.Empty);
                        var databases = analyzer.GetEntities(dbName, EntityType.Database);

                        if (databases.Count > 0)
                        {
                            System.Console.WriteLine("Database names accepted: {0}", String.Join(", ", databases.Select(d => "'" + d.FullName + "'").ToArray()));
                            System.Console.ReadLine();
                            foreach (var database in databases)
                            {
                                analyzer.GenerateCleanupForUnused(database);
                                System.Console.WriteLine("Database {0} cleanup script printed", database.FullName);
                            }
                        }
                        else
                        {
                            System.Console.WriteLine("Database name '{0}' provided does not exist", dbName);
                        }
                    }
                }
                if (command.Contains("pattern"))
                {
                    var filter = new List<String>();
                    if (command.Any(n => n.StartsWith("-f:")))
                    {
                        var file = new FileInfo(command.Single(n => n.StartsWith("-f:")).Replace("-f:", String.Empty));
                        if (file.Exists)
                        {
                            filter = File.ReadAllLines(file.FullName).ToList();
                            System.Console.WriteLine("Patterns file '{0}' accepted", file.FullName);
                        }
                        else
                        {
                            System.Console.WriteLine("Patterns file '{0}' provided does not exist", file.FullName);
                        }
                    }
                    System.Console.WriteLine("Cleanup by pattern detected...");
                    analyzer.GenerateCleanupByPatterns(filter);
                }
                else
                {
                    System.Console.WriteLine("Cleanup command not detected");
                }
            }
            else
            {
                System.Console.WriteLine();
                System.Console.WriteLine("Looking for objects...");
                //Searching for provided objects
                var items = analyzer.Match(result);
                System.Console.WriteLine("Found {0} objects...", items.Count);

                foreach (var item in items)
                {
                    item.ExportToFile();
                }
            }
        }
    }
}
