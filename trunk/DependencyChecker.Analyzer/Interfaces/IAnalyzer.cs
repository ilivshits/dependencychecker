﻿using System;
using System.Collections.Generic;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Analyzer.Interfaces
{
    public interface IAnalyzer : IDisposable, IProgressReporter
    {
        void Analyze();
        void AnalyzeEntity(IEntity entity);
        List<IEntity> Match(List<string> inputNames);
        List<IEntity> Search(string inputNames, bool useRegularExpression = false, bool searchInDefinition = false, bool exactSearch = false);
        List<IEntity> Servers { get; }
        void GenerateCleanupByNames(List<string> filter);
        void GenerateCleanupByPatterns(List<string> filter);
        IEntity GetEntity(Guid uid);
        List<IEntity> GetEntities(string dbName, EntityType type);
        List<string> GenerateCleanupForUnused(IEntity database = null);
        List<string> GenerateCleanup(IEntity database, Guid[] entities);
        List<IEntity> GetUnusedEntities(IEntity scope = null, int cyclesLimit = -1);
        DateTime? LastRunTime { get; }
    }
}
