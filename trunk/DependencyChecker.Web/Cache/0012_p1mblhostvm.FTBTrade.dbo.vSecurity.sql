
CREATE VIEW [dbo].[vSecurity]
AS
SELECT     s.sid, s.ticker, s.description, s.underlying_sid, ccy.ticker AS Currency, e.description AS Exchange, spt.description AS Type, sst.description AS SubType,
                       iss.description AS Issuer, s.priceMultiplier, s.tradableFlag, s.priceableFlag, s.startDate, s.endDate, s.version, s.lastUpdate, 
                      s.application_source
FROM         dbo.Security AS s INNER JOIN
                      dbo.Exchange AS e ON s.exchangeId = e.exchangeId INNER JOIN
                      dbo.Security AS ccy ON s.currencySid = ccy.sid INNER JOIN
                      dbo.Issuer AS iss ON s.issuerId = iss.issuerId INNER JOIN
                      dbo.SecurityPrimaryType AS spt ON spt.securityPrimaryTypeId = s.securityPrimaryTypeId INNER JOIN
                      dbo.SecuritySubType AS sst ON sst.securitySubTypeId = s.securitySubTypeId



