﻿using System;
using DependencyChecker.Entities.Events;

namespace DependencyChecker.Entities.Interfaces
{
    public interface IProgressReporter
    {
        event EventHandler<DependencyCheckerProgressEventArgs> Progress;
    }
}
