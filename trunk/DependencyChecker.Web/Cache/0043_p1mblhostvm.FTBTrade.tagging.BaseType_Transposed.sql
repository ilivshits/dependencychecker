CREATE VIEW [tagging].[BaseType_Transposed] AS
SELECT
[Text] = 1
, [Datetime] = 2
, [Bool] = 3
, [Integer] = 4
, [Decimal] = 5
, [System List] = 6
, [User List] = 7
, [Date] = 8
, [Time] = 9
, [XML] = 10


