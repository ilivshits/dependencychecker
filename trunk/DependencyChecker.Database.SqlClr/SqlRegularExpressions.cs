﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;

namespace DependencyChecker.Database.SqlClr
{
    public partial class SqlRegularExpressions
    {
        /// <summary>
        /// Checks string on match to regular expression
        /// </summary>
        /// <param name="text">string to check</param>
        /// <param name="pattern">regular expression</param>
        /// <returns>true - text consists match one at least,
        ///           false - no matches</returns>
        [SqlFunction]
        public static bool Like(string text, string pattern)
        {
            Match match = Regex.Match(text, pattern, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            return (match.Value != String.Empty);
        }
    }
}
