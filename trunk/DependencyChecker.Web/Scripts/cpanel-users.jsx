﻿UserManagement = React.createClass({
    render: function() {
        return (
            <div id="dataContainer">
                <UserManagement.Users source="GetAllUsers"/>
                <UserManagement.NewUserButton/>
            </div>
            );
    }
});

UserManagement.NewUserButton = React.createClass({
    render: function() {
        return (
            <div className="text-right">
                <button id="addNewUser" className="btn btn-default" data-toggle="modal" data-target="#addNewUserModal" data-backdrop="static">
                    <img src="../Content/images/plus-green.png" width="20"/>{"Add new"}
                </button>
            </div>
            );
    }
});

UserManagement.Users = React.createClass({
    getInitialState: function () {
        return {
            data: [
            ]
        };
    },

    componentDidMount: function () {
        this.serverRequest = $.get(this.props.source, function (users) {
            var usersData = [];
            $.each(users, function(index, value) {
                usersData.push({
                    Username: value.Username,
                    Fn: value.FirstName,
                    Ln: value.LastName,
                    Email: value.Email,
                    Role: value.Role
                });
            });
            this.setState({
                data: usersData
            });
        }.bind(this));
    },

    render: function () {
        var userItems = this.state.data.map(function (user) {
            return (
                <UserManagement.Users.UserList username={user.Username} fullname={user.Fn+" "+user.Ln} role={user.Role} email={user.Email}/>
            );
        });

        return (
            <div className="users">
                <div className="table-header table-row">
                    <div className="table-cell">Username</div>
                    <div className="table-cell">Full name</div>
                    <div className="table-cell">Email</div>
                    <div className="table-cell">Role</div>
                </div>
                {userItems}
            </div>
        );
    }
});

UserManagement.Users.UserList = React.createClass({
    render: function () {
        var mailto = 'mailto:' + this.props.email;
        return (
            <div className="table-row">
                <div className="table-cell username">{this.props.username}</div>
                <div className="table-cell">{this.props.fullname}</div>
                <div className="table-cell"><a href={mailto}>{this.props.email}</a></div>
                <div className="table-cell">{this.props.role}</div>
                <div className="table-cell removeUser">
                    <UserManagement.Users.UserList.RemoveUserLink/>
                </div>
            </div>
        );
    }
});

UserManagement.Users.UserList.RemoveUserLink = React.createClass({
    render: function() {
        return (
                <img src="../Content/images/cross-red.png" alt="" ref={(ref) => this.removeLink = ref} onClick={this.handleClick}/>
            );
    },
    handleClick: function() {
        var areYouSure = confirm('Are you sure want to remove user? It can be you.');

        if (areYouSure) {
            $('#loadingModal').modal('show');
            var uname = $(this.removeLink).closest('.table-row').find('.username').text();
            $.ajax({
                type: 'POST',
                url: './RemoveUser',
                data: { 'username': uname },
                success: function () {
                    $('#loadingModal').modal('hide');
                    location.reload();
                },
                error: function (msg) {
                    alert('Can\'t remove user: \r\n' + msg.responseText);
                    $('#loadingModal').modal('hide');
                }
            });
        }
    }

});



ReactDOM.render(
  <UserManagement />,
  $('#userManagement')[0]
);