﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DependencyChecker.Analyzer.Interfaces;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Analyzer
{
    public class ReportsResolver : IResolver
    {
        #region Implementation of IResolver

        public string ProviderName
        {
            get
            {
                return ReportEntity.ProviderName;
            }
        }

        public bool TryResolve(EntityMatch textMatch, List<IEntity> matches, IEntity entity, SearchContent content, out bool added)
        {
            var local = matches.Where(m => m.Scope.Id == entity.Scope.Id).ToList();
            if (local.Count == 1)
            {
                added = entity.AddDependency(local.Single(), textMatch);
            }
            else
            {
                entity.AddErrorFormat(EntityErrorCode.DependencyWarning, "Cannot resolve '{0}' in '{1}'. Possible matches: {2}",
                    textMatch, entity.FullName, String.Join("; ", matches.Select(m => m.FullName)));
                added = false;
            }
            return local.Count == 1;
        }
        #endregion
    }
}