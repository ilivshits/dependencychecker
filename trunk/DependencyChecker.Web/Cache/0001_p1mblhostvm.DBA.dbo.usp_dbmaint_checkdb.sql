CREATE PROCEDURE [dbo].[usp_dbmaint_checkdb]
        @cur_dbname                sysname
       ,@report_only               char(1)

AS
--PRINT 'Entering usp_dbmaint_checkdb for ' + QUOTENAME(@cur_dbname)
DECLARE @dynsql                    varchar(8000)
       ,@spid                      int
       ,@dbmaint_log_rowid         int
       ,@maxrows                   int
       ,@tbl_row_count             bigint
       ,@nclix_count               smallint
       ,@i                         int
       ,@dbname                    sysname
       ,@objowner                  sysname
       ,@objname                   sysname
       ,@is_clustered              char(1)

IF EXISTS(
     SELECT 1 FROM #dbmaint_exceptions
       WHERE (dbname = QUOTENAME(@cur_dbname) AND objowner IS NULL AND objname IS NULL AND
              x_checkdb = 'Y'))
  BEGIN 
    PRINT '[' + CONVERT(varchar(22), GETDATE(), 100) + 
          ']     ***** Skipped CHECKDB process for database: ' + QUOTENAME(@cur_dbname) + ' *****'
    PRINT '.'
    PRINT REPLICATE('==>', 50)
    RETURN
  END


PRINT '[' + CONVERT(varchar(22), GETDATE(), 100) + 
      ']     ***** Start - CHECKDB of database: ' + QUOTENAME(@cur_dbname) + ' *****'
PRINT '.'

SET @dynsql = 'DBCC CHECKDB ('+ QUOTENAME(@cur_dbname) + ', NOINDEX)'
PRINT  QUOTENAME(CONVERT(varchar(22), GETDATE(),100)) + '     ~ Executing ===> ' + @dynsql 
PRINT '.'

IF @report_only = 'N'  
  BEGIN
    INSERT INTO DBA.dbo.dbmaint_log
          (maint_desc,         [db_name],    obj_owner,    obj_name, 
           maint_start_time,   [status],     executed_by,  spid, 
           is_clustered,       nclix_count,  row_count)
      VALUES ('CHECKDB', @cur_dbname,  '', '', GETDATE(), 'started', SUSER_SNAME(), @@spid,
              '', NULL, NULL)
               
    SET @dbmaint_log_rowid = SCOPE_IDENTITY()
    EXEC (@dynsql)
  
    UPDATE DBA.dbo.dbmaint_log
      SET maint_end_time = GETDATE(),
          [status] = 'successful'
      WHERE rowid = @dbmaint_log_rowid
  END
    
PRINT '.'
PRINT '[' + CONVERT(varchar(22), GETDATE(), 100) + 
      ']     ***** End  - CHECKDB of database: ' + QUOTENAME(@cur_dbname) + ' *****'
PRINT '.' 
PRINT REPLICATE('==>', 50)

RETURN

