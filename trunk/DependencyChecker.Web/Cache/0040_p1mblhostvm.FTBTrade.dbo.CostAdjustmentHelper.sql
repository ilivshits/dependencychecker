
CREATE view [dbo].[CostAdjustmentHelper] as
select distinct reinitiateOrderId = r.orderId, reinitiatePrice = r.averagePrice, liquidatePrice = o.averagePrice
from [Order] r
join OpsTransferLink rLink on r.orderId = rLink.toOrderId and rLink.toOtcEventId = 0
join OpsTransferLink l on rLink.opsTransferNodeId = l.opsTransferNodeId
join [Order] o on l.toOrderId = o.orderId and l.toOtcEventId = 0
join OperationalInfo roi on r.orderId = roi.orderId and roi.blockId = 0
join OperationalInfo ooi on o.orderId = ooi.orderId and ooi.blockId = 0
where roi.note like '%Reinitiating order at cost in the destination entity'
      and ooi.note like '%Liquidating order at market in the source entity'
      and r.averagePrice != o.averagePrice

union all

select distinct reinitiateOrderId = r.orderId, reinitiatePrice = r.averagePrice, liquidatePrice = e.price + isnull(fsp.px, 0) * rs.quantityMultiplier
from [Order] r
join [OrderSide] rs on r.orderSideId = rs.orderSideId
join OpsTransferLink rLink on r.orderId = rLink.toOrderId and rLink.toOtcEventId = 0
join OpsTransferLink l on rLink.opsTransferNodeId = l.opsTransferNodeId
join OrderOTCEvent e on l.toOrderId = e.orderId and l.toOtcEventId = e.otcEventId
join OperationalInfo roi on r.orderId = roi.orderId and roi.blockId = 0
join OperationalInfo eoi on e.orderId = eoi.orderId and eoi.blockId = e.blockId
left join v_FspUnwindInstructions fsp on e.orderId = fsp.orderId and e.blockId = fsp.blockId
where roi.note like '%Reinitiating order at cost in the destination entity'
      and eoi.note like '%Liquidating OTC event block at market in the source entity'
      and r.averagePrice != e.price + isnull(fsp.px, 0) * rs.quantityMultiplier







