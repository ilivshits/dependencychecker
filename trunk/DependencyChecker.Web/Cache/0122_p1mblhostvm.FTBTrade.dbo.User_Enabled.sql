CREATE view [dbo].[User_Enabled] as
select
	u.userId, u.fullName
from
	[User] as u
where
	u.enabledFlag != 0


