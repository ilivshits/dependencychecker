﻿using System;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Entities
{
    public class EntityAlias
    {
        public IEntity DefinitionScope { get; set; }
        public String DataProvider { get; set; }
        public String TypeName { get; set; }
        public String DisplayName { get; set; }
    }
}