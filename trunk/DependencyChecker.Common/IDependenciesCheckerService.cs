﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using DependencyChecker.WCF;

namespace DependencyChecker.Common
{
    [ServiceContract]
    public interface IDependenciesCheckerService : IDisposable
    {
        [OperationContract]
        DependencyCheckerStatus GetStatus();

        [OperationContract]
        String PrintEntity(Guid id);

        [OperationContract]
        String ExportEntity(Guid id, bool up, bool down);

        [OperationContract]
        List<Entity> Search(string name, bool useRegularExpression, bool searchInDefinition, bool exactSearch);

        [OperationContract]
        Entity Drillthrough(Guid uid, String[] types);

        [OperationContract]
        Entity GetDetails(Guid id);

        [OperationContract]
        byte[] GetUnusedCleanupScripts(Guid id);

        [OperationContract]
        byte[] GetUnusedCleanupScriptsForSet(Guid databaseId, Guid[] ids);

        [OperationContract]
        List<Entity> GetUnusedEntities(Guid databaseId, int generations);

        [OperationContract]
        String GetState();

        [OperationContract]
        Entity GetEntitiesTree(Guid itemId, TraversalDirection dir);

        [OperationContract]
        String PrintEntityDefinition(Guid id);

        [OperationContract]
        DateTime? GetLastRunTime();

        [OperationContract]
        List<Entity> GetDatabases();

        [OperationContract]
        List<Entity> GetBatchEntities(int batchId);

        [OperationContract]
        List<String> GetEntityErrors(int batchId, string entityName);

        [OperationContract]
        List<Batch> GetBatches(DateTime? from = null, DateTime? to = null);

        [OperationContract]
        List<BatchException> GetBatchExceptions(int batchId);

        [OperationContract]
        void RestartService();
    }
}
