﻿CREATE PROCEDURE [dbo].[spRecreateConstraints]

AS
SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
	DECLARE @create_scr nvarchar(max);
	SELECT TOP 1 @create_scr = t.Command FROM [dbo].[ConstraintsVault] t;
	EXEC sp_executesql @create_scr;
	DELETE FROM [dbo].[ConstraintsVault];
RETURN 0
