﻿-- =============================================
-- Author:  FORTRESS\jcheng3
-- Edited:	 7/29/2013 fortress\aboridko,Alexander Boridko. Sync from Prod from P1ReportingG2 database.
-- Description:	sp that CounterpartyExposureRepo details. (Pulse database is in use)
--				Reports that use this sp: /Imagine/InvestorRelations/CounterpartyExposure 
-- Status:      It is migrated to new P1TC schema. And it works. 
-- =============================================
CREATE PROCEDURE [operations].[GetLegalEntityCashBalances]
  @Funds            AS VARCHAR(500),
  @CounterpartyFirm AS VARCHAR(500),
  @CreatedDate      AS DATETIME,
  @ExclusivelyGL    AS INT = 0
AS
BEGIN
   	SET NOCOUNT ON
	SET FMTONLY OFF		--Required for running stored procedure via open query
		
	declare @databaseName varchar(200)     = 'P1ReportingG2'		

	---Remove comment this section to test the procedure---	
	--declare @Funds				nvarchar(max)	= '>FT_FMMF'
	--declare @CounterpartyFirm	VARCHAR(500)	= 'Deutsche Bank'
	--declare @CreatedDate		date			= '07/26/2013'
	--declare @ExclusivelyGL		INT	= 0
	--set		@databaseName						= 'P1ReportingG2'
	---END TEST PARAMETER SECTION-----
	
	DECLARE @mpsMigrationDate date = [operations].[fnGetPulseConfigMPSMigrationDate]()

    CREATE TABLE #postedCollateral(
        Fund           VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,
        LegalEntity    VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS,
        [Account Name] VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS,
        CreatedDate    DATETIME,
        Balance        BIGINT
    )
  
    DECLARE @maxdate DATETIME
	IF @CreatedDate < @mpsMigrationDate
	BEGIN
		SET @maxdate = (SELECT max(CreatedDate)
						FROM   [Pulse_CollateralDetail]
						WHERE  CreatedDate<=@CreatedDate)
    END
	ELSE
	BEGIN
		SET @maxdate = (SELECT max(CreatedDate) 
							FROM   [Pulse_Collateral]
							WHERE  CreatedDate<=@CreatedDate)
	END   
	      
    DECLARE @sql VARCHAR(max)	
	SET @sql = 'EXEC ['+@databaseName+'].[operations].[GetPostedCollateral]
				  @Funds =  '''''+@Funds+'''''
				, @CounterpartyFirm = '''''+@CounterpartyFirm+'''''
				, @CreatedDate = '''''+CONVERT(VARCHAR, @maxdate, 101)+'''''	' ; 	
	set @sql  = N' 	insert into #postedCollateral	SELECT o.* FROM OPENQUERY([SELF], ''' +  @sql + ''') AS o  ;'
	--print @sql	
    EXEC(@sql)

	SELECT	
				bs.Fund				,
                bs.Broker			,
                bs.[Account Name]	,
                bs.[ANum],
                bs.CreatedDate,
                bs.[Balance]
	into #balanceSheet
    FROM   [YReports_BalanceSheet] bs                   
    WHERE  bs.CreatedDate=Convert(datetime,@CreatedDate)
	AND bs.Fund	 IN (SELECT * FROM dbo.fnFunSplit(@Funds, ','))	
	

	SELECT		
			bs.Fund				[Fund],
            bs.Broker			[Clear_Agent],
            bs.[Account Name]	[Account Name],
            bs.[ANum],
            bs.CreatedDate,
            sum([Balance]) [Balance]
	into #result
    FROM   #balanceSheet bs
            INNER JOIN dbo.Broker_to_LegalEntity	btl	ON bs.Broker COLLATE Latin1_General_BIN2 = btl.ClearAgent
            INNER JOIN dbo.LegalEntity_to_Firm		ltf	ON btl.LegalEntity=ltf.LegalEntity
    WHERE       ltf.Firm=@CounterpartyFirm					
            AND (	( @ExclusivelyGL=0 AND bs.[ANum] IN (SELECT ANum FROM dbo.Counterparty_GL_Rules WHERE  HybridCalc=1))
			  	 OR ( @ExclusivelyGL=1 AND bs.[ANum] IN (SELECT ANum FROM dbo.Counterparty_GL_Rules WHERE  GLCalc=1    ))
				)
    GROUP  BY   
				bs.Fund,
                bs.Broker,
                bs.[Account Name],
                bs.ANum,
                bs.CreatedDate
                        
	insert into #result
        SELECT Fund,
                LegalEntity,
                [Account Name],
                0,
                CreatedDate,
                Balance        
        FROM   #postedCollateral
        WHERE  @ExclusivelyGL=0
	
	------------
	--Printing result...
	------------
	SELECT *
    FROM   #result
    where NOT(
		    ANum=11500
		AND [Clear_Agent] LIKE '%OTC'
		AND @ExclusivelyGL=0)
            
    drop table #postedCollateral    
	drop table #balanceSheet 
	drop table #result
END
