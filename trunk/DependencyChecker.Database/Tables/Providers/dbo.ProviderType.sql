﻿CREATE TABLE [dbo].[ProviderType]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[Name] varchar(max) NOT NULL,
	[Created] DATETIME NULL, 
    [Version] INT NULL, 
    [LastUpdate] DATETIME NULL, 
    [Application_source] VARCHAR(100) NULL, 
    [System_source] VARCHAR(100) NULL
)

GO

CREATE TRIGGER [dbo].[ProviderTypeTrigger]    ON [dbo].[ProviderType]
FOR INSERT, UPDATE, DELETE
NOT for Replication
AS
SET NOCOUNT ON
DECLARE @now DATETIME = GETDATE()

IF EXISTS (SELECT * FROM inserted)
IF EXISTS (SELECT * FROM deleted)
BEGIN
	-- Update
	UPDATE t SET
		Version = ISNULL(u.Version, 0) + 1,
		LastUpdate = @now,
		Created = u.Created,
		Application_source = ISNULL(t.Application_source, SYSTEM_USER),
		System_source = SYSTEM_USER
	OUTPUT 'U', INSERTED.* INTO [log].[ProviderTypeLog]
	FROM [dbo].[ProviderType] t
	INNER JOIN deleted u ON t.Id = u.Id	
END
ELSE
BEGIN	
	-- Create
	UPDATE t SET
		Version = 1,
		LastUpdate = @now,
		Created = @now,
		Application_source = ISNULL(t.Application_source, SYSTEM_USER),
		System_source = SYSTEM_USER
	OUTPUT 'N', INSERTED.* INTO [log].[ProviderTypeLog]
	FROM [dbo].[ProviderType] t
	INNER JOIN inserted u ON t.Id = u.Id	

END
ELSE
BEGIN
	-- Delete
	SELECT * INTO #logtmp FROM deleted
	DECLARE @source VARCHAR(100) = ISNULL(RTRIM(REPLACE(CAST(CONTEXT_INFO() as varchar), CHAR(0), ' ')), SYSTEM_USER)
	UPDATE #logtmp SET LastUpdate = @now, Application_source = @source, System_source = SYSTEM_USER
	OUTPUT 'D', INSERTED.* INTO [log].[ProviderTypeLog]
	DROP TABLE #logtmp	
END
GO
