﻿using System;
using System.IO;
using System.Net.Mime;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using DependencyChecker.Common;
using DependencyChecker.Web.Controllers;
using DependencyChecker.Web.Models;

namespace DependencyChecker.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            ReactConfig.Configure();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError().GetBaseException();

            Server.ClearError();

            var routeData = new RouteData();
            routeData.Values.Add("controller", "Error");

            if (ex.GetType() == typeof (HttpException))
            {
                var httpException = (HttpException) ex;
                var code = httpException.GetHttpCode();

                Response.StatusCode = code;

                if (code == 404)
                {
                    routeData.Values["action"] = "PageNotFound";
                }
                else if (code == 401)
                {
                    routeData.Values["action"] = "Unauthorized";
                }
                else
                {
                    routeData.Values["action"] = "General";
                }
            }
            else
            {
                Response.StatusCode = 500;
                routeData.Values["action"] = "General";
            }

            LoggerHelper.Log.Error(ex.Message, ex);

            IController errorController = new ErrorController();
            Response.ContentType = "text/html";
            errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }
    }
}