CREATE view [dbo].[OrderUnwind] as
select e.orderId, e.blockId, e.otcEventTypeId, e.tradeDate, e.effectiveDate
	, e.settlementDate, e.stepInBrokerId, e.stepInBrokerBranchId
	, e.currencySid, e.fxRate, e.price, e.quantity, e.execUserId
	, o.sid, o.managerId, o.businessUnitId, o.execBrokerId, o.execBrokerBranchId
	, o.orderStatusId, e.created, e.version, e.lastUpdate, e.application_source, e.system_source
from
(
	select orderId
		, blockId
		, otcEventTypeId = min(otcEventTypeId)
		, tradeDate = min(eventDate)
		, effectiveDate = min(effectiveDate)
		, settlementDate = min(settlementDate)
		, stepInBrokerId = min(stepInBrokerId)
		, stepInBrokerBranchId = min(stepInBrokerBranchId)
		, currencySid = min(settlementCurrencySid)
		, fxRate = min(fxRate)
		, price = min(price)
		, quantity = sum(quantity)
		, execUserId = min(execUserId)
		, lastUpdate = min(lastUpdate)
		, version = min(version)
		, created = min(created)
		, application_source = min(application_source)
		, system_source = min(system_source)
	from [OrderOTCEvent]
	group by orderId, blockId
) e
join [Order] o on e.orderId = o.orderId


