
create view [fortress].[v_CorporateActionType_Pivot] as
	select
		 0 as NotSpecified,
		 1 as ACQUIS,
		 2 as CHG_ID,
		 3 as CHG_LIST,
		 4 as CHG_NAME,
		 5 as CHG_TKR,
		 6 as DELIST,
		 7 as Divest,
		 8 as DVD_CASH,
		 9 as DVD_STOCK,
		10 as EQY_OFFER,
		11 as LIST,
		12 as RIGHTS_OFFER,
		13 as SH_HOLDER_MEET,
		14 as SPIN,
		15 as STOCK_BUY,
		16 as STOCK_SPLT	


