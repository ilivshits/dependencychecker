
CREATE PROCEDURE [fortress].[Bond_LookupOrAddFromBloombergSapi]
	@identifierTypeId int,
	@code varchar(150),
	@securityPrimaryTypeId int,
	@securitySubTypeId int,
	@application_source as varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- XACT_ABORT is used to abort the transaction in the face of
	-- anything.
	SET XACT_ABORT, NOCOUNT ON;

	--- The error.
	declare @error varchar(1000) = 
		'The identifierTypeId parameter of ' + coalesce(ltrim(str(@identifierTypeId)), '<null>') + ' is not supported.'

	--- Severity level.
	declare @severity int = 0

	--- Validate the identifier type id.
	declare @prefix varchar(100) = (select p.BloombergSapiIdPrefix from 
		[FTBTrade].[dbo_IdentifierType_MapFromBloombergSapiIdPrefix] as p where p.IdentifierTypeId = @identifierTypeId)
		
	--- If the prefix is null, it's an error.
	if (@prefix is null) begin raiserror(@error, @severity, 0) return end

	--- Note, the following fields are in the corporate and the government files but not populated for convertibles.
	--- These should be fetched at some point to get them when the Bond table is expanded to
	--- handle these.
    --- CV_CNVS_FEXCH_RT,CV_CNVS_PX,CV_CNVS_RATIO,CV_COMMON_TICKER,CV_COMMON_TICKER_EXCH,CV_MANDATORY_CNVS,CV_PROV_PX,CV_SH_PAR,CV_START_DT,CV_UNTIL
	declare @fields varchar(4000) =
		'SECURITY_TYP2,TICKER,MARKET_SECTOR_DES,ID_BB_COMPANY,ID_BB_GUARANTOR,MIN_PIECE,MIN_INCREMENT,ANNOUNCE_DT,ISSUE_DT,AMT_ISSUED,ISSUE_PX,INT_ACC_DT,CPN,MATURITY,FINAL_MATURITY,PAR_AMT,AMT_OUTSTANDING,BASE_CPI,PUTABLE,CALLABLE,CALLED_DT,CALLED_PX,CPN_TYP,CPN_CRNCY,CPN_FREQ,DAY_CNT,BEARER,CALL_DISCRETE,DEFAULTED,DTC_ELIGIBLE,DTC_REGISTERED,DUAL_CRNCY,EXCHANGEABLE,EXTENDIBLE,FLOATER,INSURANCE_STATUS,IS_PERPETUAL,IS_REVERSE_CONVERTIBLE,IS_SOFT_CALL,IS_UNIT_TRADED,MAKE_WHOLE_CALL,OID_BOND,PUT_DISCRETE,REGISTERED,SINKABLE,STRUCTURED_NOTE,SERIES,IS_REG_S,IS_DAY_PAYER,TRADE_STATUS,TRADE_CRNCY,FLT_CPN_CONVENTION,FLT_BENCH_MULTIPLIER,FLT_DAYS_PRIOR,FLT_PAY_DAY,LAST_REFIX_DT,NXT_CALL_DT,NXT_CALL_PX,NXT_CPN_DT,NXT_FACTOR_DT,NXT_PAR_CALL_DT,NXT_PAR_PUT_DT,NXT_PUT_DT,NXT_PUT_PX,NXT_REFIX_DT,NXT_REFUND_DT,COLLAT_TYP,FIRST_CPN_DT,PENULTIMATE_CPN_DT,CALL_FEATURE,PUT_FEATURE,FIRST_CALL_DT_ISSUANCE,CALLED,DAYS_TO_SETTLE,CALC_MATURITY,REFERENCE_INDEX,INFLATION_LINKED_INDICATOR,PRVT_PLACE,PCT_PAR_QUOTED,PREV_CPN_DT,STEPUP_DT,STEPUP_CPN,PCS_QUOTE_TYP,BASIC_SPREAD,RESET_IDX,ID_AUSTRIAN,ID_BB_GLOBAL,ID_BB_UNIQUE,ID_BELGIUM,ID_CEDEL,ID_COMMON,ID_CUSIP,ID_CUSIP_REAL,ID_DANISH,ID_DUTCH,ID_EUROCLEAR,ID_FRENCH,ID_ISIN,ID_ITALY,ID_JAPAN,ID_LUXEMBOURG,ID_NORWAY,ID_SEDOL1,ID_SEDOL2,ID_SEDOL3,ID_SEDOL4,ID_SEDOL5,ID_SPAIN,ID_SWEDISH,ID_VALOREN,ID_WERTPAPIER,ID_XTRAKTER,144A_FLAG,MARKET_SECTOR,CV_CNVS_FEXCH_RT,CV_CNVS_PX,CV_CNVS_RATIO,CV_COMMON_TICKER,CV_COMMON_TICKER_EXCH,CV_MANDATORY_CNVS,CV_PROV_PX,CV_SH_PAR,CV_START_DT,CV_UNTIL,CONVERTIBLE'
	
	--- Set up the table that will receive the data.
	select 1 a into #s;

	--- The tickers to be looked up.  There's only one here.
	select 1 as Ordinal, '/' + @prefix + '/' + @code as [bbg_ticker] into #tickers

	--- Make the call to get the data.
	exec SqlNativeDll.dbo_BDPTable 'select [bbg_ticker] from #tickers', '#s', @fields

	--- Clean up the tickers table.
	drop table #tickers

	--- Currently for debugging purposes.
	select * from #s

	--- The count.
	declare @count int = isnull((select count(*) from #s), 0)

	--- Set error.
	set @error = 'The search on identifier ' + @code + ' produced multiple results.'

	--- If there is more than one value, raise an error.
	if (@count > 1) begin raiserror(@error, @severity, 0) return end

	--- Set error.
	set @error = 'The search on identifier ' + @code + ' produced no results.'

	--- If there are no values, get out.
	if (@count = 0) begin raiserror(@error, @severity, 0) return end
	
	--- Populate.
	select
		si.sid
	into
		#sids
	from
		FTBTrade.dbo_SecurityIdentifier as si with (nolock)
			cross join fortress.v_IdentifierType_Pivot as it
			inner join #s as s on
				s.ID_BB_GLOBAL = si.code collate database_default
	where
		si.identifierTypeId = it.BloombergGlobal

	--- Set the rowcount.
	set @count = @@rowcount
	
	--- If there is more than one sid, get an error.
	--- Set error.
	set @error = 'Multiple securities found for Bloomberg Global id ' +
		(select s.ID_BB_GLOBAL from #s as s) + '.'
		
	--- Check.
	if (@count > 1) begin raiserror(@error, @severity, 0) return end

	--- The sid.
	declare @sid bigint = (select sid from #sids)
	
	--- Cleanup.
	drop table #sids

	--- If the sid is non null, return it.
	if (@sid is not null) begin select @sid as sid return end
	
	--- Convertible.
	declare @convertibleFlag bit = null

	--- Market sector description.
	declare @marketSectorDes varchar(8) = null

	select
		--- Market sector description.
		@marketSectorDes = s.MARKET_SECTOR_DES,
		@convertibleFlag = 
			case
				when s.CONVERTIBLE is null then 0
				when s.CONVERTIBLE = 'Y' then 1
				else 0
			end
	from
		#s as s

	--- Get the security primary type.
	set @securityPrimaryTypeId = (
		select
			case 
				when @convertibleFlag != 0 then spt.ConvertibleBond
				when @marketSectorDes in ('Corp', 'Pfd') then spt.CorporateBond
				when @marketSectorDes = 'Govt' then spt.GovernmentBond
				else spt.NotSpecified
			end				
		from
			fortress.v_SecurityPrimaryType_Pivot as spt
	)

	--- The default lot size and increment.
	declare @defaultInitialLotSize decimal
	declare @defaultIncrementLotSize decimal

	--- Assign defaults.
	select
		@defaultInitialLotSize = spt.defaultLotSizeInitial,
		@defaultIncrementLotSize = spt.defaultLotSizeIncrement
	from
		FTBTrade.dbo_SecurityPrimaryType as spt with (nolock)
	where
		spt.securityPrimaryTypeId = @securityPrimaryTypeId
	
	--- Declare the variables.
	declare @ticker	varchar(50) = null
	declare @description varchar(150) = null
	declare @currencySid bigint = null
	declare @exchangeId int = null
	declare @startDate datetime = null
	declare @endDate datetime = null
	declare @priceMultiplier float = null
	declare @tradableFlag bit = null
	declare @priceableFlag bit = null
	declare @primaryFlag bit = null
	declare @underlying_sid	bigint = null
	declare @issuerId int = null
	declare @lotSizeInitial float = null
	declare @lotSizeIncrement float = null

	--- Identifiers.
	declare @idBloombergCodeShort varchar(100) = null
	declare @idBloombergCodeLong varchar(100) = null
	declare @idCusip varchar(100) = null
	declare @idSedol varchar(100) = null
	declare @idIsin varchar(100) = null
	declare @idRed6 varchar(100) = null
	declare @idRed9 varchar(100) = null
	declare @idTradeId varchar(100) = null
	declare @idBloombergCdsTicker5Y varchar(100) = null
	declare @idBloombergGlobal varchar(100) = null
	declare @idBloombergUnique varchar(100) = null
	declare @idBelgium varchar(100) = null
	declare @idCedelEuroclear varchar(100) = null
	declare @idDutch varchar(100) = null
	declare @idFrench varchar(100) = null
	declare @idValoren varchar(100) = null
	declare @idWertpapier varchar(100) = null
	declare @idOcc varchar(100) = null
	declare @idOpra varchar(100) = null
	declare @idBloombergTickerAndExchange varchar(100) = null
	declare @idAustrian varchar(100) = null
	declare @idCedel varchar(100) = null
	declare @idDanish varchar(100) = null
	declare @idEuroclear varchar(100) = null
	declare @idItaly varchar(100) = null
	declare @idJapan varchar(100) = null
	declare @idLuxembourg varchar(100) = null
	declare @idNorway varchar(100) = null
	declare @idSpain varchar(100) = null
	declare @idSwedish varchar(100) = null
	declare @idXtrakter varchar(100) = null

	--- Restrictions.
	declare @restriction144A bit = null
	declare @restrictionRegS bit = null

	--- Bond-specific
	declare @announceDate date = null
	declare @series varchar(10) = null
	declare @issueDate date = null
	declare @amountIssued float = null
	declare @issuePrice float = null
	declare @accrualStartDate date = null
	declare @firstCouponDate date = null
	declare @penultimateCouponDate date = null
	declare @finalMaturityDate date = null
	declare @maturityDate date = null
	declare @dayCountConventionId int = null
	declare @businessDayConventionId int = null
	declare @businessDayConventionInterestAdjustmentId int = null
	declare @frequencyId int = null
	declare @daysBetweenPayments int = null
	declare @faceValue float = null
	declare @couponTypeId int = null
	declare @coupon float = null
	declare @couponCurrencySid bigint = null
	declare @floatingRateBenchmarkMultiplier float = null
	declare @daysBeforeCouponRefix int = null
	declare @unadjustedBusinessDayConventionPayDateId int = null
	declare @lastCouponResetDate date = null
	declare @nextCouponDate date = null
	declare @nextCouponResetDate date = null
	declare @nextFactorDate date = null
	declare @nextRefundDate date = null
	declare @guarantorId int = null
	declare @collateralTypeId int = null
	declare @amountOutstanding float = null
	declare @baseCpi float = null
	declare @putableFlag bit = null
	declare @discretelyPutableFlag bit = null
	declare @putFeatureId int = null
	declare @nextPutDate date = null
	declare @nextPutPrice float = null
	declare @nextParPutDate date = null
	declare @callableFlag bit = null
	declare @discretelyCallableFlag bit = null
	declare @callFeatureId int = null
	declare @calledFlag bit = null
	declare @firstCallDate date = null
	declare @calledDate date = null
	declare @calledPrice float = null
	declare @nextCallDate date = null
	declare @nextCallPrice float = null
	declare @nextParCallDate date = null
	declare @bearerFlag bit = null
	declare @defaultedFlag bit = null
	declare @dtcEligibleFlag bit = null
	declare @dtcRegisteredFlag bit = null
	declare @dualCurrencyFlag bit = null
	declare @exchangeableFlag bit = null
	declare @extendibleFlag bit = null
	declare @floaterFlag bit = null
	declare @insuranceBackedFlag bit = null
	declare @perpetualFlag bit = null
	declare @reverseConvertibleFlag bit = null
	declare @softCallFlag bit = null
	declare @unitTradedFlag bit = null
	declare @makeWholeCallFlag bit = null
	declare @originalIssueDiscountFlag bit = null
	declare @registeredWithTrusteeFlag bit = null
	declare @sinkableFlag bit = null
	declare @structuredNoteFlag bit = null
	declare @daysToSettle int = null
	declare @maturityForCalculationsDate date = null
	declare @referenceIndexSid bigint = null
	declare @inflationLinkedFlag bit = null
	declare @privatePlacementFlag bit = null 
	declare @quotedAsPercentageOfParFlag bit = null 
	declare @previousCouponDate date = null 
	declare @stepupDate date = null 
	declare @stepupCoupon float = null
	declare @quoteTypeId int = null
	declare @spread float = null
	declare @resetIndexSid bigint = null
	declare @calculatedFloaterFlag bit = null
	
	--- The currency and coupon currency tickers.
	declare @currencyTicker varchar(50)
	declare @couponCurrencyTicker varchar(50)
			
	--- Ticker suffix.
	declare @tickerSuffix varchar(10) = null
		
	--- Select the values from the Bloomberg data.
	select
		@ticker	= s.TICKER,
		@description = 
			s.TICKER + 
			coalesce(' ' + s.SERIES, '') +
			case
				when s.FLOATER = 'Y' then ' FLOAT'
				else coalesce(' ' + cast(s.CPN as varchar(25)) + '%', '')
			end +
			coalesce(' ' + convert(varchar(10), cast(s.MATURITY as date), 101), ''),
		@currencyTicker = s.TRADE_CRNCY,
		@couponCurrencyTicker = s.CPN_CRNCY,
		@exchangeId = 999, --- OTC
		@startDate = coalesce(s.ISSUE_DT, '1900-01-01'),
		@endDate = coalesce(s.FINAL_MATURITY, '9999-12-31'),
		@tradableFlag = case when s.TRADE_STATUS = 'Y' then 1 else 0 end,
		@priceableFlag = 1,
		@issuerId = isnull((
			select
				ii.issuerId
			from
				FTBTrade.dbo_IssuerIdentifier as ii with (nolock)
					cross join fortress.v_IdentifierType_Pivot as it
			where
				ii.identifierTypeId = it.BloombergCompany and
				ii.code = s.ID_BB_COMPANY collate database_default
		), 0),
		@securitySubTypeId = 
			case 
				when s.SECURITY_TYP2 is null then sst.None
				when upper(s.SECURITY_TYP2) = 'Preferred Stock' then sst.Preferred
				else sst.None
			end,
		@lotSizeInitial = 
			case 
				when 
					(@defaultInitialLotSize is not null and s.MIN_PIECE is not null and @defaultInitialLotSize != s.MIN_PIECE) or
					(@defaultIncrementLotSize is not null and s.MIN_INCREMENT is not null and @defaultIncrementLotSize != s.MIN_INCREMENT)
					then s.MIN_PIECE
				else null
			end,
		@lotSizeIncrement =
			case 
				when 
					(@defaultInitialLotSize is not null and s.MIN_PIECE is not null and @defaultInitialLotSize != s.MIN_PIECE) or
					(@defaultIncrementLotSize is not null and s.MIN_INCREMENT is not null and @defaultIncrementLotSize != s.MIN_INCREMENT)
					then s.MIN_INCREMENT
				else null
			end,
			
		--- Identifiers.
		@idBloombergCodeShort = s.ID_CUSIP,
		@idBloombergCodeLong = s.ID_CUSIP + ' ' + s.MARKET_SECTOR_DES,
		@idCusip = s.ID_CUSIP_REAL,
		@idSedol = coalesce(s.ID_SEDOL1, s.ID_SEDOL2, s.ID_SEDOL3, s.ID_SEDOL4, s.ID_SEDOL5),
		@idIsin = s.ID_ISIN,
		@idBloombergGlobal = s.ID_BB_GLOBAL,
		@idBloombergUnique = s.ID_BB_UNIQUE,
		@idBelgium = s.ID_BELGIUM,
		@idCedelEuroclear = s.ID_COMMON,
		@idDutch = s.ID_DUTCH,
		@idFrench = s.ID_FRENCH,
		@idValoren = s.ID_VALOREN,
		@idWertpapier = s.ID_WERTPAPIER,
		@idAustrian = s.ID_AUSTRIAN,
		@idCedel = s.ID_CEDEL,
		@idDanish = s.ID_DANISH,
		@idEuroclear = s.ID_EUROCLEAR,
		@idItaly = s.ID_ITALY,
		@idJapan = s.ID_JAPAN,
		@idLuxembourg = s.ID_LUXEMBOURG,
		@idNorway = s.ID_NORWAY,
		@idSpain = s.ID_SPAIN,
		@idSwedish = s.ID_SWEDISH,
		@idXtrakter = s.ID_XTRAKTER,

		--- Restrictions.
		@restriction144A = case when s.[144A_FLAG] = 'Y' then 1 else 0 end,
		@restrictionRegS = case when s.[IS_REG_S] = 'Y' then 1 else 0 end,

		--- Bond-specific
		@announceDate = s.ANNOUNCE_DT,
		@series = s.SERIES,
		@issueDate = s.ISSUE_DT,
		@amountIssued = s.AMT_ISSUED,
		@issuePrice = s.ISSUE_PX,
		@accrualStartDate = s.INT_ACC_DT,
		@firstCouponDate = s.FIRST_CPN_DT,
		@penultimateCouponDate = s.PENULTIMATE_CPN_DT,
		@maturityDate = s.MATURITY,
		@finalMaturityDate = s.FINAL_MATURITY,
		@dayCountConventionId = 
			case
				when s.DAY_CNT is null then 0
				else (
					select
						map.dayCountConventionId
					from
						fortress.v_Bloomberg_Map_DAY_CNT as map
					where
						map.DAY_CNT = s.DAY_CNT collate database_default
				)
			end,
		@businessDayConventionId = 
			case
				when s.FLT_CPN_CONVENTION is null then 0
				else (
					select
						map.businessDayConventionId
					from
						fortress.v_Bloomberg_Map_FLT_CPN_CONVENTION as map
					where
						map.FLT_CPN_CONVENTION = s.FLT_CPN_CONVENTION collate database_default
				)
			end,
		@businessDayConventionInterestAdjustmentId = 
			case
				when s.FLT_CPN_CONVENTION is null then 0
				else (
					select
						map.businessDayConventionInterestAdjustmentId
					from
						fortress.v_Bloomberg_Map_FLT_CPN_CONVENTION as map
					where
						map.FLT_CPN_CONVENTION = s.FLT_CPN_CONVENTION collate database_default
				)
			end,
		@frequencyId = 
			case
				when isnull(s.IS_DAY_PAYER, '') != 'Y' then 
					case
						when s.CPN_FREQ is null then 0
						else (
							select
								map.frequencyId
							from
								[fortress].[v_Bloomberg_Map_CPN_FREQ] as map
							where
								map.CPN_FREQ = s.CPN_FREQ
						)
					end
				else 0
			end,
		@daysBetweenPayments = 
			case
				when isnull(s.IS_DAY_PAYER, '') = 'Y' then s.CPN_FREQ
				else null
			end,
		@faceValue = s.PAR_AMT,
		@couponTypeId = 
			case
				when s.FLT_PAY_DAY is null then 0
				else (
					select
						map.couponTypeId
					from
						fortress.v_Bloomberg_Map_CPN_TYP as map
					where
						map.CPN_TYP = s.CPN_TYP collate database_default
				)
			end,				
		@coupon = s.CPN,
		@floatingRateBenchmarkMultiplier = s.FLT_BENCH_MULTIPLIER,
		@daysBeforeCouponRefix = s.FLT_DAYS_PRIOR,
		@unadjustedBusinessDayConventionPayDateId = 
			case
				when s.FLT_PAY_DAY is null then 0
				else (
					select
						map.unadjustedBusinessDayConventionPayDateId
					from
						fortress.v_Bloomberg_Map_FLT_PAY_DAY as map
					where
						map.FLT_PAY_DAY = s.FLT_PAY_DAY collate database_default
				)
			end,
		@lastCouponResetDate = s.LAST_REFIX_DT,
		@nextCouponDate = s.NXT_CPN_DT,
		@nextCouponResetDate = s.NXT_REFIX_DT,
		@nextFactorDate = s.NXT_FACTOR_DT,
		@nextRefundDate = s.NXT_REFUND_DT,
		@guarantorId = isnull(s.ID_BB_GUARANTOR, 0),
		@collateralTypeId = 
			case
				when s.COLLAT_TYP is null then 0
				else (
					select
						map.collateralTypeId
					from
						fortress.v_Bloomberg_Map_COLLAT_TYP as map
					where
						map.COLLAT_TYP = s.COLLAT_TYP collate database_default
				)
			end,
		@amountOutstanding = s.AMT_OUTSTANDING,
		@baseCpi = s.BASE_CPI,
		@putableFlag = case when s.PUTABLE = 'Y' then 1 else 0 end,
		@discretelyPutableFlag = case when s.PUT_DISCRETE = 'Y' then 1 else 0 end,
		@putFeatureId = 
			case
				when s.PUT_FEATURE is null then 0
				else (
					select
						map.putCallFeatureId
					from
						fortress.v_Bloomberg_Map_PUT_CALL_FEATURE as map
					where
						map.PUT_CALL_FEATURE = s.PUT_FEATURE collate database_default
				)
			end,
		@nextPutDate = s.NXT_PUT_DT,
		@nextPutPrice = s.NXT_PUT_PX,
		@nextParPutDate = s.NXT_PAR_PUT_DT,
		@callableFlag = case when s.CALLABLE = 'Y' then 1 else 0 end,
		@discretelyCallableFlag = case when s.CALL_DISCRETE = 'Y' then 1 else 0 end,
		@callFeatureId = 
			case
				when s.CALL_FEATURE is null then 0
				else (
					select
						map.putCallFeatureId
					from
						fortress.v_Bloomberg_Map_PUT_CALL_FEATURE as map
					where
						map.PUT_CALL_FEATURE = s.CALL_FEATURE collate database_default
				)
			end,
		@calledFlag = case when s.CALLED = 'Y' then 1 else 0 end,
		@firstCallDate = s.FIRST_CALL_DT_ISSUANCE,
		@calledDate = s.CALLED_DT,
		@calledPrice = s.CALLED_PX,
		@nextCallDate = s.NXT_CALL_DT,
		@nextCallPrice = s.NXT_CALL_PX,
		@nextParCallDate = s.NXT_PAR_CALL_DT,
		@bearerFlag = case when s.BEARER = 'Y' then 1 else 0 end,
		@defaultedFlag = case when s.DEFAULTED = 'Y' then 1 else 0 end,
		@dtcEligibleFlag = case when s.DTC_ELIGIBLE = 'Y' then 1 else 0 end,
		@dtcRegisteredFlag = case when s.DTC_REGISTERED = 'Y' then 1 else 0 end,
		@dualCurrencyFlag = case when s.DUAL_CRNCY = 'Y' then 1 else 0 end,
		@exchangeableFlag = case when s.EXCHANGEABLE = 'Y' then 1 else 0 end,
		@extendibleFlag = case when s.EXTENDIBLE = 'Y' then 1 else 0 end,
		@floaterFlag = case when s.FLOATER = 'Y' then 1 else 0 end,
		@insuranceBackedFlag = case when s.INSURANCE_STATUS = 'Y' then 1 else 0 end,
		@perpetualFlag = case when s.IS_PERPETUAL = 'Y' then 1 else 0 end,
		@reverseConvertibleFlag = case when s.IS_REVERSE_CONVERTIBLE = 'Y' then 1 else 0 end,
		@softCallFlag = case when s.IS_SOFT_CALL = 'Y' then 1 else 0 end,
		@unitTradedFlag = case when s.IS_UNIT_TRADED = 'Y' then 1 else 0 end,
		@makeWholeCallFlag = case when s.MAKE_WHOLE_CALL = 'Y' then 1 else 0 end,
		@originalIssueDiscountFlag = case when s.OID_BOND = 'Y' then 1 else 0 end,
		@registeredWithTrusteeFlag = case when s.REGISTERED = 'Y' then 1 else 0 end,
		@sinkableFlag = case when s.SINKABLE = 'Y' then 1 else 0 end,
		@structuredNoteFlag = case when s.STRUCTURED_NOTE = 'Y' then 1 else 0 end,
		@daysToSettle = s.DAYS_TO_SETTLE,
		@maturityForCalculationsDate = s.CALC_MATURITY,
		@referenceIndexSid = 
			case
				when s.REFERENCE_INDEX is null then 0
				else (
					select top 1
						si.sid
					from
						FTBTrade.dbo_SecurityIdentifier as si with (nolock)
							cross join fortress.v_IdentifierType_Pivot as it
							inner join FTBTrade.dbo_Security as sec with (nolock) on
								sec.sid = si.sid
							cross join fortress.v_SecurityPrimaryType_Pivot as spt
					where
						si.identifierTypeId = it.BloombergCodeLong and
						si.code = s.REFERENCE_INDEX collate database_default and
						sec.securityPrimaryTypeId in (spt.InterestRateIndex, spt.[Index])
					order by
						case sec.securityPrimaryTypeId when spt.InterestRateIndex then 0 else 1 end
				)
			end,
		@inflationLinkedFlag = case when s.INFLATION_LINKED_INDICATOR = 'Y' then 1 else 0 end,
		@privatePlacementFlag = case when s.PRVT_PLACE = 'Y' then 1 else 0 end,
		@quotedAsPercentageOfParFlag = case when s.PCT_PAR_QUOTED = 'Y' then 1 else 0 end, 
		@previousCouponDate = s.PREV_CPN_DT, 
		@stepupDate = s.STEPUP_DT, 
		@stepupCoupon = s.STEPUP_CPN,
		@quoteTypeId = 
			case
				when s.PCS_QUOTE_TYP is null then 0
				else (
					select
						map.quoteTypeId
					from
						fortress.v_Bloomberg_Map_PCS_QUOTE_TYP as map
					where
						map.PCS_QUOTE_TYP = s.PCS_QUOTE_TYP
				)
			end,
		@spread = s.BASIC_SPREAD,
		@resetIndexSid = 
			case
				when s.RESET_IDX is null or s.RESET_IDX in ('N/A', '1YR N/A', '3MO N/A') then 0
				else isnull((
					select top 1
						si.sid
					from
						FTBTrade.dbo_SecurityIdentifier as si with (nolock)
							cross join fortress.v_IdentifierType_Pivot as it
							inner join FTBTrade.dbo_Security as sec with (nolock) on
								sec.sid = si.sid
							cross join fortress.v_SecurityPrimaryType_Pivot as spt
					where
						si.identifierTypeId = it.BloombergCodeShort and
						si.code = s.RESET_IDX collate database_default and
						sec.securityPrimaryTypeId in (spt.InterestRateIndex, spt.[Index]) and
						not exists (
							select
								si2.*
							from
								FTBTrade.dbo_SecurityIdentifier as si2 with (nolock)
									inner join FTBTrade.dbo_Security as s2 with (nolock) on
										s2.sid = si2.sid
							where
								si2.identifierTypeId = si.identifierTypeId and
								si2.code = si.code and
								s2.securityPrimaryTypeId = sec.securityPrimaryTypeId and
								si2.sid != sec.sid
						)
					order by
						case sec.securityPrimaryTypeId when spt.InterestRateIndex then 0 else 1 end						
				), 0)
			end			
	from
		#s as s
			cross join fortress.v_SecuritySubType_Pivot as sst

	--- Lot size initial or increment should be one if less than one.
	if (@lotSizeInitial is not null and @lotSizeInitial < 1.0) set @lotSizeInitial = 1.0
	if (@lotSizeIncrement is not null and @lotSizeIncrement < 1.0) set @lotSizeIncrement = 1.0

	--- If this is a preferred, then face value needs to be one.
	set @faceValue = 
		case 
			when exists (select * from fortress.v_SecuritySubType_Pivot as sst where sst.Preferred = @securitySubTypeId)
				then 1
			else @faceValue
		end

	--- Check issuer, if it doesn't exist, set to 0.
	set @issuerId = isnull((
		select
			i.issuerId
		from
			FTBTrade.dbo_Issuer as i with (nolock)
		where
			i.issuerId = @issuerId
	), 0)
	
	--- Same thing with guarantor.
	set @guarantorId = isnull((
		select
			i.issuerId
		from
			FTBTrade.dbo_Issuer as i with (nolock)
		where
			i.issuerId = @guarantorId
	), 0)
		
	--- Set separately as it's based on previous statement.
	set @calculatedFloaterFlag = 
		case
			when @floaterFlag != 0 or exists(
				select
					ct.*
				from
					fortress.v_CouponType_Pivot as ct
				where
					ct.Variable = @couponTypeId or
					ct.Floating = @couponTypeId
			) then 1
			else 0
		end

	--- Clean up.
	drop table #s

	--- Translate currency ticker.
	exec fortress.Currency_TranslateFromBloomberg @currencyTicker output, @priceMultiplier output
	exec fortress.Currency_TranslateFromBloomberg @couponCurrencyTicker output, null
	
	--- Defaults.
	set @underlying_sid = 0
	set @primaryFlag = 1
	
	--- Get the currency sid.
	select 
		@currencySid = s.sid 
	from 
		FTBTrade.dbo_Security as s with (nolock)
			cross join fortress.v_SecurityPrimaryType_Pivot as spt
	where 
		s.securityPrimaryTypeId = spt.Currency and
		s.ticker = @currencyTicker
		
	--- The coupon currency sid.
	select
		@couponCurrencySid = s.sid
	from
		FTBTrade.dbo_Security as s with (nolock)
			cross join fortress.v_SecurityPrimaryType_Pivot as spt
	where
		s.securityPrimaryTypeId = spt.Currency and
		s.ticker = @couponCurrencyTicker

	--- The ticker suffix.
	set @tickerSuffix = (
		select
			case 
				when @convertibleFlag != 0 or @marketSectorDes in ('Corp', 'Govt') then ' BOND'
				when @marketSectorDes = 'Pfd' then ' PFD'
				else null
			end				
	)
	
	--- Set the ticker.
	set @ticker = @ticker + @tickerSuffix

	--- The price multiplier.
	set @priceMultiplier = (
		select
			case 
				when @convertibleFlag != 0 or @marketSectorDes in ('Corp', 'Govt') then 0.01
				when @marketSectorDes = 'Pfd' then 1.0
				else 0.0
			end				
	)
	
	--- Add the bond.
	exec [FTBTrade].[dbo_Bond_Add]
		--- Bond-specific
		@announceDate,
		@series,
		@issueDate,
		@amountIssued,
		@issuePrice,
		@accrualStartDate,
		@firstCouponDate,
		@penultimateCouponDate,
		@finalMaturityDate,
		@maturityDate,
		@dayCountConventionId,
		@businessDayConventionId,
		@businessDayConventionInterestAdjustmentId,
		@frequencyId,
		@daysBetweenPayments,
		@faceValue,
		@couponTypeId,
		@coupon,
		@couponCurrencySid,
		@floatingRateBenchmarkMultiplier,
		@daysBeforeCouponRefix,
		@unadjustedBusinessDayConventionPayDateId,
		@lastCouponResetDate,
		@nextCouponDate ,
		@nextCouponResetDate ,
		@nextFactorDate ,
		@nextRefundDate ,
		@guarantorId ,
		@collateralTypeId ,
		@amountOutstanding ,
		@baseCpi ,
		@putableFlag ,
		@discretelyPutableFlag ,
		@putFeatureId ,
		@nextPutDate ,
		@nextPutPrice ,
		@nextParPutDate ,
		@callableFlag ,
		@discretelyCallableFlag ,
		@callFeatureId ,
		@calledFlag ,
		@firstCallDate ,
		@calledDate ,
		@calledPrice ,
		@nextCallDate ,
		@nextCallPrice ,
		@nextParCallDate ,
		@bearerFlag ,
		@defaultedFlag,
		@dtcEligibleFlag,
		@dtcRegisteredFlag,
		@dualCurrencyFlag,
		@exchangeableFlag,
		@extendibleFlag,
		@floaterFlag,
		@insuranceBackedFlag,
		@perpetualFlag,
		@reverseConvertibleFlag,
		@softCallFlag,
		@unitTradedFlag,
		@makeWholeCallFlag,
		@originalIssueDiscountFlag,
		@registeredWithTrusteeFlag,
		@sinkableFlag,
		@structuredNoteFlag,
		@daysToSettle,
		@maturityForCalculationsDate,
		@referenceIndexSid,
		@inflationLinkedFlag,
		@privatePlacementFlag, 
		@quotedAsPercentageOfParFlag, 
		@previousCouponDate, 
		@stepupDate, 
		@stepupCoupon,
		@quoteTypeId,
		@spread,
		@resetIndexSid,
		@calculatedFloaterFlag,

		--- Security.
		@ticker,
		@description,
		@currencySid,
		@exchangeId,
		@securityPrimaryTypeId,
		@securitySubTypeId,	
		@startDate,
		@endDate,
		@priceMultiplier,
		@tradableFlag,
		@priceableFlag,
		@primaryFlag,
		@underlying_sid,
		@issuerId,
		@lotSizeInitial,
		@lotSizeIncrement,
		null,
			
		--- Identifiers.
		@idBloombergCodeShort,
		@idBloombergCodeLong,
		@idCusip,
		@idSedol,
		@idIsin,
		@idRed6,
		@idRed9,
		@idTradeId,
		@idBloombergCdsTicker5Y,
		@idBloombergGlobal,
		@idBloombergUnique,
		@idBelgium,
		@idCedelEuroclear,
		@idDutch,
		@idFrench,
		@idValoren,
		@idWertpapier,
		@idOcc,
		@idOpra,
		@idBloombergTickerAndExchange,
		@idAustrian,
		@idCedel,
		@idDanish,
		@idEuroclear,
		@idItaly,
		@idJapan,
		@idLuxembourg,
		@idNorway,
		@idSpain,
		@idSwedish,
		@idXtrakter,

		--- Restrictions.
		@restriction144A,
		@restrictionRegS,

		--- Auditing.
		@application_source

END




