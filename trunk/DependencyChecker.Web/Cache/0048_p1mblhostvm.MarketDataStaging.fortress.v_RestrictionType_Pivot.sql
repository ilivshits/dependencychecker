
create view [fortress].[v_RestrictionType_Pivot] as
	select
		 1 as FigRtl,
		 2 as MlpPtp,
		 3 as ShortSaleBan,
		 4 as _144A,
		 5 as RegS,
		 6 as _3C7,
		 7 as RegulatoryThreshold,
		 8 as TemporaryShortSelling,
		 9 as ShortSaleBanItaly,
		10 as ShortSaleBanSpain,
		11 as ShortSaleBanGreece,
		12 as ShortSaleBanSouthKorea,
		13 as Gsma


