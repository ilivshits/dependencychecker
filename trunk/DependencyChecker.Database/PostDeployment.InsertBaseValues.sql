﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

DELETE FROM [dbo].[ErrorCode]
GO

INSERT INTO [dbo].[ErrorCode]
VALUES 
(1, 'InvalidDefinition'),
(2, 'MissingObjectInDefinition'),
(3, 'ValidationError'),
(4, 'AnalysisError'),
(5, 'DependencyError'),
(6, 'DependencyWarning'),
(7, 'AccessError'),
(8, 'Info'),
(9, 'ParsingError'),
(10, 'Unexpected')

GO



ALTER TABLE [dbo].[ProviderItem] NOCHECK CONSTRAINT ALL
ALTER TABLE [dbo].[ProviderType] NOCHECK CONSTRAINT ALL



DELETE FROM [dbo].[ProviderType]
GO

INSERT INTO [dbo].[ProviderType]
VALUES 
(1, 'SQL', NULL, NULL, NULL, NULL, NULL),
(2, 'SSRS', NULL, NULL, NULL, NULL, NULL),
(3, 'OLAP', NULL, NULL, NULL, NULL, NULL),
(4, 'ScheduledTask', NULL, NULL, NULL, NULL, NULL),
(5, 'P1TC', NULL, NULL, NULL, NULL, NULL)


GO

ALTER TABLE [dbo].[ProviderItem] WITH CHECK CHECK CONSTRAINT ALL
ALTER TABLE [dbo].[ProviderType] WITH CHECK CHECK CONSTRAINT ALL