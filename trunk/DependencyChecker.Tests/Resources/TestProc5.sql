﻿-- =============================================
-- Author:		fortress\sbaburin,Sergey Baburin
-- Create date: <01/16/2013>
-- Modified: 2/14/2014 by fortress\aboridko Alexander Boridko PREP-364 Removed DISTINCT from the finall select statement.
-- Description:	is used by two reports: Operations/GetGlobalAggregatePositions_Permal and Complience/GetGlobaleAggregatePositions
-- [compliance].[spGetGlobalAggregatePositions] @date='20130724', @live=0
CREATE PROCEDURE [compliance].[spGetGlobalAggregatePositions]
  @date      DATETIME,
  @live      BIT = 0,
  @funds     NVARCHAR(1000) = NULL,
  @countries NVARCHAR(1000) = NULL,
  @debug     BIT = 0
AS
  BEGIN
      SET NOCOUNT ON;

      ---Remove comment this section to test the procedure---
      		--DECLARE @date datetime = '20140213' 
      		--DECLARE @live bit = 1
      		--DECLARE @funds nvarchar(1000)  = null
      		--DECLARE @countries nvarchar(1000) = null
      		--DECLARE @debug bit = 1	
      --		EXEC [compliance].[spGetGlobalAggregatePositions]  
      --			@date = @date,
      --			@live  = @live,
      --			@funds  = @funds,
      --			@countries  = @countries,
      --			@debug = 1
      ---END TEST PARAMETER SECTION-----
      DECLARE @p1ReportingDbName VARCHAR(50)  = [dbo].[fnGetP1ReportingG2DatabaseName](),
              @filterFunds       BIT = CASE
                  WHEN @funds IS NULL
                        OR LEN(LTRIM(RTRIM(@funds)))=0
                  THEN 0
                  ELSE 1
                END,
              @filterCountries   BIT = CASE
                  WHEN @countries IS NULL
                        OR LEN(LTRIM(RTRIM(@countries)))=0
                  THEN 0
                  ELSE 1
                END

      CREATE TABLE #countries
        (
           [countryId] INT
        )

      IF(@filterCountries=1)
        BEGIN
            INSERT INTO #countries
                        ([countryId])
            SELECT ltrim(rtrim([Items]))
            FROM   dbo.fnStringSplit(@countries, ',')
        END

      CREATE TABLE #funds
        (
           [code] VARCHAR(25)
        )

      IF(@filterFunds=1)
        BEGIN
            INSERT INTO #funds
                        ([code])
            SELECT ltrim(rtrim([Items]))
            FROM   dbo.fnStringSplit(@funds, ',')
        END

      SELECT *
      INTO   #positionsEnriched
      FROM   [dbo].[fnTableDefinitionPositionsEnriched]()

      CREATE NONCLUSTERED INDEX IDX_Temp_PositionsEntiched
        ON #positionsEnriched ([notTradingFundFlag], [testFundFlag])
        INCLUDE ([fund], [sptDescription], [sid], [tid], [quantity]);

      DECLARE @sql VARCHAR(8000)

      SET @sql = @p1ReportingDbName+'.[P1TC].[spPositionsEnriched] @live = '+CONVERT(VARCHAR, @live)+', @asOfDate = '''+CONVERT(VARCHAR, @date, 112)+''' '

      INSERT INTO #positionsEnriched
      EXEC(@sql) AT SELF

      SELECT *
      INTO   #securityInfo
      FROM   P1TC.[fnTableDefinitionPositionSecurityInfo]()

      SET @sql = @p1ReportingDbName+'.[P1TC].[spPositionSecurityInfo] @live = '+CONVERT(VARCHAR, @live)+', @asOfDate = '''+CONVERT(VARCHAR, @date, 112)+''' '+', @debug = '+CONVERT(VARCHAR, @debug)

      INSERT INTO #securityInfo
      EXEC(@sql) AT SELF

      /*
      Filtering Securities by Security Primary Type Id:
      	
      securityPrimaryTypeId	description
      6						Equity
      7						Equity Option
      8						Equity Future
      10						Equity Future Option
      104						Equity Swap
      */
      SELECT [tid] = p.tid,
             [sid] = p.[sid],
             [underlyingSid] = si.[underlyingSid],
             [fund] = p.[fund],
             [qty] = p.[quantity],
             [putCall] = si.[putCallFlag],
             [optionContractSize] = si.optionContractSize,
             [sptDescription] = p.[sptDescription],
             [country] = COALESCE(si.[underlyingExchangeCountry], si.[underlyingIncIssuerCountry], si.[primaryExchangeCountry], si.[primaryIncIssuerCountry]),
             [primaryCode] = si.[primaryBbgCode],
             [underlyingCode] = si.[underlyingBbgUCode],
             [exchangeId] = ISNULL(si.[underlyingExchangeId], si.[primaryExchangeId]),
             [countryId] = COALESCE(si.[underlyingExchangeCountryId], si.[underlyingIncIssuerCountryId], si.[primaryExchangeCountryId], si.[primaryIncIssuerCountryId])
      INTO   #positions
      FROM   #positionsEnriched p
             INNER JOIN #securityInfo si
               ON p.[sid]=si.[sid]
      WHERE  si.secPrimaryTypeId IN (6, 7, 8, 10, 104)
             AND p.testFundFlag=0
             AND p.notTradingFundFlag=0
             AND (@filterCountries=0
                   OR COALESCE(si.[underlyingExchangeCountryId], si.[underlyingIncIssuerCountryId], si.[primaryExchangeCountryId], si.[primaryIncIssuerCountryId]) IN (SELECT countryId
                                                                                                                                                                       FROM   #countries))
             AND (@filterFunds=0
                   OR p.fund COLLATE Latin1_General_BIN2 IN (SELECT code COLLATE Latin1_General_BIN2
                                                             FROM   #funds))

      DECLARE @tids VARCHAR(6000);

      SELECT @tids=COALESCE(@tids+', ', '')+CAST(cast(tid AS DECIMAL(13, 0)) AS VARCHAR(20))
      FROM   (SELECT DISTINCT tid
              FROM   #positions
              WHERE  tid>0) A

      SELECT *
      INTO   #equityInfo
      FROM   P1TC.fnTableDefinitionEquityInfo()

      IF(LEN(@tids)>0)
        BEGIN
            INSERT INTO #equityInfo
            EXEC P1TC.spGetEquityInfo
              @date,
              @tids
        END

      -- OLD BBG RETRIEVAL END
      -- Final Calculations:
      SELECT posn.[tid],
             posn.[sid],
             posn.[country],
             posn.[compName],
             [bPSOwned] = qty*(CASE
                                 WHEN posn.[type] LIKE '%Option%'
                                 THEN posn.[optionContractSize]
                                 ELSE 1
                               END)
      FROM   (SELECT  pos.[tid],
                      pos.[sid],
                      pos.[underlyingSid],
                      pos.[fund],
                      [qty] = (pos.[qty]/(info.EQY_SH_OUT*1000000+0.1)*10000),
                      [compName] = info.issuerName,
                      [type] = pos.[sptDescription],
                      (pos.[optionContractSize]*CASE
                                                  WHEN pos.[putCall]='C'
                                                  THEN 1
                                                  ELSE -1
                                                END) AS [optionContractSize],
                      pos.[country],
                      pos.[primaryCode],
                      pos.[underlyingCode],
                      0.0 AS [BPSOwned]
              FROM   #positions AS pos
                     LEFT JOIN #equityInfo info
                       ON pos.tid=info.tid) posn

      DROP TABLE #countries

      DROP TABLE #funds

      DROP TABLE #equityInfo

      DROP TABLE #positionsEnriched

      DROP TABLE #securityInfo

      DROP TABLE #positions
  END

