
create procedure [dispatcher].[Initialize](@targetService varchar(200))
as
begin

	set nocount on;
	set xact_abort on;
	
	declare @dialogId uniqueidentifier
	select @dialogId = dialogId
	from dispatcher.Conversation
	where targetService = @targetService

	if @dialogId is not null
	begin
		print 'Dispatcher target is already present. Nothing to do.'
		return;
	end

	begin dialog conversation @dialogId
		from service DispatcherService to service @targetService
		on contract DispatcherContract with encryption = off;
	
	insert into dispatcher.Conversation(targetService, dialogId)
	values (@targetService, @dialogId)
	
	print 'Dispatcher target successfully set up.'

end


