

CREATE FUNCTION [FTBTrade].[dbo_Security_GetSidsToDelete](
	@asOf date,
	@daysToSave int,
	@startSid bigint,
	@endSid bigint
) 
RETURNS TABLE AS RETURN
	--- Get the sids that qualify, perform some basic sanity checks.
	select
		s.sid
	from
		FTBTrade.dbo_Security as s with (nolock)
			cross join FTBTrade.dbo_SecurityPrimaryType_Transposed as spt
				
			--- Extension tables.
			left outer join FTBTrade.dbo_AssetBacked as ab with (nolock) on ab.sid = s.sid
			left outer join FTBTrade.dbo_Bond as b with (nolock) on b.sid = s.sid
			left outer join FTBTrade.dbo_CommoditySwap as csw with (nolock) on csw.sid = s.sid
			left outer join FTBTrade.dbo_CreditDefaultSwap as cds with (nolock) on cds.sid = s.sid
			left outer join FTBTrade.dbo_CreditUnderlying as cu with (nolock) on cu.sid = s.sid
			left outer join FTBTrade.dbo_Equity as e with (nolock) on e.sid = s.sid
			left outer join FTBTrade.dbo_Future as f with (nolock) on f.sid = s.sid
			left outer join FTBTrade.dbo_InterestRateSwap as irs with (nolock) on irs.sid = s.sid
			left outer join FTBTrade.dbo_Option as o with (nolock) on o.sid = s.sid
			left outer join FTBTrade.dbo_VolVarSwap as vvs with (nolock) on vvs.sid = s.sid
	where			
		--- Range.
		(@startSid is null or @startSid <= s.sid) and
		(@endSid is null or s.sid <= @endSid) and

		--- Filter on the end date or maturity date.
		(
			--- Security.
			s.endDate < dateadd(d, @daysToSave * -1, @asOf) or
			
			--- Asset backed and bond.
			--- TODO: Move to final maturity when field is attached to table.
			ab.maturityDate < dateadd(d, @daysToSave * -1, @asOf) or
			b.maturityDate < dateadd(d, @daysToSave * -1, @asOf) or
			
			csw.terminatingDate < dateadd(d, @daysToSave * -1, @asOf) or
			cds.terminationDate < dateadd(d, @daysToSave * -1, @asOf) or
			cu.validTo < dateadd(d, @daysToSave * -1, @asOf) or
			f.lastTradeDate < dateadd(d, @daysToSave * -1, @asOf) or
			(s.securityPrimaryTypeId not in (spt.[Interest Rate Swap Template], spt.[Forward Rate Agreement Template]) and irs.terminationDate < dateadd(d, @daysToSave * -1, @asOf)) or
			o.expiryDate < dateadd(d, @daysToSave * -1, @asOf) or
			vvs.terminatingDate < dateadd(d, @daysToSave * -1, @asOf)
		) and
		
		--- NOTE: No futures for now.
		s.securityPrimaryTypeId not in (
			select
				spt.securityPrimaryTypeId
			from
				FTBTrade.dbo_SecurityPrimaryType as spt
			where
				spt.detailTable = 'Future'
		) and
		
		--- NOTE: This check must exist, as there is no referential integrity
		--- between FTBExternal and FTBTrade.
		not exists (
			select
				ep.*
			from
				FTBExternal.dbo_ExternalPosition as ep with (nolock)
			where
				ep.sid = s.sid
		) and
				
		--- Some basic sanity checks.
		--- Orders.
		not exists (
			select
				o.*
			from
				FTBTrade.dbo_Order as o with (nolock)
			where
				o.sid = s.sid
		) and
		
		--- Repos.
		not exists (
			select
				r.*
			from
				FTBTrade.dbo_Repo as r with (nolock)
			where
				r.sid = s.sid						
		) and
		
		--- Issuer fundamental security.
		not exists (
			select
				i.*
			from
				FTBTrade.dbo_Issuer as i with (nolock)
			where
				i.fundamentalSid = s.sid
		) and
		
		--- Underlying security.
		not exists (
			select
				p.*
			from
				FTBTrade.dbo_Security as p with (nolock)
			where
				p.underlying_sid = s.sid
		) and
				
		--- Not on credit underlying.
		not exists (
			select
				cu.*
			from
				FTBTrade.dbo_CreditUnderlying as cu with (nolock)
			where
				cu.obligationSid = s.sid
		) and
		
		--- No marks.
		not exists (
			select
				m.*
			from
				FTBTrade.dbo_MarkOption as m with (nolock)
			where
				m.sid = s.sid
		) and
		
		not exists (
			select
				m.*
			from
				FTBTrade.dbo_MarkIndex as m with (nolock)
			where
				m.sid = s.sid				
		)



