create procedure [dbo].[PublishDataChanges](@changes dbo.DataChange readonly) as
begin
	
	set nocount on;
	set xact_abort on;

    --- The batch size.
    declare @batchSize int = 1000

    --- The last ordinal.
    declare @lastOrdinal int = 0
    
    --- The message.
    declare @message varchar(max)
	
    --- While there are items.
    while (exists(select 1 from @changes where __ordinal > @lastOrdinal))
    begin
        --- Construct message
        set @message = (
            select
			'<n>' +
				'<a>' + [action] + '</a>' +
				'<c>' + convert(varchar(24), [created], 121) + '</c>' +
				'<s>' + [schema] + '</s>' +
				'<t>' + [table] + '</t>' +
				'<p>' + [primaryKey] + '</p>' +
				isnull('<f>' + [facets] + '</f>', '') +
            '</n>'
            from
			(
				select top (@batchSize) *
				from @changes
				where __ordinal > @lastOrdinal
				order by __ordinal
			) as n
            for xml path(''), type
        ).value('.', 'varchar(max)')

        --- Dispatch message
        if @message is not null
        begin
            declare @binary varbinary(max) = cast(('<m>' + @message + '</m>') as varbinary(max))
            exec dispatcher.Send 'DataChangeQueue_Service', @binary
        end

        --- Increase last ordinal.
        set @lastOrdinal =
		(
            select top 1 __ordinal
            from (select top (@batchSize) __ordinal from @changes where __ordinal > @lastOrdinal order by __ordinal) as n
            order by __ordinal desc
        )
    end
end

