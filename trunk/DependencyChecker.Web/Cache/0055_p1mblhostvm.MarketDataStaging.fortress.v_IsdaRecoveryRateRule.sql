
create view [fortress].[v_IsdaRecoveryRateRule] as
	--- Map of credit tiers to senior/not senior
	with CreditTierMap as (
		select
			ct.creditTierId as CreditTierId,
			cast(
				case ct.creditTierId
					when ctp.SecuredOrDomestic then 1
					when ctp.SeniorUnsecured then 1
					when ctp.Tier1Capital then 0
					when ctp.JuniorSubordinated then 0
					when ctp.Subordinated then 0
				end as bit) as Senior
		from
			FTBTrade.dbo_CreditTier as ct with (nolock)
				cross join fortress.v_CreditTier_Pivot as ctp
		where
			ct.creditTierId <> ctp.NotSpecified
	),
	--- Emerging markets.
	EmergingMarket as(
		select 'AE' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'AE' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'AR' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'AR' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'BG' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'BG' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'BH' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'BH' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'BR' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'BR' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'CL' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'CL' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'CN' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'CN' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'CO' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'CO' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'CZ' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'CZ' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'EE' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'EE' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'EG' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'EG' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'HU' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'HU' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'ID' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'ID' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'IN' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'IN' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'JO' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'JO' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'KW' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'KW' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'LK' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'LK' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'LT' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'LT' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'LV' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'LV' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'MA' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'MA' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'MU' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'MU' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'MX' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'MX' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'MY' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'MY' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'OM' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'OM' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'PE' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'PE' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'PH' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'PH' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'PK' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'PK' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'PL' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'PL' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'QA' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'QA' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'RO' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'RO' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'RU' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'RU' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'SK' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'SK' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'TH' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'TH' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'TR' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'TR' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'ZA' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'ZA' as CountryCode, cast(1 as bit) as Senior, 25.0 as RecoveryRate
	),
	--- Special cases.
	SpecialCase as (
		--- XS = Europe general
		select 'XS' as CountryCode, cast(0 as bit) as Senior, 25.0 as RecoveryRate union all
		select 'XS' as CountryCode, cast(1 as bit) as Senior, 40.0 as RecoveryRate union all
		--- Japan
		select 'JP' as CountryCode, cast(0 as bit) as Senior, 15.0 as RecoveryRate union all
		select 'JP' as CountryCode, cast(1 as bit) as Senior, 35.0 as RecoveryRate
	),
	--- All special cases.
	AllSpecialCase as (
		select * from EmergingMarket union all
		select * from SpecialCase
	)
	--- All special cases.
	select
		sc.CountryCode, ctm.CreditTierId, sc.RecoveryRate
	from
		CreditTierMap as ctm
			inner join AllSpecialCase as sc on
				sc.Senior = ctm.Senior
	union all
	--- All other countries.
	select
		c.code2 as CountryCode, ctm.CreditTierId, case ctm.Senior when 0 then 20.0 else 40.0 end as RecoveryRate
	from
		FTBTrade.dbo_Country as c with (nolock)
			cross join CreditTierMap as ctm
	where
		c.code2 not in (select CountryCode from AllSpecialCase)


