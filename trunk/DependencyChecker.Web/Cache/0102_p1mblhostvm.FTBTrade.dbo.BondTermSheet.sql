
CREATE view [dbo].[BondTermSheet] as
select
	s.sid,
	s.description,
	s.securityPrimaryTypeId,
	s.securitySubTypeId,
	domicileCountry = c.code2,
	b.faceValue,
	b.coupon,
	b.couponTypeId,
	b.spread,
	b.daysToSettle,
	b.frequencyId,
	b.businessDayConventionId,
	b.dayCountConventionId,
	b.accrualStartDate,
	maturityDate = isnull(b.maturityForCalculationsDate, b.finalMaturityDate),
	b.firstCouponDate,
	b.nextCouponDate,
	b.penultimateCouponDate,
	b.daysBeforeCouponRefix,
	b.manualFlag,
	b.inflationLinkedFlag,
	b.calculatedFloaterFlag,
	b.perpetualFlag,
	b.resetIndexSid,
	b.referenceIndexSid,
	b.fixToFloatDate,
	d.eomFlag,
	b.version

from [Security] s
join [Issuer] i on s.issuerId = i.issuerId
join [Country] c on i.domicileCountryId = c.countryId
join [Bond] b on s.sid = b.sid
join [DayCountConvention] d on b.dayCountConventionId = d.dayCountConventionId


