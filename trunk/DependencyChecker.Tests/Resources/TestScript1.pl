﻿use DBI;
use DBI qw(:sql_types);
use Text::CSV;
use POSIX 'strftime';
use Cwd;
use Net::SMTP;

require '..\Globals.pl';

#-----------------------------------------#
# Globals
#-----------------------------------------#

#-------------------------------------#
# Connect to DB 
#-------------------------------------#

$DB_SERVER = "NY-GMITFTBDB1";
my $offset = 1;
my $wday = (localtime)[6];

if($wday == 1)
{
	$offset = 3;
}
my $date = strftime '%Y%m%d', localtime;
my  $date1 = strftime '%Y%m%d', localtime(time-$offset*24*3600);  #T-1
$dsn = "Provider=sqloledb;Integrated Security=SSPI;";
$dsn .= "Server=${DB_SERVER};Database=MarkIt";
my $dbh = DBI->connect("dbi:ADO:$dsn",  "", "") || die "Database connection not made: $DBI::errstr";

my $sql = "
	create table ##input
	(
		  row_id int,
		  ticker varchar(50),
		  tier varchar(50),
		  doc_clause varchar(50),
		  ccy varchar(50),
		  terminating_date datetime,
		  effective_date datetime,
		  premium float,
		  upfront_payment float,
		  notional float,
		  value_date datetime,
		  source varchar(20)
	);";

$dbh->{RaiseError} = 1;   
my $sth = $dbh->prepare( $sql );
$sth->execute();

#print $sql, "\n";;

$sql = "insert ##input values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,'MPT1')";
$sth = $dbh->prepare( $sql );
#my $tk, $tier, $doc, $ccy, $mat, $eff, $cpn, $upf, $notion;

#$date = '20110706';
my $dt2 = substr($date1, 0, 4) . '-' . substr($date1, 4, 2) . '-' . substr($date1, 6, 2);
my $input_dir = "\\\\ny-files1\\batch\\Data\\CDSPricing";
#my $input_dir = "\\\\fortressinv.com\\files\\HedgeNonRes\\GMIT\\Mike\\CDS_dbso";

#"glob" seems not support Universal Directory
my $cwd = getcwd();
chdir($input_dir);

my @f = glob "CDSPositions" . $date . "-*.CSV";


my $EMAIL_SUBJ;

if(scalar(@f) == 0)
{
   $EMAIL_SUBJ = "!!!CDSMarks_dbso task on ny-quant1 - Error: No input found for $date";
   email_notify($EMAIL_SUBJ);
   exit(0);
}

@f = sort{$b cmp $a } (@f);
my $input_file = $input_dir . "\\" . $f[0];
chdir($cwd);
#$input_file = $input_dir . "\\CDSPositions20110720-125210.CSV";
#print "source file ", $input_file, "\n";

my $csv = Text::CSV->new();
  
open (CSV, "<", $input_file) or die "Can't open file $input_file";

<CSV>; # skip first line
my $positions = 0, $invalid = 0;
while (<CSV>) {
    $positions++;
    my $id = 0;
    if ($csv->parse($_)) 
    {
     ($id, $tk, $tier, $doc, $ccy, $mat, $eff, $cpn, $upf, $notion)= $csv->fields();
     
       #print "$id, $tk, $tier, $doc, $ccy, $mat, $eff, $cpn, $upf, $notion\n";

        if ($id <= 0)
	{
		$invalid++;
		next;
	}
        #work due to some WSO tk uniq problem
		my @t = split(/[ ]/, $tk);
		$tk = $t[0]; 

		if($tk eq '')
		{
		   next;
		}
	

		if($tier eq ''  )
		{
			$tier = undef;
		}
		
		if($doc eq '')
		{
			$doc = undef;
		}

		if($tk =~ /^CDX/ && $tk =~ /[0-9]$/)
		{
			$tier = undef;
			$doc = undef;
		}

		$mat = substr($mat, 0, 4) . '-' . substr($mat, 4, 2) . '-' . substr($mat, 6, 2);
		$eff =  substr($eff, 0, 4) . '-' . substr($eff, 4, 2) . '-' . substr($eff, 6, 2);

        my $i = 1;
        $sth->bind_param($i++, $id, SQL_INTEGER);
        $sth->bind_param($i++, $tk , { ado_size => 50 });
        $sth->bind_param($i++, $tier, { ado_size => 20 });
        $sth->bind_param($i++, $doc, { ado_size => 20 });
        $sth->bind_param($i++, $ccy, { ado_size => 20 });
        $sth->bind_param($i++, $mat);
        $sth->bind_param($i++, $eff);
        $sth->bind_param($i++, $cpn);
        $sth->bind_param($i++, $upf);
        $sth->bind_param($i++, $notion);
        $sth->bind_param($i++, $dt2);                                                      
	my $rv = 0 ;
	eval {$rv = $sth->execute(); };
	if (!$rv) {
		print "Process $tk failed \n $dbh->errstr \n";
		$invalid ++;
		next;
	}
  } else {
    my $err = $csv->error_input;
    print "Failed to parse line: $err";
  }
}  # end while procees
close CSV;

if ($positions == 0)
{
   $EMAIL_SUBJ = "!!!CDSMarks_dbso task on ny-quant1 - Error: empty input positions in $date file";
   email_notify($EMAIL_SUBJ);
   exit(0);

}


$sql = "create table #output (a varchar(1));
exec SqlNativeDll.dbo.PriceCDS \@qry = 'select * from ##input', \@output_table = '#output'
select i.row_id Position_ID, convert(varchar(8), i.value_date, 112) value_date,
  i.ticker Ticker, i.tier Seniority, i.doc_clause DocClause, ccy Currency, 
  convert(varchar(8), i.terminating_date, 112)  MaturityDate, 
  convert(varchar(8), i.effective_date, 112) EffectiveDate, 
  i.premium Coupon, upfront_payment, notional,
  100*o.fair_value/notional dirty, 
  100*o.fair_minus_accrued_interest/notional clean,
  fair_value market_value,
  accrued_interest,
  o.par_spread * 10000 par_spread, 	
  dvox_spread cs01,
  convert(varchar(8), o.next_cashflow_dt, 112) next_cashflow_dt, 
  convert(varchar(8), o.prev_cashflow_dt, 112) prev_cashflow_dt, 
      Recovery*100 Recovery, Spread6m * 10000 Spread6m, 
      Spread1y * 10000 Spread1y, Spread2y * 10000 Spread2y,
      Spread3y * 10000 Spread3y, Spread4y * 10000 Spread4y, 
      Spread5y * 10000 Spread5y, Spread7y * 10000 Spread7y,
      Spread10y * 10000 Spread10y, Spread15y * 10000 Spread15y, 
      o.internal_notes
    from #output o join ##input i on o.row_id = i.row_id
drop table ##input
drop table #output";
$sth = $dbh->prepare( $sql );
$sth->execute();

print "1";

my $output_file = "\\\\ny-files1\\sharedapps\\CDSPricing\\CDSMarks" . $date .".csv";
#my $output_file = ".\\dbso.csv";

my $ret = open(OUTFILE, ">" . $output_file ) ;

if (!$ret)
{
 $EMAIL_SUBJ = "!!!CDSMarks_dbso task on ny-quant1 - Error: Couldn't open the $output_file File to write";
 print "cannot open the file to write";
 exit (0);
}
print "2";

print OUTFILE join(", ", @{$sth->{NAME}}), "\n";
my @row;
my $marked = 0;
while (@row = $sth->fetchrow_array) 
{ 
	printf OUTFILE join (",", @row, "\n");
	if (!@row[-1])
	{
		$marked++;
	}
}
close(OUTFILE);

print "3";

if ($positions == $marked)
{
	$EMAIL_SUBJ = "CDSMarks_dbso task on ny-quant1 - Success - marked $positions positions";
}
else 
{
	$EMAIL_SUBJ = "!!!CDSMarks_dbso task on ny-quant1 - Partial - marketd $marked out of $positions positions";
}

email_notify($EMAIL_SUBJ);

sub email_notify
{
  my($email_sub) = @_;

@EMAIL_TO = ('Group_GMIT@fortress.com','CreditFundsOTCFXOps@fortress.com');
$EMAIL_FROM = 'Group_GMIT@fortress.com';

#-------------------------------------#
# Configure SMTP connection
#-------------------------------------#
$smtp = Net::SMTP->new($EMAIL_SVR);

  
  $smtp->mail($EMAIL_FROM);
  $smtp->to(@EMAIL_TO);

  $smtp->data();
  $smtp->datasend("To: " . join(',', @EMAIL_TO) . "\n");
  $smtp->datasend("Subject: $email_sub \n");
  $smtp->datasend("\n\n" );

#-------------------------------------#
# Close SMTP connection
#-------------------------------------#
$smtp->dataend();
$smtp->quit;
#-------------------------------------#

}