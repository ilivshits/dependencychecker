﻿-- =======================================================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Poulates Forensics.[dbo].[instrument], 
-- Forensics.staging.country_codes, Forensics.dbo.fig_instrument_group
-- =======================================================================
CREATE PROCEDURE [staging].[PopulateFIGInstrumentGroup] 
AS
BEGIN

	SET NOCOUNT ON;
	BEGIN TRY
 			--drop table #imagine_detail
		
		DECLARE @sql VARCHAR(300);
		
		--print '1 ' + CONVERT(varchar, SYSDATETIME(), 121)

		-- Update/Merge with live 
		CREATE TABLE #imagine_detail
		(
			Value10 INT,
			[Long_Security_Name] VARCHAR(200) collate DATABASE_DEFAULT,
			country_code VARCHAR(30) collate DATABASE_DEFAULT,
			Issuer_Name VARCHAR(80) collate DATABASE_DEFAULT,
			[Sector_Name:_GICS] VARCHAR(100) collate DATABASE_DEFAULT,
			Acct VARCHAR(50) collate DATABASE_DEFAULT,
			Legal_Entity_Name VARCHAR(100) collate DATABASE_DEFAULT,
			FIG_Asset_Class VARCHAR(50) collate DATABASE_DEFAULT,
			FIG_Asset_Type VARCHAR(50) collate DATABASE_DEFAULT
		);
		
		
		
        SET @sql = 'select * from openquery(FTB_Kerberos, ''SET FMTONLY OFF;exec P1ReportingG2.dbo.spGetCountryCodes_Cube @live=1'')'

		INSERT INTO #imagine_detail
		EXEC(@sql);

		--print '2 ' + CONVERT(varchar, SYSDATETIME(), 121);

		MERGE INTO staging.country_codes dest
		USING #imagine_detail src
		ON dest.Value10=src.Value10 and dest.Acct=src.Acct and dest.Legal_Entity_Name=src.Legal_Entity_Name
		WHEN MATCHED 
			THEN UPDATE SET 
				dest.[Long_Security_Name] = src.[Long_Security_Name],
				dest.[country_code] = src.[country_code],
				dest.[Issuer_Name] = src.[Issuer_Name],
				dest.[Sector_Name:_GICS] = src.[Sector_Name:_GICS],
				dest.[FIG_Asset_Class] = src.[FIG_Asset_Class],
				dest.[FIG_Asset_Type] =  src.[FIG_Asset_Type]
		WHEN NOT MATCHED BY TARGET 
			THEN INSERT ( 
				[Value10],
				[Long_Security_Name],
				[country_code],
				[Issuer_Name],
				[Sector_Name:_GICS],
				[Acct],
				[Legal_Entity_Name],
				[FIG_Asset_Class],
				[FIG_Asset_Type])
			 VALUES (
				src.[Value10],
				src.[Long_Security_Name],
				src.[country_code],
				src.[Issuer_Name],
				src.[Sector_Name:_GICS],
				src.[Acct],
				src.[Legal_Entity_Name],
				src.[FIG_Asset_Class],
				src.[FIG_Asset_Type]);
				
		--print '3 ' + CONVERT(varchar, SYSDATETIME(), 121)
				
		drop table #imagine_detail
	
		--SELECT tid, Country INTO #country FROM FTB.FortressShared.enrichment.v_equity_country_code
		
		--print '4 ' + CONVERT(varchar, SYSDATETIME(), 121)
		
		select le_name
		, fundId
		into #legalentity
		from FTB.P1Reporting.foffice.v_legalentity
		
		select 
			iso2
			, region1
			, region2
		into #bus_tags_detail
		from FTB.[P1Reporting].dbo.v_bus_tags_detail
		
		SELECT *
		INTO #all_trader_tags_detail
		FROM OPENQUERY(FTB_Kerberos, 'SELECT * FROM [P1ReportingG2].[P1TC].[fnTableDefinitionTagger]()');
		
		SET @sql = 'SELECT * FROM OPENQUERY(FTB_Kerberos, ''SET FMTONLY OFF;EXEC [P1ReportingG2].[P1TC].[spGetTagValues];'')';
		INSERT INTO #all_trader_tags_detail
		EXEC(@sql);
		
		SELECT 	fundId, Value10 AS Id,	strategy_id 
			,FIG_Asset_Class = max(CASE 
			
					------------ the following line is split and put in corresponding sections, below
					--WHEN fundId = 48 and d.FIG_Asset_Class IN ('Credit', 'Rates') THEN 'Fixed Income'
			
					------------ FX
					WHEN fundId in (48, 60) and d.FIG_Asset_Class = 'FX' AND left(s.strategy, 2) = 'K-' THEN 'Fixed Income' 
					WHEN fundId in (48, 60) and d.FIG_Asset_Class = 'FX' AND left(s.strategy, 2) = 'R-' THEN 'Fixed Income' 
					WHEN d.FIG_Asset_Class = 'FX' AND left(s.strategy, 2) = 'K-' THEN 'Credit' 
					WHEN d.FIG_Asset_Class = 'FX' AND left(s.strategy, 2) = 'R-' THEN 'Rates'
					WHEN d.FIG_Asset_Class = 'FX' AND left(s.strategy, 2) = 'E-' THEN 'Equity' 
					WHEN d.FIG_Asset_Class = 'FX' AND left(s.strategy, 2) = 'C-' THEN 'Commodity'
					  
					------------ Equity
					WHEN fundId in (48, 60) and d.FIG_Asset_Class = 'Equity' AND left(s.strategy, 2) = 'K-' THEN 'Fixed Income' 
					WHEN fundId in (48, 60) and d.FIG_Asset_Class = 'Equity' AND left(s.strategy, 2) = 'R-' THEN 'Fixed Income' 
					WHEN d.FIG_Asset_Class = 'Equity' AND left(s.strategy, 2) = 'C-' THEN 'Commodity'
					WHEN d.FIG_Asset_Class = 'Equity' AND left(s.strategy, 2) = 'K-' THEN 'Credit' 
					WHEN d.FIG_Asset_Class = 'Equity' AND left(s.strategy, 2) = 'R-' THEN 'Rates'  
					WHEN d.FIG_Asset_Class = 'Equity' AND left(s.strategy, 2) = 'X-' THEN 'FX'
					
					----------- Credit
					WHEN fundId in (48, 60) and d.FIG_Asset_Class = 'Credit' AND left(s.strategy, 2) = 'R-' THEN 'Fixed Income'
					WHEN d.FIG_Asset_Class = 'Credit' AND left(s.strategy, 2) = 'R-' THEN 'Rates'
					WHEN d.FIG_Asset_Class = 'Credit' AND left(s.strategy, 2) = 'C-' THEN 'Commodity'
					WHEN d.FIG_Asset_Class = 'Credit' AND left(s.strategy, 2) = 'X-' THEN 'FX'
					WHEN d.FIG_Asset_Class = 'Credit' AND left(s.strategy, 2) = 'E-' THEN 'Equity'
					WHEN fundId in (48, 60) and d.FIG_Asset_Class = 'Credit' THEN 'Fixed Income'
					  
					----------- Rates
					WHEN fundId in (48, 60) and d.FIG_Asset_Class = 'Rates' AND left(s.strategy, 2) = 'K-' THEN 'Fixed Income'
					WHEN d.FIG_Asset_Class = 'Rates' AND left(s.strategy, 2) = 'C-' THEN 'Commodity'
					WHEN d.FIG_Asset_Class = 'Rates' AND left(s.strategy, 2) = 'K-' THEN 'Credit'
					WHEN d.FIG_Asset_Class = 'Rates' AND left(s.strategy, 2) = 'X-' THEN 'FX'
					WHEN d.FIG_Asset_Class = 'Rates' AND left(s.strategy, 2) = 'E-' THEN 'Equity'
					WHEN fundId in (48, 60) and d.FIG_Asset_Class = 'Rates' THEN 'Fixed Income'
					
					-- Commodity
					WHEN fundId in (48, 60) and d.FIG_Asset_Class = 'Commodity' AND left(s.strategy, 2) IN ('K-','R-') THEN 'Fixed Income'  
					WHEN d.FIG_Asset_Class = 'Commodity' AND left(s.strategy, 2) = 'E-' THEN 'Equity'
					WHEN d.FIG_Asset_Class = 'Commodity' AND left(s.strategy, 2) = 'X-' THEN 'FX'
					WHEN d.FIG_Asset_Class = 'Commodity' AND left(s.strategy, 2) = 'K-' THEN 'Credit'
					WHEN d.FIG_Asset_Class = 'Commodity' AND left(s.strategy, 2) = 'R-' THEN 'Rates'
					
					--------------------------------
					  
					WHEN d.FIG_Asset_Class is null or d.FIG_Asset_Class='' THEN 'Other' 
					ELSE d.FIG_Asset_Class 
					END)
			,fig_sector= IsNull(max(CASE 
					WHEN fundId in (48, 60) and d.FIG_Asset_Class in ('Credit', 'Rates') THEN d.FIG_Asset_Class 
					WHEN d.FIG_Asset_Class = 'Equity' and  isnull(d.[Sector_Name:_GICS], '') <> '' THEN d.[Sector_Name:_GICS]
					WHEN d.FIG_Asset_Class = 'FX' AND left(s.strategy, 2) IN ('K-', 'E-', 'R-', 'C-') THEN 'FX' 
					
					--added
					WHEN d.FIG_Asset_Class = 'Credit' AND left(s.strategy, 2) IN ('C-', 'R-', 'X-', 'E-') THEN 'Credit'
					WHEN d.FIG_Asset_Class = 'Rates' AND left(s.strategy, 2) IN ('C-', 'K-', 'X-', 'E-') THEN 'Rates'
					WHEN d.FIG_Asset_Class = 'Commodity' AND left(s.strategy, 2) IN ('K-','R-','E-', 'X-') THEN 'Commodity'
					WHEN d.FIG_Asset_Class = 'Equity'AND left(s.strategy, 2) IN ('K-', 'X-', 'R-', 'C-') THEN 'Equity'
					
					
					WHEN d.FIG_Asset_Class = 'Equity' THEN 'Index&Other'
					WHEN d.FIG_Asset_Class = 'Commodity' THEN Issuer_Name
					ELSE  d.FIG_Asset_Class 
					END), '')
			,fig_region = max(CASE
						-- In case of FAMF
						-- If it's Commodity and will stay Commodity 
						WHEN fundId in (48, 60) and FIG_Asset_Class = 'Commodity' AND left(s.strategy, 2) NOT IN ('K-', 'E-', 'R-', 'X-') 
							THEN COALESCE(tag.[BusPA], tag.[investorCountryOverrideRegion2], 'Commodity') 
						-- If it will become Commodity 
						WHEN fundId in (48, 60) and  left(s.strategy, 2) = 'C-'
							THEN COALESCE(tag.[BusPA], tag.[investorCountryOverrideRegion2], 'Commodity') 
						-- using region2
						WHEN fundId in (48, 60) and isNull(btd.region2,'')='' THEN  'Other'
						WHEN fundId in (48, 60) THEN btd.region2
						
						-- Other funds
						-- If it's Commodity and will stay Commodity  ->  'Commodity'
						WHEN d.FIG_Asset_Class = 'Commodity'  AND left(s.strategy, 2) NOT IN ('K-', 'E-', 'R-', 'X-')	THEN 'Commodity'
						-- If it will become Commodity   ->   Commodity
						WHEN left(s.strategy, 2) = 'C-' THEN 'Commodity'
						WHEN isNull(btd.region1, '') = '' THEN 'Other'
						ELSE btd.region1
						END)
			,fig_regionEx = max(CASE 
						-- HEDGES
						WHEN d.FIG_Asset_Class = 'FX' AND left(s.strategy, 2) IN ('K-', 'E-', 'R-',  'C-') THEN 'FX'
						WHEN d.FIG_Asset_Class = 'Credit' AND left(s.strategy, 2) IN ('C-', 'R-', 'X-', 'E-') THEN 'Credit'
						WHEN d.FIG_Asset_Class = 'Rates' AND left(s.strategy, 2) IN ('C-', 'K-', 'X-', 'E-') THEN 'Rates'
						WHEN d.FIG_Asset_Class = 'Commodity' AND left(s.strategy, 2) IN ('K-','R-','E-', 'X-') THEN 'Commodity'
						WHEN d.FIG_Asset_Class = 'Equity'AND left(s.strategy, 2) IN ('K-', 'X-', 'R-', 'C-') THEN 'Equity'
						
						-- In case of FAMF
						-- If it's Commodity and will stay Commodity 
						WHEN fundId in (48, 60) and FIG_Asset_Class = 'Commodity' AND left(s.strategy, 2) NOT IN ('K-', 'E-', 'R-', 'X-') 
							THEN COALESCE(tag.[BusPA], tag.[investorCountryOverrideRegion2], 'Commodity') 
						-- If it will become Commodity 
						WHEN fundId in (48, 60) and  left(s.strategy, 2) = 'C-'
							THEN COALESCE(tag.[BusPA], tag.[investorCountryOverrideRegion2], 'Commodity')
						-- using region2
						WHEN fundId in (48, 60) and isNull(btd.region2,'')='' THEN  'Other'
						WHEN fundId in (48, 60) THEN btd.region2
						
						-- Other funds
						-- If it's Commodity and will stay Commodity  ->  'Commodity'
						WHEN d.FIG_Asset_Class = 'Commodity'  AND left(s.strategy, 2) NOT IN ('K-', 'E-', 'R-', 'X-')	THEN 'Commodity'
						-- If it will become Commodity   ->   Commodity
						WHEN left(s.strategy, 2) = 'C-' THEN 'Commodity'
						WHEN isNull(btd.region1, '') = '' THEN 'Other'
						ELSE btd.region1
						END)
						
		into #tmp
		from staging.country_codes d 
		JOIN #legalentity l ON l.le_name = d.Legal_Entity_Name COLLATE Latin1_General_BIN2
		JOIN [Forensics].dbo.strategy s ON s.strategy = d.Acct COLLATE SQL_Latin1_General_CP1_CI_AS
		--LEFT JOIN #country c ON c.tid = d.Value10
		left join #bus_tags_detail btd on btd.iso2=d.country_code COLLATE Latin1_General_BIN2
		LEFT JOIN #all_trader_tags_detail tag ON tag.aexeo_trader_id=case when fundId=48 then 1 when fundId=38 then 4 else 4 end AND upper(tag.strategy) collate SQL_Latin1_General_CP1_CI_AS=CASE WHEN d.Acct='EMPTY' THEN '' ELSE d.Acct END
		where Value10 > 0 
		group by fundId, Value10, strategy_id
		
		--print '5 ' + CONVERT(varchar, SYSDATETIME(), 121)
		
		DELETE dbo.fig_instrument_group
		
		INSERT dbo.fig_instrument_group (fundId, Id, strategy_id, FIG_Asset_Class, FIG_Sector, FIG_Region, FIG_RegionEx)
		select fundId, Id, strategy_id, FIG_Asset_Class, FIG_Sector, FIG_Region, FIG_RegionEx from #tmp

		-- Update instrument table risk country id based on staging.country_codes
		update i
		set i.[risk_country_id] = c.[country_id]
		from [dbo].[instrument] AS i
		join [staging].[country_codes] AS id on id.[Value10] = i.[Id]
		join MPS.[y_assets].[dbo].[country] c on c.[iso_code] = id.[country_code] collate SQL_Latin1_General_CP1_CI_AS;
		
		--print '6 ' + CONVERT(varchar, SYSDATETIME(), 121)
		
		drop table #legalentity
		drop table #all_trader_tags_detail
		drop table #bus_tags_detail
		drop table #tmp

	END TRY
	BEGIN CATCH
		INSERT INTO [staging].[scheduler_procerrors] 
			OUTPUT INSERTED.[Identifier], INSERTED.[Description], INSERTED.[Time] INTO [staging].[scheduler_procerrors_detail]
		VALUES('[staging].[PopulateFIGInstrumentGroup]', 'Proc Run Failed. ' + COALESCE(ERROR_MESSAGE(), ''), GetDate());
	END CATCH

		--print '7 ' + CONVERT(varchar, SYSDATETIME(), 121)


END
