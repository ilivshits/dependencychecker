create view [report].[EngineStuckOrders] as
SELECT [Order Id] = o.orderId
 , [Manager] = m.description
 , [Security] = s.description
 , [Clear Broker] = b.code
 , [Strategy] = t.code
 , [Engine] = f.code
FROM [Order] o
 INNER JOIN [Security] s ON o.sid = s.sid
 INNER JOIN [Manager] m ON o.managerId = m.managerId
 INNER JOIN [OrderAllocation] a ON o.orderId = a.orderId
 INNER JOIN [Fund] f ON a.fundId = f.fundId
 INNER JOIN [Broker] b ON a.clearBrokerId = b.brokerId
 INNER JOIN [Strategy] t ON a.strategyId = t.strategyId
WHERE f.fundId > 1000 AND o.orderStatusId IN (1, 2) AND
	DATEDIFF(MINUTE, o.lastUpdate, GETDATE()) > 10


