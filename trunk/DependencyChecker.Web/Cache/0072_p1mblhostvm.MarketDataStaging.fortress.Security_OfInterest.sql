

CREATE FUNCTION [fortress].[Security_OfInterest]
(	
	@includeMatchedOn date
)
RETURNS TABLE 
AS
RETURN 
(
	--- Set up securities as well as chain of underliers.	
	with ExpandedSecurities as
	(
		select
			b.sid as baseSid, s.sid, s.securityPrimaryTypeId, s.underlying_sid
		from
			FTBTrade.dbo_Security as b with (nolock)
				cross apply (
					--- Get the original sid.
					select 
						b.sid
					union
					--- Credit underlier.
					select
						cu.obligationSid as sid
					from
						FTBTrade.dbo_CreditUnderlying as cu with (nolock)
					where
						cu.sid = b.sid and
						cu.obligationSid != 0
					--- ADR if needed.
				) as r
					inner join FTBTrade.dbo_Security as s with (nolock) on
						r.sid = s.sid
	), SecuritiesOfInterest as
	(
		--- Base case, anything in position.
		select distinct
			s.baseSid, s.sid, s.underlying_sid, s.securityPrimaryTypeId
		from
			ExpandedSecurities as s
				inner join FTBTrade.dbo_TradeMatch as tm with (nolock) on
					tm.sid = s.baseSid
		where
			@includeMatchedOn is not null and 
			(
				tm.tradeMatchId = 0 or
				(@includeMatchedOn <= tm.matchTime and tm.matchTime < dateadd(d, 1, @includeMatchedOn))
			)
		union all
		select
			s.baseSid, s.sid, s.underlying_sid, s.securityPrimaryTypeId
		from
			SecuritiesOfInterest as soi
				inner join ExpandedSecurities as s with (nolock) on
					s.baseSid = soi.underlying_sid
		where
			soi.underlying_sid != 0
	)
	select distinct
		soi.sid, soi.securityPrimaryTypeId, soi.underlying_sid
	from
		SecuritiesOfInterest as soi
)


