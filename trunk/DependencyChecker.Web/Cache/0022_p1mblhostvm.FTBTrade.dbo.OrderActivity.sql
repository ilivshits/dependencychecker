CREATE view [OrderActivity] as

select o.orderId, a.allocationId, otcEventId = 0, o.managerId, o.execBrokerId,
	booked = CAST(case when o.orderStatusId in (1, 2) then 1 else 0 end as bit)
from [Order] o
inner join OrderAllocation a on o.orderId = a.orderId

union all

select o.orderId, e.allocationId, otcEventId, o.managerId, o.execBrokerId,
	booked = CAST(case when o.orderStatusId in (1, 2) then 1 else 0 end as bit)
from [Order] o
inner join OrderOTCEvent e on o.orderId = e.orderId

