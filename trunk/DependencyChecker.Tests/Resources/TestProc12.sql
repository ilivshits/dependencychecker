﻿CREATE proc [ma].[GetMngdAcctMacroPnlPermal]
@asofdate datetime = null,
@debug int = 0
as
BEGIN


--drop table #tbl
--drop table #reftbl
--drop table #captbl
--drop table #tmpTrader

--declare @asofdate datetime
--declare @debug int

--set @asofdate = '20121015'
--set @debug = 1
	
	set nocount on;

	if (@asofdate is null) set @asofdate = dbo.LastBusinessDay();
	declare @dateStr varchar(10);
	declare @qry varchar(max);
	set @dateStr = Convert(varchar(10), @asofdate, 126);

	--******************************************************************************
	create table #captbl
	(
		trader varchar(20),
		mainfund varchar(10),
		fund_id varchar(10),
		label varchar(20),
		capital float,
		TradingCapital float
	)
	declare @tmpFund varchar(20)
	--select @tmpFund = 'FAMF-Permal'
	select @tmpFund = 'Macro-Asia'
	
	insert into #captbl(trader,mainfund,fund_id, label, capital,TradingCapital) 
		exec [ma].[GetCapitalAllocationPermal]  @Fund = @tmpFund,@date = @asofdate


	-- ********************************** ********************************************		
	

	declare @fundStr varchar(max);
	select @fundStr = COALESCE(@fundStr + ',', '') + '[Funds].[Fund Id].&[' + ltrim(str(fund_id)) + ']' from #captbl
	where fund_id in (48,60);

	declare @legalStr varchar(max);
	select @legalStr = COALESCE(@legalStr + ',', '') + '[Legal Entity].[Legal Entity].&[' + FundAbbrev + ']' from 
	[Booking_AllocationFundGroup] where MetaFundId = 48;


 --   DECLARE @allTraderStr varchar(max);
	--SELECT @allTraderStr = COALESCE(@allTraderStr + ',', '') + '[Original Traders].[Aexeo Trader Name].&[' + rtrim(name) + ']' FROM [foffice].[v_trader]

	select * into #tmpTrader from [foffice].[v_trader]
	
	---- Just a hard coded to take out minis from pnl when date is after May
	--if @asofdate > '05/30/2011'
	--	delete from #tmpTrader where name not in ('Capital Markets','Macro G-10','Adam Levinson')

	
	DECLARE @traderStr varchar(max);
	SELECT @traderStr = COALESCE(@traderStr + ',', '') + '[Original Traders].[Aexeo Trader Name].&[' + Trader + ']' FROM [Booking_ReportCapitalAllocationsView] acd 
	JOIN  #tmpTrader VT On VT.name = acd.Trader	
	WHERE acd.StrategyFilter = '__DEFAULT__' 
	AND acd.ReportEntity IN ('>FT_PERM')
	
	--select top 10 * from [Booking_ReportCapitalAllocationsView] where ReportEntity IN ('>FT_PERM')
	
	--select top 100 * from [Booking_ReportCapitalAllocationsView] where ReportEntity IN ('>FT_MANM')
	
	--select * from Booking_AllocationTrader
	

	create table #tbl
	(
		fund_id varchar(10),
		daily_pnl float,
		wtd_pnl float,
		mtd_pnl float,
		ytd_pnl float,
		daily_bps float,
		wtd_bps float,
		mtd_bps float,
		ref_daily float,
		ref_wtd float,
		ref_mtd float,
		ref_daily_bps float,
		ref_wtd_bps float,
		ref_mtd_bps float,
		ytd_bps_actual float
	);


	set @qry = 'insert into #tbl(fund_id, daily_pnl, wtd_pnl, mtd_pnl, ytd_pnl, ytd_bps_actual)
	select * from openquery(CUBE1_OLAP, ''select nonempty({' +@fundStr+ '}, [Measures].[YTD_Real]) on 1,
	{[Measures].[Aexeo Pnl], [Measures].[WTD_REAL], [Measures].[MTD_REAL], [Measures].[YTD_REAL], [Measures].[YTD_BPS]} on 0
	from [Pnl Data] where [Time].[Year -  Quarter -  Month -  Date].[Date].&[' + @dateStr + 'T00:00:00]*{' + @traderStr + '}'')';
	--set @qry = 'insert into #tbl(fund_id, daily_pnl, wtd_pnl, mtd_pnl, ytd_pnl)
	--select * from openquery(CUBE1_OLAP, ''select nonempty({' +@fundStr+ '}, [Measures].[YTD_Real]) on 1,
	--{[Measures].[Aexeo Pnl], [Measures].[WTD_REAL], [Measures].[MTD_REAL], [Measures].[YTD_REAL]} on 0
	--from [Pnl Data] where [Time].[Year -  Quarter -  Month -  Date].[Date].&[' + @dateStr + 'T00:00:00]*{' +@allTraderStr+ '}'')';
	begin try
		exec (@qry);
	end try
	begin catch
		print 'error in cube query'
	end catch
	print @qry
	if @debug = 1
	BEGIN
		print @qry
		select * from #tbl
	END
	
	--select * from #tbl
	
	update #tbl 
	set daily_bps = daily_pnl*10000/TradingCapital, wtd_bps = wtd_pnl*10000/TradingCapital, mtd_bps = mtd_pnl*10000/TradingCapital
	from #tbl t join #captbl c on c.fund_id = t.fund_id
	
	--select * from #tbl
	--select * from #captbl


	declare @ref_daily float, @ref_wtd float, @ref_mtd float, @ref_daily_bps float, @ref_wtd_bps float, @ref_mtd_bps float;

	create table #reftbl
	(
		fund_id varchar(10),
		label varchar(20),
		daily_pnl float,
		wtd_pnl float,
		mtd_pnl float--,
	--	daily_bps float,
	--	wtd_bps float,
	--	mtd_bps float
	);

	declare @stratStr varchar(max);
	select @stratStr = COALESCE(@stratStr + ',', '') + '[Strategy].[Strategy].&[' + ltrim(str(strategy_id)) + ']' from v_MngdAcctExcludeStrategy;
	PRINT(@stratStr)

	declare @tidStr varchar(max);
	select @tidStr = COALESCE(@tidStr + ',', '') + '[Instrument].[Id].&[' +ltrim(str(ExcludedTid))+ ']' from v_MngdAcctExcludeTid;
	PRINT(@tidStr)

	set @qry = 'insert into #reftbl(fund_id, daily_pnl, wtd_pnl, mtd_pnl)
	select * from openquery(CUBE1_OLAP, ''select nonempty([Funds].[Fund Id].[Fund Id].members, [Measures].[MTD_Real]) on 1,
	{[Measures].[Aexeo Pnl], [Measures].[WTD_REAL], [Measures].[MTD_REAL]} on 0
	from (select Except([Strategy].[Strategy].[Strategy].Members,{' +@stratStr+ '})*Except([Instrument].[Id].[Id].Members,{' +@tidStr+ '}) on 0 from [Pnl Data])
	where [Time].[Year -  Quarter -  Month -  Date].[Date].&[' +@dateStr+ 'T00:00:00]*{' +@legalStr+ '}*{' +@traderStr+ '}'')';
	
	begin try
		exec (@qry);
	end try
	begin catch
		print '2. error in query'
	end catch
	PRINT(@qry);

	UPDATE #reftbl SET daily_pnl = daily_pnl - IsNull(dbo.fn_GetCostOfCarryFOrMngdMacroAsia(@asofdate, 'D'), 0),
						wtd_pnl = wtd_pnl - IsNull(dbo.fn_GetCostOfCarryFOrMngdMacroAsia(@asofdate, 'W'), 0),
						mtd_pnl = mtd_pnl - IsNull(dbo.fn_GetCostOfCarryFOrMngdMacroAsia(@asofdate, 'M'), 0);

	select  @ref_daily  = daily_pnl/capital, @ref_wtd = wtd_pnl/capital, @ref_mtd = mtd_pnl/capital, @ref_daily_bps = daily_pnl*10000/capital, @ref_wtd_bps = wtd_pnl*10000/capital, @ref_mtd_bps = mtd_pnl*10000/capital
	from #reftbl r join #captbl c on r.fund_id = c.fund_id where r.fund_id = 48

	update #tbl set ref_daily_bps = daily_bps , ref_wtd_bps = wtd_bps ,	ref_mtd_bps = mtd_bps
	where fund_id = 48


	update #tbl set ref_daily = @ref_daily*capital, ref_wtd = @ref_wtd*capital, ref_mtd = @ref_mtd*capital
	from #tbl t join #captbl c on t.fund_id = c.fund_id where t.fund_id not in (48)

	update #tbl set ref_daily_bps = ref_daily/TradingCapital *10000  , ref_wtd_bps = ref_wtd/TradingCapital *10000 , ref_mtd_bps = ref_mtd/TradingCapital *10000
	from #tbl t join #captbl c on t.fund_id = c.fund_id where t.fund_id not in (48)
	
	





	if @debug = 1
	BEGIN
		SELECT 'REF_FUND',* FROM #reftbl
		SELECT 'FUND',* FROM #tbl
		SELECT 'CAPITAL',* FROM #captbl
	END
	
	
	
	update #captbl set label = 'GAM' where fund_id = '46'
	update #captbl set label = 'Man' where fund_id = '41'
	UPDATE #captbl SET label = 'Macro' WHERE fund_id = '38'
	UPDATE #captbl SET label = 'Macro Asia' WHERE fund_id = '48'
	UPDATE #captbl SET label = 'GSMA' WHERE fund_id = '49'
	UPDATE #captbl SET label = 'PERM' WHERE fund_id = '60'

	select t.fund_id, c.label, c.capital, t.daily_pnl, t.wtd_pnl, t.mtd_pnl, t.ytd_pnl, 
	t.daily_bps AS daily_bps, 
	t.wtd_bps AS wtd_bps, 
	t.mtd_bps AS mtd_bps,
	t.ytd_bps_actual,
	t.ref_daily, t.ref_wtd, t.ref_mtd, 
	t.ref_daily_bps AS ref_daily_bps, 
	t.ref_wtd_bps AS ref_wtd_bps, 
	t.ref_mtd_bps AS ref_mtd_bps
	 from #tbl t join #captbl c on t.fund_id = c.fund_id order by c.fund_id asc
	 
	drop table #tbl
	drop table #reftbl
	drop table #captbl
	drop table #tmpTrader

END