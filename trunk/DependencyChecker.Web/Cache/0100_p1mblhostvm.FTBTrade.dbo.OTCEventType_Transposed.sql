CREATE VIEW [dbo].[OTCEventType_Transposed] AS
SELECT
[Swap Reset] = 1
, [Option Expiry] = 2
, [Swap Termination] = 3
, [Swap Novation] = 4
, [Option Exercise] = 5
, [Credit Default Event] = 6
, [Transfer] = 7


