﻿CREATE TABLE [log].[ExceptionLog] (
    [Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
	[BatchId] INT not null,
    [Date] datetime2 NOT NULL,
    [Level] varchar (50) NOT NULL,
    [Logger] varchar (255) NOT NULL,
    [Message] varchar (4000) NOT NULL,
    [Exception] varchar (2000) NULL
)
