CREATE VIEW [dbo].[CR_SecurityRestriction] AS
SELECT s.sid, security = s.description, bloomberg_unique = i.code, restriction = t.description, r.startDate, r.endDate, r.lastUpdate
FROM SecurityRestriction r
INNER JOIN [Security] s ON r.sid = s.sid
INNER JOIN RestrictionType t ON r.restrictionTypeId = t.restrictionTypeId
LEFT JOIN [SecurityIdentifier] i ON s.sid = i.sid AND i.identifierTypeId = 11
WHERE r.endDate >= GETDATE() AND r.startDate <= GETDATE() AND t.rtlApplicableFlag = 1


