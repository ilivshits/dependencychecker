﻿namespace DependencyChecker.Entities.Base
{
    public enum EntityErrorCode
    {
        InvalidDefinition = 1,
        MissingObjectInDefinition,
        ValidationError,
        AnalysisError,
        DependencyError,
        DependencyWarning,
        AccessError,
        Info,
        ParsingError,
        Unexpected
    }
}