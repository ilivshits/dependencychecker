
CREATE view [dbo].[PricingConfiguration] as
select d.sid
	, eodTicker = coalesce(s.eodTicker, d.eodTicker)
	, eodTickerLevelId = case
		when s.eodTicker is not null then 3
		else d.eodTickerLevelId end
	
	, liveTicker = coalesce(s.liveTicker, d.liveTicker)
	, liveTickerLevelId = case
		when s.liveTicker is not null then 3
		else d.liveTickerLevelId end

from dbo.PricingDefault d
left join PricingOverride s on s.sid = d.sid


