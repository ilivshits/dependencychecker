
create view [fortress].[v_CreditTier_Pivot] as
	select
		0 as NotSpecified,
		1 as JuniorSubordinated,
		2 as Tier1Capital,
		3 as SecuredOrDomestic,
		4 as SeniorUnsecured,
		5 as Subordinated


