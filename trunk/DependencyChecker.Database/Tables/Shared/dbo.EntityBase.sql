﻿CREATE TABLE [dbo].[EntityBase]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	[Parent]   UNIQUEIDENTIFIER            CONSTRAINT [FK_Entity_ParentEntity] FOREIGN KEY REFERENCES [dbo].[EntityBase] ([Id]) NULL,
    [Name]       NVARCHAR (500) NOT NULL,
	[FullName]       NVARCHAR (500) NULL,
    [Created]     DATETIME2       NULL,
    [LastModified] DATETIME2       NULL, 
    [Definition] NVARCHAR(MAX) NULL, 
    [Root] UNIQUEIDENTIFIER CONSTRAINT [FK_Entity_RootIdEntity] FOREIGN KEY REFERENCES [dbo].[EntityBase] ([Id]) NULL, 
    [Type] INT CONSTRAINT [FK_Entity_TypeId] FOREIGN KEY REFERENCES [dbo].[EntityType] ([Id]) NOT NULL,    
	[CreatedBy] UNIQUEIDENTIFIER CONSTRAINT [FK_Entity_CreatedById] FOREIGN KEY REFERENCES [dbo].[PersonEntity] ([Id]) NULL,    
	[LastModifiedBy] UNIQUEIDENTIFIER CONSTRAINT [FK_Entity_ModifiedById] FOREIGN KEY REFERENCES [dbo].[PersonEntity] ([Id]) NULL, 
    [InternalId] NVARCHAR(100) NULL, 
	[Level] INT NOT NULL,
    [Provider] NVARCHAR(50) NOT NULL 
)
