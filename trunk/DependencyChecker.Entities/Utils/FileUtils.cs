﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DependencyChecker.Entities.Utils
{
    public static class FileUtils
    {
        public static String BuildRemotePath(string server, string filename)
        {
            return "\\\\" + server + "\\" + filename.Replace(":\\", "$\\");
        }
    }
}
