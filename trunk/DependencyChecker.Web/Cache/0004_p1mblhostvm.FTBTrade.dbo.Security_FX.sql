

--- Create the view.
CREATE view [dbo].[Security_FX] as
select
	s.*
from
	Security as s
		cross join SecurityPrimaryType_Transposed as spt
where
	s.securityPrimaryTypeId in (spt.FX, spt.[Metal Forward])



