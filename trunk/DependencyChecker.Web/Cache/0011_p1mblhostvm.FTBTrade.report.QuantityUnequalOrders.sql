CREATE VIEW [report].[QuantityUnequalOrders] AS
SELECT 
      [Order Id] = o.orderId, 
      [Manager] = m.description,
      [Security] = s.description 
FROM [Order] o
INNER JOIN [Manager] m WITH(NOLOCK) ON o.managerId = m.managerId
INNER JOIN [Security] s WITH(NOLOCK) ON o.sid = s.sid
LEFT JOIN
(
      SELECT orderId, quantity = ROUND(SUM(quantity), 2) FROM OrderFill GROUP BY orderId
) f on o.orderId = f.orderId
LEFT JOIN
(
      SELECT orderId, quantity = ROUND(SUM(quantity), 2) FROM OrderAllocation GROUP BY orderId
) a ON o.orderId = a.orderId
WHERE tradeDate > dateadd(day, -7, getdate()) AND orderStatusId IN (1, 2)
      AND (f.orderId IS NULL OR a.orderId IS NULL OR f.quantity != a.quantity)


