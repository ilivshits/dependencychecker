
create view [fortress].[v_Security_Currencies] as
	select
		s.*
	from
		FTBTrade.dbo_Security as s with (nolock)
			cross join fortress.v_SecurityPrimaryType_Pivot as spt
			cross join fortress.v_SecuritySubType_Pivot as sst
	where
		s.securityPrimaryTypeId = spt.Currency and
		s.securitySubTypeId = sst.None


