CREATE view [dbo].[Security_Currency] as
select
	s.sid, s.description
from
	Security as s
		cross join SecurityPrimaryType_Transposed as spt
where
	s.securityPrimaryTypeId in (spt.Cash)


