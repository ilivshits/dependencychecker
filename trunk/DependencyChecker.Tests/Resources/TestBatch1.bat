﻿***********************************************
@ECHO OFF
REM **************************************
REM Run stored proc to create Consensys position file (Intraday)
REM Ftp Position file to server
REM 
REM **************************************

SET RUNDATE=%date:~10,4%%date:~4,2%%date:~7,2%
SET RUNTIME=%time:~0,1%
IF "%RUNTIME%" == " " SET RUNTIME=0
SET RUNTIME=%RUNTIME%%time:~1,1%%time:~3,2%
ECHO ********************************************************************************************************
ECHO Run at: %date% %time%

SET DB_SERVER=NY-GMITFTBDB1
rem SET DB_SERVER=NJ-GMITFTBDB-DE

SET ROOT_DIR=Y:\Consensys
SET BIN_DIR=%ROOT_DIR%\bin
SET DATA_DIR=%ROOT_DIR%\data

SET POSITION_FILE=%DATA_DIR%\ConsensysPosition%1.txt
SET POSITION_FILE_WITHDATE=%DATA_DIR%\ConsensysPosition%1_%RUNDATE%_%RUNTIME%.txt
SET FTP_CMD_FILE=%BIN_DIR%\ConsensysFtpCommand%1.txt
SET HEADER_FILE=%BIN_DIR%\ConsensysHeaderFile.txt

if exist %POSITION_FILE_WITHDATE% DEL %POSITION_FILE_WITHDATE%
if exist %POSITION_FILE% DEL %POSITION_FILE%

echo FileName: %POSITION_FILE_WITHDATE%
echo Bcping....
bcp "exec P1ReportingG2.compliance.CreatePositionFileForConsensys_P1TC" queryout %POSITION_FILE_WITHDATE% -S%DB_SERVER% -T -c -t"|" -C ACP
IF ERRORLEVEL 1 (
	echo Failed to create position file
	sqlcmd -S %DB_SERVER% -i %BIN_DIR%\SendMailCmdError.sql
	goto :end
)

SET DB_SERVER=NY-GMITFTBTEST2
rem SET DB_SERVER=NJ-GMITFTBFAKE1

bcp "exec P1ReportingG2.compliance.CreatePositionFileForConsensys_TEST2" queryout %POSITION_FILE_WITHDATE% -S%DB_SERVER% -T -c -t"|" -C ACP

if exist %POSITION_FILE_WITHDATE% (
	type %HEADER_FILE% >%POSITION_FILE% 
	type %POSITION_FILE_WITHDATE% >>%POSITION_FILE%
	type %POSITION_FILE% >%POSITION_FILE_WITHDATE%
	echo %POSITION_FILE% created successfuly
)

if exist %POSITION_FILE% (
	echo Running ftp command...
	FTP -s:%FTP_CMD_FILE%
	echo Sending email notification...
	sqlcmd -S %DB_SERVER% -i %BIN_DIR%\SendMailCmd.sql
)

:end
