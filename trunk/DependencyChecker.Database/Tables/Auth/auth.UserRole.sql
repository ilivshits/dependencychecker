﻿CREATE TABLE [auth].[UserRole]
(
	[UselessId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [User] SMALLINT NOT NULL, 
    [Role] SMALLINT NOT NULL,
    CONSTRAINT [FK_UserRole_User] FOREIGN KEY ([User]) REFERENCES [auth].[User]([Id]), 
    CONSTRAINT [FK_UserRole_Role] FOREIGN KEY ([Role]) REFERENCES [auth].[Role]([Id]), 
)
