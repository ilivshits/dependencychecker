﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.XPath;
using DependencyChecker.Common;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Utils;

namespace DependencyChecker.Entities
{
    public class DTSEntity : EntityBase
    {
        public static readonly string ProviderName = EntityProviderType.Perl.ToString();
        private static readonly XNamespace SqlTaskNamespace = "www.microsoft.com/sqlserver/dts/tasks/sqltask";

        public DTSEntity(string name) : this(name, String.Empty)
        {
        }
        public DTSEntity(string name, string definition) : this(name, definition, ProviderName)
        {
        }
        protected DTSEntity(string name, string definition, string providerName)
            : base(name, definition, providerName, EntityType.DTSX)
        {
            _searchNames = new List<string>
            {
                Name,
                Path.GetFileName(Name)
            }; 
        }

        public override string ScopeDelimiter
        {
            get
            {
                return "/";
            }
        }

        #region Overrides of EntityBase

        public override string InternalId { get; set; }

        public override string FullName
        {
            get { return FileUtils.BuildRemotePath(Root.Name, Name); }
        }

        #endregion

        private readonly List<string> _searchNames;
        public override List<string> SearchNames
        {
            get { return _searchNames; }
        }

        protected override List<SearchContent> GetSearchContent()
        {
            var result = new List<SearchContent>();
            if (String.IsNullOrEmpty(Definition))
            {
                return result;
            }
            try
            {
                var definitionDoc = XDocument.Parse(Definition);
                XNamespace ns = Regex.Match(Definition, @"<DTS:Executable xmlns:DTS=""(?<ns>[^""]+)""").Groups["ns"].Value;
                if (definitionDoc.Root != null)
                {
                    ns = definitionDoc.Root.Name.Namespace.NamespaceName;
                }
                var sqlNs = SqlTaskNamespace;
                var connections = definitionDoc.Descendants(ns + "ConnectionManager");

                var serversAdded = new List<String>();

                foreach (var connection in connections)
                {
                    var server = Regex.Match(connection.Value, @"Data Source=(?<value>[^;]+)(;|$)");
                    var database = Regex.Match(connection.Value, @"Initial Catalog=(?<value>[^;]+)(;|$)");

                    if (server.Success && database.Success && server.Groups["value"].Success &&
                        database.Groups["value"].Success)
                    {
                        var serverName = String.Format("[{0}].[{1}]", server.Groups["value"].Value,
                            database.Groups["value"].Value);

                        if (serversAdded.Contains(serverName)) continue;

                        serversAdded.Add(serverName);

                        var idElement = connection.Descendants(ns + "Property")
                            .SingleOrDefault(e => e.Attribute(ns + "Name").Value == "DTSID");
                        if (idElement != null)
                        {
                            var connectionId = idElement.Value;
                            result.AddRange(definitionDoc.Descendants(sqlNs + "SqlTaskData").Where(e => e.Attribute(sqlNs + "Connection").Value == connectionId)
                                .ToList()
                                .Select(c => new SearchContent
                                {
                                    ContentType = EntityProviderType.SQL,
                                    ScopeName = serverName,
                                    Text = c.Attribute(sqlNs + "SqlStatementSource").Value
                                }).ToList());

                            foreach (var component in definitionDoc.Descendants("component").Where(c => c.Descendants("connection").Any(con => con.Attribute("connectionManagerID").Value == connectionId)))
                            {
                                var content = component.Descendants("property").SingleOrDefault(p => p.Attribute("name").Value == "OpenRowset");
                                if (content != null && !String.IsNullOrEmpty(content.Value))
                                {
                                    result.Add(new SearchContent
                                    {
                                        ContentType = EntityProviderType.SQL,
                                        ScopeName = serverName,
                                        Text = content.Value
                                    });
                                }
                                content = component.Descendants("property").SingleOrDefault(p => p.Attribute("name").Value == "SqlCommand");
                                if (content != null && !String.IsNullOrEmpty(content.Value))
                                {
                                    result.Add(new SearchContent
                                    {
                                        ContentType = EntityProviderType.SQL,
                                        ScopeName = serverName,
                                        Text = content.Value
                                    });
                                }
                            }
                        
                        }

                        var serverEntity = new SearchContent
                        {
                            ContentType = EntityProviderType.SQL,
                            Text = serverName
                        };

                        result.Add(serverEntity);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Log.WarnFormat("Cannot parse XML content for entity '{0}' ({1}). Error: {3}. XML:\r\n{2}", FullName, TypeName, Definition, ex.Message);
            }

            return result;
        }
    }
}