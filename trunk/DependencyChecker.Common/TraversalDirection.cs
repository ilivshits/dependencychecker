﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DependencyChecker.WCF
{
    public enum TraversalDirection
    {
        Depending,
        UsedBy
    }
}
