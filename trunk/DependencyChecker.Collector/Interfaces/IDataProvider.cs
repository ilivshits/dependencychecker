﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Events;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Collector.Interfaces
{
    public interface IDataProvider
    {
        String Name { get; }
        List<IEntity> Search();
        List<EntityMatch> MatchItems(string input, IEntity entity);
        bool IsKeyword(String text);

        event EventHandler<DependencyCheckerProgressEventArgs> Progress;
    }
}
