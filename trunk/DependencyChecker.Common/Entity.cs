﻿using System;
using System.Collections.Generic;

namespace DependencyChecker.Common
{
    public class Entity
    {
        public Guid Id { get; set; }
        public String Name { get; set; }
        public String Type { get; set; }
        public String Provider { get; set; }
        public int Level { get; set; }

        public String Server { get; set; }
        public String Database { get; set; }

        public List<Entity> DependsOn { get; set; }
        public List<Entity> UsedBy { get; set; }
        public String[] Errors { get; set; }
        public String CreatedBy { get; set; }
        public String LastModifiedBy { get; set; }
        public String Created { get; set; }
        public String LastModified { get; set; }
        public String LastRun { get; set; }
    }
}
