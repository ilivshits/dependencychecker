CREATE function [tagging].[GetNoVariant](@value sql_variant)
returns table as return
(
	select value_Int = case when sql_variant_property(@value, 'BaseType')
		in ('bit', 'tinyint', 'smallint', 'int', 'bigint') then cast(@value as bigint)
		else null end

	, value_Text = case when sql_variant_property(@value, 'BaseType')
		in ('char', 'varchar', 'char', 'nvarchar', 'xml') then cast(@value as nvarchar(max))
		else null end

	, value_DateTime = case when sql_variant_property(@value, 'BaseType')
		in ('date', 'datetime', 'datetime2', 'datetimeoffset') then cast(@value as datetime)
		else null end

	, value_Decimal = case when sql_variant_property(@value, 'BaseType')
		in ('decimal', 'numeric') then cast(@value as decimal(28,14))
		else null end
)


