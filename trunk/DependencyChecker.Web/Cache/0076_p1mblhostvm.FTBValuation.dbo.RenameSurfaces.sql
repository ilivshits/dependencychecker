
CREATE PROCEDURE RenameSurfaces
	@namesXml xml
AS
BEGIN
	SET NOCOUNT ON;

	create table #names (oldCode varchar(50), newCode varchar(50))

	insert into #names 
	select nameRows.n.value('oldCode[1]', 'varchar(50)'), nameRows.n.value('newCode[1]', 'varchar(50)')
	from @namesXml.nodes('//row') AS nameRows(n)
    
	UPDATE p
	SET code = r.newCode
	FROM dbo.SurfacePublishImagine p
	INNER JOIN #names r ON r.oldCode collate Latin1_General_BIN2 = p.code collate Latin1_General_BIN2

	drop table #names
END

