﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.DirectoryServices;
using System.IO;
using System.Linq;
using System.Management.Instrumentation;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DependencyChecker.Collector.Base;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Common;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Events;
using DependencyChecker.Entities.Interfaces;
using DependencyChecker.Entities.Utils;
using Microsoft.AnalysisServices;
using Microsoft.SqlServer.Dts.Runtime;
using Microsoft.Win32;
using Microsoft.Win32.TaskScheduler;


namespace DependencyChecker.Collector
{
    public class P1TCDataProvider : BaseDataProvider
    {
        private static readonly List<Regex> BasicPattern = new List<Regex>
        {
            new Regex(@"({[A-Z]{1,2}Tag\: (?<name>[a-zA-Z0-9_\- \&\$%\\\/\(\)\.,\:\[\]]+) (\[Start\]|\[End\])}|" + 
                      @"{[A-Z]{1,2}Tag\: (?<name>[a-zA-Z0-9_\- \&\$%\\\/\(\)\.,\:\[\]]+)}|" + 
                      @"[ =\-+\*\|\&\(,]+(?<name>[a-zA-Z0-9_]+)\(|" + 
                       @"{[a-zA-Z0-9_\- \&\$%\\\/\(\)\.,\:\[\]]+\.Tag\[(?<name>[a-zA-Z0-9_\- \&\$%\\\/\(\)\.,\:\[\]]+)(\[start\]|\[end\]|\-start|\-end)?\]}|" + 
                       @"{(?<name>[a-zA-Z0-9_\- \&\$%\\\/\(\)\.,\:\[\]]+)})", RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.Compiled)
        };

        private float _totalProgress;
        private float _serverWeight;

        public P1TCDataProvider(IServerProvider serverProvider)
            : base(serverProvider)
        {
            _totalProgress = 0;
        }

        #region Overrides of BaseDataProvider

        public override string Name
        {
            get { return P1TCEntity.ProviderName; }
        }

        protected override List<Regex> ItemSearchPattern
        {
            get { return BasicPattern; }
        }

        public override List<IEntity> Search()
        {
            var instancesList = new List<IEntity>();
            var servers = ServerProvider.GetServers(EntityProviderType.P1TC).ToList();
            _serverWeight = 1.0f / servers.Count;

            var result = servers.SelectMany(s => LoadServer(s, instancesList)).Where(s  => s != null).ToList();
            return result;
        }

        #endregion

        public static List<IEntity> LoadEntityChildren(string connectionString, IEntity entity = null, Dictionary<string, P1TCEntity> allNames = null,  int gen = 0)
        {
            if (allNames == null)
            {
                allNames = new Dictionary<string, P1TCEntity>();
            }

            var result = new List<IEntity>();
            if (gen > 10)
            {
                return result;
            }
            var entitiesResult = new DataTable();
            var entityName = entity == null ? "" : entity.FullName;
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var command = new SqlCommand(String.Format(@"SELECT [Name], [Type] FROM [POneSqlClient].[dbo].[GetEntityStructure] ('{0}')", 
                        entityName), connection)
                    {
                        CommandType = CommandType.Text
                    })
                    {
                        LoggerHelper.Log.Debug("Loading: " + entityName);
                        connection.Open();

                        var da = new SqlDataAdapter(command);
                        da.Fill(entitiesResult);

                        connection.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Failure. Details: " + e);
            }

            foreach (DataRow row in entitiesResult.Rows)
            {
                var type = row["Type"].ToString() == "None" ? EntityType.P1TCEntity : EntityType.P1TCColumn;
                var name = row["Name"].ToString();

                var newEntity = new P1TCEntity(name, type);
                if (type == EntityType.P1TCEntity)
                {
                    lock (allNames)
                    {
                        if (allNames.ContainsKey(name))
                        {
                            allNames[name].Prefixes.Add(entityName + "." + name);
                            continue;
                        }

                        allNames.Add(name, newEntity);
                    }
                }
                else
                {
                    LoggerHelper.Log.DebugFormat("Adding column: {0}", name);
                }

                if (entity != null && (entity.Name == name || entity.FullName.Contains("." + name + ".") || entity.FullName.StartsWith(name + ".")))
                {
                    LoggerHelper.Log.DebugFormat("Circular reference detected for {0} in {1}. Skipping.", name, entity.FullName);
                    continue;
                }

                if (entity != null)
                {
                    newEntity.Scope = entity;
                }
                result.Add(newEntity);
            }

            Parallel.ForEach(result.Where(i => i.Type == EntityType.P1TCEntity), item =>
            {
                item.ScopeChildren = LoadEntityChildren(connectionString, item, allNames, gen + 1);
            });

            return result;
        }

        private List<IEntity> LoadDatabase(string connectionString, string mipsConnection)
        {
            var columnsResult = new DataTable();
            var mipsResult = new DataTable();
            var tagsResult = new DataTable();
            var viewsResult = new DataTable();
            var allResult = new DataTable();

            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var command = new SqlCommand(@"SELECT [description]
    ,[created]
    ,[lastUpdate]
    ,[application_source]
    ,[system_source]
    ,[content].value('(/Calculation/Expression/Script/node())[1]', 'nvarchar(MAX)') as [definition]
    ,[ownerId]
FROM [FTBTrade].[grid].[CalculationDefinition]
UNION 
SELECT 
      r.[reference]
      ,MAX(r.[created])
      ,MAX(r.[lastUpdate])
      ,'' as [application_source]
      ,'' as [system_source]
      ,STUFF((
				SELECT ', ' + rr.[path]
				FROM [FTBTrade].[grid].[KnownResource] rr
				WHERE (rr.[reference] = r.[reference]) 
				FOR XML PATH (''))
			  ,1,2,'') AS [path]
	  , 0
FROM [FTBTrade].[grid].[KnownResource] AS r
GROUP BY [reference]", connection)
                    {
                        CommandType = CommandType.Text
                    })
                    {
                        connection.Open();
                        var da = new SqlDataAdapter(command);
                        da.Fill(columnsResult);

                    }

                    using (var command = new SqlCommand(@"SELECT 
	'[dbo].[' + tableName + ']' as [table],
	columnName as [column], 
	startLabel as [name]
FROM [portfolio].[MipsResource]
WHERE startLabel IS NOT NULL
UNION
SELECT 
	'[dbo].[' + tableName + ']' as [table],
	columnName as [column], 
	endLabel as [name]
FROM [portfolio].[MipsResource]
WHERE endLabel IS NOT NULL", connection)
                    {
                        CommandType = CommandType.Text
                    })
                    {
                        var da = new SqlDataAdapter(command);
                        da.Fill(mipsResult);
                    }

                    using (var command = new SqlCommand(@"SELECT [description]
      ,[created]
      ,[lastUpdate]
      ,[application_source]
      ,[system_source]
FROM [tagging].[Tag]", connection)
                    {
                        CommandType = CommandType.Text
                    })
                    {
                        var da = new SqlDataAdapter(command);
                        da.Fill(tagsResult);
                    }

                    try
                    {
                        using (
                            var command =
                                new SqlCommand(
                                    @"SELECT DISTINCT [Name]
FROM (SELECT [Name] as [Name] FROM [POneSqlClient].[dbo].[GetPortfolioColumns] ()
WHERE [Name] NOT LIKE '%Tag: %'
UNION 
SELECT [Name] as [Name] FROM [POneSqlClient].[securities].[GetColumns] ()
WHERE [Name] NOT LIKE '%Tag: %'
UNION 
SELECT [Name] as [Name] FROM [POneSqlClient].[orders].[GetColumns] ()
WHERE [Name] NOT LIKE '%Tag: %') Columns",
                                    connection)
                                {
                                    CommandType = CommandType.Text
                                })
                        {
                            var da = new SqlDataAdapter(command);
                            da.Fill(allResult);
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerHelper.Log.WarnFormat("Cannot get all available columns. Details: {0}", ex);
                    }

                    using (var command = new SqlCommand(@"SELECT [description]
      ,[created]
      ,[lastUpdate]
      ,[application_source]
      ,[system_source]
      ,STUFF((SELECT '{' + C.col.value('@Id', 'NVARCHAR(MAX)') + '}, '
               FROM   [content].nodes('/Grid/*') C ( col )
			   WHERE C.col.value('@Id', 'NVARCHAR(MAX)') <> 'PreFilters'
               FOR XML PATH(''),TYPE).value('.', 'NVARCHAR(MAX)'),
              1, 0, '') + CASE WHEN [content].exist('/Grid/CustomData/PreFilter') = 1 THEN '
PreFilter: 
' + [content].value('(/Grid/CustomData/node())[1]', 'NVARCHAR(MAX)') ELSE '' END
		 AS [definition]
      ,[ownerId]
FROM [grid].[ViewDefinition]", connection)
                    {
                        CommandType = CommandType.Text
                    })
                    {
                        var da = new SqlDataAdapter(command);
                        da.Fill(viewsResult);

                        connection.Close();
                    }
                }
            }
            catch (Exception e)
            {
                LoggerHelper.Log.WarnFormat("Cannot access '{0}'. Details: {1}", connectionString, e);
            }

            /* Adding calculated columns */
            var result = (from DataRow row in columnsResult.Rows
                select
                    new P1TCEntity(row["description"] == DBNull.Value ? null : (string)row["description"],
                        row["definition"] == DBNull.Value ? null : (string)row["definition"],
                        EntityType.P1TCCalcCol)
                    {
                        Created = row["created"] == DBNull.Value ? DateTime.MinValue : (DateTime)row["created"],
                        LastModified = row["lastUpdate"] == DBNull.Value ? DateTime.MinValue : (DateTime)row["lastUpdate"]
                    }).Cast<IEntity>().ToList();

            var mips = new SqlConnectionStringBuilder(mipsConnection);

            /* Adding mips columns */
            result.AddRange((from DataRow row in mipsResult.Rows
                             select
                                 new P1TCEntity((string)row["name"],
                                     String.Format("SELECT {1} FROM [{2}].[{3}].{0}", row["table"], row["column"], 
                                                mips.DataSource,
                                                String.IsNullOrEmpty(mips.InitialCatalog) ? "Mips" : mips.InitialCatalog),
                                     EntityType.P1TCMipsCol)).Cast<IEntity>().ToList());

            /* Adding tags */
            result.AddRange((from DataRow row in tagsResult.Rows
                             select
                                 new P1TCEntity((string)row["description"], EntityType.P1TCTag)
                                 {
                                     Created = (DateTime)row["created"],
                                     LastModified = (DateTime)row["lastUpdate"]
                                 }).Cast<IEntity>().ToList());

            if (allResult.Rows.Count > 0)
            {
                /* Adding system columns (not any of above) */
                result.AddRange((from DataRow row in allResult.Rows
                    where !result.Exists(r => r.Name == (string) row["Name"])
                    select
                        new P1TCEntity((string) row["Name"], EntityType.P1TCColumn)).Cast<IEntity>().ToList());
            }

            /* Adding views */
            result.AddRange((from DataRow row in viewsResult.Rows
                             select
                                 new P1TCEntity((string)row["description"], row["definition"] == DBNull.Value ? String.Empty : (string)row["definition"],
                                     EntityType.P1TCView)
                                 {
                                     Created = (DateTime)row["created"],
                                     LastModified = (DateTime)row["lastUpdate"]
                                 }).Cast<IEntity>().ToList());

            return result;
        }

        public IEntity LoadDatabaseServer(string serverName = "", List<IEntity> instancesList = null, string database = "FTBTrade", string mipsServer = "", string mipsDatabase = "Mips")
        {
            if (instancesList == null)
            {
                instancesList = new List<IEntity>();
            }
            var server = new DatabaseEntity(serverName, EntityType.Server)
            {
                ScopeChildren = new List<IEntity>()
            };
            OnProgress(new DependencyCheckerProgressEventArgs
            {
                Message = "Loading server " + server.Name,
                Progress = _totalProgress
            });

            var connection = new SqlConnectionStringBuilder
            {
                DataSource = serverName,
                InitialCatalog = database,
                IntegratedSecurity = true
            };
            var clientConnection = new SqlConnectionStringBuilder
            {
                DataSource = serverName,
                InitialCatalog = "POneSqlClient",
                IntegratedSecurity = true
            };
            var mipsConnection = new SqlConnectionStringBuilder
            {
                DataSource = String.IsNullOrEmpty(mipsServer) ? serverName : mipsServer,
                InitialCatalog = mipsDatabase,
                IntegratedSecurity = true
            };
            if (instancesList.All(i => i.Name != serverName))
            {
                var newInstance = new P1TCEntity("FTBTrade",
                    connection.ConnectionString, EntityType.P1TCInstance)
                {
                    Scope = server,
                    ScopeChildren = LoadDatabase(connection.ConnectionString, mipsConnection.ConnectionString)
                };
                newInstance.ScopeChildren.AddRange(LoadEntityChildren(clientConnection.ConnectionString));

                newInstance.ScopeChildren.ForEach(i => i.Scope = newInstance);
                server.ScopeChildren.Add(newInstance);
                instancesList.Add(newInstance);
            }
            _totalProgress += _serverWeight;
            return server;
        }

        public List<IEntity> LoadServer(string serverName = "", List<IEntity> instancesList = null)
        {
            var result = new List<IEntity>();
            if (instancesList == null)
            {
                instancesList = new List<IEntity>();
            }
            var server = new P1TCEntity(serverName, EntityType.Server)
            {
                ScopeChildren = new List<IEntity>()
            };

            ServiceController[] services;
            try
            {
                services = ServiceController.GetServices(serverName);
            }
            catch (Exception e)
            {
                LoggerHelper.Log.WarnFormat("Cannot get access to the server '{0}' services. Details: {1}", serverName, e);
                return new List<IEntity> {LoadDatabaseServer(serverName, instancesList)};
            }
            OnProgress(new DependencyCheckerProgressEventArgs
            {
                Message = "Loading server " + server.Name,
                Progress = _totalProgress
            });

            var matched = false;
            try
            {
                foreach (var service in services)
                {
                    if (service.ServiceName.Contains("POne.Portfolio"))
                    {
                        var key =
                            RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, serverName)
                                .OpenSubKey(@"SYSTEM\CurrentControlSet\Services\" + service.ServiceName);
                        if (key != null)
                        {
                            var path = key.GetValue("ImagePath").ToString();
                            key.Close();

                            path = Path.Combine("\\\\" + serverName, path.Replace(":\\", "$\\").Trim('"') + ".config");

                            LoggerHelper.Log.Debug("Opening configuration file: " + path);

                            var configurationMap = new ExeConfigurationFileMap
                            {
                                ExeConfigFilename = path 
                            };
                            var config = ConfigurationManager.OpenMappedExeConfiguration(configurationMap, ConfigurationUserLevel.None);

                            var connection = new SqlConnectionStringBuilder
                            {
                                DataSource = config.AppSettings.Settings["ServerName"].Value,//TODO: WRONG PATH! CAN'T BEW LOCALHOST IN CONfIG:(
                                InitialCatalog = config.AppSettings.Settings["StorageDatabase"].Value,
                                IntegratedSecurity = true
                            };

                            var serviceEntity = new P1TCEntity(service.ServiceName, EntityType.P1TCService)
                            {
                                Scope = server
                            };
                            server.ScopeChildren.Add(serviceEntity);

                            var mipsConnection = new SqlConnectionStringBuilder(config.AppSettings.Settings["MipsConnection"].Value);
                            LoggerHelper.Log.DebugFormat("Mips connection detected: " + mipsConnection.ConnectionString);

                            if (instancesList.All(i => i.Definition != connection.ConnectionString))
                            {
                                LoggerHelper.Log.DebugFormat("Adding instance '{0}'", connection);
                                var newServer = LoadDatabaseServer(connection.DataSource.ToUpperInvariant(), instancesList,
                                    connection.InitialCatalog.ToUpperInvariant(), mipsConnection.DataSource.ToUpperInvariant(), mipsConnection.InitialCatalog.ToUpperInvariant());
                                result.Add(newServer);

                                //var newInstance = new P1TCEntity(config.AppSettings.Settings["StorageDatabase"].Value,
                                //    connection.ConnectionString, EntityType.P1TCInstance);

                                //var instanceContents = LoadDatabase(connection.ConnectionString, mipsConnection);
                                //Log.DebugFormat("Loaded {1} items for '{0}'", connection, instanceContents.Count);

                                //newInstance.ScopeChildren = new List<IEntity>();
                                //newInstance.ScopeChildren.AddRange(instanceContents);
                                //instanceContents.ForEach(i => i.Scope = newInstance);

                                //instancesList.Add(newInstance);
                                serviceEntity.AddDependency(newServer.ScopeChildren.First());

                                matched = true;
                            }
                        }
                    }
                }
            }
            catch (FileNotFoundException exception)
            {
                LoggerHelper.Log.WarnFormat("Cannot get P1TC data for the server '{0}'. Exception details: file '{1}' not found. Exception {2}",
                    serverName,
                    exception.FileName,
                    exception);
            }
            catch (Exception exception)
            {
                LoggerHelper.Log.WarnFormat("Cannot get P1TC data for the server '{0}'. Exception details: {1}",
                    serverName,
                    exception);
            }
            if (!matched)
            {
                return new List<IEntity> {LoadDatabaseServer(serverName, instancesList)};
            }
            _totalProgress += _serverWeight;
            result.Add(server);
            return result;
        }
    }
}
