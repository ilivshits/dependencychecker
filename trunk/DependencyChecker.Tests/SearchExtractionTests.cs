﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using DependencyChecker.Analyzer;
using DependencyChecker.Collector;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Extensions;
using DependencyChecker.Entities.Interfaces;
using DependencyChecker.Entities.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DependencyChecker.Tests
{
    [TestClass]
    [DeploymentItem("Resources\\test.rdl")]
    [DeploymentItem("Resources\\test_2.rdl")]
    [DeploymentItem("Resources\\test_3.rdl")]
    [DeploymentItem("Resources\\test_4.rdl")]
    [DeploymentItem("Resources\\test_5.rdl")]
    [DeploymentItem("Resources\\test_6.rdl")]
    [DeploymentItem("Resources\\test_7.rdl")]
    [DeploymentItem("Resources\\DBNameTestProc.sql")]
    [DeploymentItem("Resources\\TestProc.sql")]
    [DeploymentItem("Resources\\TestProc2.sql")]
    [DeploymentItem("Resources\\TestProc3.sql")]
    [DeploymentItem("Resources\\TestProc4.sql")]
    [DeploymentItem("Resources\\TestProc5.sql")]
    [DeploymentItem("Resources\\TestProc6.sql")]
    [DeploymentItem("Resources\\TestProc7.sql")]
    [DeploymentItem("Resources\\TestProc8.sql")]
    [DeploymentItem("Resources\\TestProc9.sql")]
    [DeploymentItem("Resources\\TestProc10.sql")]
    [DeploymentItem("Resources\\TestProc11.sql")]
    [DeploymentItem("Resources\\TestProc12.sql")]
    [DeploymentItem("Resources\\TestProc13.sql")]
    [DeploymentItem("Resources\\TestProc14.sql")]
    [DeploymentItem("Resources\\TestProc15.sql")]
    [DeploymentItem("Resources\\TestProc16.sql")]
    [DeploymentItem("Resources\\TestProc17.sql")]
    [DeploymentItem("Resources\\TestProc18.sql")]
    [DeploymentItem("Resources\\TestProc19.sql")]
    [DeploymentItem("Resources\\TestProc20.sql")]
    [DeploymentItem("Resources\\TestProc21.sql")]
    [DeploymentItem("Resources\\TestProc22.sql")]
    [DeploymentItem("Resources\\TestProc23.sql")]
    [DeploymentItem("Resources\\Trigger1.sql")]
    [DeploymentItem("Resources\\Trigger2.sql")]
    [DeploymentItem("Resources\\Trigger3.sql")]
    [DeploymentItem("Resources\\TestScript1.pl")]
    [DeploymentItem("Resources\\TestScript2.pl")]
    [DeploymentItem("Resources\\TestScript3.pl")]
    [DeploymentItem("Resources\\TestBatch1.bat")]
    [DeploymentItem("Resources\\TestDTS1.dts")]
    public class SearchExtractionTests
    {
        [TestMethod]
        public void ReportEntitySearchContentExtract()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "test_2.rdl"));

            var report = new ReportEntity("TestReport", source, EntityType.Report);

            report.AddDependency(new ReportEntity("TestDataSource", "test connection string", EntityType.DataSource) { InternalId = "internalDataSourceId" }, "FTB");

            var result = report.SearchContent;

            foreach (var content in result.Where(c => c.ContentType == EntityProviderType.SQL))
            {
                Debug.WriteLine(content.Text);
            }

            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(0, report.GetErrors().Length);
            Assert.AreEqual(2, result.Count(c => c.ContentType == EntityProviderType.SQL));
        }

        [TestMethod]
        public void ReportEntitySearchContentExtract_2()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "test_3.rdl"));

            var report = new ReportEntity("TestReport", source, EntityType.Report);

            report.AddDependency(new ReportEntity("TestDataSource", "test connection string", EntityType.DataSource) { InternalId = "internalDataSourceId" }, "FTB");

            var result = report.SearchContent;

            foreach (var content in result.Where(c => c.ContentType == EntityProviderType.SQL))
            {
                Debug.WriteLine(content.Text);
            }

            Assert.AreEqual(4, result.Count);
            Assert.AreEqual(3, result.Count(c => c.ContentType == EntityProviderType.SQL));
            Assert.AreEqual(1, report.GetErrors().Length);
            Assert.AreEqual(1, result.Count(c => c.ContentType == EntityProviderType.SSRS));
        }

        [TestMethod]
        public void ReportEntitySearchContentExtract_3()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "test_4.rdl"));

            var report = new ReportEntity("TestReport", source, EntityType.Report);

            report.AddDependency(new ReportEntity("TestDataSource", "test connection string", EntityType.DataSource) { InternalId = "internalDataSourceId" }, "FTB");

            var result = report.SearchContent;

            foreach (var content in result.Where(c => c.ContentType == EntityProviderType.SQL))
            {
                Debug.WriteLine(content.Text);
            }

            Assert.AreEqual(4, result.Count);
            Assert.AreEqual(3, result.Count(c => c.ContentType == EntityProviderType.SQL));
            Assert.AreEqual(0, report.GetErrors().Length);
        }

        [TestMethod]
        public void ReportEntitySearchContentExtract_4()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "test_5.rdl"));

            var report = new ReportEntity("TestReport", source, EntityType.Report);

            report.AddDependency(new ReportEntity("TestDataSource", "test connection string", EntityType.DataSource) { InternalId = "internalDataSourceId" }, "FTB");

            var result = report.SearchContent;

            foreach (var content in result.Where(c => c.ContentType == EntityProviderType.SQL))
            {
                Debug.WriteLine(content.Text);
            }

            Assert.AreEqual(9, result.Count);
            Assert.AreEqual(5, result.Count(c => c.ContentType == EntityProviderType.SQL));
            Assert.AreEqual(0, report.GetErrors().Length);
        }

        [TestMethod]
        public void ReportEntitySearchContentExtract_5()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "test_6.rdl"));

            var report = new ReportEntity("TestReport", source, EntityType.Report);

            report.AddDependency(new ReportEntity("TestDataSource", "test connection string", EntityType.DataSource) { InternalId = "internalDataSourceId" }, "FTB");

            var result = report.SearchContent;

            foreach (var content in result.Where(c => c.ContentType == EntityProviderType.SQL))
            {
                Debug.WriteLine(content.Text);
            }

            Assert.AreEqual(9, result.Count);
            Assert.AreEqual(5, result.Count(c => c.ContentType == EntityProviderType.SQL));
            Assert.AreEqual(0, report.GetErrors().Length);
        }

        [TestMethod]
        public void ReportEntitySearchContentExtract_6()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "test_7.rdl"));

            var report = new ReportEntity("TestReport", source, EntityType.Report);

            report.AddDependency(new ReportEntity("TestDataSource", "test connection string", EntityType.DataSource) { InternalId = "internalDataSourceId" }, "FTB");

            var result = report.SearchContent;

            foreach (var content in result.Where(c => c.ContentType == EntityProviderType.SQL))
            {
                Debug.WriteLine(content.Text);
            }

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result.Count(c => c.ContentType == EntityProviderType.SQL));
            Assert.AreEqual("[operations].[BrokerGridData]", result.Single(c => c.ContentType == EntityProviderType.SQL).Text);
            Assert.AreEqual(0, report.GetErrors().Length);
        }

        [TestMethod]
        public void ReportDatasourceSearchContentExtract()
        {
            var source = "Data Source=ny-gmitftbdb1;Initial Catalog=P1ReportingG2";

            var report = new ReportEntity("TestReport", source, EntityType.DataSource);

            var result = report.SearchContent;

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result.Count(c => c.ContentType == EntityProviderType.SQL));
            Assert.AreEqual("NY-GMITFTBDB1.P1ReportingG2", result.First(c => c.ContentType == EntityProviderType.SQL).Text);
        }

        [TestMethod]
        public void ReportDatasourceSearchContentExtract2()
        {
            var source = "Data Source=ny-gmitftbdb1;Initial Catalog=P1ReportingZZUAT";

            var report = new ReportEntity("TestReport", source, EntityType.DataSource);

            var result = report.SearchContent;

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result.Count(c => c.ContentType == EntityProviderType.SQL));
            Assert.AreEqual("NY-GMITFTBDB1.P1ReportingZZUAT", result.First(c => c.ContentType == EntityProviderType.SQL).Text);
        }

        [TestMethod]
        public void ReportDatasourceSearchContentExtract3()
        {
            var source = "Data Source=ny-gmitftbdb1;Initial Catalog=P1ReportingZZUAT;User=FORTRESS\\test";

            var report = new ReportEntity("TestReport", source, EntityType.DataSource, "FORTRESS\\test");

            var result = report.SearchContent;

            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result.Count(c => c.ContentType == EntityProviderType.SQL));
            Assert.AreEqual(1, result.Count(c => c.ContentType == EntityProviderType.Person));
            Assert.AreEqual("NY-GMITFTBDB1.P1ReportingZZUAT", result.First(c => c.ContentType == EntityProviderType.SQL).Text);
            Assert.AreEqual("FORTRESS\\test", result.First(c => c.ContentType == EntityProviderType.Person).Text);
        }

        [TestMethod]
        public void DatabaseEntitySearchContentExtract()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc.sql"));

            var report = new DatabaseEntity("TestProcedure", source, EntityType.StoredProcedure);

            var result = report.SearchContent;

            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(1, result.Count(c => c.ContentType == EntityProviderType.SQL));
            Assert.AreEqual(2, result.Count(c => c.ContentType == EntityProviderType.OLAP));
        }

        [TestMethod]
        public void DatabaseEntitySearchPatternMatch()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc2.sql"));

            var sp = new DatabaseEntity("TestProcedure2", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);


            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }
            Assert.AreEqual(14, matches.Count);
        }

        [TestMethod]
        public void VariablesSubstiutionTest()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc17.sql"));

            var sp = new DatabaseEntity("TestProcedure2", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);


            Assert.IsTrue(content.Text.IndexOf("P1ReportingG2", StringComparison.InvariantCultureIgnoreCase) > 0);

            content.ProcessVariables();

            var matches = sqlProvider.MatchItems(content.Text, sp);

            Debug.WriteLine("MATCHES FOUND:");
            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(9, matches.Count);
            Debug.WriteLine("PARSED CONTENT:");
            Debug.WriteLine(content.Text);

            Assert.AreEqual(-1, content.Text.IndexOf("@databaseName", StringComparison.InvariantCultureIgnoreCase));
        }

        [TestMethod]
        public void VariablesSubstiutionTest2()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc18.sql"));

            var sp = new DatabaseEntity("TestProcedure2", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);


            Assert.IsTrue(content.Text.IndexOf("P1ReportingG2", StringComparison.InvariantCultureIgnoreCase) > 0);

            content.ProcessVariables();

            var matches = sqlProvider.MatchItems(content.Text, sp);

            Debug.WriteLine("MATCHES FOUND:");
            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(17, matches.Count);
            Debug.WriteLine("PARSED CONTENT:");
            Debug.WriteLine(content.Text);

            Assert.AreEqual(-1, content.Text.IndexOf("@databaseName", StringComparison.InvariantCultureIgnoreCase));
        }

        [TestMethod]
        public void VariablesSubstiutionTest3()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestScript1.pl"));

            var entity = new PerlScriptEntity("TestSript", source);
            var sqlProvider = new PerlScriptsDataProvider(new TestServerProvider());
            //Assert.AreEqual(1, entity.SearchContent.Count(c => c.ContentType == EntityProviderType.Perl));
            //Assert.AreEqual(2, entity.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));

            var content = entity.SearchContent;

            foreach (var searchContent in content)
            {
                Debug.WriteLine("\r\n\r\ncontent [{0}]:\r\n{1}", searchContent.ContentType.ToString(), searchContent.Text);
            }
        }

        [TestMethod]
        public void PerlScriptParsingTest1()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestScript2.pl"));

            var entity = new PerlScriptEntity("TestSript", source);
            //Assert.AreEqual(1, entity.SearchContent.Count(c => c.ContentType == EntityProviderType.Perl));
            //Assert.AreEqual(2, entity.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));

            var content = entity.SearchContent;

            foreach (var searchContent in content)
            {
                Debug.WriteLine("\r\n\r\ncontent [{0}](in {2}):\r\n{1}", searchContent.ContentType, searchContent.Text, searchContent.ScopeName);
            }
        }

        [TestMethod]
        public void PerlScriptParsingTest2()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestScript3.pl"));

            var entity = new PerlScriptEntity("TestSript", source);
            //Assert.AreEqual(1, entity.SearchContent.Count(c => c.ContentType == EntityProviderType.Perl));
            //Assert.AreEqual(2, entity.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));

            var content = entity.SearchContent;

            foreach (var searchContent in content)
            {
                Debug.WriteLine("\r\n\r\ncontent [{0}](in {2}):\r\n{1}", searchContent.ContentType, searchContent.Text, searchContent.ScopeName);
            }
        }

        [TestMethod]
        public void BatchBcpParsingTest1()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestBatch1.bat"));

            var entity = new ScheduledTaskEntity("TestSript", source, EntityType.BatchFile);

            var content = entity.SearchContent;

            foreach (var searchContent in content)
            {
                Debug.WriteLine("\r\n\r\ncontent [{0}](in {2}):\r\n{1}", searchContent.ContentType, searchContent.Text, searchContent.ScopeName);
            }
            Assert.AreEqual(2, entity.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
        }

        [TestMethod]
        public void DTSParsingTest1()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestDTS1.dts"));

            var entity = new DTSEntity("TestSript", source);
            //Assert.AreEqual(1, entity.SearchContent.Count(c => c.ContentType == EntityProviderType.Perl));
            //Assert.AreEqual(2, entity.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));

            var content = entity.SearchContent;

            foreach (var searchContent in content)
            {
                Debug.WriteLine("\r\n\r\ncontent [{0}]:\r\n{1}", searchContent.ContentType.ToString(), searchContent.Text);
            }
        }



        [TestMethod]
        public void FunctionSubstiutionTest()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc2.sql"));

            var sp = new DatabaseEntity("TestProcedure2", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            var funMatch = matches.SingleOrDefault(m => m.MatchText == "dbo.fun_split");
            Assert.IsNotNull(funMatch);
            Assert.AreEqual(14, matches.Count);

            Assert.IsTrue(content.Text.IndexOf("fun_split", StringComparison.InvariantCultureIgnoreCase) > 0);

            content.ReplaceMatch(funMatch, "'!!!SOME TEST!!!'");

            Debug.WriteLine(content.Text);

            Assert.AreEqual(-1, content.Text.IndexOf("fun_split", StringComparison.InvariantCultureIgnoreCase));
        }

        [TestMethod]
        public void FunctionSubstiutionTest2()
        {
            var funcSource = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "DBNameTestProc.sql"));
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc23.sql"));

            var sp = new DatabaseEntity("spGetGSMACountryPositions", source, EntityType.StoredProcedure);
            var func = new DatabaseEntity("fnGetP1ReportingG2DatabaseName", source, EntityType.StoredProcedure);

            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            var funMatch = matches.SingleOrDefault(m => m.MatchText == "[dbo].[fngetp1reportingg2databasename]");
            Assert.IsNotNull(funMatch);
            Assert.AreEqual(14, matches.Count);
        }

        [TestMethod]
        public void DatabaseFunctionSearchPatternMatch()
        {
            var source = @"declare @asOfDate date    	= @asOfDateParam 
declare @historicalDays int	= @historicalDaysParam 

IF(@asOfDate is null) set @asOfDate = getdate()
	
if(@historicalDays is not null)
begin	
      set @asOfDate = dateadd(d,(-1)*ABS(@historicalDays),@asOfDate)
      set @asOfDate = dbo.fnBusDayOrPrev(@asOfDate)
end

select asOfDate= @asOfDate
GO


CREATE VIEW [dbo].[v_LiquidBLByRegion]
AS
SELECT l.business_id, r1.region_label AS region_1, r2.region_label AS region_2, r3.region_label AS region_3, 
rank() OVER (ORDER BY r1.region_order, r2.region_order, r3.region_order) AS rank
FROM v_LiquidBusinessConfig l 
LEFT JOIN busl_region r1 ON l.region_level_1 = r1.region_id
LEFT JOIN busl_region r2 ON l.region_level_2 = r2.region_id
LEFT JOIN busl_region r3 ON l.region_level_3 = r3.region_id

";

            var sp = new DatabaseEntity("TestProcedure2", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);


            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }
            Assert.AreEqual(3, matches.Count);
        }

        [TestMethod]
        public void DatabaseFunctionSearchPatternMatch2()
        {
            var source = @"select dbo.fn_GetCapitalLeverageFactor1(@ReportFund,@To)";

            var sp = new DatabaseEntity("TestProcedure3", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);


            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }
            Assert.AreEqual(1, matches.Count);
        }

        [TestMethod]
        public void DatabaseFunctionSearchPatternMatch3()
        {
            var source = @"select * into #sync_funds 	from MPS3.Imagine.sync.v_syncLegalEntity;
	
	DECLARE @legal_entity varchar(500);
	SELECT @legal_entity  = COALESCE(@legal_entity + ', ', '') + legal_entity
	FROM #sync_funds sf 
	WHERE sf.fund_id  IN (@fund)
	drop table #sync_funds;
	
	select @legal_entity AS legal_entity
";

            var sp = new DatabaseEntity("TestProcedure4", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);


            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }
            Assert.AreEqual(1, matches.Count);
        }

        [TestMethod]
        public void DatabaseSequenseSelectSearchPatternMatch3()
        {
            var source = @"SELECT r1.as_of_date,	Trader,	Acct,[FIG_Con_SSS4_1], r1.Value10 as Tid, r1.RowID, r1.Long_Security_Name,r1.Type
  FROM [MipsHist].[dbo].[FIG_ConStressReport] sr  ,[MipsHist].[dbo].z_mips_reporting1 r1
  where sr.RowID = r1.RowID and r1.as_of_date = sr.as_of_date and Trader='CNVX' and MONTH(r1.as_of_date) = month(@date)  and YEAR(r1.as_of_date) = year(@date)";

            var sp = new DatabaseEntity("TestProcedure4_1", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            } 

            Assert.AreEqual(2, matches.Count);
        }

        [TestMethod]
        public void DatabaseFunctionSearchPatternMatch4()
        {
            var source = @"select distinct Fund from MPS.CharlesRiver.dbo.FutureCommissionSchedule";

            var sp = new DatabaseEntity("TestProcedure4", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);


            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            } 
            Assert.AreEqual(1, matches.Count);
        }

        [TestMethod]
        public void ProcedureDynamicDatabaseNameSearchPatternMatch()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc5.sql"));

            var sp = new DatabaseEntity("TestProcedure5", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);


            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }
            Assert.AreEqual(9, matches.Count);
        }

        [TestMethod]
        public void ProcedureFunctionSearchPatternMatch()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc6.sql"));

            var sp = new DatabaseEntity("TestProcedure6", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(17, matches.Count);
        }

        [TestMethod]
        public void ProcedureSearchPatternMatch()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc7.sql"));

            var sp = new DatabaseEntity("TestProcedure7", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(4, matches.Count);
        }

        [TestMethod]
        public void FunctionSearchPatternMatch()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc8.sql"));

            var sp = new DatabaseEntity("TestProcedure8", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(17, matches.Count);
        }

        [TestMethod]
        public void FunctionCrossApplySearchPatternMatch()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc9.sql"));

            var sp = new DatabaseEntity("TestProcedure9", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(7, matches.Count);
        }

        [TestMethod]
        public void SlowProcedureSearchPatternMatch()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc10.sql"));

            var sp = new DatabaseEntity("TestProcedure10", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(13, matches.Count);
        }

        [TestMethod]
        public void SlowProcedureSearchPatternMatch2()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc11.sql"));

            var sp = new DatabaseEntity("TestProcedure11", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(18, matches.Count);

        }

        [TestMethod]
        public void SlowProcedureSearchPatternMatch3()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc21.sql"));

            var sp = new DatabaseEntity("TestProcedure21", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(1, matches.Count);

        }

        [TestMethod]
        public void SlowProcedureSearchPatternMatch4()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc22.sql"));

            var sp = new DatabaseEntity("TestProcedure22", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(1, matches.Count);

        }

        [TestMethod]
        public void FunctionSearchPatternMatch2()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc12.sql"));

            var sp = new DatabaseEntity("TestProcedure12", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(8, matches.Count);

        }


        [TestMethod]
        public void SearchWithCommentsPatternMatch()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc13.sql"));

            var sp = new DatabaseEntity("TestProcedure13", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(8, matches.Count);

        }

        [TestMethod]
        public void OpenQueryPatternMatch()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc14.sql"));

            var sp = new DatabaseEntity("TestProcedure14", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(13, matches.Count);

        }

        [TestMethod]
        public void SlowTriggersSearchPatternMatch()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Trigger1.sql"));

            var sp = new DatabaseEntity("Trigger1", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(1, matches.Count);
        }

        [TestMethod]
        public void SlowTriggersSearchPatternMatch2()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Trigger2.sql"));

            var sp = new DatabaseEntity("Trigger1", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }
            Assert.AreEqual(0, matches.Cast<Match>().Count(m => m.Groups["name"].Success));
        }

        [TestMethod]
        public void SlowTriggersSearchPatternMatch3()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Trigger3.sql"));

            var sp = new DatabaseEntity("Trigger1", source, EntityType.StoredProcedure);
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }

            Assert.AreEqual(0, matches.Count);
        }

        [TestMethod]
        public void DatabaseEntityOlapExtract()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc3.sql"));

            var sp = new DatabaseEntity("TestProcedure3", source, EntityType.StoredProcedure);
            var olapProvider = new OlapDataProvider(new TestServerProvider());

            var extracted = sp.SearchContent;

            Assert.AreEqual(3, extracted.Count);
            Assert.AreEqual(2, extracted.Count(c => c.ContentType == EntityProviderType.OLAP));


            var content = sp.SearchContent.First(c => c.ContentType == EntityProviderType.OLAP);

            var matches = olapProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }
            Assert.AreEqual(8, matches.Count);
        }

        [TestMethod]
        public void DatabaseEntityOlapExtract2()
        {
            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestProc20.sql"));

            var sp = new DatabaseEntity("TestProcedure20", source, EntityType.StoredProcedure);
            var olapProvider = new OlapDataProvider(new TestServerProvider());

            var extracted = sp.SearchContent;

            Assert.AreEqual(2, extracted.Count);
            Assert.AreEqual(1, extracted.Count(c => c.ContentType == EntityProviderType.OLAP));


            var content = sp.SearchContent.First(c => c.ContentType == EntityProviderType.OLAP);

            var matches = olapProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }
            Assert.AreEqual(5, matches.Count);
        }

        [TestMethod]
        public void P1TCEntityExtract1()
        {
            var source = @"{Security Type}, {Simple Long/Short}, {Clearing Broker}, {Sid}, {Strategy}, {Forward Date}, {End Quantity}, {End Market Value}, {Sid}, {Forward Date}, {Clearing Broker}, {Strategy}, {Security Type}, {YC1 Curve}, {IsOption}, {Start Con Shift Day 1M}, {End Con Shift Day 1M}, {Start Delta VegaAdj}, {End Delta VegaAdj}, {Start YC1 +10}, {End YC1 +10}, {Start YC1 +20}, {End YC1 +20}, {Start YC1 +30}, {End YC1 +30}, {Start YC1 -10}, {End YC1 -10}, {Start YC1 -20}, {End YC1 -20}, {Start YC1 -30}, {End YC1 -30}, {Start YC1 10Y}, {End YC1 10Y}, {Start YC1 18M}, {End YC1 18M}, {Start YC1 1Y}, {End YC1 1Y}, {Start YC1 20Y}, {End YC1 20Y}, {Start YC1 2Y}, {End YC1 2Y}, {Start YC1 30Y}, {End YC1 30Y}, {Start YC1 3M}, {End YC1 3M}, {Start YC1 3Y}, {End YC1 3Y}, {Start YC1 4Y}, {End YC1 4Y}, {Start YC1 5Y}, {End YC1 5Y}, {Start YC1 6M}, {End YC1 6M}, {Start YC1 7Y}, {End YC1 7Y}, {Start YC1 9M}, {End YC1 9M}, {YC1 Currency}, {Start YC1 Spot}, {End YC1 Spot}, {Start YC2 +10}, {End YC2 +10}, {Start YC2 +20}, {End YC2 +20}, {Start YC2 +30}, {End YC2 +30}, {Start YC2 -10}, {End YC2 -10}, {Start YC2 -20}, {End YC2 -20}, {Start YC2 -30}, {End YC2 -30}, {Start YC2 10Y}, {End YC2 10Y}, {Start YC2 18M}, {End YC2 18M}, {Start YC2 1Y}, {End YC2 1Y}, {Start YC2 20Y}, {End YC2 20Y}, {Start YC2 2Y}, {End YC2 2Y}, {Start YC2 30Y}, {End YC2 30Y}, {Start YC2 3M}, {End YC2 3M}, {Start YC2 3Y}, {End YC2 3Y}, {Start YC2 4Y}, {End YC2 4Y}, {Start YC2 5Y}, {End YC2 5Y}, {Start YC2 6M}, {End YC2 6M}, {Start YC2 7Y}, {End YC2 7Y}, {Start YC2 9M}, {End YC2 9M}, {YC2 Currency}, {Start YC2 Spot}, {End YC2 Spot}, {Start YC3 +10}, {End YC3 +10}, {Start YC3 +20}, {End YC3 +20}, {Start YC3 +30}, {End YC3 +30}, {Start YC3 -10}, {End YC3 -10}, {Start YC3 -20}, {End YC3 -20}, {Start YC3 -30}, {End YC3 -30}, {Start YC3 10Y}, {End YC3 10Y}, {Start YC3 18M}, {End YC3 18M}, {Start YC3 1Y}, {End YC3 1Y}, {Start YC3 20Y}, {End YC3 20Y}, {Start YC3 2Y}, {End YC3 2Y}, {Start YC3 30Y}, {End YC3 30Y}, {Start YC3 3M}, {End YC3 3M}, {Start YC3 3Y}, {End YC3 3Y}, {Start YC3 4Y}, {End YC3 4Y}, {Start YC3 5Y}, {End YC3 5Y}, {Start YC3 6M}, {End YC3 6M}, {Start YC3 7Y}, {End YC3 7Y}, {Start YC3 9M}, {End YC3 9M}, {YC3 Currency}, {Start YC3 Spot}, {End YC3 Spot}, {Start YC4 +10}, {End YC4 +10}, {Start YC4 +20}, {End YC4 +20}, {Start YC4 +30}, {End YC4 +30}, {Start YC4 -10}, {End YC4 -10}, {Start YC4 -20}, {End YC4 -20}, {Start YC4 -30}, {End YC4 -30}, {Start YC4 10Y}, {End YC4 10Y}, {Start YC4 18M}, {End YC4 18M}, {Start YC4 1Y}, {End YC4 1Y}, {Start YC4 20Y}, {End YC4 20Y}, {Start YC4 2Y}, {End YC4 2Y}, {Start YC4 30Y}, {End YC4 30Y}, {Start YC4 3M}, {End YC4 3M}, {Start YC4 3Y}, {End YC4 3Y}, {Start YC4 4Y}, {End YC4 4Y}, {Start YC4 5Y}, {End YC4 5Y}, {Start YC4 6M}, {End YC4 6M}, {Start YC4 7Y}, {End YC4 7Y}, {Start YC4 9M}, {End YC4 9M}, {YC4 Currency}, {Start YC4 Spot}, {End YC4 Spot}, {Start YC5 +10}, {End YC5 +10}, {Start YC5 +20}, {End YC5 +20}, {Start YC5 +30}, {End YC5 +30}, {Start YC5 -10}, {End YC5 -10}, {Start YC5 -20}, {End YC5 -20}, {Start YC5 -30}, {End YC5 -30}, {Start YC5 10Y}, {End YC5 10Y}, {Start YC5 18M}, {End YC5 18M}, {Start YC5 1Y}, {End YC5 1Y}, {Start YC5 20Y}, {End YC5 20Y}, {Start YC5 2Y}, {End YC5 2Y}, {Start YC5 30Y}, {End YC5 30Y}, {Start YC5 3M}, {End YC5 3M}, {Start YC5 3Y}, {End YC5 3Y}, {Start YC5 4Y}, {End YC5 4Y}, {Start YC5 5Y}, {End YC5 5Y}, {Start YC5 6M}, {End YC5 6M}, {Start YC5 7Y}, {End YC5 7Y}, {Start YC5 9M}, {End YC5 9M}, {YC5 Currency}, {Start YC5 Spot}, {End YC5 Spot}, {Start YC6 +10}, {End YC6 +10}, {Start YC6 +20}, {End YC6 +20}, {Start YC6 +30}, {End YC6 +30}, {Start YC6 -10}, {End YC6 -10}, {Start YC6 -20}, {End YC6 -20}, {Start YC6 -30}, {End YC6 -30}, {Start YC6 10Y}, {End YC6 10Y}, {Start YC6 18M}, {End YC6 18M}, {Start YC6 1Y}, {End YC6 1Y}, {Start YC6 20Y}, {End YC6 20Y}, {Start YC6 2Y}, {End YC6 2Y}, {Start YC6 30Y}, {End YC6 30Y}, {Start YC6 3M}, {End YC6 3M}, {Start YC6 3Y}, {End YC6 3Y}, {Start YC6 4Y}, {End YC6 4Y}, {Start YC6 5Y}, {End YC6 5Y}, {Start YC6 6M}, {End YC6 6M}, {Start YC6 7Y}, {End YC6 7Y}, {Start YC6 9M}, {End YC6 9M}, {YC6 Currency}, {Start YC6 Spot}, {End YC6 Spot}, {End FX Exp +1%}, {End FX Exp +2%}, {End FX Exp +3%}, {End FX Exp -1%}, {End FX Exp -2%}, {End FX Exp -3%}, {End FX Spot +1%}, {End FX Spot +2%}, {End FX Spot +3%}, {End FX Spot -1%}, {End FX Spot -2%}, {End FX Spot -3%}, {Start Delta [N]}, {End Delta [N]}, {Start Delta $}, {End Delta $}, {Start Carry $}, {End Carry $}, {Start Carry P&L $}, {Carry P&L}, {Start Cr01 $}, {End Cr01 $}, {Start Cr01 P&L $}, {Cr01 P&L}, {Start Delta}, {End Delta}, {Start Delta P&L $}, {Delta P&L}, {Start Dv01 $}, {End Dv01 $}, {Start Dv01 P&L $}, {Dv01 P&L}, {Start Gamma $}, {End Gamma $}, {Start Gamma P&L $}, {Gamma P&L}, {Start P&L Position $}, {Position P&L}, {Start P&L Trade $}, {Trading P&L}, {Start Theta $}, {End Theta $}, {Start Theta P&L $}, {Theta P&L}, {Start Vega $}, {End Vega $}, {Start Vega P&L $}, {Vega P&L}, {Start FX Exposure Notional}, {End FX Exposure Notional}, {Start FX Risk Notional}, {End FX Risk Notional}, {STag: Centaurus Beta Override [Start]}, {USTag: Centaurus Beta Override [Start]}, {STag: Centaurus Liquidity Days Override [Start]}, {USTag: Centaurus Liquidity Days Override [Start]}, {USTag: Centaurus Liquidity Days Override [End]}, {USTag: Bloomberg Raw Beta [End]}, {USTag: Bloomberg Raw Beta [Start]}, {USTag: Bloomberg Beta R Squared [End]}, {USTag: Bloomberg Beta R Squared [Start]}, {Simple Long/Short}, ";

            var entity = new P1TCEntity("View1", source, EntityType.P1TCView);
            var provider = new P1TCDataProvider(new TestServerProvider());

            var extracted = entity.SearchContent;

            Assert.AreEqual(1, extracted.Count);
            Assert.AreEqual(1, extracted.Count(c => c.ContentType == EntityProviderType.P1TC));


            var content = entity.SearchContent.First(c => c.ContentType == EntityProviderType.P1TC);

            var matches = provider.MatchItems(content.Text, entity);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }
            Assert.AreEqual(279, matches.Count);
        }

        [TestMethod]
        public void OlapSearchPatternMatch()
        {
            var source = @"insert into #bline (business_line, daily_pnl, wtd_pnl, mtd_pnl, ytd_pnl) select * from OpenQuery(CUBE1_OLAP, select {[Measures].[Aexeo Pnl], [Measures].[WTD_REAL], [Measures].[MTD_REAL], [Measures].[YTD_REAL]} on 0,
	nonempty([Tags].[Business Tag].[Business Tag].Members, [Measures].[YTD_Real]) on 1
	from [Pnl Data]
	where {[Time].[Date].&[";
            var olapProvider = new OlapDataProvider(new TestServerProvider());

            var matches = olapProvider.MatchItems(source, new ReportEntity("test", source, EntityType.Report));

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }
            Assert.AreEqual(7, matches.Count);
        }

        [TestMethod]
        public void GetExecuablePath()
        {
            var result = ScheduledTasksDataProvider.GetExecutableName(@"\\ny-files1\groups\HedgeNonRes\Operations\Compliance\BatchFileImport.exe",
                @"\\ny-files1\groups\HedgeNonRes\Operations\Compliance");

            Assert.AreEqual(@"\\ny-files1\groups\HedgeNonRes\Operations\Compliance\BatchFileImport.exe", result);

            result = ScheduledTasksDataProvider.GetExecutableName(@"y:\Prod\Scripts\ComplianceOwnershipAlert.pl",
                @"y:\Prod\Scripts");

            Assert.AreEqual(@"y:\Prod\Scripts\ComplianceOwnershipAlert.pl", result);

            result = ScheduledTasksDataProvider.GetExecutableName(@"perl Y:\Prod\Scripts\DownloadCESR_MiFID_SharesList.pl",
                @"Y:\Prod\Scripts");

            Assert.AreEqual(@"Y:\Prod\Scripts\DownloadCESR_MiFID_SharesList.pl", result);

            result = ScheduledTasksDataProvider.GetExecutableName(@"cmd /C  DownloadDB_FX.pl >> Y:\Prod\log\DownloadDB_FX_log.txtl",
                @"Y:\Prod\Scripts");

            Assert.AreEqual(@"Y:\Prod\Scripts\DownloadDB_FX.pl", result);

            result = ScheduledTasksDataProvider.GetExecutableName(@"Y:\Prod\Scripts\DownloadFuturesMarginFiles.bat ",
                @"Y:\Prod\Scripts");

            Assert.AreEqual(@"Y:\Prod\Scripts\DownloadFuturesMarginFiles.bat", result);

            result = ScheduledTasksDataProvider.GetExecutableName(@"cmd.exe /c FtpConsensysPositionFile.bat _rt >> ..\logs\audit_%date:~10,4%%date:~4,2%%date:~7,2%.txt",
                @"Y:\Prod\Scripts");

            Assert.AreEqual(@"Y:\Prod\Scripts\FtpConsensysPositionFile.bat", result);
        }

        [TestMethod]
        public void DatabaseEntityResolveDynamic()
        {
            DatabaseEntityResolveDynamicTest("NY-GMITFTBDB1", "NY-GMITMPS1", "MPS", "y_assets", "dbo",
                new List<string> { "ActivePnL" }, "TestProc2.sql", 14);
        }

        [TestMethod]
        public void DatabaseEntityResolveDynamic2()
        {
            DatabaseEntityResolveDynamicTest("NY-GMITFTBDB1", "NY-GMITFTBDB2", "P1G2", "FTBTrade", "dbo",
                new List<string> {"Security"}, "TestProc15.sql", 12);
        }
        [TestMethod]
        public void DatabaseEntityResolveByServer()
        {
            DatabaseEntityResolveDynamicTest("NY-GMITFTBDB1", "NY-GMITFTBDB2", "P1TC", "FTBTrade", "dbo",
                new List<string> { "SecurityRestriction", "SecurityPrimaryType", "Order" }, "TestProc19.sql", 12);
        }

        
        private void DatabaseEntityResolveDynamicTest(string dbServerName, 
                                                        string remoteServerName, 
                                                        string linkedName, 
                                                        string dbName,
                                                        string schemaName, List<string> objectNames, string spFile, int matchesCount = -1)
        {
            var remoteServer = new DatabaseEntity(remoteServerName, EntityType.Server);
            var dbServer = new DatabaseEntity(dbServerName, EntityType.Server);
            var linkedServer = new DatabaseEntity(linkedName, remoteServerName, EntityType.LinkedServer)
            {
                Scope = dbServer
            };
            dbServer.ScopeChildren = new List<IEntity>
            {
                linkedServer
            };
            linkedServer.AddDependency(remoteServer, remoteServer.Name);

            remoteServer.Aliases.Add(new EntityAlias
            {
                TypeName = EntityType.LinkedServer.ToString(),
                DisplayName = linkedName,
                DataProvider = DatabaseEntity.ProviderName,
                DefinitionScope = dbServer
            });

            var yAssetsDatabase = new DatabaseEntity(dbName, EntityType.Database)
            {
                Scope = dbServer
            };

            var schema = new DatabaseEntity(schemaName, EntityType.Table)
            {
                Scope = yAssetsDatabase
            };

            var source = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, spFile));

            var sp = new DatabaseEntity(spFile.Replace(".sql", String.Empty), source, EntityType.StoredProcedure)
            {
                Scope = schema
            };
            var sqlProvider = new SQLDataProvider(new TestServerProvider());
            var resolver = new SQLResolver();
            Assert.AreEqual(1, sp.SearchContent.Count(c => c.ContentType == EntityProviderType.SQL));
            var content = sp.SearchContent.Single(c => c.ContentType == EntityProviderType.SQL);
            content.ProcessVariables();

            Debug.WriteLine("CONTENT:");
            Debug.WriteLine(content.Text);

            var matches = sqlProvider.MatchItems(content.Text, sp);

            foreach (var match in matches)
            {
                Debug.WriteLine(match);
            }
            if (matchesCount > 0)
            {
                Assert.AreEqual(matchesCount, matches.Count);
            }

            foreach (var objectName in objectNames)
            {
                bool added;
                var result =
                    resolver.TryResolve(
                        matches.Single(m => m.MatchText.EndsWith(objectName.ToLowerInvariant()) || m.MatchText.EndsWith("[" + objectName.ToLowerInvariant() + "]")),
                        new List<IEntity>
                        {
                            new DatabaseEntity(objectName, EntityType.Table)
                            {
                                Scope = new DatabaseEntity(schemaName, EntityType.Table)
                                {
                                    Scope = new DatabaseEntity(dbName, EntityType.Database)
                                    {
                                        Scope = remoteServer
                                    }
                                }
                            },
                            new DatabaseEntity(objectName, EntityType.Table)
                            {
                                Scope = schema
                            }
                        }.RemoveDuplicates(), sp, content, out added);
                Assert.IsTrue(added);
                Assert.IsTrue(result);
                Assert.IsTrue(sp.DependsOn.Any(e => e.Root.Id == remoteServer.Id));
            }
        }
    }
}
