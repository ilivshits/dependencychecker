
CREATE PROCEDURE [FTBTrade].[dbo_SecurityIndustryClassification_Upsert]
	@sid bigint,
			
	--- Auditing.
	@application_source varchar(100),
	
	--- Industry classifications
	@bloombergIndustrySectorCode varchar(100),
	@bloombergIndustryGroupCode varchar(100),
	@bloombergIndustrySubgroupCode varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--- NOTE: The expectation is that this is called from within another transaction.	
	--- Bloomberg industry classifications.
	declare @ic table (industryClassificationLevelId int, industryClassificationId int)
	
	--- Stage into temp table.
	insert into @ic (industryClassificationLevelId, industryClassificationId)
	select
		ic.industryClassificationLevelId, ic.industryClassificationId
	from
		FTBTrade.dbo_IndustryClassificationLevel as icl with (nolock)
			cross join FTBTrade.dbo_IndustryClassificationType_Transposed as ict
			inner join FTBTrade.dbo_IndustryClassification as ic with (nolock) on
				ic.industryClassificationLevelId = icl.industryClassificationLevelId
	where
		icl.industryClassificationTypeId = ict.[Bloomberg Industry Classification System] and
		(
			(icl.description = 'Sector' and ic.code = @bloombergIndustrySectorCode) or
			(icl.description = 'Group' and ic.code = @bloombergIndustryGroupCode) or
			(icl.description = 'Subgroup' and ic.code = @bloombergIndustrySubgroupCode)
		)

	--- Update existing where they don't exist.
	update 
		sic
	set
		industryClassificationId = ic.industryClassificationId,
		application_source = @application_source
	from
		FTBTrade.dbo_SecurityIndustryClassification as sic with (nolock)
			inner join @ic as ic on
				ic.industryClassificationLevelId = sic.industryClassificationLevelId
	where
		sic.sid = @sid and
		sic.industryClassificationId != ic.industryClassificationId

	--- Insert where it doesn't exist.
	insert into FTBTrade.dbo_SecurityIndustryClassification (sid, industryClassificationLevelId, industryClassificationId, application_source)
	select
		@sid, ic.industryClassificationLevelId, ic.industryClassificationId, @application_source
	from
		@ic as ic
	where
		not exists (
			select
				sic.*
			from
				FTBTrade.dbo_SecurityIndustryClassification as sic with (nolock)
			where
				sic.sid = @sid and
				sic.industryClassificationLevelId = ic.industryClassificationLevelId
		)
END



