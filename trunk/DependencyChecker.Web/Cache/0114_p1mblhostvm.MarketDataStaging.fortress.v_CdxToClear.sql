
create view [fortress].[v_CdxToClear] as
	select
		f.*,
		e.exchangeId as ExchangeId
	from
		FTBTrade.dbo_Exchange as e with (nolock)
			cross join (
				--- CDX.NA.IG
				select 'CDX.NA.IG' as IndexSubFamily, '3Y' as Tenor, 15 as Series union
				select 'CDX.NA.IG' as IndexSubFamily, '5Y' as Tenor, 11 as Series union
				select 'CDX.NA.IG' as IndexSubFamily, '7Y' as Tenor, 8 as Series union
				select 'CDX.NA.IG' as IndexSubFamily, '10Y' as Tenor, 8 as Series union

				--- CDX.NA.IG
				select 'CDX.NA.HY' as IndexSubFamily, '5Y' as Tenor, 11 as Series union

				--- iTraxx Europe
				select 'iTraxx Europe' as IndexSubFamily, '5Y' as Tenor, 10 as Series union
				select 'iTraxx Europe' as IndexSubFamily, '10Y' as Tenor, 7 as Series union

				--- iTraxx Europe Crossover
				select 'iTraxx Europe Crossover' as IndexSubFamily, '5Y' as Tenor, 10 as Series union

				--- iTraxx Europe HiVol
				select 'iTraxx Europe HiVol' as IndexSubFamily, '5Y' as Tenor, 10 as Series
			
			) as f
		cross join fortress.v_Exchange_KnownMic_Pivot as km
	where
		e.codeIsMicFlag != 0 and
		e.code in (km.Ice, km.Cme)



