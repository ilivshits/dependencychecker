create function [dbo].[GetIssuerWithDescendants](@issuerId int)
returns table as return
with Subsidiaries(parentId, childId) as
(
	select [parentIssuerId], [issuerId]
	from [dbo].[Issuer] where [issuerId] = @issuerId
	union all 
	select t.[parentIssuerId], t.[issuerId]
	from [dbo].[Issuer] t
	inner join Subsidiaries h on t.[parentIssuerId] = h.childId
)
select childId from Subsidiaries


