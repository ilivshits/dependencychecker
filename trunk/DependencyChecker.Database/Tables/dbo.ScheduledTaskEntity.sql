﻿CREATE TABLE [dbo].[ScheduledTaskEntity]
(
	[Id]               UNIQUEIDENTIFIER       CONSTRAINT [FK_ScheduledTaskEntity_BaseEntity] FOREIGN KEY REFERENCES [dbo].[EntityBase] ([Id])  NOT NULL,
    [TaskLastRunTime] DATETIME2 NULL, 
	CONSTRAINT [PK_ScheduledTaskEntity] PRIMARY KEY ([Id])
)
 