﻿CREATE PROCEDURE [dbo].[GetRebalanceDataDump]	
@Traders as varchar(5000) = 'Macro G-10,Macro: Lo,Macro: Szilagyi,Adam Levinson,Adam: SM,Tom Barket,CONVEX,Geskos,Gwee,Dratewska,Phantom Live,Sandeep,Macro: Sholes,Swartz,Phantom Macro,Capital Markets,Capital Markets Asia,Andreas,Phantom,PHM,Convex_Test,Macro: Arnopolin,Macro: Illiano,Macro: Tisch,CreditVal,IPO,Callanan,Bajaj,Macro: McTeague,Feder,Craft,Mansfield,CASH'
AS  
BEGIN
--declare @Traders as varchar(5000);
--set @Traders= 'Macro G-10,Macro: Lo,Macro: Szilagyi,Adam Levinson,Adam: SM,Tom Barket,CONVEX,Geskos,Gwee,Dratewska,Phantom Live,Sandeep,Macro: Sholes,Swartz,Phantom Macro,Capital Markets,Capital Markets Asia,Andreas,Phantom,PHM,Convex_Test,Macro: Arnopolin,Macro: Illiano,Macro: Tisch,CreditVal,IPO,Callanan,Bajaj,Macro: McTeague,Feder,Craft,Mansfield,CASH'

select * into #tmpTrader from dbo.fun_Split(@Traders, ',')

select distinct
	Trader_Name
	, Acct
	, Legal_Entity_Description
	, Customer
	, Value10
	, [FIG_ESP_$] = sum([FIG_ESP_$])
	, [FIG_Dv01_$] = sum([FIG_Dv01_$])
	, [FIG_Cr01_$] = sum([FIG_Cr01_$])
into #tmpReporting1
from FTB.Mips.dbo.z_mips_reporting1
where Trader <> 'PNL'
and Trader not like 'ZZ-%'
and Trader_Name not like 'ZZ-%'
and Acct not like '%TEST'
group by
	Trader_Name
	, Acct
	, Legal_Entity_Description
	, Customer
	, Value10

select distinct--oaa.Subfund
oaa.[Trader Id] 
, oa.[Clear Agent]
, oa.Strategy
, oa.Id
, oa.[Exec Broker]
into #tmpExec
from y_assets.dbo.OrderActivity oa 
			join y_assets.dbo.OrderAllocActivity oaa on oaa.[Order Id] = oa.[Order Id]
join y_assets.dbo.Instrument i on i.Tid = oa.Id
where oa.Status = 'filled'
and oa.[Exec Broker] not like 'Z%'
and (oa.[Clear Agent] like '%-OTC' 

-- Request to show IR swap executing brokers for -FCM clear brokers
or oa.[Clear Agent] like '%-FCM' and i.[Instr Type] = 'IR/Currency Swap'

-- Request to show OTC FX Option executing brokers for DB clear brokers
or oa.[Clear Agent] = 'DB' and i.[Instr Type] = 'OTC FX Option'

-- Request to show all CDS executing brokers
or i.[Instr Type] = 'Credit Default Swap')
group by --oaa.Subfund
oaa.[Trader Id] 
, oa.[Clear Agent]
, oa.Strategy
, oa.Id
, oa.[Exec Broker]
order by
oaa.[Trader Id] 
, oa.[Clear Agent]
, oa.Strategy
, oa.Id
, oa.[Exec Broker]

select distinct
	[Trader Id]
	, Strategy
	, Id
	, [Exec Broker]
into #exec
from #tmpExec
order by [Trader Id]
	, Strategy
	, Id
	, [Exec Broker]

select trader_id, name
into #traders
from y_assets.dbo.trader
 
--select * from #exec

select distinct--Subfund
[Trader Id] 
--, [Clear Agent]
, Strategy
, Id
, stuff((select ',' +  t2.[Exec Broker]
		from #exec t2 --where t1.Subfund = t2.Subfund
		where t1.[Trader Id] = t2.[Trader Id] --and t1.[Clear Agent] = t2.[Clear Agent]
		and t1.Strategy = t2.Strategy and t1.Id = t2.Id 
		for xml path('')),1,1,'') ExecBroker
into #tmpExecBroker
from #exec t1
group by --Subfund
 [Trader Id] 
--, [Clear Agent]
, Strategy
, Id

	create table #tmpP1TC
	(
		sid int
		, lotSizeIncrement int
		, defaultLotSizeIncrement int
		, irsDiscountFactor int
		, uirsDiscountFactor int
		, tid int
	)
	
	declare @sql varchar (max)
	set @sql = 'select
					si.sid,
					s.lotSizeIncrement,
					spt.defaultLotSizeIncrement,
					irsDiscountFactor = irs.discountFactor,
					uirsDiscountFactor = uirs.discountFactor,
					tid = cast(si.code as int)
				from FTBTrade.dbo.[SecurityIdentifier] si
					join FTBTrade.dbo.[Security] s on s.sid = si.sid
					join FTBTrade.dbo.[SecurityPrimaryType] spt on s.securityPrimaryTypeId = spt.securityPrimaryTypeId
					join FTBTrade.dbo.[Security] us on s.underlying_sid = us.sid
					left join FTBTrade.dbo.[InterestRateSwap] irs on irs.sid = s.sid
					left join FTBTrade.dbo.[InterestRateSwap] uirs on uirs.sid = us.sid
				where si.identifierTypeId = 8' -- and si.code in (' + @livePositions + ')'
      
	set @sql  = N' 
				insert into #tmpP1TC
				select distinct
					sid
					, lotSizeIncrement
					, defaultLotSizeIncrement
					, irsDiscountFactor
					, uirsDiscountFactor
					, tid
				from OPENQUERY([P1G2], ''' +  @sql + ''') 
		 '
	      
	exec (@sql)
	
	SELECT 
		apnl.[Trader]
		,apnl.[Fund]
		,apnl.[Strategy]
		,apnl.[Type]
		,apnl.[Instrument Description]
		,apnl.[Id]
		,apnl.[Symbol]       
		,apnl.[Inst Ccy]
		,apnl.[Value Date]
		,apnl.[End Qty]
		,apnl.[End Price [N]]]
		,apnl.[Clear Broker]
		,[LotSize] = case when coalesce(p.lotSizeIncrement,p.defaultLotSizeIncrement, 1) < 1 then 1 else coalesce(p.lotSizeIncrement, p.defaultLotSizeIncrement, 1) end 
		,[PV] = apnl.[End Qty]
		,[FV] = apnl.[End Qty] / case when coalesce(p.irsDiscountFactor, p.uirsDiscountFactor, 1) = 0 then 1 else coalesce(p.irsDiscountFactor, p.uirsDiscountFactor, 1) end
		,r1.[FIG_ESP_$]
		,r1.[FIG_Dv01_$]
		,r1.[FIG_Cr01_$]
		,[sid] = p.[sid]
		,ExecBroker = case when eb.ExecBroker is null then '-' else eb.ExecBroker end
	FROM [y_assets].[dbo].[ActivePnL] apnl
		join #tmpP1TC p on p.tid = apnl.Id
		left join #traders t on t.name = apnl.Trader
		left join #tmpReporting1 r1 on apnl.Id = r1.Value10
			and apnl.Fund = r1.Legal_Entity_Description
			and apnl.Strategy = r1.Acct
			and apnl.[Clear Broker] = r1.Customer
			and apnl.Trader = r1.Trader_Name
		left join #tmpExecBroker eb on apnl.Id = eb.Id 
				and apnl.Strategy = eb.Strategy
				and t.trader_id = eb.[Trader Id]
	where apnl.Trader in (select * from #tmpTrader)
	and p.[sid] not in (2000004051, 108406583, 108779036)
	order by
		apnl.[Trader]
		,apnl.[Fund]
		,apnl.[Strategy]
		,apnl.[Id]
	
	drop table #tmpTrader
	drop table #tmpExec
	drop table #exec
	drop table #tmpExecBroker
	drop table #tmpReporting1
	drop table #tmpP1TC

END
