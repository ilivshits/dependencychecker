﻿create proc [FIG].[SendAllocationSplitEmail]
	@OrderId int = 0,
	@InstrumentType varchar(100) = null,
	@EmailAddress varchar(500) = 'tyler.weltz@omnivistasolutions.com;',--'ayesha.muzaffar@omnivistasolutions.com;',
	@debug int = 0
	
AS
BEGIN

DECLARE @COUNT int, @mailErr varchar(2000)
exec [FIG].[sp_getp1tcsecuritytypefororder]  @OrderId, @COUNT output
print @COUNT
	       
IF @COUNT  = 0
  BEGIN
	       
		select 0 as IsProcessed,*,0 as TotalQuantity,
		-- if you want to send seperate email for below combination
		-- RANK() OVER (Order by OrderId,ID ,Trader,ExecBroker,PrimeBroker,Strategy) as TmpOrderId,
		
		-- one email per order, exec broker
		RANK() OVER (Order by OrderId,ExecBroker) as TmpOrderId,
		ROW_NUMBER() OVER ( partition by OrderId,ID ,Trader,ExecBroker,PrimeBroker,Strategy Order by OrderId,ID ,Trader,ExecBroker,PrimeBroker,Strategy,Fund) as TmpFillId
		into #tmpAllOrders
		from fig.v_crd_orders where OrderId = convert(varchar, @OrderId)
		
		select @InstrumentType = A.AexeoSecType
		from 
		(select top 1 sm.AexeoSecType from [fig].[CRDToAexeoSectypeMap] sm
			join #tmpAllOrders tmpo on tmpo.CRDSecCode collate Latin1_General_BIN2 = sm.CRDSecType collate Latin1_General_BIN2
			
		)A

		update #tmpAllOrders set TotalQuantity = b.Quantity
		from #tmpAllOrders a
		join (select TmpOrderId,sum(Quantity) as Quantity from #tmpAllOrders group by TmpOrderId) b on a.TmpOrderId = b.TmpOrderId
		

		select @EmailAddress = addr from 
		(
		select 'EmailIds' as EmailIds, 
		(select  Email + ';'   from [fig].[AllocationConfirm] AMC
			where AMC.Instrument  = @InstrumentType 
			FOR XML PATH('')) as addr
		 )T 

		print 'Email:' + @EmailAddress

		declare @sql varchar(2000)
		declare @EmailSubject varchar(2000)
		declare @bodyHtml nvarchar(max)
		declare @headerHtml nvarchar(max)
		declare @TradeDate varchar(100)
		declare @SettlementDate varchar(100)
		declare @ExecBroker varchar(50)
		declare @ClearBroker varchar(50)

		if @debug = 1
			print @EmailAddress
		
		--select @sql = 'select 0 as IsProcessed,* into #tmpAllOrders from #' + @FundTableName 
		--exec (@sql)

		declare @curRowId int
		select @curRowId = 1

		if @debug = 1
		select 'All Orders',* from #tmpAllOrders


		SET @headerHtml =

			N'<table border="0">' +
			N'<tr><th></th></tr>'


		while (select count(*) from #tmpAllOrders where IsProcessed = 0) > 0 
		BEGIN
			print 'Now Sending emails for RowId: ' + cast(@curRowId as varchar)

			select @EmailSubject = '',@bodyHtml = ''


			select @EmailSubject = 'Fortress ' + case when o.BSSSC = 'B' then 'Buys' else 'Sells ' end + ' ' + cast(o.TotalQuantity as varchar) + ' ' + o.Ticker + '(' + o.SecurityName + ') @'
					 + case when CRDSecCode IN ('CB','GB') then cast( cast(Price as decimal(31,8)) as varchar) else  cast(Price as varchar) end ,
					@TradeDate	= '  Trade Date: ' + convert(varchar(12),isnull(o.TradeDate,''),101),
					@SettlementDate = '  Settle Date: ' + convert(varchar(12),isnull(o.SettleDate,''),101),
					@ExecBroker = '  Exec Broker:  ' + isnull(o.ExecBroker,''),
					@ClearBroker = 'Clearing Broker :' + isnull(PrimeBroker,'')

			from (select top 1 * from #tmpAllOrders where TmpOrderId = @curRowId )o
			


			select @bodyHtml = @headerHtml + @bodyHtml + N'<tr><td> ' + @EmailSubject + N'</td></tr> '
			select @bodyHtml = @bodyHtml + N'<tr><td> ' + @TradeDate + N'</td></tr> '
			select @bodyHtml = @bodyHtml + N'<tr><td> ' + @SettlementDate + N'</td></tr> '
			select @bodyHtml = @bodyHtml + N'<tr><td> ' + @ExecBroker + N'</td></tr> '
			select @bodyHtml = @bodyHtml + N'<tr><td> ' + @ClearBroker + N'</td></tr> <tr><td>'
			select @bodyHtml = @bodyHtml + N'<tr><td> ----------------------------------</td></tr> <tr><td>'


			select  @bodyHtml = @bodyHtml + cast((select
			 convert(varchar,convert(money,convert(numeric(20,0),o.Quantity)),1)	 + ' - ' + f.acct_name + ' ' + isnull(fb.Account,''),''
			from #tmpAllOrders o
			join cs_fund f on o.Fund collate Latin1_General_BIN2  = f.acct_cd collate Latin1_General_BIN2 
			left join [fig].[AllocationFundBrokerAccount] fb on fb.FundAbbrev = f.acct_cd and fb.Broker collate Latin1_General_BIN2 = o.ExecBroker collate Latin1_General_BIN2
			where TmpOrderId = @curRowId 
			FOR XML PATH('tr'), TYPE)
			as nvarchar(max)) 

			select  @bodyHtml = @bodyHtml + '<tr><td>

			<a href="http://ny-gmitrpt1/ReportServer?%2fManagedAccounts%2fCRD+AllocationActivityByOrderId&amp;OrderId=' + cast(@OrderId as varchar) + '&amp;rs%3aParameterLanguage=en-US">Report Link</a></td></tr></table>'

			if  @debug = 1
				select @bodyHtml mailBody,@EmailSubject mailSubject 

			if exists(select 1 from #tmpAllOrders where TmpOrderId = @curRowId )
			BEGIN
				execute fig.[SendEmail] @recipients = @EmailAddress,@subject = @EmailSubject, @body = @bodyHtml	, @body_type = 'HTML', @error_msg = @mailErr OUTPUT
			END

			update #tmpAllOrders set IsProcessed = 1 where TmpOrderId = @curRowId
			select @curRowId = @curRowId + 1

	 	END
 END
END
