
CREATE view [ott].[OtcLegalEntityTransfer] as
select originalOrderId = isnull(o1.originalOrderId, o1.orderId)
      , originalAllocationId = isnull(a1.originalAllocationId, a1.allocationId)
      , oldOrderId = a1.orderId
      , oldAllocationId = a1.allocationId
      , oldEventId = e1.otcEventId
      , oldBlockId = e1.blockId
      , oldLegalEntityId = a1.legalEntityId
      , newOrderId = a2.orderId
      , newAllocationId = a2.allocationId
      , newLegalEntityId = a2.legalEntityId
      , r.opsTransferRunId

from [OpsTransferRun] r
join [OpsTransferProperty] p on r.opsTransferRunId = p.opsTransferRunId
      and p.[key] in ('OutputParameters.FundId', 'OutputParameters.LegalEntityId')
join [OpsTransferNode] n on r.opsTransferRunId = n.opsTransferRunId

join [OpsTransferLink] l1 on n.opsTransferNodeId = l1.opsTransferNodeId
join [OrderOTCEvent] e1 with(nolock) on l1.toOrderId = e1.orderId and l1.toOtcEventId = e1.otcEventId
join [OperationalInfo] eoi on e1.orderId = eoi.orderId and e1.blockId = eoi.blockId 
	  and eoi.note like '%Liquidating OTC event block %'
join [OrderAllocation] a1 with(nolock) on e1.orderId = a1.orderId and e1.allocationId = a1.allocationId
join [Order] o1 with(nolock) on e1.orderId = o1.orderId

join [OpsTransferLink] l2 on n.opsTransferNodeId = l2.opsTransferNodeId
join [Order] o2 with(nolock) on l2.toOrderId = o2.orderId and l2.toOtcEventId = 0
join [OperationalInfo] ooi on ooi.orderId = o2.orderId and ooi.blockId = 0
      and ooi.note like '%Reinitiating order %'
join [OrderAllocation] a2 with(nolock) on o2.orderId = a2.orderId

where a1.clearBrokerId = a2.clearBrokerId
      and a1.strategyId = a2.strategyId
      and a1.legalEntityId != a2.legalEntityId
      and o1.sid = o2.sid
      and o2.orderStatusId = 2



