﻿
--[p1tc].[spGetRestrictedOrders_Floyd] @restrictions =  '1, 2, 3, 4, 5'
--[p1tc].[spGetRestrictedOrders_Floyd] @fromDate  = '2013-02-28',@toDate  = '2013-03-01'


CREATE PROCEDURE [p1tc].[spGetRestrictedOrders] --@fromDate = '2013-02-28',   @toDate = '2013-03-01' 
		@fromDate datetime = '2013-03-03',
		@toDate datetime = '2013-03-04',
		@restrictions varchar (100) = null

		
AS
BEGIN
	SET NOCOUNT ON;
	
--declare	@fromDate datetime
--declare	@toDate datetime
--set	@fromDate  = '20130101'
--set	@toDate  = '20130131'
--declare @restrictions varchar (500)
--set @restrictions =  '1, 2, 3'



declare @q nvarchar(4000)
declare @OPENQUERY nvarchar(4000)
declare @LinkedServer nvarchar(4000)
set @LinkedServer = '[P1TC]'
  

set @q = '	
	select  distinct
		manager = rtrim(ltrim(m.description)) 
		, a.object
		, o.tradeDate
		, a.startDate
		, a.endDate
		, o.orderId
		, a.securityPrimaryType
		, orderStatus = os.code
		, a.sid
		, a.securityDescription
		, a.issuer
		, ticker
		, strategy = st.code
		, a.restrictionType
		, a.restrictionTypeId
		, a.restrictionDescription
		, orderSide = osd.description

	from 	
	(

	select 
		[object] = ''''Issuer''''
		,	ir.issuerId
		, s.sid
		, s.ticker
		, securityDescription = s.description
		, ir.restrictionTypeId
		, restrictionType = rt.code
		, restrictionDescription = rt.description
		, s.securityPrimaryTypeId
		, securityPrimaryType = spt.description
		, issuer = i.description
		, startDate = ir.startDate
		, endDate = ir.endDate
	from vIssuerRestriction ir
		join Security s on s.issuerId = ir.issuerId
		join SecurityPrimaryType spt on spt.securityPrimaryTypeId = s.securityPrimaryTypeId
		join Issuer i on i.issuerId = ir.issuerId
		join RestrictionType rt on rt.restrictionTypeId = ir.restrictionTypeId
				
	union all

	select 
		[object] = ''''Security''''
		,	s.issuerId
		, s.sid
		, s.ticker
		, securityDescription = s.description
		, sr.restrictionTypeId
		, restrictionType = rt.code
		, restrictionDescription = rt.description
		, s.securityPrimaryTypeId
		, securityPrimaryType = spt.description
		, issuer = i.description
		, startDate = sr.startDate
		, endDate = sr.endDate
	from SecurityRestriction sr
		join Security s on s.sid = sr.sid
		join SecurityPrimaryType spt on spt.securityPrimaryTypeId = s.securityPrimaryTypeId
		join Issuer i on i.issuerId = s.issuerId
		join RestrictionType rt on rt.restrictionTypeId = sr.restrictionTypeId
		
	) as a
		join [Order] o on a.sid = o.sid
		join [OrderStatus] os on os.orderStatusId = o.orderStatusId
		join [OrderSide] osd on osd.orderSideId = o.orderSideId
		join Manager m on m.managerId = o.managerId
		join OrderAllocation oa on oa.orderId = o.orderId
		join Strategy st on st.strategyId = oa.strategyId
		where o.orderStatusId <> 6
		and m.description not like ''''ZZ-%''''
	'
		set @q = @q + ' and o.tradeDate >=  cast(''''' + CONVERT(varchar, @fromDate, 112) + ''''' as datetime) '
		set @q = @q + ' and o.tradeDate <= cast(''''' + CONVERT(varchar, @toDate, 112) + ''''' as datetime) '
		set @q = @q + ' and  o.tradeDate <= a.endDate '
		set @q = @q + ' and  o.tradeDate >= a.startDate '


	if (@restrictions is not null)
	set @q = @q + ' and a.restrictionTypeId in ('+ @restrictions + ')'
	
set @OPENQUERY = 'SELECT * FROM OPENQUERY(' + @LinkedServer + ','''+ @q + ''')'

print @OPENQUERY

EXEC sp_executesql  @OPENQUERY
--where rt.rtlApplicableFlag = 1

END

