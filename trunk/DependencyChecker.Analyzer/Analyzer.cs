﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using DependencyChecker.Analyzer.Interfaces;
using DependencyChecker.Collector;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Common;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Events;
using DependencyChecker.Entities.Extensions;
using DependencyChecker.Entities.Interfaces;
using DependencyChecker.Entities;
using log4net.DateFormatter;

namespace DependencyChecker.Analyzer
{
    public class Analyzer : IAnalyzer
    {
        private readonly List<IEntity> _entities;
        private readonly List<IDataProvider> _dataProviders;
        private readonly Dictionary<string, Dictionary<Guid, List<IEntity>>> _namesHashTable =
            new Dictionary<string, Dictionary<Guid, List<IEntity>>>();
        private readonly Dictionary<string, Dictionary<string, IEntity>> _idHashTable =
            new Dictionary<string, Dictionary<string, IEntity>>();
        private readonly Dictionary<string, List<IEntity>> _missingHashTable = new Dictionary<string, List<IEntity>>();
        private readonly List<DatabaseEntity> _aliases;
        private readonly IResolverFactory _resolverFactory;
        private int _detected;
        private int _unDetected;

        private DateTime? _lastRunTime = null;

        public event EventHandler<DependencyCheckerProgressEventArgs> Progress;
        protected virtual void OnProgress(DependencyCheckerProgressEventArgs e)
        {
            Progress?.Invoke(this, e);
        }

        public Analyzer(List<IEntity> rootsList, List<IDataProvider> dataProviders, IResolverFactory resolverFactory)
        {
            _entities = rootsList;
            _dataProviders = dataProviders;
            _resolverFactory = resolverFactory;
            _aliases = new List<DatabaseEntity>();
            foreach (var dataProvider in dataProviders)
            {
                _idHashTable.Add(dataProvider.Name, new Dictionary<string, IEntity>());
            }
        }

        private void FillHashTable(IEnumerable<IEntity> entities)
        {
            if (entities == null)
            {
                return;
            }
            foreach (var entity in entities)
            {
                var databaseEntity = entity as DatabaseEntity;
                if (databaseEntity != null && !String.IsNullOrEmpty(databaseEntity.FunctionAlias))
                {
                    _aliases.Add(databaseEntity);
                }
                if (entity.Type == EntityType.Server || entity.Type == EntityType.Database)
                {
                    LoggerHelper.Log.InfoFormat("Hashing '{0}'", entity.FullName);
                }
                var scopeId = entity.Scope == null ? Guid.Empty : entity.Scope.Id;
                foreach (var searchName in entity.SearchNames)
                {
                    var hashKey = searchName.Trim().ToLowerInvariant();
                    if (_namesHashTable.ContainsKey(hashKey))
                    {
                        if (_namesHashTable[hashKey].All(d => d.Key != scopeId))
                        {
                            _namesHashTable[hashKey].Add(scopeId, new List<IEntity> { entity });
                        }
                        else if (_namesHashTable[hashKey][scopeId].All(d => d.Id != scopeId))
                        {
                            _namesHashTable[hashKey][scopeId].Add(entity);
                        }
                    }
                    else
                    {
                        _namesHashTable.Add(hashKey, new Dictionary<Guid, List<IEntity>> { { scopeId, new List<IEntity> { entity } } });
                    }
                }
                if (!String.IsNullOrEmpty(entity.InternalId))
                {
                    //_idHashTable[entity.DataProvider].Add(entity.InternalId, entity);
                }
                FillHashTable(entity.ScopeChildren);
            }
        }

        private void DetectDependencies(List<IEntity> entities)
        {
            var p = 0;
            var step = entities.Count / 100.0;
            var lastP = 0;
            foreach (var entity in entities)
            {
                AnalyzeEntity(entity);
                //if (entity.ScopeChildren != null)
                //{
                //    DetectDependencies(entity.ScopeChildren);
                //}
                if (lastP >= (int)(++p / step))
                {
                    continue;
                }
                lastP = (int)(p / step);
                var message = String.Format("Cycle #{1}. Analyzed {0}% so far...", lastP, _cycles + 1);
                OnProgress(new DependencyCheckerProgressEventArgs
                {
                    Message = message,
                    Progress = Math.Min(((lastP * 0.3f) + _cycles * 33.33f) / 100.0f, 1.0f)
                });
                LoggerHelper.Log.Info(message);
                GC.Collect();
            }
        }

        public void AnalyzeEntity(IEntity entity)
        {
            var sw = new Stopwatch();
            if (entity.IsAnalyzed && !entity.HasUnresolved)
            {
                return;
            }
            if (entity.Type == EntityType.Server || entity.Type == EntityType.Database)
            {
                LoggerHelper.Log.DebugFormat("Analysing '{0}'", entity.FullName);
            }
            if (entity.Type == EntityType.LinkedServer)
            {
                return;
            }
            var scopeId = entity.Scope == null ? Guid.Empty : entity.Scope.Id;
            if (entity.SearchContent.Count == 0)
            {
                return;
            }
            try
            {
                if (entity.Type == EntityType.DataSource)
                {
                    LoggerHelper.Log.DebugFormat("Analyzing data source {0} with content: {1}", entity.FullName, entity.SearchContent.First(c => c.ContentType == EntityProviderType.SQL).Text);
                }

                foreach (var contentSet in entity.SearchContent.GroupBy(c => c.ContentType))
                {
                    var provider = _dataProviders.Single(p => p.Name == contentSet.Key.ToString());
                    
                    foreach (var content in contentSet)
                    {
                        if (!String.IsNullOrEmpty(content.ScopeName) && content.Scope == null)
                        {
                            if (_namesHashTable.ContainsKey(content.ScopeName.Trim().ToLowerInvariant()))
                            {
                                var scopes = _namesHashTable[content.ScopeName.Trim().ToLowerInvariant()]
                                    .ToDictionary(d => d.Key, d => d.Value).SelectMany(d => d.Value).ToList();
                                if (scopes.Count == 1)
                                {
                                    content.Scope = scopes.Single();
                                }
                                else
                                {
                                    entity.AddError(EntityErrorCode.DependencyError,
                                        String.Format("Cannot detect scope '{0}' for entity {1}. Possible scopes:\r\n{2}",
                                        content.ScopeName, entity.FullName,
                                        String.Join("\r\n", scopes.Select(s => s.FullName).ToArray())));
                                }
                            }
                        }

                        //if (contentSet.Key == EntityProviderType.OLAP)
                        //{
                        //    LoggerHelper.Log.DebugFormat("Parsing OLAP content: '{0}' for entity {1}", content.Text, entity.FullName);
                        //}
                        if (entity.DependsOn.Any(d => d.SearchNames.Any(s => s.Equals(content.Text, StringComparison.InvariantCultureIgnoreCase))))
                        {
                            continue;
                        }
                        if (_namesHashTable.ContainsKey(content.Text.Trim().ToLowerInvariant()))
                        {
                            var dependents = _namesHashTable[content.Text.Trim().ToLowerInvariant()]
                                .Where(d => d.Value.All(v => v.Type != EntityType.JobStep) &&
                                            d.Value.All(v => v.DataProvider == provider.Name))
                                .ToDictionary(d => d.Key, d => d.Value);

                            if (entity.Type == EntityType.DataSource)
                            {
                                LoggerHelper.Log.DebugFormat("data source {0}: key {1} found in hash. Matches: {2}",
                                    entity.FullName, entity.SearchContent.First(c => c.ContentType == EntityProviderType.SQL).Text,
                                    String.Join("; ", dependents.SelectMany(d => d.Value.Select(dd => dd.FullName)).ToArray()));
                            }

                            if (dependents.Count > 1)
                            {
                                bool added;
                                var candidates = dependents.SelectMany(d => d.Value).ToList().RemoveDuplicates();
                                var resolved = _resolverFactory.GetResolver(provider.Name)
                                    .TryResolve(new EntityMatch(content.Text), candidates, entity, content, out added);

                                if (!resolved && !entity.IsAnalyzed)
                                {
                                    entity.AddErrorFormat(EntityErrorCode.DependencyError,
                                        "Multiple matches for '{0}' in '{2}' (possible matches: {1}) not resolved...",
                                        content.Text,
                                        String.Join("; ", candidates.Select(dd => dd.FullName).ToArray()),
                                        entity.FullName);
                                    entity.AddDependency(new DatabaseEntity(content.Text, EntityType.Unresolved));
                                    entity.HasUnresolved = true;
                                    _unDetected++;
                                }
                                else if (resolved && added)
                                {
                                    _detected++;
                                }
                            }
                            else if (dependents.Count == 1)
                            {
                                foreach (var dependent in dependents.Single().Value)
                                {
                                    if (entity.AddDependency(dependent, new EntityMatch(content.Text)))
                                    {
                                        _detected++;
                                    }
                                }
                            }
                            else
                            {
                                entity.AddErrorFormat(EntityErrorCode.DependencyWarning, "Element with name '{0}' is not found", content.Text.Trim());
                            }
                        }
                        else
                        {
                            
                            LoggerHelper.Log.DebugFormat("parsing: '{0}'", entity.FullName);
                            content.ProcessVariables();

                            sw.Restart();

                            var textMatches = provider.MatchItems(content.Text, entity);

                            //if (matches.Count == 0)
                            //{
                            //    entity.AddErrorFormat(EntityErrorCode.ParsingError, "Entity: {1} [{2}]. Content '{0}' is not found in dictionary",
                            //        content.Text, entity.FullName, entity.TypeName);
                            //    _unDetected++;
                            //}
                            foreach (var textMatch in textMatches)
                            {
                                if (_namesHashTable.ContainsKey(textMatch.MatchText))
                                {
                                    if (
                                        entity.DependsOn.Any(
                                            d =>
                                                d.SearchNames.Any(
                                                    s =>
                                                        s.Equals(textMatch.MatchText,
                                                            StringComparison.InvariantCultureIgnoreCase))))
                                    {
                                        continue;
                                    }
                                    var dependents = _namesHashTable[textMatch.MatchText]
                                        .Where(d => d.Value.All(v => v.Type != EntityType.JobStep) &&
                                                    d.Value.All(v => v.DataProvider == provider.Name))
                                        .ToDictionary(d => d.Key, d => d.Value);

                                    if (entity.Type == EntityType.DataSource)
                                    {
                                        LoggerHelper.Log.DebugFormat("data source {0}: key {1} found in hash. Matches: {2}",
                                            entity.FullName, entity.SearchContent.First(c => c.ContentType == EntityProviderType.SQL).Text,
                                            String.Join("; ",
                                                dependents.SelectMany(d => d.Value.Select(dd => dd.FullName))
                                                    .ToArray()));
                                    }

                                    if (entity.Type == EntityType.DataSource)
                                    {
                                        LoggerHelper.Log.DebugFormat("data source {0}: key {1} found in hash. Matches: {2}",
                                            entity.FullName, entity.SearchContent.First(c => c.ContentType == EntityProviderType.SQL).Text,
                                            String.Join("; ",
                                                dependents.SelectMany(d => d.Value.Select(dd => dd.FullName))
                                                    .ToArray()));
                                    }

                                    if (dependents.Count > 1)
                                    {
                                        //LoggerHelper.Log.WarnFormat("Multiple matches for '{0}' in dictionary: {1}. Trying to resolve...",
                                        //    match.Groups["name"].Value,
                                        //    String.Join("; ",
                                        //        dependents.SelectMany(d => d.Value.Select(dd => dd.FullName)).ToArray()));


                                        var candidates = dependents.SelectMany(d => d.Value).ToList().RemoveDuplicates();
                                        bool added;
                                        var resolved = _resolverFactory.GetResolver(provider.Name)
                                            .TryResolve(textMatch, candidates, entity, content, out added);

                                        if (!resolved && !entity.IsAnalyzed)
                                        {
                                            entity.AddErrorFormat(EntityErrorCode.DependencyError,
                                                "Multiple matches for '{0}' in '{2}' (possible matches: {1}) not resolved...",
                                                textMatch,
                                                String.Join("; ",
                                                    candidates.Select(dd => dd.FullName).ToArray()),
                                                entity.FullName);

                                            entity.AddDependency(new DatabaseEntity(textMatch.MatchText, EntityType.Unresolved), textMatch);
                                            entity.HasUnresolved = true;
                                            _unDetected++;
                                        }
                                        else if (resolved && added)
                                        {
                                            _detected++;
                                        }
                                    }
                                    else if (dependents.Count == 1)
                                    {
                                        foreach (var dependent in dependents.Single().Value)
                                        {
                                            // Checking if the match was found on a remote server, but the match scope doesn't contein remote server name
                                            if (dependent.DataProvider == entity.DataProvider && 
                                                dependent.Root != entity.Root &&
                                                textMatch.ScopeText.Split(new[] {entity.ScopeDelimiter},
                                                    StringSplitOptions.RemoveEmptyEntries).Length != dependent.Level)
                                            {
                                                entity.AddErrorFormat(EntityErrorCode.DependencyError,
                                                    "Possible non-exisitng object found. {0} is defined only on server '{1}'",
                                                    textMatch, dependent.Root.FullName);
                                            }
                                            else
                                            {
                                                if (entity.AddDependency(dependent, textMatch))
                                                {
                                                    _detected++;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        entity.AddErrorFormat(EntityErrorCode.DependencyWarning,
                                            "Element with name '{0}' is not found", textMatch);
                                    }
                                    //foreach (var dependent in dependents.Where(d => d.Key == scopeId || d.Key == Guid.Empty).SelectMany(d => d.Value))
                                }
                                else
                                {
                                    if (_missingHashTable.ContainsKey(textMatch.MatchText))
                                    {
                                        if (_missingHashTable[textMatch.MatchText].All(e => e.Id != entity.Id))
                                        {
                                            _missingHashTable[textMatch.MatchText].Add(entity);
                                        }
                                    }
                                    else
                                    {
                                        _missingHashTable.Add(textMatch.MatchText, new List<IEntity> { entity });
                                    }
                                }
                            }
                        }
                    }
                }

                //if (entity.SearchContent.ContainsKey(EntityProviderType.SQL) &&
                //    entity.DependsOn.Any(d => _aliases.Any(a => a.Id == d.Id)))
                //{
                //    foreach (var content in entity.SearchContent[EntityProviderType.SQL])
                //    {
                //        //TODO: Replace matches
                //        if (_aliases.Any(a => content.Text.ToLower().Contains(a.Name)))
                //        {
                //            content.Text.Replace()
                //            _unDetected++;
                //        }
                //    }
                //}

                entity.IsAnalyzed = true;
            }
            catch (Exception e)
            {
                entity.AddErrorFormat(EntityErrorCode.AnalysisError, "Exception during analysis of {0}: {1}", entity.FullName, e);
            }
        }

        private List<string> _propertyFunctionsResults = new List<string>()
        {
            "[dbo].[fnGetSqlNativeDllDatabaseName]",
            
        };

        #region Implementation of IAnalyzer

        public void Analyze()
        {
            LoggerHelper.Log.InfoFormat("Analysis started...");
            LoggerHelper.Log.InfoFormat("Building hash...");
            _lastRunTime = DateTime.Now;
            OnProgress(new DependencyCheckerProgressEventArgs
            {
                Message = "Building hash...",
                Progress = 0
            });


            Regex comments = new Regex(@"\/\*(.*?)\*\/", RegexOptions.Singleline | RegexOptions.Compiled);
            Regex onelineComments = new Regex(@"\-\-.*\r\n", RegexOptions.Compiled);
            Regex allComments = new Regex("\\-\\-[^\\n]*\\n", RegexOptions.IgnoreCase | RegexOptions.Compiled);
            DatabaseEntity dbEntity = _entities.Single(e => e is DatabaseEntity) as DatabaseEntity;

            foreach (var func in _propertyFunctionsResults)
            {
                string def; //_entities.Single(e => e.FullName.ToLower().EndsWith(func.ToLower())).Definition;

                
                    def = dbEntity.FindFirstByFullNamePart(func.Replace("[", "").Replace("]",""))?.Definition;
                if (def == null)
                {
                    continue;
                }

                def = comments.Replace(onelineComments.Replace(def, String.Empty).Replace("''", "")
                                                                                                    .Replace("**", "*")
                                                                                                    .Replace("/**", "/*")
                                                                                                    .Replace("**/", "*/"), String.Empty);

                def = allComments.Replace(def, String.Empty);
                string returnVal = Regex.Match(def, @"(?<=begin\s+return\s+\')\S+(?=\')", RegexOptions.IgnoreCase).Value;


                DatabaseEntity.PropertyFunctionsResults.Add(func, returnVal);
            }


            try
            {
                FillHashTable(_entities);
                File.WriteAllLines(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ailases.txt"), _aliases.Select(a => a.FullName).ToArray());
            }
            catch (Exception ex)
            {
                LoggerHelper.Log.ErrorFormat("Exception occured: {0}", ex);
                throw;
            }

            

            LoggerHelper.Log.InfoFormat("Building queue...");
            var listToProcess = BuildProcessList(_entities);
            listToProcess = listToProcess.OrderByDescending(i => i.Priority).ToList();

            LoggerHelper.Log.InfoFormat("Detecting dependencies...");
            _cycles = 0;
            do
            {
                _detected = 0;
                _unDetected = 0;
                DetectDependencies(listToProcess);
                _cycles++;
                LoggerHelper.Log.InfoFormat("Cycle {0} completed (max = 3). Found {1} dependencies. Not detected: {2}...", _cycles, _detected, _unDetected);
            } while ((_detected > 0 || _unDetected == 0) && _cycles < 3);

            LoggerHelper.Log.InfoFormat("Analysis completed in {0} cycles", _cycles);

            foreach (var missing in _missingHashTable)
            {
                if (IsMarkedForRemoval(missing.Key))
                {
                    continue;
                }
                if (missing.Key.Length < 5)
                {
                    continue;
                }
                LoggerHelper.Log.DebugFormat("Object '{0}' was not found in scopes '{1}'. Detected in body of:\r\n'{2}'",
                    missing.Key,
                    String.Join("; ", missing.Value.Select(e =>
                    {
                        var entity = e as DatabaseEntity;
                        if (entity != null && entity.Database != null)
                        {
                            return entity.Database.FullName + " (Database)";
                        }
                        return e.Root.FullName + " (" + e.Root.TypeName + ")";
                    }).Distinct().ToArray()),
                    String.Join("\r\n", missing.Value.Select(e => " - " + e.FullName).ToArray()));
                missing.Value.ForEach(e =>
                {
                    if (e.DataProvider != EntityProviderType.SQL.ToString() ||
                        (!missing.Key.StartsWith("[#") && !missing.Key.StartsWith("#")))
                    {
                        e.AddErrorFormat(EntityErrorCode.MissingObjectInDefinition,
                            "Element {0} uses undefined element {1}",
                            e.FullName, missing.Key);
                    }
                });
            }

            var sb = new StringBuilder();
            _missingHashTable.Keys.ToList().ForEach(k =>
            {
                if (IsMarkedForRemoval(k))
                {
                    sb.Append("CLEANUP: ");
                }
                else if (k.Length < 5)
                {
                    sb.Append("**SMALL: ");
                }
                sb.Append(k + "\r\n");
            });

            LoggerHelper.Log.InfoFormat("Entities not found:\r\n{0}", sb);

            GC.Collect();
        }

        private static bool IsMarkedForRemoval(string name)
        {
            return name.IndexOf("~IGNORE~", StringComparison.InvariantCultureIgnoreCase) >= 0;
        }

        public void GenerateCleanupByNames(List<string> filter)
        {
            if (!Directory.Exists(CleanupPath))
            {
                Directory.CreateDirectory(CleanupPath);
            }
            if (filter.Count > 0)
            {
                var entities = _missingHashTable.Where(k => filter.Contains(k.Key.ToLowerInvariant()) && !IsMarkedForRemoval(k.Key)).SelectMany(h => h.Value).Distinct().ToList();

                PrintCleanup("names_" + DateTime.Now.ToString("hh-mm"), entities);
            }
            else
            {
                foreach (var missing in _missingHashTable.Where(missing => !IsMarkedForRemoval(missing.Key)).Where(missing => missing.Key.Length >= 5))
                {
                    PrintCleanup(missing.Key, missing.Value);
                }
            }
        }

        public void GenerateCleanupByPatterns(List<string> filter)
        {
            var matchingHash = _namesHashTable.Keys.Where(k => filter.Any(f => Regex.IsMatch(k, f, RegexOptions.IgnoreCase)) && !IsMarkedForRemoval(k)).ToList();
            var entitiesResult = new List<IEntity>();

            foreach (var entityKey in matchingHash)
            {
                foreach (var result in _namesHashTable[entityKey].SelectMany(d => d.Value))
                {
                    if (!entitiesResult.Contains(result))
                    {
                        if (GetUsages(false, result.UsedByRecursive, SearchDrillthroughTypes).Count == 0)
                        {
                            entitiesResult.Add(result);
                            LoggerHelper.Log.InfoFormat("Matched for cleanup: {0}", result.FullName);
                        }
                        else
                        {
                            LoggerHelper.Log.WarnFormat("USED OBJECT NOT Matched for cleanup: {0}", result.FullName);
                        }
                    }
                }
            }
            PrintCleanup("pattern_" + DateTime.Now.ToString("hh-mm"), entitiesResult);
        }
        
        /// <summary>
        /// Retrieves all dependencies/usages of an entities list for specific types
        /// </summary>
        /// <param name="downDirectionFlag">Specifies whether usages or dependencies should be scanned. True value scans dependencies, fals - usages</param>
        /// <param name="elements">initial list for scan</param>
        /// <param name="matchingTypes">types on which scan should be stopped</param>
        /// <param name="gen">generation - internal purpose variable for preventinv stack overflow issue</param>
        /// <returns></returns>
        public static List<IEntity> GetUsages(bool downDirectionFlag, IEnumerable<IEntity> elements, List<EntityType> matchingTypes = null, int gen = 0)
        {
            if (gen == 20)
            {
                LoggerHelper.Log.Warn("Failed to get usages list, Max loop reached");
                return new List<IEntity>();
            }

            //if (matchingTypes == null || matchingTypes.Count == 0)
            //{
            //    matchingTypes = SearchDrillthroughTypes;
            //}

            var result = new List<IEntity>();
            foreach (var element in elements)
            {
                if (result.All(r => r.Id != element.Id) && (matchingTypes == null || matchingTypes.Any(t => element.Type == t)))
                {
                    LoggerHelper.Log.Warn("Failed to get usages list, Max loop reached");
                    result.Add(element);
                }
                result.AddRange((downDirectionFlag ? element.DependsOnRecursive : element.UsedByRecursive).Where(i => result.All(r => r.Id != i.Id) &&
                    (matchingTypes == null || matchingTypes.Any(t => i.Type == t))));
                var nextGen = (downDirectionFlag ? element.DependsOnRecursive : element.UsedByRecursive)
                                    .Where(i => (matchingTypes == null || matchingTypes.All(t => i.Type != t)) &&
                                                i.Type != EntityType.Server && i.Type != EntityType.Database).ToList();
                if (nextGen.Count > 0)
                {
                    result.AddRange(GetUsages(downDirectionFlag, nextGen, matchingTypes, gen + 1)
                        .Where(i => result.All(r => r.Id != i.Id)));
                }
            }
            return result;
        }

        public IEntity GetEntity(Guid uid)
        {
            var matched = Servers.Select(s => s.FindById(uid)).Where(i => i != null).ToList();
            return matched.SingleOrDefault(e => e != null);
        }

        public List<IEntity> GetEntities(string dbName, EntityType type)
        {
            return Match(new List<string>() { dbName }).Where(e => e.Type == type).ToList();
        }

        private static readonly List<EntityType> SearchDrillthroughTypes = new List<EntityType>
        {
            EntityType.Report,
            EntityType.Job,
            EntityType.Folder,
            EntityType.Trigger,
            EntityType.JobStep,
            EntityType.LinkedReport,
            EntityType.DataSource
        };

        private int _cycles;

        public List<IEntity> GetUnusedEntities(IEntity scope = null, int cyclesLimit = -1)
        {
            var entitiesResult = new List<IEntity>();

            if (scope == null)
            {
                foreach (
                    var result in
                        _namesHashTable.SelectMany(d => d.Value.SelectMany(e => e.Value))
                            .ToList()
                            .Distinct()
                            .Where(i => i.UsedByRecursive.Count == 0))
                {
                    if (!entitiesResult.Contains(result))
                    {
                        if (GetUsages(false, result.UsedByRecursive, SearchDrillthroughTypes).Count == 0)
                        {
                            entitiesResult.Add(result);
                            LoggerHelper.Log.InfoFormat("Matched for cleanup: {0}", result.FullName);
                        }
                        else
                        {
                            LoggerHelper.Log.WarnFormat("USED OBJECT NOT Matched for cleanup: {0}", result.FullName);
                        }
                    }
                }
            }
            else
            {
                int collected, counter = 1;
                do
                {
                    collected = entitiesResult.Count;
                    CollectUnused(scope, entitiesResult);
                    LoggerHelper.Log.WarnFormat("Unused objects cleanup cycle {0} collected {1}", counter++, entitiesResult.Count - collected);
                    if (counter >= cyclesLimit && cyclesLimit > 0)
                    {
                        break;
                    }
                } while (collected != entitiesResult.Count);
            }
            return entitiesResult;
        } 

        public List<String> GenerateCleanupForUnused(IEntity database = null)
        {
            var entities = GetUnusedEntities(database);

            return PrintCleanup("unused_" + DateTime.Now.ToString("hh-mm"), entities);
        }

        public List<string> GenerateCleanup(IEntity database, Guid[] entities)
        {
            var entitiesResult = entities.Select(database.FindById).ToList();

            return PrintCleanup("unused_" + DateTime.Now.ToString("hh-mm"), entitiesResult);
        }

        private void CollectUnused(IEntity entity, ICollection<IEntity> entitiesResult)
        {
            if (entity.ScopeChildren != null)
            {
                foreach (var child in entity.ScopeChildren)
                {
                    if ((child.UsedByRecursive.Count == 0 ||
                         child.UsedByRecursive.All(entitiesResult.Contains)) &&
                        !entitiesResult.Contains(child))
                    {
                        entitiesResult.Add(child);
                        LoggerHelper.Log.InfoFormat("Unused item detected: {0} ", child.FullName);
                    }
                    CollectUnused(child, entitiesResult);
                }
            }
        }

        public void GenerateCleanupForTestItems()
        {

        }

        const string CleanupPathName = "cleanup";

        private static string CleanupPath
        {
            get { return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, CleanupPathName); }
        }

        private List<string> PrintCleanup(string cleanupName, IEnumerable<IEntity> items)
        {
            var createdFiles = new List<string>();
            var folderName = Path.Combine(CleanupPath, DateTime.Now.ToString("yyyy-MM-dd"), cleanupName.Replace("[", String.Empty)
                .Replace("]", String.Empty)
                .Replace("/", "_")
                .Replace("\\", "_"));

            if (!Directory.Exists(folderName))
            {
                Directory.CreateDirectory(folderName);
            }
            foreach (var server in items.OfType<DatabaseEntity>().GroupBy(i => i.Root.Name))
            {
                var serverPath = Path.Combine(folderName, server.Key);
                var relativePath = serverPath.Replace(AppDomain.CurrentDomain.BaseDirectory, String.Empty);

                if (!Directory.Exists(serverPath))
                {
                    Directory.CreateDirectory(serverPath);
                }

                if (!Directory.Exists(Path.Combine(serverPath, "definitions")))
                {
                    Directory.CreateDirectory(Path.Combine(serverPath, "definitions"));
                }

                if (!Directory.Exists(Path.Combine(serverPath, "create")))
                {
                    Directory.CreateDirectory(Path.Combine(serverPath, "create"));
                }
                if (!Directory.Exists(Path.Combine(serverPath, "delete")))
                {
                    Directory.CreateDirectory(Path.Combine(serverPath, "delete"));
                }
                if (!Directory.Exists(Path.Combine(serverPath, "rename")))
                {
                    Directory.CreateDirectory(Path.Combine(serverPath, "rename"));
                }
                if (!Directory.Exists(Path.Combine(serverPath, "rollback")))
                {
                    Directory.CreateDirectory(Path.Combine(serverPath, "rollback"));
                }


                foreach (var database in server.GroupBy(i => i.Database))
                {
                    using (var rollbackWriter = new StreamWriter(Path.Combine(serverPath, "rollback", database.Key.Name + ".sql")))
                    {
                        using (var renameWriter = new StreamWriter(Path.Combine(serverPath, "rename", database.Key.Name + ".sql")))
                        {
                            using (var createWriter = new StreamWriter(Path.Combine(serverPath, "create", database.Key.Name + ".sql")))
                            {
                                using (var deleteWriter = new StreamWriter(Path.Combine(serverPath, "delete", database.Key.Name + ".sql")))
                                {
                                    var usage = String.Format(@"USE [{0}]
", database.Key.Name);

                                    createWriter.WriteLine(usage);
                                    createWriter.WriteLine("GO");
                                    deleteWriter.WriteLine(usage);
                                    renameWriter.WriteLine(usage);
                                    rollbackWriter.WriteLine(usage);

                                    var printed = new List<String>();

                                    foreach (var item in database)
                                    {
                                        if (item.Type != EntityType.Function &&
                                            item.Type != EntityType.StoredProcedure &&
                                            item.Type != EntityType.Table &&
                                            item.Type != EntityType.Synonym &&
                                            item.Type != EntityType.View)
                                        {
                                            LoggerHelper.Log.WarnFormat("Cannot cleanup object {0} of type {1}. Skipped",
                                                item.FullName, item.TypeName);
                                            continue;
                                        }

                                        if (printed.Contains(item.Name))
                                        {
                                            LoggerHelper.Log.WarnFormat("Object {0} (type {1}) already added for cleanup. Skipped",
                                                item.FullName, item.TypeName);
                                            continue;
                                        }

                                        /* Creation build */
                                        createWriter.WriteLine("PRINT 'Creating {1}.{0}...'", item.Name, item.Scope.Name);
                                        createWriter.WriteLine("GO");
                                        createWriter.WriteLine(item.Definition);
                                        createWriter.WriteLine("GO");

                                        /* Rename build */
                                        renameWriter.WriteLine("PRINT 'Renaming {1}.{0}...'", item.Name, item.Scope.Name);
                                        renameWriter.WriteLine("EXEC sp_rename '[{1}].[{0}]', '~IGNORE~{0}'", item.Name,
                                            item.Scope.Name);
                                        renameWriter.WriteLine();

                                        /* Rollback build */
                                        rollbackWriter.WriteLine("PRINT 'Rolling back rename ~IGNORE~{1}.{0}...'", item.Name, item.Scope.Name);
                                        rollbackWriter.WriteLine("EXEC sp_rename '[{1}].[~IGNORE~{0}]', '{0}'", item.Name,
                                            item.Scope.Name);
                                        rollbackWriter.WriteLine();

                                        /* Deletion build */
                                        var objectType = String.Empty;
                                        switch (item.Type)
                                        {
                                            case EntityType.StoredProcedure:
                                                objectType = "PROCEDURE";
                                                break;
                                            case EntityType.View:
                                                objectType = "VIEW";
                                                break;
                                            case EntityType.Function:
                                                objectType = "FUNCTION";
                                                break;
                                            case EntityType.Table:
                                                objectType = "TABLE";
                                                break;
                                            case EntityType.Synonym:
                                                objectType = "SYNONYM";
                                                break;
                                        }

                                        deleteWriter.WriteLine("PRINT 'Deleting {1}.{0}...'", item.Name, item.Scope.Name);
                                        deleteWriter.WriteLine("DROP {0} [{2}].[~IGNORE~{1}]", objectType, item.Name,
                                            item.Scope.Name);

                                        printed.Add(item.Name);

                                        var defFile = item.ExportToFile(Path.Combine(serverPath, "definitions"));
                                        if (!String.IsNullOrEmpty(defFile))
                                        {
                                            createdFiles.Add(defFile.Replace(AppDomain.CurrentDomain.BaseDirectory, String.Empty));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    createdFiles.Add(Path.Combine(relativePath, "rollback", database.Key.Name + ".sql"));
                    createdFiles.Add(Path.Combine(relativePath, "rename", database.Key.Name + ".sql"));
                    createdFiles.Add(Path.Combine(relativePath, "create", database.Key.Name + ".sql"));
                    createdFiles.Add(Path.Combine(relativePath, "delete", database.Key.Name + ".sql"));
                }
            }
            return createdFiles;
        }

        private static List<IEntity> BuildProcessList(IEnumerable<IEntity> entities, List<IEntity> result = null)
        {
            if (result == null)
            {
                result = new List<IEntity>();
            }

            foreach (var entity in entities)
            {
                result.Add(entity);
                if (entity.ScopeChildren != null)
                {
                    BuildProcessList(entity.ScopeChildren, result);
                }
            }
            return result;
        }

        #endregion

        public List<IEntity> Match(List<string> inputNames)
        {
            var result = new List<IEntity>();
            foreach (var name in inputNames)
            {
                if (_namesHashTable.ContainsKey(name.Trim().ToLowerInvariant()))
                {
                    result.AddRange(_namesHashTable[name.Trim().ToLowerInvariant()].SelectMany(d => d.Value));
                }
                else
                {
                    LoggerHelper.Log.ErrorFormat("Provided name '{0}' was not found in dictionary", name);
                }
            }
            return result;
        }

        public List<IEntity> Search(string inputName, bool useRegularExpression = false, bool searchInDefinition = false, bool exactSearch = false)
        {
            LoggerHelper.Log.DebugFormat("Search for '{0}' requested. RegularExpressions: {1}", inputName, useRegularExpression);
            var timer = new Stopwatch();
            timer.Start();

            var result = new List<IEntity>();

            if (!searchInDefinition)
            {
                if (useRegularExpression)
                {
                    var nameEx = new Regex(inputName, RegexOptions.IgnoreCase);
                    foreach (var key in _namesHashTable.Keys.Where(key => nameEx.IsMatch(key)))
                    {
                        result.AddRange(
                            _namesHashTable[key].SelectMany(d => d.Value)
                                .Distinct()
                                .Where(d => result.All(r => r.Id != d.Id)));
                    }
                }
                else
                {
                    if (exactSearch)
                    {
                        if (_namesHashTable.ContainsKey(inputName.ToLowerInvariant()))
                        {
                            result.AddRange(
                                _namesHashTable[inputName.ToLowerInvariant()].SelectMany(d => d.Value)
                                    .Distinct()
                                    .Where(d => result.All(r => r.Id != d.Id)));
                        }
                    }
                    else
                    {
                        var lowerName = inputName.ToLowerInvariant();
                        foreach (var key in _namesHashTable.Keys.Where(key => key.Contains(lowerName)))
                        {
                            result.AddRange(
                                _namesHashTable[key].SelectMany(d => d.Value)
                                    .Distinct()
                                    .Where(d => result.All(r => r.Id != d.Id)));
                        }
                    }
                }
            }
            else
            {
                if (useRegularExpression)
                {
                }
                else
                {
                }
                foreach (var key in _namesHashTable.Keys)
                {
                    result.AddRange(
                        _namesHashTable[key].SelectMany(d => d.Value)
                            .Distinct()
                            .Where(d => result.All(r => r.Id != d.Id
                                    && d.Definition.IndexOf(inputName, StringComparison.InvariantCultureIgnoreCase) >= 0)));
                }
            }
            timer.Stop();
            LoggerHelper.Log.DebugFormat("Search for '{0}' completed. Results found: {2}. Search took: {1}ms", inputName, timer.ElapsedMilliseconds, result.Count);
            return result;
        }

        public List<IEntity> Servers
        {
            get
            {
                return _entities;
            }
        }

        public DateTime? LastRunTime
        {
            get { return _lastRunTime; }
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _entities.Clear();
            _namesHashTable.Clear();
            _idHashTable.Clear();
            _missingHashTable.Clear();
            GC.Collect();
        }

        #endregion
    }
}