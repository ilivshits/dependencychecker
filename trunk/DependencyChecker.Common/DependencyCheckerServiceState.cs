﻿
namespace DependencyChecker.Common
{
    public enum DependencyCheckerServiceState
    {
        Ready = 0,
        LoadingData = 1,
        Analyzing = 2,
        Initializing = 2
    }
}
