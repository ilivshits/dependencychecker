﻿using System;

namespace DependencyChecker.Entities
{
    public enum EntityType
    {
        /* General */
        [Style(Name = "person")]
        Person = 1,

        /* MS SQL */
        [Style]
        Server,
            [Style]
            Database,
                [Style]
                Schema,
                    [Style]
                    Table,
                        [Style]
                        ForeignKey,
                    [Style]
                    View,
                    [Style]
                    StoredProcedure,
                    [Style]
                    Function,
                [Style]
                Trigger,
                [Style]
                Synonym,
            [Style]
            LinkedServer,
            [Style]
            Job,
                [Style]
                JobStep,

        /* SSRS */
        [Style(Name = "info")]
        Folder,
            [Style(Name = "info")]
            Report,
                [Style(Name = "info")]
                LinkedReport,
                [Style(Name = "info")]
                DataSource,
        [Style(Name = "info")]
        Resource,

        /* Analysis server */
        [Style(Name = "warning")]
        Cube,
            [Style(Name = "warning")]
            CalculatedMember,
            [Style(Name = "warning")]
            Perspective,
            [Style(Name = "warning")]
            Dimension,
                [Style(Name = "warning")]
                Hierarchy,
                    [Style(Name = "warning")]
                    Level,
                    [Style(Name = "warning")]
                    CubeAttribute,
            [Style(Name = "warning")]
            MeasureGroup,
                [Style(Name = "warning")]
                Measure,
            [Style(Name = "warning")]
            mdxScript,

        /* Scheduled tasks */
        [Style(Name = "task")]
        ScheduledTask,
            [Style(Name = "task")]
            ScheduledAction,
                [Style(Name = "file")]
                BatchFile,
                [Style(Name = "file")]
                PerlScript,
                [Style(Name = "file")]
                Executable,
                [Style(Name = "dts")]
                DTSX,

        /* Scheduled tasks */
        [Style(Name = "service")]
        WindowsService,

        /* P1TC */
        [Style(Name = "p1tc")]
        P1TCService,
        [Style(Name = "p1tc")]
        P1TCInstance,
            [Style(Name = "p1tc")]
            P1TCEntity,
                [Style(Name = "p1tc")]
                P1TCColumn,
            [Style(Name = "p1tc")]
            P1TCMipsCol,
            [Style(Name = "p1tc")]
            P1TCCalcCol,
            [Style(Name = "p1tc")]
            P1TCLookupTable,
            [Style(Name = "p1tc")]
            P1TCView,
            [Style(Name = "p1tc")]
            P1TCTag,

        /* Unknown */
        [Style(Name = "danger")]
        Unresolved,
    }

    public class StyleAttribute : Attribute
    {
        private String _name;

        public String Name
        {
            set { _name = value; }
            get
            {
                if (String.IsNullOrEmpty(_name))
                {
                    _name = "primary";
                }
                return "label-" + _name;
            }
        }
    }
}
