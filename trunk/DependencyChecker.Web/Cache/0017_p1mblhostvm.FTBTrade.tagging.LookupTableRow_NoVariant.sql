create view [tagging].[LookupTableRow_NoVariant] as
select lookupTableRowId
	, lookupTableId
	, created
	, version
	, lastUpdate
	, application_source
	, system_source
	, _globalVersion

	, inputValue1_Int = i1.value_Int
	, inputValue1_Text = i1.value_Text
	, inputValue1_DateTime = i1.value_DateTime
	, inputValue1_Decimal = i1.value_Decimal

	, inputValue2_Int = i2.value_Int
	, inputValue2_Text = i2.value_Text
	, inputValue2_DateTime = i2.value_DateTime
	, inputValue2_Decimal = i2.value_Decimal

	, inputValue3_Int = i3.value_Int
	, inputValue3_Text = i3.value_Text
	, inputValue3_DateTime = i3.value_DateTime
	, inputValue3_Decimal = i3.value_Decimal

	, inputValue4_Int = i4.value_Int
	, inputValue4_Text = i4.value_Text
	, inputValue4_DateTime = i4.value_DateTime
	, inputValue4_Decimal = i4.value_Decimal

from tagging.LookupTableRow
cross apply tagging.GetNoVariant(inputValue) i1
cross apply tagging.GetNoVariant(inputValue2) i2
cross apply tagging.GetNoVariant(inputValue3) i3
cross apply tagging.GetNoVariant(inputValue4) i4


