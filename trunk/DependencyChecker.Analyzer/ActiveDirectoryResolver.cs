﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DependencyChecker.Analyzer.Interfaces;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Analyzer
{
    public class ActiveDirectoryResolver : IResolver
    {
        #region Implementation of IResolver

        public string ProviderName
        {
            get
            {
                return OlapEntity.ProviderName;
            }
        }

        public bool TryResolve(EntityMatch textMatch, List<IEntity> matches, IEntity entity, SearchContent content, out bool added)
        {
            added = false;
            return false;
        }

        #endregion
    }
}
