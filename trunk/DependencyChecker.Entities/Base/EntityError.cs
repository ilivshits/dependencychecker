﻿using System;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Entities.Base
{
    internal class EntityError
    {
        public EntityErrorCode Code { get; set; }
        public String Message { get; set; }
    }
}