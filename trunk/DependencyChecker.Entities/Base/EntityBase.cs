﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DependencyChecker.Common;
using DependencyChecker.Entities.Interfaces;
using log4net;

namespace DependencyChecker.Entities.Base
{
    public abstract class EntityBase : IEntity
    {
        protected EntityBase(string name, string dataProvider, EntityType type)
            : this(name, String.Empty, dataProvider, type)
        {
            Id = Guid.NewGuid();
        }

        protected EntityBase(string name, string definition, string dataProvider, EntityType type)
        {
            Definition = definition;
            Aliases = new List<EntityAlias>();
            Type = type;
            DataProvider = dataProvider;
            Id = Guid.NewGuid();
            Name = name;
            UsedBy = new List<IEntity>();
            DependsOn = new List<IEntity>();
            IsAnalyzed = false;
            HasUnresolved = false;
            Errors = new List<EntityError>();
        }

        public IEntity FindFirstByFullNamePart(string partName)
        {
            return GetFirstByFullNamePart(this, partName.ToLower());
        }

        private IEntity GetFirstByFullNamePart(IEntity entity, string partName)
        {
            if (entity.FullName.ToLower().EndsWith(partName))
            {
                return entity;
            }

            if (entity.ScopeChildren == null) return null;
            foreach (var childEntity in entity.ScopeChildren)
            {
                var e = GetFirstByFullNamePart(childEntity, partName);
                if (e != null) return e;
            }

            return null;
        }

        #region Database compatibility requirements

        public Guid RootId { get; set; }
        public Guid ParentId { get; set; }

        #endregion

        #region Implementation of IEntity

        public string Name { get; private set; }
        public string Definition { get; protected set; }

        public IEntity Root
        {
            get
            {
                IEntity result = this;
                while (result.Scope != null)
                {
                    result = result.Scope;
                }
                return result;
            }
        }

        

        public int Level
        {
            get
            {
                var result = 0;
                IEntity item = this;
                while (item.Scope != null)
                {
                    item = item.Scope;
                    result++;
                }
                return result;
            }
        }

        private List<IEntity> _parents;
        public List<IEntity> Parents
        {
            get
            {
                if (_parents != null && _parents.Count == Level + 1)
                {
                    return _parents;
                }
                _parents = new List<IEntity>();
                IEntity item = this;
                _parents.Add(item);
                while (item.Scope != null)
                {
                    item = item.Scope;
                    _parents.Add(item);
                }
                _parents.Reverse();
                return _parents;
            }
        }

        protected IEntity _scope;
        public virtual IEntity Scope
        {
            get { return _scope; }
            set
            {
                _scope = value;
                if (_scope.ScopeChildren == null)
                {
                    _scope.ScopeChildren = new List<IEntity>();
                }
                if (_scope.ScopeChildren.All(e => e.Id != Id))
                {
                    _scope.ScopeChildren.Add(this);
                }
            }
        }

        //TODO: Use Scope property only to link entities
        public List<IEntity> ScopeChildren { get; set; }

        public List<IEntity> UsedBy { get; set; }
        public List<IEntity> DependsOn { get; set; }

        public List<IEntity> UsedByRecursive
        {
            get
            {
                var result = new List<IEntity>(UsedBy);
                if (ScopeChildren != null)
                {
                    result.AddRange(GetUsages(ScopeChildren));
                }
                return result;
            }
        }

        private static IEnumerable<IEntity> GetUsages(IEnumerable<IEntity> elements)
        {
            var result = new List<IEntity>();
            foreach (var element in elements)
            {
                result.AddRange(element.UsedBy.Where(i => result.All(r => r.Id != i.Id)));
                if (element.ScopeChildren != null)
                {
                    result.AddRange(GetUsages(element.ScopeChildren).Where(i => result.All(r => r.Id != i.Id)));
                }
            }
            return result;
        }

        public List<IEntity> DependsOnRecursive
        {
            get
            {
                var result = new List<IEntity>(DependsOn);
                if (ScopeChildren != null)
                {
                    result.AddRange(GetDependencies(ScopeChildren));
                }
                return result;
            }
        }

        public IEntity CreatedBy { get; set; }
        public IEntity LastModifiedBy { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }

        public bool IsAnalyzed { get; set; }
        public bool HasUnresolved { get; set; }
        internal List<EntityError> Errors { get; set; }

        private static IEnumerable<IEntity> GetDependencies(IEnumerable<IEntity> elements)
        {
            var result = new List<IEntity>();
            foreach (var element in elements)
            {
                result.AddRange(element.DependsOn.Where(i => result.All(r => r.Id != i.Id)));
                if (element.ScopeChildren != null)
                {
                    result.AddRange(GetDependencies(element.ScopeChildren).Where(i => result.All(r => r.Id != i.Id)));
                }
            }
            return result;
        }

        public virtual String ScopeDelimiter
        {
            get
            {
                return ".";
            }
        }

        public virtual string FullName
        {
            get
            {
                return (Scope == null ? String.Empty : Scope.FullName + ScopeDelimiter) + Name;
            }
        }

        public abstract List<string> SearchNames { get; }


        private List<SearchContent> _searchContent;
        public String[] GetErrors()
        {
            return Errors.Select(e => e.Message).ToArray();
        }

        public void AddError(EntityErrorCode code, String error)
        {
            if (!Errors.Any(e => e.Code == code && e.Message == error))
            {
                Errors.Add(new EntityError
                {
                    Code = code,
                    Message = error
                });
                switch (code)
                {
                    case EntityErrorCode.ParsingError:
                        break;
                    case EntityErrorCode.AnalysisError:
                        LoggerHelper.Log.Error(error);
                        break;
                    case EntityErrorCode.ValidationError:
                    case EntityErrorCode.AccessError:
                    case EntityErrorCode.InvalidDefinition:
                        LoggerHelper.Log.Warn(error);
                        break;
                    default:
                        LoggerHelper.Log.Debug(error);
                        break;
                }
            }
        }

        public void AddErrorFormat(EntityErrorCode code, String error, params object[] args)
        {
            var errorStr = String.Format(error, args);
            AddError(code, errorStr);
        }

        public void AddErrorFormat(string error, params object[] args)
        {
            AddErrorFormat(EntityErrorCode.InvalidDefinition, error, args);
        }

        public virtual List<SearchContent> SearchContent
        {
            get
            {
                if (_searchContent == null)
                {
                    _searchContent = GetSearchContent();
                }
                return _searchContent;
            }
        }

        protected abstract List<SearchContent> GetSearchContent();

        public Guid Id { get; private set; }
        public string DataProvider { get; private set; }

        public string TypeName
        {
            get
            {
                return Type.ToString();
            }
        }

        public EntityType Type { get; private set; }
        public bool IsAccessible { get; set; }
        public List<EntityAlias> Aliases { get; private set; }
        public abstract String InternalId { get; set; }

        public virtual bool AddDependency(IEntity dependency, EntityMatch match = null)
        {
            var result = false;
            if (Id == dependency.Id)
            {
                return false;
            }
            if (dependency.Type == EntityType.Unresolved)
            {
                if (DependsOn.All(e => e.Type == EntityType.Unresolved && e.Name != dependency.Name))
                {
                    if (DependsOn.Any(d => d.SearchNames.Any(s => s.Equals(dependency.Name, StringComparison.InvariantCultureIgnoreCase))))
                    {
                        AddErrorFormat(EntityErrorCode.DependencyWarning, "Possibly unresolved entity '{0}' detected as previously resolved", dependency.Name);
                    }
                    else
                    {
                        DependsOn.Add(dependency);
                        result = true;
                    }
                }
            }
            else
            {
                if (dependency.UsedBy.All(d => d.Id != Id))
                {
                    dependency.UsedBy.Add(this);
                    result = true;
                }
                if (DependsOn.All(e => e.Id != dependency.Id))
                {
                    var unresolved =
                        DependsOn.Where(d => d.Name == match.MatchText && d.Type == EntityType.Unresolved).ToList();
                    if (unresolved.Count > 0)
                    {
                        AddErrorFormat(EntityErrorCode.Info,
                            "Dependency with name '{0}' was resolved with next attempt. Removing {1} unresolved dependencies. Matched one: {2}",
                            match.MatchText, unresolved.Count, dependency.FullName);
                        DependsOn.RemoveAll(unresolved.Contains);
                    }

                    DependsOn.Add(dependency);
                    result = true;
                }
            }
            return result;
        }

        public bool AddDependency(IEntity dependency, string matchText)
        {
            return AddDependency(dependency, new EntityMatch(matchText));
        }

        public int Priority { get; protected set; }

        public virtual IEntity Find(string path, StringComparison comparison = StringComparison.InvariantCultureIgnoreCase)
        {
            if (path.StartsWith(ScopeDelimiter) && Scope != null)
            {
                Root.Find(path, comparison);
            }

            var pathParts = path.Split(new[] { ScopeDelimiter }, StringSplitOptions.RemoveEmptyEntries);

            IEntity node = this;
            foreach (var part in pathParts)
            {
                if (node.ScopeChildren == null)
                {
                    return null;
                }
                node = node.ScopeChildren.Single(i => String.Compare(i.Name, part, comparison) == 0);
            }

            return node;
        }

        public virtual IEntity FindByInternalId(string identifier, StringComparison comparison = StringComparison.CurrentCulture)
        {
            if (String.Compare(InternalId, identifier, comparison) == 0)
            {
                return this;
            }

            return ScopeChildren != null ?
                ScopeChildren.Select(child => child.FindByInternalId(identifier, comparison)).FirstOrDefault(c => c != null) :
                null;
        }

        public IEntity FindById(Guid id)
        {
            if (Id == id)
            {
                return this;
            }

            return ScopeChildren != null ?
                ScopeChildren.Select(child => child.FindById(id)).FirstOrDefault(c => c != null) :
                null;
        }

        #endregion
    }
}
