﻿CREATE TABLE [log].[ProviderTypeLog]
(
	[Action] [char](1) NOT NULL,
	[ItemId] [int] NOT NULL,
	[ItemName] [varchar](max) NULL,
	[Created] [datetime] NULL,
	[Version] [int] NULL,
	[LastUpdate] [datetime] NULL,
	[Application_source] [varchar](100) NULL,
	[System_source] [varchar](100) NULL
)
