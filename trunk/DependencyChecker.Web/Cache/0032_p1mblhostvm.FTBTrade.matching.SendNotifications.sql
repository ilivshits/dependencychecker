create procedure [matching].[SendNotifications]
(
	@notifications matching.Notification readonly
)
as
begin
	set nocount on;
	set xact_abort on;
	
	-- construct message
	declare @message VARCHAR(MAX) =
	(
		select '<n>' +
			'<o>' + cast(orderId as varchar) + '</o>' +
			'<t>' + isnull(cast(securityPrimaryTypeId as varchar), '') + '</t>' +
			'<m>' + isnull(cast(matchTypeId as varchar), '') + '</m>' +
			'<u>' + convert(varchar(24), lastUpdate, 121) + '</u>' +
		'</n>'
		from @notifications
		for xml path(''), type
	).value('.', 'varchar(max)')

	-- dispatch message
	if @message is not null
	begin
		declare @binary varbinary(max) = cast(('<msg>' + @message + '</msg>') as varbinary(max))
		exec notifications.DispatchMessage 'matching_NotificationQueue_Service', @binary
	end
end


