CREATE view [dbo].[Order_PqlProvider] as

-- orders
select orderTypeId = 1, o.orderId, a.allocationId, otcEventId = 0, blockId = 0, repoId = 0
	, o.sid, o.managerId, o.tradeDate, o.effectiveDate, o.settlementDate
	, orderSide = s.description, a.quantity, a.referenceQuantity, price = o.averagePrice, o.fxRate
	, o.businessUnitId, o.currencySid, o.execBrokerId
	, o.execUserId, o.externalReferenceCode
	, o.orderStatusId, a.legalEntityId, a.clearBrokerId, a.strategyId
	, signedQuantity = a.quantity * s.quantityMultiplier
	, signedReferenceQuantity = a.referenceQuantity * s.quantityMultiplier
		
from [Order] o
join [OrderAllocation] a on o.orderId = a.orderId
join [OrderSide] s on o.orderSideId = s.orderSideId

union all

-- OTC events
select 2, o.orderId, a.allocationId, e.otcEventId, e.blockId, 0
	, o.sid, o.managerId, e.eventDate, e.effectiveDate, e.settlementDate
	, tradeSide = t.description, e.quantity, null, price = e.price, e.fxRate
	, o.businessUnitId, e.settlementCurrencySid, case when e.stepInBrokerId != 0 then e.stepInBrokerId else o.execBrokerId end
	, e.execUserId, e.externalReferenceCode
	, o.orderStatusId, a.legalEntityId, a.clearBrokerId, a.strategyId
	, -e.quantity * s.quantityMultiplier, null
	
from [Order] o
join [OrderAllocation] a on o.orderId = a.orderId
join [OrderOTCEvent] e on a.orderId = e.orderId and a.allocationId = e.allocationId
join [OTCEventType] t on e.otcEventTypeId = t.otcEventTypeId
join [OrderSide] s on o.orderSideId = s.orderSideId

union all

-- repos
select 3, l.orderId, a.allocationId, 0, 0, r.repoId
    , r.sid, r.managerId, l.rateDate, null, l.startDate
    , os.description, a.quantity, null, r.price, null
    , r.businessUnitId, r.currencySid, r.execBrokerId
    , r.execUserId, l.externalReferenceCode
    , l.orderStatusId, a.legalEntityId, r.clearBrokerId, a.strategyId
	, a.quantity * os.quantityMultiplier, null
    
from [Repo] r
join [Security] s on r.sid = s.sid
join [RepoLeg] l on r.repoId = l.repoId
join [RepoAllocation] a on r.repoId = a.repoId
join [OrderSide] os on r.orderSideId = os.orderSideId

union all

-- cashflows
select 4, c.cashflowId, a.allocationId, 0, 0, 0
	, c.sid, c.managerId, c.postingDate, null, c.settlementDate
	, s.description, a.amount, a.referenceAmount, 1, null
	, c.businessUnitId, c.currencySid, c.execBrokerId
	, 0, c.externalReferenceCode
	, c.orderStatusId, a.legalEntityId, c.clearBrokerId, a.strategyId
	, a.amount * s.quantityMultiplier, a.referenceAmount * s.quantityMultiplier
	
from [Cashflow] c
join [CashflowAllocation] a on c.cashflowId = a.cashflowId
join [OrderSide] s on c.orderSideId = s.orderSideId

