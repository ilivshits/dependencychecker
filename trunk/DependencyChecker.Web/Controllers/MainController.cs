﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DependencyChecker.Common;
using DependencyChecker.DataProvider.DAL;
using DependencyChecker.DataProvider.Helpers;
using DependencyChecker.Entities.Utils;
using DependencyChecker.WCF;
using DependencyChecker.Web.Authorization;
using DependencyChecker.Web.Models;

namespace DependencyChecker.Web.Controllers
{
    public class MainController : Controller
    {
        protected String BasePath
        {
            get
            {
                var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Cache");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                return path;
            }
        }

        //
        // GET: /Main/

        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult ErrorsList()
        {
            var client = new DependencyCheckerServiceProxy();
            var status = client.GetStatus();
            if (status.State != DependencyCheckerServiceState.Ready)
                return RedirectToAction("Index");
            

            return View();
        }

        public ActionResult Cleanup(Guid id, int gens = 1)
        {
            var client = new DependencyCheckerServiceProxy();
            var scope = client.GetDetails(id);
            var result = client.GetUnusedEntities(id, gens);
            return View(new CleanupModel
            {
                Scope = scope,
                Unused = result
            });
        }

        private void LogAction(string message)
        {
            if (User.Identity.IsAuthenticated)
            {
                LoggerHelper.Log.InfoFormat("User {0} requested {1}", User.Identity.Name, message);
            }
            else
            {
                LoggerHelper.Log.InfoFormat("Anonymous requested {0}", message);
            }
        }

        [HttpGet]
        public JsonResult GetBatches()
        {
            var client = new DependencyCheckerServiceProxy();
            var ents = client.GetBatches();

            return Json(new { result = ents.OrderBy(e => e.Date).ToList() }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetBatchEntities(int batchId)
        {
            var client = new DependencyCheckerServiceProxy();
            var ents = client.GetBatchEntities(batchId);

            return Json(new {result = ents.OrderBy(e=>e.Name)}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetEntityErrors(int batchId, string entityName)
        {
            var client = new DependencyCheckerServiceProxy();
            var ents = client.GetEntityErrors(batchId, entityName);

            return Json(new { result = ents.OrderBy(e=>e) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Drillthrough(Guid id, String[] types)
        {
            LogAction(String.Format("drill through with parameters: id = {0}", id));

            var client = new DependencyCheckerServiceProxy();
            var result = client.Drillthrough(id, types);
            var status = client.GetStatus();

            return Json(new { result = result, status = status });
        }

        [HttpPost]
        public JsonResult Search(string name, bool isRegex = false, bool includeBody = false, bool exactSearch = false)
        {
            LogAction(String.Format("search with parameters: name = {0}, regular expression: {1}, in body: {2}, exact: {3}",
                name, isRegex, includeBody, exactSearch));

            if (isRegex)
            {
                try
                {
                    Regex.Match("", name.Trim());
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException("Regular expression is not correct. Please, fix it and try again.");
                }
            }

            var client = new DependencyCheckerServiceProxy();
            var status = client.GetStatus();

            if (String.IsNullOrEmpty(name.Trim()))
            {
                return Json(new { result = new List<Entity>(), status = status });
            }

            var result = client.Search(name.Trim(), isRegex, includeBody, exactSearch).OrderBy(e => e.Name).ToList();

            return Json(new { result = result, status = status });
        }

        [HttpPost]
        public JsonResult GetStatus()
        {
            var client = new DependencyCheckerServiceProxy();
            var result = client.GetStatus();

            return Json(new { status = result });
        }

        [HttpPost]
        public JsonResult GetDatabases()
        {
            var client = new DependencyCheckerServiceProxy();
            var status = client.GetStatus();
            var result = client.GetDatabases().OrderBy(e => e.Name);
            //var result = DatabaseHelpers.GetDatabases().OrderBy(e=>e.Name);
            return Json(new { status = status, result = result.Select(e=>e.Name).ToList() });
        }

        [HttpPost]
        public JsonResult GetDetails(Guid id)
        {
            LogAction(String.Format("object details for: id = {0}", id));

            var client = new DependencyCheckerServiceProxy();
            var result = client.GetDetails(id);
            result.DependsOn = result.DependsOn.OrderBy(e => e.Name).ToList();
            result.UsedBy = result.UsedBy.OrderBy(e => e.Name).ToList();
            var status = client.GetStatus();

            return Json(new { result = result, status = status });
        }

        [HttpGet]
        public FileResult DownloadCleanup(Guid id)
        {
            LogAction(String.Format("cleanup download for: id = {0}", id));

            var client = new DependencyCheckerServiceProxy();
            var status = client.GetStatus();
            var item = client.GetDetails(id);
            var result = client.GetUnusedCleanupScripts(id);

            if (client.GetState() != DependencyCheckerServiceState.Ready.ToString())
            {
                var name = Path.Combine(BasePath, item.Name + ".zip");
                var info = new FileInfo(name);
                using (var writer = info.CreateText())
                {
                    writer.WriteLine("Dependency checker is unavailable. Current status: " + status);
                }
                return File(info.OpenRead(), "text/plain");
            }
            return File(result, "application/zip, application/octet-stream");
        }

        [HttpPost]
        public FileResult DownloadSetCleanup(Guid id, String idStr)
        {
            var ids = idStr.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries);
            LogAction(String.Format("download object details for: ids:\r\n{0}", String.Join("\r\n", ids)));

            var client = new DependencyCheckerServiceProxy();
            var status = client.GetStatus();
            var item = client.GetDetails(id);
            var result = client.GetUnusedCleanupScriptsForSet(id, ids.Select(s => new Guid(s)).ToArray());

            if (client.GetState() != DependencyCheckerServiceState.Ready.ToString())
            {
                var name = Path.Combine(BasePath, item.Name + ".zip");
                var info = new FileInfo(name);
                using (var writer = info.CreateText())
                {
                    writer.WriteLine("Dependency checker is unavailable. Current status: " + status);
                }
                return File(info.OpenRead(), "text/plain");
            }
            return File(result, "application/zip, application/octet-stream");
        }        
        
        [HttpGet]
        public FileResult DownloadDetails(Guid id)
        {
            LogAction(String.Format("download object details for: id = {0}", id));

            var client = new DependencyCheckerServiceProxy();
            var status = client.GetStatus();

            var result = client.PrintEntity(id);

            result = Regex.Replace(result, @"([ \t]+)?(?<var>\$(USER|PWD))([ \t]+)?=([ \t]+)?(?<value>('[^']+'|""[^""]+""))([ \t]+)?;([ \t]+)?", 
                String.Empty, RegexOptions.IgnoreCase | RegexOptions.Multiline);

            var name = Path.Combine(BasePath, id + ".txt");
            var info = new FileInfo(name);
            if (info.Exists)
                return File(info.OpenRead(), "text/plain", info.Name + ".txt");


            if (client.GetState() != DependencyCheckerServiceState.Ready.ToString())
            {
                using (var writer = info.CreateText())
                {
                    writer.WriteLine("Dependency checker is unavailable. Current status: " + status);
                }
            }
            else
            {
                using (var writer = info.CreateText())
                {
                    writer.WriteLine(result);
                }
            }
            return File(info.OpenRead(), "text/plain", info.Name + ".txt");
        }

        /// <summary>
        /// Controller method for exporting Item's dependents information
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult ExportAll(Guid id)
        {
            LogAction(String.Format("download export all details for: id = {0}", id));

            var client = new DependencyCheckerServiceProxy();
            var status = client.GetStatus();
            string name = client.GetDetails(id).Name;
            var ctx = new DomainLogicContext(BasePath, client.GetLastRunTime());
            ctx.GetDependsOnArchive(id);

            string path = Path.Combine(BasePath, name + ".zip");

            return Json(new { result = path, status = status }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public FileResult DownloadFile(string path)
        {
            FileInfo fInfo = new FileInfo(path);
            return File(fInfo.OpenRead(), "application/zip, application/octet-stream", Path.GetFileName(path));
        }
    }
}
