﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DependencyChecker.Common
{
    public static class LoggerHelper
    {
        public static readonly log4net.ILog Log;

        static LoggerHelper()
        {
            Log = log4net.LogManager.GetLogger("DependencyChecker"/*MethodBase.GetCurrentMethod().DeclaringType*/);
        }
    }
}
