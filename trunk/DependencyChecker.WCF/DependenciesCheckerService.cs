﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using DependencyChecker.Analyzer.Interfaces;
using DependencyChecker.Collector;
using DependencyChecker.Common;
using DependencyChecker.DataProvider.Helpers;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Interfaces;
using DependencyChecker.Entities.Utils;

namespace DependencyChecker.WCF
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class DependenciesCheckerService : IDependenciesCheckerService
    {
        private DependencyCheckerWorker _worker;
        //private Task<IAnalyzer> _analyzerTask;
        private Thread _thread;
        private IAnalyzer _analyzer;
        private CancellationTokenSource _cancelTaskToken;

        public DependenciesCheckerService()
        {
            _worker = new DependencyCheckerWorker();
            _worker.StatusChanged += WorkerOnStatusChanged;

            _thread = new Thread(() =>
            {
                _analyzer = _worker.GetEntities();
            });
            _thread.Start();

            /*_analyzerTask = _worker.GetEntities();
            _analyzerTask.ContinueWith((task) =>
            {
                _analyzer = task.Result;
            });*/
        }

        private DependencyCheckerStatus _status = new DependencyCheckerStatus()
        {
            State = DependencyCheckerServiceState.Initializing,
            Progress = 0,
            StepProgress = 0,
            CompletedSteps = new Dictionary<string, int>(),
            Status = "Initializing service"
        };
        private DependencyCheckerServiceState _state = DependencyCheckerServiceState.LoadingData;

        private void WorkerOnStatusChanged(object sender, DependencyCheckerEventArgs dependencyCheckerEventArgs)
        {
            _status.Status = dependencyCheckerEventArgs.Status;
            _status.Comment = dependencyCheckerEventArgs.Comment;
            _status.Progress = dependencyCheckerEventArgs.Progress;
            _status.StepProgress = dependencyCheckerEventArgs.StepProgress;
            _status.State = dependencyCheckerEventArgs.State;
            _status.CompletedSteps = dependencyCheckerEventArgs.CompletedSteps;

            _state = dependencyCheckerEventArgs.State;
        }

        public void RestartService()
        {
            _status = new DependencyCheckerStatus()
            {
                State = DependencyCheckerServiceState.Initializing,
                Progress = 0,
                StepProgress = 0,
                CompletedSteps = new Dictionary<string, int>(),
                Status = "Initializing service"
            };
            _state = DependencyCheckerServiceState.LoadingData;

            _worker = new DependencyCheckerWorker();
            _worker.StatusChanged += WorkerOnStatusChanged;

            if (_thread != null && _thread.IsAlive)
            {
                _thread.Abort();
            }
            _thread = new Thread(() =>
            {
                _analyzer = _worker.GetEntities();
            });
            _thread.Start();

        }


        public DependencyCheckerStatus GetStatus()
        {
            return _status;
        }

        public string PrintEntity(Guid id)
        {
            if (_thread.IsAlive)
            {
                return _status.Status;
            }
            
            return DatabaseHelpers.ExportToText(id);
        }

        public string PrintEntityDefinition(Guid id)
        {
            if (_thread.IsAlive)
            {
                return _status.Status;
            }

            return DatabaseHelpers.PrintDefinition(id);
        }

        public string ExportEntity(Guid id, bool up, bool down)
        {
            return string.Empty;
        }

        public List<Entity> Search(string name, bool useRegularExpression, bool searchInDefinition, bool exactSearch)
        {
            return _thread.IsAlive
                ? new List<Entity>()
                : DatabaseHelpers.Search(name, useRegularExpression, searchInDefinition, exactSearch);
        }

        public Entity Drillthrough(Guid uid, string[] types)
        {
            if (_thread.IsAlive)
            {
                return null;
            }

            return DatabaseHelpers.Drillthrough(uid, types);
        }

        public Entity GetDetails(Guid id)
        {
            return DatabaseHelpers.GetEntityDetailsById(id);
        }

        public byte[] GetUnusedCleanupScripts(Guid id)
        {
            var scope = _analyzer.GetEntity(id);
            try
            {
                var files = _analyzer.GenerateCleanupForUnused(scope).OrderBy(f => f).ToList();

                return ZipPacker.GeneratePackage(scope.FullName, files);
            }
            catch (Exception exception)
            {
                LoggerHelper.Log.ErrorFormat("Error occured while generating unused script for {0}. Exception details: {1}", scope.FullName, exception);
                throw;
            }
        }

        public byte[] GetUnusedCleanupScriptsForSet(Guid databaseId, Guid[] ids)
        {
            var scope = _analyzer.GetEntity(databaseId);
            try
            {
                var files = _analyzer.GenerateCleanup(scope, ids);

                return ZipPacker.GeneratePackage(scope.FullName, files);
            }
            catch (Exception exception)
            {
                LoggerHelper.Log.ErrorFormat("Error occured while generating unused script for {0}. Exception details: {1}", scope.FullName, exception);
                throw;
            }
        }

        public List<Entity> GetUnusedEntities(Guid databaseId, int generations)
        {
            var scope = _analyzer.GetEntity(databaseId);
            return _analyzer.GetUnusedEntities(scope, generations).Select(e => BuildEntity(e)).ToList();
        }

        /// <summary>
        /// With given Item ID fetching information about all dependencies in given direction.
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="dir">Traversing direction: by Used By or Depends On</param>
        /// <returns></returns>
        public Entity GetEntitiesTree(Guid itemId, TraversalDirection dir)
        {
            LoggerHelper.Log.Info("Started traversing entities tree");
            if (dir == TraversalDirection.Depending)
            {
                var source = _analyzer.GetEntity(itemId);

                var root = new Entity
                {
                    Id = source.Id,
                    Name = source.FullName.Trim(new []{' ','_', '-', '/','\\'}),
                    Type = source.TypeName,
                    Server = source.Root.FullName,
                    Level = source.Level,
                    Provider = source.DataProvider,
                    LastModified = source.LastModified == DateTime.MinValue ? null : source.LastModified.ToLocalTime().ToString("MM/dd/yyyy hh:mm"),
                    LastModifiedBy = source.LastModifiedBy?.Name,
                    Created = source.Created == DateTime.MinValue ? null : source.Created.ToLocalTime().ToString("MM/dd/yyyy hh:mm"),
                    CreatedBy = source.CreatedBy?.Name
                };
                
                return FillEntity(root);
                
            }
            else if (dir == TraversalDirection.UsedBy)
            {
                throw new NotImplementedException("Direction to UsedBy not supported yet.");
            }

            return null;
        }

        private Entity FillEntity(Entity entity, List<Guid> ids = null)
        {
            /******/
            if (ids == null) ids = new List<Guid>();
            /******/

            GetDependsOnDetails(entity, ids);
            foreach (var dEntity in entity.DependsOn)
            {
                FillEntity(dEntity, ids);
            }


            return entity;
        }

        private void GetDependsOnDetails(Entity entity, List<Guid> ids)
        {
            
            ids.Add(entity.Id);
            var source = _analyzer.GetEntity(entity.Id);
            if (source == null)
            {
                entity.DependsOn = new List<Entity>();
                return;
            }
            entity.Errors = source.GetErrors();
            var dependencies = new List<IEntity>();
            dependencies.AddRange(source.DependsOnRecursive);

            entity.DependsOn = dependencies.Select(e =>
            {
                if (ids.Contains(e.Id)) return null;

                var res = new Entity
                {
                    Id = e.Id,
                    Name = e.FullName,
                    Type = e.TypeName,
                    Server = e.Root.FullName,
                    Level = e.Level,
                    Provider = e.DataProvider,
                    LastModified =
                        e.LastModified == DateTime.MinValue
                            ? null
                            : e.LastModified.ToLocalTime().ToString("MM/dd/yyyy hh:mm"),
                    LastModifiedBy = e.LastModifiedBy?.Name,
                    Created =
                        e.Created == DateTime.MinValue ? null : e.Created.ToLocalTime().ToString("MM/dd/yyyy hh:mm"),
                    CreatedBy = e.CreatedBy?.Name
                };

                var dbSubEntity = e as DatabaseEntity;
                if (dbSubEntity != null)
                {
                    res.Database = dbSubEntity.Database == null ? string.Empty : dbSubEntity.Database.FullName;
                }

                return res;
            }).Where(e => e != null).ToList();

        } 

        public string GetState()
        {
            return _state.ToString();
        }

        private string ParseComplexName(string str)
        {
            if (str.Contains(':'))
            {
                StringBuilder sb = new StringBuilder();
                Regex reg = new Regex("[a-z]+:", RegexOptions.IgnoreCase);
                var match = reg.Match(str);
                str = str.Replace("/", "\\");
                str = sb.AppendFormat("{0} | {1}", str.Substring(0, match.Index).TrimEnd('\\'), str.Substring(match.Index)).ToString();
            }
            return str;
        }

        private Entity BuildEntity(IEntity source, bool addDependencies = false, List<EntityType> drillthroughTypes = null)
        {
            var result = new Entity
            {
                Id = source.Id,
                Name = ParseComplexName(source.FullName),
                Type = source.TypeName,
                Server = source.Root.FullName,
                Level = source.Level,
                Provider = source.DataProvider,
                LastModified = source.LastModified == DateTime.MinValue ? null : source.LastModified.ToLocalTime().ToString("MM/dd/yyyy hh:mm"),
                LastModifiedBy = source.LastModifiedBy?.Name,
                Created = source.Created == DateTime.MinValue ? null : source.Created.ToLocalTime().ToString("MM/dd/yyyy hh:mm"),
                CreatedBy = source.CreatedBy?.Name
            };



            LoggerHelper.Log.DebugFormat("Requested enity dependencies list with the following parameters:\r\nEntity - {0}\r\nAdd dependencied flag - {1}\r\nTypes - {2}", 
                source.FullName, addDependencies, drillthroughTypes == null ? "NONE" :  string.Join(",", drillthroughTypes.Select(t => t.ToString()).ToArray()));

            var dbEntity = source as DatabaseEntity;
            if (dbEntity != null)
            {
                result.Database = dbEntity.Database == null ? string.Empty : dbEntity.Database.FullName;
                var lastRun = dbEntity.JobLastRunTime == DateTime.MinValue
                    ? null
                    : dbEntity.JobLastRunTime.ToLocalTime().ToString("MM/dd/yyyy hh:mm");
                result.LastRun = lastRun;
            }

            var taskEntity = source as ScheduledTaskEntity;
            if (taskEntity != null)
            {
                var lastRun = taskEntity.TaskLastRunTime == DateTime.MinValue
                    ? null
                    : taskEntity.TaskLastRunTime.ToLocalTime().ToString("MM/dd/yyyy hh:mm");
                result.LastRun = lastRun;
            }

            if (addDependencies)
            {
                result.Errors = source.GetErrors();
                var dependencies = new List<IEntity>();
                if (drillthroughTypes != null)
                {
                    if (source.DependsOnRecursive.Any())
                    {
                        dependencies.AddRange(Analyzer.Analyzer.GetUsages(true, source.DependsOnRecursive, drillthroughTypes));
                    }
                }
                else
                {
                    dependencies.AddRange(source.DependsOnRecursive);
                }   

                result.DependsOn = dependencies.Select(e =>
                {
                    var res = new Entity
                    {
                        Id = e.Id,
                        Name = ParseComplexName(e.FullName),
                        Type = e.TypeName,
                        Server = e.Root.FullName,
                        Provider = e.DataProvider
                    };
                    var dbSubEntity = e as DatabaseEntity;
                    if (dbSubEntity != null)
                    {
                        res.Database = dbSubEntity.Database == null ? string.Empty : dbSubEntity.Database.FullName;
                    }

                    return res;
                }).ToList();


                var usages = new List<IEntity>();
                if (drillthroughTypes != null)
                {
                    if (source.UsedByRecursive.Any())
                    {
                        usages.AddRange(Analyzer.Analyzer.GetUsages(false, source.UsedByRecursive, drillthroughTypes));
                    }
                }
                else
                {
                    usages.AddRange(source.UsedByRecursive);
                }

                result.UsedBy = usages.Select(e =>
                {
                    var res = new Entity
                    {
                        Id = e.Id,
                        Name = ParseComplexName(e.FullName),
                        Type = e.TypeName,
                        Server = e.Root.FullName,
                        Provider = e.DataProvider
                    };
                    var dbSubEntity = e as DatabaseEntity;
                    if (dbSubEntity != null)
                    {
                        res.Database = dbSubEntity.Database == null ? string.Empty : dbSubEntity.Database.FullName;
                    }

                    return res;
                }).ToList();
            }

            return result;
        }

        public DateTime? GetLastRunTime()
        {
            return _analyzer.LastRunTime;
        }

        public List<Entity> GetDatabases()
        {
            return DatabaseHelpers.GetDatabases();
        }

        public List<Entity> GetBatchEntities(int batchId)
        {
            return DatabaseHelpers.GetBatchEntities(batchId);
        }

        public List<string> GetEntityErrors(int batchId, string entityName)
        {
            return DatabaseHelpers.GetEntityErrors(batchId, entityName);
        }

        public List<Batch> GetBatches(DateTime? from = null, DateTime? to = null)
        {
            return DatabaseHelpers.GetBatches(from, to);
        }

        public List<BatchException> GetBatchExceptions(int batchId)
        {
            return DatabaseHelpers.GetBatchExceptions(batchId);
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion
    }
}
