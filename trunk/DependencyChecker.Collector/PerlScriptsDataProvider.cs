﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using DependencyChecker.Collector.Base;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Interfaces;
using DependencyChecker.Entities.Utils;
using Microsoft.Win32.TaskScheduler;


namespace DependencyChecker.Collector
{
    public class PerlScriptsDataProvider : BaseDataProvider
    {
        public PerlScriptsDataProvider(IServerProvider serverProvider)
            : base(serverProvider)
        {
        }

        #region Overrides of BaseDataProvider

        public override string Name
        {
            get { return PerlScriptEntity.ProviderName; }
        }

        protected override List<Regex> ItemSearchPattern
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
