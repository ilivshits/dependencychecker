﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DependencyChecker.Common;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Entities
{
    public class DatabaseEntity : EntityBase
    {
        public static readonly string ProviderName = EntityProviderType.SQL.ToString();
        public static Dictionary<String, string> PropertyFunctionsResults = new Dictionary<String, string>();
        public override string InternalId { get; set; }

        private bool? _isFunctionAlias;
        protected bool IsFunctionAlias
        {
            get
            {
                if (_isFunctionAlias == null)
                {
                    DetermineFunctionAlias();    
                }
                return _isFunctionAlias.Value;
            }
        }
        private String _functionAlias;
        public String FunctionAlias
        {
            get
            {
                if (_isFunctionAlias != null)
                {
                    return _functionAlias;
                }
                DetermineFunctionAlias();
                return _functionAlias;
            }
        }

        private void DetermineFunctionAlias()
        {
            if (SearchContent.All(c => c.ContentType != EntityProviderType.SQL))
            {
                _isFunctionAlias = false;
            }
            else if (Type != EntityType.Function || SearchContent.Count(c => c.ContentType == EntityProviderType.SQL) > 1)
            {
                _isFunctionAlias = false;
            }
            else
            {
                var match = Regex.Match(SearchContent.Single(c => c.ContentType == EntityProviderType.SQL).Text,
                    @"AS[ \t\r\n]+(BEGIN)?[ \t\r\n]+RETURN[ \t\r\n]+(?<name>'[a-z0-9_\-]+')(;)?[ \t\r\n]+(END)?[ \t\r\n]*$", RegexOptions.IgnoreCase);
                if (match.Success && match.Groups["name"].Success)
                {
                    _functionAlias = match.Groups["name"].Value;
                }
                else
                {
                    _functionAlias = String.Empty;
                }
                _isFunctionAlias = String.IsNullOrEmpty(_functionAlias);
            }
        }

        public IEntity Database
        {
            get
            {
                switch (Level)
                {
                    case 0:
                        return null;
                    case 1:
                        return this;
                    default:
                        IEntity result = this;
                        while (result.TypeName != EntityType.Database.ToString())
                        {
                            result = result.Scope;
                        }
                        return result;
                }
            }
        }

        public List<IEntity> Schemas { get; set; }
        public List<IEntity> Tables { get; set; }
        public List<IEntity> Views { get; set; }
        public List<IEntity> StoredProcedures { get; set; }
        public List<IEntity> Functions { get; set; }
        public List<IEntity> Triggers { get; set; }
        public List<IEntity> Synonyms { get; set; }
        public List<IEntity> LinkedServers { get; set; }
        public List<IEntity> Jobs { get; set; }
        public List<IEntity> JobSteps { get; set; }
        public DateTime JobLastRunTime { get; set; }


        public override List<string> SearchNames
        {
            get
            {
                var selfNames = new List<String> { "[" + Name + "]" };
                if (!Name.Contains(" "))
                {
                    selfNames.Add(Name);
                }
                if (Name.Contains(".FORTRESSINV.COM"))
                {
                    selfNames.Add(Name.Replace(".FORTRESSINV.COM", String.Empty));
                }
                var result = new List<String>();

                if (TypeName == EntityType.Schema.ToString() && Name == "dbo")
                {
                    selfNames.Add(String.Empty);
                }

                foreach (var alias in Aliases)
                {
                    if (!selfNames.Contains(alias.DisplayName))
                        selfNames.Add(alias.DisplayName);
                    if (!selfNames.Contains("[" + alias.DisplayName + "]"))
                        selfNames.Add("[" + alias.DisplayName + "]");
                }

                if (Scope == null)
                {
                    return selfNames;
                }
                result.AddRange(selfNames);
                foreach (var parentName in Scope.SearchNames)
                {
                    var name = parentName;
                    result.AddRange(selfNames.Select(n => name + ScopeDelimiter + n));
                }
                return result;
            }
        }

        public override bool AddDependency(IEntity dependency, EntityMatch match = null)
        {
            var result = base.AddDependency(dependency, match);

            var dbDependency = dependency as DatabaseEntity;
            if (dbDependency != null && dbDependency.IsFunctionAlias && !String.IsNullOrEmpty(dbDependency.FunctionAlias))
            {
                var content = SearchContent.First(c => c.ContentType == EntityProviderType.SQL);
                content.ReplaceMatch(match, dbDependency.FunctionAlias);
                HasUnresolved = true;
                LoggerHelper.Log.DebugFormat("Alias function '{1}' dependency detected and replaced in '{0}'", FullName, dependency.FullName);
            }

            return result;
        }

        const string olapQueryPattern =
            @"from[ \t\r\n]+openquery[ \t\r\n]*\([a-z0-9_]+OLAP[ \t\r\n]*,[ \t\r\n]*select[ \t\r\n]*('[ \t\r\n]*\+[ \t\r\n]*((?<var>@[a-z0-9]+)[ \t\r\n]*\+[ \t\r\n]*)?'[ \t\r\n]*)*(on[ \t\r\n]*[0-9]+[ \t\r\n]*,[ \t\r\n]*)?(non[ \t\r\n]*empty|nonempty|{)([^']*('[ \t\r\n]*\+[ \t\r\n]*((?<var>@[a-z0-9]+)[ \t\r\n]*\+[ \t\r\n]*)?'[ \t\r\n]*)*)*";

        private static readonly List<String> Keywords = new List<string>
        {
            "INSERT",
            "CREATE",
            "OPENQUERY",
            "SELECT",
            "INTO",
            "CAST",
            "CONVERT",
            "GETDATE",
            "DECLARE",
            "UPDATE",
            "ISNULL",
            "VARCHAR",
            "INT",
            "DECIMAL",
            "AS",
            "SET",
            "AT",
            "WHERE",
            "DROP",
            "NEW",
            "sp_executesql"
        };

        

        private static Regex olapRegex = new Regex(olapQueryPattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private static Regex comments = new Regex(@"\/\*(.*?)\*\/", RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex onelineComments = new Regex(@"\-\-.*\r\n", RegexOptions.Compiled);
        private static Regex allComments = new Regex("\\-\\-[^\\n]*\\n", RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private static Regex textPicker = new Regex(@"('[ ]?\+[^+]+\+[ ]?'|\-\-.*\r\n)", RegexOptions.Compiled);
        private static Regex quotesPicker = new Regex(@"([""'])(?:\\\1|.)*?\1", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex expressionQuotesPicker = new Regex(@"(as|=)+\s*([""'])(\\?.)*?\2|(?<=([\s\t\n\r]|^)values\s*\(.*)\'[\w\s]+\'(?=.*\))", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex xmlValuesParamsPicker = new Regex(@"([\s\t\n\r]|^)[\w\.]+\.value\(.*?\)[,\r\n\t]", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex inPicker = new Regex(@"(?<=(^|[\s\t\n\r])in[\s\t\r\n]*\()(?![\s\t\r\n]*(" + String.Join("|", Keywords) + @"))([^()]|(?<Level>\()|(?<-Level>\)))+(?(Level)(?!))(?=\))", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex execProcParamsPicker = new Regex(@"(?<=[\s\t\r\n]|^)EXEC(UTE)?[\s\t\r\n]+[^(][\.\w]+[\s\t\r\n]+.*?(" + String.Join("|", Keywords) + @")", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex casesPicker = new Regex(@"(?<=^|\s)CASE\s.*?\sEND(?=$|\s)", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex sqlNativePicker = new Regex(@"(?<=declare\s+)\S+?(?=\s+.*?([[]dbo[]]\.)?[[]fnGetSqlNativeDllDatabaseName[]])", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static Regex linkedSelfPicker = new Regex(@"\@\S+(?=.*[\s\t\r]+('SELF'))", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);



        protected override List<SearchContent> GetSearchContent()
        {
            var result = new List<SearchContent>();
            if (string.IsNullOrWhiteSpace(Definition))
                return result;

            var commentsRemoved = comments.Replace(onelineComments.Replace(Definition, String.Empty).Replace("''", "")
                                                                                                    .Replace("**", "*")
                                                                                                    .Replace("/**", "/*")
                                                                                                    .Replace("**/", "*/"), String.Empty);

            commentsRemoved = allComments.Replace(commentsRemoved, String.Empty);

            var declares = sqlNativePicker.Matches(commentsRemoved);
            foreach (Match declare in declares)
            {
                string fnResult = PropertyFunctionsResults.Keys.Contains("[dbo].[fnGetSqlNativeDllDatabaseName]")
                    ? PropertyFunctionsResults["[dbo].[fnGetSqlNativeDllDatabaseName]"]
                    : null;
                if (fnResult != null)
                    commentsRemoved = Regex.Replace(
                        commentsRemoved, 
                        @"\'\s*\+\s*\" + declare.Value + @"\s*\+\s*\'",
                        PropertyFunctionsResults["[dbo].[fnGetSqlNativeDllDatabaseName]"]
                    );
            }

            var linkeds = linkedSelfPicker.Matches(commentsRemoved);
            foreach (Match linkedSrvVar in linkeds)
            {
                commentsRemoved = Regex.Replace(commentsRemoved, @"\'\s*\+\s*\" + linkedSrvVar.Value + @"\s*\+\s*\'",
                    this.Root.Name);
            }


            string textPicked = textPicker.Replace(commentsRemoved.Replace("''", ""), string.Empty);
            textPicked = casesPicker.Replace(textPicked, "");
            string expressionQuotesRemoved = expressionQuotesPicker.Replace(textPicked, match =>
            {
                if (Regex.IsMatch(match.Value, @"[\$\[\]\(\)\.]"))
                {
                    return match.Value;
                }

                return "";
            });
            expressionQuotesRemoved = xmlValuesParamsPicker.Replace(expressionQuotesRemoved, "");
            expressionQuotesRemoved = inPicker.Replace(expressionQuotesRemoved, match =>
            {
                if (quotesPicker.IsMatch(match.Value)) return "";
                return match.Value;
            });
            expressionQuotesRemoved = execProcParamsPicker.Replace(expressionQuotesRemoved, match =>
            {
                if (quotesPicker.IsMatch(match.Value)) return "";
                return match.Value;
            });
            MatchCollection quotesPicked = quotesPicker.Matches(expressionQuotesRemoved);
            result.AddRange(quotesPicked
                .Cast<Match>()
                .SelectMany(m => new List<SearchContent> { new SearchContent
                {
                    ContentType = EntityProviderType.P1TC,
                    Text = m.Value
                }, new SearchContent
                {
                    ContentType = EntityProviderType.Person,
                    Text = m.Value
                }}).ToList());

            var olapSearchContent = olapRegex.Matches(commentsRemoved)
                .Cast<Match>()
                .Select(m => new SearchContent
                {
                    Text = m.Value,
                    ContentType = EntityProviderType.OLAP
                }).ToList();

            if (olapSearchContent.Count > 0)
            {
                result.AddRange(olapSearchContent);
                commentsRemoved = olapRegex.Replace(commentsRemoved, String.Empty);
            }

            result.AddRange(new List<SearchContent>{
                    new SearchContent
                        {
                            Text = commentsRemoved,
                            Scope = Database,
                            ContentType = EntityProviderType.SQL
                        }
                    });

            return result;
        }

        public DatabaseEntity(string name, EntityType type)
            : base(name, ProviderName, type)
        {

        }

        public DatabaseEntity(string name, string definition, EntityType type)
            : base(name, definition, ProviderName, type)
        {

        }

        protected DatabaseEntity(string name, string definition, EntityType type, string providerName)
            : base(name, definition, providerName, type)
        {

        }
    }
}