

CREATE PROCEDURE [FTBTrade].[dbo_Security_GetSecuritiesBySecurityIdentifier]
	@identifierTypeId int,
	@code varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--- Query.
	select
		s.*
	from
		FTBTrade.dbo_SecurityIdentifier as si with (nolock)
			inner join FTBTrade.dbo_Security as s with (nolock) on
				s.sid = si.sid
	where
		si.identifierTypeId = @identifierTypeId and
		si.code = @code	
END



