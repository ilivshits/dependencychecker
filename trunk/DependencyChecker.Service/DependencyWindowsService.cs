﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using DependencyChecker.Common;
using DependencyChecker.WCF;

namespace DependencyChecker.Service
{
    public partial class DependencyWindowsService : ServiceBase
    {
        private ServiceHost _host;
        private IDependenciesCheckerService _service;
        public DependencyWindowsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (_service == null)
            {
                _service = new DependenciesCheckerService();
            }
            if (_host != null)
            {
                _host.Close();
                _host = null;
            }

            _host = new ServiceHost(_service);
            _host.Open();
        }

        protected override void OnStop()
        {
            if (_service != null)
            {
                _service.Dispose();
                _service = null;
            }
            if (_host != null)
            {
                _host.Close();
                _host = null;
            }
        }
    }
}
