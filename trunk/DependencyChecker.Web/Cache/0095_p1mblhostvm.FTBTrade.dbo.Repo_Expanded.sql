CREATE view [dbo].[Repo_Expanded] as
select r.repoId
	, l.orderId
	, a.allocationId
	
	, r.repoTypeId	
	, r.sid
	, r.managerId
	, r.execBrokerId
	, r.clearBrokerId
	, r.price
	, r.haircut
	, r.repoDeliveryId
	, r.dayCountConventionId
	, r.closeDate
	, r.note
	, r.execUserId
	, r.businessUnitId
	
	, a.legalEntityId
	, a.strategyId
	
	, quantity = a.quantity * os.quantityMultiplier

	, l.orderStatusId
	, l.rateDate
	, l.startDate
	, l.endDate
	, l.rate
	
from [Repo] r
join [RepoLeg] l on r.repoId = l.repoId
join [RepoAllocation] a on r.repoId = a.repoId
join [OrderSide] os on r.orderSideId = os.orderSideId

