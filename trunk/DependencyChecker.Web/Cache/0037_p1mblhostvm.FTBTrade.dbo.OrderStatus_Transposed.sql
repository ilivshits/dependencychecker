CREATE VIEW [dbo].[OrderStatus_Transposed] AS
SELECT
[Pending Book] = 1
, [Booked] = 2
, [Pending Desk] = 3
, [Desk] = 4
, [Pending Cxl] = 5
, [Cancelled] = 6
, [Error] = 7


