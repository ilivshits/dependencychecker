
create view [report].[ErrorOrders] as
SELECT [Order Id]= o.orderId
 , [Manager] = m.description
 , [Security] = s.description
 , [Message] = n.message
FROM [Order] o
 INNER JOIN [Security] s ON o.sid = s.sid
 INNER JOIN [Manager] m ON o.managerId = m.managerId
 INNER JOIN [OrderNotification] n ON o.orderId = n.orderId
WHERE o.orderStatusId = 7


