﻿CREATE PROCEDURE [pnl].[GetDetailedPnlByTraderFund_opt]
	@asofdate DATETIME = '20130728',
	
	--23	FCF
	--38	Fortress Macro Fund
	--41	Man Managed Account
	--42	Hermes Managed Account
	--43	UBS Managed Account
	--45	NFC Managed Account
	--46	GAM Managed Account
	--48	Fortress Asia Macro Fund
	--49	GSAM Managed Fund
	--58	Fortress Convex Asia Fund
	--60	Permal Managed Account
	--61	Amundi Managed Account
	-- singlevalued
	@fund varchar(200) = '48',

	-- 1 - Adam Levinson
	-- 4 - Macro G-10
	-- 7 - Tom Barket
	-- 2165 - Sandeep
	-- singlevalued
	@trader varchar(200) = '[All]',
	
	-- level
	-- 0 - show sector
	-- 1 - show region
	@level bit = 0,
	
	-- showDollar param
	-- 0 - will show bps value
	-- 1 - will show dollar value
	@showDollar bit = 0,
	
	@region varchar(100) = '[All]',
	@sector varchar(100) = '[All]',
	@asset varchar(100) = '[All]'
AS
BEGIN
	SET NOCOUNT ON;

	--DROP TABLE  #pnl_bps
	--declare @asofdate DATETIME 
	--set @asofdate= '20130228'
	--declare @fund varchar(200)
	--set @fund = null--'48';
	--declare @trader varchar(200)
	--set @trader = '7'
	-- --level
	-- --0 - by sector
	-- --1 - by region
	--declare @level bit 
	--set @level= 0
	--declare @showDollar bit;
	--set @showDollar=0;
	--declare @region varchar(100), @sector varchar(100),  @asset varchar(100)
	--set @region = '[All]'
	--set @sector  = '[All]'
	--set @asset = '[All]'
	
	if(@fund = '[All]') set @fund=null
	if(@trader = '[All]') set @trader=null
	
	if(@fund is not null AND @trader is null)
	begin
		print 'Calling optimized fund level stored proc'
		EXEC pnl.GetDetailedPnlByFund_opt @asofdate = @asofdate, @fund = @fund , @level=@level,
		@showDollar = @showDollar, @sector=@sector, @region=@region, @asset=@asset
	end
	ELSE
	BEGIN
		declare @fundPart varchar(150);
		declare @traderPart varchar(150);
		
		if(@fund is not null)
			set @fundPart = '*{[Funds].[Fund Name].&[' + 
				(select DisplayName from CUBE1.Forensics.dbo.v_MetaFund where fundId = @fund)
			 + ']}';
		
		if(@trader is not null)
			set @traderPart = '*{[Traders].[Aexeo Trader Name].&[' + 
				(select name from foffice.v_trader where aexeo_trader_id = @trader)
			 + ']}'
			
		 
		DECLARE @dateStr VARCHAR(10);
		DECLARE @qry VARCHAR(max);

		IF (@asofdate IS NULL) SET @asofdate = dbo.LastBusinessDay();
		SET @dateStr = Convert(VARCHAR(10), @asofdate, 126);
		CREATE TABLE #pnl_bps
		(
			tid bigint,
			strategy int,
			asset VARCHAR(100),
			level1 VARCHAR(100),
			--traderCap bigint,
			--cap bigint,
			dtd FLOAT,
			wtd FLOAT,
			mtd FLOAT,
			ytd FLOAT
		);
		
		declare @row varchar(500)
		declare @res varchar(500)
		if(@showDollar=1)
		begin
			set @row = '{
			--[Measures].[TraderCapital]
			-- , [Measures].[Capital],
			 [Measures].[Aexeo Pnl], 
			 [Measures].[WTD_REAL], 
			 [Measures].[MTD_REAL], 
			[Measures].[YTD_REAL]}';
			set @res = '
			--convert(bigint, convert(varchar, "[Measures].[TraderCapital]"))
			--,	convert(bigint, convert(varchar, "[Measures].[Capital]")),
			convert(float, convert(varchar, "[Measures].[Aexeo Pnl]"))
			  , convert(float, convert(varchar, "[Measures].[WTD_REAL]"))
			,   convert(float, convert(varchar, "[Measures].[MTD_REAL]")),
			  convert(float, convert(varchar, "[Measures].[YTD_REAL]"))';
		end
		else
		begin
			set @row = '{
			--[Measures].[TraderCapital]
			-- , [Measures].[Capital],
			 [Measures].[BPS]
			 , [Measures].[WTD_BPS]
			 , [Measures].[MTD_BPS],
			  [Measures].[YTD_BPS]}';
			set @res = '
			--convert(bigint, convert(varchar, "[Measures].[TraderCapital]"))
			--,	convert(bigint, convert(varchar, "[Measures].[Capital]")),
			convert(float, convert(varchar, "[Measures].[BPS]"))
			,   convert(float, convert(varchar, "[Measures].[WTD_BPS]"))
			,   convert(float, convert(varchar, "[Measures].[MTD_BPS]")),
			   convert(float, convert(varchar, "[Measures].[YTD_BPS]"))';
		end

		declare @levelConvert VARCHAR(150), @levelAxis VARCHAR(150), @assetAxis VARCHAR(150)
		
		set @assetAxis = CASE WHEN IsNull(@asset, '[All]')='[All]' THEN
								',[FIG Tags Link].[FIG Asset Class].[FIG Asset Class].AllMembers'
						 ELSE ',[FIG Tags Link].[FIG Asset Class].&['+ @asset +']'
						 END

		if(@level = 0)
		BEGIN
			SET @levelConvert = ',   convert(varchar, "[FIG Tags Link].[FIG Sector].[FIG Sector].[MEMBER_CAPTION]")';
			SET @levelAxis = CASE WHEN IsNull(@sector, '[All]')='[All]' THEN
									',[FIG Tags Link].[FIG Sector].[FIG Sector].AllMembers'
							 ELSE ',[FIG Tags Link].[FIG Sector].&[' + @sector + ']'
							 END
		END
		ELSE
		BEGIN
			SET @levelConvert = ',   convert(varchar, "[FIG Tags Link].[FIG Region Ex].[FIG Region Ex].[MEMBER_CAPTION]")';
			SET @levelAxis = CASE WHEN IsNull(@region, '[All]')='[All]' THEN
									',[FIG Tags Link].[FIG Region Ex].[FIG Region Ex].AllMembers'
							 ELSE ',[FIG Tags Link].[FIG Region Ex].&[' + @region + ']'
							 END
		END
		
		IF (@fund is not null)
		BEGIN
			SET @levelConvert = '';
			SET @levelAxis = '';
		END

		SET @qry = 'select
			convert(bigint, convert(varchar, "[Instrument].[Id].[Id].[MEMBER_CAPTION]"))
			,	convert(int, convert(varchar, "[Strategy].[Strategy].[Strategy].[MEMBER_CAPTION]"))
			,	convert(varchar, "[FIG Tags Link].[FIG Asset Class].[FIG Asset Class].[MEMBER_CAPTION]")
			' + @levelConvert + '
			, '+@res+ ' 
		from openquery(CUBE1_OLAP, ''select ' + @row + ' on 0,
		nonempty(
			CrossJoin([Instrument].[Id].[Id].AllMembers,
				[Strategy].[Strategy].[Strategy].AllMembers
					' + @assetAxis + '
						' + @levelAxis + '
					)
			, [Measures].[YTD_REAL]) on 1
		from [Pnl Data]
		where [Time].[Year -  Quarter -  Month -  Date].[Date].&[' + @dateStr + 'T00:00:00]' +
		isNull(@fundPart,'') + isNull(@traderPart,'') + ''')';
		
		BEGIN TRY
			--PRINT @qry
			IF (@fund is null)
				insert into #pnl_bps(tid, strategy, asset, level1, dtd, wtd, mtd, ytd)
				EXEC(@qry)
			ELSE
				insert into #pnl_bps(tid, strategy, asset, dtd, wtd, mtd, ytd)
				EXEC(@qry)
		END TRY
		BEGIN CATCH
			DECLARE 
			@ErrorNum as int
			, @ErrorMessage nvarchar(4000)
			, @ErrorSeverity int
			, @ErrorState Int
			, @ErrorProcedure nvarchar(2000)
			, @ErrorLine int;
			
			SELECT @ErrorNum=ERROR_NUMBER(),
			@ErrorSeverity=ERROR_SEVERITY(),
			@ErrorLine=ERROR_LINE(),
			@ErrorProcedure=ERROR_PROCEDURE(),
			@ErrorMessage=ERROR_MESSAGE(),
			@ErrorState=ERROR_STATE();
			
			IF (@ErrorNum<>213 AND @ErrorNum<>207) -- Will be thrown when MDX returns empty result
			BEGIN
			RAISERROR(
					@ErrorMessage,
					@ErrorSeverity,
					1,
					@ErrorNum,
					@ErrorSeverity,
					@ErrorState,
					@ErrorProcedure,
					@ErrorLine
					)
			END
		END CATCH;
		
	--select * from #pnl_bps



		SELECT distinct
		s.strategy AS [Strategy]
		, pnl.tid AS [TID]
		, i.Name AS [SecurityName]
		, i.instrument_type AS [SecurityType]
		, pnl.asset AS [FIG_Asset_Class]	
		, CASE WHEN @fund is null THEN
			pnl.level1 collate Latin1_General_BIN
		  ELSE 
			CASE WHEN (@level=0) THEN fig.FIG_Sector collate Latin1_General_BIN
			ELSE fig.FIG_RegionEx collate Latin1_General_BIN
			END
		  END AS [Level]
		, c.name AS [Country]
		, pnl.dtd AS [DayToDay] 
		, pnl.wtd as [WeekToDay]
		, pnl.mtd AS [MonthToDay]
		, pnl.ytd AS [YearToDay]
		FROM #pnl_bps pnl
		JOIN CUBE1.Forensics.dbo.strategy s on s.strategy_id=pnl.strategy
		JOIN CUBE1.Forensics.dbo.instrument i on pnl.tid=i.Id
		LEFT JOIN MPS.y_assets.dbo.country c on c.country_id=i.risk_country_id
		LEFT JOIN CUBE1.Forensics.dbo.fig_instrument_group fig on 
			fig.Id=pnl.tid AND fig.strategy_id=pnl.strategy AND fig.fundId=convert(int, @fund)
		WHERE 
		@fund is null AND
		(@level=0 AND (IsNull(@sector, '[All]')='[All]' OR pnl.level1=@sector)
		OR
		@level=1 AND (IsNull(@region, '[All]')='[All]' OR pnl.level1=@region))
		OR
		@fund is not null AND
		(@level=0 AND (IsNull(@sector, '[All]')='[All]' OR fig.FIG_Sector=@sector)
		OR
		@level=1 AND (IsNull(@region, '[All]')='[All]' OR fig.FIG_RegionEx=@region))
		ORDER BY [Strategy], [TID], FIG_Asset_Class
		
		DROP TABLE  #pnl_bps
	END
	
END
