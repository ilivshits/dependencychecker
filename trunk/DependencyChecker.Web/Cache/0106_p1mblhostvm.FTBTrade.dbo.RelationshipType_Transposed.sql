CREATE VIEW [dbo].[RelationshipType_Transposed] WITH SCHEMABINDING AS
SELECT
[Template] = 1
, [Tagging Auto-Enrichment Identifier Proxy] = 2
, [Restriction] = 3

