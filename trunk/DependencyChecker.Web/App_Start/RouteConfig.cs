﻿using System.Web.Mvc;
using System.Web.Routing;

namespace DependencyChecker.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Main",
                url: "",
                defaults: new { controller = "Main", action = "Index" }
            );

            routes.MapRoute(
                name: "Cleanup unused for scope",
                url: "cleanup/{id}.zip",
                defaults: new { controller = "Main", action = "DownloadCleanup", gens = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Cleanup unused set",
                url: "cleanupset",
                defaults: new { controller = "Main", action = "DownloadSetCleanup" }
            );

            routes.MapRoute(
                name: "Download",
                url: "download/{id}",
                defaults: new { controller = "Main", action = "DownloadDetails" }
            );

            routes.MapRoute(
                name: "DownloadFile",
                url: "downloadzip/{id}",
                defaults: new { controller = "Main", action = "DownloadFile" }
            );

            routes.MapRoute(
                name: "Export all",
                url: "exportall/{id}.zip",
                defaults: new { controller = "Main", action = "ExportAll" }
            );

            routes.MapRoute(
                name: "Manage Cleanup",
                url: "buildcleanup/{id}",
                defaults: new { controller = "Main", action = "Cleanup" }
            );

            routes.MapRoute(
                name: "Get Status",
                url: "getstatus",
                defaults: new { controller = "Main", action = "GetStatus" }
            );

            routes.MapRoute(
                name: "Get Batch Entities",
                url: "GetBatchEntities/{batchId}",
                defaults: new { controller = "Main", action = "GetBatchEntities" }
            );

            routes.MapRoute(
                name: "Get Entity Errors",
                url: "GetEntityErrors/{batchId}&{entityName}",
                defaults: new { controller = "Main", action = "GetEntityErrors" }
            );

            routes.MapRoute(
                name: "Get Batch Exceptions",
                url: "GetBatchExceptions/{batchId}",
                defaults: new { controller = "Main", action = "GetBatchExceptions" }
            );

            routes.MapRoute(
                name: "Error",
                url: "Error",
                defaults: new { controller = "Error", action = "General" }
            );

            routes.MapRoute(
                name: "Control Panel default",
                url: "ControlPanel/{action}",
                defaults: new { controller = "ControlPanel", action = "Index" }
            );

            //routes.MapRoute(
            //    name: "",
            //    url: "ControlPanel/{action}",
            //    defaults: new { controller = "ControlPanel", action = "Index" }
            //);

            routes.MapRoute(
                name: "Check User Existence",
                url: "ControlPanel/CheckUserExistence/{username}",
                defaults: new { controller = "ControlPanel", action = "CheckUserExistence" }
            );

            routes.MapRoute(
                name: "Add admin",
                url: "ControlPanel/AddNewAdministrator/",
                defaults: new { controller = "ControlPanel", action = "AddNewAdministrator" }
            );

            routes.MapRoute(
                name: "Remove user",
                url: "ControlPanel/RemoveUser/",
                defaults: new { controller = "ControlPanel", action = "RemoveUser" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{action}/{id}",
                defaults: new { controller = "Main", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}