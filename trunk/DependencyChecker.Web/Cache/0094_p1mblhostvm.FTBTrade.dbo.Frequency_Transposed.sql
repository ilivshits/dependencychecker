CREATE VIEW [dbo].[Frequency_Transposed] AS
SELECT
[Annual] = 1
, [Semi Annual] = 2
, [Quarterly] = 3
, [Monthly] = 4
, [Biweekly] = 5
, [Weekly] = 6
, [28 Days] = 7
, [35 Days] = 8
, [42 Days] = 9
, [Daily] = 10
, [Continuous] = 11
, [At Maturity] = 12
, [21 Days] = 13
, [Bimonthly] = 14
, [Four Months] = 15
, [49 Days] = 16
, [7 Days] = 17


