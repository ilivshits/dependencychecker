
CREATE view [report].[ItalyOrders] as
select [Order Id] = o.orderId, Manager = m.description, Security = s.description 
from [Order] o
join [Manager] m with(nolock) on o.managerId = m.managerId
join [Security] s with(nolock) on o.sid = s.sid
join [Issuer] i with(nolock) on s.issuerId = i.issuerId
join [Exchange] e with(nolock) on s.exchangeId = e.exchangeId
where o.tradeDate > dateadd(day, -7, getdate())
	  and o.orderStatusId in (1, 2) and (i.domicileCountryId = 110 or i.incorporationCountryId = 110 or i.riskCountryId = 110 or e.countryId = 110)
      and s.securityPrimaryTypeId in (3, 7, 8, 10, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 25, 26, 28, 29, 30, 31, 32, 35, 36, 106, 112)



