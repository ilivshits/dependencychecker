﻿
    var entityMap = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': '&quot;',
        "'": '&#39;',
        "/": '&#x2F;'
    };

    function escapeHtml(string) {
        return String(string).replace(/[&<>"'\/]/g, function(s) {
            return entityMap[s];
        });
    }

    var DC = {};
    DC.ControlPanel = {};


    DC.ControlPanel.ExceptionLog = new function() {
        var _batches = null;
        var _jBox = undefined;
        var me = this;

        var ErrorTypes = {
            ToFrom: 'Error: "From date" is later than "To"',
            Empty: 'Error: Pick both dates, please'
        };

        var ExceptionTypes = {
            Debug: 'DEBUG',
            Info: 'INFO',
            Warning: 'WARN',
            Error: 'ERROR',
            Fatal: 'FATAL'
        };

        var BatchStatus = {
            Failed: 0,
            Success: 1
        };

        var ListTypes = {
            Batch: 'Batch',
            Error: 'Error'
        };

        var getBatches = function(success) {
            if (_batches == null) {
                $("#loadingModal").modal('show');

                _batches = [];
                $.ajax({
                    type: 'GET',
                    url: 'GetBatches',
                    dataType: "json",
                    async: true,
                    contentType: "application/json",
                    success: function(e) {
                        $.each(e.result, function(index, value) {

                            _batches.push({
                                id: value.Id,
                                date: new Date(value.Date),
                                user: value.Username,
                                status: value.Status
                            });

                        });

                        if (success) {
                            success(_batches);
                            $("#loadingModal").modal('hide');
                        } else {
                            console.error('Define success function when calling getBatches');
                        }
                    },
                    error: function(msg) {
                        $("#loadingModal").modal('hide');

                        alert('load failed: \r\n' + msg.responseText);
                    }
                });
            } else {
                success(_batches);
            }
        };

        var getBatchDOMListFromArray = function(batchArray) {
            var list = '';
            $.each(batchArray, function(index, value) {

                var yyyy, MM, dd, HH, mm, ss;

                yyyy = value.date.getFullYear();

                MM = (value.date.getMonth() + 1);
                MM = ("0" + MM).slice(-2);

                dd = value.date.getDate();
                dd = ("0" + dd).slice(-2);


                HH = value.date.getHours();
                HH = ("0" + HH).slice(-2);

                mm = value.date.getMinutes();
                mm = ("0" + mm).slice(-2);

                ss = value.date.getSeconds();
                ss = ("0" + ss).slice(-2);

                if (value.status == BatchStatus.Failed) {
                    list += '<li class="list-group-item" data-id="' + value.id + '" data-status="' + value.status + '">\
                        <div class="form-group">\
                            <button type="button" class="btn-default btn-xs">+</button>\
                            <span class="label t label-primary" style="margin-right: 11px;background-color: red;">Batch</span><span> ID: \'' + value.id +
                        '\'; Ran \'' + MM + '.' + dd + '.' + yyyy +
                        ' ' + HH + ':' + mm + ':' + ss +
                        ' by ' + value.user + '\'</span><span style="color: red;">  FAILED</span>\
                        </div>\
                    </li>';
                } else {
                    list += '<li class="list-group-item batch-success" data-id="' + value.id + '" data-status="' + value.status + '">\
                        <div class="form-group">\
                            <button type="button" class="btn-default btn-xs">+</button>\
                            <span class="label t label-primary" style="margin-right: 11px;" >Batch</span><span> ID: \'' + value.id +
                        '\'; Ran \'' + MM + '.' + dd + '.' + yyyy +
                        ' ' + HH + ':' + mm + ':' + ss +
                        ' by ' + value.user + '\'</span>\
                        </div>\
                    </li>';
                }
            });

            return list;
        };

        var getBatchesList = function(batches, fromDate, toDate) {

            if (!fromDate || !toDate) {
                return getBatchDOMListFromArray(batches);
            } else {
                fromDate.setHours(0, 0, 0, 0);
                var fromMillis = fromDate.getTime();
                toDate.setHours(0, 0, 0, 0);
                var toMillis = toDate.getTime();

                var filtered = batches.filter(function(batch) {

                    var date = new Date(batch.date);
                    date.setHours(0, 0, 0, 0);
                    var millis = date.getTime();

                    return millis >= fromMillis && millis <= toMillis;
                });

                return getBatchDOMListFromArray(filtered);
            }
        };


        var countErrorsOfType = function(array, type) {
            var count = 0;
            $.each(array, function(index, value) {
                if (value.Level == type) {
                    count++;
                }
            });
            return count;
        };

        var setExpandEvents = function() {
            setExpandListEvent($('.form-group>button'), 'GetBatchExceptions', ListTypes.Error, function(e, $fgroup) {
                var list = '';
                if (e.result.length != 0) {
                    list += '<div class="filter btn-group type" data-toggle="buttons" style="margin-bottom: 20px;">' +
                        '<span>Filter by level: </span>' +
                        '<label class="btn btn-default btn-xs active"><input name="WARN" type="checkbox" checked="checked">Warning &nbsp;<span class="badge pull-right">' + countErrorsOfType(e.result, ExceptionTypes.Warning) + '</span></label>' +
                        '<label class="btn btn-default btn-xs active"><input name="ERROR" type="checkbox" checked="checked">Error &nbsp;<span class="badge pull-right">' + countErrorsOfType(e.result, ExceptionTypes.Error) + '</span></label>' +
                        '<label class="btn btn-default btn-xs active"><input name="FATAL" type="checkbox" checked="checked">Fatal &nbsp;<span class="badge pull-right">' + countErrorsOfType(e.result, ExceptionTypes.Fatal) + '</span></label>' +
                        '</div>';
                }
                $.each(e.result, function(index, value) {
                    var levelInfo = '';
                    if (value.Level == ExceptionTypes.Warning) {
                        levelInfo = '<span class="label t label-info" style="margin-right: 15px;background-color: #ffa51f;">Warning</span>';
                    } else if (value.Level == ExceptionTypes.Error) {
                        levelInfo = '<span class="label t label-info" style="margin-right: 15px;background-color: red;">Error</span>';
                    } else if (value.Level == ExceptionTypes.Fatal) {
                        levelInfo = '<span class="label t label-info" style="margin-right: 15px;background-color: black;">Fatal</span>';
                    }


                    var disabled = 'disabled';
                    if (value.Exception != '' && value.Exception != undefined && value.Exception != null) {
                        disabled = '';
                    }


                    var escMsg = escapeHtml(value.Message);
                    list +=
                        '<li class="list-group-item" data-level="' + value.Level + '">' +
                        '<div class="form-group">' +
                        '<div class="left-align">' +
                        '<button type="button" class="btn-default btn-xs ' + disabled + '" style="margin-right: 4px;"' + disabled + '>+</button>' + levelInfo +
                        '</div>' +
                        '<span class="error-message j-box-tooltip" title="' + escMsg + '">' + escMsg + '</span>' +
                        '</div>' +
                        '<span class="error-exception" style="display: none;">' + value.Exception + '</span>' +
                        '</li>';
                });

                $fgroup.append('<div class="details"></div>');
                $fgroup.find('.details').html(list);


                if (!_jBox) {
                    _jBox = new jBox('Tooltip', {
                        attach: $('.j-box-tooltip'),
                        closeOnMouseleave: true,
                        maxWidth: '500px'
                    });
                } else {
                    _jBox.attach($('.j-box-tooltip'));
                }


                $('.filter input').change(function() {
                    var $cbox = $(this);

                    if ($cbox.prop('checked')) {
                        $cbox.closest('.details').find('li[data-level="' + $cbox.attr('name') + '"]').show();
                    } else {
                        $cbox.closest('.details').find('li[data-level="' + $cbox.attr('name') + '"]').hide();
                    }
                });

                $fgroup.children('button').text('-');

                $fgroup.find('.details button').click(function() {
                    var $btn = $(this);
                    var $ex = $btn.closest('.list-group-item').find('.error-exception');
                    if ($btn.text() == '-') {
                        $ex.hide();
                        $btn.text('+');
                    } else {
                        $ex.show();
                        $btn.text('-');
                    }
                });

            }, function(msg) {
                alert('load failed: \r\n' + msg.responseText);
            });
        };

        var setExpandListEvent = function(element, methodName, type, success, error) {
            element.click(function() {
                var $btn = $(this);
                var $fgroup = $(this).parent();

                if ($btn.text() == '-') {
                    $fgroup.children('.details').hide();
                    $btn.text('+');
                } else {
                    var params = {};
                    if (type == ListTypes.Error) {
                        params.batchId = +$fgroup.parent().attr('data-id');
                    }

                    if ($fgroup.find('.details').length == 0) {
                        $.ajax({
                            type: 'GET',
                            url: '' + methodName,
                            dataType: "json",
                            data: params,
                            async: true,
                            contentType: "application/json",
                            success: function(e) {
                                success(e, $fgroup);
                            },
                            error: error
                        });
                    } else {
                        $fgroup.children('.details').show();
                        $btn.text('-');
                    }
                }
            });
        };

        this.init = function() {

            $(document).ready(function() {

                $(".btn-group button").mouseup(function() {
                    $(this).blur();
                });
                var date = new Date();
                date.setDate(date.getDate() - 7);

                $('#from-date-picker').datetimepicker({
                    format: 'MM.DD.YYYY',
                    defaultDate: date
                });


                $('#to-date-picker').datetimepicker({
                    format: 'MM.DD.YYYY',
                    defaultDate: new Date()
                });

                $('#from-date-picker').on('dp.change', function(e) {
                    var minDate = $(this).data("DateTimePicker").date().toDate();
                    minDate.setHours(0, 0, 0, 0);
                    $('#to-date-picker').data("DateTimePicker").minDate(minDate);
                });
                $('#to-date-picker').on('dp.change', function(e) {
                    var maxDate = $(this).data("DateTimePicker").date().toDate();
                    maxDate.setHours(0, 0, 0, 0);
                    $('#from-date-picker').data("DateTimePicker").maxDate(maxDate);
                });

                $('#show-all').click(function() {
                    $('#date-error').hide();
                    getBatches(function(batches) {
                        var list = '';
                        list = getBatchesList(batches);
                        $('#results').html(list);

                        setExpandEvents();
                    });
                });
                $('#search-by-date').click(function() {
                    $('#date-error').hide();
                    var fromDatePicker = $("#from-date-picker").data("DateTimePicker").date();
                    var toDatePicker = $("#to-date-picker").data("DateTimePicker").date();

                    if (!fromDatePicker || !toDatePicker) {
                        $('#date-error').text(ErrorTypes.Empty);
                        $('#date-error').attr('title', ErrorTypes.Empty);
                        $('#date-error').show();
                        return;
                    }

                    var fromDate = fromDatePicker.toDate();
                    var toDate = toDatePicker.toDate();

                    getBatches(function(batches) {
                        var list = '';
                        list = getBatchesList(batches, fromDate, toDate);
                        $('#results').html(list);

                        setExpandEvents();
                    });
                });

                var bId = sessionStorage.getItem('da-exception-batch-id');
                if (bId != null) {
                    sessionStorage.clear();
                    getBatches(function(batches) {
                        var bat = batches.filter(function(batch) {
                            if (batch.id == bId) {
                                return batch;
                            }
                        });

                        if (bat.length != 0) {
                            var list = '';
                            list = getBatchesList(bat);
                            $('#results').html(list);

                            setExpandEvents();
                        }
                    });
                }
            });
        }
    };

    DC.ControlPanel.Users = new function() {
        function setLoginFieldValid(isValid) {
            if (!isValid) {
                $('#addNewUserModal .modal-body').addClass('has-error');
                $('#addNewUserModal .modal-body').removeClass('has-success');
            } else {
                $('#addNewUserModal .modal-body').removeClass('has-error');
                $('#addNewUserModal .modal-body').removeClass('has-success');
            }
        }

        this.init = function() {
            $(document).ready(function() {
                $('.removeUserBtn').click(function(e) {
                    e.preventDefault();
                    var areYouSure = confirm('Are you sure want to remove user? It can be you.');

                    if (areYouSure) {
                        $('#loadingModal').modal('show');
                        var uname = $(this).closest('.table-row').find('.username').text();
                        $.ajax({
                            type: 'POST',
                            url: 'RemoveUser',
                            data: { 'username': uname },
                            success: function() {
                                $('#loadingModal').modal('hide');
                                location.reload();
                            },
                            error: function(msg) {
                                alert('Can\'t remove user: \r\n' + msg.responseText);
                                $('#loadingModal').modal('hide');
                            }
                        });
                        //location.reload();
                    }
                });

                $('#addNewUserBtn').click(function() {
                    var uname = $('#userNameInput').val();
                    if (uname == '') {
                        setLoginFieldValid(false);
                        return;
                    }

                    $.ajax({
                        type: 'POST',
                        url: 'AddNewAdministrator',
                        data: { 'username': uname },
                        success: function() {
                            $('#userNameInput').val('');
                            $('#addNewUserModal').modal('hide');
                            location.reload();
                        },
                        error: function(msg) {
                            alert('Can\'t add new user: \r\n' + msg.responseText);
                            $('#loadingModal').modal('hide');
                        }
                    });

                });

                $('#userNameInput').focus(function() {
                    setLoginFieldValid(true);
                });

                $('#checkUser').click(function() {

                    var uname = $('#userNameInput').val();
                    if (uname == '') {
                        setLoginFieldValid(false);
                        return;
                    }

                    $('#loadingModal').modal('show');
                    $.ajax({
                        type: 'GET',
                        url: 'CheckUserExistence',
                        dataType: "json",
                        async: false,

                        data: {
                            'username': uname
                        },
                        success: function(isExist) {

                            if (!isExist) {
                                setLoginFieldValid(false);
                            } else {
                                $('#addNewUserModal .modal-body').addClass('has-success');
                            }

                            $('#loadingModal').modal('hide');
                        },
                        error: function(msg) {
                            alert('Load failed: \r\n' + msg.responseText);
                            $('#loadingModal').modal('hide');
                        }
                    });
                });
            });
        }
    };

    DC.ControlPanel.Home = new function() {
        this.init = function() {
            $(document).ready(function() {
                $('#restartSvcBtn').click(function(e) {
                    e.preventDefault();
                    var areUSure = confirm('Are you sure want to restart crawling? It may take long time.');
                    if (areUSure) {
                        location.href = this.href;
                    }
                });
            });
        }
    }
