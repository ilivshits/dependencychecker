CREATE VIEW [dbo].[Module_Transposed] AS
SELECT
[Securities] = 1
, [Orders] = 2
, [Operation Management] = 3
, [Configuration] = 7
, [RTL Management] = 8
, [Tagging] = 9
, [SHORES] = 10
, [Analytics Management] = 11
, [Financing Tool] = 12
, [Permissions] = 13
, [Portfolio Live] = 14
, [STP] = 15
, [Portfolio] = 17
, [PnL Suite] = 18
, [Subscriptions] = 19
, [POne Mobile] = 100001
, [Regulatory] = 100002


