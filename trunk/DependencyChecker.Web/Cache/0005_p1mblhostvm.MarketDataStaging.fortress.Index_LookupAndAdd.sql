

CREATE PROCEDURE [fortress].[Index_LookupAndAdd]
	@identifierTypeId int,
	@code varchar(150),
	@application_source as varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- XACT_ABORT is used to abort the transaction in the face of
	-- anything.
	SET XACT_ABORT, NOCOUNT ON;

	--- The error.
	declare @error varchar(1000) = 
		'The identifierTypeId parameter of ' + coalesce(ltrim(str(@identifierTypeId)), '<null>') + ' is not supported.'

	--- Severity level.
	declare @severity int = 0

	--- Validate the identifier type id.
	declare @prefix varchar(100) = (select p.BloombergSapiIdPrefix from 
		[FTBTrade].[dbo_IdentifierType_MapFromBloombergSapiIdPrefix] as p where p.IdentifierTypeId = @identifierTypeId)
		
	--- If the prefix is null, it's an error.
	if (@prefix is null) begin raiserror(@error, @severity, 0) return end

	--- The fields to look up.
	declare @fields varchar(1500)
	SET @fields = 'ID_BB_UNIQUE,TICKER,MARKET_SECTOR_DES,LONG_COMP_NAME,NAME,ID_BB_GLOBAL,' +
		'CRNCY,ID_BB_COMPANY'

	--- Set up the table that will receive the data.
	select 1 a into #s;

	--- The tickers to be looked up.  There's only one here.
	select 1 as Ordinal, '/' + @prefix + '/' + @code as [bbg_ticker] into #tickers

	--- Make the call to get the data.
	exec SqlNativeDll.dbo_BDPTable 'select [bbg_ticker] from #tickers', '#s', @fields

	--- Clean up the tickers table.
	drop table #tickers

	--- The count.
	declare @count int = isnull((select count(*) from #s), 0)

	--- Set error.
	set @error = 'The call to SqlNativeDll.dbo_BDPTable returned an unexpected number of results (' + str(@count) + ').'

	--- If there is more than one value, raise an error.
	if (@count <> 1) begin raiserror(@error, @severity, 0) return end

	--- Assign values.
	declare @idBloombergUnique varchar(150) = (select ID_BB_UNIQUE from #s)

	--- Set error.
	set @error = 'The search on identifier ' + @code + ' produced no results.'

	--- If the unique value is wrong, raise an error.
	if (@idBloombergUnique is null) begin raiserror(@error, @severity, 0) return end

	--- Ids.
	declare @idBloombergCodeShort varchar(150) = (select TICKER from #s)
	set @error = 'Could not generate a Bloomberg short code; TICKER is null.'
	if (@idBloombergCodeShort is null) begin raiserror(@error, @severity, 0) return end

	declare @idBloombergCodeLong varchar(150) = (select TICKER + ' ' + MARKET_SECTOR_DES from #s)
	set @error = 'Could not generate a Bloomberg long code; TICKER and/or MARKET_SECTOR_DES are null.'
	if (@idBloombergCodeLong is null) begin raiserror(@error, @severity, 0) return end

	declare @idBloombergGlobal varchar(150) = (select ID_BB_GLOBAL from #s)
	set @error = 'Could not generate a Bloomberg global id; ID_BB_GLOBAL is null.'
	if (@idBloombergGlobal is null) begin raiserror(@error, @severity, 0) return end

	--- The description.
	declare @description varchar(150) = (select coalesce(LONG_COMP_NAME, NAME) from #s)
	set @error = 'Could not generate the description; LONG_COMP_NAME and NAME are null.'
	if (@description is null) begin raiserror(@error, @severity, 0) return end

	--- Currency.  Null is ok.
	declare @currencySid bigint = isnull((
		select
			c.sid	
		from
			#s as s
				inner join FTBTrade.dbo_Security as c with (nolock) on
					c.ticker = s.CRNCY collate database_default
				cross join fortress.v_SecurityPrimaryType_Pivot as spt
		where
			c.securityPrimaryTypeId = spt.Currency
	), 0)

	--- Issuer id, null is ok.
	declare @issuerId int = isnull((
		select
			i.issuerId
		from
			#s as s
				cross join fortress.v_IdentifierType_Pivot as it
				inner join FTBTrade.dbo_IssuerIdentifier as ii with (nolock) on
					ii.identifierTypeId = it.BloombergCompany and
					ii.code = s.ID_BB_COMPANY collate database_default
	), 0)			

	--- Clean up.
	drop table #s

	--- To generate the sid.
	declare @sidTable table (sid bigint not null)

	--- Add the records for the base security first.
	insert @sidTable
	exec fortress.Index_Add 
		0, --@dividendCurveId
		0, --@fundingCurveId,
		0, --@volSurfaceId,
		0, --@otcVolSurfaceId,
		@idBloombergCodeShort, @description, 
		@currencySid, 0, 9 /* @securityPrimaryTypeId = Index */, 
		0 /* @securitySubTypeId = none */, 
		'1900-01-01', '9999-12-31', 1, 0, 1, 1, 0, @issuerId, 
		null, null, @idBloombergCodeShort, @idBloombergCodeLong, null, 
		null, null, null, null, null, null, 
		@idBloombergGlobal, @idBloombergUnique, null, null, null, 
		null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, 
		null, null, null, null,
		@application_source
	
	--- Return the sid.
	select sid from @sidTable
END



