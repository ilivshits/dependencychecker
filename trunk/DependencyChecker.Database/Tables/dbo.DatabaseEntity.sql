﻿CREATE TABLE [dbo].[DatabaseEntity] (
    [Id]               UNIQUEIDENTIFIER       CONSTRAINT [FK_DbEntity_BaseEntity] FOREIGN KEY REFERENCES [dbo].[EntityBase] ([Id])  NOT NULL,
    
    [JobLastRunTime] DATETIME2 NULL, 
    [Database] NVARCHAR(500) NULL, 
    CONSTRAINT [PK_DatabaseEntity] PRIMARY KEY ([Id]),    
)

GO

