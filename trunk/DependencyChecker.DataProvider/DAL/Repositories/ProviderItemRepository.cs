﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DependencyChecker.Common;

namespace DependencyChecker.DataProvider.DAL.Repositories
{
    public class ProviderItemRepository
    {
        private DependencyCheckerEntities _context;

        public ProviderItemRepository(DependencyCheckerEntities context)
        {
            _context = context;
        }

        public Dictionary<string, List<string>> GetConfiguration()
        {
            var config = new Dictionary<string, List<string>>();
            try
            {
                var items = _context.ProviderItem.AsQueryable().Include(e => e.ProviderType).ToList();
                foreach (var item in items)
                {
                    string key = item.ProviderType.Name;
                    if (config.ContainsKey(key))
                    {
                        config[key].Add(item.Name);
                    }
                    else
                    {
                        config.Add(key, new List<string> { item.Name });
                    }
                }
            }
            catch (Exception e)
            {
                LoggerHelper.Log.Error(e);
                throw e;
            }
            
            

            return config;
        }
    }
}
