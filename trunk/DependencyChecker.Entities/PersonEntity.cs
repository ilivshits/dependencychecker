using System;
using System.Collections.Generic;
using DependencyChecker.Entities.Base;

namespace DependencyChecker.Entities
{
    public class PersonEntity : EntityBase
    {
        public static readonly string ProviderName = EntityProviderType.Person.ToString();
        private readonly string _domainName;

        public PersonEntity(string name, EntityType type)
            : base(name, String.Empty, type)
        {
        }

        public PersonEntity(string name) : base(name, String.Empty, EntityType.Person)
        {
        }

        public PersonEntity(string name, string definition, string domain = "") : base(name, definition, String.Empty, EntityType.Person)
        {
            _domainName = domain;
        }

        #region Overrides of EntityBase

        public override string InternalId { get; set; }

        public override List<string> SearchNames
        {
            get
            {
                return new List<string>{ Definition, _domainName + "\\" + Definition, Name };
            }
        }

        public override String ScopeDelimiter
        {
            get
            {
                return "\\";
            }
        }

        protected override List<SearchContent> GetSearchContent()
        {
            return new List<SearchContent>();
        }

        #endregion
    }
}