CREATE VIEW [dbo].[ValuationMode_Transposed] WITH SCHEMABINDING AS
SELECT
[Default] = 1
, [Live] = 2
, [Valuation] = 3
, [Mips] = 4

