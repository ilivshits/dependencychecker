

CREATE view [dbo].[AbsTermSheet] as
select
	s.sid,
	s.description,
	s.securityPrimaryTypeId,
	s.securitySubTypeId,
	domicileCountry = c.code2,
	a.faceValue,
	a.coupon,
	a.couponTypeId,
	a.spread,
	a.daysToSettle,
	a.frequencyId,
	a.dayCountConventionId,
	maturityDate = isnull(a.maturityDate, a.finalMaturityDate),
	a.firstCouponDate,
	a.nextCouponDate,
	a.previousCouponDate,
	a.resetIndexSid,
	a.floaterFlag,
	a.issueDate,
	d.eomFlag,
	a.manualFlag,
	a.version,
	tv1.value as cycleStartDate,
	tv2.value as sifmaCalendar

from [Security] s
join [Issuer] i on s.issuerId = i.issuerId join [Country] c on i.domicileCountryId = c.countryId
join [AssetBacked] a on s.sid = a.sid
join [DayCountConvention] d on a.dayCountConventionId = d.dayCountConventionId
left join [tagging].[Tag] t1 on t1.description = 'ABS Accrual - Cycle Start Day'
left join [tagging].[Value] tv1 on tv1.tagId = t1.tagId and s.sid = tv1.objectId
left join [tagging].[Tag] t2 on t2.description = 'ABS Accrual - SIFMA Calendar'
left join [tagging].[Value] tv2 on tv2.tagId = t2.tagId and s.sid = tv2.objectId



