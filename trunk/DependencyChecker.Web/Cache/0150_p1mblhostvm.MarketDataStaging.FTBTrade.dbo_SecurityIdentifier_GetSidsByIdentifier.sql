
CREATE PROCEDURE [FTBTrade].[dbo_SecurityIdentifier_GetSidsByIdentifier]
	@identifierTypeId int,
	@code varchar(100)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--- Get the sid(s) of the securities that have the bloomberg
	--- unique id.
	select 
		si.sid 
	from 
		FTBTrade.dbo_SecurityIdentifier as si with (nolock)
	where  
		si.identifierTypeId = @identifierTypeId and
		si.code = @code



