﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Reflection;
using DependencyChecker.Common;

namespace DependencyChecker.Entities.Utils
{
    public class ZipPacker
    {
        public static Package Create(Stream packageStream)
        {
            return Package.Open(packageStream, FileMode.OpenOrCreate);
        }

        public static void AddFile(Package package, string fileName, byte[] fileContent, string contentType = "file/file")
        {
            var uri = PackUriHelper.CreatePartUri(new Uri(fileName, UriKind.Relative));
            try
            {
                var part = package.CreatePart(uri, contentType);
                if (part == null)
                {
                    throw new ArgumentException("cannot create part for the package");
                }
                var partStream = part.GetStream();
                partStream.Write(fileContent, 0, fileContent.Length);
            }
            catch (Exception ex)
            {
                LoggerHelper.Log.ErrorFormat("Error packing file {0}. Exception details: {1}", uri, ex);
                throw;
            }
        }

        public static byte[] GeneratePackage(string packageName, List<String> files, string folderName = "", bool shortPath = false)
        {
            try
            {
                var uniqueFiles = files.Distinct(new InvariantComparer()).ToList();

                using (var packageStream = new MemoryStream())
                {
                    using (var package = Create(packageStream))
                    {
                        LoggerHelper.Log.InfoFormat("Packing files to ZIP:\r\n{0}", String.Join("\r\n", uniqueFiles.ToArray()));

                        foreach (var file in uniqueFiles)
                        {
                            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file);

                            

                            if (!File.Exists(filePath))
                            {
                                LoggerHelper.Log.WarnFormat("File '{0}' was not found. Skipped for package {1}.", filePath, packageName);
                            }
                            else
                            {
                                var fileName = shortPath ? Path.GetFileName(file) : file;

                                if (!String.IsNullOrEmpty(folderName))
                                {
                                    fileName = String.Concat(folderName, "\\", fileName);
                                }
                                
                                AddFile(package, fileName,
                                    File.ReadAllBytes(filePath));
                            }
                        }
                        package.Flush();
                        return packageStream.ToArray();
                    }
                }
            }
            catch (Exception exception)
            {
                LoggerHelper.Log.ErrorFormat("Error occured while generating package '{0}'. Exception details: {1}", packageName, exception);
                throw;
            }
        }

        private static void CopyStream(Stream source, Stream target)
        {
            const int bufSize = 0x1000;
            var buf = new byte[bufSize];
            int bytesRead;
            while ((bytesRead = source.Read(buf, 0, bufSize)) > 0)
                target.Write(buf, 0, bytesRead);
        }
    }
}
