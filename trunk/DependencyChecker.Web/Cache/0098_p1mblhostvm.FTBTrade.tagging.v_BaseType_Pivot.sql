
CREATE view [tagging].[v_BaseType_Pivot] as
	select
		 1 as Text,
		 2 as DateTime,
		 3 as Bool,
		 4 as Integer,
		 5 as Decimal, 
		 6 as SystemList,
		 7 as UserList,
		 8 as Date,
		 9 as Time,
		 10 as XML



