﻿CREATE PROCEDURE [dbo].[cs_imp_fund_update]
								@p_strExtSys        varchar(20),
								@p_intRunID         decimal(18,0),
								@p_importUserId  		varchar(30),
								@p_startOfDay       char(1),
				        @p_strDefaultManager  varchar(30),
								@p_strDumpTiming    varchar(1),
								@strAllow_Del       char(1),
								@enabled247				char(1)
WITH RECOMPILE
AS

DECLARE @intError       int             -- Used to capture the value of @@error
DECLARE @strLastOp      varchar(100)    -- Brief description of last operation performed - used for error reporting
DECLARE @intInitImpCount  int           -- Number of rows in import table
DECLARE @intFinalImpCount int           -- POST VALIDATION COUNT
DECLARE @intInsertCount   int           -- number of new rows inserted
DECLARE @intUpdateCount   int           -- number of old rows updated
DECLARE @intCountTotal    int           -- Number of rows in import table
DECLARE @intRows          int           -- Number of rows an operation manipulated
DECLARE @dtmRunTime     datetime        -- Used to measure performance
DECLARE @timeValue      dateTime    		-- Stores current time used to calculate timing info to log
DECLARE @strIMP_CD      varchar(9)      -- Holds constant for message log prefix code
DECLARE @strIMP_TABLE   varchar(40)     -- Holds name of import table for use in message text
DECLARE @strMAIN_TABLE  varchar(40)
DECLARE @informalTableName  varchar(40)
DECLARE @strAddNewManagers  varchar(1)  -- switch for handling new manager values
DECLARE @crrncyCd  					varchar(3)  -- default currency code
DECLARE @managerJumpCode    varchar(3)  -- controls re-entry to bypass manager translation
DECLARE @firstSeqNum    numeric(18,0)   -- Used to insert new records into fund table
DECLARE @increment      numeric(10,0)   -- the number of new rows we need acct_id's for.
DECLARE @tmpTable       varchar(100)    -- used to store temp table name
DECLARE @sql            nvarchar(4000) -- used to store dynamic sql statement

-- column update variables (fund), ALPHABETICAL
DECLARE @intACCRUED_EXP_INCM int
DECLARE @intACCRUED_INT_INCM int
DECLARE @intACCT_CD int
DECLARE @intACCT_ID int
DECLARE @intACCT_NAME int
DECLARE @intACCT_SHT_NAME int
DECLARE @intACCT_TYP_CD int
DECLARE @intACTIVE int
DECLARE @intALLOW_AUTOFX int
DECLARE @intALLOW_BUY int
DECLARE @intALLOW_NEG_CASH int
DECLARE @intALLOW_SELL int
DECLARE @intAMRTZD_COST int
DECLARE @intAPPLY_GUIDELINES int
DECLARE @intAUTH_GROUP int
DECLARE @intAUTH_NUM int
DECLARE @intAUTO_CFWD_IND int
DECLARE @intAVG_COST int
DECLARE @intBANK_ACCT_NUM int
DECLARE @intBASKET_TYP_CD int
DECLARE @intBLOCK_ORDER_IND int
DECLARE @intBUNDLED_FEE_IND int
DECLARE @intCASH_BAL_SOD int
DECLARE @intCASH_RSRV_AMT int
DECLARE @intCASH_RSRV_PCT int
DECLARE @intCASH_RSRV_TYP int
DECLARE @intCATEGORY1 int
DECLARE @intCATEGORY2 int
DECLARE @intCATEGORY3 int
DECLARE @intCFWD_CLOSE_CPARTY_IND int
DECLARE @intCFWD_NEW_CPARTY_IND int
DECLARE @intCG_TAX_RATE int
DECLARE @intCLOSE_DATE int
DECLARE @intCNTRBS int
DECLARE @intCNTRY_CD int
DECLARE @intCOMMENTS int
DECLARE @intCOMP_BID_REQ int
DECLARE @intCOMPLIANCE_TOLERANCE int
DECLARE @intCPL_BATCH_RUNTIME int
DECLARE @intCREATE_DATE int
DECLARE @intCROSS_EXT_ACCT_CD int
DECLARE @intCROSS_GRP_ACCT_CD int
DECLARE @intCROSS_INT_ACCT_CD int
DECLARE @intCROSS_MAG_ACCT_CD int
DECLARE @intCRRNCY_CD int
DECLARE @intCUR_INVESTED int
DECLARE @intCUR_MKT_VAL int
DECLARE @intCUSTODIAN_CD int
DECLARE @intDEF_BENCH_CD int
DECLARE @intDEF_BENCH_ID int
DECLARE @intDEF_BENCH_TYPE int
DECLARE @intDEF_FWD_DATE int
DECLARE @intDEF_FWD_DAYS int
DECLARE @intDEF_FWD_DAYS_IND int
DECLARE @intDEF_HEDGE_BENCH_CD int
DECLARE @intDEF_HEDGE_BENCH_ID int
DECLARE @intDEF_HEDGE_BENCH_TYPE int
DECLARE @intDEF_HEDGE_MDL_CD int
DECLARE @intDEF_HEDGE_MDL_ID int
DECLARE @intDEF_HEDGE_MDL_TYPE int
DECLARE @intDEF_MDL_ID int
DECLARE @intDEF_MDL_REM_IND int
DECLARE @intDEF_MDL_TYPE int
DECLARE @intDEFAULT_GRP_CD int
DECLARE @intDEFAULT_MODEL_CD int
DECLARE @intDEFAULT_SPECIAL_INST int
DECLARE @intDEFAULT_TMP_ACCT int
DECLARE @intDISC_ACCRUAL_CNVTN int
DECLARE @intDIV_PAYBL int
DECLARE @intDIV_RCVBLE int
DECLARE @intDIV_RECEIVED int
DECLARE @intDM_IND int
DECLARE @intEQTY_ACCT int
DECLARE @intERISA_ELIGIBLE int
DECLARE @intEX_ANTE_CALCS_METHOD_CD int
DECLARE @intEXCH_LIST_CD int
DECLARE @intFACTOR_MODEL int
DECLARE @intFEE_PAYMT_IND int
DECLARE @intFUND_ACCT int
DECLARE @intFUND_SHRS_OUTST int
DECLARE @intFUT_BROKER_IND int
DECLARE @intGAIN_LOSS_SENSITIVE int
DECLARE @intGROSS_INCOME int
DECLARE @intGRP_TYPE_CD int
DECLARE @intINACTIVE int
DECLARE @intINC_TAX_RATE int
DECLARE @intINCL_ACCRD_INTEREST int
DECLARE @intINDEX_SEC_ID int
DECLARE @intINT_RECEIVED int
DECLARE @intINTERESTED_PARTY1_CD int
DECLARE @intINTERESTED_PARTY2_CD int
DECLARE @intINTERESTED_PARTY3_CD int
DECLARE @intINTRADAY_GL_LONG_TERM int
DECLARE @intINTRADAY_GL_SHORT_TERM int
DECLARE @intINV_CLASS_CD int
DECLARE @intLAST_UPD_DATE int
DECLARE @intLAST_UPD_USER int
DECLARE @intLIABILITIES int
DECLARE @intLOT_SIZE int
DECLARE @intMAG_IND int
DECLARE @intMANAGER int
DECLARE @intMAX_ISSUER_HOLDING int
DECLARE @intMDL_ANALYTIC_TYPE int
DECLARE @intMDL_BENCH_ACCT_CD int
DECLARE @intMDL_CAT_WEIGHT_ONLY_IND int
DECLARE @intMDL_QTY_BASED_IND int
DECLARE @intMDL_TYP_IND int
DECLARE @intMKT_VAL int
DECLARE @intMKT_VAL_SOD int
DECLARE @intMODEL_PRIV int
DECLARE @intMULT_MDL_IND int
DECLARE @intMULT_MDL_NORM_IND int
DECLARE @intNET_ASSETS int
DECLARE @intNET_CASH int
DECLARE @intNET_FUNDS_AVAIL int
DECLARE @intNEXT_REBAL_DATE int
DECLARE @intNOTNL_FREEZE_IND int
DECLARE @intOASYS_ACCT_CD int
DECLARE @intOMNIBUS_ACCT_CD int
DECLARE @intOTH_ASSET int
DECLARE @intOTH_LIAB int
DECLARE @intPARENT_CHILD_FLAG int
DECLARE @intPAYBL_FUND_SHRS_LIQD int
DECLARE @intPAYBL_SEC_PURCH int
DECLARE @intPMA_ACCT_TOT_RET_APPROX int
DECLARE @intPMA_ATTR_METHOD_EQUITY int
DECLARE @intPMA_ATTR_METHOD_FI int
DECLARE @intPMA_COMMENT int
DECLARE @intPMA_DATA_FREQ int
DECLARE @intPMA_FOOTNOTE int
DECLARE @intPMA_INCPTN_DATE int
DECLARE @intPMA_MGMT_FEE_BASIS int
DECLARE @intPMA_TAX_BASIS int
DECLARE @intPMNTS int
DECLARE @intREBAL_APPROVAL_IND int
DECLARE @intREBAL_APPROVED_IND int
DECLARE @intREBAL_FREQ_CD int
DECLARE @intRECVB_FUND_SHRS_SOLD int
DECLARE @intRECVB_SEC_SOLD int
DECLARE @intREPRICE_IND int
DECLARE @intROTATION_CD int
DECLARE @intROTATION_SEQ int
DECLARE @intSERVICE_ID int
DECLARE @intSHAW_CLASS_CD int
DECLARE @intSHAW_MANAGER_CD int
DECLARE @intSHORT_SHORT_GAINS int
DECLARE @intSLEEVE_TYPE int
DECLARE @intSTATE_CD int
DECLARE @intSYST_OF_REFERENCE int
DECLARE @intTAX_LOT_PROC_IND int
DECLARE @intTAX_LOT_SELL_CNVTN int
DECLARE @intTEN_MIN_CHECK_IND int
DECLARE @intTHEME_1 int
DECLARE @intTHEME_2 int
DECLARE @intTHEME_3 int
DECLARE @intTOT_ASSETS int
DECLARE @intTOT_COST int
DECLARE @intTOT_INVSTMNTS int
DECLARE @intTRANSFERS int
DECLARE @intUDF_CHAR1 int
DECLARE @intUDF_CHAR10 int
DECLARE @intUDF_CHAR11 int
DECLARE @intUDF_CHAR12 int
DECLARE @intUDF_CHAR13 int
DECLARE @intUDF_CHAR14 int
DECLARE @intUDF_CHAR15 int
DECLARE @intUDF_CHAR16 int
DECLARE @intUDF_CHAR17 int
DECLARE @intUDF_CHAR18 int
DECLARE @intUDF_CHAR19 int
DECLARE @intUDF_CHAR2 int
DECLARE @intUDF_CHAR3 int
DECLARE @intUDF_CHAR4 int
DECLARE @intUDF_CHAR5 int
DECLARE @intUDF_CHAR6 int
DECLARE @intUDF_CHAR7 int
DECLARE @intUDF_CHAR8 int
DECLARE @intUDF_CHAR9 int
DECLARE @intUDF_DATE1 int
DECLARE @intUDF_DATE10 int
DECLARE @intUDF_DATE11 int
DECLARE @intUDF_DATE12 int
DECLARE @intUDF_DATE2 int
DECLARE @intUDF_DATE3 int
DECLARE @intUDF_DATE4 int
DECLARE @intUDF_DATE5 int
DECLARE @intUDF_DATE6 int
DECLARE @intUDF_DATE7 int
DECLARE @intUDF_DATE8 int
DECLARE @intUDF_DATE9 int
DECLARE @intUDF_FLOAT1 int
DECLARE @intUDF_FLOAT10 int
DECLARE @intUDF_FLOAT11 int
DECLARE @intUDF_FLOAT12 int
DECLARE @intUDF_FLOAT13 int
DECLARE @intUDF_FLOAT14 int
DECLARE @intUDF_FLOAT15 int
DECLARE @intUDF_FLOAT16 int
DECLARE @intUDF_FLOAT17 int
DECLARE @intUDF_FLOAT18 int
DECLARE @intUDF_FLOAT19 int
DECLARE @intUDF_FLOAT2 int
DECLARE @intUDF_FLOAT3 int
DECLARE @intUDF_FLOAT4 int
DECLARE @intUDF_FLOAT5 int
DECLARE @intUDF_FLOAT6 int
DECLARE @intUDF_FLOAT7 int
DECLARE @intUDF_FLOAT8 int
DECLARE @intUDF_FLOAT9 int
DECLARE @intUSE_SETTLE_DATE_BAL_IND int
DECLARE @intUSR_CLASS_CD_1 int
DECLARE @intUSR_CLASS_CD_2 int
DECLARE @intUSR_CLASS_CD_3 int
DECLARE @intUSR_CLASS_CD_4 int
DECLARE @intVIEW_ID int
DECLARE @intYTD_GL_LONG_TERM int
DECLARE @intYTD_GL_SHORT_TERM int


SELECT @dtmRunTime        = GETDATE()    -- Get process start time
SELECT @timeValue         = @dtmRunTime  -- reporting time from entry until here
SELECT @strIMP_CD         = 'IMP-FUND-'
SELECT @strIMP_TABLE      = 'CS_IMP_FUND'
SELECT @strMAIN_TABLE     = 'CS_FUND'
SELECT @informalTableName = 'FUND'
SELECT @managerJumpCode   = '005'
SELECT @crrncyCd 	  = 'USD'

-- Get the number of funds to be processed - for message log
SELECT @intInitImpCount = COUNT(*) FROM CS_IMP_FUND
SELECT  @intError = @@error 
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'import table record count'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'import table record count'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intInitImpCount, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END
SELECT @intFinalImpCount = @intInitImpCount


-- load column update variables
SELECT  @intACCRUED_EXP_INCM = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ACCRUED_EXP_INCM', column_name ) ), 0 ) ),
@intACCRUED_INT_INCM = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ACCRUED_INT_INCM', column_name ) ), 0 ) ),
@intACCT_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ACCT_CD', column_name ) ), 0 ) ),
@intACCT_ID = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ACCT_ID', column_name ) ), 0 ) ),
@intACCT_NAME = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ACCT_NAME', column_name ) ), 0 ) ),
@intACCT_SHT_NAME = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ACCT_SHT_NAME', column_name ) ), 0 ) ),
@intACCT_TYP_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ACCT_TYP_CD', column_name ) ), 0 ) ),
@intALLOW_AUTOFX = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ALLOW_AUTOFX', column_name ) ), 0 ) ),
@intALLOW_BUY = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ALLOW_BUY', column_name ) ), 0 ) ),
@intALLOW_NEG_CASH = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ALLOW_NEG_CASH', column_name ) ), 0 ) ),
@intALLOW_SELL = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ALLOW_SELL', column_name ) ), 0 ) ),
@intAMRTZD_COST = SIGN( ISNULL( MAX( no_update * PATINDEX( 'AMRTZD_COST', column_name ) ), 0 ) ),
@intAPPLY_GUIDELINES = SIGN( ISNULL( MAX( no_update * PATINDEX( 'APPLY_GUIDELINES', column_name ) ), 0 ) ),
@intAUTH_GROUP = SIGN( ISNULL( MAX( no_update * PATINDEX( 'AUTH_GROUP', column_name ) ), 0 ) ),
@intAUTH_NUM = SIGN( ISNULL( MAX( no_update * PATINDEX( 'AUTH_NUM', column_name ) ), 0 ) ),
@intAUTO_CFWD_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'AUTO_CFWD_IND', column_name ) ), 0 ) ),
@intAVG_COST = SIGN( ISNULL( MAX( no_update * PATINDEX( 'AVG_COST', column_name ) ), 0 ) ),
@intBANK_ACCT_NUM = SIGN( ISNULL( MAX( no_update * PATINDEX( 'BANK_ACCT_NUM', column_name ) ), 0 ) ),
@intBASKET_TYP_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'BASKET_TYP_CD', column_name ) ), 0 ) ),
@intBLOCK_ORDER_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'BLOCK_ORDER_IND', column_name ) ), 0 ) ),
@intBUNDLED_FEE_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'BUNDLED_FEE_IND', column_name ) ), 0 ) ),
@intCASH_BAL_SOD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CASH_BAL_SOD', column_name ) ), 0 ) ),
@intCASH_RSRV_AMT = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CASH_RSRV_AMT', column_name ) ), 0 ) ),
@intCASH_RSRV_PCT = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CASH_RSRV_PCT', column_name ) ), 0 ) ),
@intCASH_RSRV_TYP = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CASH_RSRV_TYP', column_name ) ), 0 ) ),
@intCATEGORY1 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CATEGORY1', column_name ) ), 0 ) ),
@intCATEGORY2 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CATEGORY2', column_name ) ), 0 ) ),
@intCATEGORY3 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CATEGORY3', column_name ) ), 0 ) ),
@intCFWD_CLOSE_CPARTY_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CFWD_CLOSE_CPARTY_IND', column_name ) ), 0 ) ),
@intCFWD_NEW_CPARTY_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CFWD_NEW_CPARTY_IND', column_name ) ), 0 ) ),
@intCG_TAX_RATE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CG_TAX_RATE', column_name ) ), 0 ) ),
@intCLOSE_DATE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CLOSE_DATE', column_name ) ), 0 ) ),
@intCNTRBS = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CNTRBS', column_name ) ), 0 ) ),
@intCNTRY_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CNTRY_CD', column_name ) ), 0 ) ),
@intCOMMENTS = SIGN( ISNULL( MAX( no_update * PATINDEX( 'COMMENTS', column_name ) ), 0 ) ),
@intCOMP_BID_REQ = SIGN( ISNULL( MAX( no_update * PATINDEX( 'COMP_BID_REQ', column_name ) ), 0 ) ),
@intCOMPLIANCE_TOLERANCE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'COMPLIANCE_TOLERANCE', column_name ) ), 0 ) ),
@intCPL_BATCH_RUNTIME = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CPL_BATCH_RUNTIME', column_name ) ), 0 ) ),
@intCREATE_DATE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CREATE_DATE', column_name ) ), 0 ) ),
@intCROSS_EXT_ACCT_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CROSS_EXT_ACCT_CD', column_name ) ), 0 ) ),
@intCROSS_GRP_ACCT_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CROSS_GRP_ACCT_CD', column_name ) ), 0 ) ),
@intCROSS_INT_ACCT_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CROSS_INT_ACCT_CD', column_name ) ), 0 ) ),
@intCROSS_MAG_ACCT_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CROSS_MAG_ACCT_CD', column_name ) ), 0 ) ),
@intCRRNCY_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CRRNCY_CD', column_name ) ), 0 ) ),
@intCUR_MKT_VAL = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CUR_MKT_VAL', column_name ) ), 0 ) ),
@intCUSTODIAN_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'CUSTODIAN_CD', column_name ) ), 0 ) ),
@intDEF_BENCH_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_BENCH_CD', column_name ) ), 0 ) ),
@intDEF_BENCH_ID = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_BENCH_ID', column_name ) ), 0 ) ),
@intDEF_BENCH_TYPE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_BENCH_TYPE', column_name ) ), 0 ) ),
@intDEF_FWD_DATE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_FWD_DATE', column_name ) ), 0 ) ),
@intDEF_FWD_DAYS = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_FWD_DAYS', column_name ) ), 0 ) ),
@intDEF_FWD_DAYS_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_FWD_DAYS_IND', column_name ) ), 0 ) ),
@intDEF_HEDGE_BENCH_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_HEDGE_BENCH_CD', column_name ) ), 0 ) ),
@intDEF_HEDGE_BENCH_ID = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_HEDGE_BENCH_ID', column_name ) ), 0 ) ),
@intDEF_HEDGE_BENCH_TYPE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_HEDGE_BENCH_TYPE', column_name ) ), 0 ) ),
@intDEF_HEDGE_MDL_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_HEDGE_MDL_CD', column_name ) ), 0 ) ),
@intDEF_HEDGE_MDL_ID = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_HEDGE_MDL_ID', column_name ) ), 0 ) ),
@intDEF_HEDGE_MDL_TYPE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_HEDGE_MDL_TYPE', column_name ) ), 0 ) ),
@intDEF_MDL_ID = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_MDL_ID', column_name ) ), 0 ) ),
@intDEF_MDL_REM_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_MDL_REM_IND', column_name ) ), 0 ) ),
@intDEF_MDL_TYPE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEF_MDL_TYPE', column_name ) ), 0 ) ),
@intDEFAULT_GRP_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEFAULT_GRP_CD', column_name ) ), 0 ) ),
@intDEFAULT_MODEL_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEFAULT_MODEL_CD', column_name ) ), 0 ) ),
@intDEFAULT_SPECIAL_INST = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEFAULT_SPECIAL_INST', column_name ) ), 0 ) ),
@intDEFAULT_TMP_ACCT = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DEFAULT_TMP_ACCT', column_name ) ), 0 ) ),
@intDIV_PAYBL = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DIV_PAYBL', column_name ) ), 0 ) ),
@intDIV_RCVBLE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DIV_RCVBLE', column_name ) ), 0 ) ),
@intDIV_RECEIVED = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DIV_RECEIVED', column_name ) ), 0 ) ),
@intDM_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'DM_IND', column_name ) ), 0 ) ),
@intEQTY_ACCT = SIGN( ISNULL( MAX( no_update * PATINDEX( 'EQTY_ACCT', column_name ) ), 0 ) ),
@intERISA_ELIGIBLE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ERISA_ELIGIBLE', column_name ) ), 0 ) ),
@intEX_ANTE_CALCS_METHOD_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'EX_ANTE_CALCS_METHOD_CD', column_name ) ), 0 ) ),
@intEXCH_LIST_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'EXCH_LIST_CD', column_name ) ), 0 ) ),
@intFACTOR_MODEL = SIGN( ISNULL( MAX( no_update * PATINDEX( 'FACTOR_MODEL', column_name ) ), 0 ) ),
@intFEE_PAYMT_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'FEE_PAYMT_IND', column_name ) ), 0 ) ),
@intFUND_ACCT = SIGN( ISNULL( MAX( no_update * PATINDEX( 'FUND_ACCT', column_name ) ), 0 ) ),
@intFUND_SHRS_OUTST = SIGN( ISNULL( MAX( no_update * PATINDEX( 'FUND_SHRS_OUTST', column_name ) ), 0 ) ),
@intFUT_BROKER_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'FUT_BROKER_IND', column_name ) ), 0 ) ),
@intGAIN_LOSS_SENSITIVE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'GAIN_LOSS_SENSITIVE', column_name ) ), 0 ) ),
@intGROSS_INCOME = SIGN( ISNULL( MAX( no_update * PATINDEX( 'GROSS_INCOME', column_name ) ), 0 ) ),
@intGRP_TYPE_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'GRP_TYPE_CD', column_name ) ), 0 ) ),
@intINACTIVE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'INACTIVE', column_name ) ), 0 ) ),
@intINC_TAX_RATE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'INC_TAX_RATE', column_name ) ), 0 ) ),
@intINCL_ACCRD_INTEREST = SIGN( ISNULL( MAX( no_update * PATINDEX( 'INCL_ACCRD_INTEREST', column_name ) ), 0 ) ),
@intINDEX_SEC_ID = SIGN( ISNULL( MAX( no_update * PATINDEX( 'INDEX_SEC_ID', column_name ) ), 0 ) ),
@intINT_RECEIVED = SIGN( ISNULL( MAX( no_update * PATINDEX( 'INT_RECEIVED', column_name ) ), 0 ) ),
@intINTERESTED_PARTY1_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'INTERESTED_PARTY1_CD', column_name ) ), 0 ) ),
@intINTERESTED_PARTY2_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'INTERESTED_PARTY2_CD', column_name ) ), 0 ) ),
@intINTERESTED_PARTY3_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'INTERESTED_PARTY3_CD', column_name ) ), 0 ) ),
@intINTRADAY_GL_LONG_TERM = SIGN( ISNULL( MAX( no_update * PATINDEX( 'INTRADAY_GL_LONG_TERM', column_name ) ), 0 ) ),
@intINTRADAY_GL_SHORT_TERM = SIGN( ISNULL( MAX( no_update * PATINDEX( 'INTRADAY_GL_SHORT_TERM', column_name ) ), 0 ) ),
@intINV_CLASS_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'INV_CLASS_CD', column_name ) ), 0 ) ),
@intLAST_UPD_DATE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'LAST_UPD_DATE', column_name ) ), 0 ) ),
@intLAST_UPD_USER = SIGN( ISNULL( MAX( no_update * PATINDEX( 'LAST_UPD_USER', column_name ) ), 0 ) ),
@intLIABILITIES = SIGN( ISNULL( MAX( no_update * PATINDEX( 'LIABILITIES', column_name ) ), 0 ) ),
@intMAG_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MAG_IND', column_name ) ), 0 ) ),
@intMANAGER = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MANAGER', column_name ) ), 0 ) ),
@intMAX_ISSUER_HOLDING = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MAX_ISSUER_HOLDING', column_name ) ), 0 ) ),
@intMDL_ANALYTIC_TYPE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MDL_ANALYTIC_TYPE', column_name ) ), 0 ) ),
@intMDL_BENCH_ACCT_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MDL_BENCH_ACCT_CD', column_name ) ), 0 ) ),
@intMDL_CAT_WEIGHT_ONLY_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MDL_CAT_WEIGHT_ONLY_IND', column_name ) ), 0 ) ),
@intMDL_QTY_BASED_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MDL_QTY_BASED_IND', column_name ) ), 0 ) ),
@intMDL_TYP_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MDL_TYP_IND', column_name ) ), 0 ) ),
@intMKT_VAL = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MKT_VAL', column_name ) ), 0 ) ),
@intMKT_VAL_SOD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MKT_VAL_SOD', column_name ) ), 0 ) ),
@intMODEL_PRIV = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MODEL_PRIV', column_name ) ), 0 ) ),
@intMULT_MDL_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MULT_MDL_IND', column_name ) ), 0 ) ),
@intMULT_MDL_NORM_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'MULT_MDL_NORM_IND', column_name ) ), 0 ) ),
@intNET_ASSETS = SIGN( ISNULL( MAX( no_update * PATINDEX( 'NET_ASSETS', column_name ) ), 0 ) ),
@intNET_CASH = SIGN( ISNULL( MAX( no_update * PATINDEX( 'NET_CASH', column_name ) ), 0 ) ),
@intNET_FUNDS_AVAIL = SIGN( ISNULL( MAX( no_update * PATINDEX( 'NET_FUNDS_AVAIL', column_name ) ), 0 ) ),
@intNEXT_REBAL_DATE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'NEXT_REBAL_DATE', column_name ) ), 0 ) ),
@intNOTNL_FREEZE_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'NOTNL_FREEZE_IND', column_name ) ), 0 ) ),
@intOASYS_ACCT_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'OASYS_ACCT_CD', column_name ) ), 0 ) ),
@intOMNIBUS_ACCT_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'OMNIBUS_ACCT_CD', column_name ) ), 0 ) ),
@intOTH_ASSET = SIGN( ISNULL( MAX( no_update * PATINDEX( 'OTH_ASSET', column_name ) ), 0 ) ),
@intOTH_LIAB = SIGN( ISNULL( MAX( no_update * PATINDEX( 'OTH_LIAB', column_name ) ), 0 ) ),
@intPAYBL_FUND_SHRS_LIQD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PAYBL_FUND_SHRS_LIQD', column_name ) ), 0 ) ),
@intPAYBL_SEC_PURCH = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PAYBL_SEC_PURCH', column_name ) ), 0 ) ),
@intPMA_ACCT_TOT_RET_APPROX = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PMA_ACCT_TOT_RET_APPROX', column_name ) ), 0 ) ),
@intPMA_ATTR_METHOD_EQUITY = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PMA_ATTR_METHOD_EQUITY', column_name ) ), 0 ) ),
@intPMA_ATTR_METHOD_FI = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PMA_ATTR_METHOD_FI', column_name ) ), 0 ) ),
@intPMA_COMMENT = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PMA_COMMENT', column_name ) ), 0 ) ),
@intPMA_DATA_FREQ = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PMA_DATA_FREQ', column_name ) ), 0 ) ),
@intPMA_FOOTNOTE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PMA_FOOTNOTE', column_name ) ), 0 ) ),
@intPMA_INCPTN_DATE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PMA_INCPTN_DATE', column_name ) ), 0 ) ),
@intPMA_MGMT_FEE_BASIS = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PMA_MGMT_FEE_BASIS', column_name ) ), 0 ) ),
@intPMA_TAX_BASIS = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PMA_TAX_BASIS', column_name ) ), 0 ) ),
@intPMNTS = SIGN( ISNULL( MAX( no_update * PATINDEX( 'PMNTS', column_name ) ), 0 ) ),
@intREBAL_APPROVAL_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'REBAL_APPROVAL_IND', column_name ) ), 0 ) ),
@intREBAL_APPROVED_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'REBAL_APPROVED_IND', column_name ) ), 0 ) ),
@intREBAL_FREQ_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'REBAL_FREQ_CD', column_name ) ), 0 ) ),
@intRECVB_FUND_SHRS_SOLD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'RECVB_FUND_SHRS_SOLD', column_name ) ), 0 ) ),
@intRECVB_SEC_SOLD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'RECVB_SEC_SOLD', column_name ) ), 0 ) ),
@intREPRICE_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'REPRICE_IND', column_name ) ), 0 ) ),
@intROTATION_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ROTATION_CD', column_name ) ), 0 ) ),
@intROTATION_SEQ = SIGN( ISNULL( MAX( no_update * PATINDEX( 'ROTATION_SEQ', column_name ) ), 0 ) ),
@intSERVICE_ID = SIGN( ISNULL( MAX( no_update * PATINDEX( 'SERVICE_ID', column_name ) ), 0 ) ),
@intSHAW_CLASS_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'SHAW_CLASS_CD', column_name ) ), 0 ) ),
@intSHAW_MANAGER_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'SHAW_MANAGER_CD', column_name ) ), 0 ) ),
@intSHORT_SHORT_GAINS = SIGN( ISNULL( MAX( no_update * PATINDEX( 'SHORT_SHORT_GAINS', column_name ) ), 0 ) ),
@intSLEEVE_TYPE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'SLEEVE_TYPE', column_name ) ), 0 ) ),
@intSTATE_CD = SIGN( ISNULL( MAX( no_update * PATINDEX( 'STATE_CD', column_name ) ), 0 ) ),
@intSYST_OF_REFERENCE = SIGN( ISNULL( MAX( no_update * PATINDEX( 'SYST_OF_REFERENCE', column_name ) ), 0 ) ),
@intTAX_LOT_PROC_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'TAX_LOT_PROC_IND', column_name ) ), 0 ) ),
@intTAX_LOT_SELL_CNVTN = SIGN( ISNULL( MAX( no_update * PATINDEX( 'TAX_LOT_SELL_CNVTN', column_name ) ), 0 ) ),
@intTEN_MIN_CHECK_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'TEN_MIN_CHECK_IND', column_name ) ), 0 ) ),
@intTHEME_1 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'THEME_1', column_name ) ), 0 ) ),
@intTHEME_2 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'THEME_2', column_name ) ), 0 ) ),
@intTHEME_3 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'THEME_3', column_name ) ), 0 ) ),
@intTOT_ASSETS = SIGN( ISNULL( MAX( no_update * PATINDEX( 'TOT_ASSETS', column_name ) ), 0 ) ),
@intTOT_COST = SIGN( ISNULL( MAX( no_update * PATINDEX( 'TOT_COST', column_name ) ), 0 ) ),
@intTOT_INVSTMNTS = SIGN( ISNULL( MAX( no_update * PATINDEX( 'TOT_INVSTMNTS', column_name ) ), 0 ) ),
@intTRANSFERS = SIGN( ISNULL( MAX( no_update * PATINDEX( 'TRANSFERS', column_name ) ), 0 ) ),
@intUDF_CHAR1 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR1', column_name ) ), 0 ) ),
@intUDF_CHAR10 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR10', column_name ) ), 0 ) ),
@intUDF_CHAR11 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR11', column_name ) ), 0 ) ),
@intUDF_CHAR12 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR12', column_name ) ), 0 ) ),
@intUDF_CHAR13 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR13', column_name ) ), 0 ) ),
@intUDF_CHAR14 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR14', column_name ) ), 0 ) ),
@intUDF_CHAR15 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR15', column_name ) ), 0 ) ),
@intUDF_CHAR16 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR16', column_name ) ), 0 ) ),
@intUDF_CHAR17 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR17', column_name ) ), 0 ) ),
@intUDF_CHAR18 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR18', column_name ) ), 0 ) ),
@intUDF_CHAR19 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR19', column_name ) ), 0 ) ),
@intUDF_CHAR2 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR2', column_name ) ), 0 ) ),
@intUDF_CHAR3 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR3', column_name ) ), 0 ) ),
@intUDF_CHAR4 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR4', column_name ) ), 0 ) ),
@intUDF_CHAR5 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR5', column_name ) ), 0 ) ),
@intUDF_CHAR6 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR6', column_name ) ), 0 ) ),
@intUDF_CHAR7 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR7', column_name ) ), 0 ) ),
@intUDF_CHAR8 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR8', column_name ) ), 0 ) ),
@intUDF_CHAR9 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_CHAR9', column_name ) ), 0 ) ),
@intUDF_DATE1 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE1', column_name ) ), 0 ) ),
@intUDF_DATE10 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE10', column_name ) ), 0 ) ),
@intUDF_DATE11 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE11', column_name ) ), 0 ) ),
@intUDF_DATE12 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE12', column_name ) ), 0 ) ),
@intUDF_DATE2 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE2', column_name ) ), 0 ) ),
@intUDF_DATE3 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE3', column_name ) ), 0 ) ),
@intUDF_DATE4 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE4', column_name ) ), 0 ) ),
@intUDF_DATE5 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE5', column_name ) ), 0 ) ),
@intUDF_DATE6 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE6', column_name ) ), 0 ) ),
@intUDF_DATE7 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE7', column_name ) ), 0 ) ),
@intUDF_DATE8 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE8', column_name ) ), 0 ) ),
@intUDF_DATE9 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_DATE9', column_name ) ), 0 ) ),
@intUDF_FLOAT1 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT1', column_name ) ), 0 ) ),
@intUDF_FLOAT10 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT10', column_name ) ), 0 ) ),
@intUDF_FLOAT11 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT11', column_name ) ), 0 ) ),
@intUDF_FLOAT12 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT12', column_name ) ), 0 ) ),
@intUDF_FLOAT13 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT13', column_name ) ), 0 ) ),
@intUDF_FLOAT14 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT14', column_name ) ), 0 ) ),
@intUDF_FLOAT15 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT15', column_name ) ), 0 ) ),
@intUDF_FLOAT16 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT16', column_name ) ), 0 ) ),
@intUDF_FLOAT17 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT17', column_name ) ), 0 ) ),
@intUDF_FLOAT18 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT18', column_name ) ), 0 ) ),
@intUDF_FLOAT19 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT19', column_name ) ), 0 ) ),
@intUDF_FLOAT2 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT2', column_name ) ), 0 ) ),
@intUDF_FLOAT3 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT3', column_name ) ), 0 ) ),
@intUDF_FLOAT4 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT4', column_name ) ), 0 ) ),
@intUDF_FLOAT5 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT5', column_name ) ), 0 ) ),
@intUDF_FLOAT6 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT6', column_name ) ), 0 ) ),
@intUDF_FLOAT7 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT7', column_name ) ), 0 ) ),
@intUDF_FLOAT8 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT8', column_name ) ), 0 ) ),
@intUDF_FLOAT9 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'UDF_FLOAT9', column_name ) ), 0 ) ),
@intUSE_SETTLE_DATE_BAL_IND = SIGN( ISNULL( MAX( no_update * PATINDEX( 'USE_SETTLE_DATE_BAL_IND', column_name ) ), 0 ) ),
@intUSR_CLASS_CD_1 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'USR_CLASS_CD_1', column_name ) ), 0 ) ),
@intUSR_CLASS_CD_2 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'USR_CLASS_CD_2', column_name ) ), 0 ) ),
@intUSR_CLASS_CD_3 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'USR_CLASS_CD_3', column_name ) ), 0 ) ),
@intUSR_CLASS_CD_4 = SIGN( ISNULL( MAX( no_update * PATINDEX( 'USR_CLASS_CD_4', column_name ) ), 0 ) ),
@intVIEW_ID = SIGN( ISNULL( MAX( no_update * PATINDEX( 'VIEW_ID', column_name ) ), 0 ) ),
@intYTD_GL_LONG_TERM = SIGN( ISNULL( MAX( no_update * PATINDEX( 'YTD_GL_LONG_TERM', column_name ) ), 0 ) ),
@intYTD_GL_SHORT_TERM = SIGN( ISNULL( MAX( no_update * PATINDEX( 'YTD_GL_SHORT_TERM', column_name ) ), 0 ) )
FROM    IMP_FEED_COLUMN
WHERE   feed_cd = @p_strExtSys
    and table_name = 'CS_FUND'

    SELECT  @intError = @@error 
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'determine which fields are enabled'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'determine which fields are enabled'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      NULL, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

-- Get default currency code
SELECT @crrncyCd = 'USD'
SELECT @crrncyCd = UPPER( param_val ) FROM csm_parameter WHERE param_name = 'PRICE_CURR'
SELECT  @intError = @@error 
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Retrieving PRICE_CURR parameter setting'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Retrieving PRICE_CURR parameter setting'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      NULL, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END


-- Constraint Validation: omnibus account children inherit the rotation_cd and rotation_seq
-- of the parent if they have none of their own.
IF @intROTATION_CD = 0 BEGIN

UPDATE  CS_IMP_FUND
SET     rotation_cd = (SELECT rotation_cd FROM CS_IMP_FUND
		       WHERE acct_cd = i.omnibus_acct_cd)
FROM    CS_IMP_FUND i
WHERE   omnibus_acct_cd IS NOT NULL
  AND   rotation_cd IS NULL
SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.rotation_cd for omnibus accounts from imports'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.rotation_cd for omnibus accounts from imports'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END


UPDATE  CS_IMP_FUND
SET     rotation_cd = (SELECT rotation_cd FROM CS_FUND
		       WHERE acct_cd = i.omnibus_acct_cd)
FROM    CS_IMP_FUND i
WHERE   omnibus_acct_cd IS NOT NULL
  AND   rotation_cd IS NULL
SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.rotation_cd for omnibus accounts from existing data'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.rotation_cd for omnibus accounts from existing data'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

END 

IF @intROTATION_SEQ = 0 BEGIN

UPDATE  CS_IMP_FUND
SET     rotation_seq = (SELECT rotation_seq FROM CS_IMP_FUND
			WHERE acct_cd = i.omnibus_acct_cd)
FROM    CS_IMP_FUND i
WHERE   omnibus_acct_cd IS NOT NULL
  AND   rotation_seq IS NULL
SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.rotation_seq for omnibus accounts from imports'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.rotation_seq for omnibus accounts from imports'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END


UPDATE  CS_IMP_FUND
SET     rotation_seq = (SELECT rotation_seq FROM CS_FUND
			WHERE acct_cd = i.omnibus_acct_cd)
FROM    CS_IMP_FUND i
WHERE   omnibus_acct_cd IS NOT NULL
  AND   rotation_seq IS NULL
SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.rotation_seq for omnibus accounts from existing data'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.rotation_seq for omnibus accounts from existing data'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END  

END


-- update close_date to current date if inactive = 'Y' and close_date is null
-- update close_date to null if inactive ='N' and close_date is not null

UPDATE  CS_IMP_FUND
SET  	close_date = GETDATE() 
FROM 	CS_IMP_FUND i
WHERE 	inactive = 'Y'
  AND 	close_date IS NULL
SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.close_date for the inactive accounts'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.close_date for the inactive accounts'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END  

UPDATE 	CS_IMP_FUND
SET 	close_date = NULL
FROM 	CS_IMP_FUND i
WHERE 	inactive = 'N' 
  AND	close_date IS NOT NULL
SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.close_date for the inactive accounts'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_IMP_FUND.close_date for the inactive accounts'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END  



--==============================================================================
--   S T A R T   O F   D A Y   C L E A N - U P   O P E R A T I O N S
--==============================================================================

    IF @p_startOfDay = 'Y'
    BEGIN

        -- Set compliance tolerance, intraday, and *_prev columns for all accounts in import 
        UPDATE  CS_FUND
        SET     compliance_tolerance = 0,
                udf_float1_prev = f.udf_float1,
                udf_float2_prev = f.udf_float2,
                udf_float3_prev = f.udf_float3,
                udf_float4_prev = f.udf_float4,
                udf_float5_prev = f.udf_float5,
                udf_float6_prev = f.udf_float6,
                udf_float7_prev = f.udf_float7,
                udf_float8_prev = f.udf_float8,
                udf_float9_prev = f.udf_float9,
                udf_float10_prev = f.udf_float10,
                udf_float11_prev = f.udf_float11,
                udf_float12_prev = f.udf_float12,
                udf_float13_prev = f.udf_float13,
                udf_float14_prev = f.udf_float14,
                udf_float15_prev = f.udf_float15,
                udf_float16_prev = f.udf_float16,
                udf_float17_prev = f.udf_float17,
                udf_float18_prev = f.udf_float18,
                udf_float19_prev = f.udf_float19,
                mkt_val_prev = f.mkt_val,
                net_assets_prev = f.net_assets,
                tot_assets_prev = f.tot_assets,
                tot_cost_prev = f.tot_cost,
                gross_income_prev = f.gross_income,
                intraday_gl_short_term = 0,
                intraday_gl_long_term = 0
        FROM    CS_FUND f, CS_IMP_FUND i
        WHERE   i.acct_cd = f.acct_cd
        SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update Tolerance & Previous Columns'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update Tolerance & Previous Columns'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

        -- Set compliance tolerance, *_prev columns for all parents of accounts in import 
        UPDATE  CS_FUND
        SET     compliance_tolerance = 0,
                udf_float1_prev = f.udf_float1,
                udf_float2_prev = f.udf_float2,
                udf_float3_prev = f.udf_float3,
                udf_float4_prev = f.udf_float4,
                udf_float5_prev = f.udf_float5,
                udf_float6_prev = f.udf_float6,
                udf_float7_prev = f.udf_float7,
                udf_float8_prev = f.udf_float8,
                udf_float9_prev = f.udf_float9,
                udf_float10_prev = f.udf_float10,
                udf_float11_prev = f.udf_float11,
                udf_float12_prev = f.udf_float12,
                udf_float13_prev = f.udf_float13,
                udf_float14_prev = f.udf_float14,
                udf_float15_prev = f.udf_float15,
                udf_float16_prev = f.udf_float16,
                udf_float17_prev = f.udf_float17,
                udf_float18_prev = f.udf_float18,
                udf_float19_prev = f.udf_float19,
                mkt_val_prev = f.mkt_val,
                net_assets_prev = f.net_assets,
                tot_assets_prev = f.tot_assets,
                tot_cost_prev = f.tot_cost,
                gross_income_prev = f.gross_income,
                intraday_gl_short_term = 0,
                intraday_gl_long_term = 0
        FROM    CS_FUND f, CS_IMP_FUND i, CS_FUND_CONFIG c
        WHERE   c.parent_acct_cd = f.acct_cd
            AND c.child_acct_cd = i.acct_cd
        SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update Tolerance & Previous Columns for parent accounts'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update Tolerance & Previous Columns for parent accounts'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

    END

---------------------------------------------------------------------------
--   D E T E R M I N E   A C C T _ I D s   O F   N E W   A C C O U N T S
---------------------------------------------------------------------------

SELECT @tmpTable = '#T134_CS_IMP_NEW_ACCTS'
SELECT @sql = 'INSERT INTO ' + @tmpTable + 
      ' SELECT acct_cd FROM CS_IMP_FUND i
      WHERE NOT EXISTS (SELECT 1 FROM CS_FUND p
      WHERE i.ACCT_CD = p.ACCT_CD)'

EXECUTE (@sql)
SELECT  @intError = @@error , @increment = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'INSERT INTO ' + @tmpTable
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'INSERT INTO ' + @tmpTable
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @increment, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

-- get block of sequence numbers OUTSIDE OF TRANSACTION because others may be using the table
EXEC @intError = crdImp_getSeqNums @increment, 'DEFAULT', @firstSeqNum = @firstSeqNum  OUTPUT,
                                               @strLastOp   = @strLastOp    OUTPUT

IF @intError != 0  GOTO Error_Handler
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'assign new acct_id values complete'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @increment, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

SELECT @firstSeqNum = @firstSeqNum - 1  -- temp table identity column starts with 1

--==============================================================================
--   U P D A T E   M A I N   T A B L E
--==============================================================================

BEGIN TRANSACTION

    UPDATE CS_FUND
    SET accrued_exp_incm = CASE WHEN @intACCRUED_EXP_INCM = 1 THEN p.accrued_exp_incm ELSE i.accrued_exp_incm END,
accrued_int_incm = CASE WHEN @intACCRUED_INT_INCM = 1 THEN p.accrued_int_incm ELSE i.accrued_int_incm END,
acct_name = CASE WHEN @intACCT_NAME = 1 THEN p.acct_name ELSE i.acct_name END,
acct_sht_name = CASE WHEN @intACCT_SHT_NAME = 1 THEN p.acct_sht_name ELSE i.acct_sht_name END,
acct_typ_cd = CASE WHEN @intACCT_TYP_CD = 1 THEN p.acct_typ_cd ELSE i.acct_typ_cd END,
active = CASE WHEN @intACTIVE = 1 THEN p.active ELSE i.active END,
allow_autofx = CASE WHEN @intALLOW_AUTOFX = 1 THEN p.allow_autofx ELSE i.allow_autofx END,
allow_buy = CASE WHEN @intALLOW_BUY = 1 THEN p.allow_buy ELSE ISNULL( i.allow_buy, 'Y' ) END,
allow_neg_cash = CASE WHEN @intALLOW_NEG_CASH = 1 THEN p.allow_neg_cash ELSE ISNULL( i.allow_neg_cash, 'Y' ) END,
allow_sell = CASE WHEN @intALLOW_SELL = 1 THEN p.allow_sell ELSE ISNULL( i.allow_sell, 'Y' ) END,
amrtzd_cost = CASE WHEN @intAMRTZD_COST = 1 THEN p.amrtzd_cost ELSE i.amrtzd_cost END,
apply_guidelines = CASE WHEN @intAPPLY_GUIDELINES = 1 THEN p.apply_guidelines ELSE i.apply_guidelines END,
auth_group = CASE WHEN @intAUTH_GROUP = 1 THEN p.auth_group ELSE i.auth_group END,
auth_num = CASE WHEN @intAUTH_NUM = 1 THEN p.auth_num ELSE ISNULL( i.auth_num, 0 ) END,
auto_cfwd_ind = CASE WHEN @intAUTO_CFWD_IND = 1 THEN p.auto_cfwd_ind ELSE ISNULL( i.auto_cfwd_ind, 'N' ) END,
avg_cost = CASE WHEN @intAVG_COST = 1 THEN p.avg_cost ELSE i.avg_cost END,
bank_acct_num = CASE WHEN @intBANK_ACCT_NUM = 1 THEN p.bank_acct_num ELSE i.bank_acct_num END,
basket_typ_cd = CASE WHEN @intBASKET_TYP_CD = 1 THEN p.basket_typ_cd ELSE i.basket_typ_cd END,
block_order_ind = CASE WHEN @intBLOCK_ORDER_IND = 1 THEN p.block_order_ind ELSE ISNULL( i.block_order_ind, 'Y' ) END,
bundled_fee_ind = CASE WHEN @intBUNDLED_FEE_IND = 1 THEN p.bundled_fee_ind ELSE ISNULL( i.bundled_fee_ind, 'N' ) END,
cash_bal_sod = CASE WHEN @intCASH_BAL_SOD = 1 THEN p.cash_bal_sod ELSE i.cash_bal_sod END,
cash_rsrv_amt = CASE WHEN @intCASH_RSRV_AMT = 1 THEN p.cash_rsrv_amt ELSE i.cash_rsrv_amt END,
cash_rsrv_pct = CASE WHEN @intCASH_RSRV_PCT = 1 THEN p.cash_rsrv_pct ELSE i.cash_rsrv_pct END,
cash_rsrv_typ = CASE WHEN @intCASH_RSRV_TYP = 1 THEN p.cash_rsrv_typ ELSE ISNULL( i.cash_rsrv_typ, 'N' ) END,
category1 = CASE WHEN @intCATEGORY1 = 1 THEN p.category1 ELSE i.category1 END,
category2 = CASE WHEN @intCATEGORY2 = 1 THEN p.category2 ELSE i.category2 END,
category3 = CASE WHEN @intCATEGORY3 = 1 THEN p.category3 ELSE i.category3 END,
cfwd_close_cparty_ind = CASE WHEN @intCFWD_CLOSE_CPARTY_IND = 1 THEN p.cfwd_close_cparty_ind ELSE ISNULL( i.cfwd_close_cparty_ind, 'N' ) END,
cfwd_new_cparty_ind = CASE WHEN @intCFWD_NEW_CPARTY_IND = 1 THEN p.cfwd_new_cparty_ind ELSE ISNULL( i.cfwd_new_cparty_ind, 'N' ) END,
cg_tax_rate = CASE WHEN @intCG_TAX_RATE = 1 THEN p.cg_tax_rate ELSE i.cg_tax_rate END,
close_date = CASE WHEN @intCLOSE_DATE = 1 THEN p.close_date ELSE i.close_date END,
cntrbs = CASE WHEN @intCNTRBS = 1 THEN p.cntrbs ELSE i.cntrbs END,
cntry_cd = CASE WHEN @intCNTRY_CD = 1 THEN p.cntry_cd ELSE i.cntry_cd END,
comments = CASE WHEN @intCOMMENTS = 1 THEN p.comments ELSE i.comments END,
comp_bid_req = CASE WHEN @intCOMP_BID_REQ = 1 THEN p.comp_bid_req ELSE ISNULL( i.comp_bid_req, 'N' ) END,
cross_ext_acct_cd = CASE WHEN @intCROSS_EXT_ACCT_CD = 1 THEN p.cross_ext_acct_cd ELSE i.cross_ext_acct_cd END,
cross_grp_acct_cd = CASE WHEN @intCROSS_GRP_ACCT_CD = 1 THEN p.cross_grp_acct_cd ELSE i.cross_grp_acct_cd END,
cross_int_acct_cd = CASE WHEN @intCROSS_INT_ACCT_CD = 1 THEN p.cross_int_acct_cd ELSE i.cross_int_acct_cd END,
cross_mag_acct_cd = CASE WHEN @intCROSS_MAG_ACCT_CD = 1 THEN p.cross_mag_acct_cd ELSE i.cross_mag_acct_cd END,
cur_invested = CASE WHEN @intCUR_INVESTED = 1 THEN p.cur_invested ELSE i.cur_invested END,
custodian_cd = CASE WHEN @intCUSTODIAN_CD = 1 THEN p.custodian_cd ELSE i.custodian_cd END,
def_bench_cd = CASE WHEN @intDEF_BENCH_CD = 1 THEN p.def_bench_cd ELSE i.def_bench_cd END,
def_bench_id = CASE WHEN @intDEF_BENCH_ID = 1 THEN p.def_bench_id ELSE i.def_bench_id END,
def_bench_type = CASE WHEN @intDEF_BENCH_TYPE = 1 THEN p.def_bench_type ELSE i.def_bench_type END,
def_fwd_date = CASE WHEN @intDEF_FWD_DATE = 1 THEN p.def_fwd_date ELSE i.def_fwd_date END,
def_fwd_days = CASE WHEN @intDEF_FWD_DAYS = 1 THEN p.def_fwd_days ELSE i.def_fwd_days END,
def_fwd_days_ind = CASE WHEN @intDEF_FWD_DAYS_IND = 1 THEN p.def_fwd_days_ind ELSE i.def_fwd_days_ind END,
def_hedge_bench_cd = CASE WHEN @intDEF_HEDGE_BENCH_CD = 1 THEN p.def_hedge_bench_cd ELSE i.def_hedge_bench_cd END,
def_hedge_bench_id = CASE WHEN @intDEF_HEDGE_BENCH_ID = 1 THEN p.def_hedge_bench_id ELSE i.def_hedge_bench_id END,
def_hedge_bench_type = CASE WHEN @intDEF_HEDGE_BENCH_TYPE = 1 THEN p.def_hedge_bench_type ELSE i.def_hedge_bench_type END,
def_hedge_mdl_cd = CASE WHEN @intDEF_HEDGE_MDL_CD = 1 THEN p.def_hedge_mdl_cd ELSE i.def_hedge_mdl_cd END,
def_hedge_mdl_id = CASE WHEN @intDEF_HEDGE_MDL_ID = 1 THEN p.def_hedge_mdl_id ELSE i.def_hedge_mdl_id END,
def_hedge_mdl_type = CASE WHEN @intDEF_HEDGE_MDL_TYPE = 1 THEN p.def_hedge_mdl_type ELSE i.def_hedge_mdl_type END,
def_mdl_id = CASE WHEN @intDEF_MDL_ID = 1 THEN p.def_mdl_id ELSE i.def_mdl_id END,
def_mdl_rem_ind = CASE WHEN @intDEF_MDL_REM_IND = 1 THEN p.def_mdl_rem_ind ELSE ISNULL( i.def_mdl_rem_ind, 'P' ) END,
def_mdl_type = CASE WHEN @intDEF_MDL_TYPE = 1 THEN p.def_mdl_type ELSE i.def_mdl_type END,
default_grp_cd = CASE WHEN @intDEFAULT_GRP_CD = 1 THEN p.default_grp_cd ELSE i.default_grp_cd END,
default_model_cd = CASE WHEN @intDEFAULT_MODEL_CD = 1 THEN p.default_model_cd ELSE i.default_model_cd END,
default_special_inst = CASE WHEN @intDEFAULT_SPECIAL_INST = 1 THEN p.default_special_inst ELSE i.default_special_inst END,
default_tmp_acct = CASE WHEN @intDEFAULT_TMP_ACCT = 1 THEN p.default_tmp_acct ELSE i.default_tmp_acct END,
disc_accrual_cnvtn = CASE WHEN @intDISC_ACCRUAL_CNVTN = 1 THEN p.disc_accrual_cnvtn ELSE i.disc_accrual_cnvtn END,
div_paybl = CASE WHEN @intDIV_PAYBL = 1 THEN p.div_paybl ELSE i.div_paybl END,
div_rcvble = CASE WHEN @intDIV_RCVBLE = 1 THEN p.div_rcvble ELSE i.div_rcvble END,
div_received = CASE WHEN @intDIV_RECEIVED = 1 THEN p.div_received ELSE i.div_received END,
dm_ind = CASE WHEN @intDM_IND = 1 THEN p.dm_ind ELSE ISNULL( i.dm_ind, 'N' ) END,
eqty_acct = CASE WHEN @intEQTY_ACCT = 1 THEN p.eqty_acct ELSE i.eqty_acct END,
erisa_eligible = CASE WHEN @intERISA_ELIGIBLE = 1 THEN p.erisa_eligible ELSE ISNULL( i.erisa_eligible, 'N' ) END,
ex_ante_calcs_method_cd = CASE WHEN @intEX_ANTE_CALCS_METHOD_CD = 1 THEN p.ex_ante_calcs_method_cd ELSE i.ex_ante_calcs_method_cd END,
exch_list_cd = CASE WHEN @intEXCH_LIST_CD = 1 THEN p.exch_list_cd ELSE i.exch_list_cd END,
factor_model = CASE WHEN @intFACTOR_MODEL = 1 THEN p.factor_model ELSE i.factor_model END,
fee_paymt_ind = CASE WHEN @intFEE_PAYMT_IND = 1 THEN p.fee_paymt_ind ELSE ISNULL( i.fee_paymt_ind, 'Y' ) END,
fund_acct = CASE WHEN @intFUND_ACCT = 1 THEN p.fund_acct ELSE i.fund_acct END,
fund_shrs_outst = CASE WHEN @intFUND_SHRS_OUTST = 1 THEN p.fund_shrs_outst ELSE i.fund_shrs_outst END,
fut_broker_ind = CASE WHEN @intFUT_BROKER_IND = 1 THEN p.fut_broker_ind ELSE ISNULL( i.fut_broker_ind, 'N' ) END,
gain_loss_sensitive = CASE WHEN @intGAIN_LOSS_SENSITIVE = 1 THEN p.gain_loss_sensitive ELSE ISNULL( i.gain_loss_sensitive, 'N' ) END,
gross_income = CASE WHEN @intGROSS_INCOME = 1 THEN p.gross_income ELSE i.gross_income END,
grp_type_cd = CASE WHEN @intGRP_TYPE_CD = 1 THEN p.grp_type_cd ELSE i.grp_type_cd END,
inactive = CASE WHEN @intINACTIVE = 1 THEN p.inactive ELSE ISNULL( i.inactive, 'N' ) END,
inc_tax_rate = CASE WHEN @intINC_TAX_RATE = 1 THEN p.inc_tax_rate ELSE i.inc_tax_rate END,
incl_accrd_interest = CASE WHEN @intINCL_ACCRD_INTEREST = 1 THEN p.incl_accrd_interest ELSE i.incl_accrd_interest END,
index_sec_id = CASE WHEN @intINDEX_SEC_ID = 1 THEN p.index_sec_id ELSE i.index_sec_id END,
int_received = CASE WHEN @intINT_RECEIVED = 1 THEN p.int_received ELSE i.int_received END,
interested_party1_cd = CASE WHEN @intINTERESTED_PARTY1_CD = 1 THEN p.interested_party1_cd ELSE i.interested_party1_cd END,
interested_party2_cd = CASE WHEN @intINTERESTED_PARTY2_CD = 1 THEN p.interested_party2_cd ELSE i.interested_party2_cd END,
interested_party3_cd = CASE WHEN @intINTERESTED_PARTY3_CD = 1 THEN p.interested_party3_cd ELSE i.interested_party3_cd END,
inv_class_cd = CASE WHEN @intINV_CLASS_CD = 1 THEN p.inv_class_cd ELSE i.inv_class_cd END,
liabilities = CASE WHEN @intLIABILITIES = 1 THEN p.liabilities ELSE i.liabilities END,
lot_size = CASE WHEN @intLOT_SIZE = 1 THEN p.lot_size ELSE i.lot_size END,
mag_ind = CASE WHEN @intMAG_IND = 1 THEN p.mag_ind ELSE ISNULL( i.mag_ind, 'N' ) END,
max_issuer_holding = CASE WHEN @intMAX_ISSUER_HOLDING = 1 THEN p.max_issuer_holding ELSE i.max_issuer_holding END,
mdl_analytic_type = CASE WHEN @intMDL_ANALYTIC_TYPE = 1 THEN p.mdl_analytic_type ELSE ISNULL( i.mdl_analytic_type, 'N' ) END,
mdl_bench_acct_cd = CASE WHEN @intMDL_BENCH_ACCT_CD = 1 THEN p.mdl_bench_acct_cd ELSE i.mdl_bench_acct_cd END,
mdl_cat_weight_only_ind = CASE WHEN @intMDL_CAT_WEIGHT_ONLY_IND = 1 THEN p.mdl_cat_weight_only_ind ELSE ISNULL( i.mdl_cat_weight_only_ind, 'N' ) END,
mdl_qty_based_ind = CASE WHEN @intMDL_QTY_BASED_IND = 1 THEN p.mdl_qty_based_ind ELSE ISNULL( i.mdl_qty_based_ind, 'N' ) END,
mdl_typ_ind = CASE WHEN @intMDL_TYP_IND = 1 THEN p.mdl_typ_ind ELSE i.mdl_typ_ind END,
mkt_val = CASE WHEN @intMKT_VAL = 1 THEN p.mkt_val ELSE i.mkt_val END,
mkt_val_sod = CASE WHEN @intMKT_VAL_SOD = 1 THEN p.mkt_val_sod ELSE i.mkt_val_sod END,
model_priv = CASE WHEN @intMODEL_PRIV = 1 THEN p.model_priv ELSE i.model_priv END,
mult_mdl_ind = CASE WHEN @intMULT_MDL_IND = 1 THEN p.mult_mdl_ind ELSE ISNULL( i.mult_mdl_ind, 'N' ) END,
mult_mdl_norm_ind = CASE WHEN @intMULT_MDL_NORM_IND = 1 THEN p.mult_mdl_norm_ind ELSE ISNULL( i.mult_mdl_norm_ind, 'N' ) END,
net_assets = CASE WHEN @intNET_ASSETS = 1 THEN p.net_assets ELSE i.net_assets END,
net_cash = CASE WHEN @intNET_CASH = 1 THEN p.net_cash ELSE i.net_cash END,
net_funds_avail = CASE WHEN @intNET_FUNDS_AVAIL = 1 THEN p.net_funds_avail ELSE i.net_funds_avail END,
next_rebal_date = CASE WHEN @intNEXT_REBAL_DATE = 1 THEN p.next_rebal_date ELSE i.next_rebal_date END,
notnl_freeze_ind = CASE WHEN @intNOTNL_FREEZE_IND = 1 THEN p.notnl_freeze_ind ELSE ISNULL( i.notnl_freeze_ind, 'N' ) END,
oasys_acct_cd = CASE WHEN @intOASYS_ACCT_CD = 1 THEN p.oasys_acct_cd ELSE i.oasys_acct_cd END,
omnibus_acct_cd = CASE WHEN @intOMNIBUS_ACCT_CD = 1 THEN p.omnibus_acct_cd ELSE i.omnibus_acct_cd END,
oth_asset = CASE WHEN @intOTH_ASSET = 1 THEN p.oth_asset ELSE i.oth_asset END,
oth_liab = CASE WHEN @intOTH_LIAB = 1 THEN p.oth_liab ELSE i.oth_liab END,
parent_child_flag = CASE WHEN @intPARENT_CHILD_FLAG = 1 THEN p.parent_child_flag ELSE i.parent_child_flag END,
paybl_fund_shrs_liqd = CASE WHEN @intPAYBL_FUND_SHRS_LIQD = 1 THEN p.paybl_fund_shrs_liqd ELSE i.paybl_fund_shrs_liqd END,
paybl_sec_purch = CASE WHEN @intPAYBL_SEC_PURCH = 1 THEN p.paybl_sec_purch ELSE i.paybl_sec_purch END,
pma_acct_tot_ret_approx = CASE WHEN @intPMA_ACCT_TOT_RET_APPROX = 1 THEN p.pma_acct_tot_ret_approx ELSE i.pma_acct_tot_ret_approx END,
pma_attr_method_equity = CASE WHEN @intPMA_ATTR_METHOD_EQUITY = 1 THEN p.pma_attr_method_equity ELSE i.pma_attr_method_equity END,
pma_attr_method_fi = CASE WHEN @intPMA_ATTR_METHOD_FI = 1 THEN p.pma_attr_method_fi ELSE i.pma_attr_method_fi END,
pma_data_freq = CASE WHEN @intPMA_DATA_FREQ = 1 THEN p.pma_data_freq ELSE ISNULL( i.pma_data_freq, 'D' ) END,
pma_incptn_date = CASE WHEN @intPMA_INCPTN_DATE = 1 THEN p.pma_incptn_date ELSE i.pma_incptn_date END,
pma_mgmt_fee_basis = CASE WHEN @intPMA_MGMT_FEE_BASIS = 1 THEN p.pma_mgmt_fee_basis ELSE ISNULL( i.pma_mgmt_fee_basis, 'G' ) END,
pma_tax_basis = CASE WHEN @intPMA_TAX_BASIS = 1 THEN p.pma_tax_basis ELSE ISNULL( i.pma_tax_basis, 'G' ) END,
pmnts = CASE WHEN @intPMNTS = 1 THEN p.pmnts ELSE i.pmnts END,
rebal_approval_ind = CASE WHEN @intREBAL_APPROVAL_IND = 1 THEN p.rebal_approval_ind ELSE ISNULL( i.rebal_approval_ind, 'N' ) END,
rebal_approved_ind = CASE WHEN @intREBAL_APPROVED_IND = 1 THEN p.rebal_approved_ind ELSE ISNULL( i.rebal_approved_ind, 'N' ) END,
rebal_freq_cd = CASE WHEN @intREBAL_FREQ_CD = 1 THEN p.rebal_freq_cd ELSE i.rebal_freq_cd END,
recvb_fund_shrs_sold = CASE WHEN @intRECVB_FUND_SHRS_SOLD = 1 THEN p.recvb_fund_shrs_sold ELSE i.recvb_fund_shrs_sold END,
recvb_sec_sold = CASE WHEN @intRECVB_SEC_SOLD = 1 THEN p.recvb_sec_sold ELSE i.recvb_sec_sold END,
reprice_ind = CASE WHEN @intREPRICE_IND = 1 THEN p.reprice_ind ELSE ISNULL( i.reprice_ind, 'N' ) END,
rotation_cd = CASE WHEN @intROTATION_CD = 1 THEN p.rotation_cd ELSE i.rotation_cd END,
rotation_seq = CASE WHEN @intROTATION_SEQ = 1 THEN p.rotation_seq ELSE i.rotation_seq END,
service_id = CASE WHEN @intSERVICE_ID = 1 THEN p.service_id ELSE i.service_id END,
shaw_class_cd = CASE WHEN @intSHAW_CLASS_CD = 1 THEN p.shaw_class_cd ELSE i.shaw_class_cd END,
shaw_manager_cd = CASE WHEN @intSHAW_MANAGER_CD = 1 THEN p.shaw_manager_cd ELSE i.shaw_manager_cd END,
short_short_gains = CASE WHEN @intSHORT_SHORT_GAINS = 1 THEN p.short_short_gains ELSE i.short_short_gains END,
sleeve_type = CASE WHEN @intSLEEVE_TYPE = 1 THEN p.sleeve_type ELSE i.sleeve_type END,
state_cd = CASE WHEN @intSTATE_CD = 1 THEN p.state_cd ELSE i.state_cd END,
tax_lot_proc_ind = CASE WHEN @intTAX_LOT_PROC_IND = 1 THEN p.tax_lot_proc_ind ELSE ISNULL( i.tax_lot_proc_ind, 'N' ) END,
tax_lot_sell_cnvtn = CASE WHEN @intTAX_LOT_SELL_CNVTN = 1 THEN p.tax_lot_sell_cnvtn ELSE i.tax_lot_sell_cnvtn END,
ten_min_check_ind = CASE WHEN @intTEN_MIN_CHECK_IND = 1 THEN p.ten_min_check_ind ELSE ISNULL( i.ten_min_check_ind, 'P' ) END,
theme_1 = CASE WHEN @intTHEME_1 = 1 THEN p.theme_1 ELSE i.theme_1 END,
theme_2 = CASE WHEN @intTHEME_2 = 1 THEN p.theme_2 ELSE i.theme_2 END,
theme_3 = CASE WHEN @intTHEME_3 = 1 THEN p.theme_3 ELSE i.theme_3 END,
tot_assets = CASE WHEN @intTOT_ASSETS = 1 THEN p.tot_assets ELSE i.tot_assets END,
tot_cost = CASE WHEN @intTOT_COST = 1 THEN p.tot_cost ELSE i.tot_cost END,
tot_invstmnts = CASE WHEN @intTOT_INVSTMNTS = 1 THEN p.tot_invstmnts ELSE i.tot_invstmnts END,
transfers = CASE WHEN @intTRANSFERS = 1 THEN p.transfers ELSE i.transfers END,
udf_char1 = CASE WHEN @intUDF_CHAR1 = 1 THEN p.udf_char1 ELSE i.udf_char1 END,
udf_char10 = CASE WHEN @intUDF_CHAR10 = 1 THEN p.udf_char10 ELSE i.udf_char10 END,
udf_char11 = CASE WHEN @intUDF_CHAR11 = 1 THEN p.udf_char11 ELSE i.udf_char11 END,
udf_char12 = CASE WHEN @intUDF_CHAR12 = 1 THEN p.udf_char12 ELSE i.udf_char12 END,
udf_char13 = CASE WHEN @intUDF_CHAR13 = 1 THEN p.udf_char13 ELSE i.udf_char13 END,
udf_char14 = CASE WHEN @intUDF_CHAR14 = 1 THEN p.udf_char14 ELSE i.udf_char14 END,
udf_char15 = CASE WHEN @intUDF_CHAR15 = 1 THEN p.udf_char15 ELSE i.udf_char15 END,
udf_char16 = CASE WHEN @intUDF_CHAR16 = 1 THEN p.udf_char16 ELSE i.udf_char16 END,
udf_char17 = CASE WHEN @intUDF_CHAR17 = 1 THEN p.udf_char17 ELSE i.udf_char17 END,
udf_char18 = CASE WHEN @intUDF_CHAR18 = 1 THEN p.udf_char18 ELSE i.udf_char18 END,
udf_char19 = CASE WHEN @intUDF_CHAR19 = 1 THEN p.udf_char19 ELSE i.udf_char19 END,
udf_char2 = CASE WHEN @intUDF_CHAR2 = 1 THEN p.udf_char2 ELSE i.udf_char2 END,
udf_char3 = CASE WHEN @intUDF_CHAR3 = 1 THEN p.udf_char3 ELSE i.udf_char3 END,
udf_char4 = CASE WHEN @intUDF_CHAR4 = 1 THEN p.udf_char4 ELSE i.udf_char4 END,
udf_char5 = CASE WHEN @intUDF_CHAR5 = 1 THEN p.udf_char5 ELSE i.udf_char5 END,
udf_char6 = CASE WHEN @intUDF_CHAR6 = 1 THEN p.udf_char6 ELSE i.udf_char6 END,
udf_char7 = CASE WHEN @intUDF_CHAR7 = 1 THEN p.udf_char7 ELSE i.udf_char7 END,
udf_char8 = CASE WHEN @intUDF_CHAR8 = 1 THEN p.udf_char8 ELSE i.udf_char8 END,
udf_char9 = CASE WHEN @intUDF_CHAR9 = 1 THEN p.udf_char9 ELSE i.udf_char9 END,
udf_date1 = CASE WHEN @intUDF_DATE1 = 1 THEN p.udf_date1 ELSE i.udf_date1 END,
udf_date10 = CASE WHEN @intUDF_DATE10 = 1 THEN p.udf_date10 ELSE i.udf_date10 END,
udf_date11 = CASE WHEN @intUDF_DATE11 = 1 THEN p.udf_date11 ELSE i.udf_date11 END,
udf_date12 = CASE WHEN @intUDF_DATE12 = 1 THEN p.udf_date12 ELSE i.udf_date12 END,
udf_date2 = CASE WHEN @intUDF_DATE2 = 1 THEN p.udf_date2 ELSE i.udf_date2 END,
udf_date3 = CASE WHEN @intUDF_DATE3 = 1 THEN p.udf_date3 ELSE i.udf_date3 END,
udf_date4 = CASE WHEN @intUDF_DATE4 = 1 THEN p.udf_date4 ELSE i.udf_date4 END,
udf_date5 = CASE WHEN @intUDF_DATE5 = 1 THEN p.udf_date5 ELSE i.udf_date5 END,
udf_date6 = CASE WHEN @intUDF_DATE6 = 1 THEN p.udf_date6 ELSE i.udf_date6 END,
udf_date7 = CASE WHEN @intUDF_DATE7 = 1 THEN p.udf_date7 ELSE i.udf_date7 END,
udf_date8 = CASE WHEN @intUDF_DATE8 = 1 THEN p.udf_date8 ELSE i.udf_date8 END,
udf_date9 = CASE WHEN @intUDF_DATE9 = 1 THEN p.udf_date9 ELSE i.udf_date9 END,
udf_float1 = CASE WHEN @intUDF_FLOAT1 = 1 THEN p.udf_float1 ELSE i.udf_float1 END,
udf_float10 = CASE WHEN @intUDF_FLOAT10 = 1 THEN p.udf_float10 ELSE i.udf_float10 END,
udf_float11 = CASE WHEN @intUDF_FLOAT11 = 1 THEN p.udf_float11 ELSE i.udf_float11 END,
udf_float12 = CASE WHEN @intUDF_FLOAT12 = 1 THEN p.udf_float12 ELSE i.udf_float12 END,
udf_float13 = CASE WHEN @intUDF_FLOAT13 = 1 THEN p.udf_float13 ELSE i.udf_float13 END,
udf_float14 = CASE WHEN @intUDF_FLOAT14 = 1 THEN p.udf_float14 ELSE i.udf_float14 END,
udf_float15 = CASE WHEN @intUDF_FLOAT15 = 1 THEN p.udf_float15 ELSE i.udf_float15 END,
udf_float16 = CASE WHEN @intUDF_FLOAT16 = 1 THEN p.udf_float16 ELSE i.udf_float16 END,
udf_float17 = CASE WHEN @intUDF_FLOAT17 = 1 THEN p.udf_float17 ELSE i.udf_float17 END,
udf_float18 = CASE WHEN @intUDF_FLOAT18 = 1 THEN p.udf_float18 ELSE i.udf_float18 END,
udf_float19 = CASE WHEN @intUDF_FLOAT19 = 1 THEN p.udf_float19 ELSE i.udf_float19 END,
udf_float2 = CASE WHEN @intUDF_FLOAT2 = 1 THEN p.udf_float2 ELSE i.udf_float2 END,
udf_float3 = CASE WHEN @intUDF_FLOAT3 = 1 THEN p.udf_float3 ELSE i.udf_float3 END,
udf_float4 = CASE WHEN @intUDF_FLOAT4 = 1 THEN p.udf_float4 ELSE i.udf_float4 END,
udf_float5 = CASE WHEN @intUDF_FLOAT5 = 1 THEN p.udf_float5 ELSE i.udf_float5 END,
udf_float6 = CASE WHEN @intUDF_FLOAT6 = 1 THEN p.udf_float6 ELSE i.udf_float6 END,
udf_float7 = CASE WHEN @intUDF_FLOAT7 = 1 THEN p.udf_float7 ELSE i.udf_float7 END,
udf_float8 = CASE WHEN @intUDF_FLOAT8 = 1 THEN p.udf_float8 ELSE i.udf_float8 END,
udf_float9 = CASE WHEN @intUDF_FLOAT9 = 1 THEN p.udf_float9 ELSE i.udf_float9 END,
use_settle_date_bal_ind = CASE WHEN @intUSE_SETTLE_DATE_BAL_IND = 1 THEN p.use_settle_date_bal_ind ELSE ISNULL( i.use_settle_date_bal_ind, 'N' ) END,
usr_class_cd_1 = CASE WHEN @intUSR_CLASS_CD_1 = 1 THEN p.usr_class_cd_1 ELSE i.usr_class_cd_1 END,
usr_class_cd_2 = CASE WHEN @intUSR_CLASS_CD_2 = 1 THEN p.usr_class_cd_2 ELSE i.usr_class_cd_2 END,
usr_class_cd_3 = CASE WHEN @intUSR_CLASS_CD_3 = 1 THEN p.usr_class_cd_3 ELSE i.usr_class_cd_3 END,
usr_class_cd_4 = CASE WHEN @intUSR_CLASS_CD_4 = 1 THEN p.usr_class_cd_4 ELSE i.usr_class_cd_4 END,
view_id = CASE WHEN @intVIEW_ID = 1 THEN p.view_id ELSE i.view_id END,
ytd_gl_long_term = CASE WHEN @intYTD_GL_LONG_TERM = 1 THEN p.ytd_gl_long_term ELSE i.ytd_gl_long_term END,
ytd_gl_short_term = CASE WHEN @intYTD_GL_SHORT_TERM = 1 THEN p.ytd_gl_short_term ELSE i.ytd_gl_short_term END,	
    		CRRNCY_CD = CASE WHEN @intCRRNCY_CD = 1 THEN p.CRRNCY_CD ELSE ISNULL( i.CRRNCY_CD, @crrncyCd ) END,
        cur_mkt_val = CASE WHEN @intMKT_VAL = 1 THEN p.mkt_val ELSE i.mkt_val END,
        manager = CASE WHEN @intMANAGER = 1 THEN UPPER (ISNULL (p.manager, @p_strDefaultManager))
                                            ELSE UPPER (ISNULL (i.manager, @p_strDefaultManager)) END,
        syst_of_reference = @p_strExtSys,
        record_version = p.record_version + 1,
		last_upd_user = CASE WHEN @intLAST_UPD_USER = 0
								THEN @p_importUserId ELSE p.last_upd_user END,
		last_upd_date = CASE WHEN @intLAST_UPD_DATE = 0
								THEN GETDATE() ELSE ISNULL( p.last_upd_date, GETDATE() ) END
    FROM CS_IMP_FUND i,
         CS_FUND p
    WHERE i.acct_cd = p.acct_cd
			-- Only update rows with changes
			AND (
				( @intACCRUED_EXP_INCM = 0 AND NOT ((p.accrued_exp_incm IS NULL AND i.accrued_exp_incm IS NULL) OR (p.accrued_exp_incm IS NOT NULL AND i.accrued_exp_incm IS NOT NULL AND p.accrued_exp_incm = i.accrued_exp_incm))) 
OR ( @intACCRUED_INT_INCM = 0 AND NOT ((p.accrued_int_incm IS NULL AND i.accrued_int_incm IS NULL) OR (p.accrued_int_incm IS NOT NULL AND i.accrued_int_incm IS NOT NULL AND p.accrued_int_incm = i.accrued_int_incm))) 
OR ( @intACCT_NAME = 0 AND NOT ((p.acct_name IS NULL AND i.acct_name IS NULL) OR (p.acct_name IS NOT NULL AND i.acct_name IS NOT NULL AND p.acct_name = i.acct_name))) 
OR ( @intACCT_SHT_NAME = 0 AND NOT ((p.acct_sht_name IS NULL AND i.acct_sht_name IS NULL) OR (p.acct_sht_name IS NOT NULL AND i.acct_sht_name IS NOT NULL AND p.acct_sht_name = i.acct_sht_name))) 
OR ( @intACCT_TYP_CD = 0 AND NOT ((p.acct_typ_cd IS NULL AND i.acct_typ_cd IS NULL) OR (p.acct_typ_cd IS NOT NULL AND i.acct_typ_cd IS NOT NULL AND p.acct_typ_cd = i.acct_typ_cd))) 
OR ( @intACTIVE = 0 AND NOT ((p.active IS NULL AND i.active IS NULL) OR (p.active IS NOT NULL AND i.active IS NOT NULL AND p.active = i.active))) 
OR ( @intALLOW_AUTOFX = 0 AND NOT ((p.allow_autofx IS NULL AND i.allow_autofx IS NULL) OR (p.allow_autofx IS NOT NULL AND i.allow_autofx IS NOT NULL AND p.allow_autofx = i.allow_autofx))) 
OR ( @intALLOW_BUY = 0 AND ( p.allow_buy IS NULL OR p.allow_buy != ISNULL( i.allow_buy, 'Y' ) ) )
OR ( @intALLOW_NEG_CASH = 0 AND ( p.allow_neg_cash IS NULL OR p.allow_neg_cash != ISNULL( i.allow_neg_cash, 'Y' ) ) )
OR ( @intALLOW_SELL = 0 AND ( p.allow_sell IS NULL OR p.allow_sell != ISNULL( i.allow_sell, 'Y' ) ) )
OR ( @intAMRTZD_COST = 0 AND NOT ((p.amrtzd_cost IS NULL AND i.amrtzd_cost IS NULL) OR (p.amrtzd_cost IS NOT NULL AND i.amrtzd_cost IS NOT NULL AND p.amrtzd_cost = i.amrtzd_cost))) 
OR ( @intAPPLY_GUIDELINES = 0 AND NOT ((p.apply_guidelines IS NULL AND i.apply_guidelines IS NULL) OR (p.apply_guidelines IS NOT NULL AND i.apply_guidelines IS NOT NULL AND p.apply_guidelines = i.apply_guidelines))) 
OR ( @intAUTH_GROUP = 0 AND NOT ((p.auth_group IS NULL AND i.auth_group IS NULL) OR (p.auth_group IS NOT NULL AND i.auth_group IS NOT NULL AND p.auth_group = i.auth_group))) 
OR ( @intAUTH_NUM = 0 AND ( p.auth_num IS NULL OR p.auth_num != ISNULL( i.auth_num, 0 ) ) )
OR ( @intAUTO_CFWD_IND = 0 AND ( p.auto_cfwd_ind IS NULL OR p.auto_cfwd_ind != ISNULL( i.auto_cfwd_ind, 'N' ) ) )
OR ( @intAVG_COST = 0 AND NOT ((p.avg_cost IS NULL AND i.avg_cost IS NULL) OR (p.avg_cost IS NOT NULL AND i.avg_cost IS NOT NULL AND p.avg_cost = i.avg_cost))) 
OR ( @intBANK_ACCT_NUM = 0 AND NOT ((p.bank_acct_num IS NULL AND i.bank_acct_num IS NULL) OR (p.bank_acct_num IS NOT NULL AND i.bank_acct_num IS NOT NULL AND p.bank_acct_num = i.bank_acct_num))) 
OR ( @intBASKET_TYP_CD = 0 AND NOT ((p.basket_typ_cd IS NULL AND i.basket_typ_cd IS NULL) OR (p.basket_typ_cd IS NOT NULL AND i.basket_typ_cd IS NOT NULL AND p.basket_typ_cd = i.basket_typ_cd))) 
OR ( @intBLOCK_ORDER_IND = 0 AND ( p.block_order_ind IS NULL OR p.block_order_ind != ISNULL( i.block_order_ind, 'Y' ) ) )
OR ( @intBUNDLED_FEE_IND = 0 AND ( p.bundled_fee_ind IS NULL OR p.bundled_fee_ind != ISNULL( i.bundled_fee_ind, 'N' ) ) )
OR ( @intCASH_BAL_SOD = 0 AND NOT ((p.cash_bal_sod IS NULL AND i.cash_bal_sod IS NULL) OR (p.cash_bal_sod IS NOT NULL AND i.cash_bal_sod IS NOT NULL AND p.cash_bal_sod = i.cash_bal_sod))) 
OR ( @intCASH_RSRV_AMT = 0 AND NOT ((p.cash_rsrv_amt IS NULL AND i.cash_rsrv_amt IS NULL) OR (p.cash_rsrv_amt IS NOT NULL AND i.cash_rsrv_amt IS NOT NULL AND p.cash_rsrv_amt = i.cash_rsrv_amt))) 
OR ( @intCASH_RSRV_PCT = 0 AND NOT ((p.cash_rsrv_pct IS NULL AND i.cash_rsrv_pct IS NULL) OR (p.cash_rsrv_pct IS NOT NULL AND i.cash_rsrv_pct IS NOT NULL AND p.cash_rsrv_pct = i.cash_rsrv_pct))) 
OR ( @intCASH_RSRV_TYP = 0 AND ( p.cash_rsrv_typ IS NULL OR p.cash_rsrv_typ != ISNULL( i.cash_rsrv_typ, 'N' ) ) )
OR ( @intCATEGORY1 = 0 AND NOT ((p.category1 IS NULL AND i.category1 IS NULL) OR (p.category1 IS NOT NULL AND i.category1 IS NOT NULL AND p.category1 = i.category1))) 
OR ( @intCATEGORY2 = 0 AND NOT ((p.category2 IS NULL AND i.category2 IS NULL) OR (p.category2 IS NOT NULL AND i.category2 IS NOT NULL AND p.category2 = i.category2))) 
OR ( @intCATEGORY3 = 0 AND NOT ((p.category3 IS NULL AND i.category3 IS NULL) OR (p.category3 IS NOT NULL AND i.category3 IS NOT NULL AND p.category3 = i.category3))) 
OR ( @intCFWD_CLOSE_CPARTY_IND = 0 AND ( p.cfwd_close_cparty_ind IS NULL OR p.cfwd_close_cparty_ind != ISNULL( i.cfwd_close_cparty_ind, 'N' ) ) )
OR ( @intCFWD_NEW_CPARTY_IND = 0 AND ( p.cfwd_new_cparty_ind IS NULL OR p.cfwd_new_cparty_ind != ISNULL( i.cfwd_new_cparty_ind, 'N' ) ) )
OR ( @intCG_TAX_RATE = 0 AND NOT ((p.cg_tax_rate IS NULL AND i.cg_tax_rate IS NULL) OR (p.cg_tax_rate IS NOT NULL AND i.cg_tax_rate IS NOT NULL AND p.cg_tax_rate = i.cg_tax_rate))) 
OR ( @intCLOSE_DATE = 0 AND NOT ((p.close_date IS NULL AND i.close_date IS NULL) OR (p.close_date IS NOT NULL AND i.close_date IS NOT NULL AND p.close_date = i.close_date))) 
OR ( @intCNTRBS = 0 AND NOT ((p.cntrbs IS NULL AND i.cntrbs IS NULL) OR (p.cntrbs IS NOT NULL AND i.cntrbs IS NOT NULL AND p.cntrbs = i.cntrbs))) 
OR ( @intCNTRY_CD = 0 AND NOT ((p.cntry_cd IS NULL AND i.cntry_cd IS NULL) OR (p.cntry_cd IS NOT NULL AND i.cntry_cd IS NOT NULL AND p.cntry_cd = i.cntry_cd))) 
OR ( @intCOMMENTS = 0 AND NOT ((p.comments IS NULL AND i.comments IS NULL) OR (p.comments IS NOT NULL AND i.comments IS NOT NULL AND p.comments = i.comments))) 
OR ( @intCOMP_BID_REQ = 0 AND ( p.comp_bid_req IS NULL OR p.comp_bid_req != ISNULL( i.comp_bid_req, 'N' ) ) )
OR ( @intCROSS_EXT_ACCT_CD = 0 AND NOT ((p.cross_ext_acct_cd IS NULL AND i.cross_ext_acct_cd IS NULL) OR (p.cross_ext_acct_cd IS NOT NULL AND i.cross_ext_acct_cd IS NOT NULL AND p.cross_ext_acct_cd = i.cross_ext_acct_cd))) 
OR ( @intCROSS_GRP_ACCT_CD = 0 AND NOT ((p.cross_grp_acct_cd IS NULL AND i.cross_grp_acct_cd IS NULL) OR (p.cross_grp_acct_cd IS NOT NULL AND i.cross_grp_acct_cd IS NOT NULL AND p.cross_grp_acct_cd = i.cross_grp_acct_cd))) 
OR ( @intCROSS_INT_ACCT_CD = 0 AND NOT ((p.cross_int_acct_cd IS NULL AND i.cross_int_acct_cd IS NULL) OR (p.cross_int_acct_cd IS NOT NULL AND i.cross_int_acct_cd IS NOT NULL AND p.cross_int_acct_cd = i.cross_int_acct_cd))) 
OR ( @intCROSS_MAG_ACCT_CD = 0 AND NOT ((p.cross_mag_acct_cd IS NULL AND i.cross_mag_acct_cd IS NULL) OR (p.cross_mag_acct_cd IS NOT NULL AND i.cross_mag_acct_cd IS NOT NULL AND p.cross_mag_acct_cd = i.cross_mag_acct_cd))) 
OR ( @intCRRNCY_CD = 0 AND NOT ((p.crrncy_cd IS NULL AND i.crrncy_cd IS NULL) OR (p.crrncy_cd IS NOT NULL AND i.crrncy_cd IS NOT NULL AND p.crrncy_cd = i.crrncy_cd))) 
OR ( @intCUR_INVESTED = 0 AND NOT ((p.cur_invested IS NULL AND i.cur_invested IS NULL) OR (p.cur_invested IS NOT NULL AND i.cur_invested IS NOT NULL AND p.cur_invested = i.cur_invested))) 
OR ( @intCUSTODIAN_CD = 0 AND NOT ((p.custodian_cd IS NULL AND i.custodian_cd IS NULL) OR (p.custodian_cd IS NOT NULL AND i.custodian_cd IS NOT NULL AND p.custodian_cd = i.custodian_cd))) 
OR ( @intDEF_BENCH_CD = 0 AND NOT ((p.def_bench_cd IS NULL AND i.def_bench_cd IS NULL) OR (p.def_bench_cd IS NOT NULL AND i.def_bench_cd IS NOT NULL AND p.def_bench_cd = i.def_bench_cd))) 
OR ( @intDEF_BENCH_ID = 0 AND NOT ((p.def_bench_id IS NULL AND i.def_bench_id IS NULL) OR (p.def_bench_id IS NOT NULL AND i.def_bench_id IS NOT NULL AND p.def_bench_id = i.def_bench_id))) 
OR ( @intDEF_BENCH_TYPE = 0 AND NOT ((p.def_bench_type IS NULL AND i.def_bench_type IS NULL) OR (p.def_bench_type IS NOT NULL AND i.def_bench_type IS NOT NULL AND p.def_bench_type = i.def_bench_type))) 
OR ( @intDEF_FWD_DATE = 0 AND NOT ((p.def_fwd_date IS NULL AND i.def_fwd_date IS NULL) OR (p.def_fwd_date IS NOT NULL AND i.def_fwd_date IS NOT NULL AND p.def_fwd_date = i.def_fwd_date))) 
OR ( @intDEF_FWD_DAYS = 0 AND NOT ((p.def_fwd_days IS NULL AND i.def_fwd_days IS NULL) OR (p.def_fwd_days IS NOT NULL AND i.def_fwd_days IS NOT NULL AND p.def_fwd_days = i.def_fwd_days))) 
OR ( @intDEF_FWD_DAYS_IND = 0 AND NOT ((p.def_fwd_days_ind IS NULL AND i.def_fwd_days_ind IS NULL) OR (p.def_fwd_days_ind IS NOT NULL AND i.def_fwd_days_ind IS NOT NULL AND p.def_fwd_days_ind = i.def_fwd_days_ind))) 
OR ( @intDEF_HEDGE_BENCH_CD = 0 AND NOT ((p.def_hedge_bench_cd IS NULL AND i.def_hedge_bench_cd IS NULL) OR (p.def_hedge_bench_cd IS NOT NULL AND i.def_hedge_bench_cd IS NOT NULL AND p.def_hedge_bench_cd = i.def_hedge_bench_cd))) 
OR ( @intDEF_HEDGE_BENCH_ID = 0 AND NOT ((p.def_hedge_bench_id IS NULL AND i.def_hedge_bench_id IS NULL) OR (p.def_hedge_bench_id IS NOT NULL AND i.def_hedge_bench_id IS NOT NULL AND p.def_hedge_bench_id = i.def_hedge_bench_id))) 
OR ( @intDEF_HEDGE_BENCH_TYPE = 0 AND NOT ((p.def_hedge_bench_type IS NULL AND i.def_hedge_bench_type IS NULL) OR (p.def_hedge_bench_type IS NOT NULL AND i.def_hedge_bench_type IS NOT NULL AND p.def_hedge_bench_type = i.def_hedge_bench_type))) 
OR ( @intDEF_HEDGE_MDL_CD = 0 AND NOT ((p.def_hedge_mdl_cd IS NULL AND i.def_hedge_mdl_cd IS NULL) OR (p.def_hedge_mdl_cd IS NOT NULL AND i.def_hedge_mdl_cd IS NOT NULL AND p.def_hedge_mdl_cd = i.def_hedge_mdl_cd))) 
OR ( @intDEF_HEDGE_MDL_ID = 0 AND NOT ((p.def_hedge_mdl_id IS NULL AND i.def_hedge_mdl_id IS NULL) OR (p.def_hedge_mdl_id IS NOT NULL AND i.def_hedge_mdl_id IS NOT NULL AND p.def_hedge_mdl_id = i.def_hedge_mdl_id))) 
OR ( @intDEF_HEDGE_MDL_TYPE = 0 AND NOT ((p.def_hedge_mdl_type IS NULL AND i.def_hedge_mdl_type IS NULL) OR (p.def_hedge_mdl_type IS NOT NULL AND i.def_hedge_mdl_type IS NOT NULL AND p.def_hedge_mdl_type = i.def_hedge_mdl_type))) 
OR ( @intDEF_MDL_ID = 0 AND NOT ((p.def_mdl_id IS NULL AND i.def_mdl_id IS NULL) OR (p.def_mdl_id IS NOT NULL AND i.def_mdl_id IS NOT NULL AND p.def_mdl_id = i.def_mdl_id))) 
OR ( @intDEF_MDL_REM_IND = 0 AND ( p.def_mdl_rem_ind IS NULL OR p.def_mdl_rem_ind != ISNULL( i.def_mdl_rem_ind, 'P' ) ) )
OR ( @intDEF_MDL_TYPE = 0 AND NOT ((p.def_mdl_type IS NULL AND i.def_mdl_type IS NULL) OR (p.def_mdl_type IS NOT NULL AND i.def_mdl_type IS NOT NULL AND p.def_mdl_type = i.def_mdl_type))) 
OR ( @intDEFAULT_GRP_CD = 0 AND NOT ((p.default_grp_cd IS NULL AND i.default_grp_cd IS NULL) OR (p.default_grp_cd IS NOT NULL AND i.default_grp_cd IS NOT NULL AND p.default_grp_cd = i.default_grp_cd))) 
OR ( @intDEFAULT_MODEL_CD = 0 AND NOT ((p.default_model_cd IS NULL AND i.default_model_cd IS NULL) OR (p.default_model_cd IS NOT NULL AND i.default_model_cd IS NOT NULL AND p.default_model_cd = i.default_model_cd))) 
OR ( @intDEFAULT_SPECIAL_INST = 0 AND NOT ((p.default_special_inst IS NULL AND i.default_special_inst IS NULL) OR (p.default_special_inst IS NOT NULL AND i.default_special_inst IS NOT NULL AND p.default_special_inst = i.default_special_inst))) 
OR ( @intDEFAULT_TMP_ACCT = 0 AND NOT ((p.default_tmp_acct IS NULL AND i.default_tmp_acct IS NULL) OR (p.default_tmp_acct IS NOT NULL AND i.default_tmp_acct IS NOT NULL AND p.default_tmp_acct = i.default_tmp_acct))) 
OR ( @intDISC_ACCRUAL_CNVTN = 0 AND NOT ((p.disc_accrual_cnvtn IS NULL AND i.disc_accrual_cnvtn IS NULL) OR (p.disc_accrual_cnvtn IS NOT NULL AND i.disc_accrual_cnvtn IS NOT NULL AND p.disc_accrual_cnvtn = i.disc_accrual_cnvtn))) 
OR ( @intDIV_PAYBL = 0 AND NOT ((p.div_paybl IS NULL AND i.div_paybl IS NULL) OR (p.div_paybl IS NOT NULL AND i.div_paybl IS NOT NULL AND p.div_paybl = i.div_paybl))) 
OR ( @intDIV_RCVBLE = 0 AND NOT ((p.div_rcvble IS NULL AND i.div_rcvble IS NULL) OR (p.div_rcvble IS NOT NULL AND i.div_rcvble IS NOT NULL AND p.div_rcvble = i.div_rcvble))) 
OR ( @intDIV_RECEIVED = 0 AND NOT ((p.div_received IS NULL AND i.div_received IS NULL) OR (p.div_received IS NOT NULL AND i.div_received IS NOT NULL AND p.div_received = i.div_received))) 
OR ( @intDM_IND = 0 AND ( p.dm_ind IS NULL OR p.dm_ind != ISNULL( i.dm_ind, 'N' ) ) )
OR ( @intEQTY_ACCT = 0 AND NOT ((p.eqty_acct IS NULL AND i.eqty_acct IS NULL) OR (p.eqty_acct IS NOT NULL AND i.eqty_acct IS NOT NULL AND p.eqty_acct = i.eqty_acct))) 
OR ( @intERISA_ELIGIBLE = 0 AND ( p.erisa_eligible IS NULL OR p.erisa_eligible != ISNULL( i.erisa_eligible, 'N' ) ) )
OR ( @intEX_ANTE_CALCS_METHOD_CD = 0 AND NOT ((p.ex_ante_calcs_method_cd IS NULL AND i.ex_ante_calcs_method_cd IS NULL) OR (p.ex_ante_calcs_method_cd IS NOT NULL AND i.ex_ante_calcs_method_cd IS NOT NULL AND p.ex_ante_calcs_method_cd = i.ex_ante_calcs_method_cd))) 
OR ( @intEXCH_LIST_CD = 0 AND NOT ((p.exch_list_cd IS NULL AND i.exch_list_cd IS NULL) OR (p.exch_list_cd IS NOT NULL AND i.exch_list_cd IS NOT NULL AND p.exch_list_cd = i.exch_list_cd))) 
OR ( @intFACTOR_MODEL = 0 AND NOT ((p.factor_model IS NULL AND i.factor_model IS NULL) OR (p.factor_model IS NOT NULL AND i.factor_model IS NOT NULL AND p.factor_model = i.factor_model))) 
OR ( @intFEE_PAYMT_IND = 0 AND ( p.fee_paymt_ind IS NULL OR p.fee_paymt_ind != ISNULL( i.fee_paymt_ind, 'Y' ) ) )
OR ( @intFUND_ACCT = 0 AND NOT ((p.fund_acct IS NULL AND i.fund_acct IS NULL) OR (p.fund_acct IS NOT NULL AND i.fund_acct IS NOT NULL AND p.fund_acct = i.fund_acct))) 
OR ( @intFUND_SHRS_OUTST = 0 AND NOT ((p.fund_shrs_outst IS NULL AND i.fund_shrs_outst IS NULL) OR (p.fund_shrs_outst IS NOT NULL AND i.fund_shrs_outst IS NOT NULL AND p.fund_shrs_outst = i.fund_shrs_outst))) 
OR ( @intFUT_BROKER_IND = 0 AND ( p.fut_broker_ind IS NULL OR p.fut_broker_ind != ISNULL( i.fut_broker_ind, 'N' ) ) )
OR ( @intGAIN_LOSS_SENSITIVE = 0 AND ( p.gain_loss_sensitive IS NULL OR p.gain_loss_sensitive != ISNULL( i.gain_loss_sensitive, 'N' ) ) )
OR ( @intGROSS_INCOME = 0 AND NOT ((p.gross_income IS NULL AND i.gross_income IS NULL) OR (p.gross_income IS NOT NULL AND i.gross_income IS NOT NULL AND p.gross_income = i.gross_income))) 
OR ( @intGRP_TYPE_CD = 0 AND NOT ((p.grp_type_cd IS NULL AND i.grp_type_cd IS NULL) OR (p.grp_type_cd IS NOT NULL AND i.grp_type_cd IS NOT NULL AND p.grp_type_cd = i.grp_type_cd))) 
OR ( @intINACTIVE = 0 AND ( p.inactive IS NULL OR p.inactive != ISNULL( i.inactive, 'N' ) ) )
OR ( @intINC_TAX_RATE = 0 AND NOT ((p.inc_tax_rate IS NULL AND i.inc_tax_rate IS NULL) OR (p.inc_tax_rate IS NOT NULL AND i.inc_tax_rate IS NOT NULL AND p.inc_tax_rate = i.inc_tax_rate))) 
OR ( @intINCL_ACCRD_INTEREST = 0 AND NOT ((p.incl_accrd_interest IS NULL AND i.incl_accrd_interest IS NULL) OR (p.incl_accrd_interest IS NOT NULL AND i.incl_accrd_interest IS NOT NULL AND p.incl_accrd_interest = i.incl_accrd_interest))) 
OR ( @intINDEX_SEC_ID = 0 AND NOT ((p.index_sec_id IS NULL AND i.index_sec_id IS NULL) OR (p.index_sec_id IS NOT NULL AND i.index_sec_id IS NOT NULL AND p.index_sec_id = i.index_sec_id))) 
OR ( @intINT_RECEIVED = 0 AND NOT ((p.int_received IS NULL AND i.int_received IS NULL) OR (p.int_received IS NOT NULL AND i.int_received IS NOT NULL AND p.int_received = i.int_received))) 
OR ( @intINTERESTED_PARTY1_CD = 0 AND NOT ((p.interested_party1_cd IS NULL AND i.interested_party1_cd IS NULL) OR (p.interested_party1_cd IS NOT NULL AND i.interested_party1_cd IS NOT NULL AND p.interested_party1_cd = i.interested_party1_cd))) 
OR ( @intINTERESTED_PARTY2_CD = 0 AND NOT ((p.interested_party2_cd IS NULL AND i.interested_party2_cd IS NULL) OR (p.interested_party2_cd IS NOT NULL AND i.interested_party2_cd IS NOT NULL AND p.interested_party2_cd = i.interested_party2_cd))) 
OR ( @intINTERESTED_PARTY3_CD = 0 AND NOT ((p.interested_party3_cd IS NULL AND i.interested_party3_cd IS NULL) OR (p.interested_party3_cd IS NOT NULL AND i.interested_party3_cd IS NOT NULL AND p.interested_party3_cd = i.interested_party3_cd))) 
OR ( @intINV_CLASS_CD = 0 AND NOT ((p.inv_class_cd IS NULL AND i.inv_class_cd IS NULL) OR (p.inv_class_cd IS NOT NULL AND i.inv_class_cd IS NOT NULL AND p.inv_class_cd = i.inv_class_cd))) 
OR ( @intLIABILITIES = 0 AND NOT ((p.liabilities IS NULL AND i.liabilities IS NULL) OR (p.liabilities IS NOT NULL AND i.liabilities IS NOT NULL AND p.liabilities = i.liabilities))) 
OR ( @intLOT_SIZE = 0 AND NOT ((p.lot_size IS NULL AND i.lot_size IS NULL) OR (p.lot_size IS NOT NULL AND i.lot_size IS NOT NULL AND p.lot_size = i.lot_size))) 
OR ( @intMAG_IND = 0 AND ( p.mag_ind IS NULL OR p.mag_ind != ISNULL( i.mag_ind, 'N' ) ) )
OR ( @intMAX_ISSUER_HOLDING = 0 AND NOT ((p.max_issuer_holding IS NULL AND i.max_issuer_holding IS NULL) OR (p.max_issuer_holding IS NOT NULL AND i.max_issuer_holding IS NOT NULL AND p.max_issuer_holding = i.max_issuer_holding))) 
OR ( @intMDL_ANALYTIC_TYPE = 0 AND ( p.mdl_analytic_type IS NULL OR p.mdl_analytic_type != ISNULL( i.mdl_analytic_type, 'N' ) ) )
OR ( @intMDL_BENCH_ACCT_CD = 0 AND NOT ((p.mdl_bench_acct_cd IS NULL AND i.mdl_bench_acct_cd IS NULL) OR (p.mdl_bench_acct_cd IS NOT NULL AND i.mdl_bench_acct_cd IS NOT NULL AND p.mdl_bench_acct_cd = i.mdl_bench_acct_cd))) 
OR ( @intMDL_CAT_WEIGHT_ONLY_IND = 0 AND ( p.mdl_cat_weight_only_ind IS NULL OR p.mdl_cat_weight_only_ind != ISNULL( i.mdl_cat_weight_only_ind, 'N' ) ) )
OR ( @intMDL_QTY_BASED_IND = 0 AND ( p.mdl_qty_based_ind IS NULL OR p.mdl_qty_based_ind != ISNULL( i.mdl_qty_based_ind, 'N' ) ) )
OR ( @intMDL_TYP_IND = 0 AND NOT ((p.mdl_typ_ind IS NULL AND i.mdl_typ_ind IS NULL) OR (p.mdl_typ_ind IS NOT NULL AND i.mdl_typ_ind IS NOT NULL AND p.mdl_typ_ind = i.mdl_typ_ind))) 
OR ( @intMKT_VAL = 0 AND NOT ((p.mkt_val IS NULL AND i.mkt_val IS NULL) OR (p.mkt_val IS NOT NULL AND i.mkt_val IS NOT NULL AND p.mkt_val = i.mkt_val))) 
OR ( @intMKT_VAL_SOD = 0 AND NOT ((p.mkt_val_sod IS NULL AND i.mkt_val_sod IS NULL) OR (p.mkt_val_sod IS NOT NULL AND i.mkt_val_sod IS NOT NULL AND p.mkt_val_sod = i.mkt_val_sod))) 
OR ( @intMODEL_PRIV = 0 AND NOT ((p.model_priv IS NULL AND i.model_priv IS NULL) OR (p.model_priv IS NOT NULL AND i.model_priv IS NOT NULL AND p.model_priv = i.model_priv))) 
OR ( @intMULT_MDL_IND = 0 AND ( p.mult_mdl_ind IS NULL OR p.mult_mdl_ind != ISNULL( i.mult_mdl_ind, 'N' ) ) )
OR ( @intMULT_MDL_NORM_IND = 0 AND ( p.mult_mdl_norm_ind IS NULL OR p.mult_mdl_norm_ind != ISNULL( i.mult_mdl_norm_ind, 'N' ) ) )
OR ( @intNET_ASSETS = 0 AND NOT ((p.net_assets IS NULL AND i.net_assets IS NULL) OR (p.net_assets IS NOT NULL AND i.net_assets IS NOT NULL AND p.net_assets = i.net_assets))) 
OR ( @intNET_CASH = 0 AND NOT ((p.net_cash IS NULL AND i.net_cash IS NULL) OR (p.net_cash IS NOT NULL AND i.net_cash IS NOT NULL AND p.net_cash = i.net_cash))) 
OR ( @intNET_FUNDS_AVAIL = 0 AND NOT ((p.net_funds_avail IS NULL AND i.net_funds_avail IS NULL) OR (p.net_funds_avail IS NOT NULL AND i.net_funds_avail IS NOT NULL AND p.net_funds_avail = i.net_funds_avail))) 
OR ( @intNEXT_REBAL_DATE = 0 AND NOT ((p.next_rebal_date IS NULL AND i.next_rebal_date IS NULL) OR (p.next_rebal_date IS NOT NULL AND i.next_rebal_date IS NOT NULL AND p.next_rebal_date = i.next_rebal_date))) 
OR ( @intNOTNL_FREEZE_IND = 0 AND ( p.notnl_freeze_ind IS NULL OR p.notnl_freeze_ind != ISNULL( i.notnl_freeze_ind, 'N' ) ) )
OR ( @intOASYS_ACCT_CD = 0 AND NOT ((p.oasys_acct_cd IS NULL AND i.oasys_acct_cd IS NULL) OR (p.oasys_acct_cd IS NOT NULL AND i.oasys_acct_cd IS NOT NULL AND p.oasys_acct_cd = i.oasys_acct_cd))) 
OR ( @intOMNIBUS_ACCT_CD = 0 AND NOT ((p.omnibus_acct_cd IS NULL AND i.omnibus_acct_cd IS NULL) OR (p.omnibus_acct_cd IS NOT NULL AND i.omnibus_acct_cd IS NOT NULL AND p.omnibus_acct_cd = i.omnibus_acct_cd))) 
OR ( @intOTH_ASSET = 0 AND NOT ((p.oth_asset IS NULL AND i.oth_asset IS NULL) OR (p.oth_asset IS NOT NULL AND i.oth_asset IS NOT NULL AND p.oth_asset = i.oth_asset))) 
OR ( @intOTH_LIAB = 0 AND NOT ((p.oth_liab IS NULL AND i.oth_liab IS NULL) OR (p.oth_liab IS NOT NULL AND i.oth_liab IS NOT NULL AND p.oth_liab = i.oth_liab))) 
OR ( @intPARENT_CHILD_FLAG = 0 AND NOT ((p.parent_child_flag IS NULL AND i.parent_child_flag IS NULL) OR (p.parent_child_flag IS NOT NULL AND i.parent_child_flag IS NOT NULL AND p.parent_child_flag = i.parent_child_flag))) 
OR ( @intPAYBL_FUND_SHRS_LIQD = 0 AND NOT ((p.paybl_fund_shrs_liqd IS NULL AND i.paybl_fund_shrs_liqd IS NULL) OR (p.paybl_fund_shrs_liqd IS NOT NULL AND i.paybl_fund_shrs_liqd IS NOT NULL AND p.paybl_fund_shrs_liqd = i.paybl_fund_shrs_liqd))) 
OR ( @intPAYBL_SEC_PURCH = 0 AND NOT ((p.paybl_sec_purch IS NULL AND i.paybl_sec_purch IS NULL) OR (p.paybl_sec_purch IS NOT NULL AND i.paybl_sec_purch IS NOT NULL AND p.paybl_sec_purch = i.paybl_sec_purch))) 
OR ( @intPMA_ACCT_TOT_RET_APPROX = 0 AND NOT ((p.pma_acct_tot_ret_approx IS NULL AND i.pma_acct_tot_ret_approx IS NULL) OR (p.pma_acct_tot_ret_approx IS NOT NULL AND i.pma_acct_tot_ret_approx IS NOT NULL AND p.pma_acct_tot_ret_approx = i.pma_acct_tot_ret_approx))) 
OR ( @intPMA_ATTR_METHOD_EQUITY = 0 AND NOT ((p.pma_attr_method_equity IS NULL AND i.pma_attr_method_equity IS NULL) OR (p.pma_attr_method_equity IS NOT NULL AND i.pma_attr_method_equity IS NOT NULL AND p.pma_attr_method_equity = i.pma_attr_method_equity))) 
OR ( @intPMA_ATTR_METHOD_FI = 0 AND NOT ((p.pma_attr_method_fi IS NULL AND i.pma_attr_method_fi IS NULL) OR (p.pma_attr_method_fi IS NOT NULL AND i.pma_attr_method_fi IS NOT NULL AND p.pma_attr_method_fi = i.pma_attr_method_fi))) 
OR ( @intPMA_DATA_FREQ = 0 AND ( p.pma_data_freq IS NULL OR p.pma_data_freq != ISNULL( i.pma_data_freq, 'D' ) ) )
OR ( @intPMA_INCPTN_DATE = 0 AND NOT ((p.pma_incptn_date IS NULL AND i.pma_incptn_date IS NULL) OR (p.pma_incptn_date IS NOT NULL AND i.pma_incptn_date IS NOT NULL AND p.pma_incptn_date = i.pma_incptn_date))) 
OR ( @intPMA_MGMT_FEE_BASIS = 0 AND ( p.pma_mgmt_fee_basis IS NULL OR p.pma_mgmt_fee_basis != ISNULL( i.pma_mgmt_fee_basis, 'G' ) ) )
OR ( @intPMA_TAX_BASIS = 0 AND ( p.pma_tax_basis IS NULL OR p.pma_tax_basis != ISNULL( i.pma_tax_basis, 'G' ) ) )
OR ( @intPMNTS = 0 AND NOT ((p.pmnts IS NULL AND i.pmnts IS NULL) OR (p.pmnts IS NOT NULL AND i.pmnts IS NOT NULL AND p.pmnts = i.pmnts))) 
OR ( @intREBAL_APPROVAL_IND = 0 AND ( p.rebal_approval_ind IS NULL OR p.rebal_approval_ind != ISNULL( i.rebal_approval_ind, 'N' ) ) )
OR ( @intREBAL_APPROVED_IND = 0 AND ( p.rebal_approved_ind IS NULL OR p.rebal_approved_ind != ISNULL( i.rebal_approved_ind, 'N' ) ) )
OR ( @intREBAL_FREQ_CD = 0 AND NOT ((p.rebal_freq_cd IS NULL AND i.rebal_freq_cd IS NULL) OR (p.rebal_freq_cd IS NOT NULL AND i.rebal_freq_cd IS NOT NULL AND p.rebal_freq_cd = i.rebal_freq_cd))) 
OR ( @intRECVB_FUND_SHRS_SOLD = 0 AND NOT ((p.recvb_fund_shrs_sold IS NULL AND i.recvb_fund_shrs_sold IS NULL) OR (p.recvb_fund_shrs_sold IS NOT NULL AND i.recvb_fund_shrs_sold IS NOT NULL AND p.recvb_fund_shrs_sold = i.recvb_fund_shrs_sold))) 
OR ( @intRECVB_SEC_SOLD = 0 AND NOT ((p.recvb_sec_sold IS NULL AND i.recvb_sec_sold IS NULL) OR (p.recvb_sec_sold IS NOT NULL AND i.recvb_sec_sold IS NOT NULL AND p.recvb_sec_sold = i.recvb_sec_sold))) 
OR ( @intREPRICE_IND = 0 AND ( p.reprice_ind IS NULL OR p.reprice_ind != ISNULL( i.reprice_ind, 'N' ) ) )
OR ( @intROTATION_CD = 0 AND NOT ((p.rotation_cd IS NULL AND i.rotation_cd IS NULL) OR (p.rotation_cd IS NOT NULL AND i.rotation_cd IS NOT NULL AND p.rotation_cd = i.rotation_cd))) 
OR ( @intROTATION_SEQ = 0 AND NOT ((p.rotation_seq IS NULL AND i.rotation_seq IS NULL) OR (p.rotation_seq IS NOT NULL AND i.rotation_seq IS NOT NULL AND p.rotation_seq = i.rotation_seq))) 
OR ( @intSERVICE_ID = 0 AND NOT ((p.service_id IS NULL AND i.service_id IS NULL) OR (p.service_id IS NOT NULL AND i.service_id IS NOT NULL AND p.service_id = i.service_id))) 
OR ( @intSHAW_CLASS_CD = 0 AND NOT ((p.shaw_class_cd IS NULL AND i.shaw_class_cd IS NULL) OR (p.shaw_class_cd IS NOT NULL AND i.shaw_class_cd IS NOT NULL AND p.shaw_class_cd = i.shaw_class_cd))) 
OR ( @intSHAW_MANAGER_CD = 0 AND NOT ((p.shaw_manager_cd IS NULL AND i.shaw_manager_cd IS NULL) OR (p.shaw_manager_cd IS NOT NULL AND i.shaw_manager_cd IS NOT NULL AND p.shaw_manager_cd = i.shaw_manager_cd))) 
OR ( @intSHORT_SHORT_GAINS = 0 AND NOT ((p.short_short_gains IS NULL AND i.short_short_gains IS NULL) OR (p.short_short_gains IS NOT NULL AND i.short_short_gains IS NOT NULL AND p.short_short_gains = i.short_short_gains))) 
OR ( @intSLEEVE_TYPE = 0 AND NOT ((p.sleeve_type IS NULL AND i.sleeve_type IS NULL) OR (p.sleeve_type IS NOT NULL AND i.sleeve_type IS NOT NULL AND p.sleeve_type = i.sleeve_type))) 
OR ( @intSTATE_CD = 0 AND NOT ((p.state_cd IS NULL AND i.state_cd IS NULL) OR (p.state_cd IS NOT NULL AND i.state_cd IS NOT NULL AND p.state_cd = i.state_cd))) 
OR ( @intTAX_LOT_PROC_IND = 0 AND ( p.tax_lot_proc_ind IS NULL OR p.tax_lot_proc_ind != ISNULL( i.tax_lot_proc_ind, 'N' ) ) )
OR ( @intTAX_LOT_SELL_CNVTN = 0 AND NOT ((p.tax_lot_sell_cnvtn IS NULL AND i.tax_lot_sell_cnvtn IS NULL) OR (p.tax_lot_sell_cnvtn IS NOT NULL AND i.tax_lot_sell_cnvtn IS NOT NULL AND p.tax_lot_sell_cnvtn = i.tax_lot_sell_cnvtn))) 
OR ( @intTEN_MIN_CHECK_IND = 0 AND ( p.ten_min_check_ind IS NULL OR p.ten_min_check_ind != ISNULL( i.ten_min_check_ind, 'P' ) ) )
OR ( @intTHEME_1 = 0 AND NOT ((p.theme_1 IS NULL AND i.theme_1 IS NULL) OR (p.theme_1 IS NOT NULL AND i.theme_1 IS NOT NULL AND p.theme_1 = i.theme_1))) 
OR ( @intTHEME_2 = 0 AND NOT ((p.theme_2 IS NULL AND i.theme_2 IS NULL) OR (p.theme_2 IS NOT NULL AND i.theme_2 IS NOT NULL AND p.theme_2 = i.theme_2))) 
OR ( @intTHEME_3 = 0 AND NOT ((p.theme_3 IS NULL AND i.theme_3 IS NULL) OR (p.theme_3 IS NOT NULL AND i.theme_3 IS NOT NULL AND p.theme_3 = i.theme_3))) 
OR ( @intTOT_ASSETS = 0 AND NOT ((p.tot_assets IS NULL AND i.tot_assets IS NULL) OR (p.tot_assets IS NOT NULL AND i.tot_assets IS NOT NULL AND p.tot_assets = i.tot_assets))) 
OR ( @intTOT_COST = 0 AND NOT ((p.tot_cost IS NULL AND i.tot_cost IS NULL) OR (p.tot_cost IS NOT NULL AND i.tot_cost IS NOT NULL AND p.tot_cost = i.tot_cost))) 
OR ( @intTOT_INVSTMNTS = 0 AND NOT ((p.tot_invstmnts IS NULL AND i.tot_invstmnts IS NULL) OR (p.tot_invstmnts IS NOT NULL AND i.tot_invstmnts IS NOT NULL AND p.tot_invstmnts = i.tot_invstmnts))) 
OR ( @intTRANSFERS = 0 AND NOT ((p.transfers IS NULL AND i.transfers IS NULL) OR (p.transfers IS NOT NULL AND i.transfers IS NOT NULL AND p.transfers = i.transfers))) 
OR ( @intUDF_CHAR1 = 0 AND NOT ((p.udf_char1 IS NULL AND i.udf_char1 IS NULL) OR (p.udf_char1 IS NOT NULL AND i.udf_char1 IS NOT NULL AND p.udf_char1 = i.udf_char1))) 
OR ( @intUDF_CHAR10 = 0 AND NOT ((p.udf_char10 IS NULL AND i.udf_char10 IS NULL) OR (p.udf_char10 IS NOT NULL AND i.udf_char10 IS NOT NULL AND p.udf_char10 = i.udf_char10))) 
OR ( @intUDF_CHAR11 = 0 AND NOT ((p.udf_char11 IS NULL AND i.udf_char11 IS NULL) OR (p.udf_char11 IS NOT NULL AND i.udf_char11 IS NOT NULL AND p.udf_char11 = i.udf_char11))) 
OR ( @intUDF_CHAR12 = 0 AND NOT ((p.udf_char12 IS NULL AND i.udf_char12 IS NULL) OR (p.udf_char12 IS NOT NULL AND i.udf_char12 IS NOT NULL AND p.udf_char12 = i.udf_char12))) 
OR ( @intUDF_CHAR13 = 0 AND NOT ((p.udf_char13 IS NULL AND i.udf_char13 IS NULL) OR (p.udf_char13 IS NOT NULL AND i.udf_char13 IS NOT NULL AND p.udf_char13 = i.udf_char13))) 
OR ( @intUDF_CHAR14 = 0 AND NOT ((p.udf_char14 IS NULL AND i.udf_char14 IS NULL) OR (p.udf_char14 IS NOT NULL AND i.udf_char14 IS NOT NULL AND p.udf_char14 = i.udf_char14))) 
OR ( @intUDF_CHAR15 = 0 AND NOT ((p.udf_char15 IS NULL AND i.udf_char15 IS NULL) OR (p.udf_char15 IS NOT NULL AND i.udf_char15 IS NOT NULL AND p.udf_char15 = i.udf_char15))) 
OR ( @intUDF_CHAR16 = 0 AND NOT ((p.udf_char16 IS NULL AND i.udf_char16 IS NULL) OR (p.udf_char16 IS NOT NULL AND i.udf_char16 IS NOT NULL AND p.udf_char16 = i.udf_char16))) 
OR ( @intUDF_CHAR17 = 0 AND NOT ((p.udf_char17 IS NULL AND i.udf_char17 IS NULL) OR (p.udf_char17 IS NOT NULL AND i.udf_char17 IS NOT NULL AND p.udf_char17 = i.udf_char17))) 
OR ( @intUDF_CHAR18 = 0 AND NOT ((p.udf_char18 IS NULL AND i.udf_char18 IS NULL) OR (p.udf_char18 IS NOT NULL AND i.udf_char18 IS NOT NULL AND p.udf_char18 = i.udf_char18))) 
OR ( @intUDF_CHAR19 = 0 AND NOT ((p.udf_char19 IS NULL AND i.udf_char19 IS NULL) OR (p.udf_char19 IS NOT NULL AND i.udf_char19 IS NOT NULL AND p.udf_char19 = i.udf_char19))) 
OR ( @intUDF_CHAR2 = 0 AND NOT ((p.udf_char2 IS NULL AND i.udf_char2 IS NULL) OR (p.udf_char2 IS NOT NULL AND i.udf_char2 IS NOT NULL AND p.udf_char2 = i.udf_char2))) 
OR ( @intUDF_CHAR3 = 0 AND NOT ((p.udf_char3 IS NULL AND i.udf_char3 IS NULL) OR (p.udf_char3 IS NOT NULL AND i.udf_char3 IS NOT NULL AND p.udf_char3 = i.udf_char3))) 
OR ( @intUDF_CHAR4 = 0 AND NOT ((p.udf_char4 IS NULL AND i.udf_char4 IS NULL) OR (p.udf_char4 IS NOT NULL AND i.udf_char4 IS NOT NULL AND p.udf_char4 = i.udf_char4))) 
OR ( @intUDF_CHAR5 = 0 AND NOT ((p.udf_char5 IS NULL AND i.udf_char5 IS NULL) OR (p.udf_char5 IS NOT NULL AND i.udf_char5 IS NOT NULL AND p.udf_char5 = i.udf_char5))) 
OR ( @intUDF_CHAR6 = 0 AND NOT ((p.udf_char6 IS NULL AND i.udf_char6 IS NULL) OR (p.udf_char6 IS NOT NULL AND i.udf_char6 IS NOT NULL AND p.udf_char6 = i.udf_char6))) 
OR ( @intUDF_CHAR7 = 0 AND NOT ((p.udf_char7 IS NULL AND i.udf_char7 IS NULL) OR (p.udf_char7 IS NOT NULL AND i.udf_char7 IS NOT NULL AND p.udf_char7 = i.udf_char7))) 
OR ( @intUDF_CHAR8 = 0 AND NOT ((p.udf_char8 IS NULL AND i.udf_char8 IS NULL) OR (p.udf_char8 IS NOT NULL AND i.udf_char8 IS NOT NULL AND p.udf_char8 = i.udf_char8))) 
OR ( @intUDF_CHAR9 = 0 AND NOT ((p.udf_char9 IS NULL AND i.udf_char9 IS NULL) OR (p.udf_char9 IS NOT NULL AND i.udf_char9 IS NOT NULL AND p.udf_char9 = i.udf_char9))) 
OR ( @intUDF_DATE1 = 0 AND NOT ((p.udf_date1 IS NULL AND i.udf_date1 IS NULL) OR (p.udf_date1 IS NOT NULL AND i.udf_date1 IS NOT NULL AND p.udf_date1 = i.udf_date1))) 
OR ( @intUDF_DATE10 = 0 AND NOT ((p.udf_date10 IS NULL AND i.udf_date10 IS NULL) OR (p.udf_date10 IS NOT NULL AND i.udf_date10 IS NOT NULL AND p.udf_date10 = i.udf_date10))) 
OR ( @intUDF_DATE11 = 0 AND NOT ((p.udf_date11 IS NULL AND i.udf_date11 IS NULL) OR (p.udf_date11 IS NOT NULL AND i.udf_date11 IS NOT NULL AND p.udf_date11 = i.udf_date11))) 
OR ( @intUDF_DATE12 = 0 AND NOT ((p.udf_date12 IS NULL AND i.udf_date12 IS NULL) OR (p.udf_date12 IS NOT NULL AND i.udf_date12 IS NOT NULL AND p.udf_date12 = i.udf_date12))) 
OR ( @intUDF_DATE2 = 0 AND NOT ((p.udf_date2 IS NULL AND i.udf_date2 IS NULL) OR (p.udf_date2 IS NOT NULL AND i.udf_date2 IS NOT NULL AND p.udf_date2 = i.udf_date2))) 
OR ( @intUDF_DATE3 = 0 AND NOT ((p.udf_date3 IS NULL AND i.udf_date3 IS NULL) OR (p.udf_date3 IS NOT NULL AND i.udf_date3 IS NOT NULL AND p.udf_date3 = i.udf_date3))) 
OR ( @intUDF_DATE4 = 0 AND NOT ((p.udf_date4 IS NULL AND i.udf_date4 IS NULL) OR (p.udf_date4 IS NOT NULL AND i.udf_date4 IS NOT NULL AND p.udf_date4 = i.udf_date4))) 
OR ( @intUDF_DATE5 = 0 AND NOT ((p.udf_date5 IS NULL AND i.udf_date5 IS NULL) OR (p.udf_date5 IS NOT NULL AND i.udf_date5 IS NOT NULL AND p.udf_date5 = i.udf_date5))) 
OR ( @intUDF_DATE6 = 0 AND NOT ((p.udf_date6 IS NULL AND i.udf_date6 IS NULL) OR (p.udf_date6 IS NOT NULL AND i.udf_date6 IS NOT NULL AND p.udf_date6 = i.udf_date6))) 
OR ( @intUDF_DATE7 = 0 AND NOT ((p.udf_date7 IS NULL AND i.udf_date7 IS NULL) OR (p.udf_date7 IS NOT NULL AND i.udf_date7 IS NOT NULL AND p.udf_date7 = i.udf_date7))) 
OR ( @intUDF_DATE8 = 0 AND NOT ((p.udf_date8 IS NULL AND i.udf_date8 IS NULL) OR (p.udf_date8 IS NOT NULL AND i.udf_date8 IS NOT NULL AND p.udf_date8 = i.udf_date8))) 
OR ( @intUDF_DATE9 = 0 AND NOT ((p.udf_date9 IS NULL AND i.udf_date9 IS NULL) OR (p.udf_date9 IS NOT NULL AND i.udf_date9 IS NOT NULL AND p.udf_date9 = i.udf_date9))) 
OR ( @intUDF_FLOAT1 = 0 AND NOT ((p.udf_float1 IS NULL AND i.udf_float1 IS NULL) OR (p.udf_float1 IS NOT NULL AND i.udf_float1 IS NOT NULL AND p.udf_float1 = i.udf_float1))) 
OR ( @intUDF_FLOAT10 = 0 AND NOT ((p.udf_float10 IS NULL AND i.udf_float10 IS NULL) OR (p.udf_float10 IS NOT NULL AND i.udf_float10 IS NOT NULL AND p.udf_float10 = i.udf_float10))) 
OR ( @intUDF_FLOAT11 = 0 AND NOT ((p.udf_float11 IS NULL AND i.udf_float11 IS NULL) OR (p.udf_float11 IS NOT NULL AND i.udf_float11 IS NOT NULL AND p.udf_float11 = i.udf_float11))) 
OR ( @intUDF_FLOAT12 = 0 AND NOT ((p.udf_float12 IS NULL AND i.udf_float12 IS NULL) OR (p.udf_float12 IS NOT NULL AND i.udf_float12 IS NOT NULL AND p.udf_float12 = i.udf_float12))) 
OR ( @intUDF_FLOAT13 = 0 AND NOT ((p.udf_float13 IS NULL AND i.udf_float13 IS NULL) OR (p.udf_float13 IS NOT NULL AND i.udf_float13 IS NOT NULL AND p.udf_float13 = i.udf_float13))) 
OR ( @intUDF_FLOAT14 = 0 AND NOT ((p.udf_float14 IS NULL AND i.udf_float14 IS NULL) OR (p.udf_float14 IS NOT NULL AND i.udf_float14 IS NOT NULL AND p.udf_float14 = i.udf_float14))) 
OR ( @intUDF_FLOAT15 = 0 AND NOT ((p.udf_float15 IS NULL AND i.udf_float15 IS NULL) OR (p.udf_float15 IS NOT NULL AND i.udf_float15 IS NOT NULL AND p.udf_float15 = i.udf_float15))) 
OR ( @intUDF_FLOAT16 = 0 AND NOT ((p.udf_float16 IS NULL AND i.udf_float16 IS NULL) OR (p.udf_float16 IS NOT NULL AND i.udf_float16 IS NOT NULL AND p.udf_float16 = i.udf_float16))) 
OR ( @intUDF_FLOAT17 = 0 AND NOT ((p.udf_float17 IS NULL AND i.udf_float17 IS NULL) OR (p.udf_float17 IS NOT NULL AND i.udf_float17 IS NOT NULL AND p.udf_float17 = i.udf_float17))) 
OR ( @intUDF_FLOAT18 = 0 AND NOT ((p.udf_float18 IS NULL AND i.udf_float18 IS NULL) OR (p.udf_float18 IS NOT NULL AND i.udf_float18 IS NOT NULL AND p.udf_float18 = i.udf_float18))) 
OR ( @intUDF_FLOAT19 = 0 AND NOT ((p.udf_float19 IS NULL AND i.udf_float19 IS NULL) OR (p.udf_float19 IS NOT NULL AND i.udf_float19 IS NOT NULL AND p.udf_float19 = i.udf_float19))) 
OR ( @intUDF_FLOAT2 = 0 AND NOT ((p.udf_float2 IS NULL AND i.udf_float2 IS NULL) OR (p.udf_float2 IS NOT NULL AND i.udf_float2 IS NOT NULL AND p.udf_float2 = i.udf_float2))) 
OR ( @intUDF_FLOAT3 = 0 AND NOT ((p.udf_float3 IS NULL AND i.udf_float3 IS NULL) OR (p.udf_float3 IS NOT NULL AND i.udf_float3 IS NOT NULL AND p.udf_float3 = i.udf_float3))) 
OR ( @intUDF_FLOAT4 = 0 AND NOT ((p.udf_float4 IS NULL AND i.udf_float4 IS NULL) OR (p.udf_float4 IS NOT NULL AND i.udf_float4 IS NOT NULL AND p.udf_float4 = i.udf_float4))) 
OR ( @intUDF_FLOAT5 = 0 AND NOT ((p.udf_float5 IS NULL AND i.udf_float5 IS NULL) OR (p.udf_float5 IS NOT NULL AND i.udf_float5 IS NOT NULL AND p.udf_float5 = i.udf_float5))) 
OR ( @intUDF_FLOAT6 = 0 AND NOT ((p.udf_float6 IS NULL AND i.udf_float6 IS NULL) OR (p.udf_float6 IS NOT NULL AND i.udf_float6 IS NOT NULL AND p.udf_float6 = i.udf_float6))) 
OR ( @intUDF_FLOAT7 = 0 AND NOT ((p.udf_float7 IS NULL AND i.udf_float7 IS NULL) OR (p.udf_float7 IS NOT NULL AND i.udf_float7 IS NOT NULL AND p.udf_float7 = i.udf_float7))) 
OR ( @intUDF_FLOAT8 = 0 AND NOT ((p.udf_float8 IS NULL AND i.udf_float8 IS NULL) OR (p.udf_float8 IS NOT NULL AND i.udf_float8 IS NOT NULL AND p.udf_float8 = i.udf_float8))) 
OR ( @intUDF_FLOAT9 = 0 AND NOT ((p.udf_float9 IS NULL AND i.udf_float9 IS NULL) OR (p.udf_float9 IS NOT NULL AND i.udf_float9 IS NOT NULL AND p.udf_float9 = i.udf_float9))) 
OR ( @intUSE_SETTLE_DATE_BAL_IND = 0 AND ( p.use_settle_date_bal_ind IS NULL OR p.use_settle_date_bal_ind != ISNULL( i.use_settle_date_bal_ind, 'N' ) ) )
OR ( @intUSR_CLASS_CD_1 = 0 AND NOT ((p.usr_class_cd_1 IS NULL AND i.usr_class_cd_1 IS NULL) OR (p.usr_class_cd_1 IS NOT NULL AND i.usr_class_cd_1 IS NOT NULL AND p.usr_class_cd_1 = i.usr_class_cd_1))) 
OR ( @intUSR_CLASS_CD_2 = 0 AND NOT ((p.usr_class_cd_2 IS NULL AND i.usr_class_cd_2 IS NULL) OR (p.usr_class_cd_2 IS NOT NULL AND i.usr_class_cd_2 IS NOT NULL AND p.usr_class_cd_2 = i.usr_class_cd_2))) 
OR ( @intUSR_CLASS_CD_3 = 0 AND NOT ((p.usr_class_cd_3 IS NULL AND i.usr_class_cd_3 IS NULL) OR (p.usr_class_cd_3 IS NOT NULL AND i.usr_class_cd_3 IS NOT NULL AND p.usr_class_cd_3 = i.usr_class_cd_3))) 
OR ( @intUSR_CLASS_CD_4 = 0 AND NOT ((p.usr_class_cd_4 IS NULL AND i.usr_class_cd_4 IS NULL) OR (p.usr_class_cd_4 IS NOT NULL AND i.usr_class_cd_4 IS NOT NULL AND p.usr_class_cd_4 = i.usr_class_cd_4))) 
OR ( @intVIEW_ID = 0 AND NOT ((p.view_id IS NULL AND i.view_id IS NULL) OR (p.view_id IS NOT NULL AND i.view_id IS NOT NULL AND p.view_id = i.view_id))) 
OR ( @intYTD_GL_LONG_TERM = 0 AND NOT ((p.ytd_gl_long_term IS NULL AND i.ytd_gl_long_term IS NULL) OR (p.ytd_gl_long_term IS NOT NULL AND i.ytd_gl_long_term IS NOT NULL AND p.ytd_gl_long_term = i.ytd_gl_long_term))) 
OR ( @intYTD_GL_SHORT_TERM = 0 AND NOT ((p.ytd_gl_short_term IS NULL AND i.ytd_gl_short_term IS NULL) OR (p.ytd_gl_short_term IS NOT NULL AND i.ytd_gl_short_term IS NOT NULL AND p.ytd_gl_short_term = i.ytd_gl_short_term))) 

					OR (@intMKT_VAL = 0 AND (NOT ((p.cur_mkt_val IS NULL AND i.mkt_val IS NULL) OR (p.cur_mkt_val IS NOT NULL AND i.mkt_val IS NOT NULL AND p.cur_mkt_val = i.mkt_val))))
					OR (@intMANAGER = 0 AND NOT ((p.manager IS     NULL AND ISNULL (i.manager, @p_strDefaultManager) IS     NULL) OR
																			 (p.manager IS NOT NULL AND ISNULL (i.manager, @p_strDefaultManager) IS NOT NULL AND
																				p.manager =        UPPER (ISNULL (i.manager, @p_strDefaultManager)))))
					OR (@intMANAGER = 1 AND NOT ((p.manager IS     NULL AND ISNULL (p.manager, @p_strDefaultManager) IS     NULL) OR
																			 (p.manager IS NOT NULL AND ISNULL (p.manager, @p_strDefaultManager) IS NOT NULL AND
																				p.manager =        UPPER (ISNULL (p.manager, @p_strDefaultManager)))))
					OR (NOT ((p.syst_of_reference IS NULL AND @p_strExtSys IS NULL) OR (p.syst_of_reference IS NOT NULL AND @p_strExtSys IS NOT NULL AND p.syst_of_reference = @p_strExtSys)))
    )
    SELECT  @intError = @@error , @intUpdateCount = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_FUND'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_FUND'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intUpdateCount, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

    -- load new accounts into the fund table.
    INSERT INTO CS_FUND (
			  accrued_exp_incm,
accrued_int_incm,
acct_sht_name,
allow_autofx,
allow_buy,
allow_neg_cash,
allow_sell,
amrtzd_cost,
apply_guidelines,
auth_group,
auth_num,
auto_cfwd_ind,
avg_cost,
bank_acct_num,
basket_typ_cd,
block_order_ind,
bundled_fee_ind,
cash_bal_sod,
cash_rsrv_amt,
cash_rsrv_pct,
cash_rsrv_typ,
category1,
category2,
category3,
cfwd_close_cparty_ind,
cfwd_new_cparty_ind,
cg_tax_rate,
close_date,
cntrbs,
cntry_cd,
comments,
comp_bid_req,
cross_ext_acct_cd,
cross_grp_acct_cd,
cross_int_acct_cd,
cross_mag_acct_cd,
cur_invested,
custodian_cd,
def_bench_cd,
def_bench_id,
def_bench_type,
def_fwd_date,
def_fwd_days,
def_fwd_days_ind,
def_hedge_bench_cd,
def_hedge_bench_id,
def_hedge_bench_type,
def_hedge_mdl_cd,
def_hedge_mdl_id,
def_hedge_mdl_type,
def_mdl_id,
def_mdl_rem_ind,
def_mdl_type,
default_grp_cd,
default_model_cd,
default_special_inst,
default_tmp_acct,
disc_accrual_cnvtn,
div_paybl,
div_rcvble,
div_received,
dm_ind,
eqty_acct,
erisa_eligible,
ex_ante_calcs_method_cd,
exch_list_cd,
factor_model,
fee_paymt_ind,
fund_acct,
fund_shrs_outst,
fut_broker_ind,
gain_loss_sensitive,
gross_income,
grp_type_cd,
inactive,
inc_tax_rate,
incl_accrd_interest,
index_sec_id,
int_received,
interested_party1_cd,
interested_party2_cd,
interested_party3_cd,
inv_class_cd,
liabilities,
lot_size,
mag_ind,
max_issuer_holding,
mdl_analytic_type,
mdl_bench_acct_cd,
mdl_cat_weight_only_ind,
mdl_qty_based_ind,
mdl_typ_ind,
mkt_val,
mkt_val_sod,
model_priv,
mult_mdl_ind,
mult_mdl_norm_ind,
net_assets,
net_cash,
net_funds_avail,
next_rebal_date,
notnl_freeze_ind,
oasys_acct_cd,
omnibus_acct_cd,
oth_asset,
oth_liab,
parent_child_flag,
paybl_fund_shrs_liqd,
paybl_sec_purch,
pma_acct_tot_ret_approx,
pma_attr_method_equity,
pma_attr_method_fi,
pma_comment,
pma_data_freq,
pma_footnote,
pma_incptn_date,
pma_mgmt_fee_basis,
pma_tax_basis,
pmnts,
rebal_approval_ind,
rebal_approved_ind,
rebal_freq_cd,
recvb_fund_shrs_sold,
recvb_sec_sold,
reprice_ind,
rotation_cd,
rotation_seq,
service_id,
shaw_class_cd,
shaw_manager_cd,
short_short_gains,
sleeve_type,
state_cd,
tax_lot_proc_ind,
tax_lot_sell_cnvtn,
ten_min_check_ind,
theme_2,
theme_3,
tot_assets,
tot_cost,
tot_invstmnts,
transfers,
udf_char1,
udf_char10,
udf_char11,
udf_char12,
udf_char13,
udf_char14,
udf_char15,
udf_char16,
udf_char17,
udf_char18,
udf_char19,
udf_char2,
udf_char3,
udf_char4,
udf_char5,
udf_char6,
udf_char7,
udf_char8,
udf_char9,
udf_date1,
udf_date10,
udf_date11,
udf_date12,
udf_date2,
udf_date3,
udf_date4,
udf_date5,
udf_date6,
udf_date7,
udf_date8,
udf_date9,
udf_float1,
udf_float10,
udf_float11,
udf_float12,
udf_float13,
udf_float14,
udf_float15,
udf_float16,
udf_float17,
udf_float18,
udf_float19,
udf_float2,
udf_float3,
udf_float4,
udf_float5,
udf_float6,
udf_float7,
udf_float8,
udf_float9,
use_settle_date_bal_ind,
usr_class_cd_1,
usr_class_cd_2,
usr_class_cd_3,
usr_class_cd_4,
view_id,
ytd_gl_long_term,
ytd_gl_short_term,
				acct_cd,
				acct_name,
				acct_typ_cd,
				compliance_tolerance,
				crrncy_cd,
				cur_mkt_val,
				manager,
				syst_of_reference,
				theme_1,
				update_in_progress,
				update_seqno,
				record_version,
				acct_id,
				last_upd_user,
				last_upd_date,
				create_date)
    SELECT
			  CASE WHEN @intACCRUED_EXP_INCM = 1 THEN NULL ELSE i.accrued_exp_incm END,
CASE WHEN @intACCRUED_INT_INCM = 1 THEN NULL ELSE i.accrued_int_incm END,
CASE WHEN @intACCT_SHT_NAME = 1 THEN NULL ELSE i.acct_sht_name END,
CASE WHEN @intALLOW_AUTOFX = 1 THEN NULL ELSE i.allow_autofx END,
CASE WHEN @intALLOW_BUY = 1 THEN 'Y' ELSE ISNULL( i.allow_buy, 'Y' ) END,
CASE WHEN @intALLOW_NEG_CASH = 1 THEN 'Y' ELSE ISNULL( i.allow_neg_cash, 'Y' ) END,
CASE WHEN @intALLOW_SELL = 1 THEN 'Y' ELSE ISNULL( i.allow_sell, 'Y' ) END,
CASE WHEN @intAMRTZD_COST = 1 THEN NULL ELSE i.amrtzd_cost END,
CASE WHEN @intAPPLY_GUIDELINES = 1 THEN NULL ELSE i.apply_guidelines END,
CASE WHEN @intAUTH_GROUP = 1 THEN NULL ELSE i.auth_group END,
CASE WHEN @intAUTH_NUM = 1 THEN 0 ELSE ISNULL( i.auth_num, 0 ) END,
CASE WHEN @intAUTO_CFWD_IND = 1 THEN 'N' ELSE ISNULL( i.auto_cfwd_ind, 'N' ) END,
CASE WHEN @intAVG_COST = 1 THEN NULL ELSE i.avg_cost END,
CASE WHEN @intBANK_ACCT_NUM = 1 THEN NULL ELSE i.bank_acct_num END,
CASE WHEN @intBASKET_TYP_CD = 1 THEN NULL ELSE i.basket_typ_cd END,
CASE WHEN @intBLOCK_ORDER_IND = 1 THEN 'Y' ELSE ISNULL( i.block_order_ind, 'Y' ) END,
CASE WHEN @intBUNDLED_FEE_IND = 1 THEN 'N' ELSE ISNULL( i.bundled_fee_ind, 'N' ) END,
CASE WHEN @intCASH_BAL_SOD = 1 THEN NULL ELSE i.cash_bal_sod END,
CASE WHEN @intCASH_RSRV_AMT = 1 THEN NULL ELSE i.cash_rsrv_amt END,
CASE WHEN @intCASH_RSRV_PCT = 1 THEN NULL ELSE i.cash_rsrv_pct END,
CASE WHEN @intCASH_RSRV_TYP = 1 THEN 'N' ELSE ISNULL( i.cash_rsrv_typ, 'N' ) END,
CASE WHEN @intCATEGORY1 = 1 THEN NULL ELSE i.category1 END,
CASE WHEN @intCATEGORY2 = 1 THEN NULL ELSE i.category2 END,
CASE WHEN @intCATEGORY3 = 1 THEN NULL ELSE i.category3 END,
CASE WHEN @intCFWD_CLOSE_CPARTY_IND = 1 THEN 'N' ELSE ISNULL( i.cfwd_close_cparty_ind, 'N' ) END,
CASE WHEN @intCFWD_NEW_CPARTY_IND = 1 THEN 'N' ELSE ISNULL( i.cfwd_new_cparty_ind, 'N' ) END,
CASE WHEN @intCG_TAX_RATE = 1 THEN NULL ELSE i.cg_tax_rate END,
CASE WHEN @intCLOSE_DATE = 1 THEN NULL ELSE i.close_date END,
CASE WHEN @intCNTRBS = 1 THEN NULL ELSE i.cntrbs END,
CASE WHEN @intCNTRY_CD = 1 THEN NULL ELSE i.cntry_cd END,
CASE WHEN @intCOMMENTS = 1 THEN NULL ELSE i.comments END,
CASE WHEN @intCOMP_BID_REQ = 1 THEN 'N' ELSE ISNULL( i.comp_bid_req, 'N' ) END,
CASE WHEN @intCROSS_EXT_ACCT_CD = 1 THEN NULL ELSE i.cross_ext_acct_cd END,
CASE WHEN @intCROSS_GRP_ACCT_CD = 1 THEN NULL ELSE i.cross_grp_acct_cd END,
CASE WHEN @intCROSS_INT_ACCT_CD = 1 THEN NULL ELSE i.cross_int_acct_cd END,
CASE WHEN @intCROSS_MAG_ACCT_CD = 1 THEN NULL ELSE i.cross_mag_acct_cd END,
CASE WHEN @intCUR_INVESTED = 1 THEN NULL ELSE i.cur_invested END,
CASE WHEN @intCUSTODIAN_CD = 1 THEN NULL ELSE i.custodian_cd END,
CASE WHEN @intDEF_BENCH_CD = 1 THEN NULL ELSE i.def_bench_cd END,
CASE WHEN @intDEF_BENCH_ID = 1 THEN NULL ELSE i.def_bench_id END,
CASE WHEN @intDEF_BENCH_TYPE = 1 THEN NULL ELSE i.def_bench_type END,
CASE WHEN @intDEF_FWD_DATE = 1 THEN NULL ELSE i.def_fwd_date END,
CASE WHEN @intDEF_FWD_DAYS = 1 THEN NULL ELSE i.def_fwd_days END,
CASE WHEN @intDEF_FWD_DAYS_IND = 1 THEN NULL ELSE i.def_fwd_days_ind END,
CASE WHEN @intDEF_HEDGE_BENCH_CD = 1 THEN NULL ELSE i.def_hedge_bench_cd END,
CASE WHEN @intDEF_HEDGE_BENCH_ID = 1 THEN NULL ELSE i.def_hedge_bench_id END,
CASE WHEN @intDEF_HEDGE_BENCH_TYPE = 1 THEN NULL ELSE i.def_hedge_bench_type END,
CASE WHEN @intDEF_HEDGE_MDL_CD = 1 THEN NULL ELSE i.def_hedge_mdl_cd END,
CASE WHEN @intDEF_HEDGE_MDL_ID = 1 THEN NULL ELSE i.def_hedge_mdl_id END,
CASE WHEN @intDEF_HEDGE_MDL_TYPE = 1 THEN NULL ELSE i.def_hedge_mdl_type END,
CASE WHEN @intDEF_MDL_ID = 1 THEN NULL ELSE i.def_mdl_id END,
CASE WHEN @intDEF_MDL_REM_IND = 1 THEN 'P' ELSE ISNULL( i.def_mdl_rem_ind, 'P' ) END,
CASE WHEN @intDEF_MDL_TYPE = 1 THEN NULL ELSE i.def_mdl_type END,
CASE WHEN @intDEFAULT_GRP_CD = 1 THEN NULL ELSE i.default_grp_cd END,
CASE WHEN @intDEFAULT_MODEL_CD = 1 THEN NULL ELSE i.default_model_cd END,
CASE WHEN @intDEFAULT_SPECIAL_INST = 1 THEN NULL ELSE i.default_special_inst END,
CASE WHEN @intDEFAULT_TMP_ACCT = 1 THEN NULL ELSE i.default_tmp_acct END,
CASE WHEN @intDISC_ACCRUAL_CNVTN = 1 THEN NULL ELSE i.disc_accrual_cnvtn END,
CASE WHEN @intDIV_PAYBL = 1 THEN NULL ELSE i.div_paybl END,
CASE WHEN @intDIV_RCVBLE = 1 THEN NULL ELSE i.div_rcvble END,
CASE WHEN @intDIV_RECEIVED = 1 THEN NULL ELSE i.div_received END,
CASE WHEN @intDM_IND = 1 THEN 'N' ELSE ISNULL( i.dm_ind, 'N' ) END,
CASE WHEN @intEQTY_ACCT = 1 THEN NULL ELSE i.eqty_acct END,
CASE WHEN @intERISA_ELIGIBLE = 1 THEN 'N' ELSE ISNULL( i.erisa_eligible, 'N' ) END,
CASE WHEN @intEX_ANTE_CALCS_METHOD_CD = 1 THEN NULL ELSE i.ex_ante_calcs_method_cd END,
CASE WHEN @intEXCH_LIST_CD = 1 THEN NULL ELSE i.exch_list_cd END,
CASE WHEN @intFACTOR_MODEL = 1 THEN NULL ELSE i.factor_model END,
CASE WHEN @intFEE_PAYMT_IND = 1 THEN 'Y' ELSE ISNULL( i.fee_paymt_ind, 'Y' ) END,
CASE WHEN @intFUND_ACCT = 1 THEN NULL ELSE i.fund_acct END,
CASE WHEN @intFUND_SHRS_OUTST = 1 THEN NULL ELSE i.fund_shrs_outst END,
CASE WHEN @intFUT_BROKER_IND = 1 THEN 'N' ELSE ISNULL( i.fut_broker_ind, 'N' ) END,
CASE WHEN @intGAIN_LOSS_SENSITIVE = 1 THEN 'N' ELSE ISNULL( i.gain_loss_sensitive, 'N' ) END,
CASE WHEN @intGROSS_INCOME = 1 THEN NULL ELSE i.gross_income END,
CASE WHEN @intGRP_TYPE_CD = 1 THEN NULL ELSE i.grp_type_cd END,
CASE WHEN @intINACTIVE = 1 THEN 'N' ELSE ISNULL( i.inactive, 'N' ) END,
CASE WHEN @intINC_TAX_RATE = 1 THEN NULL ELSE i.inc_tax_rate END,
CASE WHEN @intINCL_ACCRD_INTEREST = 1 THEN NULL ELSE i.incl_accrd_interest END,
CASE WHEN @intINDEX_SEC_ID = 1 THEN NULL ELSE i.index_sec_id END,
CASE WHEN @intINT_RECEIVED = 1 THEN NULL ELSE i.int_received END,
CASE WHEN @intINTERESTED_PARTY1_CD = 1 THEN NULL ELSE i.interested_party1_cd END,
CASE WHEN @intINTERESTED_PARTY2_CD = 1 THEN NULL ELSE i.interested_party2_cd END,
CASE WHEN @intINTERESTED_PARTY3_CD = 1 THEN NULL ELSE i.interested_party3_cd END,
CASE WHEN @intINV_CLASS_CD = 1 THEN NULL ELSE i.inv_class_cd END,
CASE WHEN @intLIABILITIES = 1 THEN NULL ELSE i.liabilities END,
CASE WHEN @intLOT_SIZE = 1 THEN NULL ELSE i.lot_size END,
CASE WHEN @intMAG_IND = 1 THEN 'N' ELSE ISNULL( i.mag_ind, 'N' ) END,
CASE WHEN @intMAX_ISSUER_HOLDING = 1 THEN NULL ELSE i.max_issuer_holding END,
CASE WHEN @intMDL_ANALYTIC_TYPE = 1 THEN 'N' ELSE ISNULL( i.mdl_analytic_type, 'N' ) END,
CASE WHEN @intMDL_BENCH_ACCT_CD = 1 THEN NULL ELSE i.mdl_bench_acct_cd END,
CASE WHEN @intMDL_CAT_WEIGHT_ONLY_IND = 1 THEN 'N' ELSE ISNULL( i.mdl_cat_weight_only_ind, 'N' ) END,
CASE WHEN @intMDL_QTY_BASED_IND = 1 THEN 'N' ELSE ISNULL( i.mdl_qty_based_ind, 'N' ) END,
CASE WHEN @intMDL_TYP_IND = 1 THEN NULL ELSE i.mdl_typ_ind END,
CASE WHEN @intMKT_VAL = 1 THEN NULL ELSE i.mkt_val END,
CASE WHEN @intMKT_VAL_SOD = 1 THEN NULL ELSE i.mkt_val_sod END,
CASE WHEN @intMODEL_PRIV = 1 THEN NULL ELSE i.model_priv END,
CASE WHEN @intMULT_MDL_IND = 1 THEN 'N' ELSE ISNULL( i.mult_mdl_ind, 'N' ) END,
CASE WHEN @intMULT_MDL_NORM_IND = 1 THEN 'N' ELSE ISNULL( i.mult_mdl_norm_ind, 'N' ) END,
CASE WHEN @intNET_ASSETS = 1 THEN NULL ELSE i.net_assets END,
CASE WHEN @intNET_CASH = 1 THEN NULL ELSE i.net_cash END,
CASE WHEN @intNET_FUNDS_AVAIL = 1 THEN NULL ELSE i.net_funds_avail END,
CASE WHEN @intNEXT_REBAL_DATE = 1 THEN NULL ELSE i.next_rebal_date END,
CASE WHEN @intNOTNL_FREEZE_IND = 1 THEN 'N' ELSE ISNULL( i.notnl_freeze_ind, 'N' ) END,
CASE WHEN @intOASYS_ACCT_CD = 1 THEN NULL ELSE i.oasys_acct_cd END,
CASE WHEN @intOMNIBUS_ACCT_CD = 1 THEN NULL ELSE i.omnibus_acct_cd END,
CASE WHEN @intOTH_ASSET = 1 THEN NULL ELSE i.oth_asset END,
CASE WHEN @intOTH_LIAB = 1 THEN NULL ELSE i.oth_liab END,
CASE WHEN @intPARENT_CHILD_FLAG = 1 THEN NULL ELSE i.parent_child_flag END,
CASE WHEN @intPAYBL_FUND_SHRS_LIQD = 1 THEN NULL ELSE i.paybl_fund_shrs_liqd END,
CASE WHEN @intPAYBL_SEC_PURCH = 1 THEN NULL ELSE i.paybl_sec_purch END,
CASE WHEN @intPMA_ACCT_TOT_RET_APPROX = 1 THEN NULL ELSE i.pma_acct_tot_ret_approx END,
CASE WHEN @intPMA_ATTR_METHOD_EQUITY = 1 THEN NULL ELSE i.pma_attr_method_equity END,
CASE WHEN @intPMA_ATTR_METHOD_FI = 1 THEN NULL ELSE i.pma_attr_method_fi END,
CASE WHEN @intPMA_COMMENT = 1 THEN NULL ELSE i.pma_comment END,
CASE WHEN @intPMA_DATA_FREQ = 1 THEN 'D' ELSE ISNULL( i.pma_data_freq, 'D' ) END,
CASE WHEN @intPMA_FOOTNOTE = 1 THEN NULL ELSE i.pma_footnote END,
CASE WHEN @intPMA_INCPTN_DATE = 1 THEN NULL ELSE i.pma_incptn_date END,
CASE WHEN @intPMA_MGMT_FEE_BASIS = 1 THEN 'G' ELSE ISNULL( i.pma_mgmt_fee_basis, 'G' ) END,
CASE WHEN @intPMA_TAX_BASIS = 1 THEN 'G' ELSE ISNULL( i.pma_tax_basis, 'G' ) END,
CASE WHEN @intPMNTS = 1 THEN NULL ELSE i.pmnts END,
CASE WHEN @intREBAL_APPROVAL_IND = 1 THEN 'N' ELSE ISNULL( i.rebal_approval_ind, 'N' ) END,
CASE WHEN @intREBAL_APPROVED_IND = 1 THEN 'N' ELSE ISNULL( i.rebal_approved_ind, 'N' ) END,
CASE WHEN @intREBAL_FREQ_CD = 1 THEN NULL ELSE i.rebal_freq_cd END,
CASE WHEN @intRECVB_FUND_SHRS_SOLD = 1 THEN NULL ELSE i.recvb_fund_shrs_sold END,
CASE WHEN @intRECVB_SEC_SOLD = 1 THEN NULL ELSE i.recvb_sec_sold END,
CASE WHEN @intREPRICE_IND = 1 THEN 'N' ELSE ISNULL( i.reprice_ind, 'N' ) END,
CASE WHEN @intROTATION_CD = 1 THEN NULL ELSE i.rotation_cd END,
CASE WHEN @intROTATION_SEQ = 1 THEN NULL ELSE i.rotation_seq END,
CASE WHEN @intSERVICE_ID = 1 THEN NULL ELSE i.service_id END,
CASE WHEN @intSHAW_CLASS_CD = 1 THEN NULL ELSE i.shaw_class_cd END,
CASE WHEN @intSHAW_MANAGER_CD = 1 THEN NULL ELSE i.shaw_manager_cd END,
CASE WHEN @intSHORT_SHORT_GAINS = 1 THEN NULL ELSE i.short_short_gains END,
CASE WHEN @intSLEEVE_TYPE = 1 THEN NULL ELSE i.sleeve_type END,
CASE WHEN @intSTATE_CD = 1 THEN NULL ELSE i.state_cd END,
CASE WHEN @intTAX_LOT_PROC_IND = 1 THEN 'N' ELSE ISNULL( i.tax_lot_proc_ind, 'N' ) END,
CASE WHEN @intTAX_LOT_SELL_CNVTN = 1 THEN NULL ELSE i.tax_lot_sell_cnvtn END,
CASE WHEN @intTEN_MIN_CHECK_IND = 1 THEN 'P' ELSE ISNULL( i.ten_min_check_ind, 'P' ) END,
CASE WHEN @intTHEME_2 = 1 THEN NULL ELSE i.theme_2 END,
CASE WHEN @intTHEME_3 = 1 THEN NULL ELSE i.theme_3 END,
CASE WHEN @intTOT_ASSETS = 1 THEN NULL ELSE i.tot_assets END,
CASE WHEN @intTOT_COST = 1 THEN NULL ELSE i.tot_cost END,
CASE WHEN @intTOT_INVSTMNTS = 1 THEN NULL ELSE i.tot_invstmnts END,
CASE WHEN @intTRANSFERS = 1 THEN NULL ELSE i.transfers END,
CASE WHEN @intUDF_CHAR1 = 1 THEN NULL ELSE i.udf_char1 END,
CASE WHEN @intUDF_CHAR10 = 1 THEN NULL ELSE i.udf_char10 END,
CASE WHEN @intUDF_CHAR11 = 1 THEN NULL ELSE i.udf_char11 END,
CASE WHEN @intUDF_CHAR12 = 1 THEN NULL ELSE i.udf_char12 END,
CASE WHEN @intUDF_CHAR13 = 1 THEN NULL ELSE i.udf_char13 END,
CASE WHEN @intUDF_CHAR14 = 1 THEN NULL ELSE i.udf_char14 END,
CASE WHEN @intUDF_CHAR15 = 1 THEN NULL ELSE i.udf_char15 END,
CASE WHEN @intUDF_CHAR16 = 1 THEN NULL ELSE i.udf_char16 END,
CASE WHEN @intUDF_CHAR17 = 1 THEN NULL ELSE i.udf_char17 END,
CASE WHEN @intUDF_CHAR18 = 1 THEN NULL ELSE i.udf_char18 END,
CASE WHEN @intUDF_CHAR19 = 1 THEN NULL ELSE i.udf_char19 END,
CASE WHEN @intUDF_CHAR2 = 1 THEN NULL ELSE i.udf_char2 END,
CASE WHEN @intUDF_CHAR3 = 1 THEN NULL ELSE i.udf_char3 END,
CASE WHEN @intUDF_CHAR4 = 1 THEN NULL ELSE i.udf_char4 END,
CASE WHEN @intUDF_CHAR5 = 1 THEN NULL ELSE i.udf_char5 END,
CASE WHEN @intUDF_CHAR6 = 1 THEN NULL ELSE i.udf_char6 END,
CASE WHEN @intUDF_CHAR7 = 1 THEN NULL ELSE i.udf_char7 END,
CASE WHEN @intUDF_CHAR8 = 1 THEN NULL ELSE i.udf_char8 END,
CASE WHEN @intUDF_CHAR9 = 1 THEN NULL ELSE i.udf_char9 END,
CASE WHEN @intUDF_DATE1 = 1 THEN NULL ELSE i.udf_date1 END,
CASE WHEN @intUDF_DATE10 = 1 THEN NULL ELSE i.udf_date10 END,
CASE WHEN @intUDF_DATE11 = 1 THEN NULL ELSE i.udf_date11 END,
CASE WHEN @intUDF_DATE12 = 1 THEN NULL ELSE i.udf_date12 END,
CASE WHEN @intUDF_DATE2 = 1 THEN NULL ELSE i.udf_date2 END,
CASE WHEN @intUDF_DATE3 = 1 THEN NULL ELSE i.udf_date3 END,
CASE WHEN @intUDF_DATE4 = 1 THEN NULL ELSE i.udf_date4 END,
CASE WHEN @intUDF_DATE5 = 1 THEN NULL ELSE i.udf_date5 END,
CASE WHEN @intUDF_DATE6 = 1 THEN NULL ELSE i.udf_date6 END,
CASE WHEN @intUDF_DATE7 = 1 THEN NULL ELSE i.udf_date7 END,
CASE WHEN @intUDF_DATE8 = 1 THEN NULL ELSE i.udf_date8 END,
CASE WHEN @intUDF_DATE9 = 1 THEN NULL ELSE i.udf_date9 END,
CASE WHEN @intUDF_FLOAT1 = 1 THEN NULL ELSE i.udf_float1 END,
CASE WHEN @intUDF_FLOAT10 = 1 THEN NULL ELSE i.udf_float10 END,
CASE WHEN @intUDF_FLOAT11 = 1 THEN NULL ELSE i.udf_float11 END,
CASE WHEN @intUDF_FLOAT12 = 1 THEN NULL ELSE i.udf_float12 END,
CASE WHEN @intUDF_FLOAT13 = 1 THEN NULL ELSE i.udf_float13 END,
CASE WHEN @intUDF_FLOAT14 = 1 THEN NULL ELSE i.udf_float14 END,
CASE WHEN @intUDF_FLOAT15 = 1 THEN NULL ELSE i.udf_float15 END,
CASE WHEN @intUDF_FLOAT16 = 1 THEN NULL ELSE i.udf_float16 END,
CASE WHEN @intUDF_FLOAT17 = 1 THEN NULL ELSE i.udf_float17 END,
CASE WHEN @intUDF_FLOAT18 = 1 THEN NULL ELSE i.udf_float18 END,
CASE WHEN @intUDF_FLOAT19 = 1 THEN NULL ELSE i.udf_float19 END,
CASE WHEN @intUDF_FLOAT2 = 1 THEN NULL ELSE i.udf_float2 END,
CASE WHEN @intUDF_FLOAT3 = 1 THEN NULL ELSE i.udf_float3 END,
CASE WHEN @intUDF_FLOAT4 = 1 THEN NULL ELSE i.udf_float4 END,
CASE WHEN @intUDF_FLOAT5 = 1 THEN NULL ELSE i.udf_float5 END,
CASE WHEN @intUDF_FLOAT6 = 1 THEN NULL ELSE i.udf_float6 END,
CASE WHEN @intUDF_FLOAT7 = 1 THEN NULL ELSE i.udf_float7 END,
CASE WHEN @intUDF_FLOAT8 = 1 THEN NULL ELSE i.udf_float8 END,
CASE WHEN @intUDF_FLOAT9 = 1 THEN NULL ELSE i.udf_float9 END,
CASE WHEN @intUSE_SETTLE_DATE_BAL_IND = 1 THEN 'N' ELSE ISNULL( i.use_settle_date_bal_ind, 'N' ) END,
CASE WHEN @intUSR_CLASS_CD_1 = 1 THEN NULL ELSE i.usr_class_cd_1 END,
CASE WHEN @intUSR_CLASS_CD_2 = 1 THEN NULL ELSE i.usr_class_cd_2 END,
CASE WHEN @intUSR_CLASS_CD_3 = 1 THEN NULL ELSE i.usr_class_cd_3 END,
CASE WHEN @intUSR_CLASS_CD_4 = 1 THEN NULL ELSE i.usr_class_cd_4 END,
CASE WHEN @intVIEW_ID = 1 THEN NULL ELSE i.view_id END,
CASE WHEN @intYTD_GL_LONG_TERM = 1 THEN NULL ELSE i.ytd_gl_long_term END,
CASE WHEN @intYTD_GL_SHORT_TERM = 1 THEN NULL ELSE i.ytd_gl_short_term END,        
			  i.acct_cd,  -- PRIMARY KEY
        acct_name,  -- acct_name REQUIRED
        acct_typ_cd,  -- acct_typ_cd REQUIRED
        0, -- compliance tolerance
        ISNULL( i.crrncy_cd, @crrncyCd ),
        CASE WHEN @intMKT_VAL = 1 THEN NULL ELSE mkt_val END,  -- cur_mkt_val: set to mkt_val & uses its flag per Irina
        UPPER (ISNULL (manager, @p_strDefaultManager)),  -- manager REQUIRED
        @p_strExtSys,  -- syst_of_reference
        CASE WHEN @intTHEME_1 = 1 THEN 'SECTYPE' ELSE theme_1 END,  -- theme_1 REQUIRED
        0,             -- update_in_progress
        0,              -- update_seqno
        1,               -- record_version
        tmp.acct_id_offset + @firstSeqNum,     -- acct_id
		@p_importUserId,  -- last_upd_user
		GETDATE(),        -- last_upd_date
		GETDATE()         -- create_date
    FROM    CS_IMP_FUND i, #T134_CS_IMP_NEW_ACCTS tmp
    WHERE   i.acct_cd = tmp.acct_cd

    SELECT  @intError = @@error , @intInsertCount = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Insert new accounts'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Insert new accounts'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intInsertCount, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

		-- Asign some default values
		IF( @intDEF_MDL_TYPE = 0 OR @intDEFAULT_MODEL_CD = 0 OR @intDEF_MDL_ID = 0 ) BEGIN
				UPDATE CS_FUND 
				SET DEF_MDL_TYPE = 'A'
				WHERE DEF_MDL_ID IS NOT NULL 
               AND ( DEF_MDL_TYPE IS NULL OR DEF_MDL_TYPE != 'A' )
					AND EXISTS( SELECT 1 FROM CS_IMP_FUND i WHERE i.acct_cd = acct_cd )
				SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_MDL_TYPE'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_MDL_TYPE'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

				UPDATE CS_FUND 
				SET DEF_MDL_TYPE = ( SELECT acct_typ_cd FROM CS_FUND f1 WHERE f1.ACCT_CD = f.DEFAULT_MODEL_CD )
				FROM CS_FUND f
				WHERE DEF_MDL_ID IS NULL
					AND DEFAULT_MODEL_CD IS NOT NULL
					AND ( DEF_MDL_TYPE != ( SELECT acct_typ_cd FROM CS_FUND f1 WHERE f1.ACCT_CD = f.DEFAULT_MODEL_CD )
               OR DEF_MDL_TYPE IS NULL )
					AND EXISTS( SELECT 1 FROM CS_IMP_FUND i WHERE i.acct_cd = acct_cd )
				SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_MDL_TYPE'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_MDL_TYPE'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END
		END

		IF( @intDEF_BENCH_TYPE = 0 OR @intDEF_BENCH_CD = 0 OR @intDEF_BENCH_ID = 0 ) BEGIN
			UPDATE CS_FUND 
			SET DEF_BENCH_TYPE = 'A'
			WHERE DEF_BENCH_ID IS NOT NULL
            AND ( DEF_BENCH_TYPE IS NULL OR DEF_BENCH_TYPE != 'A' )
				AND EXISTS( SELECT 1 FROM CS_IMP_FUND i WHERE i.acct_cd = acct_cd )
			SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_BENCH_TYPE'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_BENCH_TYPE'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

			UPDATE CS_FUND 
			SET DEF_BENCH_TYPE = ( SELECT acct_typ_cd FROM CS_FUND f1 WHERE f1.ACCT_CD = f.DEF_BENCH_CD )
			FROM CS_FUND f
			WHERE DEF_BENCH_ID IS NULL
				AND DEF_BENCH_CD IS NOT NULL
				AND ( DEF_BENCH_TYPE != ( SELECT acct_typ_cd FROM CS_FUND f1 WHERE f1.ACCT_CD = f.DEF_BENCH_CD )
            OR DEF_BENCH_TYPE IS NULL )
				AND EXISTS( SELECT 1 FROM CS_IMP_FUND i WHERE i.acct_cd = acct_cd )
			SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_BENCH_TYPE'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_BENCH_TYPE'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END
		END

		IF( @intDEF_HEDGE_MDL_TYPE = 0 OR @intDEF_HEDGE_MDL_CD = 0 OR @intDEF_HEDGE_MDL_ID = 0 ) BEGIN
			UPDATE CS_FUND 
			SET DEF_HEDGE_MDL_TYPE = 'A'
			WHERE DEF_HEDGE_MDL_ID IS NOT NULL
            AND ( DEF_HEDGE_MDL_TYPE IS NULL OR DEF_HEDGE_MDL_TYPE != 'A' )
				AND EXISTS( SELECT 1 FROM CS_IMP_FUND i WHERE i.acct_cd = acct_cd )
			SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_HEDGE_MDL_TYPE'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_HEDGE_MDL_TYPE'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

			UPDATE CS_FUND 
			SET DEF_HEDGE_MDL_TYPE = ( SELECT acct_typ_cd FROM CS_FUND f1 WHERE f1.ACCT_CD = f.DEF_HEDGE_MDL_CD )
			FROM CS_FUND f
			WHERE DEF_HEDGE_MDL_ID IS NULL
				AND DEF_HEDGE_MDL_CD IS NOT NULL
				AND  ( DEF_HEDGE_MDL_TYPE != ( SELECT acct_typ_cd FROM CS_FUND f1 WHERE f1.ACCT_CD = f.DEF_HEDGE_MDL_CD )
            OR DEF_HEDGE_MDL_TYPE IS NULL )
				AND EXISTS( SELECT 1 FROM CS_IMP_FUND i WHERE i.acct_cd = acct_cd )
			SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_HEDGE_MDL_TYPE'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_HEDGE_MDL_TYPE'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END
		END

		IF( @intDEF_HEDGE_BENCH_TYPE = 0 OR @intDEF_HEDGE_BENCH_CD = 0 OR @intDEF_HEDGE_BENCH_ID = 0 ) BEGIN
			UPDATE CS_FUND 
			SET DEF_HEDGE_BENCH_TYPE = 'A'
			WHERE DEF_HEDGE_BENCH_ID IS NOT NULL
            AND ( DEF_HEDGE_BENCH_TYPE IS NULL OR DEF_HEDGE_BENCH_TYPE != 'A' )
				AND EXISTS( SELECT 1 FROM CS_IMP_FUND i WHERE i.acct_cd = acct_cd )
			SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_HEDGE_BENCH_TYPE'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_HEDGE_BENCH_TYPE'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

			UPDATE CS_FUND 
			SET DEF_HEDGE_BENCH_TYPE = ( SELECT acct_typ_cd FROM CS_FUND f1 WHERE f1.ACCT_CD = f.DEF_HEDGE_BENCH_CD )
			FROM CS_FUND f
			WHERE DEF_HEDGE_BENCH_ID IS NULL
				AND DEF_HEDGE_BENCH_CD IS NOT NULL
				AND  ( DEF_HEDGE_BENCH_TYPE != ( SELECT acct_typ_cd FROM CS_FUND f1 WHERE f1.ACCT_CD = f.DEF_HEDGE_BENCH_CD )
            OR DEF_HEDGE_BENCH_TYPE IS NULL )
				AND EXISTS( SELECT 1 FROM CS_IMP_FUND i WHERE i.acct_cd = acct_cd )
			SELECT  @intError = @@error , @intRows = @@rowcount
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_HEDGE_BENCH_TYPE'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'Update CS_FUND.DEF_HEDGE_BENCH_TYPE'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intRows, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END
		END

    -- report successful import (needs to be in the transaction)
    EXEC @intError = crdImp_logCompletion
                @p_strExtSys, @p_intRunID, @p_importUserId, @dtmRunTime, @strMAIN_TABLE, @strIMP_CD,
                @intFinalImpCount, @intInitImpCount,
                -1, @intUpdateCount, @intInsertCount,  -- deletes/updates/inserts
                @strLastOp = @strLastOp OUTPUT
    IF @intError != 0  GOTO Error_Handler
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'after return from crdImp_logCompletion'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      @intFinalImpCount, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

COMMIT TRANSACTION
SELECT  @intError = @@error 
IF @intError != 0 BEGIN
   SELECT @strLastOp = 'final COMMIT TRANSACTION'
   GOTO Error_Handler
END
IF @p_strDumpTiming = 'Y' BEGIN
   SELECT @strLastOp = 'final COMMIT TRANSACTION'
   EXEC @intError = crdImp_logDeltaTime @p_strExtSys, @p_intRunID, @strIMP_CD,
      NULL, @informalTableName, @timeValue = @timeValue OUTPUT,
      @strLastOp = @strLastOp OUTPUT
   IF @intError != 0  GOTO Error_Handler
END

-- log performance
EXEC @intError = crdImp_perfLog
						@p_strExtSys, @p_intRunID, @p_importUserId, @dtmRunTime, @strMAIN_TABLE, @strIMP_CD,
						@intFinalImpCount, @intInitImpCount,
						-1, @intUpdateCount, @intInsertCount,  -- deletes/updates/inserts
						@strLastOp = @strLastOp OUTPUT
IF @intError != 0  GOTO Error_Handler

RETURN 0

Error_Handler:

-- Rollback any transactions
WHILE (@@trancount > 0)
BEGIN
    ROLLBACK TRANSACTION
END
    EXEC @intError = crdImp_exceptionMsg @p_strExtSys, @p_intRunID, @intError, 'cs_imp_fund_update', @strLastOp

RETURN 2
