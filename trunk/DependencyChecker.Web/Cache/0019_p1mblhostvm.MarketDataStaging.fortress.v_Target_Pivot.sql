
create view [fortress].[v_Target_Pivot] as
	select 
		0 as None,
		1 as Security,
		2 as [Order],
		3 as Issuer,
		4 as Position,
		5 as Manager,
		6 as Fund,
		7 as Broker,
		8 as Exchange,
		9 as Tag,
		10 as BrokerBranch,
		11 as Country


