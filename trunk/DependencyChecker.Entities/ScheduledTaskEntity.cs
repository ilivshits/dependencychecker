﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using DependencyChecker.Common;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Entities
{
    public class ScheduledTaskEntity : EntityBase
    {
        public static readonly string ProviderName = EntityProviderType.ScheduledTask.ToString();

        public override string InternalId { get; set; }

        public bool IsEnabled { get; set; }
        public string Principal { get; set; }
        public DateTime TaskLastRunTime { get; set; }

        public override string ScopeDelimiter
        {
            get
            {
                return "/";
            }
        }

        public override List<string> SearchNames
        {
            get
            {
                var result = new List<String>(){ Name };
                return result;
            }
        }

        private static readonly Regex ExecContent = new Regex(@"([a-z0-9\.""'\\\/= \t\(\)\[\]_\-\+;\?@\<\>`,\{\}\$:\r\n]{15,})", 
            RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);
        private static readonly Regex BcpCall = new Regex(@"bcp(\.exe)?[ \t]+""(?<command>[^""""]+)"".*\-S(%(?<serverVar>[a-z_0-9]+)%|(?<server>([a-z_0-9\-]+|""[a-z_0-9\-]+"")))",
            RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);

        private const string VarDefinitionFormat = @"SET[ \t]+{0}[ \t]*=[ \t]*(?<value>([a-z_0-9\-]+|""[a-z_0-9\-]+""))";

        protected override List<SearchContent> GetSearchContent()
        {
            var result = new List<SearchContent>();
            if (String.IsNullOrEmpty(Definition) || Type == EntityType.Unresolved)
            {
                LoggerHelper.Log.DebugFormat("Scheduled task entity '{0}' ({1}) is not going to be parsed", FullName, TypeName);
                return result;
            }

            if (Type == EntityType.Executable)
            {
                return new List<SearchContent>
                {
                    new SearchContent
                    {
                        ContentType = EntityProviderType.SQL,
                        Text = String.Join(" \r\n ", ExecContent.Matches(Definition).Cast<Match>().Select(m => m.Value).ToArray())
                    }
                };
            }
            if (Type == EntityType.BatchFile)
            {
                var cleanDefinition = Regex.Replace(Definition, "rem.*", String.Empty, RegexOptions.IgnoreCase);
                return BcpCall.Matches(cleanDefinition).Cast<Match>().Select(m => new SearchContent
                {
                    ContentType = EntityProviderType.SQL,
                    Text = m.Groups["command"].Value,
                    ScopeName = m.Groups["server"].Success ? m.Groups["server"].Value : GetServerFromBatch(m.Groups["serverVar"].Value, cleanDefinition.Substring(0, m.Groups["serverVar"].Index))
                }).ToList();
            }

            try
            {
                var definitionDoc = XDocument.Parse(Definition);
                var ns =
                    Regex.Match(Definition, @"<Task( version=""(?<ns>[^""]+)"")? xmlns=""(?<ns>[^""]+)"">").Groups["ns"]
                        .Value;
                if (definitionDoc.Root != null)
                {
                    ns = definitionDoc.Root.Name.Namespace.NamespaceName;
                }

                result.AddRange(new List<SearchContent>
                {
                    new SearchContent
                    {
                        ContentType = EntityProviderType.Person,
                        Text = definitionDoc.Descendants(ns + "Author").First().Value
                    },
                    new SearchContent
                    {
                        ContentType = EntityProviderType.Person,
                        Text = Principal
                    }
                });

                result.AddRange(new List<SearchContent>
                {
                    new SearchContent
                    {
                        ContentType = EntityProviderType.Batch,
                        Text = definitionDoc.Descendants(ns + "Command").First(c => c.Value.Contains(".bat")).Value
                    },
                    new SearchContent
                    {
                        ContentType = EntityProviderType.Batch,
                        Text = definitionDoc.Descendants(ns + "Command").First(c => c.Value.Contains(".pl")).Value
                    }
                });
            }
            catch (Exception ex)
            {
                LoggerHelper.Log.WarnFormat("Cannot parse XML content for entity '{0}' ({1}). Error: {3}. XML:\r\n{2}", FullName, TypeName, Definition, ex.Message);
            }
            return result;
        }

        private static string GetServerFromBatch(string serverVar, string definition)
        {
            var match = Regex.Matches(definition, String.Format(VarDefinitionFormat, serverVar.Replace("-", "\\-")), RegexOptions.IgnoreCase).Cast<Match>().Last();
            return match.Success ? match.Groups["value"].Value : String.Empty;
        }

        public ScheduledTaskEntity(string name, EntityType type)
            : this(name, ProviderName, type)
        {
        }

        public ScheduledTaskEntity(string name, string definition, EntityType type)
            : this(name, definition, type, ProviderName)
        {

        }

        protected ScheduledTaskEntity(string name, string definition, EntityType type, string providerName)
            : base(name, definition, providerName, type)
        {
            ScopeChildren = new List<IEntity>();
        }
    }
}