﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace DependencyChecker.Client
{
    class Program
    {
        private static string _outputDir;
        private static string _serviceHost;

        static void Main(string[] args)
        {
            LoadParameters();
            ReadAndProcessInput();
        }

        private static void LoadParameters()
        {
            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["outputDir"]))
            {
                Console.WriteLine("Please enter output directory name:");
                _outputDir = Console.ReadLine();
                if (_outputDir != null && !Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _outputDir)))
                {
                    Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _outputDir));
                }
            }

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["serviceHost"]))
            {
                Console.WriteLine("Please enter dependency checker service host name:");
                _serviceHost = Console.ReadLine();
            }

        }

        private static void ReadAndProcessInput()
        {
            if (!Directory.Exists("output"))
            {
                Directory.CreateDirectory("output");
            }
        }

    }
}
