
CREATE FUNCTION [dbo].[IsWeekDay]
(
	@date datetime
)
RETURNS bit
AS
BEGIN
	DECLARE @weekday varchar(10);
	SET @weekday = DATENAME(dw, @date);
	IF (@weekday = 'Saturday' OR @weekday = 'Sunday') RETURN 0;
	RETURN 1;
END


