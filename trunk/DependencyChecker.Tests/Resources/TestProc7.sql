﻿-- ===============================================================================================
-- Author:		fortress\mcvetanovic,Milan Cvetanovic
-- Create date: <12/03/2013>
-- Description:	MPS to P1TC migration of ftb_env.dbo.GetPrecMetals that returns required data for today live precious metal securities in P1TC all Metal Forwards (securityPrimaryTypeId = 34) for XAU,XAG,XPT,XPD
-- Used by:	/Imagine/Analytics/batches/VolUploader/Voluploader/Program.cs
-- Dependencies: P1ReportingG2.P1TC.spPositions
-- ===============================================================================================
CREATE PROC [MiPS].[spGetPrecMetals]
AS
BEGIN
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	-- get today live P1TC secuirites mapping sid - tid - underlying_sid and putCallFlag, strike for Metal Option (securityPrimaryTypeId = 35) using P1ReportingG2.P1TC.spPositions and [FTBTrade].[dbo].[Option]
	-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	CREATE TABLE #sidTidUsidType (
		[sid] BIGINT
		,[tid] INT
		,[underlying_sid] BIGINT
		,[putCallFlag] CHAR(1)
		,[strike] DECIMAL(19,8)
		);
		
	DECLARE @q NVARCHAR(4000);
	
	SET @q = 'SELECT DISTINCT p.[sid], p.tid, p.underlyingSid, op.putCallFlag, op.strike FROM OPENQUERY (SELF, 
				''SET FMTONLY OFF EXEC P1ReportingG2.[P1TC].[spPositions]'') AS p
			  JOIN P1G2.[FTBTrade].[dbo].[Option] op ON p.[sid] = op.[sid]
			  WHERE p.sptId = 35';			
			
	--PRINT @q;
	
	INSERT INTO #sidTidUsidType
	EXEC sp_executesql @q;
		
	------------------------------------------------------------
	-- get only today live COt and FXBI security types from Mips
	------------------------------------------------------------
	SELECT Trader
		,[Type]
		,SM_ID
		,Usym
		,Expiry
		,Value10
	INTO #r1
	FROM [Mips].[dbo].[z_mips_reporting1]
	WHERE [Type] IN (
			'COt'
			,'FXBI'
			);
			
	--------------------------------------------------------------------------------------------------------------------------	
	-- get all P1TC Metal Forwards (securityPrimaryTypeId = 34) for precious metals replacement for MPS3.Imagine.dbo.prec_metal
	--------------------------------------------------------------------------------------------------------------------------
	CREATE TABLE #precMetalFwds (
		sid BIGINT
		);
		
	SET @q = 'SELECT * FROM OPENQUERY (P1G2, 
				''SELECT sid
				FROM [FTBTrade].[dbo].[Security]
				WHERE securityPrimaryTypeId = 34 
					AND ticker IN (
						''''XAU''''
						,''''XAG''''
						,''''XPT''''
						,''''XPD''''
						)
				'')';			
			
	--PRINT @q;
	
	INSERT INTO #precMetalFwds
	EXEC sp_executesql @q;
	
	-------------------------------------
	-- return data for precious metals
	-------------------------------------
	SELECT DISTINCT r1.[SM_ID]
		,r1.[Usym]
		,r1.[Expiry]
		,tid = r1.[Value10]
		,r1.[Type]
		,put_call_flag = stut.[putCallFlag]	
		,strike_price = stut.[strike]		
	FROM #r1 r1
	JOIN #sidTidUsidType stut ON stut.[tid] = r1.[Value10]
	JOIN #precMetalFwds pmf ON pmf.[sid] = stut.[underlying_sid] -- get only underlying Metal Forwards for Metal Options under precious metals
	ORDER BY r1.[Type];

	DROP TABLE #sidTidUsidType;
	
	DROP TABLE #precMetalFwds;

	DROP TABLE #r1;
	
END
