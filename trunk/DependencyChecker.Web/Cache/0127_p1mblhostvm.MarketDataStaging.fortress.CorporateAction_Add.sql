

CREATE PROCEDURE [fortress].[CorporateAction_Add]
	@corporateActionId	bigint,
	@sid bigint,
	@issuerId int,
	@actionId int,
	@corporateActionTypeId int,
	@announcementDate datetime,
	@effectiveDate datetime,
	@notes varchar(max),

	--- Auditing.
	@application_source varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--- Insert the record.
	insert into FTBTrade.dbo_CorporateAction (
		corporateActionId, sid, issuerId, actionId, corporateActionTypeId, announcementDate, effectiveDate, notes	
	) values (
		@corporateActionId,
		@sid,
		@issuerId,
		@actionId,
		@corporateActionTypeId,
		@announcementDate,
		@effectiveDate,
		@notes	
	)
	
	
	
	
END



