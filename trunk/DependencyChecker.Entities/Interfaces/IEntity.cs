﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DependencyChecker.Entities.Base;

namespace DependencyChecker.Entities.Interfaces
{
    public interface IEntity
    {
        String Name { get; }
        String Definition { get; }

        IEntity Root { get; }

        int Level { get; }
        List<IEntity> Parents { get; }

        IEntity Scope { get; set; }
        List<IEntity> ScopeChildren { get; set; }

        List<IEntity> UsedBy { get; set; }
        List<IEntity> DependsOn { get; set; }

        List<IEntity> UsedByRecursive { get; }
        List<IEntity> DependsOnRecursive { get; }

        IEntity CreatedBy { get; set; }
        IEntity LastModifiedBy { get; set; }

        DateTime Created { get; set; }
        DateTime LastModified { get; set; }

        bool IsAnalyzed { get; set; }
        bool HasUnresolved { get; set; }

        String[] GetErrors();
        void AddError(EntityErrorCode code, String error);
        void AddErrorFormat(EntityErrorCode code, String error, params object[] args);
        void AddErrorFormat(String error, params object[] args);

        List<SearchContent> SearchContent { get; }

        Guid Id { get; }
        String DataProvider { get; }
        String TypeName { get; }
        EntityType Type { get; }
        bool IsAccessible { get; set; }

        List<EntityAlias> Aliases { get; }
        String InternalId { get; set; }
        string FullName { get; }
        List<string> SearchNames { get; }

        bool AddDependency(IEntity dependency, EntityMatch match = null);
        bool AddDependency(IEntity dependency, string matchText);

        int Priority { get; }
        String ScopeDelimiter { get; }

        IEntity Find(string path, StringComparison comparison = StringComparison.InvariantCultureIgnoreCase);
        IEntity FindByInternalId(string identifier, StringComparison comparison = StringComparison.CurrentCulture);
        IEntity FindById(Guid id);
    }
}