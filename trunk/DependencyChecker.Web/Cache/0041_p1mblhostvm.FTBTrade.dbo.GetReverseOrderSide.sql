create function [dbo].[GetReverseOrderSide](@orderSideId int) returns int as
begin
	declare @reverse int
	select @reverse = case @orderSideId
		when [Buy] then [Sell]
		when [Sell] then [Buy]
		when [Cover] then [Short]
		when [Short] then [Cover]
		when [Buy Protection] then [Sell Protection]
		when [Sell Protection] then [Buy Protection]
		when [Repo] then [Reverse Repo]
		when [Reverse Repo] then [Repo]
		when [Pay] then [Receive]
		when [Receive] then [Pay]
		else 0 end
	from OrderSide_Transposed
	return @reverse
end


