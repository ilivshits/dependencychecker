﻿CREATE TABLE [log].[ErrorsArchive]
(
	[Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
    [batchId] INT NOT NULL CONSTRAINT [FK_Error_BatchId] FOREIGN KEY REFERENCES [log].[Batch] ([batchId]), 
    [entityName] NVARCHAR(MAX) NOT NULL, 
    [errorText] NVARCHAR(MAX) NULL, 
    [entityType] NVARCHAR(MAX) NULL
)
