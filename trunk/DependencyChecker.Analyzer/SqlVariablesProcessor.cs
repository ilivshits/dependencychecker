﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DependencyChecker.Entities;

namespace DependencyChecker.Analyzer
{
    internal class SqlVariablesProcessor
    {
        //private readonly Regex _assignmentExpression =
        //    new Regex(@"(SET[ \t\r\n]+(?<var>@[a-z0-9]+)|DECLARE[ \t\r\n]+(?<var>@[a-z0-9]+)[ \t\r\n]+N?VARCHAR[ \t\r\n]*\([0-9]+\))[ \t\r\n]*" +
        //                @"=(?<expr>[ \t\r\n]*(N?'[^']+'[ \t\r\n]*\+[ \t\r\n]*((CONVERT[ ]*\(varchar,[ ]*(?<var2>@[a-z0-9]+)(,[ ]*[0-9]+\)|\))|(?<var2>@[a-z0-9]+))[ \t\r\n]*\+[ \t\r\n]*)?)*N?(?<value>'[^']+'))", 
        //        RegexOptions.IgnoreCase | RegexOptions.Singleline);
        private readonly Regex _assignmentExpression =
            new Regex(@"(SET[ \t\r\n]+(?<var>@[a-z0-9]+)|DECLARE[ \t\r\n]+(?<var>@[a-z0-9]+)[ \t\r\n]+N?VARCHAR[ \t\r\n]*\([0-9]+\))[ \t\r\n]*" +
                        @"=[ \t\r\n]*N?(?<value>'[^']+')[ \t\r\n]*$", 
                RegexOptions.IgnoreCase | RegexOptions.Multiline);

        private void ProcessVariables(SearchContent content)
        {
            var matches = _assignmentExpression.Matches(content.Text);
            var definitions = matches.Cast<Match>().Select(m => new EntityMatch
            {
                MatchText = m.Value,
                Indexes = new List<int> {m.Index}
            }).ToList();
            var consts = matches.Cast<Match>().ToDictionary(m => m.Groups["var"].Value, m => m.Groups["value"].Value);

            definitions.ForEach(d => content.ReplaceMatch(d, String.Empty, null));

            foreach (var c in consts)
            {
                content.Text = content.Text.Replace(c.Key, c.Value);
            }
            //while (matches.Count > 0)
            //{
            //    for (var i = 0; i < matches.Count; i++)
            //    {
            //        var match = matches[i];

            //        var varName = match.Groups["var"].Value;

            //        // If we have a simple assignment. 
            //        // Example: DECLARE @a VARCHAR(20) = 'value'
            //        if (!match.Groups["var2"].Success)
            //        {
            //            if (!consts.ContainsKey(varName))
            //            {
            //                consts.Add(varName, match.Groups["expr"].Value);
            //            }
            //            else
            //            {
            //                consts[varName] = match.Groups["expr"].Value;
            //            }
            //        }
            //        // If we have a complex assignment which includes several variables.
            //        // Example: SET @a  = 'EXC ' + @db + '.spName'
            //        else if (match.Groups["var2"].Success)
            //        {
            //            // Special case when we add some text to the same variable
            //            // Example: SET @a  = 'SELECT * FROM OPENQUERY(' + @a + ')'
            //            if (match.Groups["var2"].Captures.Count == 1 && match.Groups["var2"].Value == varName)
            //            {

            //            }
            //            else
            //            {
            //                foreach (Capture capture in match.Groups["var2"].Captures)
            //                {
            //                    if (consts.ContainsKey(capture.Value))
            //                    {
                                    
            //                    }
            //                }
            //            }
            //        }
            //    }
        }
    }
}
