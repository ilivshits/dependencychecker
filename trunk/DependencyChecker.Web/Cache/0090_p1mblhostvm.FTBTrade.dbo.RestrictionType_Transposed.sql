CREATE VIEW [dbo].[RestrictionType_Transposed] AS
SELECT
[FIG RTL] = 1
, [MLP Restriction] = 2
, [Short Sale Ban] = 3
, [144a] = 4
, [REGS] = 5
, [3c7] = 6
, [Regulatory Threshold Restriction] = 7
, [Temporary Short Selling Restriction-Secondary Participation] = 8
, [Short Selling Ban-Italy] = 9
, [Short Selling Ban-Spain] = 10
, [Short Selling Ban-Greece] = 11
, [Short Selling Ban-Korea] = 12
, [GSMA RTL restrictions] = 13


