﻿
CREATE PROCEDURE [P1TC].[spGetPermissions]
AS
  BEGIN
      PRINT '0: Start '+CONVERT(VARCHAR(MAX), getdate(), 114)

      SELECT *
      FROM   openquery(P1G2, 'WITH permission
									 AS (SELECT u.userId,
												objectId
										 FROM   FTBTrade.dbo.[User] u
												CROSS apply FTBTrade.dbo.GetUserPermissions(u.userId) p
										 WHERE  userId!=0 and userId <1000
												AND targetId=5), -- manager
									 all_manager
									 AS (SELECT DISTINCT userId
										 FROM   FTBTrade.dbo.UserRole
										 WHERE  roleId=2),
									 specific
									 AS (SELECT DISTINCT p.userId
										 FROM   permission p
												LEFT JOIN all_manager m
												  ON p.userId=m.userId
										 WHERE  m.userId IS NULL)
								SELECT *
								FROM   (SELECT Login = lower(u.login),
											   Name = u.fullName,
											   Manager = m.description
										FROM   specific s
											   JOIN permission p
												 ON s.userId=p.userId
											   JOIN FTBTrade.dbo.Manager m
												 ON p.objectId=m.managerId
											   JOIN FTBTrade.dbo.[User] u
												 ON s.userId=u.userId
													AND u.enabledFlag=1
										UNION ALL
										SELECT Login = lower(u.login),
											   Name = u.fullName,
											   Manager = ''ALL TRADERS''
										FROM   all_manager s
											   JOIN FTBTrade.dbo.[User] u
												 ON s.userId=u.userId
													AND u.enabledFlag=1) t
								ORDER  BY Login 
							')

      PRINT '1: Finish P1G2 Query '+CONVERT(VARCHAR(MAX), getdate(), 114)
  END




