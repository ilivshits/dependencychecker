﻿using System.Collections.Generic;
using System.Linq;
using DependencyChecker.Analyzer.Interfaces;

namespace DependencyChecker.Analyzer
{
    public class ResolverFactory : IResolverFactory
    {
        private readonly List<IResolver> _resolvers = new List<IResolver>();

        public ResolverFactory()
        {
            _resolvers.Add(new SQLResolver());
            _resolvers.Add(new ReportsResolver());
        }

        #region Implementation of IResolverFactory

        public IResolver GetResolver(string providerName)
        {
            return _resolvers.Single(r => r.ProviderName == providerName);
        }

        #endregion
    }
}