
create view [dbo].[v_TaskEntity_Pivot] as
	select
		 1 as MarkitStaging,
		 2 as BloombergBackOfficeStaging,
		 5 as BloombergBackOfficeCleanup,
		 6 as IsoStaging,
		 8 as MarkIndexStaging,
		 9 as MarketDataUpdateRateSourceMarks,
		10 as SecurityMasterUpdateIssuers,
		11 as SecurityMasterUpdateExchanges,
		12 as SecurityMasterUpdateEquities,
		13 as SecurityMasterUpdateCommodities,
		14 as SecurityMasterUpdateCreditInstruments,
		15 as SecurityMasterUpdateEquityOptions,
		16 as SecurityMasterUpdateCommodityOptions,
		22 as BloombergSapiStaging,
		23 as SecurityMasterUpdateCredit


