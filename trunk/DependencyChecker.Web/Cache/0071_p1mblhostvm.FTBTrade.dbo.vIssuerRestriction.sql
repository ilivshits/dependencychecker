
CREATE view [dbo].[vIssuerRestriction] as
select issuerId = h.childId, r.issuerRestrictionId, r.restrictionTypeId, r.startDate, r.endDate
from IssuerRestriction r cross apply dbo.GetIssuerWithDescendants(r.issuerId) h
where (r.issuerId = h.childId or r.includeSubsidiariesFlag = 1)


