﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using DependencyChecker.Collector.Base;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Common;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Events;
using DependencyChecker.Entities.Interfaces;
using Microsoft.AnalysisServices;

namespace DependencyChecker.Collector
{
    public class OlapDataProvider : BaseDataProvider
    {

        private static readonly List<Regex> BasicPattern =
           new List<Regex> {new Regex(@"(\[Measures\]\.(?<name>\[([^\[\}]*)\])|" +
                      @"(?<name>(\[([^\[\}]*)\]\.)+(?<last>\[([^\[\}]*)\]))\.\k<last>\.Members|" +
                      @"(?<name>(\[([^\[\}]*)\]\.)+(?<last>\[([^\[\}]*)\]))\.\k<last>|" +
                      @"(?<name>(\[([^\[\}]*)\]\.)+\[([^\[\}]*)\])|" +
                      @"from[ \r\n\t]+(?<name>\[([^\[\}]*)\]))",
                RegexOptions.Singleline | RegexOptions.IgnoreCase) };
        private static readonly Regex DefinitionPattern =
            new Regex(@"CREATE[ \r\n\t]+MEMBER[ \r\n\t]+(CURRENTCUBE\.)?(?<name>(\[([^\[\}]*)\]\.)+\[([^\[\}]*)\])[ \r\n\t]+AS(?<definition>[^;]+);",
                RegexOptions.Singleline | RegexOptions.IgnoreCase);

        #region Implementation of IDataProvider

        public OlapDataProvider(IServerProvider serverProvider)
            : base(serverProvider)
        {
        }

        public override string Name
        {
            get { return OlapEntity.ProviderName; }
        }

        protected override List<Regex> ItemSearchPattern
        {
            get { return BasicPattern; }
        }

        public override List<IEntity> Search()
        {
            LoggerHelper.Log.InfoFormat("Starting search olap objects...");
            var servers = ServerProvider.GetServers(EntityProviderType.OLAP);
            var ret = new List<IEntity>();
            foreach (var server in servers)
            {
                try
                {
                    LoggerHelper.Log.Info("ServerName: " + server);
                    var strConn = "Provider=msolap; Data Source =" + server + ";";

                    var amoconn = new Server();
                    amoconn.Connect(strConn);

                    var srvEntity = new OlapEntity(server, EntityType.Server);
                    srvEntity.InternalId = server;
                    LoadDatabases(srvEntity, amoconn);
                    ret.Add(srvEntity);
                }
                catch (Exception e)
                {
                    LoggerHelper.Log.ErrorFormat("Connecting to OLAP {0} failed!. Exception: {1}", server, e);
                }
            }
            GC.Collect();
            return ret;
        }

        private void LoadDatabases(IEntity server, Server olapServer)
        {
            var dbEntities = new List<IEntity>();
            float dbWight = 1.0f / olapServer.Databases.Count;
            float totalProgress = 0;


            foreach (Database database in olapServer.Databases)
            {
                LoggerHelper.Log.Info("DBName: " + database.Name);
                var dbEntity = new OlapEntity(database.Name, EntityType.Database)
                {
                    InternalId = server.Name + ":" + database.Name,
                    Scope = server
                };
                dbEntities.Add(dbEntity);
                LoadCubes(dbEntity, database);
                totalProgress += dbWight;
                OnProgress(new DependencyCheckerProgressEventArgs
                {
                    Message = "Loaded database " + database.Name,
                    Progress = totalProgress
                });
            }
            server.ScopeChildren = dbEntities;
        }

        private static void LoadCubes(IEntity dbEntity, Database database)
        {
            var cubeEntities = new List<IEntity>();
            foreach (Cube cube in database.Cubes)
            {
                LoggerHelper.Log.Info("CUBE Name: " + cube.Name);
                var cubeEntity = new OlapEntity(cube.Name, EntityType.Cube) { InternalId = cube.ID, Scope = dbEntity };
                cubeEntities.Add(cubeEntity);
                LoadDimensions(cubeEntity, cube);
            }
            dbEntity.ScopeChildren = cubeEntities;
        }

        private static void LoadDimensions(IEntity cubeEntity, Cube cube)
        {
            //Loading all measures
            cubeEntity.ScopeChildren = (from Measure measure in cube.AllMeasures
                                        select new OlapEntity(measure.Name, EntityType.Measure)
                                        {

                                            InternalId = measure.ID,
                                            Scope = cubeEntity
                                        }).Cast<IEntity>().ToList();
            LoggerHelper.Log.InfoFormat("All measures: \r\n\t{0}",
                String.Join(",\r\n\t",
                    cubeEntity.ScopeChildren.Select(l => l.Name).ToArray()));

            //Loading perspectives
            var perspectiveEntities = new List<IEntity>();
            foreach (Perspective perspective in cube.Perspectives)
            {
                LoggerHelper.Log.Info("Perspective Name: " + perspective.Name);
                perspectiveEntities.Add(new OlapEntity(perspective.Name, EntityType.Perspective)
                {
                    InternalId = perspective.ID,
                    Scope = cubeEntity
                });
            }
            cubeEntity.ScopeChildren.AddRange(perspectiveEntities);

            //Parsing calculated members from mdxScripts commands
            var calculatedMeasuresDimension = new OlapEntity("Measures", EntityType.Dimension) //Syntetic dimension
            {
                Scope = cubeEntity
            };

            var calculatedMembers = new List<IEntity>();
            foreach (var definitions in
                    cube.MdxScripts
                            .Cast<MdxScript>()
                            .SelectMany(s => s.Commands.Cast<Command>())
                            .Select(command => DefinitionPattern.Matches(command.Text)))
            {
                var trimChars = new[] {'[', ']'};
                calculatedMembers.AddRange((from Match definition in definitions
                                            where definition.Groups["name"].Success && definition.Groups["definition"].Success
                                            select new OlapEntity(
                                                        definition.Groups["name"].Value
                                                            .Replace(String.Format("[{0}].", cubeEntity.Name), String.Empty)
                                                            .Replace(String.Format("[{0}].", calculatedMeasuresDimension.Name), String.Empty)
                                                            .Replace(String.Format("[{0}].", calculatedMeasuresDimension.Name.ToUpper()), String.Empty)
                                                            .Trim(trimChars), 
                                                        definition.Groups["definition"].Value, 
                                                        EntityType.CalculatedMember)
                                            {
                                                Scope = calculatedMeasuresDimension
                                            }));
            }
            calculatedMeasuresDimension.ScopeChildren = calculatedMembers;
            cubeEntity.ScopeChildren.Add(calculatedMeasuresDimension);

            var dimensionEntities = new List<IEntity>();
            foreach (CubeDimension dimension in cube.Dimensions)
            {
                LoggerHelper.Log.Info("Dimension Name: " + dimension.Name);
                var dimensionEntity = new OlapEntity(dimension.Name, EntityType.Dimension)
                {
                    InternalId = dimension.ID,
                    Scope = cubeEntity
                };

                //LList Hierarchies
                var hierarchiesEntities = new List<IEntity>();
                foreach (CubeHierarchy hierarchy in dimension.Hierarchies)
                {
                    LoggerHelper.Log.Info("\tHierarchy Name: " + dimension.Name);
                    var hierarchyEntity = new OlapEntity(hierarchy.Hierarchy.Name, EntityType.Hierarchy)
                    {
                        InternalId = hierarchy.HierarchyID,
                        Scope = dimensionEntity
                    };

                    //List Levels
                    hierarchyEntity.ScopeChildren = (from Level level in hierarchy.Hierarchy.Levels
                                                     select new OlapEntity(level.Name, EntityType.Level)
                                                     {
                                                         InternalId = level.ID,
                                                         Scope = hierarchyEntity
                                                     }).Cast<IEntity>().ToList();
                    LoggerHelper.Log.InfoFormat("\t\tLevels: \r\n\t\t\t{0}",
                        String.Join(",\r\n\t\t\t",
                            hierarchyEntity.ScopeChildren.Select(l => l.Name).ToArray()));

                    hierarchiesEntities.Add(hierarchyEntity);
                }
                dimensionEntity.ScopeChildren = hierarchiesEntities;

                //List Attribures
                dimensionEntity.ScopeChildren.AddRange(from CubeAttribute attribute in dimension.Attributes
                                                       select new OlapEntity(attribute.Attribute.Name, EntityType.CubeAttribute)
                                                       {
                                                           InternalId = attribute.AttributeID,
                                                           Scope = dimensionEntity
                                                       });
                LoggerHelper.Log.InfoFormat("\tAttributes: \r\n\t\t{0}",
                    String.Join(",\r\n\t\t",
                        dimensionEntity.ScopeChildren.Where(c => c.Type == EntityType.CubeAttribute).Select(l => l.Name).ToArray()));

                dimensionEntities.Add(dimensionEntity);
            }
            cubeEntity.ScopeChildren.AddRange(dimensionEntities);

            var measureGroupsEntities = new List<IEntity>();
            foreach (MeasureGroup measureGroup in cube.MeasureGroups)
            {
                LoggerHelper.Log.Info("measureGroupName: " + measureGroup.Name);
                var measureGroupEntity = new OlapEntity(measureGroup.Name, EntityType.MeasureGroup)
                {
                    InternalId = measureGroup.ID,
                    Scope = cubeEntity
                };

                //Retrieving measures
                measureGroupEntity.ScopeChildren = (from Measure measure in measureGroup.Measures
                                                    select
                                                        cubeEntity.ScopeChildren.SingleOrDefault(
                                                            m => m.InternalId == measure.ID && m.Type == EntityType.Measure) ??
                                                        new OlapEntity(measure.Name, EntityType.Measure)
                                                        {
                                                            InternalId = measure.ID,
                                                            Scope = measureGroupEntity
                                                        }).ToList();

                LoggerHelper.Log.InfoFormat("\t\tMeasures: \r\n\t\t\t{0}",
                    String.Join(",\r\n\t\t\t",
                        measureGroupEntity.ScopeChildren.Select(l => l.Name).ToArray()));

                //Linking Measure group and dimensions
                foreach (MeasureGroupDimension dimension in measureGroup.Dimensions)
                {
                    var relatedDimension = dimensionEntities.SingleOrDefault(d => d.InternalId == dimension.Dimension.ID);
                    if (relatedDimension != null)
                    {
                        measureGroupEntity.AddDependency(relatedDimension, new EntityMatch(dimension.Dimension.Name));
                    }
                }

                measureGroupsEntities.Add(measureGroupEntity);
            }
            cubeEntity.ScopeChildren.AddRange(measureGroupsEntities);
        }

        #endregion
    }
}
