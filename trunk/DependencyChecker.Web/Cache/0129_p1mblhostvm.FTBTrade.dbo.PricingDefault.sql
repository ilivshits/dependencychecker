
CREATE view PricingDefault as
select s.sid
	, eodTicker = coalesce(ti_eod.code, gi_eod.code)
	, eodIdentifierTypeId = case
		when ti_eod.code is not null then ti_eod.identifierTypeId
		when gi_eod.code is not null then gi_eod.identifierTypeId
		else 0 end
	, eodTickerLevelId = case
		when ti_eod.code is not null then 2
		when gi_eod.code is not null then 1
		else 0 end
	
	, liveTicker = coalesce(ti_live.code, gi_live.code)
	, liveIdentifierTypeId = case
		when ti_live.code is not null then ti_live.identifierTypeId
		when gi_live.code is not null then gi_live.identifierTypeId
		else 0 end
	, liveTickerLevelId = case
		when ti_live.code is not null then 2
		when gi_live.code is not null then 1
		else 0 end	

from dbo.[Security] s with(nolock)

-- global rule
join dbo.PricingRule gd on gd.securityPrimaryTypeId = 0
left join dbo.SecurityIdentifier gi_live with(nolock) on gi_live.sid = s.sid and gd.liveIdentifierTypeId = gi_live.identifierTypeId
left join dbo.SecurityIdentifier gi_eod with(nolock) on gi_eod.sid = s.sid and gd.eodIdentifierTypeId = gi_eod.identifierTypeId

-- type rule
left join dbo.PricingRule td on s.securityPrimaryTypeId = td.securityPrimaryTypeId
left join dbo.SecurityIdentifier ti_live with(nolock) on ti_live.sid = s.sid and td.liveIdentifierTypeId = ti_live.identifierTypeId
left join dbo.SecurityIdentifier ti_eod with(nolock) on ti_eod.sid = s.sid and td.eodIdentifierTypeId = ti_eod.identifierTypeId

