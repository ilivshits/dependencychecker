CREATE view [dbo].[Role_Transposed] as
select
[[All Users]]] = 1
, [[All Managers]]] = 2
, [[All Funds]]] = 3
, [[All Tags]]] = 5
, [[All Lookup Tables]]] = 6

