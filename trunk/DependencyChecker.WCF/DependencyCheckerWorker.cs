﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using DependencyChecker.Analyzer;
using DependencyChecker.Analyzer.Interfaces;
using DependencyChecker.Collector;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Common;
using DependencyChecker.DataProvider;
using DependencyChecker.DataProvider.DAL;
using DependencyChecker.DataProvider.DAL.Repositories;
using DependencyChecker.DataProvider.Helpers;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Events;
using DependencyChecker.Entities.Interfaces;
using DatabaseEntity = DependencyChecker.Entities.DatabaseEntity;

namespace DependencyChecker.WCF
{
    internal class DependencyCheckerWorker
    {
        private IAnalyzer _analyzer;
        private ExecutionStep _currentStep;
        internal event EventHandler<DependencyCheckerEventArgs> StatusChanged;
        private List<ExecutionStep> _completedSteps;
        


        protected virtual void OnStatusChanged(DependencyCheckerEventArgs e = null)
        {
            if (e == null)
            {
                e = new DependencyCheckerEventArgs();
            }
            e.CompletedSteps = _completedSteps.ToDictionary(s => s.Name, s => s.Weight);
            e.Status = _currentStep.Name;
            e.State = _currentStep.StateType;
            var handler = StatusChanged;
            if (handler != null) handler(this, e);
        }

        internal IAnalyzer GetEntities()
        {
            return GetEntitiesTree();
        }

        private IAnalyzer GetEntitiesTree()
        {
            Thread.Sleep(10000);

            DatabaseHelpers.CreateNewBatch();

            log4net.GlobalContext.Properties["BatchId"] = DatabaseHelpers.GetCurrentBatchId();


            var serversProvider = new ServerProvider();
            var adDataLoader = new ActiveDirectoryDataProvider(serversProvider);
            adDataLoader.Progress += OnProgressChanged;
            var sqlDataLoader = new SQLDataProvider(serversProvider);
            sqlDataLoader.Progress += OnProgressChanged;
            var reportsDataLoader = new ReportsDataProvider(serversProvider);
            reportsDataLoader.Progress += OnProgressChanged;
            var olapDataLoader = new OlapDataProvider(serversProvider);
            olapDataLoader.Progress += OnProgressChanged;
            var tasksLoader = new ScheduledTasksDataProvider(serversProvider);
            tasksLoader.Progress += OnProgressChanged;
            var p1tcLoader = new P1TCDataProvider(serversProvider);
            p1tcLoader.Progress += OnProgressChanged;
            var result = new List<IEntity>();
            //TODO: UNCOMMENT!
            var steps = new List<ExecutionStep>
            {
                /*new ExecutionStep
                {
                    StateType = DependencyCheckerServiceState.LoadingData,
                    Name = "Indexing AD",
                    Weight = 2,
                    Action = () => result.AddRange(adDataLoader.Search())
                },*/
                new ExecutionStep
                {
                    StateType = DependencyCheckerServiceState.LoadingData,
                    Name = "Indexing Tasks",
                    Weight = 8,
                    Action = () =>
                    {
                        try
                        {
                            result.AddRange(tasksLoader.Search());
                        }
                        catch (Exception e)
                        {
                            LoggerHelper.Log.Error(e.Message, e);
                        }
                    }
                },
                /*new ExecutionStep
                {
                    StateType = DependencyCheckerServiceState.LoadingData,
                    Name = "Indexing OLAP",
                    Weight = 10,
                    Action = () => result.AddRange(olapDataLoader.Search())
                },*/
                new ExecutionStep
                {
                    StateType = DependencyCheckerServiceState.LoadingData,
                    Name = "Indexing SSRS",
                    Weight = 10,
                    Action = () =>
                    {
                        try
                        {
                            result.AddRange(reportsDataLoader.Search());
                        }
                        catch (Exception e)
                        {
                            LoggerHelper.Log.Error(e.Message, e);
                        }
                        
                    }
                },
                new ExecutionStep
                {
                    StateType = DependencyCheckerServiceState.LoadingData,
                    Name = "Indexing SQL",
                    Weight = 10,
                    Action = () =>
                    {
                        try
                        {
                            result.AddRange(sqlDataLoader.Search());
                        }
                        catch (Exception e)
                        {
                            LoggerHelper.Log.Error(e.Message, e);
                        }
                    }
                },
                /*new ExecutionStep
                {
                    StateType = DependencyCheckerServiceState.LoadingData,
                    Name = "Indexing P1TC",
                    Weight = 5,
                    Action = () =>
                    {
                        var p1tcData = p1tcLoader.Search();
                        foreach (var server in p1tcData)
                        {
                            var existing =
                                result.OfType<DatabaseEntity>()
                                    .FirstOrDefault(s => s.Name == server.Name && server is DatabaseEntity);
                            if (existing != null)
                            {
                                LoggerHelper.Log.DebugFormat("Merging database server '{0}' with P1TC DB server '{1}'", existing.Name, server.Name);
                                existing.ScopeChildren.AddRange(server.ScopeChildren);
                                server.ScopeChildren.ForEach(c => c.Scope = existing);
                            }
                            else
                            {
                                LoggerHelper.Log.DebugFormat("Adding P1TC DB server '{0}' to results", server.Name);
                                result.Add(server);
                            }
                        }
                    }
                },*/
                new ExecutionStep
                {
                    StateType = DependencyCheckerServiceState.Analyzing,
                    Name = "Analyzing data",
                    Weight = 40,
                    Action = () => { 
                        _analyzer = new Analyzer.Analyzer(result, 
                                    new List<IDataProvider>
                                    {
                                        sqlDataLoader, 
                                        olapDataLoader, 
                                        reportsDataLoader,
                                        p1tcLoader,
                                        adDataLoader
                                    }, 
                                    new ResolverFactory());

                        _analyzer.Progress += OnProgressChanged;
                        _analyzer.Analyze(); 
                    }
                },
                new ExecutionStep()
                {
                    StateType = DependencyCheckerServiceState.Analyzing,
                    Name = "Optimizing cache",
                    Weight = 15,
                    Action = () =>
                    {
                        CacheManager mngr = new CacheManager(DatabaseHelpers.GetCurrentBatchId());
                        mngr.Progress += OnProgressChanged;
                        mngr.CacheToDatabase(_analyzer.Servers);
                    }
                },
                new ExecutionStep()
                {
                    StateType = DependencyCheckerServiceState.Ready,
                    Name = "Ready",
                    Weight = 0,
                    Action = () => { }
                },
            };
            _completedSteps = new List<ExecutionStep>();

            foreach (var step in steps)
            {
                _currentStep = step;
                OnStatusChanged();
                try
                {
                    step.Action();
                }
                catch (Exception e)
                {
                    LoggerHelper.Log.FatalFormat("Exeception occured in step '{0}'. Exiting.. Exception details: {1}", step.Name, e);
                    throw;
                }
                _completedSteps.Add(step);
            }
            return _analyzer;
        }
        
        private void OnProgressChanged(object sender, DependencyCheckerProgressEventArgs eventArgs)
        {
            OnStatusChanged(new DependencyCheckerEventArgs
            {
                Comment = eventArgs.Message,
                Progress = (int)(eventArgs.Progress * _currentStep.Weight),
                StepProgress = (int)(eventArgs.Progress * 100.0)
            });
        }
    }
}
