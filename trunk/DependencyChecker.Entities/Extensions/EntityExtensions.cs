﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Entities.Extensions
{
    public static class EntityExtensions
    {
        public static List<IEntity> RemoveDuplicates(this List<IEntity> localMatches)
        {
            //remove dublicates if it posible or inform about them
            var uniqMatches = new List<IEntity>();
            foreach (var entity in localMatches.Where(entity => 
                                            uniqMatches.Any(it => 
                                                    String.Equals(it.FullName, entity.FullName, StringComparison.InvariantCultureIgnoreCase) && 
                                                    it.Type == entity.Type) == false))
            {
                uniqMatches.Add(entity);
            }
            return uniqMatches;
        }
    }
}
