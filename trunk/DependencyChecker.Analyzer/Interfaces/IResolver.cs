﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Analyzer.Interfaces
{
    public interface IResolver
    {
        String ProviderName { get; }
        bool TryResolve(EntityMatch textMatch, List<IEntity> matches, IEntity entity, SearchContent content, out bool added);
    }
}
