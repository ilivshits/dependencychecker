CREATE view [matching].[SecurityInfo] as
select s.sid
	, s.securityPrimaryTypeId
	, p.matchTypeId
	, t.precision
	, openLotThreshold = isnull(
		o.openLotThreshold, -- take override if present
		case
			when s.securityPrimaryTypeId in (1, 2) then 1.0 -- Cash and FX get 1 by default
			else 1.0 / power(10.0, t.precision) -- everything else honors precision
		end)
from [dbo].[Security] s with (nolock)
join [dbo].[SecurityType] t with (nolock) on s.securityPrimaryTypeId = t.securityPrimaryTypeId and s.securitySubTypeId = t.securitySubTypeId
join [dbo].[SecurityPrimaryType] p with (nolock) on s.securityPrimaryTypeId = p.securityPrimaryTypeId
left join [matching].[SecurityOverride] o on s.sid = o.sid


