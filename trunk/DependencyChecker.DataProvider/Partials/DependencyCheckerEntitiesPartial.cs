﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyChecker.DataProvider
{
    public partial class DependencyCheckerEntities
    {
        public DependencyCheckerEntities(string connectionString)
            : base(connectionString)
        {
        }
    }
}
