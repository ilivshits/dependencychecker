
create view [fortress].[v_Bloomberg_Map_COLLAT_TYP] as
	select '1ST MORTGAGE' as COLLAT_TYP, 2 as collateralTypeId union
	select '1ST REF MORT' as COLLAT_TYP, 3 as collateralTypeId union
	select '2ND MORTGAGE' as COLLAT_TYP, 4 as collateralTypeId union
	select 'AGENCIES' as COLLAT_TYP, 5 as collateralTypeId union
	select 'ASSET BACKED' as COLLAT_TYP, 6 as collateralTypeId union
	select 'BANK GUARANTEED' as COLLAT_TYP, 7 as collateralTypeId union
	select 'BILLS' as COLLAT_TYP, 8 as collateralTypeId union
	select 'BOND' as COLLAT_TYP, 9 as collateralTypeId union
	select 'BONDS' as COLLAT_TYP, 9 as collateralTypeId union
	select 'CAPITAL' as COLLAT_TYP, 10 as collateralTypeId union
	select 'CASH' as COLLAT_TYP, 11 as collateralTypeId union
	select 'CD' as COLLAT_TYP, 12 as collateralTypeId union
	select 'CERT OF DEPOSIT' as COLLAT_TYP, 12 as collateralTypeId union
	select 'COLL TRUST' as COLLAT_TYP, 13 as collateralTypeId union
	select 'COLLATERAL TRUST' as COLLAT_TYP, 13 as collateralTypeId union
	select 'COLLATERALIZE' as COLLAT_TYP, 14 as collateralTypeId union
	select 'COLLATERALIZED' as COLLAT_TYP, 14 as collateralTypeId union
	select 'COMMERCIAL PAPER' as COLLAT_TYP, 15 as collateralTypeId union
	select 'COMPANY ASSETS' as COLLAT_TYP, 16 as collateralTypeId union
	select 'COMPANY GUARANTEED' as COLLAT_TYP, 17 as collateralTypeId union
	select 'COMPANY GUARNT' as COLLAT_TYP, 17 as collateralTypeId union
	select 'CORPUS' as COLLAT_TYP, 18 as collateralTypeId union
	select 'COVERED' as COLLAT_TYP, 19 as collateralTypeId union
	select 'DEBENTURES' as COLLAT_TYP, 20 as collateralTypeId union
	select 'DEBT SECURED' as COLLAT_TYP, 21 as collateralTypeId union
	select 'DEBT UNSECURED' as COLLAT_TYP, 22 as collateralTypeId union
	select 'DEPOSIT' as COLLAT_TYP, 23 as collateralTypeId union
	select 'DEPOSIT NOTES' as COLLAT_TYP, 24 as collateralTypeId union
	select 'DEPOSIT RECEIPTS' as COLLAT_TYP, 25 as collateralTypeId union
	select 'DIRECT' as COLLAT_TYP, 26 as collateralTypeId union
	select 'DISCOUNT' as COLLAT_TYP, 27 as collateralTypeId union
	select 'DISCOUNT NOTES' as COLLAT_TYP, 28 as collateralTypeId union
	select 'EQUIP TRUST' as COLLAT_TYP, 29 as collateralTypeId union
	select 'EQUIPMENT' as COLLAT_TYP, 30 as collateralTypeId union
	select 'EQUIPMENT TRUST' as COLLAT_TYP, 29 as collateralTypeId union
	select 'FACTORY FOUND' as COLLAT_TYP, 31 as collateralTypeId union
	select 'FARM CREDIT SYSTEM' as COLLAT_TYP, 32 as collateralTypeId union
	select 'FARMERS HOME ADMINISTRATION' as COLLAT_TYP, 33 as collateralTypeId union
	select 'FDIC GUARANTEED' as COLLAT_TYP, 34 as collateralTypeId union
	select 'FHA' as COLLAT_TYP, 35 as collateralTypeId union
	select 'FHL BANKS' as COLLAT_TYP, 36 as collateralTypeId union
	select 'FHLMC' as COLLAT_TYP, 37 as collateralTypeId union
	select 'FIDUCIARY' as COLLAT_TYP, 38 as collateralTypeId union
	select 'FINANCING CORP' as COLLAT_TYP, 39 as collateralTypeId union
	select 'FINANCING CORP STRIPS' as COLLAT_TYP, 40 as collateralTypeId union
	select 'FNMA' as COLLAT_TYP, 41 as collateralTypeId union
	select 'GENL REF MORT' as COLLAT_TYP, 42 as collateralTypeId union
	select 'GIC' as COLLAT_TYP, 43 as collateralTypeId union
	select 'GNMA' as COLLAT_TYP, 44 as collateralTypeId union
	select 'GOVT GUARANTEED' as COLLAT_TYP, 45 as collateralTypeId union
	select 'GOVT LIQUID GTD' as COLLAT_TYP, 46 as collateralTypeId union
	select 'HUD' as COLLAT_TYP, 47 as collateralTypeId union
	select 'INSCRIBED' as COLLAT_TYP, 48 as collateralTypeId union
	select 'INSURED' as COLLAT_TYP, 49 as collateralTypeId union
	select 'INVENTORY' as COLLAT_TYP, 50 as collateralTypeId union
	select 'JR SUB DEBS' as COLLAT_TYP, 51 as collateralTypeId union
	select 'JR SUBORDINATED' as COLLAT_TYP, 51 as collateralTypeId union
	select 'JUMBO PFANDBRIEF' as COLLAT_TYP, 52 as collateralTypeId union
	select 'KEEPWELL AGR' as COLLAT_TYP, 53 as collateralTypeId union
	select 'KEEPWELL AGRMNT' as COLLAT_TYP, 53 as collateralTypeId union
	select 'L.Y.O.N.' as COLLAT_TYP, 54 as collateralTypeId union
	select 'LEASES' as COLLAT_TYP, 55 as collateralTypeId union
	select 'LIMITD GUARANTEE' as COLLAT_TYP, 56 as collateralTypeId union
	select 'LIQUIDITY AG' as COLLAT_TYP, 57 as collateralTypeId union
	select 'LOCAL GOVT' as COLLAT_TYP, 58 as collateralTypeId union
	select 'LOCAL GOVT GUARN' as COLLAT_TYP, 59 as collateralTypeId union
	select 'MORTGAGE' as COLLAT_TYP, 60 as collateralTypeId union
	select 'MORTGAGE BACKED' as COLLAT_TYP, 61 as collateralTypeId union
	select 'NEW MONEY' as COLLAT_TYP, 62 as collateralTypeId union
	select 'NOTE' as COLLAT_TYP, 63 as collateralTypeId union
	select 'NOTES' as COLLAT_TYP, 63 as collateralTypeId union
	select 'OLD MONEY' as COLLAT_TYP, 64 as collateralTypeId union
	select 'OTHER' as COLLAT_TYP, 65 as collateralTypeId union
	select 'OWNERSHIP MA' as COLLAT_TYP, 66 as collateralTypeId union
	select 'PARI PASSU' as COLLAT_TYP, 67 as collateralTypeId union
	select 'PARTIAL' as COLLAT_TYP, 68 as collateralTypeId union
	select 'PASS THRU CERTS' as COLLAT_TYP, 69 as collateralTypeId union
	select 'PFANDBRIEFE' as COLLAT_TYP, 70 as collateralTypeId union
	select 'PROPERTY BACK' as COLLAT_TYP, 71 as collateralTypeId union
	select 'PUBLIC LOANS' as COLLAT_TYP, 72 as collateralTypeId union
	select 'RECEIPT' as COLLAT_TYP, 73 as collateralTypeId union
	select 'RECEIVE BACKEND' as COLLAT_TYP, 74 as collateralTypeId union
	select 'REFCORP DEBT OBLIGATIONS' as COLLAT_TYP, 75 as collateralTypeId union
	select 'REFCORP STRIPS' as COLLAT_TYP, 76 as collateralTypeId union
	select 'SECURED' as COLLAT_TYP, 77 as collateralTypeId union
	select 'SENIOR NOTES' as COLLAT_TYP, 78 as collateralTypeId union
	select 'SMALL BUSINESS ADMINISTRATION' as COLLAT_TYP, 79 as collateralTypeId union
	select 'SR DISCOUNT NTS' as COLLAT_TYP, 80 as collateralTypeId union
	select 'SR SECURED' as COLLAT_TYP, 81 as collateralTypeId union
	select 'SR SUB DEBS' as COLLAT_TYP, 82 as collateralTypeId union
	select 'SR SUB NOTES' as COLLAT_TYP, 83 as collateralTypeId union
	select 'SR SUBORDINATED' as COLLAT_TYP, 84 as collateralTypeId union
	select 'SR UNSECURED' as COLLAT_TYP, 85 as collateralTypeId union
	select 'SR UNSUB' as COLLAT_TYP, 86 as collateralTypeId union
	select 'STATE AND LOCAL GOVT SERIES' as COLLAT_TYP, 87 as collateralTypeId union
	select 'STOCK' as COLLAT_TYP, 88 as collateralTypeId union
	select 'STUDENT LOAN MKTG ASSOC' as COLLAT_TYP, 89 as collateralTypeId union
	select 'SUB DEBENTURES' as COLLAT_TYP, 90 as collateralTypeId union
	select 'SUB NOTES' as COLLAT_TYP, 91 as collateralTypeId union
	select 'SUBORDINATED' as COLLAT_TYP, 92 as collateralTypeId union
	select 'SUPPORT AGGR' as COLLAT_TYP, 93 as collateralTypeId union
	select 'TIER I' as COLLAT_TYP, 94 as collateralTypeId union
	select 'TIER II' as COLLAT_TYP, 95 as collateralTypeId union
	select 'UNCONDITIONAL' as COLLAT_TYP, 96 as collateralTypeId union
	select 'UNIT' as COLLAT_TYP, 97 as collateralTypeId union
	select 'UNITS' as COLLAT_TYP, 97 as collateralTypeId union
	select 'UNKNOWN' as COLLAT_TYP, 1 as collateralTypeId union
	select 'UNSECURED' as COLLAT_TYP, 98 as collateralTypeId union
	select 'UNSUBORDINATE' as COLLAT_TYP, 99 as collateralTypeId union
	select 'UNSUBORDINATED' as COLLAT_TYP, 99 as collateralTypeId union
	select 'US GOVT GUARANT' as COLLAT_TYP, 100 as collateralTypeId union
	select 'US GOVT SECURITIES' as COLLAT_TYP, 101 as collateralTypeId union
	select 'US TREASURY OBLIGATIONS' as COLLAT_TYP, 102 as collateralTypeId union
	select 'US TREASURY STRIPS' as COLLAT_TYP, 103 as collateralTypeId union
	select 'WARRANT' as COLLAT_TYP, 104 as collateralTypeId


