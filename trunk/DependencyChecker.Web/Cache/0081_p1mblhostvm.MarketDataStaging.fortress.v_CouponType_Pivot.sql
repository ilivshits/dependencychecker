
create view [fortress].[v_CouponType_Pivot] as
	select 
		 0 as [None],
		 1 as AdjustableConvertibleToFixed,
		 2 as Adjustable,
		 3 as Auction,
		 4 as ConsumerPriceIndexLinked,
		 5 as Defaulted,
		 6 as Exchanged,
		 7 as Fixed,
		 8 as FlatTrading,
		 9 as Floating,
		10 as Funged,
		11 as PayInKind,
		12 as Preliminary,
		13 as Spare,
		14 as StepCoupon,
		15 as TaxCredit,
		16 as Variable,
		17 as Warrant,
		18 as ZeroCoupon,
		19 as WhenIssued,
		20 as OriginalIssueDiscount


