
create view [fortress].[v_Bloomberg_Map_CPN_TYP] as
	select 'ADJ CONV. TO FIXED' as CPN_TYP, 1 as couponTypeId union
	select 'ADJUSTABLE' as CPN_TYP, 2 as couponTypeId union
	select 'AUCTION' as CPN_TYP, 3 as couponTypeId union
	select 'CPI LINKED' as CPN_TYP, 4 as couponTypeId union
	select 'DEFAULTED' as CPN_TYP, 5 as couponTypeId union
	select 'EXCHANGED' as CPN_TYP, 6 as couponTypeId union
	select 'FIXED' as CPN_TYP, 7 as couponTypeId union
	select 'FLAT TRADING' as CPN_TYP, 8 as couponTypeId union
	select 'FLOATING' as CPN_TYP, 9 as couponTypeId union
	select 'FUNGED' as CPN_TYP, 10 as couponTypeId union
	select 'NONE' as CPN_TYP, 0 as couponTypeId union
	select '(NONE)' as CPN_TYP, 0 as couponTypeId union
	select 'ORIG ISSUE DISC' as CPN_TYP, 20 as couponTypeId union
	select 'PAY-IN-KIND' as CPN_TYP, 11 as couponTypeId union
	select 'PRELIM' as CPN_TYP, 12 as couponTypeId union
	select 'SPARE - N/A' as CPN_TYP, 13 as couponTypeId union
	select 'STEP' as CPN_TYP, 14 as couponTypeId union
	select 'STEP CPN' as CPN_TYP, 14 as couponTypeId union
	select 'TAX CREDIT' as CPN_TYP, 15 as couponTypeId union
	select 'VARIABLE' as CPN_TYP, 16 as couponTypeId union
	select 'WARRANT' as CPN_TYP, 17 as couponTypeId union
	select 'ZERO' as CPN_TYP, 18 as couponTypeId union
	select 'WHEN ISSUED' as CPN_TYP, 19 as couponTypeId union
	select 'ZERO COUPON' as CPN_TYP, 18 as couponTypeId


