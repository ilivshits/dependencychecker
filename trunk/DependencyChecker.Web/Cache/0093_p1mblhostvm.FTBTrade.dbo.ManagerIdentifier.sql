CREATE view ManagerIdentifier with schemabinding as
select managerId = objectId, managerIdentifierTypeId = case identifierTypeId when 42 then 1 when 43 then 2 when 45 then 3 when 46 then 4 else identifierTypeId end, code
from dbo.Identifier
where targetId = 5 and identifierTypeId in (42,43,45,46)

