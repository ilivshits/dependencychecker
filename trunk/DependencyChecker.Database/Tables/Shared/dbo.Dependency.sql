﻿CREATE TABLE [dbo].[Dependency]
(
	[UselessId] INT IDENTITY (1, 1) PRIMARY KEY,
	[SourceId] UNIQUEIDENTIFIER CONSTRAINT [FK_Entity_SourceEntity] FOREIGN KEY REFERENCES [dbo].[EntityBase] ([Id]) NOT NULL,
	[DependsOnId] UNIQUEIDENTIFIER CONSTRAINT [FK_Entity_DependsOnEntity] FOREIGN KEY REFERENCES [dbo].[EntityBase] ([Id]) NOT NULL, 
)
