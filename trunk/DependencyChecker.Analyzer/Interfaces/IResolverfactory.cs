﻿using System;
using System.Text;

namespace DependencyChecker.Analyzer.Interfaces
{
    public interface IResolverFactory
    {
        IResolver GetResolver(string providerName);
    }
}
