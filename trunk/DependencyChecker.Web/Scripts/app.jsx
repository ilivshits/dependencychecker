﻿var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var Link = ReactRouter.Link;
var IndexRoute = ReactRouter.IndexRoute;

var ErrorTypes = {
    ToFrom: 'Error: "From date" is later than "To"',
    Empty: 'Error: Pick both dates, please'
};

var ExceptionTypes = {
    Debug: 'DEBUG',
    Info: 'INFO',
    Warning: 'WARN',
    Error: 'ERROR',
    Fatal: 'FATAL'
};

var BatchStatus = {
    Failed: 0,
    Success: 1
};

var ListTypes = {
    Batch: 'Batch',
    Error: 'Error'
};


var Styles = {};
Styles.BetaText = {
    color: '#428bca',
    top: '-10px',
    display: 'inline-block',
    position: 'relative'
}

Styles.CPanel = {
    marginTop: '15px',
    float: 'right'
}

Styles.H2 = {
    marginBottom: '35px',
    color: '#428bca',
    marginTop: '5px'
}

var Master = React.createClass({
    render: function () {
        return (
            <div id="master">
                <div className="container">
                    <div id="header">
                        <h1 className="text-muted">
                            <a href="../">Dependency checker</a>
                            <label className="small" style={Styles.BetaText}>Beta</label>
                            <div style={Styles.CPanel}>
                                <span>
                                    <Link style={{fontSize: '20px'}} to="/">Control panel</Link>
                                </span>
                            </div>
                        </h1>
                        <div id="header-buttons"></div>
                    </div>
                    { this.props.children }
                </div>
            </div>
            );
    }
});

Styles.Index = {};


Styles.Index.Column = {
    width: '49%'
}

var Index = React.createClass({
    componentDidMount: function () {

        DC.ControlPanel.Home.init();
    },
    render: function () {
        return (
            <div id="controlPanelMenu">
                <h2 style={Styles.H2}>Control panel</h2>
                <div className="options">
                    <div className="column" style={Styles.Index.Column}>
                        <div className="row">
                            <Link to="/Users">
                                <div>
                                    <img src="../Content/images/users.png" />
                                </div>
                                <span className="text-muted">Users management</span>
                            </Link>
                        </div>
                        <div className="row">
                            <Link to="/ExceptionLog">
                                <div>
                                    <img src="../Content/images/error.png" />
                                </div>
                                <span className="text-muted">Exception log</span>
                            </Link>
                        </div>
                    </div>
                    <div className="column" style={Styles.Index.Column}>
                        <div className="row">
                            <a id="restartSvcBtn" href="Restart">
                                <div className="caution">
                                    <img src="../Content/images/refresh.png" />
                                </div>
                                <span className="text-muted">Restart service</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

Styles.Users = {};

var Users = React.createClass({
    componentDidMount: function () {
        DC.ControlPanel.Users.init();
    },
    render: function () {
        return (
            <div>
                <div id="usersList">
                    <h2 style={Styles.H2}>Users management</h2>

                    <div id="userManagement">
                        <UserManagement />
                    </div>
                </div>
                <div id="addNewUserModal" className="modal fade" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                                <h4 className="modal-title">Add new administrator</h4>
                            </div>
                            <div className="modal-body">
                                    <span style={{display: 'block', marginBottom: '10px'}}>Logon name (username without domain):</span>
                                    <div className="input-group">
                                        <input id="userNameInput" className="form-control" style={{display: 'inline-block'}} type="text" />
                                        <div className="input-group-btn">
                                            <button className="btn btn-default" id="checkUser">Check existence</button>
                                        </div>
                                    </div>
                            </div>
                            <div className="modal-footer">
                                <button id="addNewUserBtn" style={{ width: '100px' }} type="button" className="btn btn-default">Add</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
});

var ExceptionLog = React.createClass({
    getInitialState: function () {
        return {
            data: [
            ]
        };
    },
    componentDidMount: function () {
        $(".btn-group button").mouseup(function () {
            $(this).blur();
        });
        var date = new Date();
        date.setDate(date.getDate() - 7);

        $('#from-date-picker').datetimepicker({
            format: 'MM.DD.YYYY',
            defaultDate: date
        });


        $('#to-date-picker').datetimepicker({
            format: 'MM.DD.YYYY',
            defaultDate: new Date()
        });

        $('#from-date-picker').on('dp.change', function (e) {
            var minDate = $(this).data("DateTimePicker").date().toDate();
            minDate.setHours(0, 0, 0, 0);
            $('#to-date-picker').data("DateTimePicker").minDate(minDate);
        });
        $('#to-date-picker').on('dp.change', function (e) {
            var maxDate = $(this).data("DateTimePicker").date().toDate();
            maxDate.setHours(0, 0, 0, 0);
            $('#from-date-picker').data("DateTimePicker").maxDate(maxDate);
        });
    },
    onUpdate: function (batches) {
        this.refs.batchList.setState({
            data: batches
        });
    },
    render: function () {
        return (
            <div>
                <div id="search-container">
                    <span className="text-muted picker-label">From date:</span>
                    <div className='input-group date' id='from-date-picker'>
                        <input type='text' className="form-control" />
                        <span className="input-group-addon">
                            <span className="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <span className="text-muted picker-label">To:</span>
                    <div className='input-group date' id='to-date-picker'>
                        <input type='text' className="form-control" />
                        <span className="input-group-addon">
                            <span className="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <div className="btn-group batch-btn-group">
                        <ExceptionLog.SearchButton onUpdate={this.onUpdate}/>
                        <ExceptionLog.ShowAll onUpdate={this.onUpdate} />

                    </div>
                    <span id="date-error" className="text-muted picker-label" style={{display: 'none'}}>Error</span>
                </div>
                <div id="results-list" style={{display: 'inline-block', width: '100%'}}>
                    <ExceptionLog.BatchList ref="batchList" />
                </div>

            </div>
        );
    }
});

ExceptionLog.BatchList = React.createClass({
    getInitialState: function () {
        return { data: [] };
    },
    render: function() {
        var batchListItems = this.state.data.map(function (batch) {
            return (
                <ExceptionLog.BatchList.Li batch={batch} />
            );
        });
        return (
            <ul className="list-group" id="results" style={{ marginTop: '20px' }}>
                {batchListItems}
            </ul>
        );
    }
});

ExceptionLog.BatchList.Li = React.createClass({
    onUpdate: function(excs) {
        this.refs.excList.setState({
            data: excs
        });
    },
    render: function () {
        var batch = this.props.batch;
        if (batch.status == 0) {
            return (
                <li className="list-group-item" data-id={batch.id} data-status={batch.status}>
                    <div className="form-group">
                        <ExceptionLog.BatchList.Li.ExpandButton onUpdate={this.onUpdate} batchId={batch.id}/>
                        <span className="label t label-primary" style={{marginRight: '11px',backgroundColor: 'red'}}>Batch</span>
                        <span>
                            ID: '{batch.id}'; Ran '{batch.date.MM}.{batch.date.dd}.{batch.date.yyyy} {batch.date.HH}:{batch.date.mm}:{batch.date.ss}' by {batch.user}
                        </span>
                        <span style={{color: 'red'}}>  FAILED</span>
                        <ExceptionLog.BatchList.Li.ExceptionLog ref="excList"/>
                    </div>
                </li>
            );
        } else {
            return (
                <li className="list-group-item batch-success" data-id={batch.id} data-status={batch.status}>
                    <div className="form-group">
                        <ExceptionLog.BatchList.Li.ExpandButton onUpdate={this.onUpdate} batchId={batch.id}/>
                        <span className="label t label-primary" style={{marginRight: '11px'}}>Batch</span>
                        <span>
                            ID: '{batch.id}'; Ran '{batch.date.MM}.{batch.date.dd}.{batch.date.yyyy} {batch.date.HH}:{batch.date.mm}:{batch.date.ss}' by {batch.user}
                        </span>
                        <ExceptionLog.BatchList.Li.ExceptionLog ref="excList"/>
                    </div>
                </li>
            );
        }
    }
});

ExceptionLog.BatchList.Li.ExpandButton = React.createClass({
    getInitialState: function () {
        return { isOpen: false };
    },
    handleClick: function (e) {

        var self = this;

        if (this.state.isOpen) {
            self.props.onUpdate([]);
            this.setState({ isOpen: !this.state.isOpen });
            return;
        }


        $("#loadingModal").modal('show');

        
        $.ajax({
            type: 'GET',
            url: 'GetBatchExceptions',
            data: {
                batchId: this.props.batchId
            },
            dataType: "json",
            async: true,
            contentType: "application/json",
            success: function (e) {
                var exceptions = [];
                $.each(e.result, function (index, value) {
                    exceptions.push({
                        level: value.Level,
                        message: escapeHtml(value.Message),
                        exception: value.Exception
                    });
                });

                self.props.onUpdate(exceptions);

                $("#loadingModal").modal('hide');
                self.setState({ isOpen: !self.state.isOpen });

            },
            error: function (msg) {
                $("#loadingModal").modal('hide');

                alert('load failed: \r\n' + msg.responseText);
            }
        });


        
    },
    render: function() {
        return(
            <button type="button" className="btn-default btn-xs" onClick={this.handleClick}>
                {this.state.isOpen ? '-' : '+'}
            </button>
            );
    }
});

ExceptionLog.BatchList.Li.ExceptionLog = React.createClass({
    getInitialState: function () {
        return { data: [] };
    },
    render: function () {
        var excListItems = this.state.data.map(function (exc) {
            return (
                <ExceptionLog.BatchList.Li.ExceptionLog.Li value={exc} />
            );
        });
        return (
            <div className="details">{excListItems}</div>
        );
    }
});

ExceptionLog.BatchList.Li.ExceptionLog.Li = React.createClass({
    onExpand: function(toOpen) {
        if (toOpen) {
            $(this.refs.exceptionText).show();
        } else {
            $(this.refs.exceptionText).hide();
        }
    },

    render: function () {

        var value = this.props.value;

        var color = '#ffa51f';
        if (value.level == ExceptionTypes.Warning) {
            color = '#ffa51f';
        } else if (value.level == ExceptionTypes.Error) {
            color = 'red';
        } else if (value.level == ExceptionTypes.Fatal) {
            color = 'black';
        }
        var levelInfo = <span className="label t label-info" style={{marginRight: '15px', backgroundColor: color }}>Warning</span>;
        var disabled = 'disabled';
        if (value.exception != '' && value.exception != undefined && value.exception != null) {
            disabled = '';
        }
        return (
            <li className="list-group-item" data-level={this.props.value.level}>
                <div className="form-group">
                    <div className="left-align">
                        <ExceptionLog.BatchList.Li.ExceptionLog.Li.ExpandButton disabled={disabled} onExpand={this.onExpand}/>{levelInfo}
                    </div>
                    <span className="error-message j-box-tooltip" title={this.props.value.message}>{this.props.value.message}</span>
                </div>
                <span className="error-exception" ref="exceptionText" style={{ display: 'none' }}>{this.props.value.exception}</span>
            </li>
            );
    }
});

ExceptionLog.BatchList.Li.ExceptionLog.Li.ExpandButton = React.createClass({
    getInitialState: function () {
        return { isOpen: false };
    },
    handleClick: function(e) {
        this.props.onExpand(!this.state.isOpen);
        this.setState({
            isOpen: !this.state.isOpen
        });
    },
    render: function () {
        if (this.props.disabled) {
            return (
                <button type="button" className="btn-default btn-xs disabled" disabled style={{ marginRight: '4px' }} onClick={this.handleClick}>
                    {this.state.isOpen ? '-' : '+'}
                </button>
            );
        } else {
            return (
                <button type="button" className="btn-default btn-xs" style={{ marginRight: '4px' }} onClick={this.handleClick}>
                    {this.state.isOpen ? '-' : '+'}
                </button>
            );
        }

    }
});

ExceptionLog.SearchButton = React.createClass({
    handleClick: function (e) {
        $("#loadingModal").modal('show');
        $('#date-error').hide();
        var fromDatePicker = $("#from-date-picker").data("DateTimePicker").date();
        var toDatePicker = $("#to-date-picker").data("DateTimePicker").date();
        if (!fromDatePicker || !toDatePicker) {
            $('#date-error').text(ErrorTypes.Empty);
            $('#date-error').attr('title', ErrorTypes.Empty);
            $('#date-error').show();
            return;
        }

        var fromDate = fromDatePicker.toDate().toISOString();
        var toDate = toDatePicker.toDate().toISOString();

        var self = this;
        $.ajax({
            type: 'GET',
            url: 'GetBatches',
            data: {
                from: fromDate ,
                to: toDate
            },
            dataType: "json",
            async: true,
            contentType: "application/json",
            success: function (e) {
                var batches = [];
                $.each(e.result, function (index, value) {

                    var date = new Date(value.Date);
                    var yyyy, MM, dd, HH, mm, ss;

                    yyyy = date.getFullYear();

                    MM = (date.getMonth() + 1);
                    MM = ("0" + MM).slice(-2);

                    dd = date.getDate();
                    dd = ("0" + dd).slice(-2);


                    HH = date.getHours();
                    HH = ("0" + HH).slice(-2);

                    mm = date.getMinutes();
                    mm = ("0" + mm).slice(-2);

                    ss = date.getSeconds();
                    ss = ("0" + ss).slice(-2);

                    batches.push({
                        id: value.Id,
                        date: {
                            yyyy: yyyy,
                            MM: MM,
                            dd: dd,
                            HH: HH,
                            mm: mm,
                            ss: ss
                        },
                        user: value.Username,
                        status: value.Status
                    });
                });

                self.props.onUpdate(batches);

                $("#loadingModal").modal('hide');

            },
            error: function (msg) {
                $("#loadingModal").modal('hide');

                alert('load failed: \r\n' + msg.responseText);
            }
        });
    },
    render: function () {
        return (
            <button type="button" className="btn btn-default" id="search-by-date" onClick={this.handleClick}>Search</button>
            );
    }
});

ExceptionLog.ShowAll = React.createClass({
    
    handleClick: function (e) {
        $("#loadingModal").modal('show');
        $('#date-error').hide();
        var self = this;
        $.ajax({
            type: 'GET',
            url: 'GetBatches',
            dataType: "json",
            async: true,
            contentType: "application/json",
            success: function (e) {
                var batches = [];
                $.each(e.result, function (index, value) {

                    var date = new Date(value.Date);
                    var yyyy, MM, dd, HH, mm, ss;

                    yyyy = date.getFullYear();

                    MM = (date.getMonth() + 1);
                    MM = ("0" + MM).slice(-2);

                    dd = date.getDate();
                    dd = ("0" + dd).slice(-2);


                    HH = date.getHours();
                    HH = ("0" + HH).slice(-2);

                    mm = date.getMinutes();
                    mm = ("0" + mm).slice(-2);

                    ss = date.getSeconds();
                    ss = ("0" + ss).slice(-2);

                    batches.push({
                        id: value.Id,
                        date: {
                            yyyy: yyyy,
                            MM: MM,
                            dd: dd,
                            HH: HH,
                            mm: mm,
                            ss: ss
                        },
                        user: value.Username,
                        status: value.Status
                    });
                });

                self.props.onUpdate(batches);
                
                $("#loadingModal").modal('hide');
                
            },
            error: function (msg) {
                $("#loadingModal").modal('hide');

                alert('load failed: \r\n' + msg.responseText);
            }
        });
    },
    render: function () {
        return (
            <button type="button" className="btn btn-default" id="show-all" onClick={this.handleClick}>Show all</button>
            );
    }
});

ReactDOM.render((
    <Router history={ReactRouter.hashHistory}>
        <Route path="/" component={Master}>
            <IndexRoute component={Index} />
            <Route path="/Users" component={Users}></Route>
            <Route path="/ExceptionLog" component={ExceptionLog}></Route>
        </Route>
    </Router>
), document.getElementById('root'));
