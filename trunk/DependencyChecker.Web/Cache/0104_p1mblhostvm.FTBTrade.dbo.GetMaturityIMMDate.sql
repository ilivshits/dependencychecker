

CREATE FUNCTION [dbo].[GetMaturityIMMDate](@idate as datetime, @ipayFrequency as varchar(20))
RETURNS datetime
AS
BEGIN
	-- get next IMM date 
	DECLARE @date datetime;

	set @date = dateadd(year,5, [dbo].[GetNextIMMDate] (@idate,@ipayFrequency))
		
	RETURN Convert(varchar(8), @date, 112)
END






