CREATE PROCEDURE [dbo].[usp_dbmaint_updateusage]
        @cur_dbname                sysname
       ,@report_only               char(1)
AS
--PRINT 'Entering usp_dbmaint_updateusage for ' + QUOTENAME(@cur_dbname)
DECLARE @dynsql                    varchar(8000)
       ,@dynsql_display            varchar(8000)
       ,@spid                      int
       ,@dbmaint_log_rowid         int
       ,@maxrows                   int
       ,@tbl_row_count             bigint
       ,@nclix_count               smallint
       ,@i                         int
       ,@dbname                    sysname
       ,@objectid                  int
       ,@objowner                  sysname
       ,@objname                   sysname
       ,@is_clustered              char(1)
       
IF object_id('tempdb.dbo.#db_maint_objects2') IS NOT NULL
  DROP TABLE #db_maint_objects2

IF object_id('tempdb.dbo.#db_maint_objects_final') IS NOT NULL
  DROP TABLE #db_maint_objects_final
    
SELECT t1.*    
  INTO #db_maint_objects2
  FROM #db_maint_objects t1
  LEFT JOIN #dbmaint_exceptions t2
       ON (t1.dbname        = t2.dbname    AND
           t1.objowner      = t2.objowner  AND
           t1.objname       = t2.objname  AND
           t2.x_updateusage = 'Y')
  WHERE (t2.dbname   IS NULL AND
         t2.objowner IS NULL AND
         t2.objname  IS NULL)  -- excldue/bypass all tables for which updateusage is disabled.
DELETE t1
       FROM #db_maint_objects2 t1
       INNER JOIN #dbmaint_exceptions t2
             ON (t1.dbname   = t2.dbname AND
                 t1.objowner = t2.objowner)
       WHERE (t2.objname  like '%(0-9)'   AND 
              ISNUMERIC(RIGHT(SUBSTRING(t1.objname,2,LEN(t1.objname)-2),
                             (SUBSTRING(t2.objname, 1, charindex('(', t2.objname)-1)))) = 1 )

SELECT IDENTITY(int,1,1) as rowid,  t1.*  
  INTO #db_maint_objects_final
  FROM #db_maint_objects2 t1
  ORDER BY t1.[dbname], [row_count], t1.[objowner], t1.[objname]
  
--SELECT * FROM #dbmaint_exceptions      ORDER BY [dbname], [objowner], [objname]
--SELECT * FROM #db_maint_objects        ORDER BY [dbname], [objowner], [objname]
--SELECT * FROM #db_maint_objects2       ORDER BY [dbname], [objowner], [objname]
--SELECT * FROM #db_maint_objects_final  ORDER BY [dbname], [objowner], [objname]
  
SET @maxrows = ISNULL(SCOPE_IDENTITY(), 0) -- ISNULL function reqd to deal with empty list of tables in the db maint task.
SET @i = 1

PRINT '{' + CONVERT(varchar(22), GETDATE(), 100) + 
      '}     ***** Start - UPDATEUSAGE of database: ' + QUOTENAME(@cur_dbname) + ' *****'
PRINT '{' + CONVERT(varchar(22), GETDATE(), 100) + 
      '}     Number of tables incldued for UPDATEUSAGE task ===> ' + cast(@maxrows as varchar(6))
PRINT '.'
  
LOOP_FOR_EACH_TBL_updateusage:
WHILE (0=0)
  BEGIN
    IF @i > @maxrows BREAK

    SELECT @spid = spid, @dbname = dbname,  @objectid = objectid, @objowner = objowner,
           @objname = objname,  @is_clustered = REPLACE(REPLACE(indid,'1','Y'),'0','N'),
           @tbl_row_count = row_count, @nclix_count = nclix_count
      FROM #db_maint_objects_final
      WHERE rowid = @i


    
    SET @dynsql         = 'DBCC UPDATEUSAGE (' + @dbname + ', ' + CAST(@objectid AS varchar(20)) + ') WITH NO_INFOMSGS'
    SET @dynsql_display = 'DBCC UPDATEUSAGE (' + @dbname + ', ''' + @objowner + '.' + @objname + ''') WITH NO_INFOMSGS  ** [objectid = ' +  CAST(@objectid AS varchar(20)) + ']'
  --PRINT @dynsql    
  PRINT  '(' + CONVERT(varchar(22), GETDATE(),100) +  
         '}     (' + cast(@i as char(6)) + ') ~ Executing ===> ' + @dynsql_display 
  
  SET @dynsql = 'SET QUOTED_IDENTIFIER ON  
                 ' + @dynsql    
  IF @report_only = 'N' 
    BEGIN
      INSERT INTO DBA.dbo.dbmaint_log
                (maint_desc,         [db_name],    obj_owner,    obj_name, 
                 maint_start_time,   [status],     executed_by,  spid, 
                 is_clustered,       nclix_count,  row_count)
        VALUES ('UPDATEUSAGE', @dbname, @objowner, @objname, 
                GETDATE(), 'started', SUSER_SNAME(), @spid, 
                @is_clustered, @nclix_count, @tbl_row_count)
               
      SET @dbmaint_log_rowid = SCOPE_IDENTITY() 
      EXEC (@dynsql)
    
      UPDATE DBA.dbo.dbmaint_log
        SET maint_end_time = GETDATE(),
            [status] = 'successful'
        WHERE rowid = @dbmaint_log_rowid
    END
    
  SET @i = @i + 1
END   --LOOP_FOR_EACH_TBL_updateusage
  
PRINT '.'
PRINT '{' + CONVERT(varchar(22), GETDATE(), 100) + 
      '}     ***** End  - UPDATEUSAGE of database: ' + QUOTENAME(@cur_dbname) + ' *****'
PRINT '.'
PRINT REPLICATE('==>', 50)
RETURN

