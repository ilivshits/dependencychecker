﻿using System;
using System.Collections.Generic;

namespace DependencyChecker.Common
{
    public class DependencyCheckerStatus
    {
        public String Status { get; set; }
        public String Comment { get; set; }
        public DependencyCheckerServiceState State { get; set; }
        public int Progress { get; set; }
        public int StepProgress { get; set; }
        public Dictionary<String, int> CompletedSteps;
    }
}