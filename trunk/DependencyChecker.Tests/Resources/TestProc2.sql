﻿



--[foffice].[GetImagineAexeoBlotter_test] @strategy = 'X-IN'

CREATE PROCEDURE [foffice].[GetImagineAexeoBlotter]
		@aexeo_trader_id varchar(500) = '4',
		@meta_fund_id varchar(500) = '38',
		@strategy varchar(max) = NULL,
		@live bit = 1,
		@asofdate datetime = NULL,
		@run bit = 1,
		@InstrumentTypes as varchar(5000) = null,
		@IncludeBeta bit = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF (@run = 0) RETURN;
	IF (@live = 1)
	BEGIN
	
		SELECT * into #aexeo_traders FROM dbo.fun_Split(@aexeo_trader_id, ',') t where Items > 0
		union
		SELECT m.aexeo_trader_id FROM dbo.fun_Split(@aexeo_trader_id, ',') t 
		join foffice.trader_group tg on tg.trader_group_id = -cast(t.Items as int)
		join foffice.trader_group_trader_map m on m.trader_group_id = tg.trader_group_id
		where t.Items < 0;

	
		print convert(varchar(30), getdate(), 109);
		
		select r1.*, ch.notional [NotionalEx]
		into #xxx
		from [Mips].[dbo].[z_mips_reporting1] r1
		inner join Mips.dbo.v_current_holdings ch on ch.holding_id = r1.RowID

		select r1.*
		into #tmpReporting1
		from #xxx r1
		where (@strategy IS NULL OR r1.Acct IN 
				(SELECT Items COLLATE Latin1_General_BIN2 FROM dbo.fun_Split(@strategy, ',')))
		
		--select r1.*, ch.notional [NotionalEx]
		--into #tmpReporting1
		--from [Mips].[dbo].[z_mips_reporting1] r1
		--inner join Mips.dbo.v_current_holdings ch on ch.holding_id = r1.RowID

		create table #beta
		(
			tid bigint,
			value_date datetime,
			[1] float,
			[2] float
		)
		
		if (@IncludeBeta = 1)
		BEGIN
			DECLARE @time_index int;
			SELECT @asofdate = max(date) from [RiskData].[dbo].ImagineBetasTid
			select @time_index = risk.GetBeta_SnapTime(@asofdate)
			
			
		declare @alternateDate datetime
		set @alternateDate = @asofdate
			insert into #beta
				SELECT tid, value_date, [1],[2] FROM 
				(SELECT tid, unit_beta, beta_factor_id, value_date FROM [RiskData].[dbo].v_betas_vdate WHERE date = @alternateDate AND time_index = @time_index AND repo = '') A
				PIVOT (sum(unit_beta) FOR beta_factor_id in ([1],[2])) AS P
		END
		print convert(varchar(30), getdate(), 109);
		-----------------------------------------------------------------------
		-- With block "A"... get MiPS holdings
		-----------------------------------------------------------------------
		
		SELECT 
		r1.Legal_Entity_Name 'Fund', 
		r1.[Type],
		v_t.label [Trader], 
		v_t.aexeo_trader_id,
		r1.Acct, 
		r1.Customer,
		max(r1.Long_Security_Name) as Long_Security_Name,
		cast(r1.Value10 as int) as Text14, --Text14, 
		--r1.FIG_Value_Date,
		case when r1.Type='EFWD' 
			--then dbo.NextBusinessDay(dateadd(d, 2, cast(r1.FIG_Value_Date as datetime)))
			Then dbo.AddTwoBussinesDays(cast(r1.FIG_Value_Date as datetime))
			else r1.FIG_Value_Date end as FIG_Value_Date, --r1.FIG_Value_Date,
		case --when r1.[Type] = 'SWAP' AND r1.Curr = 'BRL' then sum(r1.Value15) 
				 when	r1.[Type] in ( 'BND' , 'TBD' , 'FRN' , 'TBL', 'CNV', 'IBND') then sum(r1.Notional)
				 else sum(r1.FIG_Quantity) 
		end 'Quantity',
		sum([FIG_ESP]) [FIG_ESP],
		sum([FIG_ESP_$]) [FIG_ESP_$],
		sum([FIG_ESP_N]) [FIG_ESP_N],
		sum([FIG_Dv01_$]) [FIG_Dv01_$],
		sum([FIG_Cr01_$]) [FIG_Cr01_$],
		sum([FIG_Gamma_$]) [FIG_Gamma_$],
		sum([FIG_Theta_$]) [FIG_Theta_$],
		sum([FIG_Vega_$]) [FIG_Vega_$],
		sum([FIG_P&L]) [FIG_P&L],
		sum([FIG_Mkt_Value]) [FIG_Mkt_Value],
		sum([FIG_Prev_Mark]) [FIG_Prev_Mark],
		sum([FIG_Mkt]) [FIG_Mkt],
		sum(NotionalEx) NotionalEx,
		sum([Mod_Vol]) [Mod_Vol],
		SUM(case when r1.Type in ('CDS', 'CDSO') then (1.0-Econ_Market)*100 else Econ_Market end) Econ_Market,
		SUM(case when r1.Type in ('CDS', 'CDSO') then (1.0-PreviousEconMark)*100 else PreviousEconMark end) PreviousEconMark
		into #A
		FROM #tmpReporting1 r1
			JOIN foffice.v_trader v_t ON v_t.abbrev = r1.Trader
			JOIN foffice.v_legalentity v_l ON v_l.le_name = r1.Legal_Entity_Name			
		WHERE v_t.aexeo_trader_id IN (SELECT * FROM #aexeo_traders)
			AND v_l.fundId IN (SELECT * FROM dbo.fun_Split(@meta_fund_id, ','))
			AND (@strategy IS NULL OR r1.Acct IN 
				(SELECT Items COLLATE Latin1_General_BIN2 FROM dbo.fun_Split(@strategy, ',')))
			AND r1.Type <> 'RP'
		group by r1.Legal_Entity_Name, 
		r1.[Type],
		v_t.label, 
		v_t.aexeo_trader_id,
		r1.Acct, 
		r1.Customer,
		--r1.Long_Security_Name,
		cast(r1.Value10 as int),
		r1.FIG_Value_Date,
		r1.Curr
		print convert(varchar(30), getdate(), 109);
		-----------------------------------------------------------------------
		-- With block "B"... get YiPS holdings
		-----------------------------------------------------------------------
		select * into #ActivPnl
			from openquery(MPS,'select Type, Trader, Fund, Strategy, [Clear Broker],
			[Instrument Description], Id, [Value Date], [End Qty], [End OTE], [P&L],
			[Mkt Val], [Qty Change], [Average Cost [N]]], [Start Price [N]]], [End Price [N]]],
			i.instrument_type_id, IsNull(ui.price_factor, 1) as u_price_factor, IsNull(ui.tid, 0) as underlying_tid
			from [y_assets].[dbo].[ActivePnL] p 
			JOIN common.dbo.instrument i on p.Id = i.tid
			left JOIN common.dbo.instrument ui on ui.tid = i.underlying_tid')
		
		select v_l.le_name 'Fund', 
		Type,
		v_t.label [Trader],
		v_t.aexeo_trader_id,
		CASE Strategy WHEN '' THEN 'EMPTY' ELSE Strategy END AS Strategy, 
		[Clear Broker],
		[Instrument Description], 
		cast(Id as int) as Id, 
		convert(char(8), [Value Date], 112) 'Value Date',
		[End Qty],
		[End OTE],
		[P&L] [Aexeo P&L],
		[Mkt Val] [Mkt Val],
		[Qty Change] [Aexeo Qty Change],
		instrument_type_id,
		u_price_factor,
		underlying_tid,
		[Average Cost [N]]] [Average Cost],  -- a bit risky since it isn't additive, but should be unique.
		[Start Price [N]]] [Start Price],  -- a bit risky since it isn't additive, but should be unique.
		[End Price [N]]] [End Price]  -- a bit risky since it isn't additive, but should be unique. into #tmpB
		into #tmpB
		--FROM [MPS].[y_assets].[dbo].[ActivePnL] ap 
		FROM #ActivPnl ap
		JOIN foffice.v_trader v_t ON v_t.name = ap.Trader
		JOIN foffice.v_legalentity v_l ON v_l.aexeo collate Latin1_General_BIN2 = ap.Fund
		WHERE v_t.aexeo_trader_id IN (SELECT * FROM #aexeo_traders)
		AND v_l.fundId IN (SELECT * FROM dbo.fun_Split(@meta_fund_id, ','))
		AND (@strategy IS NULL OR upper(CASE Strategy WHEN '' THEN 'EMPTY' ELSE Strategy END) IN (SELECT Items COLLATE Latin1_General_BIN2 FROM dbo.fun_Split(@strategy, ',')))
		print convert(varchar(30), getdate(), 109);
		--select * 
		--into #tmpBond from MPS.common.dbo.bond bb 
		--where bb.tid in (select distinct Id from #tmpB)

		
		SELECT Fund, 
		Type,
		Trader,
		aexeo_trader_id,
		Strategy, 
		[Clear Broker],
		[Instrument Description], 
		Id, 
		[Value Date],
		case when instrument_type_id = 34 and underlying_tid not in (select tid from MPS3.Imagine.dbo.prec_metal) then 
				[End Qty]/IsNull(u_price_factor, 1)
			 else [End Qty]
		end 'Quantity',
		[End OTE],
		[Aexeo P&L],
		[Mkt Val],
		case when instrument_type_id = 34 and underlying_tid not in (select tid from MPS3.Imagine.dbo.prec_metal) then 
				[Aexeo Qty Change]/IsNull(u_price_factor, 1)
			 else [Aexeo Qty Change]
		end [Aexeo Qty Change],
		[Average Cost],  -- a bit risky since it isn't additive, but should be unique.
		[Start Price],  -- a bit risky since it isn't additive, but should be unique.
		[End Price]  -- a bit risky since it isn't additive, but should be unique.
		into #B
		FROM #tmpB ap 
		--LEFT JOIN #tmpBond bb ON bb.tid = ap.Id
		--LEFT JOIN [ftb_common].[dbo].[bond] b ON b.tid = ap.Id
		--left join MPS.common.dbo.instrument i on i.tid = ap.Id
		--left join MPS.common.dbo.instrument ui on i.underlying_tid = ui.tid
		
		-----------------------------------------------------------------------
		--  Join of "A" and "B"... reconciliation
		-----------------------------------------------------------------------
		select 
		isnull(A.Fund, B.Fund) 'Fund',
		isnull(A.Trader, B.Trader) 'Trader',
		isnull(A.aexeo_trader_id, B.aexeo_trader_id) 'aexeo_trader_id',
		isnull(A.Acct, B.Strategy) 'Strategy',
		isnull(A.Customer, B.[Clear Broker]) 'Clearing_Broker',
		A.Type 'Imagine_Type',
		A.Long_Security_Name 'Imagine_Security_Name',
		A.Text14 'Imagine_Text14', 
		convert(datetime, A.FIG_Value_Date) 'Imagine_Value_Date',
		A.Quantity 'Imagine_Quantity',
		B.Type 'Aexeo_Type',
		B.[Instrument Description] 'Aexeo_Instrument_Description', 
		B.Id 'Aexeo_TID', 
		convert(datetime, B.[Value Date]) 'Aexeo_Value_Date',
		B.Quantity 'Aexeo_Quantity',
		CASE WHEN abs(isnull(A.Quantity, 0) - isnull(B.Quantity, 0)) > 0.9 
			THEN abs(isnull(A.Quantity, 0) - isnull(B.Quantity, 0)) 
			ELSE 0 
  	    END 'Diff',
		--abs(isnull(A.Quantity, 0) - isnull(B.Quantity, 0)) 'Diff',
		[FIG_ESP],
		[FIG_ESP_$],
		[FIG_ESP_N],
		[FIG_Dv01_$],
		[FIG_Cr01_$],
		[FIG_Gamma_$],
		[FIG_Theta_$],
		[FIG_Vega_$],
		[FIG_P&L],
		[FIG_Mkt_Value],
		B.[Mkt Val] [Aexeo Market Value],
		[End OTE],
		[Aexeo P&L],
		[Aexeo Qty Change],
		[Average Cost],
		[Start Price],
		[End Price],
		[FIG_Mkt],
		[FIG_Prev_Mark],
		Econ_Market,
		PreviousEconMark,
		[Mod_Vol],
		isnull(bt.[1],0) * A.NotionalEx [SPX Beta 30],
		isnull(bt.[2],0) * A.NotionalEx [SPX Beta 90]
		from #A A full outer join #B B
		on A.Fund collate SQL_Latin1_General_CP1_CI_AS = B.Fund
		and A.Trader collate SQL_Latin1_General_CP1_CI_AS = B.Trader
		and A.Acct collate SQL_Latin1_General_CP1_CI_AS = upper(B.Strategy)
		and A.Customer collate SQL_Latin1_General_CP1_CI_AS= B.[Clear Broker]
		and A.Text14 = B.Id
		--and (A.FIG_Value_Date IS NULL OR B.[Value Date] IS NULL OR IsNull(A.FIG_Value_Date, 0) = IsNull(B.[Value Date], 0))
		and (A.FIG_Value_Date IS NULL OR B.[Value Date] IS NULL OR cast(A.FIG_Value_Date as datetime) = cast(B.[Value Date] as datetime))
		left join #beta bt on A.Text14 = bt.tid AND COALESCE(cast(A.FIG_Value_Date as datetime), @asofdate) = bt.value_date 
		where B.Type collate Latin1_General_BIN2 in (select * from dbo.fun_Split(@InstrumentTypes, ',')) 
		or @InstrumentTypes is null
		or B.Type is null
		
		print convert(varchar(30), getdate(), 109);
	END
	
END



