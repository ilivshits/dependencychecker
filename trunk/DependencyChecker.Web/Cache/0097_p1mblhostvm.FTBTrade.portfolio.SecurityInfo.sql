CREATE view [portfolio].[SecurityInfo] as
select s.sid
	, s.[description]
	, s.ticker
	, s.exchangeId
	, s.priceMultiplier
	, s.securityPrimaryTypeId
	, s.securitySubTypeId
	, costBasisFlag = coalesce(cb_o2.costBasisFlag, cb_o1.costBasisFlag, cb_d.costBasisFlag, 0)
	, s.currencySid
	, s.issuerId
	, ultimateIssuerId = case when i.ultimateParentId != 0 then i.ultimateParentId else s.issuerId end
	, discountFactor = coalesce(irs.discountFactor, u_irs.discountFactor, 1)
	, faceValue = coalesce(b.faceValue, m.faceValue, u_b.faceValue, 1)
	, cds_coupon = cds.coupon
	, cds_payFrequencyId = cds.pay_frequencyId
	, irs_payLegId = irs.payLegId
	, fx_fromCurrencySid = fx.fromCurrencySid
	, fx_toCurrencySid = fx.toCurrencySid
	, s.underlying_sid
	, maturityDate = coalesce(opt.expiryDate, b.maturityDate, '9999-12-31')
	, varianceFactor = coalesce(mips.quantityMultiplier,
		case
			when spt.securityPrimaryTypeId = [Variance Swap] then vs.strike * 2
			when spt.securityPrimaryTypeId = [Forward Rate Agreement] and irs_rcv.rateSourceSid = 0 then -1
			else 1
		end
		, 1)
	, lotSizeInitial = coalesce(s.lotSizeInitial, spt.defaultLotSizeInitial)
	, mipsPriceFactor = mips.priceMultiplier

from [dbo].[Security] s
join [dbo].[SecurityPrimaryType] spt on s.securityPrimaryTypeId = spt.securityPrimaryTypeId
join [dbo].[Issuer] i on s.issuerId = i.issuerId
cross join [dbo].[SecurityPrimaryType_Transposed]

left join [dbo].[FX] fx on s.sid = fx.sid
left join [dbo].[Bond] b on s.sid = b.sid
left join [dbo].[CreditDefaultSwap] cds on s.sid = cds.sid
left join [dbo].[InterestRateSwap] irs on s.sid = irs.sid
left join [dbo].[InterestRateSwapLeg] irs_rcv on irs.receiveLegId = irs_rcv.interestRateSwapLegId
left join [dbo].[VolVarSwap] vs on s.sid = vs.sid
left join [dbo].[Muni] m on s.sid = m.sid

left join [dbo].[Bond] u_b on s.securityPrimaryTypeId = [Bond Total Return Swap] and s.underlying_sid = u_b.sid
left join [dbo].[InterestRateSwap] u_irs on s.underlying_sid = u_irs.sid
left join [dbo].[Option] opt on s.sid = opt.sid
left join [dbo].[Future] f on s.underlying_sid = f.sid

left join [dbo].[CostBasisMapping] cb_d
on s.securityPrimaryTypeId = cb_d.securityPrimaryTypeId
and s.securitySubTypeId = cb_d.securitySubTypeId
and cb_d.exchangeId = 0
and cb_d.contractSpecificationSid = 0

left join [dbo].[CostBasisMapping] cb_o1
on s.securityPrimaryTypeId = cb_o1.securityPrimaryTypeId
and s.securitySubTypeId = cb_o1.securitySubTypeId
and cb_o1.exchangeId = s.exchangeId
and cb_o1.contractSpecificationSid = 0
 
left join [dbo].[CostBasisMapping] cb_o2
on s.securityPrimaryTypeId = cb_o2.securityPrimaryTypeId
and s.securitySubTypeId = cb_o2.securitySubTypeId
and cb_o2.exchangeId = s.exchangeId
and cb_o2.contractSpecificationSid = f.contractSpecificationSid

left join portfolio.MipsOverride mips on s.sid = mips.sid


