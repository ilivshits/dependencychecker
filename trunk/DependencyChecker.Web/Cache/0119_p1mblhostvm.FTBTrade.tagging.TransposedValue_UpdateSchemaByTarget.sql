
CREATE PROCEDURE [tagging].[TransposedValue_UpdateSchemaByTarget]
	@targetId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--- Get table definition items.
	declare @targetPrimaryKeyColumnName varchar(max)
	declare @targetPrimaryKeyColumnType varchar(max)
	declare @transposedValueTable varchar(max)
	declare @transposedValueTablePrimaryKeyName varchar(max)

	--- Populate variables.
	select 
		@targetPrimaryKeyColumnName = t.targetPrimaryKeyColumnName,
		@targetPrimaryKeyColumnType = t.targetPrimaryKeyColumnType,
		@transposedValueTable = t.transposedValueTable,
		@transposedValueTablePrimaryKeyName = t.transposedValueTablePrimaryKeyName
	from
		[tagging].[Target_TransposedValueTable] as t
	where
		t.targetId = @targetId

	--- Select the report table top-level info.
	select @transposedValueTable as transposedValueTable, @targetPrimaryKeyColumnName as targetPrimaryKeyColumnName,
		@targetId as targetId

	--- Get the object id of the report table.
	declare @transposedValueTableObjectId int = object_id(@transposedValueTable)

	--- The sql.
	declare @sql varchar(max)

	--- If the table does not exist, then create the table now.
	if @transposedValueTableObjectId is null
	begin
		--- The sql.
		set @sql = 'create table ' + @transposedValueTable + 
			' (contextId int not null, startDate date not null, endDate date not null, ' + 
			@targetPrimaryKeyColumnName + ' ' + @targetPrimaryKeyColumnType + 
			' not null)' 

		--- Debug.
		print @sql

		--- Execute the SQL
		exec (@sql)

		--- Create the SQL.
		set @sql = 'alter table ' + @transposedValueTable + ' add constraint ' + 
			@transposedValueTablePrimaryKeyName + ' primary key clustered (contextId asc, [' + 
			@targetPrimaryKeyColumnName + 
			'] asc, startDate asc, endDate asc) on [PRIMARY]'

		--- Print the sql.
		print @sql
		
		--- Execute.
		exec (@sql)
		
		--- Set the object id.
		set @transposedValueTableObjectId = object_id(@transposedValueTable)
	end			

	--- The transposed columns.
	select
		t.*
	into
		#transposedColumns
	from
		[tagging].[Tag_TransposedColumns] as t
	where
		t.targetId = @targetId

	--- The columns to add.
	declare @columns varchar(max) = ''

	--- Populate.
	select
		@columns = @columns + 
			'[' + t.columnName + '] ' + t.sqlDataType + ' null, '
	from
		#transposedColumns as t
	where
		not exists (
			select 
				sc.* 
			from 
				sys.columns as sc 
			where 
				--- The column name is the same.
				sc.object_id = @transposedValueTableObjectId and
				sc.name = t.columnName and
				
				--- Start date, end date, primary key name.
				sc.name not in ('startDate', 'endDate', 'contextId', @targetPrimaryKeyColumnName)
		)
		
	--- Trim everything up.
	set @columns = LTRIM(RTRIM(@columns))

	--- If there are columns, continue.
	if (LEN(@columns) > 0)
	begin
		--- Generate SQL
		set @sql = 'alter table ' + @transposedValueTable + ' add ' + 
			--- Remove last comma, space already trimmed.
			left(@columns, LEN(@columns) - 1)

		--- Print the SQL.
		print @sql

		--- Execute
		exec (@sql)
	end

	--- Reset columns.
	set @columns = ''

	--- Get the columns that exist, but shouldn't.
	select
		@columns = @columns + '[' + c.name + '], '
	from
		sys.columns as c
	where
		--- Table id.
		c.object_id = @transposedValueTableObjectId and
		
		--- Fabricated column.
		not exists (
			select
				tc.*
			from
				#transposedColumns as tc
			where
				tc.columnName = c.name
		) and
		
		--- Start, end date, primary key columns.
		c.name not in ('startDate', 'endDate', 'contextId', @targetPrimaryKeyColumnName)

	--- Trim everything up.
	set @columns = LTRIM(rtrim(@columns))

	--- If there are columns, continue.
	if (LEN(@columns) > 0)
	begin
		--- Generate SQL
		set @sql = 'alter table ' + @transposedValueTable + ' drop column ' + 
			--- Remove last comma, space already trimmed.
			LEFT(@columns, len(@columns) - 1)
				
		--- Print the SQL.
		print @sql
		
		--- Execute.
		exec (@sql)
	end

	--- Select the columns.
	select 
		tc.tagId, tc.columnName, tc.sqlDataType, cast(tc.displayValue as bit) as displayValue
	from
		#transposedColumns as tc

	--- Cleanup.
	drop table #transposedColumns
END



