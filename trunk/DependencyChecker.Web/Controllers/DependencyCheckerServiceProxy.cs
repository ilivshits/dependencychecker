﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using DependencyChecker.Common;
using DependencyChecker.Entities.Interfaces;
using DependencyChecker.WCF;

namespace DependencyChecker.Web.Controllers
{
    public class DependencyCheckerServiceProxy : ClientBase<IDependenciesCheckerService>, IDependenciesCheckerService
    {
        #region Implementation of IDependenciesCheckerService

        public DependencyCheckerStatus GetStatus()
        {
            return Channel.GetStatus();
        }

        public byte[] GetUnusedCleanupScripts(Guid id)
        {
            return Channel.GetUnusedCleanupScripts(id);
        }

        public byte[] GetUnusedCleanupScriptsForSet(Guid databaseId, Guid[] ids)
        {
            return Channel.GetUnusedCleanupScriptsForSet(databaseId, ids);
        }

        public List<Entity> GetUnusedEntities(Guid databaseId, int generations)
        {
            return Channel.GetUnusedEntities(databaseId, generations);
        }

        public string GetState()
        {
            return Channel.GetState();
        }

        public string PrintEntity(Guid id)
        {
            return Channel.PrintEntity(id);
        }

        public string ExportEntity(Guid id, bool up, bool down)
        {
            return Channel.ExportEntity(id, up, down);
        }

        public List<Entity> Search(string name, bool useRegularExpression, bool searchInDefinition, bool exactSearch)
        {
            return Channel.Search(name, useRegularExpression, searchInDefinition, exactSearch);
        }

        public Entity Drillthrough(Guid uid, String[] types)
        {
            return Channel.Drillthrough(uid, types);
        }

        public Entity GetDetails(Guid id)
        {
            return Channel.GetDetails(id);
        }

        public Entity GetEntitiesTree(Guid id, TraversalDirection direction)
        {
            return Channel.GetEntitiesTree(id, direction);
        }

        public string PrintEntityDefinition(Guid id)
        {
            return Channel.PrintEntityDefinition(id);
        }

        public DateTime? GetLastRunTime()
        {
            return Channel.GetLastRunTime();
        }

        public List<Entity> GetDatabases()
        {
            return Channel.GetDatabases();
        }

        public List<Entity> GetBatchEntities(int batchId)
        {
            return Channel.GetBatchEntities(batchId);
        }

        public List<string> GetEntityErrors(int batchId, string entityName)
        {
            return Channel.GetEntityErrors(batchId, entityName);
        }

        public List<Batch> GetBatches(DateTime? from = null, DateTime? to = null)
        {
            return Channel.GetBatches(from, to);
        }

        public List<BatchException> GetBatchExceptions(int batchId)
        {
            return Channel.GetBatchExceptions(batchId);
        }

        public void RestartService()
        {
            Channel.RestartService();
        }

        #endregion
    }
}