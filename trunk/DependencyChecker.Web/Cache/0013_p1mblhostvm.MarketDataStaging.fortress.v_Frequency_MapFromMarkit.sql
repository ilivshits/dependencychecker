
create view [fortress].[v_Frequency_MapFromMarkit] as
	select
		f.frequencyId,
		
		case 
			when f.periodsPerYear = 0 then null
			when f.periodsPerYear = 1 then '1Y'
			when (12 % f.periodsPerYear) = 0 then ltrim(str(12 / f.periodsPerYear)) + 'M'
		end as Frequency
	from
		FTBTrade.dbo_Frequency as f with (nolock)


