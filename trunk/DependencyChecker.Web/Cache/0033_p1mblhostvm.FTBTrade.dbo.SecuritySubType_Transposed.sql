CREATE VIEW [dbo].[SecuritySubType_Transposed] WITH SCHEMABINDING AS
SELECT
[Single Name] = 1
, [Index] = 2
, [Sovereign] = 3
, [Bond/Note] = 5
, [Common Stock] = 6
, [Depositary Receipt] = 7
, [Mutual Fund] = 8
, [Partnership Shares] = 9
, [Preferred] = 10
, [Real Estate Investment Trust] = 11
, [Right] = 12
, [Unit] = 13
, [Single Currency] = 14
, [Overnight Index] = 15
, [Cross Currency] = 16
, [BRL CDI] = 17
, [Cross Currency Basis] = 18
, [Basis] = 19
, [Benchmark FRA] = 20
, [Benchmark Swap] = 21
, [Exchange-Traded Product] = 34
, [TBA] = 40
, [Mortgage Servicing Rights Pool] = 46
, [Warrants] = 49
, [Residential Loan] = 50
, [Agency] = 53
, [Non-Agency] = 54
, [Reverse Mortgage] = 62
, [FSP] = 65

