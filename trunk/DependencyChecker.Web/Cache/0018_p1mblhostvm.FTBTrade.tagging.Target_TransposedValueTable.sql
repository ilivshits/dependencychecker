create view [tagging].[Target_TransposedValueTable] as
	select
		targ.targetId, 
		s.name as targetSchemaName,
		t.name as targetTableName,
		pkc.name as targetPrimaryKeyColumnName,
		st.name as targetPrimaryKeyColumnType,
		'[tagging].[TransposedValue_' + s.name + '_' + t.name + ']' as transposedValueTable,
		'[PK_tagging_TransposedValue_' + s.name + '_' + t.name + ']' as transposedValueTablePrimaryKeyName
	from
		dbo.Target as targ with (nolock)
			inner join sys.tables as t with (nolock) on
				t.object_id = object_id(targ.tableName)
			inner join sys.schemas as s with (nolock) on
				s.schema_id = t.schema_id and
				s.name = targ.schemaName
			inner join sys.indexes as i with (nolock) on
				i.object_id = t.object_id
			inner join sys.index_columns as ic with (nolock) on
				ic.object_id = t.object_id and
				ic.index_id = i.index_id
			inner join sys.columns as pkc with (nolock) on
				pkc.object_id = t.object_id and
				pkc.column_id = ic.column_id
			inner join sys.types as st with (nolock) on
				st.system_type_id = pkc.system_type_id
	where
		--- Primary keys only.
		i.is_primary_key != 0


