﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using DependencyChecker.Collector.Base;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Collector.Properties;
using DependencyChecker.Common;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Events;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.Collector
{
    public class SQLDataProvider : BaseDataProvider
    {
        

        private static readonly List<String> SystemDatabases = new List<string>
		// ReSharper disable StringLiteralTypo
		{
			"master", "model", "msdb", "tempdb"
		};
        // ReSharper enable StringLiteralTypo

        private static readonly List<String> _systemFunctions = new List<string>
            // ReSharper disable StringLiteralTypo
        {
            "sys.master_files",
        };
        // ReSharper enable StringLiteralTypo
        private static readonly List<String> _systemViews = new List<string>
		// ReSharper disable StringLiteralTypo
		{
            "ASCII",
            "CHARINDEX",
            "CONCAT",
            "DIFFERENCE",
            "FORMAT",
            "LTRIM",
            "NCHAR",
            "PATINDEX",
            "QUOTENAME",
            "REPLACE",
            "REPLICATE",
            "REVERSE",
            "RIGHT",
            "LEFT",
            "RTRIM",
            "SPACE",
            "STRING_ESCAPE",
            "STRING_SPLIT",
            "STUFF",
            "SUBSTRING",
            "UNICODE",
            "STRING_SPLIT",
            "GETDATE",
            "DATENAME",
            "DATEADD",
            "COALESCE",
            "CONVERT",
            "ROW_NUMBER",
            "CONVERT",
            "CONVERT",
            "CONVERT",
            "CONVERT"
        };

        private readonly Regex _spaceCleanup = new Regex(@"[ ]+",
        RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private const long MatchLogTimeout = 1000;

        private static readonly Regex excludedColumnsReg = new Regex(@"(?<=(^[\s\t\n\r]*\[*|,[\s\t\n\r]*\[*))[\w()'""]+(?=(\]*\s[\w\s()\[\]'""]+)($|,))|(?<=(^\s*|,\s*)(\b[\w]+\b\s*))(?<exclude>\b[\w]+\b\s*)", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Compiled);
        private static readonly Regex xmlFValuesPicker = new Regex(@"([\s\t\n\r]|^)[\w\.]+\.(value|nodes)\s*\(.*?\)[,\r\n\t]", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled);
        private static readonly List<Regex> BasicPattern =
            new List<Regex>{ new Regex(
                    @"([ \t\r\n]DELETE(?<trigger>[ \t\r\na-z_0-9]+)FROM[ \t\r\na-z_0-9]+,[ \t\r\n]+DELETED|" + //Handles triggers
                    @"[ \t\r\n]DECLARE[ \t\r\n]+(?<exclude>[a-z_0-9]+)[ \t\r\n]+CURSOR[ \t\r\n]+FOR|" +
                    @"(?<=([\s\t\r\n]declare[\s\t\r\n]+(?<exclude>\S+)[\s\t\r\n]+table[\s\t\r\n]*\([\s\t\r\n]*))(?<table_cols>(([^()])+|(?<Level>\()|(?<-Level>\)))+)(?(Level)(?!))(?=\))|" +

                    @"[ \t\r\n]FROM[ \t\r\n]+(?<exclude>OPENQUERY)|" +
                    @"(?<=[ \t\r\n]|;)WITH[ \t\r\n]+(?<exclude>([a-z0-9]+|\[[a-z0-9_\- ]+\]))[ \t\r\n]*(AS)?[ \t\r\n]*\(|" +
                    @"[ \t\r\n]INTO[ \t\r\n]+(?<exclude>\#([a-z0-9_\$\#]+|\[[a-z0-9_\- ~!%\$\#]+\]))|" +

                    @"[ \t\r\n]EXEC(UTE)?[ \t\r\n]+(?<name>((?<scope>[a-z0-9_\-\[\]\.~]+)\.)?([a-z0-9_\$\#]+|\[[a-z0-9_\- ~!%\$\#]+\]))|" +
                    @"[ \t\r\n]JOIN[ \t\r\n]+((?<name>((?<scope>[a-z0-9_\-\[\]\.~]+)\.)?([a-z0-9_\$\#]+|\[[a-z0-9_\- ~!%\$\#]+\]))(?<alias>[ \t\r\n]+(as[ \t\r\n]+)?([a-z0-9_]+|\[[a-z0-9_\- ]+\]))?([ \r\t\n]+,)?)+([\r\n\t\s\(;\)]|$)|" +
                    @"[ \t\r\n]INTO[ \t\r\n]+(?<name>((?<scope>[a-z0-9_\-\[\]\.~]+)\.)?([a-z0-9_\$\#]+|\[[a-z0-9_\- ~!%\$\#]+\]))|" +
                    @"[ \t\r\n]INSERT[ \t\r\n]+(INTO[ \t\r\n]+)?(?<name>((?<scope>[a-z0-9_\-\[\]\.~]+)\.)?([a-z0-9_\$\#]+|\[[a-z0-9_\- ~!%\$\#]+\]))|" +
                    @"[ \t\r\n]UPDATE[ \t\r\n]+(?<name>((?<scope>[a-z0-9_\-\[\]\.~]+)\.)?([a-z0-9_\$\#]+|\[[a-z0-9_\- ~!%\$\#]+\]))|" +
                    @"[ \t\r\n]AT[ \t\r\n]+(?<name>((?<scope>[a-z0-9_\-\[\]\.~]+)\.)?([a-z0-9_\$\#]+|\[[a-z0-9_\- ~!%\$\#]+\]))|" +
                    //Handles most of objects
                    //@"FROM[ \t\r\n]+((?<name>((?<scope>[a-z0-9_\-\[\]\.~]+)\.)?([a-z0-9_\$\#]+|\[[a-z0-9_\- ~!%\$\#]+\]))(?<alias>[ \t\r\n]+(as[ \t\r\n]+)?([a-z0-9_]+|\[[a-z0-9_\- ]+\]))?([ \r\t\n]+,)?)+([\r\n\t\s\(;\)]|$)|" +
                    @"[ \t\r\n]FROM[ \t\r\n]+((?<name>((?<scope>[a-z0-9_\-\[\]\.~]+)\.)?([a-z0-9_\$\#]+|\[[a-z0-9_\- ~!%\$\#]+\]))(?<alias>[ \t\r\n]+(as[ \t\r\n]+)?([a-z0-9_]+|\[[a-z0-9_\- ]+\]))?[ \r\t\n]+,)*((?<name>((?<scope>[a-z0-9_\-\[\]\.~]+)\.)?([a-z0-9_\$\#]+|\[[a-z0-9_\- ~!%\$\#]+\]))(?<alias>[ \t\r\n]+(as[ \t\r\n]+)?([a-z0-9_]+|\[[a-z0-9_\- ]+\]))?)([\r\n\t\s\(;\)]|$)|" +
                    @"[ \t\r\n]FROM[ \t\r\n]+(?<name>((?<scope>[a-z0-9_\-\[\]\.~]+)\.)?([a-z0-9_\$\#]+|\[[a-z0-9_\- ~!%\$\#]+\]))([\r\n\t\s\(;\)]|$)|" +

                    @"([ \t\r\n]ISNULL[ \t\r\n]*\(|\([ \t\r\n]*|=|>|<|,|FROM|THEN|ELSE|SELECT|WHEN|OR|AND|CROSS[ \t\r\n]+APPLY|CASE|\+|\-|\*|[ \t\r\n]+)[ \t\r\n]*(?<name>(?<scope>[a-z0-9_\-\[\]\.~]+)\.(\[[a-z0-9_\- ~!%\$\#]+\]|[a-z0-9_\$\#]+))([ \t]+)?\(|" +
                    //Handles functions

                    @"'\]?\.(?<name>(?<scope>[a-z0-9_\-\[\]\.~]+)\.(\[[a-z0-9_\- ~!%\$\#]+\]|[a-z0-9_\$\#]+))" +
                    //Handles tricky dynamic definitions
                    ")",
                    RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.Compiled)};

        private bool IsSystemView(string name)
        {
            return _systemViews.Contains(name);
        }

        private bool IsSystemFunction(string name)
        {
            return _systemFunctions.Contains(name.ToUpper());
        }

        #region Implementation of IDataProvider

        public SQLDataProvider(IServerProvider serverProvider)
            : base(serverProvider)
        {
        }

        public override string Name
        {
            get { return DatabaseEntity.ProviderName; }
        }

        protected override List<Regex> ItemSearchPattern
        {
            get { return BasicPattern; }
        }

        public override List<EntityMatch> MatchItems(string input, IEntity entity)
        {
            var sw = new Stopwatch();
            input = input.Trim();

            input = _spaceCleanup.Replace(input, "  ");
            input = xmlFValuesPicker.Replace(input, "");
            var matches = ItemSearchPattern.SelectMany(r => r.Matches(input).Cast<Match>());
            var result = new List<EntityMatch>();
            sw.Start();
            var tableColumnsMatches = matches.Where(m => m.Groups["table_cols"].Success).Select(m => m.Groups["table_cols"].Captures[0].Value);
            var tableColumns = String.Join(",", tableColumnsMatches);
            var excludedMatches = new List<string>();

            var declareTableMatches = excludedColumnsReg.Matches(tableColumns);
            foreach (Match tableMatch in declareTableMatches)
            {
                string excludeVal = tableMatch.Value;

                string textWithoutBrackets = excludeVal;
                string textWithBrackets = "[" + textWithoutBrackets + "]";
                if (excludedMatches.FirstOrDefault(match => match == textWithoutBrackets || match == textWithBrackets) == null)
                {
                    excludedMatches.Add(textWithoutBrackets);
                    excludedMatches.Add(textWithBrackets);
                }

            }

            var captures = matches.Where(m => m.Groups["exclude"].Success).SelectMany(m => m.Groups["exclude"].Captures.Cast<Capture>(), (m, b) => b.Value);
            excludedMatches = excludedMatches.Union(captures).ToList();

            foreach (Match match in matches)
            {
                if (!match.Groups["name"].Success && !match.Groups["trigger"].Success && !match.Groups["exclude"].Success)
                {
                    entity.AddErrorFormat(EntityErrorCode.ParsingError, "Entity: {1} [{2}]. Content '{0}' has mismatch: '{3}'",
                        input, entity.FullName, entity.TypeName, match.Value);
                    continue;
                }
                sw.Stop();
                if (sw.ElapsedMilliseconds > MatchLogTimeout)
                {
                    LoggerHelper.Log.DebugFormat("MATCH '{2}' TOOK: {0} FOR '{1}'", sw.ElapsedMilliseconds, entity.FullName, match.Groups["name"].Value);
                }

                var i = 0;
                foreach (var capture in match.Groups["name"].Captures.Cast<Capture>())
                {
                    var captureText = capture.Value.Trim().ToLowerInvariant();
                    var scopeText = match.Groups["scope"].Success ? match.Groups["scope"].Captures[i].Value.Trim().ToLowerInvariant() : String.Empty;
                    i++;

                    if (IsKeyword(captureText) || IsSystemFunction(captureText) || IsSystemView(captureText))
                    {
                        continue;
                    }
                    if (excludedMatches.FirstOrDefault(s => s.Trim().ToLower().Contains(capture.Value.Trim().ToLower())) != null)
                    {
                        continue;
                    }
                    var existing = result.SingleOrDefault(r => r.MatchText == captureText);
                    if (existing != null)
                    {
                        existing.Indexes.Add(capture.Index);
                    }
                    else
                    {
                        result.Add(new EntityMatch
                        {
                            MatchText = captureText,
                            ScopeText = scopeText,
                            Indexes = new List<int> { capture.Index }
                        });
                    }
                }
                sw.Restart();
            }
            if (sw.IsRunning)
            {
                sw.Stop();
                if (sw.ElapsedMilliseconds > MatchLogTimeout)
                {
                    LoggerHelper.Log.DebugFormat("MATCH WITH NO RESULT TOOK: {0} FOR '{1}'", sw.ElapsedMilliseconds, entity.FullName);
                }
            }
            return result.ToList();
        }

        private float _serverWight;
        private float _totalProgress;
        private float _databaseWeight;
        private float _serverProgress;

        public override List<IEntity> Search()
        {
            LoggerHelper.Log.InfoFormat("Starting search sql objects...");
            var result = new List<IEntity>();

            try
            {
                var servers = ServerProvider.GetServers(EntityProviderType.SQL);
                _serverWight = 1.0f / servers.Count;
                foreach (var server in servers)
                {
                    OnProgress(new DependencyCheckerProgressEventArgs
                    {
                        Message = "Loading server " + server,
                        Progress = _totalProgress
                    });
                    result.Add(LoadServer(server));
                    _totalProgress += _serverWight;
                }

                foreach (var server in result)
                {
                    foreach (
                        var linked in server.ScopeChildren.Where(c => c.TypeName == EntityType.LinkedServer.ToString()))
                    {
                        var linkedServer =
                            result.SingleOrDefault(
                                s =>
                                    String.Compare(s.Name, linked.Definition,
                                        StringComparison.InvariantCultureIgnoreCase) == 0);
                        if (linkedServer != null)
                        {
                            linked.AddDependency(linkedServer, linked.Definition);
                            linkedServer.Aliases.Add(new EntityAlias
                            {
                                TypeName = EntityType.LinkedServer.ToString(),
                                DisplayName = linked.Name,
                                DataProvider = DatabaseEntity.ProviderName,
                                DefinitionScope = linked.Scope
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LoggerHelper.Log.FatalFormat("SQL objects search failed! Exception: {0}", e);
            }
            return result;
        }

        #endregion


        private static readonly Dictionary<String, EntityType> DatabaseObjectTypes = new Dictionary<string, EntityType>
        {
            {"P", EntityType.StoredProcedure},
            {"PC", EntityType.StoredProcedure},
            {"X", EntityType.StoredProcedure},

            {"FN", EntityType.Function},
            {"TF", EntityType.Function},
            {"IF", EntityType.Function},
            {"AF", EntityType.Function},
            {"FS", EntityType.Function},
            {"FT", EntityType.Function},

            

            {"TA", EntityType.Trigger},
            {"TR", EntityType.Trigger},
            {"U", EntityType.Table},
            {"V", EntityType.View},
            {"SN", EntityType.Synonym}
        };

        private IEntity LoadServer(string serverName)
        {
            var server = new DatabaseEntity(serverName, EntityType.Server);
            try
            {
                server.ScopeChildren = LoadDatabases(server);
                server.ScopeChildren.AddRange(LoadLinkedServers(server));
                server.ScopeChildren.AddRange(LoadJobs(server));
                LoadTablesFKDependencies(server);
                FillDatabaseMetadata(server);

                GC.Collect();
            }
            catch (Exception e)
            {
                LoggerHelper.Log.ErrorFormat("Error loading server {1}: {0}", e, server.Name);
            }
            return server;
        }

        private static IEnumerable<IEntity> LoadLinkedServers(IEntity server)
        {
            var result = new List<IEntity>();
            var connBuilder = new SqlConnectionStringBuilder
            {
                DataSource = server.Name,
                IntegratedSecurity = true,
                ApplicationName = "DependencyChecker"
            };


            (server as DatabaseEntity).LinkedServers = new List<IEntity>();

            using (var connection = new SqlConnection(connBuilder.ConnectionString))
            {
                using (var command = new SqlCommand(@"select s.name SRV_NAME, s.data_source SRV_DATASOURCE, s.modify_date SRV_MODIFIED from sys.servers s", connection) { CommandType = CommandType.Text })
                {
                    connection.Open();

                    var reader = command.ExecuteReader();
                    

                    while (reader.Read())
                    {
                        var srv = new DatabaseEntity(reader["SRV_NAME"].ToString(),
                            reader["SRV_DATASOURCE"].ToString(), EntityType.LinkedServer)
                        {
                            Scope = server,
                            LastModified = (DateTime) reader["SRV_MODIFIED"]
                        };
                        result.Add(srv);
                        (server as DatabaseEntity).LinkedServers.Add(srv);
                    }
                    connection.Close();
                }
            }

            return result;
        }

        private static IEnumerable<IEntity> LoadJobs(IEntity server)
        {
            var connBuilder = new SqlConnectionStringBuilder
            {
                DataSource = server.Name,
                InitialCatalog = "msdb",
                IntegratedSecurity = true,
                ApplicationName = "DependencyChecker"
            };

            var result = new List<IEntity>();
            (server as DatabaseEntity).Jobs = new List<IEntity>(); 
            using (var connection = new SqlConnection(connBuilder.ConnectionString))
            {
                using (
                    var command =
                        new SqlCommand(SqlQueryStrings.GetJobsMetaQueryString, connection) { CommandType = CommandType.Text })
                {
                    try
                    {
                        LoggerHelper.Log.InfoFormat("Reading jobs from '{0}'", server.FullName);
                        connection.Open();
                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var jobName = reader["job_name"].ToString();
                            var stepName = reader["step_name"].ToString();
                            var stepCommand = reader["step_command"].ToString();
                            var databaseName = reader["database_name"].ToString();
                            var jobCreated = (DateTime)reader["date_created"];
                            var jobModified = (DateTime)reader["date_modified"];
                            var jobLastRun = (DateTime)reader["last_run_datetime"];
                            var job = result.SingleOrDefault(r => r.Name == jobName);
                            if (job == null)
                            {
                                job = new DatabaseEntity(jobName, EntityType.Job)
                                {
                                    ScopeChildren = new List<IEntity>(),
                                    JobSteps = new List<IEntity>(),
                                    Created = jobCreated,
                                    LastModified = jobModified,
                                    JobLastRunTime = jobLastRun
                                };
                                result.Add(job);
                            }
                            (server as DatabaseEntity).Jobs.Add(job);
                            var newStep = new DatabaseEntity(stepName, stepCommand, EntityType.JobStep);
                            (job as DatabaseEntity).JobSteps.Add(newStep);
                            job.ScopeChildren.Add(newStep);

                            var database =
                                server.ScopeChildren.SingleOrDefault(
                                    d =>
                                        d.Name.Equals(databaseName, StringComparison.InvariantCultureIgnoreCase) &&
                                        d.Type == EntityType.Database);

                            if (database != null)
                            {
                                newStep.Scope = database;
                            }
                            else
                            {
                                newStep.AddErrorFormat(EntityErrorCode.InvalidDefinition, "Cannot find database {0} on server {1} defined for the job step {2}",
                                    databaseName, server.FullName, newStep.FullName);
                            }
                            job.AddDependency(newStep, newStep.Name);
                        }
                        LoggerHelper.Log.DebugFormat("Found {0} jobs TSQL steps", result.Count);

                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        var sqlEx = ex as SqlException;
                        //LoginFailed
                        if (sqlEx != null && sqlEx.Number == 4060)
                        {
                            server.AddErrorFormat(EntityErrorCode.AccessError, "Cannot access database [msdb] on server '{0}'", server.FullName);
                        }
                        else
                        {
                            server.AddErrorFormat(EntityErrorCode.AccessError, "Cannot load jobs from '{0}'\r\nException occurred: '{1}'", server.FullName, ex);
                        }
                    }
                }
            }
            return result;
        }

        private List<IEntity> LoadDatabases(IEntity server)
        {
            var connBuilder = new SqlConnectionStringBuilder
            {
                DataSource = server.Name,
                IntegratedSecurity = true,
                ApplicationName = "DependencyChecker"
            };

            DataTable databases = null;
            var dbMeta = new Dictionary<string, DateTime>();
            using (var connection = new SqlConnection(connBuilder.ConnectionString))
            {
                try
                {
                    connection.Open();
                    databases = connection.GetSchema(SqlClientMetaDataCollectionNames.Databases);
                    using (SqlCommand command = new SqlCommand("select name, crdate create_date from master.dbo.sysdatabases", connection) { CommandType = CommandType.Text })
                    {
                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            dbMeta.Add((string)reader["name"], (DateTime)reader["create_date"]);
                        }
                    }
                    connection.Close();
                }
                catch (Exception ex)
                {
                    server.AddErrorFormat(EntityErrorCode.AccessError,
                        "Cannot load jobs from '{0}'\r\nException occurred: '{1}'", server.FullName, ex.Message);
                }
            }

            if (databases != null)
            {
                _databaseWeight = _serverWight / databases.Rows.Count;
                _serverProgress = 0;
                
                var dbList = (from DataRow row in databases.Rows
                              select LoadDatabase(server, row["database_name"].ToString()))
                        .ToList();

                foreach (var db in dbList.Cast<DatabaseEntity>())
                {
                    if (dbMeta.ContainsKey(db.Name))
                    {
                        var dbCreationDate = dbMeta[db.Name];
                        db.Created = dbCreationDate;
                    }
                }

                return dbList;
            }
            return new List<IEntity>();
        }

        private void LoadTablesFKDependencies(DatabaseEntity server)
        {
            var connBuilder = new SqlConnectionStringBuilder
            {
                DataSource = server.Name,
                IntegratedSecurity = true,
                ApplicationName = "DependencyChecker"
            };

            using (var connection = new SqlConnection(connBuilder.ConnectionString))
            {
                using (var command = new SqlCommand(SqlQueryStrings.GetForeignKeysQueryString, connection) { CommandType = CommandType.Text })
                {
                    connection.Open();
                    var reader = command.ExecuteReader();



                    while (reader.Read())
                    {
                        string dbName = reader["db_name"].ToString();
                        string fkSchema = reader["FK_Schema"].ToString();
                        string fkTable = reader["FK_Table"].ToString();
                        string pkSchema = reader["PK_Schema"].ToString();
                        string pkTable = reader["PK_Table"].ToString();

                        var dbEntity = server.ScopeChildren.FirstOrDefault(x => x.Type == EntityType.Database && x.Name == dbName);
                        var fkSchemaEntity = dbEntity?.ScopeChildren.FirstOrDefault(x => x.Type == EntityType.Schema && x.Name == fkSchema);
                        var pkSchemaEntity = dbEntity?.ScopeChildren.FirstOrDefault(x => x.Type == EntityType.Schema && x.Name == pkSchema);
                        var fkTableEntity =
                            fkSchemaEntity?.ScopeChildren.FirstOrDefault(
                                x => x.Type == EntityType.Table && x.Name == fkTable);
                        var pkTableEntity =
                            pkSchemaEntity?.ScopeChildren.FirstOrDefault(
                                x => x.Type == EntityType.Table && x.Name == pkTable);

                        if (fkTableEntity != null && pkTableEntity != null)
                        {
                            fkTableEntity.AddDependency(pkTableEntity, pkTableEntity.Name);
                        }
                    }
                    reader.Close();
                    connection.Close();
                }
            }
        }

        private IEntity LoadDatabase(IEntity server, String databaseName)
        {
            var database = new DatabaseEntity(databaseName, EntityType.Database)
            {
                Scope = server
            };

            if (SystemDatabases.Contains(databaseName))
            {
                return database;
            }

            var schemas = database.ScopeChildren = LoadSchemas(database);
            var connBuilder = new SqlConnectionStringBuilder
            {
                DataSource = database.Scope.Name,
                IntegratedSecurity = true,
                ApplicationName = "DependencyChecker",
                InitialCatalog = database.Name,
                ConnectTimeout = 1000
            };

            /*
SELECT 
	o.object_id as id, 
	COALESCE(OBJECT_DEFINITION(o.object_id), syn.base_object_name) as [definition], 
	o.type, 
	o.name, 
	sc.name as [schema]
FROM sys.objects as o
JOIN sys.schemas as sc ON  sc.[schema_id] = o.[schema_id]
LEFT JOIN sys.synonyms as syn ON (syn.[object_id] = o.[object_id])
WHERE o.type IN ('PC','FN','P','TF','TR','SN','U','V','IF','AF','FS','FT','TA','X')
AND sc.name <> 'sys'
AND o.name = 'spGetIncompatablePositions'
ORDER BY o.name
             * */


            

            using (var connection = new SqlConnection(connBuilder.ConnectionString))
            {
                using (var command = new SqlCommand(@"SELECT o.object_id as id, COALESCE(def.[text], syn.base_object_name) as [definition], o.type, o.name, sc.name as [schema]
FROM sys.objects as o
JOIN sys.schemas as sc ON  sc.[schema_id] = o.[schema_id]
LEFT JOIN sys.synonyms as syn ON (syn.[object_id] = o.[object_id])
OUTER APPLY (
	SELECT 
		REPLACE(
			REPLACE(
				REPLACE(
					REPLACE(
						(SELECT '' + s.[text] as [text()] 
							FROM syscomments AS s 
							WHERE s.id = o.[object_id]
							ORDER BY colid
							FOR XMl PATH ('')), 
					'&#x0D;', char(13)), 
				'&lt;', '<'), 
			'&gt;', '>'), 
		'&amp;', '&') as [text]
) def
WHERE o.type IN (" + String.Join(",", DatabaseObjectTypes.Keys.Select(k => "'" + k + "'").ToArray()) + @")
AND sc.name <> 'sys'
ORDER BY o.name", connection) { CommandType = CommandType.Text })
                {
                    try
                    {
                        LoggerHelper.Log.InfoFormat("Reading objects from '{0}'", database.FullName);
                        connection.Open();

                        var reader = command.ExecuteReader();
                        var total = 0;
                        while (reader.Read())
                        {
                            var type = ParseType(reader["type"].ToString().Trim().ToUpper());
                            if (type != EntityType.Unresolved)
                            {
                                var schema = schemas.Single(s => s.Name == reader["schema"].ToString());
                                var definition = (reader["definition"] == DBNull.Value
                                    ? String.Empty
                                    : (string)reader["definition"]);
                                var dbEntity = new DatabaseEntity(reader["name"].ToString(), definition, type)
                                {
                                    Scope = schema,
                                    InternalId = reader["id"].ToString(),
                                };

                                schema.ScopeChildren.Add(dbEntity);
                            }
                            total++;
                        }
                        LoggerHelper.Log.DebugFormat("Found {0} objects", total);

                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        var sqlEx = ex as SqlException;
                        //LognFailed
                        if (sqlEx != null && sqlEx.Number == 4060)
                        {
                            database.AddErrorFormat(EntityErrorCode.AccessError, "Cannot access database '{0}'",
                                database.FullName);
                        }
                        else
                        {
                            database.AddErrorFormat(EntityErrorCode.AccessError, "Cannot load database objects from '{0}'\r\nException occurred: '{1}'", database.FullName, ex.Message);
                        }
                    }
                }
            }
            
            _serverProgress += _databaseWeight;
            OnProgress(new DependencyCheckerProgressEventArgs
            {
                Message = "Database " + database.FullName + " loaded",
                Progress = _totalProgress + _serverProgress
            });
            return database;
        }

        private static List<IEntity> LoadSchemas(IEntity database)
        {
            var connBuilder = new SqlConnectionStringBuilder
            {
                DataSource = database.Scope.Name,
                IntegratedSecurity = true,
                ApplicationName = "DependencyChecker",
                InitialCatalog = database.Name
            };

            var schemaNames = new List<String>();
            using (var connection = new SqlConnection(connBuilder.ConnectionString))
            {
                using (var command = new SqlCommand("SELECT name FROM sys.schemas WHERE schema_id < 500", connection) { CommandType = CommandType.Text })
                {
                    try
                    {
                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            schemaNames.Add(reader[0].ToString());
                        }
                        LoggerHelper.Log.DebugFormat("Found {0} schemas in '{1}'", schemaNames.Count, database.FullName);

                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        var sqlEx = ex as SqlException;
                        //LognFailed
                        if (sqlEx != null && sqlEx.Number == 4060)
                        {
                            database.AddErrorFormat(EntityErrorCode.AccessError, "Cannot access database '{0}'", database.FullName);
                        }
                        else
                        {
                            database.AddErrorFormat(EntityErrorCode.AccessError, "Cannot load schemas from '{0}'\r\nException occurred: '{1}'", database.FullName, ex.Message);
                        }
                    }
                }
            }

            return new List<IEntity>(schemaNames.Select(name => new DatabaseEntity(name, EntityType.Schema)
            {
                Scope = database,
                ScopeChildren = new List<IEntity>()
            }).ToList());
        }

        private static void FillEntityAuditData(DatabaseEntity dBase, SqlDataReader reader)
        {
            string pName = (string)reader["schema_name"];
            DatabaseEntity schema = (DatabaseEntity)dBase.ScopeChildren?.SingleOrDefault(e => e.Name == pName);
            if (schema == null) return;
            //if (schema.StoredProcedures == null) schema.StoredProcedures = new List<IEntity>();

            var entity = schema.ScopeChildren.Find(e => e.Name == (string)reader["name"]);
            entity.Created = (DateTime)reader["create_date"];
            entity.LastModified = (DateTime)reader["modify_date"];


            /*schema.StoredProcedures.Add(new DatabaseEntity((string)reader["name"], EntityType.StoredProcedure)
            {
                Created = (DateTime)reader["create_date"],
                LastModified = (DateTime)reader["modify_date"]
            });*/
        }

        /// <summary>
        /// Fills databases audit information, like: Tables creation date, Stored procedures last modify date...
        /// </summary>
        /// <param name="server"></param>
        private static void FillDatabaseMetadata(IEntity server)
        {
            DatabaseEntity dbServer = server as DatabaseEntity;
            var dbEntityConnBuilder = new SqlConnectionStringBuilder
            {
                DataSource = server.Name,
                IntegratedSecurity = true,
                ApplicationName = "DependencyChecker"
            };

            var schemaNames = new List<String>();

            using (var connection = new SqlConnection(dbEntityConnBuilder.ConnectionString))
            {
                connection.Open();

                LoggerHelper.Log.Info("Starting retrieving creation and modification data for entities");

                using (var command = new SqlCommand(SqlQueryStrings.GetEntitiesMetaQueryString, connection) { CommandType = CommandType.Text })
                {
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        DatabaseEntity dBase;
                        DatabaseEntity schema;

                        dBase = (DatabaseEntity)dbServer.ScopeChildren?.SingleOrDefault(e => e.Name == (string)reader["dbname"]);
                        if (dBase == null) continue;

                        string pName = (string)reader["schema_name"];
                        
                        switch ((string)reader["obj_type"])
                        {
                            //TODO: new functional
                            case "schema":
                                if (dBase.Schemas == null) dBase.Schemas = new List<IEntity>();
                                dBase.Schemas.Add(new DatabaseEntity(pName, EntityType.Schema));
                                break;
                            default:
                                FillEntityAuditData(dBase, reader);
                                break;
                        }
                    }
                    
                    reader.Close();
                }

                LoggerHelper.Log.Info("Finished retrieving creation and modification data for entities");
                LoggerHelper.Log.Info("Starting retrieving audit data for entities");

                using (var command = new SqlCommand(SqlQueryStrings.GetAuditDataQueryString, connection) {CommandType = CommandType.Text})
                {
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        
                        string dbName = (string)reader["db_name"];
                        string eventType = (string)reader["event_type"];
                        string objType = (string)reader["obj_type"];
                        string objName = (string)reader["obj_name"];
                        string loginName = (string)reader["login_name"];
                        DateTime eventDate = (DateTime)reader["event_date"];
                        string cmd = (string)reader["sql_cmd"];

                        DatabaseEntity dbEntity = (DatabaseEntity)server.ScopeChildren.Find(db => db.Name == dbName);
                        if (dbEntity == null) continue;
                        
                        string opTypeWithObjectName = @"(CREATE|ALTER)\s+(FUNCTION|TABLE|VIEW|PROCEDURE|PROC)\s+([^\s])*"; //Ex: "CREATE TABLE [dbo].[New_Table]"
                        var match = Regex.Match(cmd, opTypeWithObjectName,RegexOptions.IgnoreCase);

                        // In case of we did not find strings needed
                        if (string.IsNullOrEmpty(match.Value)) continue;

                        string entityPath = match.Value.Trim().Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries).Last();                           //Ex: "[dbo].[New_Table]"
                        string[] entityPathValues = entityPath.Split(new char[] {'.'}, StringSplitOptions.RemoveEmptyEntries);                                  //Ex: "[dbo]", "[New_Table]"
                        string schemaName = entityPathValues.Length == 1 ? (string)reader["default_schema"] : entityPathValues[entityPathValues.Length - 2];    //Ex: "[dbo]"
                        schemaName = schemaName.TrimStart('[').TrimEnd(']');                                                                                    //Ex: "dbo"

                        IEntity entity = null;
                        IEntity refactoredEntity = null;
                        DatabaseEntity schema = (DatabaseEntity)dbEntity.ScopeChildren.SingleOrDefault(e => e.Name.ToUpper() == schemaName.ToUpper());
                        if (schema == null) continue;
                        switch (objType.ToUpper())
                        {
                            case "TABLE":
                            case "VIEW":
                            case "PROCEDURE":
                            case "FUNCTION":
                                entity = schema.ScopeChildren.Find(e => e.Name == objName);
                                //refactoredEntity = schema.Functions?.Find(e => e.Name == objName); //TODO: New functionality
                                break;
                        }
                        if (entity != null)
                        {
                            if (eventType.ToUpper().Contains("CREATE"))
                            {
                                entity.CreatedBy = new PersonEntity(loginName);
                                entity.Created = eventDate;
                                /*if (refactoredEntity != null) //TODO: New functionality
                                {
                                    refactoredEntity.CreatedBy = new PersonEntity(loginName);
                                    refactoredEntity.Created = eventDate;
                                }*/
                            }
                            else if (eventType.ToUpper().Contains("ALTER"))
                            {
                                entity.LastModified = eventDate;
                                entity.LastModifiedBy = new PersonEntity(loginName);
                                /*if (refactoredEntity != null) //TODO: New functionality
                                {
                                    refactoredEntity.LastModified = eventDate;
                                    refactoredEntity.LastModifiedBy = new PersonEntity(loginName);
                                }*/
                            }
                        }
                        
                    }

                    reader.Close();
                }
                LoggerHelper.Log.Info("Finished retrieving audit data for entities");

                connection.Close();
            }
        }

        private static
            EntityType ParseType(string type)
        {
            return DatabaseObjectTypes.ContainsKey(type) ? DatabaseObjectTypes[type] : EntityType.Unresolved;
        }
    }
}