
CREATE VIEW [dbo].[CR_IssuerRestriction] AS 
	SELECT 
		issuerId = h.childId, issuer = i.description, r.restrictionTypeId, restriction = t.description, r.startDate, r.endDate, r.lastUpdate,
		ii.code as bloombergCompanyId
	FROM 
		IssuerRestriction r 
			cross apply dbo.GetIssuerWithDescendants(r.issuerId) h 
			INNER JOIN RestrictionType t on 
				r.restrictionTypeId = t.restrictionTypeId 
			INNER JOIN Issuer i on 
				h.childId = i.issuerId 
			cross join IdentifierType_Transposed as it
			left outer join IssuerIdentifier as ii on
				ii.issuerId = h.childId and
				ii.identifierTypeId = it.[Bloomberg Company]
	WHERE 
		(r.issuerId = h.childId or r.includeSubsidiariesFlag = 1) AND	
		r.endDate >= GETDATE() AND r.startDate <= GETDATE() AND 
		t.rtlApplicableFlag = 1	AND 
		EXISTS (SELECT 1 FROM [Security] WHERE issuerId = h.childId AND issuerId = i.issuerId)


