CREATE VIEW [dbo].[Target_Transposed] AS
SELECT
[Security] = 1
, [Order] = 2
, [Issuer] = 3
, [Position] = 4
, [Manager] = 5
, [Legal Entity] = 6
, [Broker] = 7
, [Exchange] = 8
, [Tag] = 9
, [Broker Branch] = 10
, [Country] = 11
, [Business Unit] = 12
, [Module] = 13
, [Action] = 14
, [Security Primary Type] = 15
, [Execution Platform] = 16
, [Fund] = 17
, [Reporting Group] = 18
, [Strategy] = 19
, [SecurityType] = 20
, [SecuritySubType] = 21
, [Identifier Type] = 22
, [User] = 23
, [Grid View] = 24
, [Grid Calculation] = 25
, [Lookup Table] = 26


