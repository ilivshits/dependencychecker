
create view [bbg].[v_BloombergFileType_Pivot] as
	select
		 1 as [Out],
		 2 as Dif,
		 3 as Cax,
		 4 as Px,
		 5 as Hpc,
		 6 as Epx,
		 7 as Rpx,
		 8 as Dlt,
		 9 as Hpx,
		10 as Trr,
		11 as Hdc


