CREATE VIEW [tagging].[SystemList_Value] AS
SELECT [typeId] = 50001, [objectId] = [countryId], [display] = [description]
FROM [dbo].[Country] UNION ALL 
SELECT [typeId] = 50002, [objectId] = [userId], [display] = [fullName]
FROM [dbo].[v_Regulatory_144a_Approval_Users] UNION ALL 
SELECT [typeId] = 50003, [objectId] = [frequencyId], [display] = [description]
FROM [dbo].[Frequency] UNION ALL 
SELECT [typeId] = 50004, [objectId] = [tenorId], [display] = [code]
FROM [dbo].[Tenor] UNION ALL 
SELECT [typeId] = 50005, [objectId] = [userId], [display] = [fullName]
FROM [dbo].[User_CentaurusTrader] UNION ALL 
SELECT [typeId] = 50018, [objectId] = [brokerId], [display] = [code]
FROM [dbo].[Broker] UNION ALL 
SELECT [typeId] = 50023, [objectId] = [userId], [display] = [fullName]
FROM [dbo].[User_Enabled] UNION ALL 
SELECT [typeId] = 50027, [objectId] = [managerId], [display] = [description]
FROM [dbo].[Manager] UNION ALL 
SELECT [typeId] = 50032, [objectId] = [fundId], [display] = [description]
FROM [dbo].[Fund2] UNION ALL 
SELECT [typeId] = 50033, [objectId] = [summaryReportId], [display] = [description]
FROM [dbo].[SummaryReport] UNION ALL 
SELECT [typeId] = 51000, [objectId] = [securityPrimaryTypeId], [display] = [description]
FROM [dbo].[SecurityPrimaryType] UNION ALL 
SELECT [typeId] = 51001, [objectId] = [userId], [display] = [fullName]
FROM [dbo].[User] UNION ALL 
SELECT [typeId] = 51002, [objectId] = [userId], [display] = [fullName]
FROM [dbo].[User_Analyst] UNION ALL 
SELECT [typeId] = 51003, [objectId] = [sid], [display] = [description]
FROM [dbo].[Security_Currency] UNION ALL 
SELECT [typeId] = 51004, [objectId] = [executionPlatformId], [display] = [description]
FROM [dbo].[ExecutionPlatform] UNION ALL 
SELECT [typeId] = 51005, [objectId] = [legalEntityId], [display] = [description]
FROM [dbo].[LegalEntity] UNION ALL 
SELECT [typeId] = 51006, [objectId] = [securitySubTypeId], [display] = [description]
FROM [dbo].[SecuritySubType] UNION ALL 
SELECT [typeId] = 51007, [objectId] = [sid], [display] = [ticker]
FROM [dbo].[Security_FX]

