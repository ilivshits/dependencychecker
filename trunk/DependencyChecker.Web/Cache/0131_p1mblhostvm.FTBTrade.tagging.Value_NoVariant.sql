create view [tagging].[Value_NoVariant] as
select valueId
	, contextId
	, tagId
	, objectId
	, overrideFlag
	, error
	, startDate
	, endDate
	, created
	, version
	, lastUpdate
	, application_source
	, system_source
	, _globalVersion

	, value_Int = i.value_Int
	, value_Text = i.value_Text
	, value_DateTime = i.value_DateTime
	, value_Decimal = i.value_Decimal

from tagging.Value
cross apply tagging.GetNoVariant(value) i


