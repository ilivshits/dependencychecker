﻿-- =============================================
-- Author:		<Yury Zaryadov, yzaryadov>
-- Create date: <4/1/2014>
-- Description:	Returns pnl by asset and country for particular trader
-- Used By:		Imagine/Investor relations/Attribution report
-- =============================================
CREATE PROCEDURE [operations].[spGetPnLByAssetCountryForFund]
	@asofdate DATETIME
	,@fund VARCHAR(50)
	,@mode VARCHAR(10) = 'country'
AS
BEGIN
	SET NOCOUNT ON;

	--Remove comment this section to test the procedure---
	--
	--	DECLARE @asofdate DATETIME = '2013-08-30'
	--			,@fund VARCHAR(50) = '48'
	--			,@mode VARCHAR(10) = 'country'
	--
	---END TEST PARAMETER SECTION-----


	SET @fund = (
			SELECT DisplayName
			FROM CUBE1.Forensics.dbo.v_MetaFund
			WHERE fundId = @fund
			)

	DECLARE @dateStr VARCHAR(10);
	DECLARE @qry VARCHAR(max);

	IF (@asofdate IS NULL)
		SET @asofdate = dbo.LastBusinessDay();
	SET @dateStr = Convert(VARCHAR(10), @asofdate, 126);

	CREATE TABLE #pnl_bps (
		strategy VARCHAR(40)
		,tid VARCHAR(40)
		,level1 VARCHAR(40)
		,dtd FLOAT
		,wtd FLOAT
		,mtd FLOAT
		,ytd FLOAT
		);

	DECLARE @startDate VARCHAR(10);

	SET @startDate = CONVERT(VARCHAR, dateadd(d, - 6, @asofdate), 126);

	BEGIN TRY
		SET @qry = CASE @mode
				WHEN 'country'
					THEN 'insert into #pnl_bps(level1, dtd, [wtd], mtd, ytd)
				select * from openquery(CUBE1_OLAP, ''
				select {[Measures].[Aexeo Pnl], [Measures].[WTD_REAL], [Measures].[MTD_REAL], [Measures].[YTD_REAL]} on 0,
				nonempty(
					[Risk Country].[Country].[Country].Members
					, [Measures].[YTD_REAL]) on 1
				from [Pnl Data]
				where [Time].[Year -  Quarter -  Month -  Date].[Date].&[' + @dateStr + 'T00:00:00]
					*{[Funds].[Fund Name].&[' + @fund + ']}
					'')'
				WHEN 'asset'
					THEN 'insert into #pnl_bps(level1, dtd, [wtd], mtd, ytd)
				select * from openquery(CUBE1_OLAP, ''
				select {[Measures].[Aexeo Pnl], [Measures].[WTD_REAL], [Measures].[MTD_REAL], [Measures].[YTD_REAL]} on 0,
				nonempty([FIG Tags Link].[FIG Asset Class].[FIG Asset Class].Members, [Measures].[YTD_REAL]) on 1
				from [Pnl Data]
				where [Time].[Year -  Quarter -  Month -  Date].[Date].&[' + @dateStr + 'T00:00:00]*{[Funds].[Fund Name].&[' + @fund + ']}'')'
				END

		PRINT @qry;

		EXEC (@qry)
	END TRY

	BEGIN CATCH
		DECLARE @ErrorNum AS INT
			,@ErrorMessage NVARCHAR(4000)
			,@ErrorSeverity INT
			,@ErrorState INT
			,@ErrorProcedure NVARCHAR(2000)
			,@ErrorLine INT;

		SELECT @ErrorNum = ERROR_NUMBER()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorLine = ERROR_LINE()
			,@ErrorProcedure = ERROR_PROCEDURE()
			,@ErrorMessage = ERROR_MESSAGE()
			,@ErrorState = ERROR_STATE();

		IF (@ErrorNum NOT IN (213, 120)) -- Will be thrown when MDX returns empty result
		BEGIN
			RAISERROR (
					@ErrorMessage
					,@ErrorSeverity
					,1
					,@ErrorNum
					,@ErrorSeverity
					,@ErrorState
					,@ErrorProcedure
					,@ErrorLine
					)
		END
	END CATCH;

	SELECT COALESCE(level1, '') NAME
		,SUM([wtd]) AS [wtd]
		,SUM(mtd) AS mtd
		,SUM(ytd) AS ytd
	FROM #pnl_bps bps
	GROUP BY level1
	ORDER BY 4 DESC

	DROP TABLE #pnl_bps
END
