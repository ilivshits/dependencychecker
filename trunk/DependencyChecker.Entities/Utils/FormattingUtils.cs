﻿using System;

namespace DependencyChecker.Entities.Utils
{
    public static class FormattingUtils
    {
        public static String GetStyle(this EntityType entityType)
        {
            var type = entityType.GetType();
            var memInfo = type.GetMember(entityType.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(StyleAttribute),false);
            return ((StyleAttribute)attributes[0]).Name;
        }
    }
}
