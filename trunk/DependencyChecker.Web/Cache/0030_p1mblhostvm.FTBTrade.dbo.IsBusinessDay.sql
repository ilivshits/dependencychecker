
CREATE FUNCTION [dbo].[IsBusinessDay]
(
	@date datetime
)
RETURNS bit
AS
BEGIN
	DECLARE @weekday varchar(10);
	SET @weekday = DATENAME(dw, @date);
	IF (@weekday = 'Saturday' OR @weekday = 'Sunday') RETURN 0;
	IF EXISTS (SELECT 1 FROM SystemHolidays WHERE date = Convert(varchar(8),@date, 112)) RETURN 0;
	RETURN 1;
END



