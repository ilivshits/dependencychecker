﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using DependencyChecker.Common;
using DependencyChecker.DataProvider;
using DependencyChecker.DataProvider.DAL;
using DependencyChecker.DataProvider.Helpers;
using DependencyChecker.Entities.Utils;
using DependencyChecker.Web.Controllers;

namespace DependencyChecker.Web.Models
{
    /// <summary>
    /// Class with business logic helpers
    /// </summary>
    internal class DomainLogicContext
    {
        private int fileIndex = 0;

        public DomainLogicContext(string basePath, DateTime? serviceLastRun = null)
        {
            BasePath = basePath;
            ServiceLastRun = serviceLastRun;
        }

        protected String BasePath { get; }
        protected DateTime? ServiceLastRun { get; }

        public static void ClearCacheFolder()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Cache");
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
        }

        private List<string> WriteDependsOnTree(Guid id, Dictionary<string, List<string>> depAdditionalInfo, Dictionary<string, string> readmeInfo)
        {
            List<string> filePaths = new List<string>();

            var dependents = DatabaseHelpers.GetDependsOnRecursively(id).Keys.ToList();

            List<EntityBase> items;
            using (var ctx = DatabaseHelpers.GetContext())
            {
                items = ctx.Set<EntityBase>().Where(e => dependents.Any(c => c == e.Id)).ToList();
            }

            foreach (var item in items)
            {

                string fileExt = ".txt";
                bool toAdditionals = false;
                string path = String.Empty;
                string type = ((Entities.EntityType) item.Type).ToString();
                switch (type)
                {
                    case "PerlScript":
                        fileExt = ".pl";
                        break;
                    case "BatchFile":

                        fileExt = "";
                        break;
                    case "View":
                    case "StoredProcedure":
                    case "Function":
                        fileExt = ".sql";
                        break;
                    case "ScheduledTask":
                    case "Report":
                        fileExt = ".xml";
                        break;
                    case "Synonym":
                    case "Table":
                    case "Schema":
                    case "Executable":
                    case "Folder":
                    case "Database":
                    case "Server":
                    default:
                        fileExt = ".txt";
                        toAdditionals = true;
                        break;
                }


                if (toAdditionals)
                {
                    if (!depAdditionalInfo.ContainsKey(type))
                        depAdditionalInfo.Add(type, new List<string>());
                    switch (type)
                    {
                        case "Synonym":
                            depAdditionalInfo[type].Add(item.Definition);
                            break;
                        default:
                            depAdditionalInfo[type].Add(item.FullName);
                            break;
                    }
                }
                else
                {
                    var st = item.FullName.Split(new[] { ':', '.', '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);

                    path = Path.Combine(BasePath);
                    string name = "";
                    for (int i = st.Length - Math.Min(st.Length, 4); i < st.Length; i++)
                    {
                        name += st[i] + ".";
                    }
                    name = $"{fileIndex++.ToString("0000")}_{name.Substring(0, name.Length - 1)}{fileExt}";
                    path = Path.Combine(BasePath, name).Replace(' ', '_');

                    readmeInfo.Add(name, item.FullName);


                    WriteToFile(path, item.Definition);
                }

                if (!String.IsNullOrEmpty(path)) filePaths.Add(path);
            }

            return filePaths;
        }

        private List<string> WriteDependsOnTree(DependencyCheckerServiceProxy client, Entity entity, Dictionary<string, List<string>> depAdditionalInfo, Dictionary<string, string> readmeInfo, List<string> filePaths = null)
        {
            if (filePaths == null) filePaths = new List<string>();
            string fileExt = ".txt";
            bool toAdditionals = false;
            string path = String.Empty;
            switch (entity.Type)
            {
                case "PerlScript":
                    fileExt = ".pl";
                    break;
                case "BatchFile":
                
                    fileExt = "";
                    break;
                case "View":
                case "StoredProcedure":
                case "Function":
                    fileExt = ".sql";
                    break;
                case "ScheduledTask":
                case "Report":
                    fileExt = ".xml";
                    break;
                case "Synonym":
                case "Table":
                case "Schema":
                case "Executable":
                case "Folder":
                    fileExt = ".txt";
                    toAdditionals = true;
                    break;
            }


            if (toAdditionals)
            {
                if (!depAdditionalInfo.ContainsKey(entity.Type))
                    depAdditionalInfo.Add(entity.Type, new List<string>());
                switch (entity.Type)
                {
                    case "Synonym":
                        depAdditionalInfo[entity.Type].Add(client.PrintEntityDefinition(entity.Id));
                        break;
                    default:
                        depAdditionalInfo[entity.Type].Add(entity.Name);
                        break;
                }
            }
            else
            {
                var st = entity.Name.Split(new[] {':', '.', '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);

                path = Path.Combine(BasePath);
                string name = "";
                for (int i = st.Length - Math.Min(st.Length, 4); i < st.Length; i++)
                {
                    name += st[i] + ".";
                }
                name = fileIndex++.ToString("0000") + name.Substring(0, name.Length - 1) + fileExt;
                path = Path.Combine(BasePath, name).Replace(' ', '_');

                readmeInfo.Add(name, entity.Name);

                
                WriteToFile(path, client.PrintEntityDefinition(entity.Id));
            }

            if (!String.IsNullOrEmpty(path)) filePaths.Add(path);

            foreach (var depOn in entity.DependsOn)
                WriteDependsOnTree(client, depOn, depAdditionalInfo, readmeInfo, filePaths);

            return filePaths;
        }

        private void WriteToFile(string path, string content)
        {
            var file = new FileInfo(path);
            bool fileNotExistsOrOld = !file.Exists || ServiceLastRun > File.GetLastWriteTime(path);
            
            if (ServiceLastRun == null || (ServiceLastRun != null && fileNotExistsOrOld))
            using (var writer = file.CreateText())
            {
                writer.WriteLine(content);
            }
            
        }

        private string WriteAdditionalInfo(Dictionary<string, List<string>> depAdditionalInfo, EntityBase entity)
        {
            if (depAdditionalInfo.Count != 0)
            {
                var dict = depAdditionalInfo.OrderBy(x => x.Key).ToDictionary(t => t.Key, t => t.Value);
                var sb = new StringBuilder();
                foreach (var item in dict)
                {
                    item.Value.Sort();
                    foreach (var listItem in item.Value)
                    {
                        sb.AppendFormat("{0}: {1}", item.Key, listItem);
                        sb.AppendLine();
                    }
                }
                string infoPath = "";
                var sst = entity.FullName.Split(new[] { ':', '.', '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = sst.Length - Math.Min(sst.Length, 4); i < sst.Length; i++)
                {
                    infoPath += sst[i] + ".";
                }
                infoPath = Path.Combine(BasePath, infoPath.Substring(0, infoPath.Length - 1).Replace(' ', '_') + ".additional.txt");
                
                WriteToFile(infoPath, sb.ToString());
                return infoPath;
            }
            return String.Empty;
        }

        private string WriteReadmeInfo(Dictionary<string, string> readmeInfo, EntityBase entity)
        {
            if (readmeInfo.Count != 0)
            {
               

                var dict = readmeInfo.OrderBy(x => x.Key).ToDictionary(t => t.Key, t => t.Value);
                var sb = new StringBuilder();
                foreach (var item in dict)
                {
                    sb.AppendFormat("{0};        Full path: {1}", item.Key, item.Value);
                    sb.AppendLine();
                }
                string infoPath = "";
                var sst = entity.FullName.Split(new[] { ':', '.', '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = sst.Length - Math.Min(sst.Length, 4); i < sst.Length; i++)
                {
                    infoPath += sst[i] + ".";
                }
                infoPath = Path.Combine(BasePath, infoPath.Substring(0, infoPath.Length - 1).Replace(' ', '_') + ".readme.txt");

                WriteToFile(infoPath, sb.ToString());
                return infoPath;
            }
            return String.Empty;
        }

        public byte[] GetDependsOnArchive(Guid id)
        {
            var depAdditionalInfo = new Dictionary<string, List<string>>();
            var readmeInfo = new Dictionary<string, string>();

            EntityBase entity;
            using (var ctx = DatabaseHelpers.GetContext())
            {
                entity = ctx.Set<EntityBase>().Single(e => e.Id == id);
            }


            var files = WriteDependsOnTree(id, depAdditionalInfo, readmeInfo);

            string aInfoPath = WriteAdditionalInfo(depAdditionalInfo, entity);
            if (!String.IsNullOrEmpty(aInfoPath)) files.Add(aInfoPath);

            string rInfoPath = WriteReadmeInfo(readmeInfo, entity);
            if (!String.IsNullOrEmpty(rInfoPath)) files.Add(rInfoPath);


            var st = entity.FullName.Split(new[] { ':', '.', '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
            string fileName = "";
           
            for (int i = st.Length - Math.Min(st.Length, 4); i < st.Length; i++)
            {
                fileName += st[i] + ".";
            }
            fileName = fileName.Substring(0, fileName.Length - 1).Replace(' ', '_');
            //string fileName = Path.GetFileName(entity.Name);
            return GeneratePackage(entity.FullName, files, fileName, true);
        }

        private byte[] GeneratePackage(string name, List<string> fileNames, string folderName = "", bool shortPath = false)
        {
            string zipPath = Path.Combine(BasePath, folderName.Replace('\\', '_').Replace('/', '_') + ".zip");
            using (FileStream zipToOpen = new FileStream(zipPath, FileMode.Create))
            {
                using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                {
                    foreach (var fileName in fileNames)
                    {
                        string tName = shortPath ? Path.GetFileName(fileName) : fileName;
                        archive.CreateEntryFromFile(fileName, Path.Combine(folderName, tName));
                    }
                }
            }

            return File.ReadAllBytes(zipPath);
        }
    }
}
