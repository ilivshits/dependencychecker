

CREATE PROCEDURE [fortress].[Index_Add]
	--- Index-specific.
	@dividendCurveId int,
	@fundingCurveId int,
	@volSurfaceId int,
	@otcVolSurfaceId int,	

	--- Security.
	@ticker	varchar(50),
	@description varchar(150),
	@currencySid bigint,
	@exchangeId int,
	@securityPrimaryTypeId int,
	@securitySubTypeId int,	
	@startDate datetime,
	@endDate datetime,
	@priceMultiplier float,
	@tradableFlag bit,
	@priceableFlag bit,
	@primaryFlag bit,
	@underlying_sid	bigint,
	@issuerId int,
	@lotSizeInitial float,
	@lotSizeIncrement float,
	@alternateDescription varchar(150),
		
	--- Identifier types.
	@idBloombergCodeShort varchar(100),
	@idBloombergCodeLong varchar(100),
	@idCusip varchar(100),
	@idSedol varchar(100),
	@idIsin varchar(100),
	@idRed6 varchar(100),
	@idRed9 varchar(100),
	@idTradeId varchar(100),
	@idBloombergCdsTicker5Y varchar(100),
	@idBloombergGlobal varchar(100),
	@idBloombergUnique varchar(100),
	@idBelgium varchar(100),
	@idCedelEuroclear varchar(100),
	@idDutch varchar(100),
	@idFrench varchar(100),
	@idValoren varchar(100),
	@idWertpapier varchar(100),
	@idOcc varchar(100),
	@idOpra varchar(100),
	@idBloombergTickerAndExchange varchar(100),
	@idAustrian varchar(100),
	@idCedel varchar(100),
	@idDanish varchar(100),
	@idEuroclear varchar(100),
	@idItaly varchar(100),
	@idJapan varchar(100),
	@idLuxembourg varchar(100),
	@idNorway varchar(100),
	@idSpain varchar(100),
	@idSwedish varchar(100),
	@idXtrakter varchar(100),
	
	--- Restrictions.
	@restriction144A bit,
	@restrictionRegS bit,

	--- Auditing.
	@application_source varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- XACT_ABORT is used to abort the transaction in the face of
	-- anything.
	SET XACT_ABORT, NOCOUNT ON;
	
	--- Get the sid.
	declare @sid bigint
	exec [FTBTrade].[dbo_SequenceGenerator_GetNextSidBySecurityPrimaryType] @securityPrimaryTypeId, @sid output
	
	--- Wrap.
	begin try
		--- Begin the transaction.
		begin transaction

		--- Begin work.

		--- Add the records for the base security first.
		exec fortress.Security_Add @sid, @ticker, @description, @currencySid, @exchangeId, 
			@securityPrimaryTypeId, @securitySubTypeId, @startDate, @endDate, @priceMultiplier, 
			@tradableFlag, @priceableFlag, @primaryFlag, @underlying_sid, @issuerId, @lotSizeInitial, @lotSizeIncrement,
			@idBloombergCodeShort, @idBloombergCodeLong, @idCusip, @idSedol, @idIsin, @idRed6, @idRed9, @idTradeId, 
			@idBloombergCdsTicker5Y, @idBloombergGlobal, @idBloombergUnique, @idBelgium, @idCedelEuroclear, @idDutch, 
			@idFrench, @idValoren, @idWertpapier, @idOcc, @idOpra, @idBloombergTickerAndExchange, 
			@idAustrian, @idCedel, @idDanish, @idEuroclear, @idItaly, @idJapan, @idLuxembourg, @idNorway, @idSpain, 
			@idSwedish, @idXtrakter, @restriction144A, @restrictionRegS,
			@application_source, @alternateDescription
			
		--- Insert the record.
		insert into [FTBTrade].[dbo_Index] (
			sid, dividendCurveId, fundingCurveId, volSurfaceId, otcVolSurfaceId, application_source
		) values (
			@sid, @dividendCurveId, @fundingCurveId, @volSurfaceId, @otcVolSurfaceId, @application_source 
		)
		
		--- Return the sid.
		select @sid
		
		--- End work.

		--- Commit the transaction.
		commit transaction
	end try
	begin catch
		--- Store the error message, etc.
		declare @errMessage nvarchar(max) = error_message()
		declare @errSeverity int = error_severity()
		declare @errState int = error_state()

		--- Rollback if in a transaction state.
		--- Possible if something happened between begin try and 
		--- begin transaction.
		if (xact_state() <> 0) rollback transaction

		--- Raise the error again.
		raiserror(@errMessage, @errSeverity, @errState)
	end catch
END



