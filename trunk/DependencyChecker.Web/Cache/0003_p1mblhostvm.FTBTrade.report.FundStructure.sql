create view [report].[FundStructure] as
select fundId
      , legalEntityId
      , startDate = cast('1900-01-01' as date)
      , endDate = cast('9999-12-31' as date)
      , percentage = 1.0
from LegalEntity


