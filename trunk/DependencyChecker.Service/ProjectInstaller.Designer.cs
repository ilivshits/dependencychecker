﻿namespace DependencyChecker.Service
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DCProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.DCServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // DCProcessInstaller
            // 
            this.DCProcessInstaller.Password = null;
            this.DCProcessInstaller.Username = null;
            // 
            // DCServiceInstaller
            // 
            this.DCServiceInstaller.Description = "Dependency checker service";
            this.DCServiceInstaller.DisplayName = "Dependency checker";
            this.DCServiceInstaller.ServiceName = "DependencyWindowsService";
            this.DCServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.DCProcessInstaller,
            this.DCServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller DCProcessInstaller;
        private System.ServiceProcess.ServiceInstaller DCServiceInstaller;
    }
}