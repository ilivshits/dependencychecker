CREATE view [dbo].[User_Analyst] as
select
	u.userId, u.fullName
from
	[User] as u
		inner join tagging.LookupTable as lt on
			lt.name = 'Analysts'
		inner join tagging.LookupTableRow as ltr on
			ltr.lookupTableId = lt.lookupTableId and
			ltr.inputValue = u.userId
		inner join tagging.LookupTableColumn as ltc on
			ltc.lookupTableId = lt.lookupTableId and
			ltc.perspective = 'Enabled'
		inner join tagging.LookupTableCell as cell on
			cell.lookupTableRowId = ltr.lookupTableRowId and
			cell.lookupTableColumnId = ltc.lookupTableColumnId
where
	cell.value_Int != 0


