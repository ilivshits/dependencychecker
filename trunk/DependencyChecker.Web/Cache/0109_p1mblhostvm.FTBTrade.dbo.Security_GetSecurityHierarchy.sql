CREATE FUNCTION [dbo].[Security_GetSecurityHierarchy]
(
	@sids dbo.Int64List readonly
)
RETURNS TABLE 
AS
RETURN 
(
	--- Set up securities as well as chain of underliers.	
	with ExpandedSecurities as
	(
		select
			b.sid as baseSid, s.sid, s.securityPrimaryTypeId, s.underlying_sid
		from
			[dbo].[Security] as b with (nolock)
				cross apply (
					--- Get the original sid.
					select 
						b.sid
					union
					--- Credit underlier.
					select
						cu.obligationSid as sid
					from
						[dbo].[CreditUnderlying] as cu with (nolock)
					where
						cu.sid = b.sid and
						cu.obligationSid != 0
					--- ADR if needed.
				) as r
					inner join [dbo].[Security] as s with (nolock) on
						r.sid = s.sid
	), Hierarchy as
	(
		--- Base case, anything in position.
		select distinct
			s.baseSid, s.sid, s.underlying_sid, s.securityPrimaryTypeId
		from
			ExpandedSecurities as s
		where
			exists (select * from @sids as sids where sids.value = s.baseSid)
		union all
		select
			s.baseSid, s.sid, s.underlying_sid, s.securityPrimaryTypeId
		from
			Hierarchy as h
				inner join ExpandedSecurities as s with (nolock) on
					s.baseSid = h.underlying_sid
		where
			h.underlying_sid != 0
	)
	select distinct
		h.sid, h.securityPrimaryTypeId, h.underlying_sid
	from
		Hierarchy as h
)

