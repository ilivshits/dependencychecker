﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.DirectoryServices;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using DependencyChecker.Collector.Base;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Common;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Interfaces;
using DependencyChecker.Entities.Utils;
using Microsoft.AnalysisServices;
using Microsoft.SqlServer.Dts.Runtime;
using Microsoft.Win32.TaskScheduler;


namespace DependencyChecker.Collector
{
    public class ScheduledTasksDataProvider : BaseDataProvider
    {
        public ScheduledTasksDataProvider(IServerProvider serverProvider)
            : base(serverProvider)
        {
        }

        #region Overrides of BaseDataProvider

        public override string Name
        {
            get { return ScheduledTaskEntity.ProviderName; }
        }

        protected override List<Regex> ItemSearchPattern
        {
            get { throw new NotImplementedException(); }
        }

        public override List<IEntity> Search()
        {
            return ServerProvider.GetServers(EntityProviderType.ScheduledTask).Select(LoadServer).ToList();
        }

        #endregion

        public IEntity LoadServer(string serverName = "")
        {
            
            var tasksList = new List<IEntity>();
            var server = new ScheduledTaskEntity(serverName, EntityType.Server)
            {
                ScopeChildren = tasksList
            };
            var scheduler = new TaskService(serverName);

            var tasks = scheduler.GetFolder("\\").GetTasks();

            try
            {
                foreach (var task in tasks)
                {
                    var taskDefinition = task.Definition;
                    var result = new ScheduledTaskEntity(task.Name, taskDefinition.XmlText, EntityType.ScheduledTask)
                    {
                        Scope = server,
                        IsEnabled = task.Enabled,
                        Principal = task.Definition.Principal.LogonType == TaskLogonType.Group ? 
                                        task.Definition.Principal.GroupId : 
                                        task.Definition.Principal.UserId,
                        Created = taskDefinition.RegistrationInfo.Date,
                        CreatedBy = new PersonEntity(taskDefinition.RegistrationInfo.Author),
                                            };
                    if (task.LastRunTime != DateTime.Parse("30.11.1999 0:00:00"))
                    {
                        result.TaskLastRunTime = task.LastRunTime;
                    }

                    var scripts = new List<IEntity>(task.Definition.Actions
                        .Where(a => a.ActionType == TaskActionType.Execute)
                        .Select(a => BuildScheduledEntity(serverName, result, GetExecutableName(a.ToString(),
                            ((ExecAction) a).WorkingDirectory))).ToList());

                    LoggerHelper.Log.DebugFormat("Indexing task '{0}' with {1} exec steps:\r\n{2}", task.Name, scripts.Count, String.Join("\r\n", task.Definition.Actions
                        .Where(a => a.ActionType == TaskActionType.Execute)
                        .Select(a => a.ToString() + "|| IN:" + ((ExecAction)a).WorkingDirectory).ToArray()));

                    foreach (var script in scripts)
                    {
                        result.AddDependency(script, script.Name);
                    }

                    //result.ScopeChildren = task.Definition.Actions
                    //    .Count(a => a.ActionType == TaskActionType.Execute &&
                    //                (a.ToString().Contains(".bat") ||
                    //                 a.ToString().Contains(".pl") ||
                    //                 a.ToString().Contains(".exe") ||
                    //                 a.ToString().Contains(".cmd"))) > 0
                    //    ? new List<IEntity>(task.Definition.Actions
                    //        .Where(a => a.ActionType == TaskActionType.Execute)
                    //        .Select(a => BuildScheduledEntity(serverName, result, GetExecutableName(a.ToString(), 
                    //            ((ExecAction)a).WorkingDirectory))).ToList())
                    //    : null;
                    tasksList.Add(result);
                }
            }
            catch (FileNotFoundException exception)
            {
                LoggerHelper.Log.WarnFormat("Cannot get tasks for the server '{0}'. Exception details: file '{1}' not found. Exception {2}",
                    serverName,
                    exception.FileName,
                    exception);
            }
            return server;
        }

        public String GetDTSPackageSource(string server, string path)
        {
            Package package;
            var application = new Application();
            
            package = application.LoadPackage(FileUtils.BuildRemotePath(server, path), null);
            foreach (var exec in package.Executables)
            {
                var task = exec as TaskHost;
                if (task != null)
                {
                    try
                    {
                        Debug.WriteLine(task.Properties["SqlStatementSource"].GetValue(task));
                    }
                    catch (DtsRuntimeException ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }

                    try
                    {
                        Debug.WriteLine(task.Properties["OpenRowset"].GetValue(task));
                    }
                    catch (DtsRuntimeException ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }

                    foreach (var property in task.Properties)
                    {
                        if (property.Get)
                        {
                            Debug.WriteLine("[{0}] = {1}", property.Name, property.GetValue(task));
                        }
                    }
                }
            }

            string result;
            package.SaveToXML(out result, new DefaultEvents());
            return result;
        }

        public IEntity BuildScheduledEntity(string server, IEntity parent, string execPath)
        {
            var perlUsage = new Regex(@"require[ \t]+'(?<path>[^']+)'", RegexOptions.IgnoreCase);
            var batchPerlCall = new Regex(@"system[ \t]*\([ \t]*""(?<path>[^""]+)""[ \t]*\)", RegexOptions.IgnoreCase);
            var dtexecUsage = new Regex(@"\\\\dtexec(\.exe)?("")?([ \t\r\n]+\/[a-z])*[ \t\r\n]+(?<path>[\\a-z0-9\._]+\.dtsx)", RegexOptions.IgnoreCase);
            var batchCall = new Regex(@"((call|(cmd|perl)(\.exe)?)[ \t]+)?([ \t]+)?(?<path>[a-z0-9_\-\.\\:]+\.(pl|exe|bat))([ \t]+)?", RegexOptions.IgnoreCase | RegexOptions.Multiline);

            LoggerHelper.Log.DebugFormat("Parsing file '{0}' on server {1}...", execPath, server);

            try
            {
                

                var extention = Path.GetExtension(execPath);
                EntityType type;

                

                switch (extention)
                {
                    case ".exe":
                        var executableResult = new ScheduledTaskEntity(execPath, PullFileContents(server, execPath), EntityType.Executable)
                        {
                            Scope = parent
                        };

                        executableResult.Created = File.GetCreationTime(FileUtils.BuildRemotePath(server, execPath));
                        executableResult.LastModified = File.GetLastWriteTime(FileUtils.BuildRemotePath(server, execPath));
                        FileSecurity exeFs = File.GetAccessControl(FileUtils.BuildRemotePath(server, execPath));
                        executableResult.CreatedBy = new PersonEntity(exeFs.GetOwner(typeof(System.Security.Principal.NTAccount)).Value);

                        

                        //var configPath = FileUtils.BuildRemotePath(server, execPath + ".config");
                        //if (File.Exists(configPath))
                        //{
                        //    var config = ConfigurationManager.OpenExeConfiguration(configPath);

                        //    //config.ConnectionStrings

                        //    var existingScript = parent.Root.ScopeChildren.SingleOrDefault(s =>
                        //        String.Compare(s.Name, referencePath, StringComparison.InvariantCultureIgnoreCase) == 0 &&
                        //        s.Type == EntityType.PerlScript) ?? BuildScheduledEntity(server, parent, referencePath);

                        //    if (existingScript != null)
                        //    {
                        //        executableResult.AddDependency(existingScript, new EntityMatch
                        //        {
                        //            Indexes = new List<int> { usage.Groups["path"].Index },
                        //            MatchText = usage.Groups["path"].Value
                        //        });
                        //    }
                        //}

                        return executableResult;
                    case ".dtsx":
                        var dtsContents = PullFileContents(server, execPath);
                        var dtsResult = new DTSEntity(execPath, dtsContents)
                        {
                            Scope = parent.Root
                        };
                        return dtsResult;
                    case ".pl":
                        var perlContents = PullFileContents(server, execPath);
                        var result = new PerlScriptEntity(execPath, perlContents)
                        {
                            Scope = parent.Root,
                        };

                        result.Created = File.GetCreationTime(FileUtils.BuildRemotePath(server, execPath));
                        result.LastModified = File.GetLastWriteTime(FileUtils.BuildRemotePath(server, execPath));
                        FileSecurity fs = File.GetAccessControl(FileUtils.BuildRemotePath(server, execPath));
                        result.CreatedBy = new PersonEntity(fs.GetOwner(typeof(System.Security.Principal.NTAccount)).Value);
                        perlContents = Regex.Replace(perlContents, @"#.*$", String.Empty,
                            RegexOptions.IgnoreCase | RegexOptions.Multiline);

                        foreach (var content in result.SearchContent.Where(c => c.ContentType == EntityProviderType.SQL))
                        {
                            LoggerHelper.Log.DebugFormat("Perl script ({0} on {2}) SQL dependency detected:\r\n{1}", execPath, content.Text, server);
                        }

                        var addedFiles = new List<String>();
                        foreach (var usage in perlUsage.Matches(perlContents).Cast<Match>().Where(m => m.Groups["path"].Success))
                        {
                            if (addedFiles.Contains(usage.Groups["path"].Value)) continue;
                            addedFiles.Add(usage.Groups["path"].Value);

                            var referencePath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(execPath), usage.Groups["path"].Value));
                            var existingScript = parent.Root.ScopeChildren.SingleOrDefault(s =>
                                        String.Compare(s.Name, referencePath, StringComparison.InvariantCultureIgnoreCase) == 0 &&
                                        s.Type == EntityType.PerlScript) ?? BuildScheduledEntity(server, parent, referencePath);

                            if (existingScript != null)
                            {
                                result.AddDependency(existingScript, new EntityMatch
                                {
                                    Indexes = new List<int> { usage.Groups["path"].Index },
                                    MatchText = usage.Groups["path"].Value
                                });
                            }
                        }

                        foreach (var usage in batchPerlCall.Matches(perlContents).Cast<Match>().Where(m => m.Groups["path"].Success))
                        {
                            if (addedFiles.Contains(usage.Groups["path"].Value)) continue;
                            addedFiles.Add(usage.Groups["path"].Value);

                            var referencePath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(execPath), usage.Groups["path"].Value));
                            var existingScript = parent.Root.ScopeChildren.SingleOrDefault(s =>
                                        String.Compare(s.Name, referencePath, StringComparison.InvariantCultureIgnoreCase) == 0) ??
                                        BuildScheduledEntity(server, parent, referencePath);

                            if (existingScript != null)
                            {
                                result.AddDependency(existingScript, new EntityMatch
                                {
                                    Indexes = new List<int> { usage.Groups["path"].Index },
                                    MatchText = usage.Groups["path"].Value
                                });
                            }
                        }

                        foreach (var usage in dtexecUsage.Matches(perlContents).Cast<Match>().Where(m => m.Groups["path"].Success))
                        {
                            if (addedFiles.Contains(usage.Groups["path"].Value)) continue;
                            addedFiles.Add(usage.Groups["path"].Value);

                            var referencePath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(execPath), usage.Groups["path"].Value));

                            var existingScript = parent.Root.ScopeChildren.SingleOrDefault(s =>
                                        String.Compare(s.Name, referencePath, StringComparison.InvariantCultureIgnoreCase) == 0 &&
                                        s.Type == EntityType.DTSX) ?? BuildScheduledEntity(server, parent, referencePath);

                            if (existingScript != null)
                            {
                                result.AddDependency(existingScript, new EntityMatch
                                {
                                    Indexes = new List<int> { usage.Groups["path"].Index },
                                    MatchText = usage.Groups["path"].Value
                                });
                            }
                        }

                        return result;
                    case ".bat":
                    case ".cmd":
                        var batchContents = PullFileContents(server, execPath);
                        var batchEntity = new ScheduledTaskEntity(execPath, batchContents, EntityType.BatchFile)
                        {
                            Scope = parent.Root
                        };

                        batchEntity.Created = File.GetCreationTime(FileUtils.BuildRemotePath(server, execPath));
                        batchEntity.LastModified = File.GetLastWriteTime(FileUtils.BuildRemotePath(server, execPath));
                        FileSecurity batFs = File.GetAccessControl(FileUtils.BuildRemotePath(server, execPath));
                        batchEntity.CreatedBy = new PersonEntity(batFs.GetOwner(typeof(System.Security.Principal.NTAccount)).Value);

                        batchContents = Regex.Replace(batchContents, @"REM .*$", String.Empty,
                            RegexOptions.IgnoreCase | RegexOptions.Multiline);


                        LoggerHelper.Log.DebugFormat("Batch {1} contents:\r\n'{0}'", batchContents, execPath);

                        var added = new List<String>();
                        foreach (var usage in batchCall.Matches(batchContents).Cast<Match>().Where(m => m.Groups["path"].Success))
                        {
                            if (added.Contains(usage.Groups["path"].Value)) continue;
                            added.Add(usage.Groups["path"].Value);

                            var referencePath = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(execPath), usage.Groups["path"].Value));

                            LoggerHelper.Log.DebugFormat("Referenced command found '{0}' in '{1}'", referencePath, execPath);

                            var existingScript = parent.Root.ScopeChildren.FirstOrDefault(s =>
                                        String.Compare(s.Name, referencePath, StringComparison.InvariantCultureIgnoreCase) == 0) ??
                                        BuildScheduledEntity(server, parent, referencePath);

                            if (existingScript != null)
                            {
                                batchEntity.AddDependency(existingScript, new EntityMatch
                                {
                                    Indexes = new List<int> { usage.Groups["path"].Index },
                                    MatchText = usage.Groups["path"].Value
                                });
                            }
                        }

                        return batchEntity;
                    default:
                        type = EntityType.Unresolved;
                        break;
                }

                return new ScheduledTaskEntity(execPath, PullFileContents(server, execPath), type)
                {
                    Scope = parent
                };
            }
            catch (Exception exception)
            {
                LoggerHelper.Log.ErrorFormat("Error occured while building entity for path '{0}' on server '{1}'. Details: {2}", 
                    execPath, server, exception);
                return null;
            }
        }

        public static String PullFileContents(string server, string execPath)
        {
            var result = String.Empty;
            try
            {
                result = File.ReadAllText(FileUtils.BuildRemotePath(server, execPath));
            }
            catch (Exception e)
            {
                LoggerHelper.Log.WarnFormat("Cannot access path '{0}' on the server '{1}'. Details: {2}", execPath, server, e); 
            }
            
            return result;
        }

        public static String GetExecutableName(string path, string workingDirectory = "")
        {
            //INFO: IDK what was in that reg D: Just didn't get it. But anyway this regex couldn't handle even one sight of white spaces, so I needed to rewrite this one

            /*var execPath = Regex.Match(path,
                                @"((^(cmd|perl)(\.exe)?([ \t]+(\/|\-)[a-z])*[ \t]+)?(?<name>(""[^ \t]+\.[a-z]+""|[^ \t]+\.[a-z]+))|(?<name>^[a-z0-9\\\: ]+\.(exe|bat|cmd))([ \t]+(\/\-)[a-z])*)( |$)",
                                RegexOptions.IgnoreCase).Groups["name"].Value.Trim(new[] { '"' });

            if (!execPath.Contains(":\\") && 
                !execPath.StartsWith(@"\\") && 
                !String.IsNullOrEmpty(workingDirectory))
            {
                execPath = Path.Combine(workingDirectory, execPath);
            }

            return execPath;*/


            var execPath = Regex.Match(path,
                @"(([a-z]+\:)|(\\)|([a-z0-9]+))(\\[a-z0-9() _-]+)*\\[a-z0-9 \.()_-]+\.[a-z]+",
                RegexOptions.IgnoreCase).Value;

            if (!execPath.Contains(":\\") &&
                !execPath.StartsWith(@"\\") &&
                !String.IsNullOrEmpty(workingDirectory))
            {
                execPath = Path.Combine(workingDirectory, execPath);
            }

            return execPath;
        }
    }
}
