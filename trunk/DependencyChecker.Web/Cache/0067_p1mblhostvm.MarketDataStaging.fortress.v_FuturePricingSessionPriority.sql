
create view [fortress].[v_FuturePricingSessionPriority] as
	select 'P' as FuturePricingSession, 1 as Priority union
	select 'S' as FuturePricingSession, 2 as Priority union
	select 'C' as FuturePricingSession, 3 as Priority union
	select 'E' as FuturePricingSession, 4 as Priority union
	select ' ' as FuturePricingSession, 5 as Priority


