

CREATE PROCEDURE [FTBTrade].[dbo_SequenceGenerator_GetNextSidBySecurityPrimaryType]
	@securityPrimaryTypeId int,
	@id bigint output,
	@count int = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--- Get the sequence name.
	declare @sequenceName varchar(100) = 
		(select sequenceGenerator from FTBTrade.dbo_SecurityPrimaryType as spt with (nolock) where spt.securityPrimaryTypeId = @securityPrimaryTypeId)

	--- Execute the generator and return.
	exec [FTBTrade].[dbo_SequenceGenerator_GetNextIds] @sequenceName, @id output, @count
END



