CREATE view [dbo].[User_CentaurusTrader] as
select
	u.userId, u.fullName
from
	tagging.LookupTable as lt
		inner join tagging.LookupTableRow as ltr on
			ltr.lookupTableId = lt.lookupTableId
		inner join tagging.LookupTableColumn as ltcol on
			ltcol.lookupTableId = lt.lookupTableId
		inner join tagging.LookupTableCell as ltc on
			ltc.lookupTableRowId = ltr.lookupTableRowId and
			ltc.lookupTableColumnId = ltcol.lookupTableColumnId
		inner join [User] as u on
			u.userId = ltr.inputValue
where
	lt.name = 'Centaurus Traders' and
	ltc.value_Int != 0


