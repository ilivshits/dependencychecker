﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using DependencyChecker.Common;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.DataProvider.Helpers
{
    public static class MapperManager<TDestination>
    {
        public static TDestination Map(object source)
        {
            return Mapper.Map<TDestination>(source);
        }

        static MapperManager()
        {
            InitializeMapper();
        }

        public static void InitializeMapper()
        {
            Mapper.Initialize(cfg =>
            {
                //cfg.RecognizePrefixes("Entity");
                var map = cfg.CreateMap<EntityBase, Entity>();
                map.ForAllMembers(e => e.Ignore());
                map.Include<ReportEntity, Entity>();
                map.Include<ScheduledTaskEntity, Entity>();
                map.Include<DatabaseEntity, Entity>();
                map.Include<PerlScriptEntity, Entity>();
                //-----------
                map.ForMember(dest => dest.Name,
                    opts =>
                        opts.MapFrom(
                            src => ParseComplexName(src.FullName)))
                //----------------
                .ForMember(dest => dest.Type,
                    opts =>
                        opts.MapFrom(
                            src => ((Entities.EntityType)src.Type).ToString()))
                //-----------

                .ForMember(dest => dest.Created,
                    opts =>
                        opts.ResolveUsing(src => src.Created?.ToString()))
                .ForMember(dest => dest.LastModified,
                    opts =>
                        opts.ResolveUsing(src => src.LastModified?.ToString()))
                .ForMember(dest => dest.CreatedBy,
                    opts =>
                        opts.ResolveUsing(src => src.PersonEntity?.FullName))
                .ForMember(dest => dest.LastModifiedBy,
                    opts =>
                        opts.ResolveUsing(src => src.PersonEntity1?.FullName))
                ;
                cfg.CreateMap<ReportEntity, Entity>();
                cfg.CreateMap<PerlScriptEntity, Entity>();
                cfg.CreateMap<ScheduledTaskEntity, Entity>()
                .ForMember(dest => dest.LastRun, opts => opts.ResolveUsing(src => (src.TaskLastRunTime?.ToString())));
                cfg.CreateMap<DatabaseEntity, Entity>()
                .ForMember(dest => dest.LastRun, opts => opts.ResolveUsing(src => (src.JobLastRunTime?.ToString())))
                .ForMember(dest => dest.Database, opts => opts.ResolveUsing(src => (src.Database?.ToString())));

                //////////////////////////////

                var iEntityMap = cfg.CreateMap<IEntity, EntityBase>();
                iEntityMap.ForAllMembers(e => e.Ignore());
                iEntityMap.Include<IEntity, ReportEntity>();
                iEntityMap.Include<IEntity, ScheduledTaskEntity>();
                iEntityMap.Include<IEntity, DatabaseEntity>();
                iEntityMap.Include<IEntity, PerlScriptEntity>();
                iEntityMap
                    .ForMember(dest => dest.Created,
                        opts =>
                            opts.MapFrom(src => src.Created == DateTime.MinValue ? (DateTime?)null : (DateTime?)src.Created))
                    .ForMember(dest => dest.LastModified,
                        opts =>
                            opts.MapFrom(src => src.LastModified == DateTime.MinValue ? (DateTime?)null : (DateTime?)src.LastModified))
                    .ForMember(dest => dest.CreatedBy,
                        opts =>
                            opts.MapFrom(src => src.CreatedBy == null ? (Guid?)null : src.CreatedBy.Id))
                    .ForMember(dest => dest.LastModifiedBy,
                        opts =>
                            opts.MapFrom(src => src.LastModifiedBy == null ? (Guid?)null : src.LastModifiedBy.Id))
                    .ForMember(dest => dest.Name,
                        opts =>
                            opts.MapFrom(src => src.Name))
                    .ForMember(dest => dest.FullName,
                        opts =>
                            opts.MapFrom(src => src.FullName))
                    .ForMember(dest => dest.Definition,
                        opts =>
                            opts.MapFrom(src => src.Definition))
                    .ForMember(dest => dest.Type,
                        opts =>
                            opts.MapFrom(src => (int)src.Type))
                    .ForMember(dest => dest.Provider,
                        opts =>
                            opts.MapFrom(src => src.DataProvider))
                    .ForMember(dest => dest.Id,
                        opts =>
                            opts.MapFrom(src => src.Id))
                    .ForMember(dest => dest.InternalId,
                        opts =>
                            opts.MapFrom(src => src.InternalId))
                    .ForMember(dest => dest.Parent,
                        opts =>
                            opts.MapFrom(src => src.Scope == null ? (Guid?)null : src.Scope.Id))
                    .ForMember(dest => dest.Root,
                        opts =>
                            opts.MapFrom(src => src.Root == null ? (Guid?)null : src.Root.Id))
                    .ForMember(dest=>dest.Level,
                        opts=>
                            opts.MapFrom(src=>src.Level));

                cfg.CreateMap<IEntity, ReportEntity>();
                cfg.CreateMap<IEntity, PerlScriptEntity>();
                cfg.CreateMap<IEntity, ScheduledTaskEntity>()
                    .ForMember(dest => dest.TaskLastRunTime,
                        opts => opts.MapFrom(src=>(src as Entities.ScheduledTaskEntity).TaskLastRunTime == DateTime.MinValue ? null : (DateTime?)(src as Entities.ScheduledTaskEntity).TaskLastRunTime));
                cfg.CreateMap<IEntity, DatabaseEntity>()
                    .ForMember(dest => dest.Database,
                        opts => opts.MapFrom(src => (src as Entities.DatabaseEntity).Database.FullName))
                    .ForMember(dest => dest.JobLastRunTime,
                        opts => opts.MapFrom((src => (src as Entities.DatabaseEntity).JobLastRunTime == DateTime.MinValue ? null : (DateTime?)(src as Entities.DatabaseEntity).JobLastRunTime)));
            });
        }
        private static string ParseComplexName(string str)
        {
            if (str.Contains(':'))
            {
                StringBuilder sb = new StringBuilder();
                Regex reg = new Regex("[a-z]+:", RegexOptions.IgnoreCase);
                var match = reg.Match(str);
                str = str.Replace("/", "\\");
                str = sb.AppendFormat("{0} | {1}", str.Substring(0, match.Index).TrimEnd('\\'), str.Substring(match.Index)).ToString();
            }
            return str;
        }
    }
}
