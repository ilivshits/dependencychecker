
CREATE VIEW [dbo].[vSecuritySubTypes]
AS
SELECT     spt.description AS Type, sst.description AS SubType
FROM         dbo.SecurityType AS st INNER JOIN
              dbo.SecurityPrimaryType AS spt ON st.securityPrimaryTypeId = spt.securityPrimaryTypeId INNER JOIN
              dbo.SecuritySubType AS sst ON sst.securitySubTypeId = st.securitySubTypeId



