﻿-- =============================================
-- Modified by: fortress\mcvetanovic, Milan Cvetanovic
-- Modified date: <03/08/2014>
-- Description of change:	Tag migration
-- Used by reports:	\Imagine\InvestorRelations\FAMFPortfolioSensitivitiesDetail.rdl
-- \Imagine\InvestorRelations\Subrep_FAMFPortfolioSensitivitiesDetail.rdl
-- =============================================
--[cfg].[GetFAMFSensitivities] @aexeo_trader_id   = '1',	@meta_fund_id   = '48',	@live  = 0,		@asofdate  = '7/30/2013',@strategy ='R-HK-ASW'
--select FIG_Theta_$, * from MipsHist.dbo.z_mips_reporting1 where Legal_Entity_Name='FAMF' and as_of_date='2012-01-31'
CREATE PROCEDURE [cfg].[GetFAMFSensitivities]
  @aexeo_trader_id VARCHAR(500) = '1',
  @meta_fund_id    VARCHAR(500) = '48',
  @strategy        VARCHAR(max) = NULL,
  @live            BIT = 0,
  @asofdate        DATETIME = '5/31/2013',
  @run             BIT = 1,
  @RiskAssetClass  VARCHAR(50) = NULL
AS
  BEGIN
      SET NOCOUNT ON;

      --declare @aexeo_trader_id varchar(500)
      --set @aexeo_trader_id='96,53,123,131,129,133,134,7,135,137,138,139,140,141,1,4,71,120'
      --declare @meta_fund_id varchar(500)
      --set @meta_fund_id=48--'38' --'48'
      --declare @strategy varchar(max)
      --set @strategy=null
      --declare @live bit
      --set @live=0
      --declare @asofdate datetime
      --set @asofdate='5/31/2012'
      --declare @run bit
      --set @run=1
      IF (@run=0)
        RETURN;
        
        IF (@asofdate IS NULL)
			SET @asofdate = CONVERT(VARCHAR(8), getdate(), 112);
	        
		SELECT *
		INTO #all_trader_tags_detail
		FROM [P1TC].[fnTableDefinitionTagger]();

		INSERT INTO #all_trader_tags_detail
		EXEC [P1TC].[spGetTagValues] @asOf = @asofdate;        
        
      DECLARE @databaseName VARCHAR(50) = 'P1ReportingG2'
      DECLARE @repoEffectiveDate DATETIME

      SET @repoEffectiveDate = '20120601'

      --------------------------------------------------
      -- Calculate vol/var for both business line groupings and GICS classifications
      --------------------------------------------------
      DECLARE @myqry VARCHAR(MAX);

      CREATE TABLE #tmpInvestorView
        (
           [row_id]                   [INT] NOT NULL,
           [Trader]                   [VARCHAR](20) NULL,
           [tid]                      [FLOAT] NULL,
           [value_date]               [VARCHAR](MAX) NULL,
           [aexeo_qty]                [FLOAT] NULL,
           [im_type]                  [VARCHAR](MAX) NULL,
           [Acct]                     [VARCHAR](MAX) NULL,
           [New Business Line]        VARCHAR(MAX) NULL,
           [asset_1]                  [VARCHAR](50) NULL,
           [asset_2]                  [VARCHAR](50) NULL,
           [region_1]                 [VARCHAR](50) NULL,
           [region_2]                 [VARCHAR](50) NULL,
           [region_3]                 [VARCHAR](50) NULL,
           [gics_1]                   [VARCHAR](50) NULL,
           FIG_Asset_Class_Imagine    [VARCHAR](50) NULL,
           FIG_Asset_Class            [VARCHAR](50) NULL,
           FIG_Asset_Type             [VARCHAR](50) NULL,
           RiskAssetClassExposure     FLOAT NULL,
           RiskAssetClassLongShort    CHAR(1) NULL,
           isSovereign                FLOAT NULL,
           Issuer_Name                [VARCHAR](50) NULL,
           [ReportingTraderSortOrder] [VARCHAR](100) NULL,
           [TraderName]               [VARCHAR](100) NULL,
           aexeo_trader_id            INT NULL,
           Legal_Entity_Name          [VARCHAR](100) NULL,
           Long_Security_Name         [VARCHAR](100) NULL,
           Usym                       [VARCHAR](100) NULL,
           Underlying_Type            [VARCHAR](100) NULL,
           [Issuer_Indst_Name]        [VARCHAR](100) NULL,
           [Issuer_Cntry_Name]        [VARCHAR](100) NULL,
           [FIG_Mkt_Value]            FLOAT NULL,
           [FIG_ESP_$]                FLOAT NULL,
           [FIG_Dv01_$]               FLOAT NULL,
           [FIG_Cr01_$]               FLOAT NULL,
           [FIG_Gamma_$]              FLOAT NULL,
           [FIG_Carry_$]              FLOAT NULL,
           [FIG_Vega_$]               FLOAT NULL,
           [FIG_Theta_$]              FLOAT NULL,
           [FIG_Mkt]                  FLOAT NULL,
           [Curr]                     [VARCHAR](100) NULL,
           Text1                      [VARCHAR](100) NULL,
           [FIG_Quantity]             FLOAT NULL
        )

      DECLARE @bbgdv01 FLOAT
      DECLARE @dt DATETIME

      IF (@live=1)
        BEGIN
            SELECT @dt=MAX([date])
            FROM   RiskData.dbo.BBG_DV01

            SELECT @bbgdv01=[value]/100
            FROM   RiskData.dbo.BBG_DV01
            WHERE  [date]=@dt
        END
      ELSE
        BEGIN
            SELECT @dt=MAX([date])
            FROM   RiskData.dbo.BBG_DV01
            WHERE  [date]<=@asofdate

            SELECT @bbgdv01=[value]/100
            FROM   RiskData.dbo.BBG_DV01
            WHERE  [date]=@dt

            IF (dbo.fnIsWeekDay(@asofdate)=0)
              BEGIN
                  SET @asofdate = dbo.fnPrevWeekDay(@asofdate)
              END
        END

      --------------------------------------------------
      -- Main vol/var query (to be passed to risk server)
      --------------------------------------------------
      SELECT r1.RowID,
             r1.FIG_Asset_Class,
             FIG_Asset_Type,
             r1.Acct,
             r1.Trader,
             r1.Issuer_Cntry_Code,
             r1.Underlying_Cntry_Code,
             r1.[Sector_Name:_GICS],
             r1.Legal_Entity_Name,
             r1.FIG_FX_Exposure_Currency,
             r1.FX_Risk_Curr,
             r1.FX_Base_Curr,
             r1.FIG_Value_Date,
             [Issuer_Indst_Name],
             [Issuer_Cntry_Name],
             [FIG_Mkt_Value],
             [FIG_ESP_$],
             [FIG_Dv01_$],
             [FIG_Cr01_$],
             [FIG_Gamma_$],
             [FIG_Carry_$],
             [FIG_Vega_$],
             [FIG_Theta_$],
             [FIG_Mkt],
             r1.[Curr],
             r1.Text1,
             r1.[FIG_Quantity],
             r1.[Long_Security_Name],
             r1.[Usym],
             r1.[Type],
             r1.Underlying_Type,
             r1.Maturity,
             ch.tid [Value10],
             ch.notional [NotionalEx],
             dbo.GetRiskAssetClassExposure(r1.FIG_Asset_Class, [FIG_ESP_$], [FIG_Dv01_$], [FIG_Cr01_$], @bbgdv01) RiskAssetClassExposure,
             dbo.GetRiskAssetClassLongShort(r1.FIG_Asset_Class, [FIG_ESP_$], [FIG_Dv01_$], [FIG_Cr01_$], ch.notional, r1.Type) RiskAssetClassLongShort,
             r1.isSovereign,
             r1.Issuer_Name
      INTO   #tmpReporting1
      FROM   [Mips].[dbo].[z_mips_reporting1] r1
             INNER JOIN Mips.dbo.v_current_holdings ch
               ON ch.holding_id=r1.RowID
      WHERE  @live=1
             AND abs([FIG_Quantity])>0.00001

      --       AND r1.[Type]<>'RP'
      INSERT #tmpReporting1
      SELECT r1.RowID,
             r1.FIG_Asset_Class,
             FIG_Asset_Type,
             r1.Acct,
             r1.Trader,
             r1.Issuer_Cntry_Code,
             r1.Underlying_Cntry_Code,
             r1.[Sector_Name:_GICS],
             r1.Legal_Entity_Name,
             r1.FIG_FX_Exposure_Currency,
             r1.FX_Risk_Curr,
             r1.FX_Base_Curr,
             CONVERT(VARCHAR(8), r1.[FIG_Value_Date], 112),
             [Issuer_Indst_Name],
             [Issuer_Cntry_Name],
             [FIG_Mkt_Value],
             [FIG_ESP_$],
             [FIG_Dv01_$],
             [FIG_Cr01_$],
             [FIG_Gamma_$],
             [FIG_Carry_$],
             [FIG_Vega_$],
             [FIG_Theta_$],
             [FIG_Mkt],
             r1.[Curr],
             r1.Text1,
             r1.[FIG_Quantity],
             r1.[Long_Security_Name],
             r1.[Usym],
             r1.[Type],
             r1.Underlying_Type,
             r1.Maturity,
             ch.tid [Value10],
             ch.notional [NotionalEx],
             dbo.GetRiskAssetClassExposure(r1.FIG_Asset_Class, [FIG_ESP_$], [FIG_Dv01_$], [FIG_Cr01_$], @bbgdv01) RiskAssetClassExposure,
             dbo.GetRiskAssetClassLongShort(r1.FIG_Asset_Class, [FIG_ESP_$], [FIG_Dv01_$], [FIG_Cr01_$], ch.notional, r1.Type) RiskAssetClassLongShort,
             r1.isSovereign,
             r1.Issuer_Name
      FROM   [MipsHist].[dbo].[z_mips_reporting1] r1 --Maturity
             JOIN [MipsHist].dbo.v_current_holdings ch
               ON r1.RowID=ch.holding_id
                  AND ch.as_of_date=r1.as_of_date
      WHERE  @live=0
             AND r1.as_of_date=@asofdate
             AND abs([FIG_Quantity])>0.00001

      --      AND r1.[Type]<>'RP'
      IF @asofdate<@repoEffectiveDate
        DELETE #tmpReporting1
        WHERE  Type='RP'

      ----------------------------
      -- identify asset swaps
      SELECT Acct,
             Abs(sum(r1.FIG_Dv01_$)/sum(Abs(r1.FIG_Dv01_$))) AssetSwapRatio
      INTO   #AssetSwapRatio
      FROM   #tmpReporting1 r1
      WHERE  r1.Acct IN (SELECT DISTINCT Acct
                         FROM   #tmpReporting1
                         WHERE  FIG_Asset_Type IN ('Bond Future', 'Government Bond')
                                AND Acct IN (SELECT DISTINCT Acct
                                             FROM   #tmpReporting1
                                             WHERE  FIG_Asset_Type IN ('IR Swap', 'IR Swaption')))
             AND r1.FIG_Dv01_$!=0
      GROUP  BY Acct

      -- end identify asset swaps
      SELECT *
      INTO   #p1tcCountry
      FROM   P1TC.fnTableDefinitionCountryCode()

      DECLARE @sql VARCHAR(4000)

      SET @sql = 'exec '+@databaseName+'.dbo.spGetCountryCodes @as_of_date='''''+CONVERT(VARCHAR(8), @asofdate, 112)+''''', @live='+cast(@live AS VARCHAR(1))
      ---------------------------
      --   INSERT @p1tcCountry
      --EXEC dbo.spGetCountryCodes @asofdate, @live
      SET @sql = N' INSERT INTO #p1tcCountry
					SELECT *
					FROM OPENQUERY([SELF], '''+@sql+''')';

      PRINT @sql

      EXEC (@sql)
      
      UPDATE #p1tcCountry
      SET    country_name='Greater China'
      WHERE  country_name IN ('China', 'Hong Kong', 'Taiwan', 'Macau')

      INSERT INTO #tmpInvestorView
                  ([row_id],
                   [Trader],
                   [tid],
                   [value_date],
                   [aexeo_qty],
                   [im_type],
                   [Acct],
                   [New Business Line],
                   [asset_1],
                   [asset_2],
                   [region_1],
                   [region_2],
                   [region_3],
                   [gics_1],
                   FIG_Asset_Class_Imagine,
                   FIG_Asset_Class,
                   FIG_Asset_Type,
                   ReportingTraderSortOrder,
                   [TraderName],
                   aexeo_trader_id,
                   Legal_Entity_Name,
                   Long_Security_Name,
                   Usym,
                   Underlying_Type,
                   [Issuer_Indst_Name],
                   [Issuer_Cntry_Name],
                   [FIG_Mkt_Value],
                   [FIG_ESP_$],
                   [FIG_Dv01_$],
                   [FIG_Cr01_$],
                   [FIG_Gamma_$],
                   [FIG_Carry_$],
                   [FIG_Vega_$],
                   [FIG_Theta_$],
                   [FIG_Mkt],
                   [Curr],
                   Text1,
                   [FIG_Quantity],
                   RiskAssetClassExposure,
                   RiskAssetClassLongShort)
      (SELECT r1.[RowID] AS row_id,
              v_t.label Trader,
              r1.[Value10] AS tid,
              CONVERT(VARCHAR(8), r1.[FIG_Value_Date], 112) AS value_date,
              r1.NotionalEx AS aexeo_qty,
              r1.Type AS im_type,
              r1.[Acct],
              COALESCE(tag.BusPA, tag.[investorCountryOverrideRegion2], '') AS [New Business Line],
              -- start: left chart/table
              [asset_1]=P1Reporting.dbo.GetRiskAssetClassAdj(r1.FIG_Asset_Class, r1.FIG_Asset_Type, r1.[Issuer_Indst_Name], r1.Acct, r1.isSovereign, assetswap.AssetSwapRatio),
              CASE --WHEN r1.FIG_Asset_Class = 'FX' AND left(r1.Acct, 2) <> 'X-' THEN 'FX Hedge' 
                WHEN ','+@meta_fund_id+',' LIKE '%,48,%'
                     AND r1.FIG_Asset_Class='Commodity'
                THEN COALESCE(tag.BusPA, tag.[investorCountryOverrideRegion2], 'Commodity')
                WHEN ','+@meta_fund_id+',' LIKE '%,48,%'
                THEN isnull(c.region2, '')
                WHEN r1.FIG_Asset_Class='Commodity'
                THEN 'Commodity'
                ELSE c.region1
              END AS [asset_2]
              -- end: left chart/table
              ,
              isnull(c.divide, 'Emerging') AS [region_1]
              -- start: right chart/table
              ,
              CASE
                WHEN ','+@meta_fund_id+',' LIKE '%,48,%'
                     AND r1.FIG_Asset_Class='Commodity'
                THEN COALESCE(tag.BusPA, tag.[investorCountryOverrideRegion2], 'Commodity')
                WHEN ','+@meta_fund_id+',' LIKE '%,48,%'
                THEN isnull(c.region2, '')
                WHEN r1.FIG_Asset_Class='Commodity'
                THEN 'Commodity'
                ELSE c.region1
              END [region2],
              [region3]=P1Reporting.dbo.GetRiskAssetClassAdj(r1.FIG_Asset_Class, r1.FIG_Asset_Type, r1.[Issuer_Indst_Name], r1.Acct, r1.isSovereign, assetswap.AssetSwapRatio),
              -- end: right chart/table
              CASE
                WHEN isnull(r1.[Sector_Name:_GICS], '')=''
                THEN '*None*'
                ELSE r1.[Sector_Name:_GICS]
              END [gics_1] --r1.[Industry_Name:_GICS]
              ,
              FIG_Asset_Class_Imagine = r1.FIG_Asset_Class,
              FIG_Asset_Class = P1Reporting.dbo.GetRiskAssetClassAdj(r1.FIG_Asset_Class, r1.FIG_Asset_Type, r1.[Issuer_Indst_Name], r1.Acct, r1.isSovereign, assetswap.AssetSwapRatio),
              r1.FIG_Asset_Type,
              v_t.sort_order [ReportingTraderSortOrder],
              v_t.name [TraderName],
              v_t.aexeo_trader_id,
              r1.Legal_Entity_Name,
              r1.[Long_Security_Name],
              r1.[Usym],
              r1.Underlying_Type,
              CASE
                WHEN charindex('XAU', r1.[Usym])>0
                THEN 'X-GOLD'
                WHEN charindex('XPD', r1.[Usym])>0
                THEN 'X-PALLADIUM'
                WHEN charindex('XPT', r1.[Usym])>0
                THEN 'X-PLATINUM'
                WHEN charindex('XAG', r1.[Usym])>0
                THEN 'X-SILVER'
                ELSE r1.[Issuer_Indst_Name]
              END [Issuer_Indst_Name],
              r1.[Issuer_Cntry_Name],
              r1.[FIG_Mkt_Value],
              r1.[FIG_ESP_$],
              r1.[FIG_Dv01_$],
              r1.[FIG_Cr01_$],
              r1.[FIG_Gamma_$],
              r1.[FIG_Carry_$],
              r1.[FIG_Vega_$],
              r1.[FIG_Theta_$],
              r1.[FIG_Mkt],
              r1.[Curr],
              r1.Text1,
              r1.FIG_Quantity,
              r1.RiskAssetClassExposure,
              r1.RiskAssetClassLongShort
       FROM   #tmpReporting1 r1
              LEFT JOIN foffice.v_trader v_t
                ON v_t.abbrev=r1.Trader
              LEFT JOIN foffice.v_legalentity v_l
                ON v_l.le_name=r1.Legal_Entity_Name
              LEFT JOIN #all_trader_tags_detail tag
                ON tag.aexeo_trader_id=v_t.aexeo_trader_id
                   AND upper(tag.strategy)=CASE
                                             WHEN r1.Acct='EMPTY'
                                             THEN ''
                                             ELSE r1.Acct
                                           END
              LEFT JOIN #p1tcCountry c
                ON r1.RowID=c.row_id
              LEFT JOIN #AssetSwapRatio assetswap
                ON assetswap.Acct=r1.Acct
       WHERE  v_t.aexeo_trader_id IN (SELECT *
                                      FROM   dbo.fnFunSplit(@aexeo_trader_id, ','))
              AND v_l.fundId IN (SELECT *
                                 FROM   dbo.fnFunSplit(@meta_fund_id, ','))
              AND (@strategy IS NULL
                    OR r1.Acct IN (SELECT Items COLLATE Latin1_General_BIN2
                                   FROM   dbo.fnFunSplit(@strategy, ','))))

      SELECT r1.row_id,
             r1.Trader [ReportingTrader],
             r1.[ReportingTraderSortOrder],
             r1.[TraderName],
             r1.aexeo_trader_id,
             r1.Legal_Entity_Name,
             r1.[Acct],
             r1.[FIG_Quantity],
             r1.[Long_Security_Name],
             r1.[Usym],
             r1.im_type,
             r1.Underlying_Type,
             r1.[Issuer_Indst_Name],
             r1.[Issuer_Cntry_Name],
             r1.[FIG_Mkt_Value],
             r1.[FIG_ESP_$],
             r1.[FIG_Dv01_$],
             r1.[FIG_Cr01_$],
             r1.[FIG_Gamma_$],
             r1.[FIG_Carry_$],
             r1.[FIG_Vega_$],
             r1.[FIG_Theta_$],
             r1.[FIG_Mkt],
             r1.[Curr],
             r1.Text1,
             r1.asset_1,
             r1.[asset_2],
             r1.region_1,
             r1.[region_2],
             CASE
               WHEN r1.region_1='Commodities'
               THEN 'Commodities'
               WHEN isnull(r1.region_3, '')=''
               THEN 'Other'
               ELSE r1.region_3
             END [region_3],
             FIG_Asset_Class_Imagine,
             FIG_Asset_Class,
             RiskAssetClassExposure = abs(r1.RiskAssetClassExposure),
             r1.RiskAssetClassLongShort
      FROM   #tmpInvestorView r1
      WHERE  (@RiskAssetClass IS NULL
               OR r1.FIG_Asset_Class=@RiskAssetClass)
  --select g=sum(abs(RiskAssetClassExposure)), n=sum(abs(RiskAssetClassExposure) * case when RiskAssetClassLongShort='L' then 1 else -1 end)
  --from #tmpInvestorView where FIG_Asset_Class='Credit'
  --select Long_Security_Name n, RiskAssetClassExposure,RiskAssetClassLongShort, * from #tmpInvestorView where FIG_Asset_Class='Equity' and Trader='Adam' and Acct='E-ASIATEC' order by Long_Security_Name
  --select FIG_Asset_Class_Imagine,FIG_Asset_Class,FIG_Asset_Type,* from #tmpInvestorView where row_id=10402797
	
	DROP TABLE #AssetSwapRatio;
	DROP TABLE #tmpInvestorView;
	DROP TABLE #tmpReporting1;
	DROP TABLE #p1tcCountry;
	DROP TABLE #all_trader_tags_detail;
	
  END
