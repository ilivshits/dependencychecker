﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using DependencyChecker.Web.Models;

namespace DependencyChecker.Web.Authorization
{
    public class ActiveDirectoryContext : IDisposable
    {
        private DirectoryEntry _directoryEntry = new DirectoryEntry(ConfigurationManager.ConnectionStrings["ActiveDirectory"].ConnectionString);

        public List<AdUser> GetAllUsers()
        {
            var adUsers = new List<AdUser>();

            using (DirectorySearcher search = new DirectorySearcher(_directoryEntry) { Filter = "(&(objectCategory=person)(objectClass=user))" } )
            {
                search.PropertiesToLoad.Add("givenName");   // first name
                search.PropertiesToLoad.Add("sn");          // last name
                search.PropertiesToLoad.Add("mail");        // smtp mail address
                search.PropertiesToLoad.Add("userPrincipalName");
                search.PropertiesToLoad.Add("sn");
                search.PageSize = 1000;

                var result = search.FindAll();

                adUsers.AddRange(from SearchResult user in result
                    where user.Properties.Contains("mail") || user.Properties.Contains("givenName") || user.Properties.Contains("sn") || user.Properties.Contains("userPrincipalName")
                    select new AdUser()
                    {
                        Email = user.Properties.Contains("mail") ? user.Properties["mail"][0].ToString() : null, FirstName = user.Properties.Contains("givenName") ? user.Properties["givenName"][0].ToString() : null, LastName = user.Properties.Contains("sn") ? user.Properties["sn"][0].ToString() : null, Username = user.Properties.Contains("userPrincipalName") ? string.Format("{0}\\{1}", _directoryEntry.Name.Substring(3), user.Properties["userPrincipalName"][0].ToString().Split('@')[0]) : null,
                    });
            }
            return adUsers;
        }

        public List<AdUser> GetUsersFilter(List<string> names)
        {
            StringBuilder namesQuery = new StringBuilder("");

            if (names.Count > 1)
            {
                namesQuery.Append("(|");
            }

            foreach (var name in names)
            {
                namesQuery.Append("(sAMAccountName=" + name.Substring(name.IndexOf("\\", StringComparison.Ordinal) + 1) + ")");
            }

            if (names.Count > 1)
            {
                namesQuery.Append(")");
            }

            var adUsers = new List<AdUser>();
            using (DirectorySearcher search = new DirectorySearcher(_directoryEntry) {Filter = "(&(objectClass=user)" + namesQuery + ")" })
            {
                search.PropertiesToLoad.Add("givenName");   // first name
                search.PropertiesToLoad.Add("userPrincipalName");          // last name
                search.PropertiesToLoad.Add("mail");        // smtp mail address
                search.PropertiesToLoad.Add("name");
                search.PropertiesToLoad.Add("sn");

                var users = search.FindAll();

                adUsers.AddRange(from SearchResult user in users
                                 select new AdUser()
                                 {
                                     Email = user.Properties.Contains("mail") ? user.Properties["mail"][0].ToString() : null,
                                     FirstName = user.Properties.Contains("givenName") ? user.Properties["givenName"][0].ToString() : null,
                                     LastName = user.Properties.Contains("sn") ? user.Properties["sn"][0].ToString() : null,
                                     Username = user.Properties.Contains("userPrincipalName") ? string.Format("{0}\\{1}", _directoryEntry.Name.Substring(3), user.Properties["userPrincipalName"][0].ToString().Split('@')[0]) : null,
                                 });
            }

            return adUsers;
        }

        public bool IsUserExisting(string username)
        {
            if (username.Contains('\\'))
            {
                username = username.Substring(username.IndexOf("\\", StringComparison.Ordinal) + 1);
            }
            using (DirectorySearcher search = new DirectorySearcher(_directoryEntry) { Filter = "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" + username + "))" })
            {
                var user = search.FindOne();
                return user != null;
            }
        }

        public string GetDomain()
        {
            return _directoryEntry.Name.Substring(3);
        }

        public void Dispose()
        {
            _directoryEntry.Dispose();
        }
    }
}
