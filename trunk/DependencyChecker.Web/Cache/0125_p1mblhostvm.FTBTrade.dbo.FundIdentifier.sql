CREATE view FundIdentifier with schemabinding as
select fundId = objectId, fundIdentifierTypeId = case identifierTypeId
      when 42 then 1
      when 43 then 2
      when 45 then 3
      when 47 then 4
      when 44 then 5
      else identifierTypeId end, code
from dbo.Identifier
where targetId = 6 and identifierTypeId in (42,43,44,45,47)

