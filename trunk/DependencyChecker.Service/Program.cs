﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using log4net.Config;

namespace DependencyChecker.Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
           // Thread.Sleep(10000); //TODO TODOTODO REMOVE!


            XmlConfigurator.Configure(new FileInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log4net.config")));
            var servicesToRun = new ServiceBase[] 
            { 
                new DependencyWindowsService() 
            };
            ServiceBase.Run(servicesToRun);
        }
    }
}
