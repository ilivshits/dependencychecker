﻿CREATE TABLE [log].[Batch]
(
	[batchId] INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
    [date] DATETIME2 NOT NULL, 
    [username] NVARCHAR(MAX) NULL,
	[result] nvarchar(max) not null DEFAULT 'Failed'
)
