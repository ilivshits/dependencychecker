-- =============================================
-- Author:		fortress\aboridko,Alexander Boridko
-- Create date: <8/14/2013>
-- Description:	function that returns current database name. 
-- Used by:	stored procedures, that use dynamic SQL that execute local sp from the current P1ReportingG2 database. 
--- this function allow to not override db name on the DEV / Test / ZZUAT / Prod env.	
-- For Reports: reports that use this sp: /Operations/LiquidityBreakDown
-- =============================================
CREATE FUNCTION [dbo].[fnGetP1ReportingG2DatabaseName]()
RETURNS varchar(200)
AS
BEGIN
	RETURN 'P1ReportingG2'
END

GO
