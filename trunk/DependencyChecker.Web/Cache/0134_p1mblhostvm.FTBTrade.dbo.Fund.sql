CREATE view Fund as
SELECT [fundId] = le.[legalEntityId]      
      ,le.[code]
      ,le.[description]
      ,le.[startDate]
      ,le.[endDate]
      ,le.[created]
      ,le.[version]
      ,le.[lastUpdate]
      ,le.[application_source]
      ,le.[system_source]
      ,le.[currencySid]
      ,le.[internalFlag]
      ,[businessUnitId]
      ,le.[lockupDate]      
FROM [dbo].[LegalEntity] le
JOIN [dbo].Fund2 f on le.fundId = f.fundId

