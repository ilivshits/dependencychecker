
create view [fortress].[v_Bloomberg_Map_FLT_PAY_DAY] as
	select '1st Monday' as FLT_PAY_DAY, 1 as unadjustedBusinessDayConventionPayDateId union
	select '1st Thursday of month' as FLT_PAY_DAY, 2 as unadjustedBusinessDayConventionPayDateId union
	select '1st Wednesday of month' as FLT_PAY_DAY, 3 as unadjustedBusinessDayConventionPayDateId union
	select '1st business day' as FLT_PAY_DAY, 4 as unadjustedBusinessDayConventionPayDateId union
	select '3rd Wednesday of month' as FLT_PAY_DAY, 5 as unadjustedBusinessDayConventionPayDateId union
	select 'At maturity' as FLT_PAY_DAY, 6 as unadjustedBusinessDayConventionPayDateId union
	select 'Irreg. Cpn (Mex Dom FRN)' as FLT_PAY_DAY, 7 as unadjustedBusinessDayConventionPayDateId union
	select 'Last Bus Day No Leap Yr' as FLT_PAY_DAY, 8 as unadjustedBusinessDayConventionPayDateId union
	select 'Last Day of month' as FLT_PAY_DAY, 9 as unadjustedBusinessDayConventionPayDateId union
	select 'Last business day' as FLT_PAY_DAY, 10 as unadjustedBusinessDayConventionPayDateId union
	select 'Odd Cpn Frequencies' as FLT_PAY_DAY, 11 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 1' as FLT_PAY_DAY, 12 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 2' as FLT_PAY_DAY, 13 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 3' as FLT_PAY_DAY, 14 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 4' as FLT_PAY_DAY, 15 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 5' as FLT_PAY_DAY, 16 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 6' as FLT_PAY_DAY, 17 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 7' as FLT_PAY_DAY, 18 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 8' as FLT_PAY_DAY, 19 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 9' as FLT_PAY_DAY, 20 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 10' as FLT_PAY_DAY, 21 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 11' as FLT_PAY_DAY, 22 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 12' as FLT_PAY_DAY, 23 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 13' as FLT_PAY_DAY, 24 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 14' as FLT_PAY_DAY, 25 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 15' as FLT_PAY_DAY, 26 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 16' as FLT_PAY_DAY, 27 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 17' as FLT_PAY_DAY, 28 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 18' as FLT_PAY_DAY, 29 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 19' as FLT_PAY_DAY, 30 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 20' as FLT_PAY_DAY, 31 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 21' as FLT_PAY_DAY, 32 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 22' as FLT_PAY_DAY, 33 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 23' as FLT_PAY_DAY, 34 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 24' as FLT_PAY_DAY, 35 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 25' as FLT_PAY_DAY, 36 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 26' as FLT_PAY_DAY, 37 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 27' as FLT_PAY_DAY, 38 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 28' as FLT_PAY_DAY, 39 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 29' as FLT_PAY_DAY, 40 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 30' as FLT_PAY_DAY, 41 as unadjustedBusinessDayConventionPayDateId union
	select 'Calendar day: 31' as FLT_PAY_DAY, 42 as unadjustedBusinessDayConventionPayDateId


