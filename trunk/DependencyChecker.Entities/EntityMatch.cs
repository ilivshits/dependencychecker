﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DependencyChecker.Entities
{
    public class EntityMatch
    {
        public EntityMatch()
        {
        }

        public EntityMatch(string name)
        {
            MatchText = name;
            Indexes = new List<int>{0};
        }

        public String ScopeText { get; set; }
        public String MatchText { get; set; }
        public List<int> Indexes { get; set; }

        public override String ToString()
        {
            return "'" + MatchText + "'" + (string.IsNullOrEmpty(ScopeText) ? String.Empty : " in scope '" + ScopeText + "'") + " matched in: " + String.Join(",", Indexes.Select(i => i.ToString()).ToArray());
        }
    }
}