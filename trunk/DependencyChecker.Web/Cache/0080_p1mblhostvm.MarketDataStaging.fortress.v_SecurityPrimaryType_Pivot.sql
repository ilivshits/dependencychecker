
create view [fortress].[v_SecurityPrimaryType_Pivot] as
	select
		0 as NotSpecified,
		1 as Currency,
		2 as Fx,
		3 as FxOption,
		4 as CorporateBond,
		5 as GovernmentBond,
		6 as Equity,
		7 as EquityOption,
		8 as EquityFuture,
		9 as [Index],
		10 as EquityFutureOption,
		11 as Commodity,
		12 as CommodityFuture,
		13 as CommodityFutureOption,
		14 as CurrencyFuture,
		15 as CurrencyFutureOption,
		16 as BondFuture,
		17 as BondFutureOption,
		18 as InterestRateFuture,
		19 as InterestRateFutureOption,
		20 as FreightIndex,
		21 as FreightFuture,
		22 as FreightFutureOption,
		23 as FreightSwap,
		24 as EnvironmentalIndex,
		25 as EnvironmentalFuture,
		26 as EnvironmentalFutureOption,
		27 as EnergyIndex,
		28 as EnergyFuture,
		29 as EnergyFutureOption,
		30 as IndexFuture,
		31 as IndexOption,
		32 as IndexFutureOption,
		33 as Metal,
		34 as MetalForward,
		35 as MetalOption,
		36 as BondOption,
		37 as ConvertibleBond,
		101 as InterestRateSwap,
		102 as ForwardRateAgreement,
		103 as CreditDefaultSwapOtc,
		104 as EquitySwap,
		105 as BondTotalReturnSwap,
		106 as IrSwaption,
		107 as VarianceSwap,
		108 as VolatilitySwap,
		109 as CommoditySwap,
		110 as ContractSpecification,
		111 as IndexSwap,
		112 as CommoditySwaption,
		113 as CreditDefaultSwapCleared,
		114 as AssetBacked,
		900 as CreditUnderlying,
		901 as InterestRateIndex,
		902 as InterestRateSwapTemplate,
		903 as ForwardRateAgreementTemplate,
		904 as VolatilitySurfacePoint,
		905 as FxForwardPoint,
		1000 as Generic


