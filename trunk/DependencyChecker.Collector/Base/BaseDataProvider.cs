﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using DependencyChecker.Collector.Interfaces;
using DependencyChecker.Common;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Base;
using DependencyChecker.Entities.Events;
using DependencyChecker.Entities.Interfaces;
using DependencyChecker.Entities.Utils;

namespace DependencyChecker.Collector.Base
{
    public abstract class BaseDataProvider : IDataProvider, IProgressReporter
    {
        private const long MatchLogTimeout = 1000;
        protected readonly IServerProvider ServerProvider;

        private readonly Regex _spaceCleanup = new Regex(@"[ ]+",
            RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.Compiled);

        protected BaseDataProvider(IServerProvider serverProvider)
        {
            ServerProvider = serverProvider;
            OnProgress(new DependencyCheckerProgressEventArgs
            {
                Message = "Starting " + Name + " load...",
                Progress = 0
            });
        }

        #region Implementation of IDataProvider
        private static readonly InvariantComparer InvariantComparer = new InvariantComparer();
        private static readonly List<String> Keywords = new List<string>
        {
            "INSERT",
            "SERVICE", 
            "CREATE", 
            "OPENQUERY", 
            "SELECT", 
            "INTO", 
            "CAST", 
            "CONVERT", 
            "GETDATE", 
            "DECLARE", 
            "UPDATE", 
            "ISNULL",
            "VARCHAR", 
            "INT", 
            "DECIMAL", 
            "AS", 
            "SET", 
            "AT", 
            "WHERE", 
            "DROP",
            "NEW",
            "sp_executesql"
        };

        public virtual bool IsKeyword(String text)
        {
            return Keywords.Contains(text, InvariantComparer) || text.StartsWith("#");
        }

        public event EventHandler<DependencyCheckerProgressEventArgs> Progress;
        protected virtual void OnProgress(DependencyCheckerProgressEventArgs e)
        {
            var handler = Progress;
            if (handler != null) handler(this, e);
        }


        public abstract string Name { get; }
        protected abstract List<Regex> ItemSearchPattern { get; }
        public virtual List<IEntity> Search()
        {
            return new List<IEntity>();
        }
        public virtual List<EntityMatch> MatchItems(string input, IEntity entity)
        {
            var sw = new Stopwatch();
            input = input.Trim();
            input = _spaceCleanup.Replace(input, " ");
            var matches = ItemSearchPattern.SelectMany(r => r.Matches(input).Cast<Match>());
            var result = new List<EntityMatch>();
            sw.Start();
            foreach (Match match in matches)
            {
                if (!match.Groups["name"].Success && !match.Groups["trigger"].Success && !match.Groups["exclude"].Success)
                {
                    entity.AddErrorFormat(EntityErrorCode.ParsingError, "Entity: {1} [{2}]. Content '{0}' has mismatch: '{3}'",
                        input, entity.FullName, entity.TypeName, match.Value);
                    continue;
                }
                sw.Stop();
                if (sw.ElapsedMilliseconds > MatchLogTimeout)
                {
                    LoggerHelper.Log.DebugFormat("MATCH '{2}' TOOK: {0} FOR '{1}'", sw.ElapsedMilliseconds, entity.FullName, match.Groups["name"].Value);
                }

                var i = 0;
                foreach (var capture in match.Groups["name"].Captures.Cast<Capture>())
                {
                    var captureText = capture.Value.Trim().ToLowerInvariant();
                    var scopeText = match.Groups["scope"].Success ? match.Groups["scope"].Captures[i].Value.Trim().ToLowerInvariant() : String.Empty;
                    i++;

                    if (IsKeyword(captureText))
                    {
                        continue;
                    }
                    var existing = result.SingleOrDefault(r => r.MatchText == captureText);
                    if (existing != null)
                    {
                        existing.Indexes.Add(capture.Index);
                    }
                    else
                    {
                        result.Add(new EntityMatch
                        {
                            MatchText = captureText,
                            ScopeText = scopeText,
                            Indexes = new List<int> { capture.Index }
                        });
                    }
                }
                sw.Restart();
            }
            if (sw.IsRunning)
            {
                sw.Stop();
                if (sw.ElapsedMilliseconds > MatchLogTimeout)
                {
                    LoggerHelper.Log.DebugFormat("MATCH WITH NO RESULT TOOK: {0} FOR '{1}'", sw.ElapsedMilliseconds, entity.FullName);
                }
            }
            return result.ToList();
        }

        #endregion
    }
}