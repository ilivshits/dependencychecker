﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using AutoMapper;
using DependencyChecker.Common;
using DependencyChecker.DataProvider.DAL;
using DependencyChecker.DataProvider.Extensions;
using DependencyChecker.Entities;
using DependencyChecker.Entities.Interfaces;

namespace DependencyChecker.DataProvider.Helpers
{
    public static class DatabaseHelpers
    {
        private static string _connectionString = "metadata=res://*/DependencyChecker.csdl|res://*/DependencyChecker.ssdl|res://*/DependencyChecker.msl;provider=System.Data.SqlClient;provider connection string=\"data source=P1MBLHOSTVM;initial catalog=DependencyChecker;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework\"";
        private static List<Guid> _addedGuids = new List<Guid>();
        private static readonly List<String> SystemDatabases = new List<string>
		// ReSharper disable StringLiteralTypo
		{
            "master", "model", "msdb", "tempdb"
        };

        public static void CreateNewBatch()
        {
            using (var ctx = DatabaseHelpers.GetContext())
            {
                ctx.Batch.Add(new Batch() { date = DateTime.Now, username = WindowsIdentity.GetCurrent()?.Name, result = BatchResult.Failed.ToString() });
                ctx.SaveChanges();
            }
        }

        public static void SetBatchSuccessful(int batchId)
        {
            using (var ctx = DatabaseHelpers.GetContext())
            {
                var original = ctx.Batch.Find(batchId);
                var updated = new DataProvider.Batch()
                {
                    batchId = original.batchId,
                    date = original.date,
                    result = BatchResult.Success.ToString(),
                    username = original.username
                };

                ctx.Entry(original).CurrentValues.SetValues(updated);
                ctx.SaveChanges();
            }
        }

        public static List<BatchException> GetBatchExceptions(int batchId)
        {
            using (var ctx = GetContext())
            {
                return
                    ctx.ExceptionLog.Where(
                        e =>
                            e.BatchId == batchId &&
                            (e.Level == log4net.Core.Level.Error.Name || e.Level == log4net.Core.Level.Fatal.Name || e.Level == log4net.Core.Level.Warn.Name))
                        .ToList()
                        .Select(
                            e =>
                                new BatchException()
                                {
                                    BatchId = e.BatchId,
                                    Exception = e.Exception,
                                    Date = e.Date,
                                    Message = e.Message,
                                    Level = e.Level,
                                    Logger = e.Logger
                                })
                        .ToList();
            }

        }

        public static List<Common.Batch> GetBatches(DateTime? from = null, DateTime? to = null)
        {
            using (var ctx = GetContext())
            {
                if (from == null || to == null)
                {
                    return
                    ctx.Batch.ToList().Select(e => new Common.Batch() { Date = e.date.ToString("s"), Id = e.batchId, Username = e.username, Status = (BatchResult)Enum.Parse(typeof(BatchResult), e.result) })
                        .ToList();
                }
                else
                {
                    return
                    ctx.Batch.ToList().Where(e => e.date.Date >= from.Value.Date && e.date.Date <= to.Value.Date).Select(e => new Common.Batch() { Date = e.date.ToString("s"), Id = e.batchId, Username = e.username, Status = (BatchResult)Enum.Parse(typeof(BatchResult), e.result) })
                        .ToList();
                }
                
            }
        }

        public static List<Entity> GetBatchEntities(int batchId)
        {
            using (var ctx = GetContext())
            {
                return ctx.ErrorsArchive.Where(e => e.batchId == batchId).Select(e => new Entity() {Type = e.entityType, Name = e.entityName}).Distinct().ToList();
            }
        }

        public static List<String> GetEntityErrors(int batchId, string entityName)
        {
            using (var ctx = GetContext())
            {
                return ctx.ErrorsArchive.Where(e => e.batchId == batchId).Where(e=>e.entityName == entityName).Select(e => e.errorText).ToList();
            }
        }

        public static int GetCurrentBatchId()
        {
            using (var ctx = GetContext())
            {
                if (!ctx.Batch.Any()) throw new Exception("Batch table is empty.");
                return ctx.Batch.OrderByDescending(e => e.batchId).Select(e=>e.batchId).First();
            }
        }

        public static string GetTableName(DependencyCheckerEntities context, Type entityType)
        {
            var sc = DatabaseHelpers.GetSContainer(context);
            var set = sc.EntitySets.Single(e => e.Name == entityType.Name);
            return set.Name;
        }

        public static string GetSchemaName(DependencyCheckerEntities context, Type entityType)
        {
            var sc = DatabaseHelpers.GetSContainer(context);
            var set = sc.EntitySets.Single(e => e.Name == entityType.Name);
            return set.Schema;
        }

        public static List<Entity> GetDatabases()
        {
            using (var ctx = GetContext())
            {
                var dbs =
                    ctx.EntityBase.Where(
                        e => e.Type == (int) Entities.EntityType.Database && SystemDatabases.All(s => s != e.Name))
                        .ToList();
                return dbs.Select(MapperManager<Entity>.Map).ToList();
            }
        } 

        public static Entity Drillthrough(Guid uid, string[] types)
        {
            var typesList = new List<Entities.EntityType>();
            Entities.EntityType type;
            foreach (var typeName in types)
            {
                if (Enum.TryParse(typeName, out type))
                {
                    typesList.Add(type);
                }
            }

            var e = GetEntityDetailsById(uid, typesList);
            return e;
            
        }

        public static String ExportToText(Guid id)
        {
            using (var ctx = GetContext())
            {
                var entity = ctx.EntityBase.Include(e=>e.EntityType).Single(e=>e.Id == id);
                var errors = ctx.EntityError.Where(e => e.EntityId == id).ToList();
                var dependsOnIds =
                    ctx.Dependency.Where(e => e.SourceId == id).Select(e => e.DependsOnId).ToList();
                var usedByIds =
                    ctx.Dependency.Where(e => e.DependsOnId == id).Select(e => e.SourceId).ToList();

                var sb = new StringBuilder();
                //Log.InfoFormat("Printing object '{0}'...", entity.FullName);
                sb.AppendLine(String.Format("Object '{0}' of type '{1}'", entity.FullName, entity.EntityType.Name));

                sb.AppendLine("\r\nUsed by:\r\n");
                sb.AppendLine("***********************************************");
                sb.AppendLine(DependenciesToString(usedByIds));
                sb.AppendLine("***********************************************");

                sb.AppendLine("\r\nDepends on:\r\n");
                sb.AppendLine("***********************************************");
                sb.AppendLine(DependenciesToString(dependsOnIds));
                sb.AppendLine("***********************************************");

                sb.AppendLine("\r\nIssues detected:\r\n");
                sb.AppendLine("***********************************************");
                foreach (var error in errors)
                {
                    sb.AppendLine(error.Message);
                }
                sb.AppendLine("***********************************************");

                sb.AppendLine();
                sb.AppendLine("\r\nDefinition:\r\n");
                sb.AppendLine("***********************************************");
                sb.AppendLine(entity.Definition);
                sb.AppendLine("***********************************************");
                return sb.ToString();
            }
        }

        public static string PrintDefinition(Guid id)
        {
            using (var ctx = GetContext())
            {
                var entity = ctx.EntityBase.Single(e => e.Id == id);

                return entity.Definition;
            }
        }
        
        public static string HierarchyToString(DependencyCheckerEntities context, Dictionary<Guid, Guid?> hierarchy)
        {
            var entities = context.EntityBase.Where(e => hierarchy.Keys.Contains(e.Id)).ToList();

            Guid? root = hierarchy.First(e=>!e.Value.HasValue || !hierarchy.Keys.Contains(e.Value.Value)).Key;

            Func<Guid, List<EntityTree>> GetChildren = null;
            GetChildren = (parentId) =>
            {
                var ch = hierarchy.Where(e => e.Value == parentId)
                    .Select(e => new EntityTree() {Id = e.Key, Children = GetChildren(e.Key) }).ToList();
                return ch;
            };

            EntityTree treeRoot = new EntityTree {Id = root.Value};
            treeRoot.Children = hierarchy.Where(e => e.Value == treeRoot.Id).Select(e => new EntityTree() {Id = e.Key, Children = GetChildren(e.Key) }).ToList();

            Func<EntityTree, int, string> EntityToString = null;
            EntityToString = (eTree, lvl) =>
            {
                var entity = entities.First(e => e.Id == eTree.Id);
                string result = $"{new string('-', lvl)}{entity.Name} ({(Entities.EntityType) entity.Type})\n";

                foreach (var tree in eTree.Children)
                {
                    result += EntityToString(tree, lvl + 1);
                }
                

                return result;
            };

            return EntityToString(treeRoot, 0);
        }

        public static string DependenciesToString(List<Guid> ids)
        {
            var sb = new StringBuilder("");

            using (var ctx = GetContext())
            {
                var entities = ctx.EntityBase.Where(e => ids.Contains(e.Id)).ToList();

                var groups = entities.GroupBy(e => ((Entities.EntityType) e.Type).ToString()).ToList();
                
                foreach (var group in groups.OrderBy(e => e.Key))
                {
                    sb.AppendLine($"{group.Key} entities:");
                    foreach (var groupItem in group.OrderBy(e => e.FullName))
                    {
                        sb.AppendLine($"{groupItem.FullName} ({group.Key})");
                    }
                    sb.AppendLine();
                }
            }

            return sb.ToString();
        }

        public static Dictionary<Guid, Guid?> GetDependsOnRecursively(Guid id)
        {
            var result = new Dictionary<Guid, Guid?> { {id, null} };

            using (var ctx = GetContext())
            {
                GetDependencies(ctx, id, true, result, true, true);
            }

            _addedGuids.Clear();
            return result;
        }

        public static Dictionary<Guid, Guid?> GetDependsOn(Guid id)
        {
            var result = new Dictionary<Guid, Guid?>();

            using (var ctx = GetContext())
            {
                GetDependencies(ctx, id, true, result, false);
            }

            _addedGuids.Clear();
            return result;
        }

        public static Dictionary<Guid, Guid?> GetUsedByRecursively(Guid id)
        {
            var result = new Dictionary<Guid, Guid?> { { id, null } };

            using (var ctx = GetContext())
            {
                GetDependencies(ctx, id, false, result, true, true);
            }

            _addedGuids.Clear();
            return result;
        }

        public static Dictionary<Guid, Guid?> GetUsedBy(Guid id)
        {
            var result = new Dictionary<Guid, Guid?>();

            using (var ctx = GetContext())
            {
                GetDependencies(ctx, id, false, result, false);
            }

            _addedGuids.Clear();
            return result;
        }

        public static DependencyCheckerEntities GetContext()
        {
            var ctx = new DependencyCheckerEntities(_connectionString);
            ctx.Configuration.AutoDetectChangesEnabled = false;
            ctx.Configuration.LazyLoadingEnabled = false;
            ctx.Configuration.ProxyCreationEnabled = false;
            return ctx;
        }

        public static List<Entity> Search(string inputName, bool useRegularExpression = false,
            bool searchInDefinition = false, bool exactSearch = false)
        {
            List<Entity> result = new List<Entity>();

            using (var context = GetContext())
            {
                if (!searchInDefinition)
                {
                    if (useRegularExpression)
                    {
                        //It is necessary to create new context to avoid EF error. Kind of workaround.
                        var ctx = GetContext();
                        List<Guid> ids = context.EntityBase.RegexSearch("FullName", inputName, context).Select(c=>c.Id).ToList();
                        result = GetEntities(ids, ctx);
                        return result;
                    }
                    else
                    {
                        if (exactSearch)
                        {
                            List<Guid> ids = context.EntityBase.Where(s => s.FullName == inputName).Select(s=>s.Id).ToList();
                            result = GetEntities(ids, context);
                            return result;

                        }
                        else
                        {
                            List<Guid> ids = context.EntityBase.Where(s => s.FullName.ToLower().Contains(inputName.ToLower())).Select(s => s.Id).ToList();
                            result = GetEntities(ids, context);
                            return result;
                        }
                    }
                }
                else
                {
                    if (useRegularExpression)
                    {
                        var ctx = GetContext();
                        List<Guid> ids = context.EntityBase.RegexSearch("Definition", inputName, context).Select(c => c.Id).ToList();
                        result = GetEntities(ids, ctx);
                        return result;
                    }
                    else
                    {
                        if (exactSearch)
                        {
                            List<Guid> ids = context.EntityBase.Where(s => s.Definition == inputName).Select(s => s.Id).ToList();
                            result = GetEntities(ids, context);
                            return result;

                        }
                        else
                        {
                            List<Guid> ids = context.EntityBase.Where(s => s.Definition.Contains(inputName)).Select(s => s.Id).ToList();
                            result = GetEntities(ids, context);
                            return result;
                        }
                    }
                }
               
            }
            return
                null;
        }

        public static Entity GetEntityDetailsById(Guid id, List<Entities.EntityType> drillThroughTypes = null)
        {
            var baseSet = GetContext().Set<EntityBase>();
            var baseEntity = baseSet.Single(e => e.Id == id);

            var ctx = GetContext();
            EntityBase bEntity = null;
            if (baseEntity.Provider == EntityProviderType.SQL.ToString())
            {
                DbSet<DatabaseEntity> set = ctx.Set<DatabaseEntity>();
                bEntity = set.Include(s => s.PersonEntity).Include(s => s.PersonEntity1).Single(s => s.Id == id);
            }
            else if (baseEntity.Provider == EntityProviderType.ScheduledTask.ToString())
            {
                DbSet<ScheduledTaskEntity> set = ctx.Set<ScheduledTaskEntity>();
                bEntity = set.Include(s => s.PersonEntity).Include(s => s.PersonEntity1).Single(s => s.Id == id);;
            }
            else if (baseEntity.Provider == EntityProviderType.SSRS.ToString())
            {
                DbSet<ReportEntity> set = ctx.Set<ReportEntity>();
                bEntity = set.Include(s => s.PersonEntity).Include(s => s.PersonEntity1).Single(s => s.Id == id);
            }
            else if (baseEntity.Provider == EntityProviderType.Perl.ToString())
            { 
                DbSet<PerlScriptEntity> set = ctx.Set<PerlScriptEntity>();
                bEntity = set.Include(s => s.PersonEntity).Include(s => s.PersonEntity1).Single(s => s.Id == id);
            }

            Entity entity = bEntity != null
                ? GetEntityDetails(ctx, bEntity, drillThroughTypes)
                : null;

            return entity;
        }


        public static MetadataWorkspace GetMetadata(DbContext ctx)
        {
            var metadata = ((IObjectContextAdapter)ctx).ObjectContext.MetadataWorkspace;
            return metadata;
        }

        public static ObjectItemCollection GetObjectItemCollection(DbContext ctx)
        {
            var metadata = ((IObjectContextAdapter)ctx).ObjectContext.MetadataWorkspace;

            // Object part of the model that contains info about the actual CLR types
            var objectItemCollection = ((ObjectItemCollection)metadata.GetItemCollection(DataSpace.OSpace));

            return objectItemCollection;
        }

        /// <summary>
        /// Returns info about the entities classes
        /// </summary>
        /// <param name="ctx"></param>
        /// <returns></returns>
        public static EntityContainer GetCContainer(DbContext ctx)
        {
            var metadata = ((IObjectContextAdapter)ctx).ObjectContext.MetadataWorkspace;

            var conceptualContainer = metadata.GetItems<EntityContainer>(DataSpace.CSpace).Single();

            return conceptualContainer;
        }

        /// <summary>
        /// Returns info about the shape of tables
        /// </summary>
        /// <param name="ctx"></param>
        /// <returns></returns>
        public static EntityContainer GetSContainer(DbContext ctx)
        {
            var metadata = ((IObjectContextAdapter)ctx).ObjectContext.MetadataWorkspace;

            // Storage part of the model has info about the shape of our tables
            var storeContainer = metadata.GetItems<EntityContainer>(DataSpace.SSpace).Single();

            return storeContainer;
        }

        /// <summary>
        /// Bulk insert entities
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="items"></param>
        /// <param name="connection"></param>
        public static void InsertAll<TEntity>(IEnumerable<TEntity> items, DbConnection connection = null)
            where TEntity : class
        {
            BulkInsertManager<TEntity> manager = new BulkInsertManager<TEntity>(GetContext());

            manager.InsertAll(items, connection);
        }

        #region Private methods

        private static List<Entity> GetEntities(List<Guid> ids, DependencyCheckerEntities ctx)
        {
            List<Entity> result = new List<Entity>();

            result.AddRange(ctx.Set<DatabaseEntity>().Where(c => ids.Contains(c.Id)).AsEnumerable().Select(MapperManager<Entity>.Map));
            result.AddRange(ctx.Set<ScheduledTaskEntity>().Where(c => ids.Contains(c.Id)).AsEnumerable().Select(MapperManager<Entity>.Map));
            result.AddRange(ctx.Set<ReportEntity>().Where(c => ids.Contains(c.Id)).AsEnumerable().Select(MapperManager<Entity>.Map));

            return result;
        }

        private static List<Entity> GetDependsOnEntities(DependencyCheckerEntities ctx, Guid id, List<Entities.EntityType> drillThroughTypes = null)
        {
            var depSet = ctx.Set<Dependency>();

            var dependsOnIds = depSet.Where(e => e.SourceId == id).Select(e => e.DependsOnId).Cast<Guid>();
            
            List<EntityBase> dependsOnEnts = new List<EntityBase>();

            dependsOnEnts.AddRange(drillThroughTypes != null
                ? ctx.Set<DatabaseEntity>().Where(e => drillThroughTypes.Contains((Entities.EntityType)e.Type)).Where(e => dependsOnIds.Contains(e.Id))
                : ctx.Set<DatabaseEntity>().Where(e => dependsOnIds.Contains(e.Id)));
            dependsOnEnts.AddRange(drillThroughTypes != null
                ? ctx.Set<ReportEntity>().Where(e => drillThroughTypes.Contains((Entities.EntityType)e.Type)).Where(e => dependsOnIds.Contains(e.Id))
                : ctx.Set<ReportEntity>().Where(e => dependsOnIds.Contains(e.Id)));
            dependsOnEnts.AddRange(drillThroughTypes != null
                ? ctx.Set<ScheduledTaskEntity>().Where(e => drillThroughTypes.Contains((Entities.EntityType)e.Type)).Where(e => dependsOnIds.Contains(e.Id))
                : ctx.Set<ScheduledTaskEntity>().Where(e => dependsOnIds.Contains(e.Id)));
            dependsOnEnts.AddRange(drillThroughTypes != null
                ? ctx.Set<PerlScriptEntity>().Where(e => drillThroughTypes.Contains((Entities.EntityType)e.Type)).Where(e => dependsOnIds.Contains(e.Id))
                : ctx.Set<PerlScriptEntity>().Where(e => dependsOnIds.Contains(e.Id)));

            return dependsOnEnts.ToList().Select((e) => BuildEntity(e)).ToList();
        }

        private static List<Entity> GetUsedByEntities(DependencyCheckerEntities ctx, Guid id, List<Entities.EntityType> drillThroughTypes = null)
        {
            var depSet = ctx.Set<Dependency>();

            var usedByIds = depSet.Where(e => e.DependsOnId == id).Select(e => e.SourceId).Cast<Guid>();
           
            List<EntityBase> usedByEnts = new List<EntityBase>();
            
            usedByEnts.AddRange(drillThroughTypes != null 
                ? ctx.Set<DatabaseEntity>().Where(e => drillThroughTypes.Contains((Entities.EntityType)e.Type)).Where(e => usedByIds.Contains(e.Id))
                : ctx.Set<DatabaseEntity>().Where(e => usedByIds.Contains(e.Id)));
            usedByEnts.AddRange(drillThroughTypes != null
                ? ctx.Set<ReportEntity>().Where(e => drillThroughTypes.Contains((Entities.EntityType)e.Type)).Where(e => usedByIds.Contains(e.Id))
                : ctx.Set<ReportEntity>().Where(e => usedByIds.Contains(e.Id)));
            usedByEnts.AddRange(drillThroughTypes != null
                ? ctx.Set<ScheduledTaskEntity>().Where(e => drillThroughTypes.Contains((Entities.EntityType)e.Type)).Where(e => usedByIds.Contains(e.Id))
                : ctx.Set<ScheduledTaskEntity>().Where(e => usedByIds.Contains(e.Id)));
            usedByEnts.AddRange(drillThroughTypes != null
                ? ctx.Set<PerlScriptEntity>().Where(e => drillThroughTypes.Contains((Entities.EntityType)e.Type)).Where(e => usedByIds.Contains(e.Id))
                : ctx.Set<PerlScriptEntity>().Where(e => usedByIds.Contains(e.Id)));

            return usedByEnts.ToList().Select((e) => BuildEntity(e)).ToList();
        }

        private static Entity BuildEntity(EntityBase e, String[] errors = null, List<Entity> dependsOn = null, List<Entity> usedBy = null)
        {
            Entity result = MapperManager<Entity>.Map(e);

            result.Errors = errors ?? new string[0];
            result.DependsOn = dependsOn;
            result.UsedBy = usedBy;

            if (e is DatabaseEntity)
            {
                var dbE = e as DatabaseEntity;
                result.LastRun = dbE.JobLastRunTime?.ToString();
                result.Database = String.IsNullOrEmpty(dbE.Database) ? "UNDEFINED" : dbE.Database;
            }
            else if (e is ScheduledTaskEntity)
            {
                result.LastRun = (e as ScheduledTaskEntity).TaskLastRunTime?.ToString();
            }

            return result;
        }

        private static Entity GetEntityDetails(DependencyCheckerEntities ctx, EntityBase entity, List<Entities.EntityType> drillThroughTypes)
        {
            DbSet<EntityError> errorsSet = ctx.Set<EntityError>();

            List<Entity> dependsOnEnts = GetDependsOnEntities(ctx, entity.Id, drillThroughTypes);
            dependsOnEnts = dependsOnEnts.Where(e => e.Type != Entities.EntityType.Server.ToString() && e.Type != Entities.EntityType.Database.ToString()).ToList();
            List<Entity> usedByEnts = GetUsedByEntities(ctx, entity.Id, drillThroughTypes);
            usedByEnts = usedByEnts.Where(e => e.Type != Entities.EntityType.Server.ToString() && e.Type != Entities.EntityType.Database.ToString()).ToList();

            String[] errors = errorsSet.Where(e => e.EntityId == entity.Id).Select(e => e.Message).ToArray();

            Entity result = BuildEntity(entity, errors, dependsOnEnts, usedByEnts);

            return result;
        }

        private static void GetDependencies(DependencyCheckerEntities ctx, Guid id, bool directionDependents, Dictionary<Guid, Guid?> guidToParentVault, bool ignoreServerAndDb = true, bool recursive = false)
        {
            _addedGuids.Add(id);


            var deps = directionDependents ? GetDependsOnForId(id, ctx) : GeUsedByForId(id, ctx);

            var query = ctx.EntityBase.Where(e => deps.Contains(e.Id)).ToList();
            var depsVault = query
                .Select(e => new KeyValuePair<Guid, Guid?>(e.Id, e.Parent)).ToList();

            foreach (var dependency in depsVault.Where(d => !_addedGuids.Contains(d.Key) || (d.Value != null && guidToParentVault[d.Key] == null)))
            {
                if (guidToParentVault.Keys.Any(e => e == dependency.Key))
                {
                    if (dependency.Value != null)
                        guidToParentVault[dependency.Key] = dependency.Value;
                }
                else
                {
                    guidToParentVault.Add(dependency.Key, dependency.Value);
                }
                if (ignoreServerAndDb)
                {
                    var entityType = query.Single(e => e.Id == dependency.Key).Type;
                    if (entityType == (int)Entities.EntityType.Server || entityType == (int)Entities.EntityType.Database)
                        continue;
                }
                if (recursive)
                    GetDependencies(ctx, dependency.Key, directionDependents, guidToParentVault, ignoreServerAndDb, true);
            }

        }

        private static List<Guid> GetDependsOnForId(Guid id, DependencyCheckerEntities ctx)
        {
            var deps = ctx.Dependency.Where(e => e.SourceId == id).Select(e => e.DependsOnId);
            return deps.Cast<Guid>().ToList();
        }

        private static List<Guid> GeUsedByForId(Guid id, DependencyCheckerEntities ctx)
        {
            var deps = ctx.Dependency.Where(e => e.DependsOnId == id).Select(e => e.SourceId);
            return deps.Cast<Guid>().ToList();
        }

        private class EntityTree
        {
            public Guid Id { get; set; }
            public EntityTree Parent { get; set; }
            public List<EntityTree> Children{ get; set; }
        }

        #endregion
    }
}
